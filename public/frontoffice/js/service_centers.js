$(document).ready(function () {

    var site_url = base_url;
    var array = [];
    var state = $('.state');
    var city = $('.city');
    var category = $('.category');
    var subcategory = $('.subcategory');
    var form = $("#form");

    var dealers_list = $('#dealers_list');
    var map_canvas = $('#map-canvas');

    dealers_list.hide();
    map_canvas.hide();

    var selected = $('.state:selected').text();
    if (selected == '') {
        $('.find_dealers').attr('disabled', 'disabled');
    }

    //triggers when the state is changed
    state.change(function () {

        $selected_state = state.val();

        if ($selected_state != '') {

            $('.find_dealers').removeAttr('disabled');
            $('#loader').removeClass('hidden');
            $('#loader').show();
            city.empty();
            $url = site_url + "/service-centers/ajax-cities";
            $.post($url, form.serialize(), function (data) {

                var option = $('<option></option>');

                var total_cities = data.length;

                for ($i = 0; $i < total_cities; $i++) {

                    $option = $('<option></option>');
                    $option.attr('value', data[$i].city);
                    $option.text(data[$i].city);
                    city.append($option).show('slow');

                }

                $('#loader').hide();

            });

        } else {
            city.empty();
            $('.find_dealers').attr('disabled', 'disabled');
            $('.initialCopy').show('fast');
            //$('#dealers_list').remove();
            $('.complete_result_div').addClass('hidden');
        }

    });

    category.change(function () {

        $selected_category = category.val();

        if ($selected_category != '') {

            $('.find_dealers').removeAttr('disabled');
            $('#loader1').removeClass('hidden');
            $('#loader1').show();
            subcategory.empty();
            $url = site_url + "/service-centers/ajax-subcategories";
            $.post($url, form.serialize(), function (data) {

                var option = $('<option></option>');

                var total_subcategories = data.length;
                for ($i = 0; $i < total_subcategories; $i++) {

                    $option = $('<option></option>');
                    $option.attr('value', data[$i].name);
                    $option.text(data[$i].name);
                    subcategory.append($option).show('slow');

                }

                $('#loader1').hide();

            });

        } else {
            subcategory.empty();
            $('.find_dealers').attr('disabled', 'disabled');
            $('.initialCopy').show('fast');
            //$('#dealers_list').remove();
            $('.complete_result_div').addClass('hidden');
        }

    });

    //triggers when find service center button is clicked
    form.submit(function (e) {

        e.preventDefault();
        var category_val = category.val();
        var subcategory_val = subcategory.val();
        var state_val = state.val();
        var city_val = city.val();
        var flag = true;

        if (category_val == '' || subcategory_val == '') {
            $('.find_dealers').attr('disabled', 'disabled');
            flag = false;
        }

        if (state_val == '' || city_val == '') {
            $('.find_dealers').attr('disabled', 'disabled');
            flag = false;
        }

        if (flag == true) {
            array = [];
            $('.initialCopy').hide('fast');
            var url = form.attr('action');

            $('#no_product').html("");

            $('#loading').removeClass('hidden');
            $('.complete_result_div').addClass('hidden');

            $.post(url, form.serialize(), function (data) {

                var i = 1;
                dealers_list.hide('fast');
                map_canvas.hide('fast');

                $('.address_box').remove();
                var res = data.length;

                if (res > 0 && res != 'false') {

                    $.each(data, function (key, value) {
                        //  console.log(value);
                        var obj = {};
                        var id = value.id;
                        var shop_name = value.name;
                        var address = value.address1;
                        var city = value.city;
                        var state = value.state;
                        var pincode = is_undefined(value.pincode);
                        if (pincode == true) {
                            pincode = '';
                        } else {
                            pincode = value.pincode;
                        }
                        var phone = value.phone;
                        var mobile = value.mobile;
                        var email = value.email_id;
                        var contact_person = value.contact_person;

                        var address_container = '<div class="shadow address_box" ' + 'data-id=' + id + '>';
                        if (shop_name != '') {
                            address_container = address_container + "<h3 class='base_color'>" + shop_name + "</h3>"
                        }
                        if (address != '') {
                            address_container = address_container + address + "<br/>"
                        }
                        if (city != '' && state != '' && pincode != '') {
                            address_container = address_container + city + ', ' + state + ' - ' + pincode + "<br/>"
                        } else if (city != '' && state != '') {
                            address_container = address_container + city + ', ' + state + "<br/>"
                        } else if (city != '' && state == '' && pincode != '') {
                            address_container = address_container + city + ' - ' + pincode + "<br/>"
                        } else if (city != '' && state == '') {
                            address_container = address_container + city + "<br/>"
                        } else if (city == '' && state != '' && pincode != '') {
                            address_container = address_container + state + ' - ' + pincode + "<br/>"
                        } else if (city == '' && state != '') {
                            address_container = address_container + state + "<br/>"
                        }
                        if (phone != '') {
                            address_container = address_container + phone + "<br/>"
                        }
                        if (mobile) {
                            address_container = address_container + mobile + "<br/>"
                        }
                        if (email) {
                            address_container = address_container + email + "<br/>"
                        }
                        if (contact_person) {
                            address_container = address_container + contact_person + "<br/>"
                        }

                        address_container = address_container + '</div>'

                        dealers_list.append(address_container);
                        dealers_list.append('<div class="clearfix"></div>');
                        $('#results').removeClass('hidden');

                        obj['id'] = id;
                        obj['name'] = shop_name;
                        obj['address'] = address;
                        obj['city'] = city;
                        obj['state'] = state;
                        obj['pincode'] = pincode;
                        obj['phone'] = phone;
                        obj['mobile'] = mobile;
                        obj['email'] = email;
                        obj['contact_person'] = contact_person;

                        array.push(obj);
                    });

                    $('#loading').addClass('hidden');
                    $('.complete_result_div').removeClass('hidden');

                    initialize(array);   //initialize google map here

                    $('.default_text').addClass("hidden");
                    dealers_list.show('fast');
                    map_canvas.show('fast');


                } else {
                    $('#no_product').html("<h3>Sorry, no dealers found.</h3>");
                    $('#loading').addClass('hidden');
                    $('.complete_result_div').addClass('hidden');
                }

            });
        } else {
            return false;
        }


    });

    // Enable the visual refresh
    google.maps.visualRefresh = true;
    var markers = [];
    var windows = [];
    var map;

    function initialize(array) {

        var mapOptions = {
            center: new google.maps.LatLng(-25.363882, 131.044922),
            zoom: 1
        };

        map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);

        google.maps.visualRefresh = true;
        google.maps.event.addListenerOnce(map, 'idle', function () {
            google.maps.event.trigger(map, 'resize');
        });


        $.each(array, function (i, obj) {


            var geo = new google.maps.Geocoder;
            geo.geocode({'address': obj.name + ' ' + obj.address + ' ' + obj.city + ' ' + obj.state + ' ' + obj.pincode}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var markerObject = {
                        'id': obj.id,
                        'data': new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location,
                            title: obj.name
                        })
                    };

                    var phone = (obj.phone) ? '<span>Ph: ' + obj.phone + '</span></br>' : '';
                    var mobile = (obj.mobile) ? '<span>Mobile: ' + obj.mobile + '</span></br>' : '';
                    var email = (obj.email_id) ? '<span>Email Id: ' + obj.email_id + '</span></br>' : '';
                    var contact_person = (obj.contact_person) ? '<span>Contact Person: ' + obj.contact_person + '</span></br>' : '';
                    var pincode = (obj.pincode) ? '<span>' + "-" + obj.pincode + '</span></br>' : '</br>';
                    var content = '<div class="mapdetail">' +
                        '<h3 class="base_color">' + obj.name + '</h3>' +
                        '<span>' + obj.address + '</span></br>' +
                        '<span>' + obj.city + ' ' + obj.state + '</span>' + ' ' + pincode +
                        phone + mobile + email + contact_person +
                        '</div>';

                    markerObject.info =
                        new google.maps.InfoWindow({
                            content: content,
                            position: results[0].geometry.location
                        });

                    markers.push(markerObject);
                    console.log(markerObject);

                    google.maps.event.addListener(markerObject.data, 'click', function () {
                        $.each(markers, function (a, b) {
                            b.info.close();
                        });

                        var window = markerObject.info;
                        var marker = markerObject.data;
                        window.open(map, marker);
                    });

                    if (i == 0) {
                        map.setCenter(markerObject.data.getPosition());
                        map.setZoom(12);
                        //   markerObject.info.open(map, markerObject.data);
                    }

                } else {
                    console.log("Geocode was not successful for the following reason: " + status);
                }

            });

        });
    }


    //triggers when the address box is clicked
    $('.address_box').live('click', function () {
        var id = $(this).attr('data-id');
        showOnMap(parseInt(id));
    });

    function showOnMap(i) {
        var items = markers.filter(function (elem) {
//            return elem.id === i;
            return elem.id == i;
        });
        if (items.length === 1) {
            var item = items[0];
            map.setCenter(item.data.getPosition());
            item.info.open(map, item.data);
        }
    }


    function is_undefined(value) {
        if (typeof value === "undefined") {
            return true;
        } else {
            return value;
        }
    }


});