$(document).ready(function () {
    $('.accordionButton').click(function (e) {
        e.preventDefault();
        var accordian = $(this);
        accordian.removeClass('on');
        accordian.next('.accordionContent').slideUp('normal');
        accordian.find('.plusMinus').text('+');
        if ($(this).next().is(':hidden') == true) {
            $(this).addClass('on');
            $(this).next().slideDown('normal');
            $(this).children('.plusMinus').text('-');

//            var acc_div = $(this).parent().parent().parent().height();
//            var acc_div_offset = $(this).parent().parent().parent().offset();
//            var change_div = $("div.span4").next().next().next();
//            var change_div_top = acc_div_offset.top - acc_div;
//
//            change_div.css('top',change_div_top + 'px');
//            console.log(acc_div_offset.top + "height:" + acc_div);
//            console.log(acc_div_offset.top - acc_div);

        }
    });
    $('.accordionContent').hide();
});