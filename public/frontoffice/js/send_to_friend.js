/**
 * Created by anuj on 5/20/14.
 */

$(document).ready(function () {

    var send_message_form = $('#send_to_friend_form');
    send_message_form.validate();

    //triggers when the send to friend submit button is clicked, and clear all input and textarea
    $('#send_friend_button').click(function () {
        $('#send_to_friend_form input[type="text"]').val('');
        $('#send_to_friend_form textarea').val('');
    });


    $('.submit_message').click(function (e) {

        e.preventDefault();
        $('#order_status').html("");
        if (send_message_form.valid() == true) {

            $('#ajax_loader').removeClass('hide');
            var request_message_url = send_message_form.attr("action");

            $.post(request_message_url, send_message_form.serialize(), function (data) {
                if (data == 'true') {
                    $('#ajax_loader').addClass('hide');
                    $('#messge_status').html('Your message has been sent successfully').css({'color': '#4cae4c'});
                } else {
                    $('#ajax_loader').addClass('hide');
                    $('#messge_status').html('Some error occurred please try again later').css({'color': 'FF0000'});
                }

            });

        } else {
            e.preventDefault();
            return false;
        }

    });

});