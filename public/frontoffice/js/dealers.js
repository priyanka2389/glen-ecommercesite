$(document).ready(function () {

    var site_url = base_url;
    var array = [];
    var active_tab = $('.tab-content .tab-pane.active').attr('id');
    var form = $('#' + active_tab).find(".form");
    var state = form.find('.state');
    var city = form.find('.city');
    var location = form.find('.location');
    var category = form.find('.category');
    var subcategory = form.find('.subcategory');
    var pageType = form.find('.type');


    var dealers_list = $('#dealers_list');
    var gallery_list = $('#gallery_list');
    var map_canvas = $('#map-canvas');

    dealers_list.hide();
    gallery_list.hide();

    function initializemap() {

        var mapProp = {
            center: new google.maps.LatLng(21.7679, 78.8718),
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"), mapProp);
    }

    google.maps.event.addDomListener(window, 'load', initializemap);

    var selected = $('.state:selected').text();
    if (selected == '') {
        //$('.find_dealers').attr('disabled', 'disabled');
    }


    //triggers when the state is changed
    //state.change(function () {
    //
    //
    //});
    $('a[data-toggle="tab"]').on('shown', function (e) {
        var activeTab = $(e.target).text(); // active tab

        active_tab = $(this).attr('href');
        form = $(active_tab).find(".form");
        state = form.find('.state');
        city = form.find('.city');
        category = form.find('.category');
        subcategory = form.find('.subcategory');
        pageType = form.find('.type');

        //$url = site_url + "/dealers/ajax-states/" + pageType.val();
        //$.get($url, function (data) {
        //    if (data) {
        //        var states = '<option>Select State</option>';
        //        for (var i = 0; i < data.length; i++) {
        //            var city = data[i].state.charAt(0).toUpperCase() + data[i].state.substring(1).toLowerCase(); //convert string first character in uppercase and other characters in lowercase
        //            states += '<option value="' + data[i].state + '">' + city + '</option>';
        //        }
        //        state.html(states);
        //    }
        //});

        category.change(function () {

            $selected_category = category.val();
            if ($selected_category != '') {

                $('.find_dealers').removeAttr('disabled');
                $('#loader3').removeClass('hidden');
                $('#loader3').show();
                subcategory.empty();
                $url = site_url + "/dealers/ajax-subcategories";

                $.post($url, form.serialize(), function (data) {

                    $option = $('<option value="">Select Subcategory</option>');
                    subcategory.append($option);

                    var total_subcategories = data.length;
                    for ($i = 0; $i < total_subcategories; $i++) {

                        $option = $('<option></option>');
                        $option.attr('value', data[$i].name);
                        $option.text(data[$i].name);
                        subcategory.append($option).show('slow');

                    }

                    $('#loader3').hide();

                });

            } else {
                subcategory.empty();
                $('.find_dealers').attr('disabled', 'disabled');
                $('.initialCopy').show('fast');
                //$('#dealers_list').remove();
                $('.complete_result_div').addClass('hidden');
            }

        });
    });

//triggers when find dealers button is clicked
    $('form').submit(function (e) {

        e.preventDefault();
        e.stopPropagation();

        var category_val = category.val();
        var subcategory_val = subcategory.val();
        var state_val = state.val();
        var city_val = city.val();
        var location_val = location.val();
        var type_val = pageType.val();
        var dealersFound = false;
        var galleryFound = false;
        var flag = true;
        var submit_button = $(this).find('.find_dealers').attr('id');

        //console.log('category' + category_val + 'subcategory' + subcategory_val + 'state' + $(state).val() + 'city' + $(city).val());
        if (type_val == 'dealer') {
            form.validate({
                rules: {
                    category: "required",
                    subcategory: "required",
                    state1: "required",
                    city1: "required"
                },
                messages: {
                    category: "Category is required",
                    subcategory: "Subcategory is required",
                    state: "State is required",
                    city: "City is required"
                },
                errorPlacement: function (error, element) {

                    if (element.attr('name') == 'state') {
                        error.appendTo('#stateError');
                    }
                    if (element.attr('name') == 'city') {
                        error.appendTo('#cityError');
                    }
                }
            });

        } else {
            form.validate({
                rules: {
                    state: "required",
                    city: "required"
                },
                messages: {
                    state: "State is required",
                    city: "City is required"
                },
                errorPlacement: function (error, element) {
                    console.log(element);
                    if (element.attr('name') == 'state') {
                        error.appendTo('#stateError');
                        //$('#stateError').removeClass('hidden');
                    }
                    if (element.attr('name') == 'city') {
                        error.appendTo('#cityError');
                        //$('#cityError').removeClass('hidden');
                    }
                }
            });
        }


        if (form.valid()) {
            array = [];
            $('.initialCopy').hide('fast');
            var url = form.attr('action');

            $('#no_product').html("");

            $('#loading').removeClass('hidden');
            $('.complete_result_div').addClass('hidden');

            $.post(url, form.serialize(), function (data) {

                var i = 1;
                dealers_list.hide('fast');
                gallery_list.hide('fast');
                map_canvas.hide('fast');

                $('.address_box').remove();
                var total_stores = (data != false) ? data.length : 0;
                //console.log(total_stores);

                if (total_stores > 0) {

                    $.each(data, function (key, value) {
                        var obj = {};
                        var id = value.id;
                        if (value.type == 'retail_store') {
                            //var shop_name = 'Exclusive Display Center';
                            var cssClass = 'center_color';
                        } else {

                            var cssClass = 'base_color';
                        }

                        var shop_name = (value.name) ? value.name : '';
                        var address = value.address1;
                        var city = value.city;
                        var state = value.state;
                        var location = value.location;
                        var pincode = value.pincode;

                        if (pincode == null) {
                            pincode = '';
                        }
                        var phone = value.phone;

                        if ((value.mobile) != null || value.mobile != "NULL") {
                            var mobile = value.mobile;
                        } else {
                            var mobile = '';
                        }
                        var email = value.email_id;
                        var contact_person = value.contact_person;
                        var type = value.type;

                        var address_container = '<div class="shadow address_box span12" ' + 'data-id=' + id + '>';
                        address_container = address_container + "<div class='span2'><i class='fa fa-map-marker fa-3x'></i></div>";
                        address_container = address_container + "<div class='span10'>";
                        if (shop_name != '') {
                            address_container = address_container + "<h3 class=" + cssClass + ">" + shop_name + "</h3>"
                        }
                        if (address != '') {
                            address_container = address_container + address + "<br/>"
                        }
                        if (city != '' && state != '' && pincode != '') {
                            address_container = address_container + city + ', ' + state + ' - ' + pincode + "<br/>"
                        } else if (city != '' && state != '') {
                            address_container = address_container + city + ', ' + state + "<br/>"
                        } else if (city != '' && state == '' && pincode != '') {
                            address_container = address_container + city + ' - ' + pincode + "<br/>"
                        } else if (city != '' && state == '') {
                            address_container = address_container + city + "<br/>"
                        } else if (city == '' && state != '' && pincode != '') {
                            address_container = address_container + state + ' - ' + pincode + "<br/>"
                        } else if (city == '' && state != '') {
                            address_container = address_container + state + "<br/>"
                        }

                        if (phone) {
                            address_container = address_container + phone + "<br/>"
                        }
                        if (mobile) {
                            address_container = address_container + mobile + "<br/>"
                        }
                        if (email) {
                            address_container = address_container + email + "<br/>"
                        }
                        if (contact_person) {
                            address_container = address_container + contact_person + "<br/>"
                        }


                        address_container = address_container + '</div></div>'

                        if (type == type_val && type == 'retail_store') {
                            galleryFound = true;
                            gallery_list.append(address_container);
                            gallery_list.append('<div class="clearfix"></div>');
                        } else if (type == type_val && type == 'dealer') {
                            dealersFound = true;
                            dealers_list.append(address_container);
                            dealers_list.append('<div class="clearfix"></div>');
                        }

                        $('.results').removeClass('hidden');

                        obj['id'] = id;
                        obj['name'] = shop_name;
                        obj['address'] = address;
                        obj['city'] = city;
                        obj['state'] = state;
                        obj['location'] = location;
                        obj['pincode'] = pincode;
                        obj['phone'] = phone;
                        obj['mobile'] = mobile;
                        obj['email'] = email;
                        obj['contact_person'] = contact_person;
                        obj['type'] = type;

                        //if (type == pageType) {
                        array.push(obj);
                        //}

                    });

                    $('#loading').addClass('hidden');
                    $('.complete_result_div').removeClass('hidden');

                    dealers_list.show('fast');
                    gallery_list.show('fast')
                    initialize(array);   //initialize google map here

                    $('.default_text').addClass("hidden");

                    map_canvas.show('fast');

                    if (dealersFound == false) {
                        $('.dealers_div').hide();
                    } else {
                        $('.dealers_div').show();
                    }

                    if (galleryFound == false) {
                        $('.gallery_div').hide();
                    } else {
                        $('.gallery_div').show();
                    }

                } else {
                    $('#no_product').html("<h3>Sorry, no dealers found.</h3>");
                    $('#loading').addClass('hidden');
                    $('.complete_result_div').addClass('hidden');
                    map_canvas.show('fast');

                }

            });
        }

    });

    // Enable the visual refresh
    google.maps.visualRefresh = true;
    var markers = [];
    var windows = [];
    var map;

    function initialize(array) {

        var mapOptions = {
            center: new google.maps.LatLng(21.7679, 78.8718),
            zoom: 1
        };

        map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);

        google.maps.visualRefresh = true;
        google.maps.event.addListenerOnce(map, 'idle', function () {
            google.maps.event.trigger(map, 'resize');
        });


        $.each(array, function (i, obj) {


            var geo = new google.maps.Geocoder;
            geo.geocode({'address': obj.address + ',' + obj.state + ',' + obj.city + ',' + obj.location + ' ' + obj.pincode}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var markerObject = {
                        'id': obj.id,
                        'data': new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location,
                            title: obj.name
                        })
                    };

                    var phone = (obj.phone) ? '<span>Ph: ' + obj.phone + '</span></br>' : '';
                    var mobile = (obj.mobile) ? '<span>Mobile: ' + obj.mobile + '</span></br>' : '';
                    var email = (obj.email_id) ? '<span>Email Id: ' + obj.email_id + '</span></br>' : '';
                    var contact_person = (obj.contact_person) ? '<span>Contact Person: ' + obj.contact_person + '</span></br>' : '';
                    var pincode = (obj.pincode) ? '<span>' + "-" + obj.pincode + '</span></br>' : '</br>';
                    var content = '<div class="mapdetail">' +
                        '<h3 class="base_color">' + obj.name + '</h3>' +
                        '<span>' + obj.address + '</span></br>' +
                        '<span>' + obj.city + ' ' + obj.state + '</span>' + ' ' + pincode +
                        phone + mobile + email + contact_person +
                        '</div>';

                    markerObject.info =
                        new google.maps.InfoWindow({
                            content: content,
                            position: results[0].geometry.location
                        });

                    markers.push(markerObject);

                    google.maps.event.addListener(markerObject.data, 'click', function () {
                        $.each(markers, function (a, b) {
                            b.info.close();
                        });

                        var window = markerObject.info;
                        var marker = markerObject.data;
                        window.open(map, marker);
                    });

                    if (i == 0) {
                        map.setCenter(markerObject.data.getPosition());
                        map.setZoom(12);
                        //   markerObject.info.open(map, markerObject.data);
                    }

                } else {
                    console.log("Geocode was not successful for the following reason: " + status);
                }

            });

        });
    }


    //triggers when the address box is clicked
    $('.address_box').live('click', function () {
        var id = $(this).attr('data-id');
        showOnMap(parseInt(id));
    });

    function showOnMap(i) {
        var items = markers.filter(function (elem) {
//            return elem.id === i;
            return elem.id == i;
        });
        if (items.length === 1) {
            var item = items[0];
            map.setCenter(item.data.getPosition());
            item.info.open(map, item.data);
        }
    }


    function is_undefined(value) {
        if (typeof value === "undefined") {
            return true;
        } else {
            return value;
        }
    }


});
function getCities(state, city_control, loader) {

    //$selected_state = $(this).val();
    var stateName = $(state).val();

    var site_url = base_url;
    if (stateName != '') {

        $('.find_dealers').removeAttr('disabled');
        $('#' + loader).removeClass('hidden');
        $('#' + loader).show();
        $('.city option:first-child').attr("selected", "selected");
        $('.location option:first-child').attr("selected", "selected");

        var type = $(state).attr('data-item-type');

        $url = site_url + "/dealers/ajax-cities/" + stateName + '/' + type;

        $.get($url, function (data) {
            $('#' + loader).addClass('hidden');
            if (data) {
                //console.log(data);
                var cities = '<option value="">Select City</option>';
                for (var i = 0; i < data.length; i++) {
                    var city = data[i].city.charAt(0).toUpperCase() + data[i].city.substring(1).toLowerCase(); //convert string first character in uppercase and other characters in lowercase
                    cities += '<option value="' + data[i].city + '">' + city + '</option>';
                }

                $('#' + city_control).html(cities);
            }
        });
        //$.post($url, form.serialize(), function (data) {
        //
        //    var option = $('<option></option>');
        //
        //    var total_cities = data.length;
        //    for ($i = 0; $i < total_cities; $i++) {
        //
        //        $option = $('<option></option>');
        //        $option.attr('value', data[$i].city);
        //        var cityname = data[$i].city.charAt(0).toUpperCase() + data[$i].city.slice(1).toLowerCase();
        //        $option.text(cityname);
        //        city.append($option).show('slow');
        //
        //    }
        //
        //    $('#loader').hide();
        //
        //});

    } else {
        //city.empty();
        //$('.find_dealers').attr('disabled', 'disabled');
        $('.initialCopy').show('fast');
        //$('#dealers_list').remove();
        $('.complete_result_div').addClass('hidden');
    }

}

function getLocations(state, city, location_control, loader) {

    var selected_state = $('#' + state).val();
    var cityName = $(city).val();
    var site_url = base_url;
    if (cityName != '') {

        $('.find_dealers').removeAttr('disabled');
        $('#' + loader).removeClass('hidden');
        $('#' + loader).show();
        $('.location option:first-child').attr("selected", "selected");

        var type = $(city).attr('data-item-type');
        $url = site_url + "/dealers/ajax-locations/" + selected_state + '/' + cityName + '/' + type;

        $.get($url, function (data) {
            $('#' + loader).addClass('hidden');
            if (data) {

                var locations = '<option>Select Location</option>';
                for (var i = 0; i < data.length; i++) {
                    var location = data[i].location.charAt(0).toUpperCase() + data[i].location.substring(1).toLowerCase(); //convert string first character in uppercase and other characters in lowercase
                    locations += '<option value="' + data[i].location + '">' + location + '</option>';
                }
                $('#' + location_control).html(locations);
            }
        });

    } else {
        //city.empty();
        //$('.find_dealers').attr('disabled', 'disabled');
        $('.initialCopy').show('fast');
        //$('#dealers_list').remove();
        $('.complete_result_div').addClass('hidden');
    }

}

//function checkLocation(location_control) {
//
//    var id = location_control.attr('id');
//    var location = $('#' + id).val();
//
//    if (location == '' || location == 'Select Location') {
//        $('.find_dealers').attr('disabled', 'disabled');
//    } else {
//        $('.find_dealers').removeAttr('disabled');
//    }
//
//}