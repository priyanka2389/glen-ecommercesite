/**
 * Created by priyanka on 9/16/14.
 */

$(document).ready(function () {

    var call_us_form = $('#call_us_form');
    call_us_form.validate();

    //triggers when the send to friend submit button is clicked, and clear all input and textarea
    $('#send_friend_button').click(function () {
        $('#call_us_form input[type="text"]').val('');
        $('#call_us_form textarea').val('');
    });


    $('.submit_call').click(function (e) {

        e.preventDefault();
        $('#order_status').html("");
        if (call_us_form.valid() == true) {

            $('#ajax_loader').removeClass('hide');
            var request_call_url = call_us_form.attr("action");

            $.post(request_call_url, call_us_form.serialize(), function (data) {
                if (data == 'true') {
                    $('#ajax_loader').addClass('hide');
                    $('#call_status').html('Your message has been sent successfully').css({'color': '#4cae4c'});
                } else {
                    $('#ajax_loader').addClass('hide');
                    $('#call_status').html('Some error occurred please try again later').css({'color': 'FF0000'});
                }

            });

        } else {
            e.preventDefault();
            return false;
        }

    });

});