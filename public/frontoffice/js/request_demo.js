/**
 * Created by anuj on 5/20/14.
 */

$(document).ready(function () {

    var request_demo_form = $('#request_demo_form');
    request_demo_form.validate();

    //triggers when the request demo button is clicked, cand clear all input and textarea
    $('.request_demo_btn').click(function () {
        $('#request_demo_form input[type="text"]').val('');
        $('#request_demo_form textarea').val('');
    });


    $('.submit_demo').click(function (e) {

        e.preventDefault();
        $('#order_status').html("");
        if (request_demo_form.valid() == true) {

            $('#ajax_loader').removeClass('hide');
            var request_demo_url = request_demo_form.attr("action");
            $.post(request_demo_url, request_demo_form.serialize(), function (data) {
                if (data == 'true') {
                    $('#ajax_loader').addClass('hide');
                    $('#order_status').html('Your request for demo has been registered successfully').css({'color': '#4cae4c'});
                } else {
                    $('#ajax_loader').addClass('hide');
                    $('#order_status').html('Some error occurred please try again later').css({'color': 'FF0000'});
                }

            });

        } else {
            e.preventDefault();
            return false;
        }

    });

});