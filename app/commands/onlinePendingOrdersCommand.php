<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class onlinePendingOrdersCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:onlinePendingOrdersCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It shows Online pending orders.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //
        $orderRepo = new EloquentOrderRepository();
        $orderData = $orderRepo->getOnlinePendingOrders();

        if (!empty($orderData)) {
            $total_orders = (count($orderData) > 1) ? count($orderData) . " orders found" : count($orderData) . " order found";

            Event::fire("onlinePendingOrders.sendMail", array($orderData));

            $this->info(json_encode($orderData));
            $this->info($total_orders);
        } else {
            $this->info('No pending orders found');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('example', InputArgument::OPTIONAL, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
