<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class toBeRefundedOrdersCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:toBeRefundedOrdersCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It shows cancelled orders where payment is going to be refunded.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //
        $orderRepo = new EloquentOrderRepository();
        $orderData = $orderRepo->getToBeRefundedOrders();

        if (!empty($orderData)) {
            $total_orders = (count($orderData) > 1) ? count($orderData) . " orders found" : count($orderData) . " order found";

            Event::fire("toBeRefundedOrders.sendMail", array($orderData));

            $this->info(json_encode($orderData));
            $this->info($total_orders);
        } else {
            $this->info('No to be refunded orders found');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('example', InputArgument::OPTIONAL, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
