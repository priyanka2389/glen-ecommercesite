<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class cartAbandonmentReportCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:cartAbandonmentReportCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It shows all users cart data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {

        $cartRepo = new EloquentCartRepository();
        $cartData = $cartRepo->getCartData();

        if (!empty($cartData)) {

            $total_orders = (count($cartData) > 1) ? count($cartData) . " cart items found" : count($cartData) . " cart item found";

            Event::fire("cartAbandonmentCommand.sendMail", array($cartData));

            $this->info(json_encode($cartData));
            $this->info($total_orders);
        } else {
            $this->info('No data added in the cart');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('example', InputArgument::OPTIONAL, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
