<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        //pivot table that contains coupon main id and category id
        Schema::create("coupon_main_categories", function ($table) {

            $table->increments('id');
            $table->integer('coupon_main_id')->unsigned();
            $table->integer('category_id')->unsigned(); //category on which the coupon is applicable
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('coupon_main_id')->references('id')->on('coupon_main')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('coupon_main_categories', function ($table) {

            $table->dropForeign('coupon_main_categories_coupon_main_id_foreign');
            $table->dropForeign('coupon_main_categories_category_id_foreign');

        });
        Schema::drop('coupon_main_categories');
    }

}
