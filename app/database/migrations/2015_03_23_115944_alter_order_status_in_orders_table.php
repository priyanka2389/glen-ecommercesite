<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderStatusInOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//

        DB::beginTransaction();

//        $table->enum('status', array('new', 'open', 'closed'))->default('new');
//        $table->enum('payment_status', array('paid', 'unpaid'))->default('unpaid');

        //change enum value in payment status and  order stus

        DB::update("ALTER TABLE orders MODIFY COLUMN status ENUM('new','open','closed','cancelled','dispatched','in-transit','delivered')");
        DB::update("ALTER TABLE orders MODIFY COLUMN payment_status  ENUM('paid','unpaid','pending','failed','refunded','to be refunded')");

        DB::commit();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        DB::beginTransaction();

        //remove pending enum value in payment_status
        DB::update("ALTER TABLE orders MODIFY COLUMN status ENUM('new','open','closed','cancelled')");
        DB::update("ALTER TABLE orders MODIFY COLUMN payment_status  ENUM('paid','unpaid','pending','failed')");

        DB::commit();
	}

}
