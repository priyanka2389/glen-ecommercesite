<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsCodAvailableInPincodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pincodes', function(Blueprint $table)
		{
			//
            $table->boolean('is_cod_available')->nullable()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pincodes', function(Blueprint $table)
		{
			//
            $table->dropColumn('is_cod_available');
		});
	}

}
