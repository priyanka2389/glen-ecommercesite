<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedByAdminFieldInOrderAndCartTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->timestamp("deleted_by_admin")->nullable();

        });
        Schema::table('cart_items', function (Blueprint $table) {
            $table->timestamp("deleted_by_admin")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('deleted_by_admin');
        });
        Schema::table('cart_items', function (Blueprint $table) {
            $table->dropColumn('deleted_by_admin');
        });
    }

}
