<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_items', function ($table) {

            $table->increments('id')->unsigned();
            $table->integer('cart_id')->unsigned();
            $table->string('item_type')->nullbale();
            $table->integer('item_id');
            $table->integer('qty');
            $table->float('price');
            $table->float('subtotal');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign("cart_id")->references('id')->on('cart');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart_items', function ($table) {
            $table->drop('cart_items_cart_id');
        });

        Schema::drop('cart_items');
    }

}
