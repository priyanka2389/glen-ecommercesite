<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDescriptionInCombosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('combos', function(Blueprint $table)
		{
			//
            DB::statement("ALTER TABLE `combos` CHANGE COLUMN `description` `description` text ;");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('combos', function(Blueprint $table)
		{
			//
		});
	}

}
