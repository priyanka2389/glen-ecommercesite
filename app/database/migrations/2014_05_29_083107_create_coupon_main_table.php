<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponMainTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        //this table contains the coupon info
        Schema::create("coupon_main", function ($table) {

            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('percentage')->nullable(); //percentage of discount
            $table->string('min_value')->nullable(); //min value required for coupon to apply
            $table->date('expiry_date')->nullable();; //expiry date of coupon
            $table->boolean('is_unique')->nullable(); //is coupon unique
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coupon_main');
    }

}
