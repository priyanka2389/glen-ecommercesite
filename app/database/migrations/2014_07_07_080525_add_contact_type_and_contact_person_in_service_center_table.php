<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContactTypeAndContactPersonInServiceCenterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('service_centers', function(Blueprint $table)
		{
            $table->string('contact_type')->nullable();
            $table->string('contact_person')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('service_centers', function(Blueprint $table)
		{
			$table->dropColumn('contact_type');
            $table->dropColumn('contact_person');
		});
	}

}
