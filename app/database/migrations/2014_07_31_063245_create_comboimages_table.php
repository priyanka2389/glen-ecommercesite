<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComboimagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('comboImages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('combo_id')->unsigned();
            $table->integer('image_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('title')->nullable();
            $table->string('caption')->nullable();
            $table->string('notes')->nullable();
            $table->boolean('is_primary')->nullable();
            $table->timestamps();

            $table->foreign('combo_id')->references('id')->on('combos')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade')->onUpdate("cascade");
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table("comboImages", function ($table) {
            $table->drop("comboImages_product_id");
            $table->drop("comboImages_image_id");
        });
        Schema::drop('comboImages');
	}

}
