<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddPaymentGatewayResponseTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pg_response', function ($table) { //pg_resonse(Payment Gateway Reponse)

            $table->increments('id');
            $table->string('amount')->nullable();
            $table->string('batch_no')->nullable();
            $table->string('command')->nullable();
            $table->string('locale')->nullable();
            $table->string('merchant_txn_ref')->nullable();
            $table->string('merchant')->nullable();
            $table->text('message')->nullable();
            $table->string('order_info')->nullable();
            $table->string('transaction_no')->nullable();
            $table->string('txn_response_code')->nullable();
            $table->string('version')->nullable();
            $table->text('additional_notes')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pg_response');
    }

}
