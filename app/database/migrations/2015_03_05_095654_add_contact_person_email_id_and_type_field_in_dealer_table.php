<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContactPersonEmailIdAndTypeFieldInDealerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dealers', function(Blueprint $table)
		{
            $table->string("email_id")->nullable();
            $table->string("contact_person")->nullable();
            $table->string("type")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dealers', function(Blueprint $table)
		{
            $table->dropColumn('email_id');
            $table->dropColumn('contact_person');
            $table->dropColumn('type');
		});
	}

}
