<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        //this table will contain only unique coupon codes for main coupons
        Schema::create("coupons", function ($table) {

            $table->increments('id');
            $table->string('code');
            $table->integer('order_id')->nullable();
            $table->integer('coupon_main_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('coupon_main_id')->nullable()->references('id')->on('coupon_main')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("coupons", function ($table) {
            $table->dropForeign('coupons_coupon_main_id_foreign');
        });
        Schema::drop('coupons');
    }

}
