<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMessageInMailRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mailRecords', function(Blueprint $table)
		{
			//
			DB::statement("ALTER TABLE `mailRecords` CHANGE COLUMN `message` `message` text;");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mailRecords', function(Blueprint $table)
		{
			//
		});
	}

}
