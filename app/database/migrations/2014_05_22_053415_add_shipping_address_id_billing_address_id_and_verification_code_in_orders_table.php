<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingAddressIdBillingAddressIdAndVerificationCodeInOrdersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        Schema::table('orders', function ($table) {

            $table->integer('shipping_address_id')->unsigned();
            $table->integer('billing_address_id')->unsigned();
            $table->string('verification_code')->nullable();

            $table->foreign('shipping_address_id')->references('id')->on('addresses')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('billing_address_id')->references('id')->on('addresses')->onDelete('cascade')->onUpdate("cascade");


        });
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("orders", function ($table) {
            $table->dropForeign("orders_shipping_address_id_foreign");
            $table->dropForeign("orders_billing_address_id_foreign");
            $table->drop("verification_code");
        });
    }

}
