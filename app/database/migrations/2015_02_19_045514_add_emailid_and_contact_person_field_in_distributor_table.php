<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailidAndContactPersonFieldInDistributorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('distributor', function(Blueprint $table)
		{
			//
			$table->string("email_id")->nullable();
			$table->string("contact_person")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('distributor', function(Blueprint $table)
		{
			//
			$table->dropColumn('email_id');
			$table->dropColumn('contact_person');
		});
	}

}
