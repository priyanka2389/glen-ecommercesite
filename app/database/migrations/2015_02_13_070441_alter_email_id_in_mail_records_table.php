<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmailIdInMailRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mailRecords', function(Blueprint $table)
		{
			//
			DB::statement("ALTER TABLE `mailRecords` CHANGE COLUMN `email_id` `user_name` VARCHAR(255);");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mailRecords', function(Blueprint $table)
		{
			//
		});
	}

}
