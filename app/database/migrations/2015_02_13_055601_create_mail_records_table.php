<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('mailRecords', function ($table) {
			$table->increments('id');
			$table->string('email_id')->nullable();
			$table->string('to')->nullable();
			$table->string('from')->nullable();
			$table->string('subject')->nullable();
			$table->string('message')->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('mailRecords');
	}

}
