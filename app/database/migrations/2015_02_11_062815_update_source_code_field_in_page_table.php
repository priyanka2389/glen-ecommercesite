<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSourceCodeFieldInPageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('page', function(Blueprint $table)
		{
			//
			DB::statement("ALTER TABLE `page` CHANGE COLUMN `source_code` `source_code` text ;");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('page', function(Blueprint $table)
		{
			//
		});
	}

}
