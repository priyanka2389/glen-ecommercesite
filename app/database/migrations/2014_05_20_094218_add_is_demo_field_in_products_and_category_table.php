<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsDemoFieldInProductsAndCategoryTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function ($table) {
            $table->boolean('is_demo')->nullable();
        });

        Schema::table('products', function ($table) {
            $table->boolean('is_demo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function ($table) {
            $table->drop('is_demo');
        });

        Schema::table('products', function ($table) {
            $table->drop('is_demo');
        });
    }

}
