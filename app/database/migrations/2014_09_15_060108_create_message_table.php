<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function(Blueprint $table)
		{
            $table->increments('id');

            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('friend_name')->nullable();
            $table->string('friend_email')->nullable();
            $table->text('message')->nullable();
            $table->string('link')->nullable();

			$table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('messages');
	}

}
