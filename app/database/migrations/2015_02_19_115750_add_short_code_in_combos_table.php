<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShortCodeInCombosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('combos', function(Blueprint $table)
		{
			//
			$table->string('code')->nullable();
			$table->string('shortcode')->unique()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('combos', function(Blueprint $table)
		{
			//
			$table->dropColumn('code');
			$table->dropColumn('shortcode');
		});
	}

}
