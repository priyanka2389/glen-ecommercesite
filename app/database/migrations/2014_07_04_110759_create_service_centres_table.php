<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceCentresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('service_centers', function ($table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email_id')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('pincode')->nullable();
            $table->string('mobile')->nullable();
            $table->string('phone')->nullable();
            $table->integer('sequence')->default(0)->nullable();
            $table->boolean("is_small_appliance")->default(true)->nullable();
            $table->boolean("is_large_appliance")->default(true)->nullable();
            $table->boolean("is_active")->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('service_centers');
	}

}
