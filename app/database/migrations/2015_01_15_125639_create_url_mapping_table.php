<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUrlMappingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('url_list', function(Blueprint $table) {
            $table->increments('id');
            $table->string('searched_url')->nullable();
            $table->string('mapped_url')->nullable();

            $table->boolean('is_mapped')->nullable()->default(0); //enable disable product availability
            $table->timestamps();
            $table->softDeletes();

        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('url_list');
	}

}
