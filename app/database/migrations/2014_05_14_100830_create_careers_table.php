<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("careers", function ($table) {

            $table->increments('id');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('dob')->nullable();
            $table->string('applying_department')->nullable();
            $table->string('educational_qualification')->nullable();
            $table->string('professional_qualification')->nullable();
            $table->string('primary_skill')->nullable();
            $table->text('career_highlights')->nullable();
            $table->string('work_exp')->nullable();
            $table->string('resume_path')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("careers");
    }

}
