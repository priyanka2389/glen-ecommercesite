<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsActiveAndSequenceColumnInDistributorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('distributor', function(Blueprint $table)
        {
            //
            $table->boolean("is_active")->default(0);
            $table->integer("sequence")->default(0)->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('distributor', function ($table) {

            $table->dropColumn('is_active');
            $table->integer("sequence")->default(0)->nullable();

        });
	}

}
