<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrackingInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trackingInfos', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('order_id')->unsigned();;
            $table->string('provider');
            $table->string('code');
            $table->string('tracking_url');
            $table->text('notes')->nullable();
			$table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('TrackingInfos');
	}

}
