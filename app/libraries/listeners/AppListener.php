<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 2/10/14
 * Time: 4:44 PM
 */


Event::listen("order.created", function ($data) {
    return false;
});

Event::fire("order.creation_failed", function ($data) {
    return false;
});

Event::listen("order.success", function ($msgdata = array()) {

    $data = $msgdata['order'];
    $data = $data + array('order_items' => $msgdata['order_items']);

    Mail::send($msgdata['email_template'], $data, function ($message) use ($msgdata) {
        $message->from($msgdata['from_emailId'], 'Glen India');
        if (isset($msgdata['cc_emailID'])) {
            $message->to($msgdata['to_emailId'])->cc($msgdata['cc_emailID'])->subject($msgdata['email_subject']);
        } else {
            $message->to($msgdata['to_emailId'])->subject($msgdata['email_subject']);
        }
    });

    return false;
});

Event::listen("order.fail", function ($data) {
    return false;
});

Event::listen("order.track", function ($msgdata = array()) {

    Mail::send($msgdata['email_template'], $msgdata, function ($message) use ($msgdata) {
        $message->from($msgdata['from_emailId'], 'Glen India');
        $message->to($msgdata['to_emailId'])->subject($msgdata['email_subject']);
    });
    return false;
});

Event::listen('user.register', function ($data) {
    return false;
});

Event::listen('user.forgot_password', function ($password, $email, $user_id) {

    $data = array(
        'password' => $password,
        'email' => $email,
        'user_id' => $user_id
    );

    Mail::send('emails.forgotpassword', $data, function ($mail) use ($data) {

        $mail->to($data['email'])->subject('Your password has been changed successfully');

    });

});
Event::listen('sendMessageToFriend.success', function ($msgdata = array()) {
    Mail::send('emails.send_message_to_friend', $msgdata, function ($message) use ($msgdata) {
        $message->from($msgdata['from_emailId'], $msgdata['from_name']);
        $message->to($msgdata['to_emailId'])->subject($msgdata['email_subject']);

    });

});
Event::listen('order.track_sendSMS', function ($smsdata = array()) {
    $sms = array();
    $sms['sender_id'] = AppUtil::getCurrentUserId();
    $sms['mobile'] = $smsdata['mobile'];

    $message = urlencode('Dear Customer,%0AProduct order no. ' . $smsdata['order_id'] . ' has been dispatched, your tracking no. is ' . $smsdata['tracking_id'] . ' & will reach you by ' . $smsdata['time'] . '.');
    $message = str_replace("%2C", ",", $message);
    $message = str_replace("%25", "%", $message);
    $sms['message'] = $message;

    SmsSend::sendMessages($sms);

});

Event::listen('order.success_sendSMS', function ($smsdata = array()) {
    SmsSend::sendMessages($smsdata);
    return false;
});


//fires the event which transfer the session cart data to database as soon as the user is logged in
Event::listen("cart.transfer", function () {

    //transfer the session cart items to database cart
    $user_id = AppUtil::getCurrentUserId();
    $items = Cart::content();
    if (sizeof($items) != 0) {
        foreach ($items as $item) {
            $custom_id = $item->id;
            $item_type_array = explode('_', $custom_id);

            $item_type = $item_type_array[0];
            $id = $item_type_array[1];
            $qty = $item->qty;

            $cartRepo = new EloquentCartRepository();

            if ($item_type == 'product') {

                $productRepo = new EloquentProductRepository();
                $product = $productRepo->getProductBasicInfo($id, null, null, null);
                if (!empty($product) && $product->offer_price != '0.00') {
                    $price = $product->offer_price;
                } else {
                    $price = $product->list_price;
                }


                $item = $cartRepo->getItem('product', $id, $user_id);

                if (isset($item) && $item->cartitems->count() != 0) { //item already exists in database just update it

                    $existing_qty = $item->cartitems[0]->qty;
                    $new_qty = $existing_qty + $qty;

                    $cart_id = $item->id;
                    $item_id = $item->cartitems[0]->item_id;
                    $cartRepo->updateCart($cart_id, $item_id, 'product', $new_qty, $price);

                } else { //create a new cart item
                    $cartRepo->addItem('product', $id, $user_id, $qty, $price);
                }
                Cart::destroy();

            } else {

                $comboRepo = new EloquentComboRepository();
                $combo = $comboRepo->getCombo($id, null);
                $price = $combo->combo_price;

                $item = $cartRepo->getItem('combo', $id, $user_id);
                $cartRepo = new EloquentCartRepository();
                if (isset($item) && $item->cartitems->count() != 0) { //item already exists in database just update it

                    $existing_qty = $item->cartitems[0]->qty;
                    $new_qty = $existing_qty + $qty;

                    $cart_id = $item->id;
                    $item_id = $item->cartitems[0]->item_id;
                    $cartRepo->updateCart($cart_id, $item_id, 'combo', $new_qty, $price);

                } else { //create a new cart item
                    $cartRepo->addItem('combo', $id, $user_id, $qty, $price);
                }
                Cart::destroy();
            }

        }
        return true;
    } else {
        return false;
    }
});

Event::listen('user.logged_in', function () {
    return false;
});


Event::listen('verification_code.send', function ($data) {
    $res = SmsSend::sendMessages($data);
    return $res;
});

Event::listen("order.success", function ($msgdata = array()) {

    $data = $msgdata['order'];
    $data = $data + array('order_items' => $msgdata['order_items']);

    Mail::send($msgdata['email_template'], $data, function ($message) use ($msgdata) {
        $message->from($msgdata['from_emailId'], 'Glen India');
        if (isset($msgdata['cc_emailID'])) {
            $message->to($msgdata['to_emailId'])->cc($msgdata['cc_emailID'])->subject($msgdata['email_subject']);
        } else {
            $message->to($msgdata['to_emailId'])->subject($msgdata['email_subject']);
        }
    });

    return false;
});

Event::listen("cartAbandonmentCommand.sendMail", function ($cartData) {

    $msgdata = array(
        'to_emailId' => array('cr5@glenindia.com','enquiry@glenindia.com'),
        'cc_emailID' => 'anurag@aldaindia.com',
        'bcc_emailID' => array('amulchandani@greenapplesolutions.com','ptailor@greenapplesolutions.com'),
        'from_emailId' => 'sales@glenindia.com',
        'email_subject' => 'Cart Abandonment Data (' . date('d-m-y H:i:s') . ')',
        'email_template' => 'emails.cronJob.cart_abandon_data_template',
        'cartData' => $cartData,
        'page_url' => 'http://glenindia.com//dashboard/reports/cart-report'
    );

    Mail::send($msgdata['email_template'], $msgdata, function ($message) use ($msgdata) {
        $message->from($msgdata['from_emailId'], 'Glen India');
        if (isset($msgdata['cc_emailID'])) {
            $message->to($msgdata['to_emailId'])->cc($msgdata['cc_emailID'])->bcc($msgdata['bcc_emailID'])->subject($msgdata['email_subject']);
        } else {
            $message->to($msgdata['to_emailId'])->subject($msgdata['email_subject']);
        }
    });

    return false;
});

Event::listen("onlinePendingOrders.sendMail", function ($orderData) {

    $msgdata = array(
        'to_emailId' => array('cr5@glenindia.com','enquiry@glenindia.com'),
        'cc_emailID' => 'anurag@aldaindia.com',
        'bcc_emailID' => array('amulchandani@greenapplesolutions.com','ptailor@greenapplesolutions.com'),
        'from_emailId' => 'sales@glenindia.com',
        'email_subject' => 'Online Pending Order List (' . date('d-m-y H:i:s') . ')',
        'email_template' => 'emails.cronJob.online_pending_orders_template',
        'orderData' => $orderData,
        'page_url' => 'http://glenindia.com/dashboard/order'
    );

    Mail::send($msgdata['email_template'], $msgdata, function ($message) use ($msgdata) {
        $message->from($msgdata['from_emailId'], 'Glen India');
        if (isset($msgdata['cc_emailID'])) {
            $message->to($msgdata['to_emailId'])->cc($msgdata['cc_emailID'])->bcc($msgdata['bcc_emailID'])->subject($msgdata['email_subject']);
        } else {
            $message->to($msgdata['to_emailId'])->subject($msgdata['email_subject']);
        }
    });

    return false;
});

Event::listen("toBeRefundedOrders.sendMail", function ($orderData) {

    $msgdata = array(
   //     'to_emailId' => array('ptailor@greenapplesolutions.com'),
        'to_emailId' => array('cr5@glenindia.com','enquiry@glenindia.com'),
      //  'cc_emailID' => 'amulchandani@greenapplesolutions.com',
        'cc_emailID' => 'anurag@aldaindia.com',
        'bcc_emailID' => array('amulchandani@greenapplesolutions.com','ptailor@greenapplesolutions.com'),
        'from_emailId' => 'sales@glenindia.com',
        'email_subject' => 'To Be Refunded Order List (' . date('d-m-y H:i:s') . ')',
        'email_template' => 'emails.cronJob.to_be_refunded_orders_template',
        'orderData' => $orderData,
        'page_url' => 'http://glenindia.com/dashboard/order'
    );

    Mail::send($msgdata['email_template'], $msgdata, function ($message) use ($msgdata) {
        $message->from($msgdata['from_emailId'], 'Glen India');
        $message->to($msgdata['to_emailId'])->cc($msgdata['cc_emailID'])->subject($msgdata['email_subject']);
    });

    return false;
});

//Event::listen("feedback.success", function ($orderData) {
//
//    $msgdata = array(
//        'to_emailId' => 'ptailor@greenapplesolutions.com',
//        'cc_emailID' => 'amulchandani@greenapplesolutions.com',
//        'from_emailId' => 'sales@glenindia.com',
//        'email_subject' => 'Feedback',
//        'email_template' => 'emails.cronJob.online_pending_orders_template',
//        'orderData' => $orderData,
//    );
//
//    Mail::send($msgdata['email_template'], $msgdata, function ($message) use ($msgdata) {
//        $message->from($msgdata['from_emailId'], 'Glen India');
//        if (isset($msgdata['cc_emailID'])) {
//            $message->to($msgdata['to_emailId'])->cc($msgdata['cc_emailID'])->subject($msgdata['email_subject']);
//        } else {
//            $message->to($msgdata['to_emailId'])->subject($msgdata['email_subject']);
//        }
//    });
//
//    return false;
//});
