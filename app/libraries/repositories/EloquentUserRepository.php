<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/13/14
 * Time: 1:15 PM
 */
class EloquentUserRepository implements iUserRepository
{
    /**
     * @param string $first_name
     * @param string $last_name
     * @param string $email
     * @param $password
     * @param bool $newsletters
     * @param bool $special_offers
     * @throws Exception
     * @return bool
     */
    public function createUser($first_name, $last_name, $email, $mobile, $password, $newsletters, $special_offers)
    {

        try {

            $user = new User();
            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->email = $email;
            $user->mobile = $mobile;
            $user->password = $password;
            $user->newsletters = $newsletters;
            $user->special_offers = $special_offers;
            $user->save();
            return $user;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }


    }

    /** add user address as well as updates users first_name,last_name,landline and mobile
     * @param int $user_id
     * @param string $line1 address line 1
     * @param string $line2 address line 2
     * @param string $state
     * @param string $city
     * @param string $landmark
     * @param int $pincode
     * @param string $first_name
     * @param string $last_name
     * @param int $landline
     * @param int $mobile
     * @param $type
     * @throws Exception
     * @return address
     */
    public function addUserAddress($user_id, $line1, $line2, $state, $city, $landmark, $pincode, $first_name,
                                   $last_name, $landline, $mobile, $type)
    {

        try {

            DB::beginTransaction();

            $address = new Address();
            $address->line1 = $line1;
            $address->line2 = $line2;
            $address->state = $state;
            $address->city = $city;
            $address->landmark = $landmark;
            $address->pincode = $pincode;
            $address->user_id = $user_id;
            $address->type = $type;
            $address->first_name = $first_name;
            $address->last_name = $last_name;
            $address->phone_no = $landline;
            $address->mobile = $mobile;
            $address->save();

//            $user = User::find($user_id);
//            $user->first_name = $first_name;
//            $user->last_name = $last_name;
//            $user->landline = $landline;
//            $user->mobile = $mobile;
//            $user->save();

            DB::commit();

            return $address;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function updateUserAddress($address_id, $user_id, $line1, $line2, $state, $city, $landmark, $pincode, $first_name,
                                      $last_name, $landline, $mobile, $type)
    {

        try {

            DB::beginTransaction();


            $address = Address::find($address_id);
            $address->line1 = $line1;
            $address->line2 = $line2;
            $address->state = $state;
            $address->city = $city;
            $address->landmark = $landmark;
            $address->pincode = $pincode;
            $address->user_id = $user_id;
            $address->type = $type;
            $address->first_name = $first_name;
            $address->last_name = $last_name;
            $address->phone_no = $landline;
            $address->mobile = $mobile;
            $address->save();

            DB::commit();

            return $address;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }


    public function deleteUserAddress($address_id)
    {
        try {

            $order = Order::where('shipping_address_id', '=', $address_id)->orWhere('billing_address_id', '=', $address_id)->first();
            if (isset($order)) {
                return false;
            } else {
                return Address::find($address_id)->delete();
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getUserAddresses($user_id, $type)
    {
        $query = Address::query();
        if (DbUtil::checkDbNotNullValue($user_id)) {
            $query->where('user_id', '=', $user_id);
        } else if (DbUtil::checkDbNullValue($user_id)) {
            $query->whereNull('user_id');
        }
        if (DbUtil::checkDbNotNullValue($type)) {
            $query->where('type', '=', $type);
        } else if (DbUtil::checkDbNullValue($type)) {
            $query->whereNull('type');
        }
        return $query->orderBy('created_at')->get();
    }

    public function getAddress($address_id)
    {
        $query = Address::query();
        if (DbUtil::checkDbNotNullValue($address_id)) {
            $query->where('id', '=', $address_id);
        } else if (DbUtil::checkDbNullValue($address_id)) {
            $query->whereNull('id');
        }

        return $query->first();
    }

    public function getUserAddress($user_id)
    {
        $result = Address::where('user_id', '=', $user_id)->get();
        return $result;
    }

    public function getUsers($newletters, $special_offers, $from_date, $to_date,$paginate)
    {
        try {
            $query = User::query();

            if (!is_null($newletters)) {
                $query->where('newsletters', '=', $newletters);
            }

            if (!is_null($special_offers)) {
                $query->where('special_offers', '=', $special_offers);
            }

            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
            }

            if (!is_null($paginate)) {
                return $query->orderBy('created_at', 'desc')->paginate($paginate);
            }
            return $query->orderBy('created_at', 'desc')->get();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /**
     * @param int $id user_id
     * @param string $email
     * @param int $phone
     * @return \Illuminate\Database\Eloquent\Model|null|static
     * @throws Exception
     */
    public function getUser($id, $email, $phone)
    {
        try {

            $query = User::with('addresses', 'orders');
            if (DbUtil::checkDbNotNullValue($id)) {
                $query->where('id', '=', $id);
            } elseif (DbUtil::checkDbNullValue($id)) {
                $query->whereNull('id');
            }
            if (DbUtil::checkDbNotNullValue($email)) {
                $query->where('email', '=', $email);
            } elseif (DbUtil::checkDbNullValue($email)) {
                $query->whereNull('email');
            }
            if (DbUtil::checkDbNotNullValue($phone)) {
                $query->where('phone', '=', $phone);
            } elseif (DbUtil::checkDbNullValue($phone)) {
                $query->whereNull('phone');
            }

            return $query->first();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /**
     * @param $id
     * @param $first_name
     * @param $last_name
     * @param $pincode
     * @param $address_line1
     * @param $address_line2
     * @param $landmark
     * @param $city
     * @param $state
     * @param $landline
     * @param $mobile
     * @throws Exception
     */
    public function updateUser($id, $first_name, $last_name, $pincode, $address_line1, $address_line2, $landmark, $city, $state, $landline, $mobile)
    {
        try {

            DB::beginTransaction();

            $user = User::find($id);
            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->landline = $landline;
            $user->mobile = $mobile;
            $user->save();

            $address = Address::where('user_id', '=', $id)->first();
            $address->pincode = $pincode;
            $address->line1 = $address_line1;
            $address->line2 = $address_line2;
            $address->landmark = $landmark;
            $address->city = $city;
            $address->state = $state;
            $address->save();

            DB::commit();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function updateUserMobile($id, $mobile)
    {
        try {

            DB::beginTransaction();

            $user = User::find($id);
            $user->mobile = $mobile;
            $user->save();

            DB::commit();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    /**
     * @param int $id
     * @param string $email
     * @param string $password hashed string
     * @return bool
     * @throws Exception
     */
    public function changePassword($id, $email, $password)
    {
        try {
            $query = User::query();
            if (DbUtil::checkDbNotNullValue($id)) {
                $query->where('id', '=', $id);
            } elseif (DbUtil::checkDbNullValue($id)) {
                $query->whereNull('id');
            }
            if (DbUtil::checkDbNotNullValue($email)) {
                $query->where('email', '=', $email);
            } elseif (DbUtil::checkDbNullValue($email)) {
                $query->whereNull('email');
            }

            $user = $query->first();

            if ($user->count() != 0) {
                $user->password = $password;
                $user->save();
                return true;

            } else {
                return false;
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function forgotPassword($email, $password)
    {
        $user = User::where('email', '=', $email)->first();
        if ($user->count() != 0) {
            $user->password = $password;
            $user->save();
            return true;
        } else {
            return false;
        }
    }

    public function getUserRole($id)
    {
        $user = User::with("roles")->first();
        return $user;
    }

    public function getStates()
    {
        try {
            $location_data = Pincode::query()->groupBy('state')->get();
            return $location_data;
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getCitiesByState($state)
    {
        try {
            $query = Pincode::where('state', '=', $state)->groupBy('city')->get();

            return $query;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getPincodesByCity($city)
    {
        try {
//            ->groupBy('postal_code')
            $query = Pincode::where('city', '=', $city)->get();
            return $query;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    public function getCityAndStateByPincode($pincode)
    {
        try {

            $location_data = Pincode::where('postal_code', '=', $pincode)->first();
            return $location_data;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getUpdateUserAddressType($user_id, $address_id, $type)
    {
        try {

            $address = Address::find($address_id)->where('user_id', '=', $user_id)->first();

            $address->type = $type;
            $address->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


} 