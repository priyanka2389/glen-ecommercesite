<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/15/14
 * Time: 10:33 AM
 */
class EloquentServicesEnquiryRepository implements iServicesEnquiryRepository
{


    public function createServiceEnquiry($name, $email, $phone, $address, $product, $bill_no, $shop_name, $bill_date,
                                         $message)
    {
        try {
            $serviceEnquiry = new ServicesEnquiry();
            $serviceEnquiry->name = $name;
            $serviceEnquiry->email = $email;
            $serviceEnquiry->phone = $phone;
            $serviceEnquiry->address = $address;
            $serviceEnquiry->product = $product;
            $serviceEnquiry->bill_no = $bill_no;
            $serviceEnquiry->shop_name = $shop_name;
            $serviceEnquiry->bill_date = $bill_date;
            $serviceEnquiry->message = $message;
            $serviceEnquiry->save();
            return $serviceEnquiry;
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getServiceEnquiries($from_date, $to_date, $paginate)
    {
        try {

            $query = ServicesEnquiry::query();
            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
            }

            if (!is_null($paginate)) {
                return $query->orderBy('created_at')->get();
            }

            return $query->orderBy('created_at')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getServiceEnquiry($id)
    {
        try {

            return ServicesEnquiry::find($id);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function deleteServiceEnquiry($id)
    {
        try {

            ServicesEnquiry::find($id)->delete();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

} 