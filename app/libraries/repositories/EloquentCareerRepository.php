<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/14/14
 * Time: 3:48 PM
 */
class EloquentCareerRepository implements iCareerRepository
{

    public function addCareer($name, $email, $phone, $address, $dob, $applying_department, $educational_qualification,
                              $professional_qualification, $primary_skill, $career_highlights, $work_exp, $resume_path)
    {

        try {

            $career = new Career();
            $career->name = $name;
            $career->email = $email;
            $career->phone = $phone;
            $career->address = $address;
            $career->dob = $dob;
            $career->applying_department = $applying_department;
            $career->educational_qualification = $educational_qualification;
            $career->professional_qualification = $professional_qualification;
            $career->primary_skill = $primary_skill;
            $career->career_highlights = $career_highlights;
            $career->work_exp = $work_exp;
            $career->resume_path = $resume_path;
            $career->save();
            return $career;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getCareers($from_date, $to_date, $paginate)
    {

        try {

            $query = Career::query();
            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
            }

            if (!is_null($paginate)) {

                return $query->orderBy('created_at')->paginate($paginate);
            }

            return $query->orderBy('created_at')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getCareer($id)
    {
        try {

            return Career::find($id);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function deleteCareer($id)
    {
        try {
            Career::find($id)->delete();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

} 