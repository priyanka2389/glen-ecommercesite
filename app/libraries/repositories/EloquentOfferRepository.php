<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 4:39 PM
 */
class EloquentOfferRepository implements iOfferRepository
{

    public function create($name, $email, $phone, $city, $message,$source)
    {
        try {
            $offer = new Offer();
            $offer->name = $name;
            $offer->email = $email;
            $offer->mobile = $phone;
            $offer->city = $city;
            $offer->message = $message;
            $offer->source = $source;
            $offer->save();

            return $offer;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getEnquiries($from_date, $to_date, $paginate)
    {
        try {

            $query = Offer::query();
            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
            }

            if (!is_null($paginate)) {
                return $query->orderBy('created_at', 'DESC')->paginate($paginate);
            }

            $res = $query->orderBy('created_at', 'DESC')->get();
            return $res;
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getEnquiry($id)
    {
        try {

            return Offer::find($id);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function deleteEnquiry($id)
    {
        try {

            return Offer::find($id)->delete();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }
} 