<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 5/20/14
 * Time: 4:39 PM
 */
class EloquentCallRepository implements iCallRepository
{

    public function createCall($name, $mobile,$email)
    {
        try {
            $call = new call();
            $call->name = $name;
            $call->mobile = $mobile;
            $call->email = $email;
            $call->save();

            return $call;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getCalls($from_date, $to_date, $paginate)
    {
        try {

            $query = Call::query();

            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
            }

            if (!is_null($paginate)) {
                return $query->orderBy('created_at', 'DESC')->paginate($paginate);
            }

            $res = $query->orderBy('created_at', 'DESC')->get();
            return $res;
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }
//
//    public function getDemo($id)
//    {
//        try {
//
//            return Demo::find($id);
//
//        } catch (Exception $ex) {
//            Log::error($ex);
//            throw $ex;
//        }
//    }
//
    public function deleteCall($id)
    {
        try {

            return Call::find($id)->delete();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }
} 