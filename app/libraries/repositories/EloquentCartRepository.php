<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/14/14
 * Time: 1:06 PM
 */
class EloquentCartRepository implements iCartRepository
{
    /**
     * @param string $item_type adds item is product or combo
     * @param int $item_id product_id or combo_id
     * @param int $user_id
     * @param int $qty
     * @param int|float $price
     * @throws Exception
     * @internal param float|int $subtotal
     * @return DbCart
     */
    public function addItem($item_type, $item_id, $user_id, $qty, $price)
    {

        try {

            DB::beginTransaction();

            //check whether user has any previous cart or not
            $cart_data = DbCart::where('user_id', '=', $user_id)->get();

            if (isset($cart_data) && $cart_data->count() != 0) { //if the user already has previous cart get cart id
                $cart_id = $cart_data[0]->id;
            } else { //else create a new cart
                $cart = new DbCart();
                $cart->user_id = $user_id;
                $cart->save();
                $cart_id = $cart->id;
            }

            $cart_item = new CartItem();
            $cart_item->cart_id = $cart_id;
            $cart_item->item_type = $item_type;
            $cart_item->item_id = $item_id;
            $cart_item->qty = $qty;
            $cart_item->price = $price;
            $cart_item->subtotal = $qty * $price;
            $cart_item->save();

            DB::commit();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /** retrieves all cart content by user_id
     * @param int $user_id
     * @throws Exception
     * @internal param $cart_id
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function getCartContents($user_id)
    {
        try {

            $query = DbCart::with('cartitems');
            if (DbUtil::checkDbNotNullValue($user_id)) {
                $query->where('user_id', '=', $user_id);
            } else if (DbUtil::checkDbNullValue($user_id)) {
                $query->whereNull('user_id');
            }

            $result = $query->get();
            return $result;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }


    public function getItem($item_type, $item_id, $user_id)
    {
        try {

            $item = DbCart::with(array('cartitems' => function ($query) use ($item_type, $item_id) {
                $query->where('cart_items.item_type', '=', $item_type)->where('cart_items.item_id', '=', $item_id);
            }))->where('user_id', '=', $user_id)->first();

            return $item;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** updates the cart item
     * @param int $cart_id
     * @param int $item_id
     * @param string $item_type product|combo
     * @param int $qty
     * @param int|float $price
     * @throws Exception
     */
    public function updateCart($cart_id, $item_id, $item_type, $qty, $price)
    {
        try {

            DB::beginTransaction();
            $cart_item = CartItem::where('cart_id', '=', $cart_id)
                ->where('item_type', '=', $item_type)->where('item_id', '=', $item_id)->first();
            $cart_item->qty = $qty;
            $cart_item->subtotal = $qty * $price;
            $cart_item->save();
            DB::commit();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    public function removeItem($item_type, $item_id, $user_id)
    {

        try {

            $cart_id = $this->getCartIdByUserId($user_id);

            $res = CartItem::where("cart_id", '=', $cart_id)
                ->where("item_type", '=', $item_type)->where("item_id", '=', $item_id)->delete();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function updateItemByCartId($cart_item_id, $cart_id, $is_deleted_by_admin)
    {

        try {
            if ($is_deleted_by_admin == 0) {
                $deleted_by_admin = null;
            } else {
                $deleted_by_admin = DB::raw('CURRENT_TIMESTAMP');
            }

            $cart_item = CartItem::withTrashed()->where("cart_id", '=', $cart_id)->where("id", '=', $cart_item_id)->first();
            $cart_item->deleted_by_admin = $deleted_by_admin;
            $cart_item->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /** get the total cart price for corresponding user
     * @param int $user_id
     * @return mixed
     * @throws Exception
     */
    public function getTotalPrice($user_id)
    {
        try {

            return DbCart::join('cart_items', 'cart.id', '=', 'cart_items.cart_id')
                ->where('user_id', '=', $user_id)->where('cart_items.deleted_at', '=', null)->sum('subtotal');

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getTotalItems($user_id)
    {
        try {
            $cart_id = $this->getCartIdByUserId($user_id);
            return CartItem::where('cart_id', '=', $cart_id)->count();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getCartIdByUserId($user_id)
    {
        try {

            $cart = DbCart::where("user_id", '=', $user_id)->first();
            return isset($cart) ? $cart->id : null;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    /** destroy the cart by cart_id
     * @param $cart_id
     * @throws Exception
     */
    public function destroyCart($cart_id)
    {
        try {

            DbCart::find($cart_id)->delete();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getAllUsersCartContent($from_date, $to_date, $paginate, $is_deleted_by_admin)
    {
        try {

            $query = DbCart::with(array('cartitems' => function ($cQuery) use ($from_date, $to_date, $is_deleted_by_admin) {

                $cQuery->orderBy('created_at');

                if (!is_null($from_date) && !is_null($to_date)) {
                    $cQuery->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
                }

                $cQuery->where('deleted_by_admin', '=', null);

            }));

            $result = $query->get();
            return $result;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getAllUsersCartTrashContent($from_date, $to_date, $paginate, $is_deleted_by_admin)
    {
        try {


            $query = DbCart::withTrashed()->with(array('cartitems' => function ($cQuery) use ($from_date, $to_date, $is_deleted_by_admin) {

                if ($is_deleted_by_admin == 0) {
                    $filter_by = 'deleted_at';
                    $cQuery->where('deleted_by_admin', '=', null);
                } else {
                    $filter_by = 'deleted_by_admin';
                }
                $cQuery->orderBy($filter_by)->withTrashed();

                if (!is_null($from_date) && !is_null($to_date)) {
                    $cQuery->where($filter_by, '>=', $from_date)->where($filter_by, '<=', $to_date);
                }

            }));

            $result = $query->get();
            return $result;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getCart($cart_id, $is_trashed)
    {
        if ($is_trashed == 1) {
            $cart = DbCart::with(array('cartitems' => function ($cQuery) {
                $cQuery->withTrashed();
            }))->where("id", '=', $cart_id)->withTrashed()->first();
        } else {
            $cart = DbCart::with('cartitems')->where("id", '=', $cart_id)->first();
        }

        return isset($cart) ? $cart : null;
    }

    public function getRestoreCart($cart_id)
    {
        try {

            DB::beginTransaction();
            $cart = DbCart::withTrashed()->where('id', '=', $cart_id)->first();
            $cart->deleted_at = null;
            $cart->save();
            DB::commit();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getRestoreCartItem($cart_item_id, $cart_id)
    {
        try {

            DB::beginTransaction();
            $cart_item = CartItem::withTrashed()->where('cart_id', '=', $cart_id)->where('id', '=', $cart_item_id)->first();
            $cart_item->deleted_at = null;
            $cart_item->save();
            DB::commit();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getCartData()
    {
        // . '7 days'

        $to_date = date("Y-m-d H:i:s");
        $from_date = date('Y-m-d H:i:s', strtotime($to_date . '-15 minutes'));
        $cartRepo = new EloquentCartRepository();

//        echo $from_date;echo $to_date;

        $result = $cartRepo->getAllUsersCartContent($from_date, $to_date, Constants::DASHBOARD_REPORT_PAGE_COUNT, 0);

        $cart = array();
        foreach ($result as $row) {
            if ($row->cartitems->count() > 0) {
                $cart_id = $row->id;
                $user_id = $row->user_id;
                $userRepo = new EloquentUserRepository();
                $user = $userRepo->getUser($user_id, null, null);

                $user_name = $user->first_name . " " . $user->last_name;
                $email = $user->email;
                $mobile = $user->mobile;

                $cart_items = array();
                foreach ($row->cartitems as $single_item) {
                    $cart_item_id = $single_item->id;
                    $item_type = $single_item->item_type;
                    $item_id = $single_item->item_id;

                    if ($item_type == 'product') {
                        $productRepo = new EloquentProductRepository();
                        $product = $productRepo->getProductBasicInfo($item_id, null, null, null);
                        $name = $product->name;
                        $category_id = $product->category_id;
                        $categoryRepo = new EloquentCategoryRepository();
                        $category = $categoryRepo->getCategory($category_id, null, null);
                        $category_name = $category->name;
                        //     $price = $product->price;
                    } else {
                        $comboRepo = new EloquentComboRepository();
                        $combo = $comboRepo->getCombo($item_id, null);
                        if (isset($combo)) {
                            $name = ($combo->name) ? $combo->name : '';
                            $category_name = "Combo";
                        }
                    }
                    $cartArray[] = array(
                        'cart_id' => $cart_id,
                        'item_id' => $item_id,
                        'cart_item_id' => $cart_item_id,
                        'item_type' => $item_type,
                        'user_id' => $user_id,
                        'user_name' => $user_name,
                        'email' => $email,
                        'mobile' => $mobile,
                        'name' => $name . " (" . $category_name . ")",
                        'created_at' => $single_item->created_at,
                    );
                }

            } else {
                //  $cart = null;
            }
        }
        if (isset($cartArray)) {
            $cartData = array_values(array_sort($cartArray, function ($value) {
                return $value['created_at'];
            }));
            return $cartData;
        }
    }



} 