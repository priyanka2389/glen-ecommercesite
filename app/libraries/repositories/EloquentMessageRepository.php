<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 5/20/14
 * Time: 4:39 PM
 */
class EloquentMessageRepository implements iMessageRepository
{

    public function createMessage($name, $email, $friend_name, $friend_email, $msg, $product_link)
    {
        try {
            $message = new Message();
            $message->name = $name;
            $message->email = $email;
            $message->friend_name = $friend_name;
            $message->Friend_email = $friend_email;
            $message->message = $msg;
            $message->link = $product_link;
            $message->save();

            return $message;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getMessages($from_date, $to_date, $paginate)
    {
        try {

            $query = Message::query();

            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
            }

            if (!is_null($paginate)) {
                return $query->orderBy('created_at', 'DESC')->paginate($paginate);
            }

            $res = $query->orderBy('created_at', 'DESC')->get();
            return $res;
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }
//
//    public function getDemo($id)
//    {
//        try {
//
//            return Demo::find($id);
//
//        } catch (Exception $ex) {
//            Log::error($ex);
//            throw $ex;
//        }
//    }
//
    public function deleteMessage($id)
    {
        try {

            return Message::find($id)->delete();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }
} 