<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/8/14
 * Time: 1:03 PM
 */
class EloquentServicesCentersRepository implements iServicesCentersRepository
{

    /**
     * @param $name
     * @param $address1
     * @param $address2
     * @param $city
     * @param $state
     * @param $pincode
     * @param $mobile
     * @param $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @throws Exception
     * @return Dealer
     */
    public function createServiceCenter($name,$email, $address1, $address2, $city, $state, $pincode, $mobile, $phone, $is_small_appliance, $is_large_appliance, $is_active,$sequence,$contact_type,$contact_person)
    {
        try {

            $servicecenter = new ServiceCenters();

            if ($sequence == 'top') {
                ServiceCenters::where('sequence', '>', 1)->increment("sequence");
                $sequence = 1;

                $servicecenter->sequence = $sequence;
                $servicecenter->save();
            } else if ($sequence == 'bottom') {
                $max_sequence = Product::max('sequence');
                $sequence = $max_sequence + 1;

                $servicecenter->sequence = $sequence;
                $servicecenter->save();
            } else {
                $this->addOrUpdateSequence(null, $sequence,null);
                $sequence = $sequence + 1;

                $servicecenter = new ServiceCenters();
                $servicecenter->sequence = $sequence;
                $servicecenter->save();

            }

//            ServiceCenters::where('sequence', '>=', $sequence)->increment('sequence', 1); //updates the sequence

            $servicecenter->name = $name;
            $servicecenter->email_id = $email;
            $servicecenter->address1 = $address1;
            $servicecenter->address2 = $address2;
            $servicecenter->city = $city;
            $servicecenter->state = $state;
            $servicecenter->pincode = $pincode;
            $servicecenter->mobile = $mobile;
            $servicecenter->phone = $phone;
            $servicecenter->is_small_appliance = $is_small_appliance;
            $servicecenter->is_large_appliance = $is_large_appliance;
            $servicecenter->is_active = $is_active;
            $servicecenter->contact_type = $contact_type;
            $servicecenter->contact_person = $contact_person;
            $servicecenter->save();

            return $servicecenter;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /**
     * @param $city
     * @param $state
     * @param $paginate
     * @throws Exception
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getServiceCenters($city, $state, $paginate)
    {
        try {

            $query = ServiceCenters::query();
            if (DbUtil::checkDbNotNullValue($city)) {
                $query->where('city', '=', $city);
            } elseif (DbUtil::checkDbNullValue($city)) {
                $query->whereNull('city');
            }

            if (DbUtil::checkDbNotNullValue($state)) {
                $query->where('state', '=', $state);
            } elseif (DbUtil::checkDbNullValue($state)) {
                $query->whereNull('state');
            }

            if (!is_null($paginate)) {
                return $query->orderBy('sequence')->paginate($paginate);
            }
            return $query->orderBy('sequence')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     * @throws Exception
     */
    public function getServiceCenter($id)
    {

        try {

            return ServiceCenters::where('id', '=', $id)->first();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /**
     * @param $id
     * @param $name
     * @param $address1
     * @param $address2
     * @param $city
     * @param $state
     * @param $pincode
     * @param $mobile
     * @param $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @throws Exception
     */
    public function updateServiceCenter($id,$email, $name, $address1, $address2, $city, $state, $pincode, $mobile, $phone, $is_small_appliance, $is_large_appliance, $is_active,$sequence,$contact_type,$contact_person)
    {
        try {

            $servicecenter = ServiceCenters::find($id);

            if ($sequence == 'top') {
                ServiceCenters::where('sequence', '>=', 1)->increment("sequence");
                $sequence = 1;

                $servicecenter->sequence = $sequence;
                $servicecenter->save();
            } else if ($sequence == 'bottom') {
                $max_sequence = Dealer::max('sequence');
                $sequence = $max_sequence;

                ServiceCenters::where('sequence', '>', 1)->decrement("sequence");

                $servicecenter->sequence = $sequence;
                $servicecenter->save();
            } else {
                $current_sequence = $servicecenter->sequence;
                $this->addOrUpdateSequence($current_sequence, (int)$sequence,$id);
//                $sequence = $sequence + 1;
            }

            $servicecenter->name = $name;
            $servicecenter->address1 = $address1;
            $servicecenter->address2 = $address2;
            $servicecenter->city = $city;
            $servicecenter->state = $state;
            $servicecenter->pincode = $pincode;
            $servicecenter->mobile = $mobile;
            $servicecenter->phone = $phone;
            $servicecenter->email_id = $email;
            $servicecenter->is_small_appliance = $is_small_appliance;
            $servicecenter->is_large_appliance = $is_large_appliance;
            $servicecenter->is_active = $is_active;
            $servicecenter->contact_type = $contact_type;
            $servicecenter->contact_person = $contact_person;
            $servicecenter->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** updates the appliance type
     * @param int $id dealer_id
     * @param string $type small|large
     * @param bool $status 0|1
     * @throws Exception
     */
    public function updateApplianceType($id, $type, $status)
    {
        try {

            $servicecenter = ServiceCenters::find($id);
            if ($type == "small") {
                $servicecenter->is_small_appliance = $status;
            } else {
                $servicecenter->is_large_appliance = $status;
            }
            $servicecenter->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** accepts dealer_id and activate or deactivate the corresponding dealer
     * @param int $id dealer_id
     * @param bool $status 0|1
     * @throws Exception
     */
    public function activateOrDeactivate($id, $status)
    {
        try {

            $servicecenter = ServiceCenters::find($id);
            $servicecenter->is_active = $status;
            $servicecenter->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function  addOrUpdateSequence($current_sequence, $after_sequence,$servicecenter_id)
    {
         if (is_null($current_sequence)) { //new product is added

             ServiceCenters::where('sequence', '>', $after_sequence)->increment('sequence');

//            $product = Product::where('sequence', '=', $after_sequence)->first();
//            $product->sequence = $after_sequence + 1;
//            $product->save();

        }
        else {

            if ($current_sequence > $after_sequence) {
                if (DbUtil::checkDbNotNullValue($servicecenter_id)) {
                    $servicecenter = ServiceCenters::where('sequence', '=', $current_sequence)->where('id',$servicecenter_id)->first();
                } elseif (DbUtil::checkDbNullValue($servicecenter_id)) {
                    $servicecenter = ServiceCenters::where('sequence', '=', $current_sequence)->whereNull('id')->first();
                }
//                $dealer = Dealer::where('sequence', '=', $current_sequence)->first();
                //Product::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->get();
                ServiceCenters::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->increment('sequence', 1);
                $servicecenter->sequence = $after_sequence + 1;
                $servicecenter->save();

            } else {
                if (DbUtil::checkDbNotNullValue($servicecenter_id)) {
                    $servicecenter = ServiceCenters::where('sequence', '=', $current_sequence)->where('id',$servicecenter_id)->first();
                } elseif (DbUtil::checkDbNullValue($servicecenter_id)) {
                    $servicecenter = ServiceCenters::where('sequence', '=', $current_sequence)->whereNull('id')->first();
                }
//                $dealer = Dealer::where('sequence', '=', $current_sequence)->first();
                ServiceCenters::where('sequence', '>', $current_sequence)->where('sequence', '<=', $after_sequence)->decrement('sequence', 1);
                $servicecenter->sequence = $after_sequence;
                $servicecenter->save();
            }
        }


    }

    public function deleteServiceCenters($id = array())
    {
        ServiceCenters::whereIn("id", $id)->delete();
    }

    public function getUniqueStates()
    {
        try {

            return ServiceCenters::groupBy('state')->whereNotNull('state')->where('state', '!=', '')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getCitiesForState($state)
    {
        try {
            return ServiceCenters::where("state", '=', $state)->groupBy("city")->get();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getSubcategoriesForCategories($category){
        try {
            return Category::where("parent_category_id", '=', $category)->groupBy("name")->get();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getData($city, $state, $is_small_appliance, $is_large_appliance)
    {

        try {

            $query = ServiceCenters::query();
            if (!empty($city)) {
                $query->where("city", '=', $city);
            }
            if (!empty($state)) {
                $query->where("state", '=', $state);
            }
            if ($is_large_appliance == true && $is_small_appliance == true || $is_large_appliance == false && $is_small_appliance == false) {
                $query->where("is_small_appliance", '=', $is_large_appliance)
                    ->where("is_large_appliance", '=', $is_small_appliance);

            } elseif ($is_large_appliance == true && $is_small_appliance == false) {
                $query->where("is_large_appliance", '=', true);

            } elseif ($is_large_appliance == false && $is_small_appliance == true) {
                $query->where("is_small_appliance", '=', true);
            }
            return $query->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }


    }

} 