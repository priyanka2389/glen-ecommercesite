<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 02/02/15
 * Time: 1:01 PM
 */
interface iDistributorsRepository
{
    public function createDistributor($shop_name,$email, $address1, $address2, $city, $state, $pincode, $mobile,$phone,$contact_person, $small_appliance, $large_appliance, $active,$sequence);

    public function getDistributors($city, $state,$is_active, $paginate);

    public function getDistributor($id);

    public function updateDistributor($id, $shop_name,$email, $address1, $address2, $city, $state, $pincode, $mobile,$phone,$contact_person, $small_appliance, $large_appliance, $active,$sequence);

    public function updateApplianceType($id, $type, $status);

    public function activateOrDeactivate($id, $status);

    public function deleteDistributors($id = array());

    public function getUniqueStates();

    public function getCitiesForState($state);

    public function getSubcategoriesForCategories($category);

    public function getData($city, $state, $is_small_appliance, $is_large_appliance);
} 