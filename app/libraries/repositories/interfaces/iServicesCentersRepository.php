<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/8/14
 * Time: 1:01 PM
 */
interface iServicesCentersRepository
{
    public function createServiceCenter($name,$email, $address1, $address2, $city, $state, $pincode, $mobile, $phone, $is_small_appliance, $is_large_appliance, $is_active,$sequence,$contact_type,$contact_person);

    public function getServiceCenters($city, $state, $paginate);

    public function getServiceCenter($id);

    public function updateServiceCenter($id, $name,$email, $address1, $address2, $city, $state, $pincode, $mobile, $phone, $is_small_appliance, $is_large_appliance, $is_active,$sequence,$contact_type,$contact_person);

    public function updateApplianceType($id, $type, $status);

    public function activateOrDeactivate($id, $status);

    public function deleteServiceCenters($id = array());

    public function getUniqueStates();

    public function getCitiesForState($state);

    public function getSubcategoriesForCategories($category);

    public function getData($city, $state, $is_small_appliance, $is_large_appliance);
} 