<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/14/14
 * Time: 1:04 PM
 */
interface iCartRepository
{

    public function addItem($item_type, $item_id, $user_id, $qty, $price);

    public function getCartContents($user_id);

    public function getAllUsersCartContent($from_date, $to_date, $paginate, $is_deleted_by_admin);

    public function getAllUsersCartTrashContent($from_date, $to_date, $paginate, $is_deleted_by_admin);

    public function getItem($item_type, $item_id, $user_id);

    public function updateCart($cart_id, $item_id, $item_type, $qty, $price);

    public function removeItem($item_type, $item_id, $user_id);

    public function updateItemByCartId($cart_item_id, $cart_id, $is_deleted_by_admin);

    public function getTotalPrice($user_id);

    public function getTotalItems($user_id);

    public function getCartIdByUserId($user_id);

    public function destroyCart($cart_id);

    public function getCart($cart_id, $is_trashed);

    public function getRestoreCart($cart_id);

    public function getRestoreCartItem($cart_item_id, $cart_id);

    public function getCartData();
}