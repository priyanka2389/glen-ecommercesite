<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 4:37 PM
 */
interface iOfferRepository
{

    public function create($name, $email, $phone, $city, $message,$source);

    public function getEnquiries($from_date, $to_date, $paginate);

    public function getEnquiry($id);

    public function deleteEnquiry($id);
} 