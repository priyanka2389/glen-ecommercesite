<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 9/15/14
 * Time: 4:54 PM
 */
interface iSettingsRepository
{

    public function createSetting($setting_label,$setting_value);

    public function getSettings();

    public function getSetting($id);

    public function updateSettings($id, $label, $value);

    public function getSettingByLabel($label);

//    public function deleteMessage($id);
} 