<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/8/14
 * Time: 1:01 PM
 */
interface iDealersRepository
{
    public function createDealer($name, $address1, $address2,  $location,$city, $state, $pincode, $mobile, $phone, $is_small_appliance, $is_large_appliance, $is_active,$sequence,$contact_person,$email_id,$type);

    public function getDealers($city, $state, $paginate);

    public function getDealer($id);

    public function updateDealer($id, $name, $address1, $address2, $city, $state, $pincode, $mobile, $phone, $is_small_appliance, $is_large_appliance, $is_active,$sequence,$contact_person,$email_id,$type);

    public function updateApplianceType($id, $type, $status);

    public function activateOrDeactivate($id, $status);

    public function deleteDealers($id = array());

    public function getUniqueStates($type);

    public function getCitiesForState($state,$type);

    public function getLocationsForCities($state,$city,$type);

    public function getSubcategoriesForCategories($category);

    public function getData($city, $state,$location, $is_small_appliance, $is_large_appliance,$type);
} 