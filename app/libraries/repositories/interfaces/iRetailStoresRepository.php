<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 02/02/15
 * Time: 1:01 PM
 */
interface iRetailStoresRepository
{
    public function createRetailStore($email,$address, $city, $state, $pincode, $mobile, $phone,$contact_person,$active,$sequence);

    public function getRetailStores($city, $state, $is_active,$paginate);

    public function getRetailStore($id);

    public function updateRetailStore($id,  $email,$address, $city, $state, $pincode, $mobile, $phone,$contact_person,$active,$sequence);

    public function updateApplianceType($id, $type, $status);

    public function activateOrDeactivate($id, $status);

    public function deleteRetailStores($id = array());

    public function getUniqueStates();

    public function getCitiesForState($state);

    public function getSubcategoriesForCategories($category);

    public function getData($city, $state, $is_small_appliance, $is_large_appliance);
} 