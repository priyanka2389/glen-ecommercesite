<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 4:37 PM
 */
interface iDemoRepository
{

    public function createDemo($name, $email, $phone, $state, $city, $message, $product_id);

    public function getDemos($from_date, $to_date, $paginate);

    public function getDemo($id);

    public function deleteDemo($id);
} 