<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/29/14
 * Time: 1:02 PM
 */
interface iCouponMainRepository
{

    public function createMainCoupon($name, $percentage, $min_vale, $expiry,
                                     $is_unique, $coupons_qty,$coupon_code, $category_ids = array());

    public function getMainCoupons($from_date, $to_date, $expired, $paginate);

    public function getMainCoupon($id);

    public function updateMainCoupon($id, $name, $percentage, $expiry);

    public function getMainCouponCategory($main_coupon_id, $category_id);

    public function getCoupons($coupon_main_id, $is_used, $paginate);

    public function getCoupon($coupon_id, $coupon_code);

    public function updateCoupon($id,$order_id);

}