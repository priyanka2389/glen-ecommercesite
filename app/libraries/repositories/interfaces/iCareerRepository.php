<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/14/14
 * Time: 3:43 PM
 */
interface iCareerRepository
{

    public function addCareer($name, $email, $phone, $address, $dob, $applying_department, $educational_qualification,
                            $professional_qualification, $primary_skill, $career_highlights, $work_exp, $resume_path);

    public function getCareers($from_date,$to_date,$paginate);

    public function getCareer($id);

    public function deleteCareer($id);

} 