<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/14/14
 * Time: 6:03 PM
 */
interface iServicesEnquiryRepository
{

    public function createServiceEnquiry($name, $email, $phone, $address, $product, $bill_no, $shop_name, $bill_date,
                                         $message);

    public function getServiceEnquiries($from_date, $to_date, $paginate);

    public function getServiceEnquiry($id);

    public function deleteServiceEnquiry($id);

} 