<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/13/14
 * Time: 1:15 PM
 */
interface iUserRepository
{

    public function createUser($first_name, $last_name, $email, $mobile,$password, $newsletters, $special_offers);

    public function addUserAddress($user_id, $line1, $line2, $state, $city, $landmark, $pincode, $first_name,
                                   $last_name, $landline, $mobile, $type);

    public function updateUserAddress($address_id, $user_id, $line1, $line2, $state, $city, $landmark, $pincode, $first_name,
                                      $last_name, $landline, $mobile, $type);

    public function deleteUserAddress($address_id);

    public function getUserAddresses($user_id, $type);

    public function getAddress($address_id);

    public function getUserAddress($user_id);

    public function getUsers($newletters, $special_offers, $from_date, $to_date,$paginate);

    public function getUser($id, $email, $phone);

    public function updateUser($id, $first_name, $last_name, $pincode, $address_line1, $address_line2,
                               $landmark, $city, $state, $landline, $mobile);

    public function updateUserMobile($id, $mobile);

    public function changePassword($id, $email, $password);

    public function forgotPassword($email, $password);

    public function getUserRole($id);

    public function getStates();

    public function getCitiesByState($state);

    public function getPincodesByCity($city);

    public function getCityAndStateByPincode($pincode);

    public function getUpdateUserAddressType($user_id, $address_id, $type);


}