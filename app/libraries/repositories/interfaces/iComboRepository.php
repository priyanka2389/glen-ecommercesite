<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/9/14
 * Time: 10:56 AM
 */
interface iComboRepository
{

    public function createCombo($name,$code,$short_code, $description, $type, $start_date, $end_date, $combo_price,
                                $is_cod,$is_active, $combo_products_array = array());

    public function getCombos($paginate,$is_active);

    public function getCombo($id,$short_code);


    public function updateCombo($id, $name,$code,$short_code, $description, $type, $start_date, $end_date, $combo_price, $is_cod,$is_active);

    public function deleteCombo($id);

    public function activateOrDeactivateCombo($id, $status);

    public function createComboProduct($combo_id, $product_id, $combo_price);

    public function getComboProducts($combo_id);

    public function getComboProduct($combo_id, $product_id);

    public function getCombosIdByProductId($product_id);

    public function updateComboProduct($combo_id, $product_id, $combo_price);

    public function deleteComboProduct($combo_id, $product_id);

    public function getRelatedCombos($combo_id);

    //Product Image CRUD starts here

    public function createComboImage($path, $name, $title, $caption, $notes, $is_primary, $combo_id);

    public function getComboImages($id);

    public function getComboImage($combo_id, $image_id);

    public function setComboPrimaryImage($combo_id, $image_id);

    public function updateComboImage($combo_id, $image_id, $name, $title, $caption, $notes, $is_primary, $path);

    public function deleteComboImage($combo_id, $image_id = array());

    //Product Image CRUD ends here

}