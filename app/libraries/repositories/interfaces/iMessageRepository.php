<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 9/15/14
 * Time: 4:54 PM
 */
interface iMessageRepository
{

    public function createMessage($name, $email, $friend_name, $friend_email, $message,$product_link);

    public function getMessages($from_date, $to_date, $paginate);
//
//    public function getDemo($id);
//
    public function deleteMessage($id);
} 