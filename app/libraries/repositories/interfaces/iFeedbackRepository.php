<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/26/14
 * Time: 3:57 PM
 */
interface iFeedbackRepository
{

    public function createFeedback($email, $phone, $category, $message);

    public function getFeedbacks($from_date, $to_date, $paginate);

    public function deleteFeedback($id);
} 