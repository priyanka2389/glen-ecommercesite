<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 9/15/14
 * Time: 4:54 PM
 */
interface iCallRepository
{

    public function createCall($name, $mobile,$email);

    public function getCalls($from_date, $to_date, $paginate);

//    public function getDemo($id);

    public function deleteCall($id);
} 