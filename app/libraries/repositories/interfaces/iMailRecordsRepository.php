<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 02/02/15
 * Time: 1:01 PM
 */
interface iMailRecordsRepository
{
    public function createMailRecord($user_name,$to_emailId,$from_emailId,$email_subject,$email_content);

    public function getMails($from_date, $to_date, $paginate);

    public function getMail($mail_id);
} 