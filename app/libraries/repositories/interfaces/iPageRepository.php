<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 9/15/14
 * Time: 4:54 PM
 */
interface iPageRepository
{

    public function createPage($title,$short_code,$source_code,$active);

    public function getPages();

    public function getPage($id,$short_code,$is_active);

    public function updatePage($id,$title, $short_code, $source_code, $active);
//
//    public function updateApplianceType($id, $type, $status);
//
    public function activateOrDeactivate($id, $status);
//
    public function deletePages($id = array());
//
//    public function getData($city, $state, $is_small_appliance, $is_large_appliance);
} 