<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/14/14
 * Time: 4:11 PM
 */
interface iEnquiryRepository
{

    public function createEnquiry($name, $company_name, $email, $phone,$country, $state, $city, $requirement,
                                  $interest, $purpose, $message, $enquiry_type);

    public function getEnquires($from_date, $to_date, $enquiry_type,$paginate);

    public function getEnquiry($id);

    public function deleteEnquiry($id);
} 