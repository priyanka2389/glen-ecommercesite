<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 2/7/14
 * Time: 4:17 PM
 */
interface iOrderRepository
{

    public function createOrder($user_id, $net_value, $final_value, $order_status, $payment_status, $order_notes, $reference,
                                $cart_id, $payment_mode, $shipping_address_id, $billing_address_id,
                                $verification_code, $coupon_id, $order_items = array());

    public function getOrder($id);

    public function getOrders($user_id, $status, $payment_status, $payment_method, $from_date, $to_date, $paginate, $show_trash);

    public function getOrderAddress($address_id, $type);

    public function updateStatus($id, $status);

    public function updatePaymentStatus($id, $status, $notes);

    public function updateVerificationCode($id, $verification_code);

    public function updateCodVerificationStatus($order_id, $status);

    public function updateCouponId($id, $coupon_id);

    public function deleteOrder($id);

    public function getUpdateOrderByAdmin($order_id, $show_trash);

    public function getOnlinePendingOrders();

} 