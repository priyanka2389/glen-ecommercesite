<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/7/14
 * Time: 11:56 AM
 */
interface iPgResponseRepository
{

    public function addResponse($amount, $batch_no, $command, $locale, $merchant_txn_ref, $merchant, $message, $order_info, $transaction_no, $txn_response_code, $version, $additional_notes);

    public function getResponse($id, $merchant_tax_ref, $order_info);

    public function updatePaymentStatus($order_id,$payment_status);

} 