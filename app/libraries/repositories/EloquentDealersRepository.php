<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/8/14
 * Time: 1:03 PM
 */
class EloquentDealersRepository implements iDealersRepository
{

    /**
     * @param $name
     * @param $address1
     * @param $address2
     * @param $city
     * @param $state
     * @param $pincode
     * @param $mobile
     * @param $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @throws Exception
     * @return Dealer
     */
    public function createDealer($name, $address1, $address2, $location, $city, $state, $pincode, $mobile, $phone, $is_small_appliance, $is_large_appliance, $is_active, $sequence, $contact_person, $email_id, $type)
    {
        try {

            if ($sequence == 'top') {
                Dealer::where('sequence', '>', 1)->increment("sequence");
                $sequence = 1;
            } else if ($sequence == 'bottom') {
                $max_sequence = Dealer::max('sequence');
                $sequence = $max_sequence + 1;
            } else {
                $this->addOrUpdateSequence(null, $sequence, null);
                $sequence = $sequence + 1;
            }

            Dealer::where('sequence', '>=', $sequence)->increment('sequence', 10); //updates the sequence

            $dealer = new Dealer();
            $dealer->name = $name;
            $dealer->address1 = $address1;
            $dealer->address2 = $address2;
            $dealer->location = $location;
            $dealer->city = $city;
            $dealer->state = $state;
            $dealer->pincode = $pincode;
            $dealer->mobile = $mobile;
            $dealer->phone = $phone;
            $dealer->sequence = $sequence;
            $dealer->is_small_appliance = $is_small_appliance;
            $dealer->is_large_appliance = $is_large_appliance;
            $dealer->is_active = $is_active;
            $dealer->contact_person = $contact_person;
            $dealer->email_id = $email_id;
            $dealer->type = $type;
            $dealer->save();

            return $dealer;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /**
     * @param $city
     * @param $state
     * @param $paginate
     * @throws Exception
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getDealers($city, $state, $paginate)
    {
        try {

            $query = Dealer::query();
            if (DbUtil::checkDbNotNullValue($city)) {
                $query->where('city', '=', $city);
            } elseif (DbUtil::checkDbNullValue($city)) {
                $query->whereNull('city');
            }

            if (DbUtil::checkDbNotNullValue($state)) {
                $query->where('state', '=', $state);
            } elseif (DbUtil::checkDbNullValue($state)) {
                $query->whereNull('state');
            }

            if (!is_null($paginate)) {
                return $query->orderBy('sequence')->paginate($paginate);
            }
            return $query->orderBy('sequence')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     * @throws Exception
     */
    public function getDealer($id)
    {

        try {

            return Dealer::where('id', '=', $id)->first();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /**
     * @param $id
     * @param $name
     * @param $address1
     * @param $address2
     * @param $city
     * @param $state
     * @param $pincode
     * @param $mobile
     * @param $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @throws Exception
     */
    public function updateDealer($id, $name, $address1, $address2, $city, $state, $pincode, $mobile, $phone, $is_small_appliance, $is_large_appliance, $is_active, $sequence, $contact_person, $email_id, $type)
    {
        try {

            $dealer = Dealer::find($id);

            if ($sequence == 'top') {
                Dealer::where('sequence', '>=', 1)->increment("sequence");
                $sequence = 1;

                $dealer->sequence = $sequence;
                $dealer->save();
            } else if ($sequence == 'bottom') {
                $max_sequence = Dealer::max('sequence');
                $sequence = $max_sequence;

                Dealer::where('sequence', '>', 1)->decrement("sequence");

                $dealer->sequence = $sequence;
                $dealer->save();
            } else {
                $current_sequence = $dealer->sequence;
                $this->addOrUpdateSequence($current_sequence, (int)$sequence, $id);
//                $sequence = $sequence + 1;
            }

            $dealer->name = $name;
            $dealer->address1 = $address1;
            $dealer->address2 = $address2;
            $dealer->city = $city;
            $dealer->state = $state;
            $dealer->pincode = $pincode;
            $dealer->mobile = $mobile;
            $dealer->phone = $phone;
            $dealer->is_small_appliance = $is_small_appliance;
            $dealer->is_large_appliance = $is_large_appliance;
            $dealer->is_active = $is_active;
            $dealer->contact_person = $contact_person;
            $dealer->email_id = $email_id;
            $dealer->type = $type;
            $dealer->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** updates the appliance type
     * @param int $id dealer_id
     * @param string $type small|large
     * @param bool $status 0|1
     * @throws Exception
     */
    public function updateApplianceType($id, $type, $status)
    {
        try {

            $dealer = Dealer::find($id);
            if ($type == "small") {
                $dealer->is_small_appliance = $status;
            } else {
                $dealer->is_large_appliance = $status;
            }
            $dealer->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** accepts dealer_id and activate or deactivate the corresponding dealer
     * @param int $id dealer_id
     * @param bool $status 0|1
     * @throws Exception
     */
    public function activateOrDeactivate($id, $status)
    {
        try {

            $dealer = Dealer::find($id);
            $dealer->is_active = $status;
            $dealer->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function  addOrUpdateSequence($current_sequence, $after_sequence, $dealer_id)
    {
        if (is_null($current_sequence)) { //new product is added

            Dealer::where('sequence', '>', $after_sequence)->increment('sequence');
//            $product = Product::where('sequence', '=', $after_sequence)->first();
//            $product->sequence = $after_sequence + 1;
//            $product->save();

        } else {

            if ($current_sequence > $after_sequence) {
                if (DbUtil::checkDbNotNullValue($dealer_id)) {
                    $dealer = Dealer::where('sequence', '=', $current_sequence)->where('id', $dealer_id)->first();
                } elseif (DbUtil::checkDbNullValue($dealer_id)) {
                    $dealer = Dealer::where('sequence', '=', $current_sequence)->whereNull('id')->first();
                }
//                $dealer = Dealer::where('sequence', '=', $current_sequence)->first();
                //Product::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->get();
                Dealer::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->increment('sequence', 1);
                $dealer->sequence = $after_sequence + 1;
                $dealer->save();

            } else if ($current_sequence == $after_sequence) {
//                $product = Product::where('sequence', '=', $current_sequence)->first();
                if (DbUtil::checkDbNotNullValue($dealer_id)) {
                    $dealer = Dealer::where('sequence', '=', $current_sequence)->where('id', $dealer_id)->first();
                } elseif (DbUtil::checkDbNullValue($dealer_id)) {
                    $dealer = Dealer::where('sequence', '=', $current_sequence)->whereNull('id')->first();
                }

                //Product::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->get();
                Dealer::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->increment('sequence', 1);
                $dealer->sequence = $after_sequence + 1;
                $dealer->save();
            } else {
                if (DbUtil::checkDbNotNullValue($dealer_id)) {
                    $dealer = Dealer::where('sequence', '=', $current_sequence)->where('id', $dealer_id)->first();
                } elseif (DbUtil::checkDbNullValue($dealer_id)) {
                    $dealer = Dealer::where('sequence', '=', $current_sequence)->whereNull('id')->first();
                }
//                $dealer = Dealer::where('sequence', '=', $current_sequence)->first();
                Dealer::where('sequence', '>', $current_sequence)->where('sequence', '<=', $after_sequence)->decrement('sequence', 1);
                $dealer->sequence = $after_sequence;
                $dealer->save();
            }
        }


    }

    public function deleteDealers($id = array())
    {
        Dealer::whereIn("id", $id)->delete();
    }

    public function getUniqueStates($type)
    {
        try {

            return Dealer::groupBy('state')->where("type", '=', $type)->whereNotNull('state')->where('state', '!=', '')->where("is_active", '=', 1)->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getCitiesForState($state, $type)
    {
        try {
            return Dealer::where("state", '=', $state)->groupBy("city")->where("type", '=', $type)->where("is_active", '=', 1)->get();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getLocationsForCities($state,$city, $type)
    {
        try {
            return Dealer::where("state", '=', $state)->where("city", '=', $city)->groupBy("location")->where("type", '=', $type)->where("is_active", '=', 1)->get();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getSubcategoriesForCategories($category)
    {
        try {
            return Category::where("parent_category_id", '=', $category)->groupBy("name")->get();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getData($city, $state, $location, $is_small_appliance, $is_large_appliance, $type)
    {

        try {

            $query = Dealer::query();
            if (!empty($state)) {
                $query->where("state", '=', $state);
            }
            if (!empty($city)) {
                $query->where("city", '=', $city);
            }
            if (!is_null($location)) {
                $query->where("location", '=', $location);
            }
            if (!empty($type)) {
                $query->where("type", '=', $type);
            }
//            if ($is_large_appliance == true && $is_small_appliance == true || $is_large_appliance == false && $is_small_appliance == false) {
//                $query->where("is_small_appliance", '=', $is_large_appliance)
//                    ->where("is_large_appliance", '=', $is_small_appliance);
//
//            } else
            if ($is_large_appliance == true) {
                $query->where("is_large_appliance", '=', 1);

            }
            if ($is_small_appliance == true) {
                $query->where("is_small_appliance", '=', 1);
            }
            $query->where("is_active", '=', 1);
            return $query->orderBy('type', 'desc')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }


    }

} 