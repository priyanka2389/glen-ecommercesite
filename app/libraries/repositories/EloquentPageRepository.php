<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 5/20/14
 * Time: 4:39 PM
 */
class EloquentPageRepository implements iPageRepository
{

   /**
* @param $title
* @param $short_code
* @param $source_code
* @param $is_active
* @throws Exception
* @return page
*/
    public function createPage($title,$short_code,$source_code,$active)
    {
        try {
            $page = new Page();
            $page->title = $title;
            $page->short_code = $short_code;
            $page->source_code = $source_code;
            $page->is_active = $active;
            $page->save();

            return $page;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /**

     * @throws Exception
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getPages()
    {
        try {

            $query = Page::query();
            return $query->orderBy('created_at')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     * @throws Exception
     */
    public function getPage($id,$short_code,$is_active)
    {

        $query = Page::query();
        try {
            if (DbUtil::checkDbNotNullValue($id)) {
                $query->where('id', '=', $id);
            } elseif (DbUtil::checkDbNullValue($id)) {
                $query->whereNull('id');
            }

            if (DbUtil::checkDbNotNullValue($short_code)) {
                $query->where('short_code', '=', $short_code);
            } elseif (DbUtil::checkDbNullValue($short_code)) {
                $query->whereNull('short_code');
            }

            if (DbUtil::checkDbNotNullValue($is_active)) {
                $query->where('is_active', '=', $is_active);
            } elseif (DbUtil::checkDbNullValue($is_active)) {
                $query->whereNull('is_active');
            }

            return $query->first();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /**
     * @param $title
     * @param $short_code
     * @param $source_code
     * @param $is_active
     * @throws Exception
     */
    public function updatePage($id,$title, $short_code, $source_code, $active)
    {
        try {

            $page = Page::find($id);

            $page->title = $title;
            $page->short_code = $short_code;
            $page->source_code = $source_code;
            $page->is_active = $active;
            $page->save();

            return $page;
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

//
    /** accepts page_id and activate or deactivate the corresponding page
     * @param int $id page_id
     * @param bool $status 0|1
     * @throws Exception
     */
    public function activateOrDeactivate($id, $status)
    {
        try {
            $page = Page::find($id);
            $page->is_active = $status;
            $page->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function deletePages($id = array())
    {
        Page::whereIn("id", $id)->delete();
    }

}