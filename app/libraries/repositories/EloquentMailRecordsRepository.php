<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/8/14
 * Time: 1:03 PM
 */
class EloquentMailRecordsRepository implements iMailRecordsRepository
{

    public function createMailRecord($user_name, $to_emailId, $from_emailId, $email_subject, $email_content)
    {
        try {

            $mail = new MailRecords();
            $mail->user_name = $user_name;
            $mail->to = $to_emailId;
            $mail->from = $from_emailId;
            $mail->subject = $email_subject;
            $mail->message = $email_content;
            $mail->save();

            return $mail;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }


    public function getMails($from_date, $to_date, $paginate)
    {
        try {

            $query = MailRecords::query();
            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
            }

            if (!is_null($paginate)) {
                return $query->orderBy('created_at')->paginate($paginate);
            }
            return $query->orderBy('created_at')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getMail($mail_id)
    {
        try {

            $query = MailRecords::find($mail_id);
            $mail = $query->first();
            return $mail;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

}