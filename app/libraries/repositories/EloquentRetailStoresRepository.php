<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/8/14
 * Time: 1:03 PM
 */
class EloquentRetailStoresRepository implements iRetailStoresRepository
{

    /**
     * @param $name
     * @param $address1
     * @param $address2
     * @param $city
     * @param $state
     * @param $pincode
     * @param $mobile
     * @param $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @throws Exception
     * @return Dealer
     */
    public function createRetailStore($email, $address, $city, $state, $pincode, $mobile, $phone, $contact_person, $is_active, $sequence)
    {
        try {

            if ($sequence == 'top') {
                Retailstore::where('sequence', '>', 1)->increment("sequence");
                $sequence = 1;
            } else if ($sequence == 'bottom') {
                $max_sequence = Distributor::max('sequence');
                $sequence = $max_sequence + 1;
            } else {
                $this->addOrUpdateSequence(null, $sequence, null);
                $sequence = $sequence + 1;
            }

            Retailstore::where('sequence', '>=', $sequence)->increment('sequence', 10); //updates the sequence

            $retailStore = new Retailstore();
//            $retailStore->name = $name;
            $retailStore->email_id = $email;
            $retailStore->address = $address;
            $retailStore->city = $city;
            $retailStore->state = $state;
            $retailStore->pincode = $pincode;
            $retailStore->mobile = $mobile;
            $retailStore->phone = $phone;
            $retailStore->contact_person = $contact_person;
            $retailStore->sequence = $sequence;
//            $retailStore->is_small_appliance = $is_small_appliance;
//            $retailStore->is_large_appliance = $is_large_appliance;
            $retailStore->is_active = $is_active;
            $retailStore->save();

            return $retailStore;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /**
     * @param $city
     * @param $state
     * @param $paginate
     * @throws Exception
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRetailStores($city, $state, $is_active, $paginate)
    {
        try {

            $query = Retailstore::query();
            if (DbUtil::checkDbNotNullValue($city)) {
                $query->where('city', '=', $city);
            } elseif (DbUtil::checkDbNullValue($city)) {
                $query->whereNull('city');
            }

            if (DbUtil::checkDbNotNullValue($state)) {
                $query->where('state', '=', $state);
            } elseif (DbUtil::checkDbNullValue($state)) {
                $query->whereNull('state');
            }

            if (DbUtil::checkDbNotNullValue($is_active)) {
                $query->where('is_active', '=', $is_active);
            } elseif (DbUtil::checkDbNullValue($is_active)) {
                $query->whereNull('is_active');
            }

            if (!is_null($paginate)) {
                return $query->orderBy('sequence')->paginate($paginate);
            }
            return $query->orderBy('sequence')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     * @throws Exception
     */
    public function getRetailStore($id)
    {

        try {

            return Retailstore::where('id', '=', $id)->first();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /**
     * @param $id
     * @param $name
     * @param $address1
     * @param $address2
     * @param $city
     * @param $state
     * @param $pincode
     * @param $mobile
     * @param $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @throws Exception
     */
    public function updateRetailStore($id, $email, $address, $city, $state, $pincode, $mobile, $phone, $contact_person, $is_active, $sequence)
    {
        try {

            $retailStore = Retailstore::find($id);

            if ($sequence == 'top') {
                Retailstore::where('sequence', '>=', 1)->increment("sequence");
                $sequence = 1;

                $retailStore->sequence = $sequence;
                $retailStore->save();
            } else if ($sequence == 'bottom') {
                $max_sequence = Distributor::max('sequence');
                $sequence = $max_sequence;

                Retailstore::where('sequence', '>', 1)->decrement("sequence");

                $retailStore->sequence = $sequence;
                $retailStore->save();
            } else {
                $current_sequence = $retailStore->sequence;
                $this->addOrUpdateSequence($current_sequence, (int)$sequence, $id);
//                $sequence = $sequence + 1;
            }

//            $retailStore->name = $name;
            $retailStore->email_id = $email;
            $retailStore->address = $address;
            $retailStore->city = $city;
            $retailStore->state = $state;
            $retailStore->pincode = $pincode;
            $retailStore->mobile = $mobile;
            $retailStore->phone = $phone;
            $retailStore->contact_person = $contact_person;
            $retailStore->is_active = $is_active;
            $retailStore->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** updates the appliance type
     * @param int $id dealer_id
     * @param string $type small|large
     * @param bool $status 0|1
     * @throws Exception
     */
    public function updateApplianceType($id, $type, $status)
    {
        try {

            $retailStore = Retailstore::find($id);
            if ($type == "small") {
                $retailStore->is_small_appliance = $status;
            } else {
                $retailStore->is_large_appliance = $status;
            }
            $retailStore->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** accepts dealer_id and activate or deactivate the corresponding dealer
     * @param int $id dealer_id
     * @param bool $status 0|1
     * @throws Exception
     */
    public function activateOrDeactivate($id, $status)
    {
        try {

            $retailStore = Retailstore::find($id);
            $retailStore->is_active = $status;
            $retailStore->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function  addOrUpdateSequence($current_sequence, $after_sequence, $store_id)
    {
        if (is_null($current_sequence)) { //new product is added

            Retailstore::where('sequence', '>', $after_sequence)->increment('sequence');
//            $product = Product::where('sequence', '=', $after_sequence)->first();
//            $product->sequence = $after_sequence + 1;
//            $product->save();

        } else {

            if ($current_sequence > $after_sequence) {
                if (DbUtil::checkDbNotNullValue($store_id)) {
                    $retailStore = Retailstore::where('sequence', '=', $current_sequence)->where('id', $store_id)->first();
                } elseif (DbUtil::checkDbNullValue($store_id)) {
                    $retailStore = Retailstore::where('sequence', '=', $current_sequence)->whereNull('id')->first();
                }
//                $dealer = Dealer::where('sequence', '=', $current_sequence)->first();
                //Product::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->get();
                Retailstore::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->increment('sequence', 1);
                $retailStore->sequence = $after_sequence + 1;
                $retailStore->save();

            } else if ($current_sequence == $after_sequence) {
//                $product = Product::where('sequence', '=', $current_sequence)->first();
                if (DbUtil::checkDbNotNullValue($store_id)) {
                    $retailStore = Retailstore::where('sequence', '=', $current_sequence)->where('id', $store_id)->first();
                } elseif (DbUtil::checkDbNullValue($store_id)) {
                    $retailStore = Retailstore::where('sequence', '=', $current_sequence)->whereNull('id')->first();
                }

                //Product::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->get();
                Distributor::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->increment('sequence', 1);
                $retailStore->sequence = $after_sequence + 1;
                $retailStore->save();
            } else {
                if (DbUtil::checkDbNotNullValue($store_id)) {
                    $retailStore = Retailstore::where('sequence', '=', $current_sequence)->where('id', $store_id)->first();
                } elseif (DbUtil::checkDbNullValue($store_id)) {
                    $retailStore = Retailstore::where('sequence', '=', $current_sequence)->whereNull('id')->first();
                }
//                $dealer = Dealer::where('sequence', '=', $current_sequence)->first();
                Retailstore::where('sequence', '>', $current_sequence)->where('sequence', '<=', $after_sequence)->decrement('sequence', 1);
                $retailStore->sequence = $after_sequence;
                $retailStore->save();
            }
        }


    }

    public function deleteRetailStores($id = array())
    {
        Retailstore::whereIn("id", $id)->delete();
    }

    public function getUniqueStates()
    {
        try {

            return Retailstore::groupBy('state')->whereNotNull('state')->where('state', '!=', '')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getCitiesForState($state)
    {
        try {
            return Retailstore::where("state", '=', $state)->groupBy("city")->get();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getSubcategoriesForCategories($category)
    {
        try {
            return Category::where("parent_category_id", '=', $category)->groupBy("name")->get();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getData($city, $state, $is_small_appliance, $is_large_appliance)
    {

        try {

            $query = Retailstore::query();
            if (!empty($city)) {
                $query->where("city", '=', $city);
            }
            if (!empty($state)) {
                $query->where("state", '=', $state);
            }
            if ($is_large_appliance == true && $is_small_appliance == true || $is_large_appliance == false && $is_small_appliance == false) {
                $query->where("is_small_appliance", '=', $is_large_appliance)
                    ->where("is_large_appliance", '=', $is_small_appliance);

            } elseif ($is_large_appliance == true && $is_small_appliance == false) {
                $query->where("is_large_appliance", '=', true);

            } elseif ($is_large_appliance == false && $is_small_appliance == true) {
                $query->where("is_small_appliance", '=', true);
            }
//            $query->where("is_active", '=', true);
            return $query->orderBy('sequence')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }


    }

} 