<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 5/20/14
 * Time: 4:39 PM
 */
class EloquentSettingsRepository implements iSettingsRepository
{

    public function createSetting($setting_label,$setting_value)
    {
        try {
            $setting = new Settings();
            $setting->label = $setting_label;
            $setting->value = $setting_value;
            $setting->save();

            return $setting;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getSettings()
    {
        try {

            $query = Settings::query();
            $res = $query->orderBy('created_at', 'DESC')->get();
            return $res;
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getSetting($id)
    {
        try {

            return Settings::find($id);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    public function updateSettings($id, $setting_label,$setting_value)
    {

        try {

            $setting = Settings::find($id);
            $setting->label = $setting_label;
            $setting->value = $setting_value;
            $setting->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getSettingByLabel($label)
    {
        try {

            return Settings::where('label','=', $label)->first();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

//
//    public function deleteMessage($id)
//    {
//        try {
//
//            return Message::find($id)->delete();
//
//        } catch (Exception $ex) {
//            Log::error($ex);
//            throw $ex;
//        }
//    }
} 