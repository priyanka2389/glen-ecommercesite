<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/29/14
 * Time: 1:40 PM
 */
class EloquentCouponMainRepository implements iCouponMainRepository
{
    public function createMainCoupon($name, $percentage, $min_vale, $expiry_date,
                                     $is_unique, $coupons_qty, $coupon_code,$category_ids = array())
    {
        try {

            DB::beginTransaction();
            //creates a main coupon
            $main_coupon = new CouponMain();
            $main_coupon->name = $name;
            $main_coupon->percentage = $percentage;
            $main_coupon->min_value = $min_vale;
            $main_coupon->expiry_date = $expiry_date;
            $main_coupon->is_unique = $is_unique;
            $main_coupon->save();

            $main_coupon_id = $main_coupon->id;


            // attach main coupon with the category_id
            foreach ($category_ids as $category_id) {

                $main_coupon->categories()->attach($category_id);
            }

            if($is_unique == 1){
                $coupon = new Coupon();
                $coupon->code = $coupon_code;
                $coupon->coupon_main_id = $main_coupon_id;
                $coupon->save();
            }

            //loops according to coupon qty and generates the random coupon codes and stores in coupons table
            for ($i = 0; $i < $coupons_qty; $i++) {

                $code = Str::random(8);
                $coupon = new Coupon();
                $coupon->code = $code;
                $coupon->coupon_main_id = $main_coupon_id;
                $coupon->save();
            }

            DB::commit();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getMainCoupons($from_date, $to_date, $expired, $paginate)
    {
        try {

            $query = CouponMain::query();
            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
            }

            if (!is_null($expired)) {
                if ($expired == true) {
                    $query->where('expired', '!=', null);
                } else {
                    $query->where('expired', '==', null);
                }
            }

            if (!is_null($paginate)) {
                return $query->orderBy('created_at', 'desc')->paginate();
            }

            return $query->orderBy('created_at', 'desc')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getMainCoupon($id)
    {
        try {

            $main_coupon = CouponMain::find($id);
            return $main_coupon;


        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    public function updateMainCoupon($id, $name, $percentage, $expiry_date)
    {
        try {

            $main_coupon = CouponMain::find($id);
            $main_coupon->name = $name;
            $main_coupon->percentage = $percentage;
            $main_coupon->expiry_date = $expiry_date;
            $main_coupon->save();


        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getMainCouponCategory($main_coupon_id, $category_id)
    {
        try {

            $result = DB::table('coupon_main_categories')->where('coupon_main_id', '=', $main_coupon_id)
                ->where('category_id', '=', $category_id)->first();
            return $result;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getCoupons($coupon_main_id, $is_used, $paginate)
    {
        try {

            $coupon = Coupon::query();
            if (DbUtil::checkDbNotNullValue($coupon_main_id)) {
                $coupon->where('coupon_main_id', '=', $coupon_main_id);
            } else {
                $coupon->whereNull('coupon_main_id');
            }
            if (!is_null($is_used)) {
                if ($is_used == true) {
                    $coupon->where('order_id', '!=', null);
                } else {
                    $coupon->where('order_id', '=', null);
                }
            }
            if (!is_null($paginate)) {
                return $coupon->orderBy('created_at')->paginate($paginate);
            }

            return $coupon->orderBy('created_at')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** return the info of the coupon code applied
     * @param $coupon_id
     * @param $coupon_code
     * @throws Exception
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|static
     */
    public function getCoupon($coupon_id, $coupon_code)
    {
        try {

            $query = Coupon::query();
            if (DbUtil::checkDbNotNullValue($coupon_id)) {
                $query->where('id', '=', $coupon_id);
            } else if (DbUtil::checkDbNullValue($coupon_id)) {
                $query->whereNull('id');
            }

            if (DbUtil::checkDbNotNullValue($coupon_code)) {
                $query->where('code', '=', $coupon_code);
            } else if (DbUtil::checkDbNullValue($coupon_code)) {
                $query->whereNull('code');
            }

            return $query->first();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    public function updateCoupon($id,$order_id){
        $coupon = Coupon::find($id);
        $coupon->order_id = $order_id;
        $coupon->save();
    }
} 