<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/2/14
 * Time: 1:31 PM
 */

class EloquentTrackingInfoRepository implements iTrackingInfoRepository
{


    public function addItem($order_id, $provider, $code, $tracking_url, $notes)
    {
        try {
            $info= TrackingInfo::where('order_id',$order_id)->first();
            if($info==null){
                $info=new TrackingInfo();
                $info->order_id=$order_id;
            }

            $info->provider=$provider;
            $info->code=$code;
            $info->tracking_url=$tracking_url;
            $info->notes=$notes;

            $info->save();
            return $info;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }
}