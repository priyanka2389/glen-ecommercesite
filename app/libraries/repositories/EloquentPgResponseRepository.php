<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/7/14
 * Time: 12:20 PM
 */
class EloquentPgResponseRepository implements iPgResponseRepository
{
    /**
     * @param $amount
     * @param $batch_no
     * @param $command
     * @param $locale
     * @param $merchant_txn_ref
     * @param $merchant
     * @param $message
     * @param $order_info
     * @param $transaction_no
     * @param $txn_response_code
     * @param $version
     * @param $additional_notes
     * @return PgResponse
     * @throws Exception
     */
    public function addResponse($amount, $batch_no, $command, $locale, $merchant_txn_ref, $merchant,
                                $message, $order_info, $transaction_no, $txn_response_code, $version, $additional_notes)
    {

        try {

            $response = new PgResponse();
            $response->amount = $amount;
            $response->batch_no = $batch_no;
            $response->command = $command;
            $response->locale = $locale;
            $response->merchant_txn_ref = $merchant_txn_ref;
            $response->merchant = $merchant;
            $response->message = $message;
            $response->order_info = $order_info;
            $response->transaction_no = $transaction_no;
            $response->txn_response_code = $txn_response_code;
            $response->version = $version;
            $response->additional_notes = $additional_notes;
            $response->save();
            return $response;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }


    }

    /**
     * @param $id
     * @param $merchant_tax_ref
     * @param $order_info
     * @return \Illuminate\Database\Eloquent\Model|null|static
     * @throws Exception
     */
    public function getResponse($id, $merchant_tax_ref, $order_info)
    {

        try {

            $query = PgResponse::query();
            if (DbUtil::checkDbNotNullValue($id)) {
                $query->where('id', '=', $id);
            } else if (DbUtil::checkDbNullValue($id)) {
                $query->whereNull('id');
            }

            if (DbUtil::checkDbNotNullValue($merchant_tax_ref)) {
                $query->where('merchant_tax_ref', '=', $merchant_tax_ref);
            } else if (DbUtil::checkDbNullValue($merchant_tax_ref)) {
                $query->whereNull('merchant_tax_ref');
            }

            if (DbUtil::checkDbNotNullValue($order_info)) {
                $query->where('order_info', '=', $order_info);
            } else if (DbUtil::checkDbNullValue($order_info)) {
                $query->whereNull('order_info');
            }

            return $query->first();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }


    }

    public function updatePaymentStatus($order_id,$payment_status){
        try{
            $query = PgResponse::where('order_info', '=', $order_id)->first();
            $query->additional_notes = $payment_status;
            $query->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }
} 