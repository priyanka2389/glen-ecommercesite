<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/9/14
 * Time: 10:56 AM
 */
class EloquentComboRepository implements iComboRepository
{

    //todo:check whether combo_price of combo products table should also be saved
    public function createCombo($name, $code, $short_code, $description, $type, $start_date, $end_date, $combo_price,
                                $is_cod,$is_active, $combo_products_array = array())
    {
        try {

            DB::beginTransaction();

            $combo = new Combo();
            $combo->name = $name;
            $combo->code = $code;
            $combo->shortcode = $short_code;
            $combo->description = $description;
            $combo->type = $type;
            $combo->start_date = $start_date;
            $combo->end_date = $end_date;
            $combo->combo_price = $combo_price;
            $combo->is_cod = $is_cod;
            $combo->is_active = $is_active;
            $combo->save();

            $combo_id = $combo->id;

            if (sizeof($combo_products_array) != 0) {
                foreach ($combo_products_array as $row) {

                    $product_id = $row['product_id'];
                    $price = $row['price'];
                    $combo->products()->attach($combo_id, array('product_id' => $product_id, 'combo_price' => $price));
                }
            }

            DB::commit();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** return all the combos
     * @param $paginate
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Pagination\Paginator|static[]
     * @throws Exception
     */
    public function getCombos($paginate,$is_active)
    {
        try {

            $combo = Combo::with(array('products', 'images'));

            if (!is_null($is_active)) {
                $combo->where('is_active', '=', $is_active);
            }

            if (!is_null($paginate) && $paginate == true) {
                return $combo->orderBy('created_at', 'DESC')->paginate(Constants::COMBOS_PAGE_COUNT);
            }

            return $combo->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** accepts combo by combo_id
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|static
     * @throws Exception
     */
    public function getCombo($id, $short_code)
    {
        try {

            $combo = Combo::with(array('products', 'images'));
            if (DbUtil::checkDbNotNullValue($id)) {
                $combo->where('id', '=', $id);
            } elseif (DbUtil::checkDbNullValue($id)) {
                $combo->whereNull('id');
            }

            if (DbUtil::checkDbNotNullValue($short_code)) {
                $combo->where('shortcode', '=', $short_code);
            } elseif (DbUtil::checkDbNullValue($short_code)) {
                $combo->whereNull('shortcode');
            }

            return $combo->first();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }


    /** updates the combo
     * @param int $id combo_id
     * @param string $name
     * @param string $description
     * @param string $type
     * @param $start_date
     * @param $end_date
     * @param int|float $combo_price
     * @param $is_cod
     * @throws Exception
     */
    public function updateCombo($id, $name, $code, $short_code, $description, $type, $start_date, $end_date, $combo_price, $is_cod,$is_active)
    {

        try {

            $combo = Combo::find($id);
            $combo->name = $name;
            $combo->code = $code;
            $combo->shortcode = $short_code;
            $combo->description = $description;
            $combo->type = $type;
            $combo->start_date = $start_date;
            $combo->end_date = $end_date;
            $combo->combo_price = $combo_price;
            $combo->is_cod = $is_cod;
            $combo->is_active = $is_active;
            $combo->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /** accepts the combo_id and deletes the combo
     * @param int $id
     * @throws Exception
     */
    public function deleteCombo($id)
    {
        try {

            Combo::find($id)->delete();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function activateOrDeactivateCombo($id, $status)
    {
        Combo::find($id)->update(array('is_active' => $status));
    }

    /** add a product in the existing combo
     * @param int $combo_id
     * @param int $product_id
     * @param int|float $combo_price
     */
    public function createComboProduct($combo_id, $product_id, $combo_price)
    {
        $combo = new ComboProduct();
        $combo->combo_id = $combo_id;
        $combo->product_id = $product_id;
        $combo->combo_price = $combo_price;
        $combo->save();

    }


    /** accepts the combo id and return the corresponding products
     * @param int $combo_id
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     * @throws Exception
     */
    public function getComboProducts($combo_id)
    {
        try {

            return Combo::with('products')->where('id', '=', $combo_id)->first();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    public function getComboProduct($combo_id, $product_id)
    {

        try {

            return ComboProduct::where('combo_id', '=', $combo_id)->where('product_id', '=', $product_id)->first();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /** return the combos by product_id
     * @param int $product_id
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     * @throws Exception
     */
    public function getCombosIdByProductId($product_id)
    {
        try {

            return ComboProduct::where('product_id', '=', $product_id)->get(array('combo_id'));

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** update the product info of combo
     * @param int $combo_id
     * @param int $product_id
     * @param int $combo_price
     * @throws Exception
     */
    public function updateComboProduct($combo_id, $product_id, $combo_price)
    {
        try {

            ComboProduct::where('combo_id', '=', $combo_id)
                ->where('product_id', '=', $product_id)->update(array('combo_price' => $combo_price));

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    /** deletes the product combo relation
     * @param int $combo_id
     * @param int $product_id
     * @throws Exception
     */
    public function deleteComboProduct($combo_id, $product_id)
    {
        try {
            $combo = Combo::find($combo_id);
            $combo->products()->detach($product_id);
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getRelatedCombos($id)
    {
//        $combo = $this->getCombo($id, null);
//        $type = $combo->type;
//        ->where('type', '=', $type)
//        ->take(Constants::RELATED_COMBOS_LIMIT)
        $combos = Combo::with('products')
            ->where('id', '!=', $id)->get();
        return $combos;
    }

    /**creates a new image and attaches it with the Combo
     * @param string $path
     * @param string $name
     * @param string $title
     * @param string $caption text beneath image
     * @param string $notes
     * @param bool $is_primary
     * @param int $combo_id
     * @return bool
     * @throws Exception
     */
    public function createComboImage($path, $name, $title, $caption, $notes, $is_primary, $combo_id)
    {
        try {

            DB::beginTransaction();

            $image = new Image();
            $image->path = $path;
            $image->save();

            $image->combos()->attach($combo_id, array('name' => $name, 'title' => $title,
                'caption' => $caption, 'notes' => $notes, 'is_primary' => $is_primary));

            DB::commit();

            return $image;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** accepts product_id and returns all the images of the corresponding product
     * @param int $id
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     * @throws Exception
     */
    public function getComboImages($id)
    {
        try {

            return Combo::with('images')->where('id', '=', $id)->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /** accepts product_id and image_id and returns the corresponding product image
     * @param int $combo_id
     * @param int $image_id
     * @return \Illuminate\Database\Eloquent\Model|mixed|null|static
     */
    public function getComboImage($combo_id, $image_id)
    {
        $result = ComboImage::join('images', function ($join) use ($image_id) {

            $join->on('comboimages.image_id', '=', 'images.id')->where('images.id', '=', $image_id);

        })->where('combo_id', '=', $combo_id)->where('comboimages.image_id', '=', $image_id)->first();

        return $result;
    }

    /** set the primary image of the product
     * @param $product_id int
     * @param $image_id int
     */
    public function setComboPrimaryImage($combo_id, $image_id)
    {
        DB::transaction(function () use ($combo_id, $image_id) {
            ComboImage::where('combo_id', '=', $combo_id)->update(array('is_primary' => false));
            ComboImage::where('combo_id', '=', $combo_id)
                ->where('image_id', '=', $image_id)
                ->update(array('is_primary' => true));
        });
    }

    /**
     * @param int $combo_id
     * @param int $image_id
     * @param string $name
     * @param string $title
     * @param string $caption
     * @param string $notes
     * @param $is_primary
     * @param $path
     */
    public function updateComboImage($combo_id, $image_id, $name, $title, $caption, $notes, $is_primary, $path)
    {

        $data = array(
            'name' => $notes,
            'title' => $title,
            'caption' => $caption,
            'notes' => $notes
        );

        if (!is_null($is_primary)) {
            $data['is_primary'] = $is_primary;
        }
        ComboImage::where('combo_id', '=', $combo_id)->where('image_id', '=', $image_id)->update($data);

        if (!is_null($path)) {
            $image = Image::find($image_id);
            $image->path = $path;
            $image->save();
        }

    }

    /** deletes the combo image relation
     * @param int $combo_id
     * @param array $image_id
     * @return bool
     */
    public function deleteComboImage($combo_id, $image_id = array())
    {
        $combo = Combo::find($combo_id);
        $combo->images()->detach($image_id);
        return true;
    }

}