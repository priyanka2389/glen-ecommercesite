<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 4:39 PM
 */
class EloquentDemoRepository implements iDemoRepository
{

    public function createDemo($name, $email, $phone, $state, $city, $message, $product_id)
    {
        try {

            $demo = new Demo();
            $demo->name = $name;
            $demo->email = $email;
            $demo->mobile = $phone;
            $demo->state = $state;
            $demo->city = $city;
            $demo->message = $message;
            $demo->product_id = $product_id;
            $demo->save();
            return $demo;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getDemos($from_date, $to_date, $paginate)
    {
        try {

            $query = Demo::query();
            $query->with('product');
            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
            }

            if (!is_null($paginate)) {
                return $query->orderBy('created_at', 'DESC')->paginate($paginate);
            }

            $res = $query->orderBy('created_at', 'DESC')->get();
            return $res;
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getDemo($id)
    {
        try {

            return Demo::find($id);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function deleteDemo($id)
    {
        try {

            return Demo::find($id)->delete();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }
} 