<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/26/14
 * Time: 4:00 PM
 */
class EloquentFeedbackRepository implements iFeedbackRepository
{
    public function createFeedback($email, $mobile, $category, $message)
    {
        try {
            $feedback = new Feedback();
            $feedback->email = $email;
            $feedback->mobile = $mobile;
            $feedback->category = $category;
            $feedback->message = $message;
            $feedback->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getFeedbacks($from_date, $to_date, $paginate)
    {
        try {
            $query = Feedback::query();

            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
            }
            if (!is_null($paginate)) {
                return $query->orderBy('created_at', 'DESC')->paginate($paginate);
            }
            $res = $query->orderBy('created_at', 'DESC')->get();
            return $res;
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function deleteFeedback($id)
    {
        Feedback::find($id)->delete();
    }
} 