<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/8/14
 * Time: 1:03 PM
 */
class EloquentDistributorsRepository implements iDistributorsRepository
{

    /**
     * @param $name
     * @param $address1
     * @param $address2
     * @param $city
     * @param $state
     * @param $pincode
     * @param $mobile
     * @param $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @throws Exception
     * @return Dealer
     */
    public function createDistributor($name,$email, $address1, $address2, $city, $state, $pincode, $mobile,$phone,$contact_person, $is_small_appliance, $is_large_appliance, $is_active,$sequence)
    {
        try {

            if ($sequence == 'top') {
                Distributor::where('sequence', '>', 1)->increment("sequence");
                $sequence = 1;
            } else if ($sequence == 'bottom') {
                $max_sequence = Distributor::max('sequence');
                $sequence = $max_sequence + 1;
            } else {
                $this->addOrUpdateSequence(null, $sequence, null);
                $sequence = $sequence + 1;
            }

            Distributor::where('sequence', '>=', $sequence)->increment('sequence', 10); //updates the sequence

            $distributor = new Distributor();
            $distributor->name = $name;
            $distributor->email_id = $email;
            $distributor->address1 = $address1;
            $distributor->address2 = $address2;
            $distributor->city = $city;
            $distributor->state = $state;
            $distributor->pincode = $pincode;
            $distributor->mobile = $mobile;
            $distributor->phone = $phone;
            $distributor->contact_person = $contact_person;
            $distributor->sequence = $sequence;
            $distributor->is_small_appliance = $is_small_appliance;
            $distributor->is_large_appliance = $is_large_appliance;
            $distributor->is_active = $is_active;
            $distributor->save();

            return $distributor;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /**
     * @param $city
     * @param $state
     * @param $paginate
     * @throws Exception
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getDistributors($city, $state,$is_active, $paginate)
    {
        try {

            $query = Distributor::query();
            if (DbUtil::checkDbNotNullValue($city)) {
                $query->where('city', '=', $city);
            } elseif (DbUtil::checkDbNullValue($city)) {
                $query->whereNull('city');
            }

            if (DbUtil::checkDbNotNullValue($state)) {
                $query->where('state', '=', $state);
            } elseif (DbUtil::checkDbNullValue($state)) {
                $query->whereNull('state');
            }

            if (DbUtil::checkDbNotNullValue($is_active)) {
                $query->where('is_active', '=', $is_active);
            } elseif (DbUtil::checkDbNullValue($is_active)) {
                $query->whereNull('is_active');
            }

            if (!is_null($paginate)) {
                return $query->orderBy('sequence')->paginate($paginate);
            }
            return $query->orderBy('sequence')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     * @throws Exception
     */
    public function getDistributor($id)
    {

        try {

            return Distributor::where('id', '=', $id)->first();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /**
     * @param $id
     * @param $name
     * @param $address1
     * @param $address2
     * @param $city
     * @param $state
     * @param $pincode
     * @param $mobile
     * @param $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @throws Exception
     */
    public function updateDistributor($id,$name,$email, $address1, $address2, $city, $state, $pincode, $mobile, $phone,$contact_person, $is_small_appliance, $is_large_appliance, $is_active,$sequence)
    {
        try {

            $distributor = Distributor::find($id);

            if ($sequence == 'top') {
                Distributor::where('sequence', '>=', 1)->increment("sequence");
                $sequence = 1;

                $distributor->sequence = $sequence;
                $distributor->save();
            } else if ($sequence == 'bottom') {
                $max_sequence = Distributor::max('sequence');
                $sequence = $max_sequence;

                Distributor::where('sequence', '>', 1)->decrement("sequence");

                $distributor->sequence = $sequence;
                $distributor->save();
            } else {
                $current_sequence = $distributor->sequence;
                $this->addOrUpdateSequence($current_sequence, (int)$sequence, $id);
//                $sequence = $sequence + 1;
            }

            $distributor->name = $name;
            $distributor->email_id = $email;
            $distributor->address1 = $address1;
            $distributor->address2 = $address2;
            $distributor->city = $city;
            $distributor->state = $state;
            $distributor->pincode = $pincode;
            $distributor->mobile = $mobile;
            $distributor->phone = $phone;
            $distributor->contact_person = $contact_person;
            $distributor->is_small_appliance = $is_small_appliance;
            $distributor->is_large_appliance = $is_large_appliance;
            $distributor->is_active = $is_active;
            $distributor->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** updates the appliance type
     * @param int $id dealer_id
     * @param string $type small|large
     * @param bool $status 0|1
     * @throws Exception
     */
    public function updateApplianceType($id, $type, $status)
    {
        try {

            $distributor = Distributor::find($id);
            if ($type == "small") {
                $distributor->is_small_appliance = $status;
            } else {
                $distributor->is_large_appliance = $status;
            }
            $distributor->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** accepts dealer_id and activate or deactivate the corresponding dealer
     * @param int $id dealer_id
     * @param bool $status 0|1
     * @throws Exception
     */
    public function activateOrDeactivate($id, $status)
    {
        try {

            $distributor = Distributor::find($id);
            $distributor->is_active = $status;
            $distributor->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function  addOrUpdateSequence($current_sequence, $after_sequence, $distributor_id)
    {
        if (is_null($current_sequence)) { //new product is added

            Distributor::where('sequence', '>', $after_sequence)->increment('sequence');
//            $product = Product::where('sequence', '=', $after_sequence)->first();
//            $product->sequence = $after_sequence + 1;
//            $product->save();

        } else {

            if ($current_sequence > $after_sequence) {
                if (DbUtil::checkDbNotNullValue($distributor_id)) {
                    $distributor = Distributor::where('sequence', '=', $current_sequence)->where('id', $distributor_id)->first();
                } elseif (DbUtil::checkDbNullValue($distributor_id)) {
                    $distributor = Distributor::where('sequence', '=', $current_sequence)->whereNull('id')->first();
                }
//                $dealer = Dealer::where('sequence', '=', $current_sequence)->first();
                //Product::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->get();
                Distributor::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->increment('sequence', 1);
                $distributor->sequence = $after_sequence + 1;
                $distributor->save();

            } else if ($current_sequence == $after_sequence) {
//                $product = Product::where('sequence', '=', $current_sequence)->first();
                if (DbUtil::checkDbNotNullValue($distributor_id)) {
                    $distributor = Distributor::where('sequence', '=', $current_sequence)->where('id', $distributor_id)->first();
                } elseif (DbUtil::checkDbNullValue($distributor_id)) {
                    $distributor = Distributor::where('sequence', '=', $current_sequence)->whereNull('id')->first();
                }

                //Product::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->get();
                Distributor::where("sequence", '>', $after_sequence)->where('sequence', '<', $current_sequence)->increment('sequence', 1);
                $distributor->sequence = $after_sequence + 1;
                $distributor->save();
            } else {
                if (DbUtil::checkDbNotNullValue($distributor_id)) {
                    $distributor = Distributor::where('sequence', '=', $current_sequence)->where('id', $distributor_id)->first();
                } elseif (DbUtil::checkDbNullValue($distributor_id)) {
                    $distributor = Distributor::where('sequence', '=', $current_sequence)->whereNull('id')->first();
                }
//                $dealer = Dealer::where('sequence', '=', $current_sequence)->first();
                Distributor::where('sequence', '>', $current_sequence)->where('sequence', '<=', $after_sequence)->decrement('sequence', 1);
                $distributor->sequence = $after_sequence;
                $distributor->save();
            }
        }


    }

    public function deleteDistributors($id = array())
    {
        Distributor::whereIn("id", $id)->delete();
    }

    public function getUniqueStates()
    {
        try {

            return Distributor::groupBy('state')->whereNotNull('state')->where('state', '!=', '')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getCitiesForState($state)
    {
        try {
            return Distributor::where("state", '=', $state)->groupBy("city")->get();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getSubcategoriesForCategories($category)
    {
        try {
            return Category::where("parent_category_id", '=', $category)->groupBy("name")->get();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getData($city, $state, $is_small_appliance, $is_large_appliance)
    {

        try {

            $query = Distributor::query();
            if (!empty($city)) {
                $query->where("city", '=', $city);
            }
            if (!empty($state)) {
                $query->where("state", '=', $state);
            }
            if ($is_large_appliance == true && $is_small_appliance == true || $is_large_appliance == false && $is_small_appliance == false) {
                $query->where("is_small_appliance", '=', $is_large_appliance)
                    ->where("is_large_appliance", '=', $is_small_appliance);

            } elseif ($is_large_appliance == true && $is_small_appliance == false) {
                $query->where("is_large_appliance", '=', true);

            } elseif ($is_large_appliance == false && $is_small_appliance == true) {
                $query->where("is_small_appliance", '=', true);
            }
            return $query->orderBy('sequence')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }


    }

} 