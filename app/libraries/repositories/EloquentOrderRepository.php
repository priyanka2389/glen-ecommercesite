<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 2/7/14
 * Time: 4:16 PM
 */
class EloquentOrderRepository implements iOrderRepository
{


    /** accepts order_info and order_item array, creates a new order and add the order items in order_items table
     * @param int $user_id
     * @param int|float $net_value
     * @param int|float $final_value
     * @param string $order_notes
     * @param array $reference
     * @param $cart_id
     * @param $payment_mode
     * @param $shipping_address_id
     * @param $billing_address_id
     * @param $verification_code
     * @param $coupon_id
     * @param array $order_items ['product_id','product_type','offer_price','list_price','qty','item_notes']
     * @throws Exception
     * @return Order
     */
    public function createOrder($user_id, $net_value, $final_value, $order_status, $payment_status, $order_notes, $reference, $cart_id,
                                $payment_mode, $shipping_address_id, $billing_address_id, $verification_code,
                                $coupon_id, $order_items = array())
    {
        try {

            DB::beginTransaction();
            $order = new Order();
            $order->user_id = $user_id;
            $order->net_value = $net_value;
            $order->final_value = $final_value;
            $order->status = $order_status;
            $order->payment_status = $payment_status;
            $order->notes = $order_notes;
            $order->reference = $reference;
            $order->cart_id = $cart_id;
            $order->payment_mode = $payment_mode;
            $order->shipping_address_id = $shipping_address_id;
            $order->billing_address_id = $billing_address_id;
            $order->verification_code = $verification_code;
            $order->coupon_id = $coupon_id;
            $order->save();

            $order_id = $order->id;


            foreach ($order_items as $single_item) {

                $item = new OrderItem();
                $item->order_id = $order_id;
                $item->item_id = $single_item['item_id'];
                $item->item_type = $single_item['item_type'];
                $item->offer_price = $single_item['offer_price'];
                $item->list_price = $single_item['list_price'];
                $item->qty = $single_item['qty'];
                $item->notes = isset($single_item['item_notes']) ? $single_item['item_notes'] : null;
                $item->save();
            }

            DB::commit();
            return $order;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }


    }

    /** accept order_id and returns the order info
     * @param int $id order_id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getOrder($id)
    {
        $order = Order::with('items', 'user')->where('id', '=', $id)->first();
        return $order;
    }

    /**
     * @param int $user_id
     * @param string $status accepts new,open,closed
     * @param string $payment_status accepts paid,unpaid
     * @param int $paginate
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Pagination\Paginator|static[]
     * @throws Exception
     */
    public function getOrders($user_id, $status, $payment_status, $payment_method, $from_date, $to_date, $paginate, $show_trash)
    {
        try {
            $query = Order::with('items', 'user');
            if (DbUtil::checkDbNotNullValue($user_id)) {
                $query->where('user_id', '=', $user_id);
            } elseif (DbUtil::checkDbNullValue($user_id)) {
                $query->whereNull('user_id');
            }

            if (DbUtil::checkDbNotNullValue($status)) {
                $query->whereIn('status', $status);
            } elseif (DbUtil::checkDbNullValue($status)) {
                $query->whereNull('status');
            }

            if (DbUtil::checkDbNotNullValue($payment_status)) {
                $query->whereIn('payment_status', $payment_status);
            } elseif (DbUtil::checkDbNullValue($payment_status)) {
                $query->whereNull('payment_status');
            }

            if (DbUtil::checkDbNotNullValue($payment_method)) {
                $query->whereIn('payment_mode', $payment_method);
            } elseif (DbUtil::checkDbNullValue($payment_method)) {
                $query->whereNull('payment_mode');
            }

            if ($show_trash == true) {
                $filter = 'deleted_by_admin';
            } else {
                $filter = 'created_at';
                $query->where('deleted_by_admin', '=', null);
            }

            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where($filter, '>=', $from_date)->where($filter, '<=', $to_date);
            }

            if (!is_null($paginate)) {
                return $query->orderBy($filter, 'desc')->paginate($paginate);
            }

            return $query->orderBy($filter, 'desc')->get();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getUpdatedOrders($user_id, $status, $payment_status, $payment_method, $from_date, $to_date, $paginate, $filterBy)
    {
        try {
            $query = Order::with('items', 'user');
            if (DbUtil::checkDbNotNullValue($user_id)) {
                $query->where('user_id', '=', $user_id);
            } elseif (DbUtil::checkDbNullValue($user_id)) {
                $query->whereNull('user_id');
            }

            if (DbUtil::checkDbNotNullValue($status)) {
                $query->whereIn('status', $status);
            } elseif (DbUtil::checkDbNullValue($status)) {
                $query->whereNull('status');
            }

            if (DbUtil::checkDbNotNullValue($payment_status)) {
                $query->whereIn('payment_status', $payment_status);
            } elseif (DbUtil::checkDbNullValue($payment_status)) {
                $query->whereNull('payment_status');
            }

            if (DbUtil::checkDbNotNullValue($payment_method)) {
                $query->whereIn('payment_mode', $payment_method);
            } elseif (DbUtil::checkDbNullValue($payment_method)) {
                $query->whereNull('payment_mode');
            }

            if ($filterBy == 'update') {
                $filter = 'updated_at';
                $query->where('deleted_by_admin', '=', null);
            }

            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where($filter, '>=', $from_date)->where($filter, '<=', $to_date);
            }

            if (!is_null($paginate)) {
                return $query->orderBy($filter, 'desc')->paginate($paginate);
            }

            return $query->orderBy($filter, 'desc')->get();
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }


    /**
     * @param $address_id
     * @param $type
     * @throws Exception
     * @internal param $order_id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getOrderAddress($address_id, $type)
    {
        try {

//            if ($type = 'shipping') {
//                return Address::where('id', '=', $address_id)->where('type', '=', "shipping")->withTrashed()->first();
//            } else {
            return Address::where('id', '=', $address_id)->withTrashed()->first();
//            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** accepts order id and updated the order status
     * @param int $id
     * @param  string $status
     * @throws Exception
     */
    public function updateStatus($id, $status)
    {
        try {

            $order = Order::find($id);
            $order->status = $status;
            $order->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** accepts order_id and updates the payment status
     * @param int $id
     * @param string $status
     * @param $notes
     * @throws Exception
     */
    public function updatePaymentStatus($id, $status, $notes)
    {
        try {

            $order = Order::find($id);
            $order->payment_status = $status;
            if (!is_null($notes)) {
                $order->notes = $notes;
            }
            $order->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function updateVerificationCode($id, $verification_code)
    {
        try {

            $order = Order::find($id);
            $order->verification_code = $verification_code;
            $order->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function updateCodVerificationStatus($order_id, $status)
    {
        try {

            $order = Order::find($order_id);
            $order->is_cod_verified = $status;
            $order->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function updateCouponId($order_id, $coupon_id)
    {
        try {

            $order = Order::find($order_id);
            $order->coupon_id = $coupon_id;
            $order->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** deletes the order by order_id
     * @param int $id order_id
     */
    public function deleteOrder($id)
    {
        Order::find($id)->delete();
    }

    public function getUpdateOrderByAdmin($order_id, $show_trash)
    {

        try {
            if ($show_trash == false) {
                $deleted_by_admin = null;
            } else {
                $deleted_by_admin = DB::raw('CURRENT_TIMESTAMP');
            }

            $order = Order::where("id", '=', $order_id)->first();
            $order->deleted_by_admin = $deleted_by_admin;
            $order->save();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getOnlinePendingOrders()
    {
        $status = array('new');
        $payment_status = array('pending');
        $payment_method = array('online');
        $show_trash = 0;
        $to_date = date("Y-m-d H:i:s");
        $from_date = date('Y-m-d H:i:s', strtotime($to_date . '-15 minutes'));

//        echo $from_date;        echo $to_date;
//        $orderRepo = new EloquentOrderRepository();
        $onlineOrders = $this->getOrders(null, $status, $payment_status, $payment_method, $from_date, $to_date, null, $show_trash);

        if(!empty($onlineOrders)){
            $orders = array();
            foreach($onlineOrders as $order){

                $userRepo = new EloquentUserRepository();
                $user = $userRepo->getUser($order->user_id, null, null);

                $user_name = $user->first_name . " " . $user->last_name;
                $mobile = $user->mobile;

                $order_array = array(
                    'order_no' => $order->id,
                    'reference_id' => $order->reference,
                    'user_name' => $user_name,
                    'mobile'=> $mobile,
                    'final_value' => $order->final_value,
                    'status' => $order->status,
                    'payment_status' => $order->payment_status,
                    'payment_mode' => $order->payment_mode,
                    'created_on' => $order->created_at
                );

                array_push($orders,$order_array);
            }

            return $orders;
        }else{
            return null;
        }

    }

    public function getToBeRefundedOrders()
    {
        $status = array('cancelled');
        $payment_status = array('to be refunded');
        $payment_method = array('online','cod');
        $show_trash = 0;
        $from_date = date("Y-m-d 00:00:00");
        $to_date = date('Y-m-d H:i:s');


        $refundedOrders = $this->getUpdatedOrders(null, $status, $payment_status, null, $from_date, $to_date, null, 'update');

        if(!empty($refundedOrders)){
            $orders = array();
            foreach($refundedOrders as $order){

                $userRepo = new EloquentUserRepository();
                $user = $userRepo->getUser($order->user_id, null, null);

                $user_name = $user->first_name . " " . $user->last_name;
                $mobile = $user->mobile;

                $order_array = array(
                    'order_no' => $order->id,
                    'reference_id' => $order->reference,
                    'user_name' => $user_name,
                    'mobile'=> $mobile,
                    'final_value' => $order->final_value,
                    'status' => $order->status,
                    'payment_status' => $order->payment_status,
                    'payment_mode' => $order->payment_mode,
                    'created_on' => $order->created_at
                );

                array_push($orders,$order_array);
            }

            return $orders;
        }else{
            return null;
        }

    }
}