<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/14/14
 * Time: 4:15 PM
 */
class EloquentEnquiryRepository implements iEnquiryRepository
{

    public function createEnquiry($name, $company_name, $email, $phone,$country, $state, $city, $requirement,
                                  $interest, $purpose, $message, $enquiry_type)
    {

        try {

            $enquiry = new Enquiry();
            $enquiry->name = $name;
            $enquiry->company_name = $company_name;
            $enquiry->email = $email;
            $enquiry->phone = $phone;
            $enquiry->country = $country;
            $enquiry->state = $state;
            $enquiry->city = $city;
            $enquiry->requirement = $requirement;
            $enquiry->interest = $interest;
            $enquiry->purpose = $purpose;
            $enquiry->message = $message;
            $enquiry->enquiry_type = $enquiry_type;
            $enquiry->save();
            return $enquiry;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getEnquires($from_date, $to_date, $enquiry_type, $paginate)
    {

        try {

            $query = Enquiry::query();
            if (!is_null($from_date) && !is_null($to_date)) {
                $query->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
            }

            if (!is_null($paginate)) {

                return $query->orderBy('created_at')->paginate($paginate);
            }

            return $query->orderBy('created_at')->get();

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getEnquiry($id)
    {
        try {

            return Enquiry::find($id);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function deleteEnquiry($id)
    {
        Enquiry::find($id)->delete();

    }
} 