<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 12/30/13
 * Time: 12:45 PM
 */
class Constants
{

    const SUCCESS_CODE = 200;
    const NULL_VALUE = -98765432109;
    const PAGE_COUNT = 100;
    const BATCH_SIZE = 500;
    const PRICE_RANGE = 1000;

    const PRODUCTS_PAGE_COUNT = 9;
    const DASHBOARD_PRODUCTS_PAGE_COUNT = 100;
    const DASHBOARD_ORDERS_PAGE_COUNT = 100;
    const DASHBOARD_COUPONS_PAGE_COUNT = 100;
    const DASHBOARD_CUSTOMERS_PAGE_COUNT = 100;
    const DASHBOARD_DEALERS_PAGE_COUNT = 100;
    const DASHBOARD_DISTRIBUTORS_PAGE_COUNT = 100;
    const DASHBOARD_PAGE_COUNT = 100;
    const DASHBOARD_RETAILSTORES_PAGE_COUNT = 100;
    const DASHBOARD_SERVICECENTERS_PAGE_COUNT = 100;

    const CATEGORIES_PAGE_COUNT = 10;
    const DASHBOARD_CATEGORIES_PAGE_COUNT = 50;

    const DASHBOARD_TAGS_PAGE_COUNT = 20;

    const COMBOS_PAGE_COUNT = 10;
    const FILTER_PAGE_COUNT = 10;

    const PAGE_DATA_PAGE_COUNT = 20;
    const FILE_HASH_ALGO = "md5";
    const RELATED_PRODUCTS_LIMIT = 12;
    const RELATED_COMBOS_LIMIT = 12;

    const DASHBOARD_ENQUIRES_PAGE_COUNT = 100;
    const DASHBOARD_DEMO_PAGE_COUNT = 100;
    const DASHBOARD_ENQUIRY_PAGE_COUNT = 100;
    const DASHBOARD_REPORT_PAGE_COUNT = 100;
    const DASHBOARD_MESSAGE_PAGE_COUNT = 100;
    const DASHBOARD_CALL_PAGE_COUNT = 100;
    const DASHBOARD_MAIL_PAGE_COUNT = 100;


//    uploads paths
    const PRODUCT_DOCUMENTS_UPLOAD_PATH = "uploads/product/documents";
    const PRODUCT_VIDEOS_UPLOAD_PATH = "uploads/product/videos";
    const DEALER_LIST_UPLOAD_PATH = "uploads/dealer";

    const PRODUCT_IMAGE_ORG_UPLOAD_PATH = "uploads/product/img/org/";
    const PRODUCT_IMAGE_300_UPLOAD_PATH = "uploads/product/img/300/";
    const PRODUCT_IMAGE_600_UPLOAD_PATH = "uploads/product/img/600/";


    const CATEGORY_DOCUMENTS_UPLOAD_PATH = "uploads/category/documents";
    const CATEGORY_VIDEOS_UPLOAD_PATH = "uploads/category/videos";

    const DEFAULT_300_IMAGE = "uploads/product/img/300/default.jpg";
    const DEFAULT_600_IMAGE = "uploads/product/img/600/default.jpg";

    const RESUME_UPLOAD_PATH = 'uploads/resume';

//image size related constants


} 