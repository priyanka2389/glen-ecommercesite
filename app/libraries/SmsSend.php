<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/14/14
 * Time: 11:07 AM
 */
class SmsSend
{

    /** accepts array data having sender_id,mobile,message
     * @param array $sms
     * @return bool
     */
    public static function sendMessages($sms = array())
    {

        $username = Config::get('sms.username');
        $password = Config::get('sms.password');

        $request = ""; //initialise the request variable

        $param['method'] = "sendMessage";
        $param['send_to'] = $sms['mobile'];
        $param['msg'] = $sms['message'];
        $param['msg_type'] = "TEXT"; //Can be "FLASH”/"UNICODE_TEXT"/”BINARY”
        $param['userid'] = $username;
        $param['auth_scheme'] = "PLAIN";
        $param['password'] = $password;
        $param['v'] = "1.1";
        $param['format'] = "text";
//Have to URL encode the values
        foreach ($param as $key => $val) {
            $request .= $key . "=" . $val;
            //we have to urlencode the values
            $request .= "&";
            //append the ampersand (&) sign after each  parameter/value pair
        }

        $request = substr($request, 0, strlen($request) - 1);
        //remove final (&) sign from the request
        $url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?" . $request;

        ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        ini_set('allow_url_fopen ', 1); //300 seconds = 5 minutes
        //$res = file_get_contents($url);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);


        $credentials = "username:password";

        // Read the XML to send to the Web Service
//        $request_file = "./SampleRequest.xml";
//        $fh = fopen($request_file, 'r');
//        $xml_data = fread($fh, filesize($request_file));
//        fclose($fh);

//        $url = "http://www.example.com/services/calculation";
//        $page = "/services/calculation";
//        $headers = array(
//            "POST ".$request." HTTP/1.0",
//            "Content-type: text/xml;charset=\"utf-8\"",
//            "Accept: text/xml",
//            "Cache-Control: no-cache",
//            "Pragma: no-cache",
//            "SOAPAction: \"run\"",
//            "Content-length: ".strlen($request),
////            "Authorization: Basic " . base64_encode($credentials)
//        );

//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL,$url);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt ($ch, CURLOPT_SSLCERT, $key);
//        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
//        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
//        curl_setopt($ch, CURLOPT_USERAGENT, $defined_vars['HTTP_USER_AGENT']);

        // Apply the XML to our curl call
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);

//        $data = curl_exec($ch);
//
//        if (curl_errno($ch)) {
//            print "Error: " . curl_error($ch);
//            exit;
//        } else {
//            // Show me the result
//            var_dump($data);
//            curl_close($ch);
//        }


    }

} 