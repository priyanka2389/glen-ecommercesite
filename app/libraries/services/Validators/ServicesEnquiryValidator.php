<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/15/14
 * Time: 11:24 AM
 */
namespace services\Validators;

class ServicesEnquiryValidator extends Validator
{
    public static $rules = array(

        'name' => 'required|regex:/^[a-zA-Z ]*$/',
        'email' => 'required|email|regex:/^(?:\w+[\.])*\w+@(?:\w+[\.])*\w+\.\w+$/',
        'mobile' => 'required|numeric|digits:10',
        'address' => 'required|regex:/^[a-zA-Z0-9-_.,\/\s]*$/',
        'product' => 'required|regex:/^[a-zA-Z0-9 ]*$/',
        'bill_no' => 'required|regex:/^[a-zA-Z0-9-_.\/\s]*$/',
        'bill_date' => 'required',
        'shop_name' => 'required|regex:/^[a-zA-Z0-9-_.\/\s]*$/',
        'message' => 'required|regex:/^[a-zA-Z0-9-_.\/\s]*$/',
//        'captcha' => 'required|captcha'
    );
}