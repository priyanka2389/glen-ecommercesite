<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/15/14
 * Time: 11:23 AM
 */

namespace services\Validators;

class RequestCallValidator extends Validator
{

    public static $rules = array(
        'name' => 'required|regex:/^[a-zA-Z ]*$/',
        'mobile' => 'required|numeric|digits:10',
        'email' => 'email|regex:/^(?:\w+[\.])*\w+@(?:\w+[\.])*\w+\.\w+$/',
        'city' => 'required|regex:/^[a-zA-Z ]*$/',
    );


} 