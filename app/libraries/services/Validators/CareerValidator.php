<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/15/14
 * Time: 11:23 AM
 */

namespace services\Validators;


class CareerValidator extends Validator
{

    public static $rules = array(
        'name'=>'required|regex:/^[a-zA-Z ]*$/',
        'email' => 'required|email|regex:/^(?:\w+[\.])*\w+@(?:\w+[\.])*\w+\.\w+$/',
        'mobile' => 'required|numeric|digits:10',
        'address' => 'required|regex:/^[a-zA-Z0-9-_.,\/\s]*$/',
        'dob' => 'required',
        'applying_department' => 'required|regex:/^[a-zA-Z-_.\/\s]*$/',
        'educational_qualification' => 'required|regex:/^[a-zA-Z-_.\s]*$/',
        'professional_qualification' => 'required|regex:/^[a-zA-Z-_.\s]*$/',
        'primary_skill' => 'required|regex:/^[a-zA-Z-_.\/\s]*$/',
        'career_highlights' => 'required|regex:/^[a-zA-Z-_.\/\s]*$/',
        'work_exp' => 'required|regex:/^[a-zA-Z0-9\s]*$/',
        'resume'=>'required|mimes:doc,txt,docx,ppt,pdf',
//        'captcha' => 'required|captcha'
    );

} 