<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/15/14
 * Time: 11:23 AM
 */

namespace services\Validators;

use Mews\Captcha\Captcha;

class InstitutionalPurchaseEnquiryValidator extends Validator
{

    public static $rules = array(

        'name' => 'required|regex:/^[a-zA-Z ]*$/',
        'company_name' => 'required|regex:/^[a-zA-Z-_.\/\s]*$/',
        'email' => 'required|email|regex:/^(?:\w+[\.])*\w+@(?:\w+[\.])*\w+\.\w+$/',
        'mobile' => 'required|numeric|digits:10',
        'country' => 'required',
        'state' => 'required',
        'city' => 'required|regex:/^[a-zA-Z ]*$/',
        'requirement' => 'required|regex:/^[a-zA-Z0-9 ]*$/',
        'message' => 'required',
//        'captcha' => 'required|captcha'
    );


} 