<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/22/14
 * Time: 3:50 PM
 */

namespace services\Validators;

class PageValidator extends Validator
{

    public static $rules = array(
        'title' => 'required|unique:page,title',
        'short_code' => 'required|unique:page,short_code',
        'source_code' => 'required',
        'active' => 'required'
    );

} 