<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/23/14
 * Time: 4:58 PM
 */

namespace services\Validators;

class RetailStoresValidator extends Validator
{

    public static $rules = array(
//        'name' => 'required',
        'email' => 'email',
        'address1' => 'required',
        'city' => 'required',
        'state' => 'required',
        'pincode' => 'numeric',
//        'mobile' => 'numeric',
        'sequence' => 'required',
    );

} 