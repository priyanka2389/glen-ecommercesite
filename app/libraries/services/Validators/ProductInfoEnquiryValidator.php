<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/15/14
 * Time: 11:24 AM
 */

namespace services\Validators;


class ProductInfoEnquiryValidator extends Validator
{

    public static $rules = array(
        'name'=>'required|regex:/^[a-zA-Z ]*$/',
        'email'=>'required|email|regex:/^(?:\w+[\.])*\w+@(?:\w+[\.])*\w+\.\w+$/',
        'mobile' => 'required|numeric|digits:10',
        'country' => 'required',
        'state' => 'required',
        'city'=>'required|regex:/^[a-zA-Z ]*$/',
        'message'=>'required',
//        'captcha' => 'required|captcha'
    );
} 