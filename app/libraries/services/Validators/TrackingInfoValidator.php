<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/22/14
 * Time: 3:50 PM
 */

namespace services\Validators;

class TrackingInfoValidator extends Validator
{

    public static $rules = array(

        'provider' => 'required',
        'code' => 'required',
        'tracking_url' => 'required'
    );

} 