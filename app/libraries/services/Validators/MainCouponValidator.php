<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/22/14
 * Time: 3:50 PM
 */

namespace services\Validators;

class MainCouponValidator extends Validator
{

    public static $rules = array(

        'name' => 'required',
        'percentage' => 'required|numeric',
        'min_value' => 'required|numeric',
        'expiry_date' => 'required',
        'unique' => 'required',
        'category_id' => 'required',
        'coupon_qty' => 'required_if:coupon_code,null|numeric',
        'coupon_code' => 'required_if:coupon_qty,0'
    );

}
