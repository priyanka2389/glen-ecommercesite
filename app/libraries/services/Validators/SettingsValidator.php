<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 2/5/14
 * Time: 11:17 AM
 */

namespace services\Validators;


class SettingsValidator extends Validator
{
    public static $rules = array(
        'setting_label' => 'required',
        'setting_value' => 'required'
    );

}