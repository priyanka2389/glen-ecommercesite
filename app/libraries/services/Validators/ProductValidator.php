<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/22/14
 * Time: 3:50 PM
 */

namespace services\Validators;

class ProductValidator extends Validator
{

    public static $rules = array(

        'code' => 'required|unique:products,code',
        'shortcode' => 'required|unique:products,shortcode',
        'sap_code' => 'required|unique:products,sap_code',
        'description' => 'required',
        'category' => 'required',
        'active' => 'required',
        'list_price' => 'required||numeric|required_if:offer_price,> 0',
        'offer_price' => 'numeric',
        'ltw' => 'required_if:warranty,null',
        'warranty' => 'required_if:ltw,0|numeric',
//        'warranty' => 'numeric',
        'sequence' => 'required',
        'available' => 'required'
    );

} 