<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 9/15/14
 * Time: 4:54 PM
 */
class CallService
{
    function __construct(iCallRepository $callRepository)
    {
        $this->callRepository = $callRepository;
    }

    public function createCall($name,$mobile,$email)
    {
        $call = $this->callRepository->createCall($name,$mobile,$email);
        return $call;
    }

    public function getCalls($from_date, $to_date, $paginate)
    {
        $calls = $this->callRepository->getCalls($from_date, $to_date, $paginate);
        return AppUtil::returnResults($calls);
    }

//    public function getDemo($id)
//    {
//        $demo = $this->demoRepository->getDemo($id);
//        return isset($demo) ? $demo : null;
//    }

    public function deleteCall($id)
    {
        $this->callRepository->deleteCall($id);
    }
//
    public function geCallCsv($data)
    {

        $header = "Name,Email,Mobile,Created At\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->name)) {
                    $string = $string . "\"$row->name\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->email)) {
                    $string = $string . "\"$row->email\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->mobile)) {
                    $string = $string . "\"$row->mobile\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }

                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . "\r\n";
                }
            }
        }
        $filename = "Calls.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }

}