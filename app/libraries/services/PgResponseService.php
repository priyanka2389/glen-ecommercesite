<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/7/14
 * Time: 1:06 PM
 */
class PgResponseService
{

    function __construct(EloquentPgResponseRepository $pgResponseRepo)
    {
        $this->pgResponseRepo = $pgResponseRepo;
    }

    /** adds payment gateway response
     * @param $amount
     * @param $batch_no
     * @param $command
     * @param $locale
     * @param $merchant_txn_ref
     * @param $merchant
     * @param $message
     * @param $order_info
     * @param $transaction_no
     * @param $txn_response_code
     * @param $version
     * @param $additional_notes
     * @return PgResponse
     * @throws Exception
     */
    public function addResponse($amount, $batch_no, $command, $locale, $merchant_txn_ref, $merchant, $message, $order_info, $transaction_no, $txn_response_code, $version, $additional_notes)
    {
        try {

            return $this->pgResponseRepo->addResponse($amount, $batch_no, $command, $locale, $merchant_txn_ref,
                $merchant, $message, $order_info, $transaction_no, $txn_response_code, $version,
                $additional_notes);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** get payment gateway response by id,merchant_txn_ref,order_info
     * @param $id
     * @param $merchant_tax_ref
     * @param $order_info
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws Exception
     */
    public function getResponse($id, $merchant_tax_ref, $order_info)
    {
        try {

            $result = $this->pgResponseRepo->getResponse($id, $merchant_tax_ref, $order_info);
            return AppUtil::returnResults($result);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    public function updatePaymentStatus($order_id,$payment_status){
        try {
            $this->pgResponseRepo->updatePaymentStatus($order_id,$payment_status);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

} 