<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 9/15/14
 * Time: 4:54 PM
 */
class MessageService
{
    function __construct(iMessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    public function createMessage($name, $email, $friend_name, $friend_email, $message, $product_link)
    {
        $message = $this->messageRepository->createMessage($name, $email, $friend_name, $friend_email, $message, $product_link);
        return $message;
    }

    public function getMessages($from_date, $to_date, $paginate)
    {
        $messages = $this->messageRepository->getMessages($from_date, $to_date, $paginate);
        return AppUtil::returnResults($messages);
    }
//
//    public function getDemo($id)
//    {
//        $demo = $this->demoRepository->getDemo($id);
//        return isset($demo) ? $demo : null;
//    }
//
    public function deleteMessage($id)
    {
        $this->messageRepository->deleteMessage($id);
    }
//
    public function getMessageCsv($data)
    {

        $header = "User Name,User Email,Friend Name,Friend Email,Message,Link,Created At\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->name)) {
                    $string = $string . "\"$row->name\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->email)) {
                    $string = $string . "\"$row->email\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->friend_name)) {
                    $string = $string . "\"$row->friend_name\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->friend_email)) {
                    $string = $string . "\"$row->friend_email\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->message)) {
                    $string = $string . "\"$row->message\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->link)) {
                    $string = $string . "\"$row->link\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . "\r\n";
                }
            }
        }
        $filename = "Messages.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }

}