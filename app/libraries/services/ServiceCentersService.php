<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/8/14
 * Time: 1:04 PM
 */
class ServicesCentersService
{


    function __construct(iServicesCentersRepository $servicecentersRepo)
    {
        $this->serviceCentersRepository  = $servicecentersRepo;
    }

    /** creates a new ServiceCenter
     * @param string $name
     * @param string $address1
     * @param string $address2
     * @param string $city
     * @param string $state
     * @param int $pincode
     * @param int $mobile
     * @param int $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @return mixed
     */
    public function createServiceCenter($name,$email, $address1, $address2, $city, $state, $pincode, $mobile,
                                 $phone, $is_small_appliance, $is_large_appliance, $is_active,$sequence,$contact_type,$contact_person)
    {
        $dealer = $this->serviceCentersRepository->createServiceCenter($name,$email, $address1, $address2, $city, $state, $pincode, $mobile,
            $phone, $is_small_appliance, $is_large_appliance, $is_active,$sequence,$contact_type,$contact_person);
        return $dealer;
    }

    /**return ServiceCenters by city or state
     * @param string $city
     * @param string $state
     * @param $paginate
     * @return null
     */
    public function getServiceCenters($city, $state, $paginate)
    {
        $dealers = $this->serviceCentersRepository->getServiceCenters($city, $state, $paginate);
        return AppUtil::returnResults($dealers);
    }

    /** return a ServiceCenter by dealer id
     * @param int $id
     * @return null
     */
    public function getServiceCenter($id)
    {
        $dealer = $this->serviceCentersRepository->getServiceCenter($id);
        return isset($dealer) ? $dealer : null;

    }

    /** updates a ServiceCenter info
     * @param int $id
     * @param string $name
     * @param string $address1
     * @param string $address2
     * @param string $city
     * @param string $state
     * @param int $pincode
     * @param int $mobile
     * @param int $phone
     * @param bool $is_small_appliance
     * @param bool $is_large_appliance
     * @param bool $is_active
     */
    public function updateServiceCenter($id,$email, $name, $address1, $address2, $city, $state, $pincode, $mobile,
                                 $phone, $is_small_appliance, $is_large_appliance, $is_active,$sequence,$contact_type,$contact_person)
    {
        $this->serviceCentersRepository->updateServiceCenter($id,$email, $name, $address1, $address2, $city, $state, $pincode, $mobile,
            $phone, $is_small_appliance, $is_large_appliance, $is_active,$sequence,$contact_type,$contact_person);
    }


    /** updates the appliance type
     * @param int $id dealer_id
     * @param string $type small|large
     * @param bool $status 0|1
     */
    public function updateApplianceType($id, $type, $status)
    {
        $this->serviceCentersRepository->updateApplianceType($id, $type, $status);
    }

    /** accepts dealer_id and activate or deactivate the corresponding dealer
     * @param int $id dealer_id
     * @param bool $status 0|1
     */
    public function activateOrDeactivate($id, $status)
    {
        $this->serviceCentersRepository->activateOrDeactivate($id, $status);
    }


    /** accept dealer id and deletes the dealers
     * @param array $id
     */
    public function deleteServiceCenters($id = array())
    {
        $this->serviceCentersRepository->deleteServiceCenters($id);
    }

    /** returns all the unique states
     * @return mixed
     */
    public function getUniqueStates()
    {
        $states = $this->serviceCentersRepository->getUniqueStates();
        return AppUtil::returnResults($states);
    }

    /** accepts the state name and return the cities for that corresponding state
     * @param string $state name of the state
     * @return mixed
     */
    public function getCitiesForState($state)
    {
        return $this->serviceCentersRepository->getCitiesForState($state);
    }

    /** accepts the state name and return the cities for that corresponding state
     * @param string $state name of the state
     * @return mixed
     */
    public function getSubcategoriesForCategories($category)
    {
        return $this->serviceCentersRepository->getSubcategoriesForCategories($category);
    }

    /** accepts city,state,is_small_appliance or is_large appliance and return all the dealers
     * @param string $city
     * @param string $state
     * @param bool $is_small_appliance
     * @param bool $is_large_appliance
     * @return mixed
     */
    public function getData($city, $state, $is_small_appliance, $is_large_appliance)
    {
        $data = $this->serviceCentersRepository->getData($city, $state, $is_small_appliance, $is_large_appliance);
        return AppUtil::returnResults($data);
    }

    /** accepts dealer_id and returns the position of the corresponding dealer
     * @param $id
     * @return mixed|null|string
     */
    public function getServiceCenterPosition($id)
    {
        $servicecenter = $this->getServiceCenter($id);
        $sequence = $servicecenter->sequence;
        if (isset($sequence)) {
            $max_sequence = ServiceCenters::max('sequence');
            $min_sequence = ServiceCenters::min('sequence');

            $after = ServiceCenters::where('sequence', '<', $sequence)->orderBy('sequence', 'desc')->first();
            $after_id = isset($after) ? $after->id : null;

            if ($sequence == $min_sequence) {
                return 'top';
            } elseif ($sequence == $max_sequence) {
                return 'bottom';
            } else {
                return $after_id;
            }
        } else {
            return null;
        }

    }

} 