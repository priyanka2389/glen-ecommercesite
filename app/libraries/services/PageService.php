<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 9/15/14
 * Time: 4:54 PM
 */
class PageService
{

    function __construct(iPageRepository $pageRepo)
    {
        $this->pageRepo = $pageRepo;
    }

    /** creates a new dealer
     * @param string $name
     * @param string $address1
     * @param string $address2
     * @param string $city
     * @param string $state
     * @param int $pincode
     * @param int $mobile
     * @param int $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @return mixed
     */
    public function createPage($title,$short_code,$source_code,$active)
    {
        $dealer = $this->pageRepo->createPage($title,$short_code,$source_code,$active);
        return $dealer;
    }

    /**return dealers by city or state
     * @param string $city
     * @param string $state
     * @param $paginate
     * @return null
     */
    public function getPages()
    {
        $pages = $this->pageRepo->getPages();
        return AppUtil::returnResults($pages);
    }

    /** return a dealer by dealer id
     * @param int $id
     * @return null
     */
    public function getPage($id,$short_code,$is_active)
    {
        $page = $this->pageRepo->getPage($id,$short_code,$is_active);
        return isset($page) ? $page : null;

    }

    /** updates a dealer info
     * @param int $id
     * @param string $name
     * @param string $address1
     * @param string $address2
     * @param string $city
     * @param string $state
     * @param int $pincode
     * @param int $mobile
     * @param int $phone
     * @param bool $is_small_appliance
     * @param bool $is_large_appliance
     * @param bool $is_active
     */
    public function updatePage($id,$title, $short_code, $source_code, $active)
    {
        $this->pageRepo->updatePage($id,$title, $short_code, $source_code, $active);
    }


    /** accepts dealer_id and activate or deactivate the corresponding dealer
     * @param int $id dealer_id
     * @param bool $status 0|1
     */
    public function activateOrDeactivate($id, $status)
    {
        $this->pageRepo->activateOrDeactivate($id, $status);
    }


    /** accept dealer id and deletes the dealers
     * @param array $id
     */
    public function deletePages($id = array())
    {
        $this->pageRepo->deletePages($id);
    }

}