<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/8/14
 * Time: 1:04 PM
 */
class DistributorsService
{


    function __construct(iDistributorsRepository $distributorsRepo)
    {
        $this->distributorsRepo = $distributorsRepo;
    }

    /** creates a new dealer
     * @param string $name
     * @param string $address1
     * @param string $address2
     * @param string $city
     * @param string $state
     * @param int $pincode
     * @param int $mobile
     * @param int $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @return mixed
     */
    public function createDistributor($shop_name,$email, $address1, $address2, $city, $state, $pincode, $mobile,
                                      $phone,$contact_person, $small_appliance, $large_appliance, $active,$sequence)
    {
        $dealer = $this->distributorsRepo->createDistributor($shop_name,$email, $address1, $address2, $city, $state, $pincode, $mobile,
            $phone,$contact_person, $small_appliance, $large_appliance, $active,$sequence);
        return $dealer;
    }

    /**return dealers by city or state
     * @param string $city
     * @param string $state
     * @param $paginate
     * @return null
     */
    public function getDistributors($city, $state,$is_active, $paginate)
    {
        $distributors = $this->distributorsRepo->getDistributors($city, $state,$is_active, $paginate);
        return AppUtil::returnResults($distributors);
    }

    /** return a dealer by dealer id
     * @param int $id
     * @return null
     */
    public function getDistributor($id)
    {
        $dealer = $this->distributorsRepo->getDistributor($id);
        return isset($dealer) ? $dealer : null;

    }

    /** updates a dealer info
     * @param int $id
     * @param string $name
     * @param string $address1
     * @param string $address2
     * @param string $city
     * @param string $state
     * @param int $pincode
     * @param int $mobile
     * @param int $phone
     * @param bool $is_small_appliance
     * @param bool $is_large_appliance
     * @param bool $is_active
     */
    public function updateDistributor($id, $shop_name,$email, $address1, $address2, $city, $state, $pincode, $mobile,
                                      $phone,$contact_person, $small_appliance, $large_appliance, $active,$sequence)
    {
        $this->distributorsRepo->updateDistributor($id,$shop_name,$email, $address1, $address2, $city, $state, $pincode, $mobile,
            $phone,$contact_person, $small_appliance, $large_appliance, $active,$sequence);
    }


    /** updates the appliance type
     * @param int $id dealer_id
     * @param string $type small|large
     * @param bool $status 0|1
     */
    public function updateApplianceType($id, $type, $status)
    {
        $this->distributorsRepo->updateApplianceType($id, $type, $status);
    }

    /** accepts dealer_id and activate or deactivate the corresponding dealer
     * @param int $id dealer_id
     * @param bool $status 0|1
     */
    public function activateOrDeactivate($id, $status)
    {
        $this->distributorsRepo->activateOrDeactivate($id, $status);
    }


    /** accept dealer id and deletes the dealers
     * @param array $id
     */
    public function deleteDistributors($id = array())
    {
        $this->distributorsRepo->deleteDistributors($id);
    }

    /** returns all the unique states
     * @return mixed
     */
    public function getUniqueStates()
    {
        $states = $this->distributorsRepo->getUniqueStates();
        return AppUtil::returnResults($states);
    }

    /** accepts the state name and return the cities for that corresponding state
     * @param string $state name of the state
     * @return mixed
     */
    public function getCitiesForState($state)
    {
        return $this->distributorsRepo->getCitiesForState($state);
    }

    /** accepts the state name and return the cities for that corresponding state
     * @param string $state name of the state
     * @return mixed
     */
    public function getSubcategoriesForCategories($category)
    {
        return $this->distributorsRepo->getSubcategoriesForCategories($category);
    }

    /** accepts city,state,is_small_appliance or is_large appliance and return all the dealers
     * @param string $city
     * @param string $state
     * @param bool $is_small_appliance
     * @param bool $is_large_appliance
     * @return mixed
     */
    public function getData($city, $state, $is_small_appliance, $is_large_appliance)
    {
        $data = $this->distributorsRepo->getData($city, $state, $is_small_appliance, $is_large_appliance);
        return AppUtil::returnResults($data);
    }

    /** accepts dealer_id and returns the position of the corresponding dealer
     * @param $id
     * @return mixed|null|string
     */
    public function getDistributorsPosition($id)
    {
        $distributor = $this->getDistributor($id);
        $sequence = $distributor->sequence;
        if (isset($sequence)) {
            $max_sequence = Distributor::max('sequence');
            $min_sequence = Distributor::min('sequence');

            $after = Distributor::where('sequence', '<', $sequence)->orderBy('sequence', 'desc')->first();
            $after_id = isset($after) ? $after->id : null;

            if ($sequence == $min_sequence) {
                return 'top';
            } elseif ($sequence == $max_sequence) {
                return 'bottom';
            } else {
                return $after_id;
            }
        } else {
            return null;
        }

    }

} 