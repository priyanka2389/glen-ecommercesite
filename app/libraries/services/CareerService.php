<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/14/14
 * Time: 3:55 PM
 */
class CareerService
{

    function __construct(iCareerRepository $careerRepository)
    {
        $this->careerRepository = $careerRepository;
    }

    public function addCareer($name, $email, $phone, $address, $dob, $applying_department, $educational_qualification,
                              $professional_qualification, $primary_skill, $career_highlights, $work_exp, $resume_path)
    {

        $this->careerRepository->addCareer($name, $email, $phone, $address, $dob, $applying_department,
            $educational_qualification, $professional_qualification, $primary_skill, $career_highlights,
            $work_exp, $resume_path);

    }

    public function getCareers($from_date, $to_date, $paginate)
    {
        $careers = $this->careerRepository->getCareers($from_date, $to_date, $paginate);
        return AppUtil::returnResults($careers);
    }

    public function getCareer($id)
    {
        $career = $this->careerRepository->getCareer($id);
        return isset($career) ? $career : null;
    }

    public function deleteCareer($id)
    {
        $this->careerRepository->deleteCareer($id);
    }

    public function getCareersCsv($data)
    {
        $header = "Name,Email,Phone,Address,Date Of Birth,Applying Department,Educational Qualifications,
        Professional Qualifications,Primary Skill,Career Highlights,Work Exp,Resume,Create At\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->name)) {
                    $string = $string . "\"$row->name\"" . ",";
                }
                if (isset($row->email)) {
                    $string = $string . "\"$row->email\"" . ",";
                }
                if (isset($row->phone)) {
                    $string = $string . "\"$row->phone\"" . ",";
                }
                if (isset($row->address)) {
                    $string = $string . "\"$row->address\"" . ",";
                }
                if (isset($row->dob)) {
                    $string = $string . "\"$row->dob\"" . ",";
                }
                if (isset($row->applying_department)) {
                    $string = $string . "\"$row->applying_department\"" . ",";
                }
                if (isset($row->educational_qualification)) {
                    $string = $string . "\"$row->educational_qualification\"" . ",";
                }
                if (isset($row->professional_qualification)) {
                    $string = $string . "\"$row->professional_qualification\"" . ",";
                }
                if (isset($row->primary_skill)) {
                    $string = $string . "\"$row->primary_skill\"" . ",";
                }
                if (isset($row->career_highlights)) {
                    $string = $string . "\"$row->career_highlights\"" . ",";
                }
                if (isset($row->work_exp)) {
                    $string = $string . "\"$row->work_exp\"" . ",";
                }
                if (isset($row->resume_path)) {
                    $string = $string . "\"$row->resume_path\"" . ",";
                }
                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . "\r\n";
                }
            }
        }
        $filename = "Careers.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }

} 