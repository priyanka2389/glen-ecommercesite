<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/14/14
 * Time: 4:38 PM
 */
class EnquiryService
{

    function __construct(iEnquiryRepository $enquiryRepository)
    {
        $this->enquiry = $enquiryRepository;
    }

    public function createEnquiry($name, $company_name, $email, $phone, $country,$state, $city, $requirement,
                                  $interest, $purpose, $message, $enquiry_type)
    {
        $this->enquiry->createEnquiry($name, $company_name, $email, $phone,$country, $state, $city, $requirement,
            $interest, $purpose, $message, $enquiry_type);
    }

    public function getEnquires($from_date, $to_date, $enquiry_type, $paginate)
    {
        $enquires = $this->enquiry->getEnquires($from_date, $to_date, $enquiry_type, $paginate);
        return AppUtil::returnResults($enquires);
    }

    public function getEnquiry($id)
    {
        $enquiry = $this->enquiry->getEnquiry($id);
        return isset($enquiry) ? $enquiry : null;
    }

    public function deleteEnquiry($id)
    {
        $this->enquiry->deleteEnquiry($id);
    }

    public function getBulkPurchaseCsv($data)
    {

        $header = "Name,Email,Phone,City,Requirement,Message,Created At\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->name)) {
                    $string = $string . "\"$row->name\"" . ",";
                }
                if (isset($row->email)) {
                    $string = $string . "\"$row->email\"" . ",";
                }
                if (isset($row->phone)) {
                    $string = $string . "\"$row->phone\"" . ",";
                }
                if (isset($row->city)) {
                    $string = $string . "\"$row->city\"" . ",";
                }
                if (isset($row->requirement)) {
                    $string = $string . "\"$row->requirement\"" . ",";
                }
                if (isset($row->message)) {
                    $string = $string . "\"$row->message\"" . ",";
                }
                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . "\r\n";
                }
            }
        }
        $filename = "BulkEnquires.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }


    public function getTradePartnerCsv($data)
    {

        $header = "Company,Email,Phone,State,City,Interested Product,Purpose,Message,Created At\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->company)) {
                    $string = $string . "\"$row->company\"" . ",";
                }
                if (isset($row->email)) {
                    $string = $string . "\"$row->email\"" . ",";
                }
                if (isset($row->phone)) {
                    $string = $string . "\"$row->phone\"" . ",";
                }
                if (isset($row->city)) {
                    $string = $string . "\"$row->city\"" . ",";
                }
                if (isset($row->interest)) {
                    $string = $string . "\"$row->interest\"" . ",";
                }
                if (isset($row->purpose)) {
                    $string = $string . "\"$row->purpose\"" . ",";
                }
                if (isset($row->message)) {
                    $string = $string . "\"$row->message\"" . ",";
                }
                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . "\r\n";
                }
            }
        }
        $filename = "TradePartners.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }

    public function getProductInfoCsv($data)
    {
        $header = "Name,Email,Phone,City,Interest,Message,Created At\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->name)) {
                    $string = $string . "\"$row->name\"" . ",";
                }
                if (isset($row->email)) {
                    $string = $string . "\"$row->email\"" . ",";
                }
                if (isset($row->phone)) {
                    $string = $string . "\"$row->phone\"" . ",";
                }
                if (isset($row->city)) {
                    $string = $string . "\"$row->city\"" . ",";
                }
                if (isset($row->interest)) {
                    $string = $string . "\"$row->interest\"" . ",";
                }
                if (isset($row->message)) {
                    $string = $string . "\"$row->message\"" . ",";
                }
                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . "\r\n";
                }
            }
        }
        $filename = "ProductInfo.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }
} 