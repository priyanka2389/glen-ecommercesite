<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/13/14
 * Time: 1:18 PM
 */
class UserService
{

    function __construct(iUserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * @param string $first_name
     * @param string $last_name
     * @param string $email
     * @param $password
     * @param bool $newsletters
     * @param bool $special_offers
     * @return mixed
     */
    public function createUser($first_name, $last_name, $email, $mobile, $password, $newsletters, $special_offers)
    {
        return $this->userRepo->createUser($first_name, $last_name, $email, $mobile, $password, $newsletters, $special_offers);

    }

    public function getUsers($newletters, $special_offers, $from_date, $to_date, $paginate)
    {
        $users = $this->userRepo->getUsers($newletters, $special_offers, $from_date, $to_date, $paginate);
        return AppUtil::returnResults($users);
    }

    /** returns user by user_id,email or phone
     * @param int $id user_id
     * @param string $email
     * @param string $phone
     * @return
     */
    public function getUser($id, $email, $phone)
    {
        $user = $this->userRepo->getUser($id, $email, $phone);
        return !is_null($user) ? $user : null;
    }


    /** add user address as well as updates users first_name,last_name,landline and mobile
     * @param $user_id
     * @param $line1
     * @param $line2
     * @param $state
     * @param $city
     * @param $landmark
     * @param $pincode
     * @param $first_name
     * @param $last_name
     * @param $landline
     * @param $mobile
     * @param $type
     * @return address
     */
    public function addUserAddress($user_id, $line1, $line2, $state, $city, $landmark, $pincode, $first_name,
                                   $last_name, $landline, $mobile, $type)
    {
        $address = $this->userRepo->addUserAddress($user_id, $line1, $line2, $state, $city, $landmark, $pincode, $first_name,
            $last_name, $landline, $mobile, $type);
        return isset($address) ? $address : null;
    }

    public function updateUserAddress($address_id, $user_id, $line1, $line2, $state, $city, $landmark, $pincode, $first_name,
                                      $last_name, $landline, $mobile, $type)
    {
        $address = $this->userRepo->updateUserAddress($address_id, $user_id, $line1, $line2, $state, $city, $landmark, $pincode, $first_name,
            $last_name, $landline, $mobile, $type);
        return isset($address) ? $address : null;
    }

    public function deleteUserAddress($address_id)
    {
        return $this->userRepo->deleteUserAddress($address_id);
    }


    public function isUserAddressExist($user_id)
    {
        $address = $this->userRepo->getUserAddress($user_id);
        return isset($address) && ($address->count() != 0) ? $address : false;
    }

    public function getUserAddresses($user_id, $type)
    {
        $addresses = $this->userRepo->getUserAddresses($user_id, $type);
        return AppUtil::returnResults($addresses);
    }

    public function getAddress($address_id)
    {
        $address = $this->userRepo->getAddress($address_id);
        return isset($address) ? AppUtil::returnResults($address) : null;
    }

    public function updateUser($id, $first_name, $last_name, $pincode, $address_line1, $address_line2, $landmark, $city, $state, $landline, $mobile)
    {
        $this->userRepo->updateUser($id, $first_name, $last_name, $pincode, $address_line1, $address_line2, $landmark, $city, $state, $landline, $mobile);
    }

    public function updateUserMobile($id, $mobile)
    {
        $this->userRepo->updateUserMobile($id, $mobile);
    }

    /** change the password by email or by user_id
     * @param int $id user_id
     * @param string $email
     * @param string $password hashed password
     * @return mixed
     */
    public function changePassword($id, $email, $password)
    {
        return $this->userRepo->changePassword($id, $email, $password);
    }

    /**
     * @param string $email
     * @param string $password hashed password
     * @return mixed
     */
    public function forgotPassword($email, $password)
    {
        return $this->userRepo->forgotPassword($email, $password);
    }

    /** return the role of the user by user_id
     * @param $id
     */
    public function getUserRole($id)
    {
        return $this->userRepo->getUserRole($id);
    }

    /** return all the states
     */
    public function getStates()
    {
        return $this->userRepo->getStates();
    }

    public function getCitiesByState($state)
    {
        $cities_data = $this->userRepo->getCitiesByState($state);
        return AppUtil::returnResults($cities_data);
    }

    public function getPincodesByCity($city)
    {
        $pincode_data = $this->userRepo->getPincodesByCity($city);
        return AppUtil::returnResults($pincode_data);
    }

    /** accepts pincode and returns the city and state info
     * @param int $pincode
     * @return null
     */
    public function getCityAndStateByPincode($pincode)
    {
        $location_data = $this->userRepo->getCityAndStateByPincode($pincode);
        return AppUtil::returnResults($location_data);
    }

    public function updateUserAddressType($user_id, $address_id, $type)
    {
        $address = $this->userRepo->getupdateUserAddressType($user_id, $address_id, $type);
        return isset($address) ? $address : null;
    }

    public function  getCustomerCsv($data)
    {
        $header = "Customer Id,First Name,Last Name,Email,Mobile,Landline,Newsletters,Special Offers,Created On\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->id)) {
                    $string = $string . "\"$row->id\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->first_name)) {
                    $string = $string . "\"$row->first_name\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->last_name)) {
                    $string = $string . "\"$row->last_name\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->email)) {
                    $string = $string . "\"$row->email\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->mobile)) {
                    $string = $string . "\"$row->mobile\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->landline)) {
                    $string = $string . "\"$row->landline\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->newsletters)) {
                    $newsletter = ($row->newsletters == 1) ? 'Yes' : 'No';
                    $string = $string . "\"$newsletter\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->special_offers)) {
                    $special_offers = ($row->special_offers == 1) ? 'Yes' : 'No';
                    $string = $string . "\"$special_offers\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }

                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . "\r\n";
                }

            }
        }

        $filename = "Customers.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }
} 