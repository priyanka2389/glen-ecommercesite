<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/29/14
 * Time: 3:30 PM
 */
class CouponService
{

    function __construct(iCouponMainRepository $couponMainRepository)
    {
        $this->couponMainRepository = $couponMainRepository;
    }

    public function createMainCoupon($name, $percentage, $min_value, $expiry_date,
                                     $is_unique, $coupons_qty,$coupon_code, $category_ids = array())
    {
        $this->couponMainRepository->createMainCoupon($name, $percentage, $min_value, $expiry_date,
            $is_unique, $coupons_qty,$coupon_code, $category_ids);

    }

    public function getMainCoupons($from_date, $to_date, $expired, $paginate)
    {
        $main_coupons = $this->couponMainRepository->getMainCoupons($from_date, $to_date, $expired, $paginate);
        return AppUtil::returnResults($main_coupons);
    }


    public function getMainCoupon($id)
    {
        $main_coupon = $this->couponMainRepository->getMainCoupon($id);
        return isset($main_coupon) ? $main_coupon : null;
    }


    public function updateMainCoupon($name, $percentage, $min_value, $expiry_date)
    {
        $this->couponMainRepository->updateMainCoupon($name, $percentage, $min_value, $expiry_date);
    }


    public function getMainCouponCategory($main_coupon_id, $category_id)
    {
        $result = $this->couponMainRepository->getMainCouponCategory($main_coupon_id, $category_id);
        return isset($result) ? $result : null;
    }


    public function getCoupons($coupon_main_id, $is_used, $paginate)
    {
        $coupons = $this->couponMainRepository->getCoupons($coupon_main_id, $is_used, $paginate);
        return AppUtil::returnResults($coupons);
    }


    public function getCoupon($coupon_id, $coupon_code)
    {
        $coupon = $this->couponMainRepository->getCoupon($coupon_id, $coupon_code);
        return isset($coupon) ? $coupon : null;
    }

    public function is_coupon_valid($coupon_code)
    {
        $coupon = $this->couponMainRepository->getCoupon(null, $coupon_code);
        return isset($coupon) ? true : false;
    }

    public function updateCoupon($coupon_id,$order_id){
        $this->couponMainRepository->updateCoupon($coupon_id,$order_id);
    }

    public function getCouponCodesCsv($data,$id)
    {
        $string = "";

        $coupon = $this->getMainCoupon($id);

        $header = "Coupon Details \r\n\n";
        $string = $string . $header;
        $string .= "Coupon Name,$coupon->name\r\n";
        $string .= "Discount Percent,$coupon->percentage\r\n";
        $string .= "Min Value for Discount,$coupon->min_value\r\n";
        $string .= "Expiry,$coupon->expiry_date\r\n";
        $string .= "Is Unique,$coupon->is_unique\r\n";
        $string .= "Created At,$coupon->created_at\r\n\n";

        $string .= "Coupon Codes";
        $string .= "Id,Code,Order Id,Is Used,Created At\r\n";


        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->id)) {
                    $string = $string . "\"$row->id\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->code)) {
                    $string = $string . "\"$row->code\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->order_id)) {
                    $string = $string . "\"$row->order_id\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if(!is_null($row->order_id)){
                    $is_used = "Yes";
                }else{
                    $is_used = "No";
                }
                if (isset($is_used)) {
                    $string = $string . "\"$is_used\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
//                if (isset($row->state)) {
//                    $string = $string . "\"$row->state\"" . ",";
//                } else {
//                    $string = $string . "-" . ",";
//                }
//                if (isset($row->message)) {
//                    $string = $string . "\"$row->message\"" . ",";
//                } else {
//                    $string = $string . "-" . ",";
//                }
                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . "\r\n";
                }
            }
        }
        $filename = "Coupon_codes.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }

} 