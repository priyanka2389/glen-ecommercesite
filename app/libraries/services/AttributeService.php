<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 12/30/13
 * Time: 4:05 PM
 */
class AttributeService
{

    function __construct(iAttributeRepository $attributeRepo, iProductRepository $productRepo, iCategoryRepository $categoryRepo)
    {
        $this->attributeRepo = $attributeRepo;
        $this->productRepo = $productRepo;
        $this->categoryRepo = $categoryRepo;
    }

    public function createAttribute($name, $code, $attribute_category, $description, $category_id, $is_comparable, $is_filterable,
                                    $attribute_value_type, $options, $active, $sequence)
    {
        $this->attributeRepo->createAttribute($name, $code, $attribute_category, $description, $category_id, $is_comparable, $is_filterable,
            $attribute_value_type, $options, $active, $sequence);

    }

    public function getAttributes()
    {
        $attributes = $this->attributeRepo->getAttributes(null, null, null, null, null);
        return AppUtil::returnResults($attributes);
    }

    public function getAttribute($id, $code)
    {
        $attribute = $this->attributeRepo->getAttribute($id, $code);
        return AppUtil::returnResults($attribute);
    }

    public function updateAttribute($id, $name, $code, $attribute_category, $description, $category_id, $is_comparable
        , $is_filterable, $attribute_value_type, $options, $active, $sequence)
    {
        $this->attributeRepo->updateAttribute($id, $name, $code, $attribute_category, $description, $category_id, $is_comparable
            , $is_filterable, $attribute_value_type, $options, $active, $sequence);
    }

    public function deleteAttribute($id)
    {
        $this->attributeRepo->deleteAttribute($id);
    }
}