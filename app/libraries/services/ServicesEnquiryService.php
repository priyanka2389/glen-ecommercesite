<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/15/14
 * Time: 10:41 AM
 */
class ServicesEnquiryService
{
    function __construct(iServicesEnquiryRepository $servicesEnquiryRepository)
    {
        $this->servicesEnquiryRepository = $servicesEnquiryRepository;
    }

    public function createServiceEnquiry($name, $email, $phone, $address, $product, $bill_no, $shop_name, $bill_date,
                                         $message)
    {
        $result = $this->servicesEnquiryRepository->createServiceEnquiry($name, $email, $phone, $address, $product, $bill_no,
            $shop_name, $bill_date, $message);
        return isset($result) ? $result : null;
    }

    public function getServiceEnquiries($from_date, $to_date, $paginate)
    {

        $result = $this->servicesEnquiryRepository->getServiceEnquiries($from_date, $to_date, $paginate);
        return AppUtil::returnResults($result);

    }

    public function getServiceEnquiry($id)
    {
        $result = $this->servicesEnquiryRepository->getServiceEnquiry($id);
        return isset($result) ? $result : null;
    }

    public function deleteServiceEnquiry($id)
    {
        $this->servicesEnquiryRepository->deleteServiceEnquiry($id);
    }

    public function getServicesEnquiryCsv($data)
    {
        $header = "Name,Email,Phone,Address,Product,Bill No,Bill Date,Shop Name,Message,Created At\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->name)) {
                    $string = $string . "\"$row->name\"" . ",";
                }
                if (isset($row->email)) {
                    $string = $string . "\"$row->email\"" . ",";
                }
                if (isset($row->phone)) {
                    $string = $string . "\"$row->phone\"" . ",";
                }
                if (isset($row->address)) {
                    $string = $string . "\"$row->address\"" . ",";
                }
                if (isset($row->product)) {
                    $string = $string . "\"$row->product\"" . ",";
                }
                if (isset($row->bill_no)) {
                    $string = $string . "\"$row->bill_no\"" . ",";
                }
                if (isset($row->bill_date)) {
                    $string = $string . "\"$row->bill_date\"" . ",";
                }
                if (isset($row->shop_name)) {
                    $string = $string . "\"$row->shop_name\"" . ",";
                }
                if (isset($row->message)) {
                    $string = $string . "\"$row->message\"" . ",";
                }
                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . "\r\n";
                }
            }
        }
        $filename = "ProductInfo.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }
}
