<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 2/7/14
 * Time: 7:26 PM
 */
class OrderService
{

    function __construct(EloquentOrderRepository $orderRepo, EloquentProductRepository $productRepo,
                         EloquentComboRepository $comboRepo, EloquentTrackingInfoRepository $trackingInfoRepository,
                         EloquentCategoryRepository $categoryRepo, EloquentUserRepository $userRepo)
    {
        $this->orderRepo = $orderRepo;
        $this->productRepo = $productRepo;
        $this->comboRepo = $comboRepo;
        $this->trackingInfoRepository = $trackingInfoRepository;
        $this->categoryRepo = $categoryRepo;
        $this->userRepo = $userRepo;

    }

    /** accepts order_info and order_item array, creates a new order and add the order items in order_items table
     * @param int $user_id
     * @param int|float $net_value
     * @param int|float $final_value
     * @param string $order_notes
     * @param $reference
     * @param $cart_id
     * @param $payment_mode
     * @param $shipping_address_id
     * @param $billing_address_id
     * @param $verification_code
     * @param $coupon_id
     * @param array $order_items ['product_id','product_type','offer_price','list_price','qty','item_notes']
     * @throws Exception
     * @return order
     * @internal param string $status accepts new,open,closed
     * @internal param string $payment_status accepts paid,unpaid
     */
    public function createOrder($user_id, $net_value, $final_value, $order_status, $payment_status, $order_notes, $reference, $cart_id,
                                $payment_mode, $shipping_address_id, $billing_address_id, $verification_code,
                                $coupon_id, $order_items = array())
    {
        try {

            $order = $this->orderRepo->createOrder($user_id, $net_value, $final_value, $order_status, $payment_status, $order_notes, $reference,
                $cart_id, $payment_mode, $shipping_address_id, $billing_address_id, $verification_code, $coupon_id,
                $order_items);
            return isset($order) ? $order : false;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** accept order_id and returns the order info
     * @param int $id order_id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     * @throws Exception
     */
    public function getOrder($id)
    {
        try {

            $order = $this->orderRepo->getOrder($id);
            return isset($order) ? $order : null;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** accepts the order_id,address_id(shipping address|billing address) and returns the address info
     * @param $address_id
     * @param $type
     * @throws Exception
     * @internal param $order_id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getOrderAddress($address_id, $type)
    {
        try {

            $address = $this->orderRepo->getOrderAddress($address_id, $type);
            return isset($address) ? $address : null;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** return orders by user_id,status,payment_status
     * @param int $user_id
     * @param string $status accepts new,open,closed
     * @param string $payment_status accepts paid,unpaid
     * @param int $paginate
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Pagination\Paginator|null|static[]
     * @throws Exception
     */
    public function getOrders($user_id, $status, $payment_status, $payment_method, $from_date, $to_date, $paginate, $show_trash)
    {
        try {

            $orders = $this->orderRepo->getOrders($user_id, $status, $payment_status, $payment_method, $from_date, $to_date, $paginate, $show_trash);
            return AppUtil::returnResults($orders) ? $orders : null;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** accepts order_id and updates order status
     * @param int $id
     * @param string $status (new|open|closed)
     * @throws Exception
     */
    public function updateStatus($id, $status)
    {
        try {
            $this->orderRepo->updateStatus($id, $status);
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** accepts order_id and updates the payment status
     * @param int $id
     * @param string $status (paid|unpaid|pending)
     * @param $notes
     * @throws Exception
     */
    public function updatePaymentStatus($id, $status, $notes)
    {
        try {
            $this->orderRepo->updatePaymentStatus($id, $status, $notes);
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function updateVerificationCode($id, $verification_code)
    {
        $this->orderRepo->updateVerificationCode($id, $verification_code);
    }

    public function updateCodVerificationStatus($order_id, $status)
    {
        $this->orderRepo->updateCodVerificationStatus($order_id, $status);
    }

    public static function generateVerificationCode()
    {
//        return Str::random(6);
        return substr(number_format(time() * mt_rand(), 0, '', ''), 0, 6);
    }

    /**
     * @param int $id order_id
     */
    public function deleteOrder($id)
    {
        $this->orderRepo->deleteOrder($id);
    }

    public function isCodApplicable($items = array())
    {
        try {
            foreach ($items as $item) {

                $custom_id = $item['id'];
                $array = explode("_", $custom_id);
                $item_type = $array[0];
                $id = $array[1];

                if ($item_type == "product") {
                    $product = $this->productRepo->getProductBasicInfo($id, null, null, null);
                    if ($product->is_cod == false) {
                        return false;
                    }
                } else {
                    $combo = $this->comboRepo->getCombo($id, null);
                    if (isset($combo->is_cod) == false) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function updateTrackingInfo($id, $provider, $code, $tracking_url, $notes)
    {

        $this->trackingInfoRepository->addItem($id, $provider, $code, $tracking_url, $notes);
    }

    public function getOrderCsv($data, $show_trash)
    {

        $header = ($show_trash == 1) ? "OrderId,Reference,CustomerName,Final Value,Order Status,Payment Status,Payment Method,Created On,Deleted On\r\n" : "OrderId,Reference,CustomerName,Final Value,Order Status,Payment Status,Payment Method,Created On\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->id)) {
                    $string = $string . "\"$row->id\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->reference)) {
                    $string = $string . "\"$row->reference\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->user->first_name) && isset($row->user->last_name)) {
                    $customer_name = $row->user->first_name . " " . $row->user->last_name;
                    $string = $string . "\"$customer_name\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->final_value)) {
                    $string = $string . "\"$row->final_value\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->status)) {
                    $string = $string . "\"$row->status\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->payment_status)) {
                    $string = $string . "\"$row->payment_status\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->payment_mode)) {
                    $string = $string . "\"$row->payment_mode\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if ($show_trash == 1) {
                    if (isset($row['created_at'])) {
                        $created_at = date("Y-m-d", strtotime($row['created_at']));
                        $string = $string . "\"$created_at\"" . ",";
                    }
                    if (isset($row['deleted_by_admin'])) {
                        $deleted_at = date("Y-m-d", strtotime($row['deleted_by_admin']));
                        $string = $string . "\"$deleted_at\"" . "\r\n";
                    }
                } else {
                    if (isset($row['created_at'])) {
                        $created_at = date("Y-m-d", strtotime($row['created_at']));
                        $string = $string . "\"$created_at\"" . "\r\n";
                    }
                }
            }
        }

        $filename = ($show_trash == 0) ? "Orders.csv" : "OrdersTrashReport.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }

    public function getDeleteOrderByAdmin($order_id, $show_trash)
    {
        $this->orderRepo->getUpdateOrderByAdmin($order_id, $show_trash);
    }

    public function getRestoreOrderByAdmin($order_id, $show_trash)
    {
        $this->orderRepo->getUpdateOrderByAdmin($order_id, $show_trash);
    }

    public function getSalesByCategory($from_date, $to_date, $category_id)
    {
        $status = array('new', 'in-transit', 'delivered');
        $payment_status = array('paid', 'pending');
//        getOrders($user_id, $status, $payment_status, $payment_method, $from_date, $to_date, $paginate, $show_trash)
        $orders = $this->getOrders(null, $status, $payment_status, null, $from_date, $to_date, null, 0);

        $products = array();
        $product_ids = array();
        $products['combo'] = array();
        if (isset($orders)) {
            foreach ($orders as $order) {
                if ($order->items->count() > 0 && !($order->status == 'new' && $order->payment_status == 'pending' && $order->payment_mode == 'online')) {
                    $items = $order->items;
                    if (isset($items)) {
                        foreach ($items as $item) {

                            if ($item->item_type == 'product') {
                                $product_id = $item->item_id;
                                $price = ($item->offer_price < $item->list_price && $item->offer_price != 0) ? $item->offer_price : $item->list_price;
                                $product = $this->productRepo->getProduct($product_id, null, null, null);
                                if (isset($product) && in_array($product->category_id, $category_id)) {
                                    if (!isset($products[$product->category_id]) && in_array($product->category_id, $category_id)) {
                                        $products[$product->category_id] = array();
                                    }
                                    array_push($products[$product->category_id], array('order_id' => $order->id, 'product_id' => $product_id, 'price' => $price, 'category_id' => $product->category_id, 'payment_method' => $order->payment_mode));
                                }
                            } else if ($item->item_type == 'combo') {
                                $combo_id = $item->item_id;
                                $price = $item->offer_price;
                                $combo = $this->comboRepo->getCombo($combo_id, null);
                                $combo_products = $this->comboRepo->getComboProducts($combo_id);
                                $combo_name = array();
                                foreach ($combo_products->products as $combo_product) {
                                    $product_id = $combo_product->id;
                                    $product = $this->productRepo->getProduct($product_id, null, null, null);
                                    $category = $this->categoryRepo->getCategory($product->category_id, null, null, null);

                                    $combo_name[] = $product->name . " (" . $category->name . ")";
                                }
                                if (isset($combo)) {
                                    array_push($products['combo'], array('order_id' => $order->id, 'combo_id' => $combo_id, 'combo_name' => join($combo_name, ' + '), 'price' => $price, 'payment_method' => $order->payment_mode));
                                }
                            }
                        }
                    }
                }
            }

            $categories = array();
            $combos = array();
            foreach ($products as $key => $order_items) {
                $total_online_price = 0;
                $total_cod_price = 0;
                if ($key == 'combo') {
                    $combos = array_merge($combos, $order_items);
                } else {
                    $qty = 0;
                    foreach ($order_items as $order_item) {
                        if ($order_item['payment_method'] == 'online') {
                            $total_online_price = $total_online_price + $order_item['price'];
                        } else {
                            $total_cod_price = $total_cod_price + $order_item['price'];
                        }
                        $qty = $qty + 1;
                    }
                    $category = $this->categoryRepo->getCategory($order_item['category_id'], null, null, null);
                    $categories[] = array('category_id' => $order_item['category_id'], 'category_name' => $category->name, 'quantity' => $qty, 'online_total_price' => $total_online_price, 'cod_total_price' => $total_cod_price);
                }
            }

            $data['combos'] = $combos;
            $data['categories'] = $categories;
//            echo "<pre>";            print_r($products);            echo "</pre>";
//            echo "<pre>";            print_r($categories);            echo "</pre>";exit;
            return $data;
        } else {
            return null;
        }
    }

    public function getOrdersByCategory($from_date, $to_date, $category_id, $item_type=null)
    {
        $status = array('new', 'in-transit', 'delivered');
        $payment_status = array('paid', 'pending');

        $validOrders = $this->getOrders(null, $status, $payment_status, null, $from_date, $to_date, null, 0);

        $orders = array();
        if (isset($validOrders)) {
            foreach ($validOrders as $validOrder) {

                $user = $this->userRepo->getUser($validOrder->user_id, null, null);
                $user_name = ($user) ? $user->first_name . " " . $user->last_name : '';
                $user_mobile = ($user->mobile) ? $user->mobile : '';

                $order_total = ($validOrder->net_value < $validOrder->final_value && $validOrder->net_value != 0) ? $validOrder->net_value : $validOrder->final_value;

                $shipping_address = $this->userRepo->getAddress($validOrder->shipping_address_id);
                $shipping_address_city = ($shipping_address->city) ? $shipping_address->city : '';
                $shipping_address_state = ($shipping_address->state) ? $shipping_address->state : '';

                if ($validOrder->items->count() > 0 && !($validOrder->status == 'new' && $validOrder->payment_status == 'pending' && $validOrder->payment_mode == 'online')) {
                    $items = $validOrder->items;
                    if (isset($items)) {
                        foreach ($items as $item) {
                            if ($item->item_type == 'product') {
                                $product_id = $item->item_id;
                                $product = $this->productRepo->getProduct($product_id, null, null, null);

                                $item_subcategory = $this->categoryRepo->getCategory($product->category_id, null, null, null);
                                $subCategory = $item_subcategory->name;

                                $item_category = $this->categoryRepo->getCategory($item_subcategory->parent_category_id, null, null, null);
                                $category = $item_category->name;

                                $category_total = ($item->offer_price < $item->list_price && $item->offer_price != 0) ? $item->offer_price : $item->list_price;
                            } else {
                                $category = 'combo';
                                $subCategory = 'combo';

                                $combo_id = $item->item_id;
                                $category_total = $item->offer_price;
                            }
                            $order = array(
                                'order_id' => ($validOrder->id) ? $validOrder->id : '',
                                'user_name' => $user_name,
                                'mobile' => $user_mobile,
                                'payment_method' => $validOrder->payment_mode,
                                'order_status' => $validOrder->status,
                                'payment_status' => $validOrder->payment_status,
                                'order_total' => $order_total,
                                'category_total' => $category_total,
                                'shipping_address_city' => $shipping_address_city,
                                'shipping_address_state' => $shipping_address_state,
                                'category' => $category,
                                'sub_category' => $subCategory,
                                'order_date' => $validOrder->created_at
                            );
                            if (isset($category_id)) {
                                if ($product->category_id == $category_id && $item_type == 'product' && $category != 'combo') {
                                    array_push($orders, $order);
                                }
                                if (isset($combo_id) && $category_id == $combo_id && $item_type == 'combo' && $category == 'combo') {
                                    array_push($orders, $order);
                                }
                            } else {
                                array_push($orders, $order);
                            }

                        }
                    }
                }
            }
        }

        return $orders;

        //            echo "<pre>";            print_r($products);            echo "</pre>";//            exit;
//            echo "<pre>";            print_r($data);            echo "</pre>";            exit;
    }

    public function getOrderSalesCsv($data)
    {
        $header = "Sr No.,Order No,User Name,Mobile,State,City,Payment Method,Order Status,Payment Status,Order Total,Category,Sub Category,Category Total,Order Date\r\n";

        $string = "";
        $i = 1;

        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                $string = $string . $i . ",";
                if (isset($row['order_id'])) {
                    $string = $string . $row["order_id"] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['user_name'])) {
                    $string = $string . $row['user_name'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['mobile'])) {
                    $string = $string . $row['mobile'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['shipping_address_state'])) {
                    $string = $string . $row['shipping_address_state'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['shipping_address_city'])) {
                    $string = $string . $row['shipping_address_city'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['payment_method'])) {
                    $string = $string . $row['payment_method'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['order_status'])) {
                    $string = $string . $row['order_status'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['payment_status'])) {
                    $string = $string . $row['payment_status'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['order_total'])) {
                    $string = $string . $row['order_total'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['category'])) {
                    $string = $string . $row['category'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['sub_category'])) {
                    $string = $string . $row['sub_category'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['category_total'])) {
                    $string = $string . $row['category_total'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['order_date'])) {
                    $string = $string . date('Y-m-d', strtotime($row['order_date'])) . "\r\n";
                } else {
                    $string = $string . "-" . "\r\n";
                }
                $i++;
            }
        }

        $filename = "SalesReport.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print trim($string);

    }


}