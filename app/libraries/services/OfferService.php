<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 4:47 PM
 */
class OfferService
{
    function __construct(iOfferRepository $offerRepository)
    {
        $this->offerRepository = $offerRepository;
    }

    public function create($name, $email, $phone, $city, $message,$source)
    {
        $offerEnquiry = $this->offerRepository->create($name, $email, $phone, $city, $message,$source);
        return $offerEnquiry;
    }

    public function getEnquiries($from_date, $to_date, $paginate)
    {
        $demos = $this->offerRepository->getEnquiries($from_date, $to_date, $paginate);
        return AppUtil::returnResults($demos);
    }

    public function getEnquiry($id)
    {
        $demo = $this->offerRepository->getEnquiry($id);
        return isset($demo) ? $demo : null;
    }

    public function deleteEnquiry($id)
    {
        $this->offerRepository->deleteEnquiry($id);
    }

    public function getCsv($data)
    {

        $header = "Name,Email,Phone,City,Message,Created At\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->name)) {
                    $string = $string . "\"$row->name\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->email)) {
                    $string = $string . "\"$row->email\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->mobile)) {
                    $string = $string . "\"$row->mobile\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->city)) {
                    $string = $string . "\"$row->city\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->message)) {
                    $string = $string . "\"$row->message\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . "\r\n";
                }
            }
        }
        $filename = "Enquiries.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }

}