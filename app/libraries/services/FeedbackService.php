<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/26/14
 * Time: 5:21 PM
 */
class FeedbackService
{
    function __construct(iFeedbackRepository $feedbackRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
    }

    public function createFeedback($email, $phone, $category, $message)
    {
        $this->feedbackRepository->createFeedback($email, $phone, $category, $message);
    }

    public function getFeedbacks($from_date, $to_date, $paginate)
    {
        $feedbacks = $this->feedbackRepository->getFeedbacks($from_date, $to_date, $paginate);
        return AppUtil::returnResults($feedbacks);
    }

    public function deleteFeedback($id)
    {
        $this->feedbackRepository->deleteFeedback($id);
    }

    public function getFeedbackCsv($data)
    {

        $header = "Id,Email,Mobile,Category,Message,Created At\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->id)) {
                    $string = $string . "\"$row->id\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->email)) {
                    $string = $string . "\"$row->email\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->mobile)) {
                    $string = $string . "\"$row->mobile\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->category)) {
                    $string = $string . "\"$row->category\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->message)) {
                    $string = $string . "\"$row->message\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }

                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . "\r\n";
                }
            }
        }
        $filename = "Feedback.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }
} 