<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/22/14
 * Time: 11:01 AM
 */
class CartService
{

    function __construct(iProductRepository $productRepo, iComboRepository $comboRepo, iCartRepository $cartRepo, iUserRepository $userRepo, iCategoryRepository $categoryRepo)
    {
        $this->productRepo = $productRepo;
        $this->comboRepo = $comboRepo;
        $this->cartRepo = $cartRepo;
        $this->userRepo = $userRepo;
        $this->categoryRepo = $categoryRepo;

    }


    /** [ADD PRODUCT IN SESSION CART]
     * accepts the product_id and quantity and adds to session cart
     * @param int $id
     * @param int $quantity
     * @param $qty_changed
     * @throws Exception
     * @internal param float|int $price
     */
    public function addProduct($id, $quantity, $qty_changed)
    {
        try {

            $custom_id = CartService::createCustomId("product", $id);
            $row_id = $this->getRowId($custom_id);

//          $id = CartService::getIdFromCustomId($custom_id);
            $product = $this->productRepo->getProduct($id, null, null, null);
            $name = $product->name;

            if (isset($product->offer_price) && $product->offer_price != 0) {
                $price = $product->offer_price;
            } else {
                $price = $product->list_price;
            }
//            $price = $product->list_price; //todo:check whether this will be offer_price or list_price

            if ($row_id) {
                $this->updateItem("product", $id, $quantity, $qty_changed);
            } else {
                Cart::add(array('id' => $custom_id, 'name' => $name, 'qty' => $quantity, 'price' => $price));
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /** [ADD PRODUCT IN DATABASE CART]
     * @param $id
     * @param $quantity
     * @param $qty_changed
     * @throws Exception
     */
    public function addProductToDbCart($id, $quantity, $qty_changed)
    {
        try {

            $product = $this->productRepo->getProduct($id, null, null, null);
            if (isset($product->offer_price) && $product->offer_price != 0) {
                $price = $product->offer_price;
            } else {
                $price = $product->list_price;
            }
//            $price = $product->list_price; //todo:check whether this will be offer_price or list_price

            $user_id = AppUtil::getCurrentUserId();
            $item = $this->cartRepo->getItem('product', $id, $user_id);

            //item already exists in database just update it
            if (isset($item) && $item->cartitems->count() != 0) {
                if ($qty_changed == true) { //if the qty is changed take the changed quantity as it is
                    $new_quantity = $quantity;
                } else { //if the qty is not changed add it with existing quantity
                    $existing_quantity = $item->cartitems[0]->qty;
                    $new_quantity = $existing_quantity + $quantity;
                }
                $cart_id = $item->id;
                $item_id = $item->cartitems[0]->item_id;
                $this->cartRepo->updateCart($cart_id, $item_id, 'product', $new_quantity, $price);

            } //item is new add it to database cart
            else {
                $this->cartRepo->addItem('product', $id, $user_id, $quantity, $price);
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /**[ADD COMBO IN SESSION CART]
     * accepts the combo_id and quantity and adds to cart
     * @param int $id
     * @param int $quantity
     * @param $qty_changed
     * @throws Exception
     * @internal param float|int $price
     */
    public function addCombo($id, $quantity, $qty_changed)
    {
        try {
            $custom_id = CartService::createCustomId("combo", $id);
            $row_id = $this->getRowId($custom_id);

//            $id = CartService::getIdFromCustomId($custom_id);
            $combo = $this->comboRepo->getCombo($id, null);
            $name = $combo->name;
            $price = $combo->combo_price;

            if ($row_id) {
                $this->updateItem("combo", $id, $quantity, $qty_changed);
            } else {
                Cart::add(array('id' => $custom_id, 'name' => $name, 'qty' => $quantity, 'price' => $price));
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    /** [ADD COMBO IN DATABASE CART]
     * @param $id
     * @param $quantity
     * @param $qty_changed
     * @throws Exception
     */
    public function addComboToDbCart($id, $quantity, $qty_changed)
    {
        try {

            $combo = $this->comboRepo->getCombo($id, null);
            $price = $combo->combo_price; //todo:check whether this price is correct or not

            $user_id = AppUtil::getCurrentUserId();
            $item = $this->cartRepo->getItem('combo', $id, $user_id);

            if (isset($item) && $item->cartitems->count() != 0) { //item already exists in database just update it

                if ($qty_changed == true) { //if the qty is changed take the changed quantity as it is
                    $new_quantity = $quantity;
                } else { //if the qty is not changed add it with existing quantity
                    $existing_quantity = $item->cartitems[0]->qty;
                    $new_quantity = $existing_quantity + $quantity;
                }
                $cart_id = $item->id;
                $item_id = $item->cartitems[0]->item_id;
                $this->cartRepo->updateCart($cart_id, $item_id, 'combo', $new_quantity, $price);

            } else { //item is new add it to database cart
                $this->cartRepo->addItem('combo', $id, $user_id, $quantity, $price);
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    /** returns all the items present in the cart
     * @return mixed
     */
    public function getCartContents()
    {
        if (AppUtil::isUserLoggedIn()) {
            $user_id = AppUtil::getCurrentUserId();
            $result = $this->cartRepo->getCartContents($user_id);
            $cart = array();
            foreach ($result as $row) {
                if ($row->cartitems->count() != 0) {
                    foreach ($row->cartitems as $single_item) {
                        $item_type = $single_item->item_type;
                        $item_id = $single_item->item_id;

                        if ($item_type == 'product') {
                            $product = $this->productRepo->getProductBasicInfo($item_id, null, null, null);
                            $name = $product->name;
                            //     $price = $product->price;
                        } else {
                            $combo = $this->comboRepo->getCombo($item_id, null);
                            $name = $combo->name;
                            //$price = $combo->combo_price;
                        }
                        $cart[] = array(
                            'id' => $item_type . '_' . $item_id,
                            'name' => $name,
                            'qty' => $single_item->qty,
                            'price' => $single_item->price,
                            'subtotal' => $single_item->subtotal
                        );
                    }
                } else {
                    $cart = null;
                }
            }
            return $cart;
        } else {
            return Cart::content();
        }

    }

    /** returns the cart id of the logged in user
     * @return null
     */
    public function getCartId()
    {
        if (AppUtil::isUserLoggedIn()) {
            $user_id = AppUtil::getCurrentUserId();
            $cart_id = $this->cartRepo->getCartIdByUserId($user_id);
            return $cart_id;
        } else {
            return null;
        }

    }

    private function updateItem($item_type, $item_id, $quantity, $qty_changed)
    {
        $custom_id = CartService::createCustomId($item_type, $item_id);
        $row_id = $this->getRowId($custom_id);

        if ($qty_changed == true) {
            $new_quantity = $quantity;
        } else {
            $cart = Cart::get($row_id);
            $existing_quantity = $cart->qty;
            $new_quantity = $existing_quantity + $quantity;
        }

        Cart::update($row_id, $new_quantity);
    }

    public function removeItem($id, $item_type)
    {
        $custom_id = CartService::createCustomId($item_type, $id);
        $row_id = $this->getRowId($custom_id);
        if ($row_id) {
            Cart::remove($row_id);
            return true;
        } else {
            return false;
        }
    }

    public function removeItemFromDbCart($id, $item_type)
    {
        $user_id = AppUtil::getCurrentUserId();

        $this->cartRepo->removeItem($item_type, $id, $user_id);
    }

    public function deleteCartItem($cart_item_id, $cart_id, $is_deleted_by_admin)
    {
        $this->cartRepo->updateItemByCartId($cart_item_id, $cart_id, $is_deleted_by_admin);
    }

    /** destroys the database cart by cart_id
     * @param int $cart_id
     */
    public function destroyDbCart($cart_id)
    {
        $this->cartRepo->destroyCart($cart_id);
    }

    /** returns row_id by custom_id
     * @param string $custom_id
     * @return null
     */
    public function getRowId($custom_id)
    {
        $cart_data = Cart::content();
        $result = Cart::search(array("id" => $custom_id));
        $row_id = $result ? $result[0] : null;
        return $row_id;
    }

    /** returns the total price
     * @return mixed
     */
    public function getTotalPrice()
    {
        if (AppUtil::isUserLoggedIn()) {
            $user_id = AppUtil::getCurrentUserId();
            return $this->cartRepo->getTotalPrice($user_id);
        } else {
            return Cart::total();
        }
    }

    /** return the total items in the session cart
     * @return mixed
     */
    public function getTotalItems()
    {
        if (AppUtil::isUserLoggedIn()) {

            $user_id = AppUtil::getCurrentUserId();
            return $this->cartRepo->getTotalItems($user_id);

        } else {
            return Cart::count();
        }
    }

    public static function createCustomId($name, $id)
    {
        return $name . "_" . $id;
    }

    public static function getIdFromCustomId($custom_id)
    {
        $array = explode("_", $custom_id);
        return $array[1];
    }


    public function getCartItemsRowsHtml()
    {
        $total_price = number_format(self::getTotalPrice());


        if (AppUtil::isUserLoggedIn()) {
            $user_id = AppUtil::getCurrentUserId();
            $cart_data = $this->cartRepo->getCartContents($user_id);
            $items = $cart_data[0]->cartitems;
            $logged_in = true;
        } else {
            $logged_in = false;
            $items = Cart::content();
        }


        $total_items = sizeof($items);

        $place_order = URL::to('user/address');
        $contact_us = URL::to('contact-us');

        $top = "<section id= columns  class= clearfix >
               <div class= container >
            <div class= row-fluid >
                <section id= center_column  class= \"span12 content_items \"  data-total-items=" . $total_items . " >

                    <div class= \"contenttop row-fluid\" >
                        <div class= \"white-color \" style= \"background-color:#17479d; padding:6px;\" >
                            <button type= button  class= \"close white-color\">
                            </button>
                            <h1 id= cart_title  class= \"title_category white-color\"  style= margin-bottom:0px; >Cart Summary</h1>
                        </div>

                        <div id= order-detail-content  class= \"table_block margin-top10 \">
                          <div class=scroll-products>
                          <table id= cart_summary  class= std >
                                <thead>
                                <tr>
                                    <th class= cart_product first_item >Product</th>
                                    <th class= cart_description item  >Item</th>
                                    <th class= cart_quantity item >Qty</th>
                                    <th class= cart_total item >Price</th>
                                    <th class= sub_total item >Sub Total</th>
                                    <th class= cart_delete last_item >&nbsp;</th>
                                </tr>
                                </thead>

                                <tbody class= cart_items>";

        $bottom = "</tbody>
                            </table></div>

                             <div class= \"clearfix row-fluid\"  id= customer_cart_total >
                                <div class=\" span12 pull-right\" >
                                    <table class= std >
                                        <tfoot>
                                          <tr class= cart_total_price >
                                            <td colspan= 5  id= cart_voucher  class= cart_voucher  width= 70% >

                                            </td>
                                            <td colspan= 2>
                                                <h3 class=display-inline-block >Amount Payable
                                                    <span id=total_price class=font-17px ><span class= WebRupee
                                                                                 style= \"display:inline-block; padding-left:6px;\" >Rs.</span>" . $total_price . "</span>
                                                </h3>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    <div style= display:inline-block >
                                        <h3 style= display:inline-block; >Need Help? </h3>

                                        <p style= display:inline-block; ><i class= icon-phone ></i>1800-180-1998 or <a
                                                href=" . $contact_us . " >Contact us</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class= cart_navigation ><a href=" . $place_order . " class= \"exclusive standard-checkout \"
                                                      title= Next >Place Order »</a>
                            <a class= button_large onClick=\"$.fancybox.close();\"
                               title= Continue shopping >« Continue shopping</a>
                        </p>

                    </div>
                </section>
            </div>
          </div>
        </section>";

        if (isset($items)) {
            $inner = $this->getCartItemsInnerHtml($items, $logged_in);
        } else {
            $inner = "";
        }

        return $top . $inner . $bottom;

    }

    private function getCartItemsInnerHtml($items, $logged_in)
    {
        $inner_html = "";

        foreach ($items as $item) {

            if ($logged_in == true) {
                $item_id = $item->item_id;
                $item_type = $item->item_type;
            } else {
                $custom_id = $item->id;
                $item_type_array = explode('_', $custom_id);
                $item_type = $item_type_array[0];
                $item_id = $item_type_array[1];
            }

            if ($item_type == "product") {

                $product_id = $item_id;
                $product = $this->productRepo->getProduct($product_id, null, null, null);
                $images = $product->images;
                $image = HtmlUtil::getPrimaryImage($images);
                $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE;


                $inner_html = $inner_html . "<tr class= \"cart_item first_item address_0 odd \">
                <td class= cart_product >
                    <img src= \"" . URL::to($path) . "\"  width= 80  height= 80 >
                </td>
                <td class= cart_description >
                    <p class= s_title_block >" . $product->name . "</p>
                </td>
                <td class= cart_quantity >
                <div class= cart_quantity_button >
                    <a rel= nofollow  class=\" qty_btn cart_quantity_up \" id= cart_quantity_up data-id=" . $product_id . " title= Add data-item-type=product>
                        <img src=" . asset('frontoffice/themes/leometr/img/icon/quantity_up.gif') . "  width= 14  height= 9 >
                    </a><br>
                    <a rel= nofollow  class=\" qty_btn cart_quantity_down \" id= cart_quantity_down data-id=" . $product_id . " title= Subtract data-item-type=product>
                        <img src=" . asset('frontoffice/themes/leometr/img/icon/quantity_down.gif') . "  alt= Subtract  width= 14  height= 9 >
                    </a>
                </div>
                <input type= hidden  value= 65  name= quantity_4_0_0_0_hidden >
                <input type= text  size= 2  autocomplete= off  class= cart_quantity_input id= cart_quantity_input_product_" . $product_id . "  value= " . $item->qty . ">
                <a class=\"save_new_qty\" data-item-type=product data-id=" . $product_id . ">Save</a>

                </td>
                <td class= cart_total >
		            <span class= price-black  id= total_product_price_4_0_0>
				    <span class= WebRupee > Rs. </span>" . number_format($item->price) . "</span>
                </td>

                <td class= cart_total >
		            <span class= price-black  id= total_product_price_4_0_0 >
				    <span class= WebRupee > Rs. </span>" . number_format($item->subtotal) . "</span>
                </td>

                <td class= cart_delete >
                    <div>
                    <a href=" . URL::to('cart/remove-item/product/' . $product_id) . " class= cart_quantity_delete>x</a>
                    </div>
                </td>
                </tr>";

            } else { //if the item is combo

                $combo_id = $item_id;
                $combo_products = $this->comboRepo->getComboProducts($combo_id);
                foreach ($combo_products->products as $single_combo_product) {

                    $product_id = $single_combo_product->id;
                    $product = $this->productRepo->getProduct($product_id, null, null, null);
                    $images = $product->images;
                    $image = HtmlUtil::getPrimaryImage($images);
                    $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE;
                    $inner_html = $inner_html . "<tr class= \"cart_item first_item address_0 odd \">
                <td class= cart_product >
                    <a href= # ><img src= \"" . URL::to($path) . "\"  width= 80  height= 80 ></a>
                </td>
                <td class= cart_description >
                    <p class= s_title_block >" . $product->name . "</p>
                </td>
                <td class= cart_quantity >
                <div class= cart_quantity_button >
                    <a rel= nofollow  class=\" qty_btn cart_quantity_up \" id= cart_quantity_up data-id=" . $combo_id . "  href= #  title= Add data-item-type=combo >
                        <img src=" . asset('frontoffice/themes/leometr/img/icon/quantity_up.gif') . "  dth= 14  height= 9 >
                    </a><br>
                    <a rel= nofollow  class=\" qty_btn cart_quantity_down \" id= cart_quantity_down data-id=" . $combo_id . " href= #  title= Subtract data-item-type=combo >
                        <img src=" . asset('frontoffice/themes/leometr/img/icon/quantity_down.gif') . "  alt= Subtract  width= 14  height= 9 >
                    </a>
                </div>
                <input type= hidden  value= 65  name= quantity_4_0_0_0_hidden >
                <input type= text  size= 2  autocomplete= off  class= cart_quantity_input id= cart_quantity_input_combo_" . $combo_id . "  value= " . $item->qty . ">
                <a class=\"save_new_qty\" data-item-type=combo data-id=" . $combo_id . ">Save</a>

                </td>
                <td class= cart_total >
		            <span class= price-black  id= total_product_price_4_0_0 >
				    <span class= WebRupee > Rs. </span>" . number_format($combo_products->combo_price) . "</span>
                </td>

                <td class= cart_total >
		            <span class= price-black  id= total_product_price_4_0_0 >
				    <span class= WebRupee > Rs. </span>" . number_format($item->subtotal) . "</span>
                </td>

                <td class= cart_delete >
                    <div>
                    <a href=" . URL::to('cart/remove-item/combo/' . $combo_id) . " class= cart_quantity_delete>x</a>
                    </div>
                </td>
                </tr>";
                }
            }
        }

        return $inner_html;
    }

    function paginateArray($data, $perPage, $page = null)
    {
        $page = $page ? (int)$page * 1 : (isset($_REQUEST['page']) ? (int)$_REQUEST['page'] * 1 : 1);
        $offset = ($page * $perPage) - $perPage;
        return Paginator::make(array_slice($data, $offset, $perPage, true), count($data), $perPage);
    }

    public function getAllCartData($from_date, $to_date, $paginate, $page, $is_deleted_by_admin)
    {
        $result = $this->cartRepo->getAllUsersCartContent($from_date, $to_date, $paginate, $is_deleted_by_admin);

        $cart = array();
        foreach ($result as $row) {
            if ($row->cartitems->count() > 0) {
                $cart_id = $row->id;
                $user_id = $row->user_id;
                $user = $this->userRepo->getUser($user_id, null, null);

                $user_name = $user->first_name . " " . $user->last_name;
                $email = $user->email;
                $mobile = $user->mobile;

                $cart_items = array();
                foreach ($row->cartitems as $single_item) {
                    $cart_item_id = $single_item->id;
                    $item_type = $single_item->item_type;
                    $item_id = $single_item->item_id;

                    if ($item_type == 'product') {
                        $product = $this->productRepo->getProductBasicInfo($item_id, null, null, null);
                        $name = $product->name;
                        $category_id = $product->category_id;
                        $category = $this->categoryRepo->getCategory($category_id, null, null);
                        $category_name = $category->name;
                        //     $price = $product->price;
                    } else {
                        $combo = $this->comboRepo->getCombo($item_id, null);
                        if (isset($combo)) {
                            $name = ($combo->name) ? $combo->name : '';
                            $category_name = "Combo";
                        }
                    }
                    $product_name = (isset($name) && isset($category_name)) ? $name . " (" . $category_name . ")" : '';
                    //  array_push($cart_items, $name);
//                    $cart_items[] = array(
//                        'name' => $name . " (" . $category_name . ")",
//                        'created_at' => $single_item->created_at
//                    );
                    $cartArray[] = array(
                        'cart_id' => $cart_id,
                        'item_id' => $item_id,
                        'cart_item_id' => $cart_item_id,
                        'item_type' => $item_type,
                        'user_id' => $user_id,
                        'user_name' => $user_name,
                        'email' => $email,
                        'mobile' => $mobile,
                        'name' => $product_name,
                        'created_at' => $single_item->created_at,
//                    'created_date' => $created_date
                    );
                }
            }
        }
//        $data['cart'] = $cart;
        if (isset($cartArray)) {
            $cartData = array_values(array_sort($cartArray, function ($value) {
                return $value['created_at'];
            }));
            $cart = array_reverse($cartData);
            if ($paginate != null) {
                $cartPaginate = $this->paginateArray($cart, $paginate, $page);

                $data['cart'] = $cartPaginate->getItems();
                if (isset($from_date) && isset($to_date)) {
                    $data['links'] = $cartPaginate->appends(array('from_date' => $from_date, 'to_date' => $to_date))->links();
                } else {
                    $data['links'] = $cartPaginate->links();
                }
            } else {
                $data['cart'] = $cart;
            }

            return $data;
        } else {
            return null;
        }

    }

    public function getAllCartTrashData($from_date, $to_date, $paginate, $page, $is_deleted_by_admin)
    {
        $result = $this->cartRepo->getAllUsersCartTrashContent($from_date, $to_date, $paginate, $is_deleted_by_admin);
//        echo "<pre>";print_r($result);echo "</pre>";exit;

        if (!empty($result)) {
            $cart = array();
            foreach ($result as $row) {
                if ($row->cartitems->count() > 0) {
                    $cart_id = $row->id;
                    $user_id = $row->user_id;
                    $user = $this->userRepo->getUser($user_id, null, null);

                    $user_name = $user->first_name . " " . $user->last_name;
                    $email = $user->email;
                    $mobile = $user->mobile;

                    $cart_items = array();
                    foreach ($row->cartitems as $single_item) {
                        $cart_item_id = $single_item->id;
                        $item_type = $single_item->item_type;
                        $item_id = $single_item->item_id;

                        if ($item_type == 'product') {
                            $product = $this->productRepo->getProductBasicInfo($item_id, null, null, null);
                            $name = $product->name;
                            $category_id = $product->category_id;
                            $category = $this->categoryRepo->getCategory($category_id, null, null);
                            $category_name = $category->name;
                            //     $price = $product->price;
                        } else {
                            $combo = $this->comboRepo->getCombo($item_id, null);
                            if (isset($combo)) {
                                $name = ($combo->name) ? $combo->name : '';
                                $category_name = "Combo";
                            }
                        }
                        $product_name = (isset($name) && isset($category_name)) ? $name . " (" . $category_name . ")" : '';

                        $cartArray[] = array(
                            'cart_id' => $cart_id,
                            'cart_item_id' => $cart_item_id,
                            'item_id' => $item_id,
                            'item_type' => $item_type,
                            'user_id' => $user_id,
                            'user_name' => $user_name,
                            'email' => $email,
                            'mobile' => $mobile,
                            'name' => $product_name,
                            'created_at' => $single_item->created_at,
                            'deleted_at' => $single_item->deleted_at,
                            'deleted_by_admin' => ($single_item->deleted_by_admin) ? $single_item->deleted_by_admin : ''
                        );
                    }
                }
            }
            if (isset($cartArray)) {
                $field = ($is_deleted_by_admin == 0) ? 'deleted_at' : 'deleted_by_admin';

                $cartData = array_values(array_sort($cartArray, function ($value) use ($field) {
                    return $value[$field];
                }));
                $cart = array_reverse($cartData);
                if ($paginate != null) {
                    $cartPaginate = $this->paginateArray($cart, $paginate, $page);

                    $data['cart'] = $cartPaginate->getItems();
                    if (isset($from_date) && isset($to_date)) {
                        $data['links'] = $cartPaginate->appends(array('from_date' => $from_date, 'to_date' => $to_date, 'deleted_by_admin' => $is_deleted_by_admin))->links();
                    } else {
                        $data['links'] = $cartPaginate->links();
                    }
                } else {
                    $data['cart'] = $cart;
                }
                return $data;
            } else {
                return null;
            }

        }


    }

    public function getCartCsv($data, $is_deleted)
    {
        if ($is_deleted == 1) {
            $header = "SrNo,Name,Email,Mobile,Cart Item,Created On,Deleted On\r\n";
        } else {
            $header = "SrNo,Name,Email,Mobile,Cart Item,Created On\r\n";
        }

        $string = "";
        $i = 1;

        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                $string = $string . $i . ",";
                if (isset($row['user_name'])) {
                    $string = $string . $row["user_name"] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['email'])) {
                    $string = $string . $row['email'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['mobile'])) {
                    $string = $string . $row['mobile'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }

                if (isset($row['name'])) {
                    $string = $string . $row['name'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if ($is_deleted == 1) {
                    if (isset($row['created_at'])) {
                        $created_at = date("Y-m-d", strtotime($row['created_at']));
                        $string = $string . "\"$created_at\"" . ",";
                    }
                    if (isset($row['deleted_at'])) {
                        $deleted_at = date("Y-m-d", strtotime($row['deleted_at']));
                        $string = $string . "\"$deleted_at\"" . "\r\n";
                    } else {
                        $string = $string . "-" . "\r\n";
                    }
                } else {
                    if (isset($row['created_at'])) {
                        $created_at = date("Y-m-d", strtotime($row['created_at']));
                        $string = $string . "\"$created_at\"" . "\r\n";
                    }
                }
                $i++;
            }

        }

        $filename = ($is_deleted == 0) ? "CartReport.csv" : "CartTrashReport.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print trim($string);
    }

    public function getCart($cart_id, $is_trashed)
    {
        $cart = $this->cartRepo->getCart($cart_id, $is_trashed);
        return $cart;
    }

    public function getUnDeleteCart($cart_id)
    {
        $this->cartRepo->getRestoreCart($cart_id);
    }

    public function getUnDeleteCartItem($cart_item_id, $cart_id)
    {
        $this->cartRepo->getRestoreCartItem($cart_item_id, $cart_id);
    }

    public function getAllCartsStatistics($from_date, $to_date, $category_id)
    {
        $is_deleted_by_admin = 0;
        $result = $this->cartRepo->getAllUsersCartContent($from_date, $to_date, null, $is_deleted_by_admin);

        $cart = array();
        $products['combo'] = array();
        foreach ($result as $row) {
            if ($row->cartitems->count() > 0) {
                $cart_id = $row->id;

                $cart_items = array();
                foreach ($row->cartitems as $single_item) {
                    $cart_item_id = $single_item->id;
                    $item_type = $single_item->item_type;
                    $price = $single_item->subtotal;

                    if ($item_type == 'product') {
                        $product_id = $single_item->item_id;

                        $product = $this->productRepo->getProduct($product_id, null, null, null);
                        if (isset($product) && in_array($product->category_id, $category_id)) {
                            if (!isset($products[$product->category_id]) && in_array($product->category_id, $category_id)) {
                                $products[$product->category_id] = array();
                            }
                            array_push($products[$product->category_id], array('cart_item_id' => $cart_item_id, 'product_id' => $product_id, 'qty' => $single_item->qty, 'price' => $price, 'category_id' => $product->category_id));
                        }

                    } else {
                        $combo_id = $single_item->item_id;

                        $combo = $this->comboRepo->getCombo($combo_id, null);
                        $combo_products = $this->comboRepo->getComboProducts($combo_id);
                        $combo_name = array();
                        if (isset($combo_products->products)) {
                            foreach ($combo_products->products as $combo_product) {
                                $product_id = $combo_product->id;
                                $product = $this->productRepo->getProduct($product_id, null, null, null);
                                $category = $this->categoryRepo->getCategory($product->category_id, null, null, null);

                                $combo_name[] = $product->name . " (" . $category->name . ")";
                            }
                            if (isset($combo)) {
                                array_push($products['combo'], array('cart_item_id' => $cart_item_id, 'combo_id' => $combo_id, 'combo_name' => join($combo_name, ' + '), 'qty' => $single_item->qty, 'price' => $price));
                            }
                        }

                    }
                }
            }
        }
//        echo "<pre>";            print_r($products);            echo "</pre>";            exit;
        $categories = array();
        $combos = array();
        foreach ($products as $key => $order_items) {
            $total_price = 0;
            if ($key == 'combo') {
                $combos = array_merge($combos, $order_items);
            } else {
                $qty = 0;
                foreach ($order_items as $order_item) {

                    $total_price = $total_price + $order_item['price'];
                    $qty = $qty + $order_item['qty'];
                }
                $category = $this->categoryRepo->getCategory($order_item['category_id'], null, null, null);
                $categories[] = array('category_id' => $order_item['category_id'], 'category_name' => $category->name, 'quantity' => $qty, 'total_price' => $total_price);
            }
        }

        $data['combos'] = $combos;
        $data['categories'] = $categories;
        return $data;
    }

    public function getCategoryWiseCartData($from_date, $to_date, $category_id, $item_type)
    {
        $is_deleted_by_admin = 0;
        $result = $this->cartRepo->getAllUsersCartContent($from_date, $to_date, null, $is_deleted_by_admin);

        $cart = array();
        $products['combo'] = array();
        foreach ($result as $row) {
            if ($row->cartitems->count() > 0) {
                $cart_id = $row->id;
                $user_id = $row->user_id;
                $user = $this->userRepo->getUser($user_id, null, null);
                $user_name = ($user) ? $user->first_name . " " . $user->last_name : '';
                $user_mobile = ($user->mobile) ? $user->mobile : null;

                foreach ($row->cartitems as $single_item) {
                    $subtotal = $single_item->subtotal;
                    if (!isset($cart_total[$cart_id])) {
                        $cart_total[$cart_id] = 0;
                    }
                    $cart_total[$cart_id] = $cart_total[$cart_id] + $subtotal;
                }
                $cart_items = array();
                foreach ($row->cartitems as $single_item) {
                    $cart_item_id = $single_item->id;
                    $quantity = $single_item->qty;
                    $price = $single_item->price;
                    $subtotal = $single_item->subtotal;

//                    if (!isset($cart_total[$cart_id])) {
//                        $cart_total[$cart_id] = 0;
//                    }
//                    $cart_total[$cart_id] = $cart_total[$cart_id] + $subtotal;

                    if ($single_item->item_type == 'product') {
                        $product_id = $single_item->item_id;
                        $product = $this->productRepo->getProduct($product_id, null, null, null);
                        $name = $product->name;

                        $item_subcategory = $this->categoryRepo->getCategory($product->category_id, null, null, null);
                        $subCategory = $item_subcategory->name;

                        $item_category = $this->categoryRepo->getCategory($item_subcategory->parent_category_id, null, null, null);
                        $category = $item_category->name;
                    } else {
                        $combo_id = $single_item->item_id;
                        if (isset($combo)) {
                            $name = ($combo->name) ? $combo->name : '';
                        }
                        $category = 'combo';
                        $subCategory = 'combo';
                    }

                    $cartItem = array(
                        'cart_id' => $row->id,
                        'item_name' => $name,
                        'user_name' => $user_name,
                        'mobile' => $user_mobile,
                        'price' => $price,
                        'quantity' => $quantity,
                        'category_total' => $subtotal,
                        'category' => $category,
                        'sub_category' => $subCategory,
                        'cart_total' => $cart_total[$cart_id],
                        'added_date' => $row->created_at
                    );
                    if (isset($category_id)) {
                        if ($product->category_id == $category_id && $item_type == 'product' && $category != 'combo') {
                            array_push($cart, $cartItem);
                        }
                        if (isset($combo_id) && $category_id == $combo_id && $item_type == 'combo' && $category == 'combo') {
                            array_push($cart, $cartItem);
                        }
                    } else {
                        array_push($cart, $cartItem);
                    }
                }

            }
        }
//        echo "<pre>";            print_r($cart);            echo "</pre>";            exit;
        return $cart;
    }

    public function getOrderSalesCsv($data)
    {
        $header = "Sr No.,Cart Id,Item Name,User Name,Mobile,Price,Quantity,Category total,Category,Sub Category,Cart Total,Creation Date\r\n";

        $string = "";
        $i = 1;

        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                $string = $string . $i . ",";
                if (isset($row['cart_id'])) {
                    $string = $string . $row["cart_id"] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (!empty($row['mobile'])) {
                    $string = $string . $row['mobile'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['user_name'])) {
                    $string = $string . ucfirst(strtolower($row['user_name'])) . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['item_name'])) {
                    $string = $string . $row['item_name'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['price'])) {
                    $string = $string . $row['price'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['quantity'])) {
                    $string = $string . $row['quantity'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['category_total'])) {
                    $string = $string . $row['category_total'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['category'])) {
                    $string = $string . $row['category'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['sub_category'])) {
                    $string = $string . $row['sub_category'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['cart_total'])) {
                    $string = $string . $row['cart_total'] . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row['added_date'])) {
                    $string = $string . date('Y-m-d', strtotime($row['added_date'])) . "\r\n";
                } else {
                    $string = $string . "-" . "\r\n";
                }
                $i++;
            }
        }

        $filename = "CartStatistics.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print trim($string);

    }

}
