<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/14/14
 * Time: 5:25 PM
 */
class ComboService
{

    function __construct(iComboRepository $comboRepo)
    {
        $this->comboRepo = $comboRepo;
    }

    /**
     * @param string $name
     * @param string $description
     * @param string $type
     * @param $start_date
     * @param $end_date
     * @param int|float $combo_price
     * @param $is_cod
     * @param array $combo_products_array contains product_id and combo_price
     */
    public function createCombo($name,$code,$short_code, $description, $type, $start_date, $end_date, $combo_price,
                                $is_cod,$is_active, $combo_products_array = array())
    {
        $this->comboRepo->createCombo($name,$code,$short_code, $description, $type, $start_date, $end_date, $combo_price, $is_cod,$is_active, $combo_products_array);
    }

    public function getCombo($id,$short_code)
    {
        $combo = $this->comboRepo->getCombo($id,$short_code);
        return isset($combo) ? $combo : null;
    }


    /** returns all the combos
     * @param $paginate
     * @return array|null
     */
    public function getCombos($paginate,$is_active)
    {
        $combo_array = array();
        $combos = $this->comboRepo->getCombos($paginate,$is_active);

        return $combos;
//        if ($combos->count() != 0) {
//            foreach ($combos as $i => $combo) {
//
//                $combo_data = array(
//                    'name' => $combo->name,
//                    'description' => $combo->description,
//                    'type' => $combo->type,
//                    'start_date' => $combo->start_date,
//                    'end_date' => $combo->end_date,
//                    'combo_price' => $combo->combo_price
//                );
//                $combo_array['combo_info'][$i] = $combo_data;
//
//                if ($combo->products->count() != 0) {
//
//                    foreach ($combo->products as $j => $product) {
//
//                        $product_data = array(
//                            'name' => $product->name,
//                            'code' => $product->code,
//                            'description' => $product->description,
//                            'description_Secondary' => $product->description_Secondary,
//                            'list_price' => $product->list_price,
//                            'offer_price' => $product->offer_price
//                        );
//                        $combo_array['products'][$j] = $product_data;
//
//                    }
//
//                } else {
//                    $combo_array['products'] = null;
//                }
//            }
//            return $combo_array;
//        } else {
//            return null;
//        }

    }

    public function updateCombo($id, $name,$code,$short_code, $description, $type, $start_date, $end_date, $combo_price, $is_cod,$is_active)
    {
        $this->comboRepo->updateCombo($id, $name,$code,$short_code, $description, $type, $start_date, $end_date, $combo_price, $is_cod,$is_active);
    }

    public function getCombosBasicInfo($paginate,$is_active)
    {
        $combos = $this->comboRepo->getCombos($paginate,$is_active);
        return AppUtil::returnResults($combos);
    }

    public function deleteCombo($id)
    {
        $this->comboRepo->deleteCombo($id);
    }

    public function createComboProduct($combo_id, $product_id, $combo_price)
    {
        $this->comboRepo->createComboProduct($combo_id, $product_id, $combo_price);
    }

    public function getComboProducts($combo_id)
    {
        $result = $this->comboRepo->getComboProducts($combo_id);
        return AppUtil::returnResults($result);
    }

    public function getComboProduct($combo_id, $product_id)
    {
        $result = $this->comboRepo->getComboProduct($combo_id, $product_id);
        return isset($result) ? $result : null;
    }

    public function updateComboProduct($combo_id, $product_id, $combo_price)
    {
        $this->comboRepo->updateComboProduct($combo_id, $product_id, $combo_price);
    }

    public function deleteComboProduct($combo_id, $product_id)
    {
        $this->comboRepo->deleteComboProduct($combo_id, $product_id);
    }

    public function getRelatedCombos($combo_id){
       return $this->comboRepo->getRelatedCombos($combo_id);
    }

    //region Combo Image CRUD stars here

    /**creates a new image and attaches it with the Combo
     * @param string $path
     * @param string $name
     * @param string $title
     * @param string $caption text beneath image
     * @param string $notes
     * @param bool $is_primary
     * @param int $product_id
     * @return bool
     * @throws Exception
     */
    public function createComboImage($path, $name, $title, $caption, $notes, $is_primary, $product_id)
    {
        $image = $this->comboRepo->createComboImage($path, $name, $title, $caption, $notes, $is_primary, $product_id);
        return $image;
    }

    /** accepts Combo_id and returns all the images of the corresponding Combo
     * @param int $id
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     * @throws Exception
     */
    public function getComboImages($id)
    {
        $images = $this->comboRepo->getComboImages($id);
        if ($images->count() != 0) {
            return $images;
        } else {
            return null;
        }
    }

    /** accepts the product_id and image_id and returns the corresponding product image
     * @param int $product_id
     * @param int $image_id
     * @return null
     */
    public function getComboImage($product_id, $image_id)
    {
        $image = $this->comboRepo->getComboImage($product_id, $image_id);
        return isset($image) ? $image : null;
    }


    /** set the primary image of the product
     * @param $product_id int
     * @param $image_id int
     */
    public function setComboPrimaryImage($product_id, $image_id)
    {
        $this->comboRepo->setComboPrimaryImage($product_id, $image_id);
    }

    /**
     * @param int $product_id
     * @param int $image_id
     * @param string $name
     * @param string $title
     * @param string $caption
     * @param string $notes
     * @param $is_primary
     * @param $path
     */
    public function updateComboImage($product_id, $image_id, $name, $title, $caption, $notes, $is_primary, $path)
    {
        $this->comboRepo->updateComboImage($product_id, $image_id, $name, $title, $caption, $notes, $is_primary, $path);
    }

    /** accepts product_id and image_id array and deletes the product image relation
     * @param int $product_id
     * @param array $image_id
     * @return bool
     */
    public function deleteComboImage($product_id, $image_id = array())
    {
        $this->comboRepo->deleteComboImage($product_id, $image_id);
    }
    //endregion Product Image CRUD ends here

    /** activates or deactivates the combo by id
     * @param int $id product_id
     * @param boolean $status
     */
    public function activateOrDeactivateCombo($id, $status)
    {
        $this->comboRepo->activateOrDeactivateCombo($id, $status);
    }
}