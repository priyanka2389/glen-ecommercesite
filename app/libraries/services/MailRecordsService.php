<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/8/14
 * Time: 1:04 PM
 */
class  MailRecordsService
{


    function __construct(iMailRecordsRepository $mailRecordsRepository)
    {
        $this->mailRecordsRepo = $mailRecordsRepository;
    }

    /** creates a new dealer
     * @param string $name
     * @param string $address1
     * @param string $address2
     * @param string $city
     * @param string $state
     * @param int $pincode
     * @param int $mobile
     * @param int $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @return mixed
     */
    public function createMailRecord($user_name,$to_emailId,$from_emailId,$email_subject,$email_content)
    {
        $mail = $this->mailRecordsRepo->createMailRecord($user_name,$to_emailId,$from_emailId,$email_subject,$email_content);
        return $mail;
    }

    /**return dealers by city or state
     * @param string $city
     * @param string $state
     * @param $paginate
     * @return null
     */
    public function getMails($from_date, $to_date, $paginate)
    {
        $mailRecords = $this->mailRecordsRepo->getMails($from_date, $to_date, $paginate);
        return AppUtil::returnResults($mailRecords);
    }

    public function getMail($mail_id){
        $mail = $this->mailRecordsRepo->getMail($mail_id);
        return $mail;
    }


} 