<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 9/15/14
 * Time: 4:54 PM
 */
class SettingsService
{
    function __construct(iSettingsRepository $settingsRepository)
    {
        $this->settingsRepository = $settingsRepository;
    }

    public function createSetting($setting_label,$setting_value)
    {
        $settings = $this->settingsRepository->createSetting($setting_label,$setting_value);
        return $settings;
    }

    public function getSettings()
    {
        $settings = $this->settingsRepository->getSettings();
        return AppUtil::returnResults($settings);
    }

    public function getSetting($id)
    {
        $setting = $this->settingsRepository->getSetting($id);
        return isset($setting) ? $setting : null;
    }

    public function updateSetting($id,$setting_label,$setting_value)
    {
        $this->settingsRepository->updateSettings($id,$setting_label,$setting_value);
    }

    public function getSettingByLabel($label)
    {
        $setting = $this->settingsRepository->getSettingByLabel($label);
        return isset($setting) ? $setting : null;
    }

//
//    public function deleteMessage($id)
//    {
//        $this->messageRepository->deleteMessage($id);
//    }
//
//    public function getMessageCsv($data)
//    {
//
//        $header = "User Name,User Email,Friend Name,Friend Email,Message,Link,Created At\r\n";
//        $string = "";
//        $string = $string . $header;
//        if (isset($data)) {
//            foreach ($data as $row) {
//                if (isset($row->name)) {
//                    $string = $string . "\"$row->name\"" . ",";
//                } else {
//                    $string = $string . "-" . ",";
//                }
//                if (isset($row->email)) {
//                    $string = $string . "\"$row->email\"" . ",";
//                } else {
//                    $string = $string . "-" . ",";
//                }
//                if (isset($row->friend_name)) {
//                    $string = $string . "\"$row->friend_name\"" . ",";
//                } else {
//                    $string = $string . "-" . ",";
//                }
//                if (isset($row->friend_email)) {
//                    $string = $string . "\"$row->friend_email\"" . ",";
//                } else {
//                    $string = $string . "-" . ",";
//                }
//                if (isset($row->message)) {
//                    $string = $string . "\"$row->message\"" . ",";
//                } else {
//                    $string = $string . "-" . ",";
//                }
//                if (isset($row->link)) {
//                    $string = $string . "\"$row->link\"" . ",";
//                } else {
//                    $string = $string . "-" . ",";
//                }
//                if (isset($row->created_at)) {
//                    $created_at = date("Y-m-d", strtotime($row->created_at));
//                    $string = $string . "\"$created_at\"" . "\r\n";
//                }
//            }
//        }
//        $filename = "Messages.csv";
//
//        //header for creation of csv
//        header("Content-type: application/octet-stream");
//        header("Content-Disposition: attachment; filename=$filename");
//        header("Pragma: no-cache");
//        header("Expires: 0");
//        print $string;
//    }

}