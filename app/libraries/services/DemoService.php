<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 4:47 PM
 */
class DemoService
{
    function __construct(iDemoRepository $demoRepository)
    {
        $this->demoRepository = $demoRepository;
    }

    public function createDemo($name, $email, $phone, $state, $city, $message, $product_id)
    {
        $demo = $this->demoRepository->createDemo($name, $email, $phone, $state, $city, $message, $product_id);
        return $demo;
    }

    public function getDemos($from_date, $to_date, $paginate)
    {
        $demos = $this->demoRepository->getDemos($from_date, $to_date, $paginate);
        return AppUtil::returnResults($demos);
    }

    public function getDemo($id)
    {
        $demo = $this->demoRepository->getDemo($id);
        return isset($demo) ? $demo : null;
    }

    public function deleteDemo($id)
    {
        $this->demoRepository->deleteDemo($id);
    }

    public function getDemoCsv($data)
    {

        $header = "Name,Email,Phone,City,State,Message,Created At\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->name)) {
                    $string = $string . "\"$row->name\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->email)) {
                    $string = $string . "\"$row->email\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->mobile)) {
                    $string = $string . "\"$row->mobile\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->city)) {
                    $string = $string . "\"$row->city\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->state)) {
                    $string = $string . "\"$row->state\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->message)) {
                    $string = $string . "\"$row->message\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . "\r\n";
                }
            }
        }
        $filename = "Demos.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }

}