<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/8/14
 * Time: 1:04 PM
 */
class DealersService
{


    function __construct(iDealersRepository $dealersRepo)
    {
        $this->dealersRepo = $dealersRepo;
    }

    /** creates a new dealer
     * @param string $name
     * @param string $address1
     * @param string $address2
     * @param string $city
     * @param string $state
     * @param int $pincode
     * @param int $mobile
     * @param int $phone
     * @param $is_small_appliance
     * @param $is_large_appliance
     * @param $is_active
     * @return mixed
     */
    public function createDealer($name, $address1, $address2, $location,$city, $state, $pincode, $mobile,
                                 $phone, $is_small_appliance, $is_large_appliance, $is_active, $sequence,$contact_person,$email_id,$type)
    {
        $dealer = $this->dealersRepo->createDealer($name, $address1, $address2, $location, $city, $state, $pincode, $mobile,
            $phone, $is_small_appliance, $is_large_appliance, $is_active, $sequence,$contact_person,$email_id,$type);
        return $dealer;
    }

    /**return dealers by city or state
     * @param string $city
     * @param string $state
     * @param $paginate
     * @return null
     */
    public function getDealers($city, $state, $paginate)
    {
        $dealers = $this->dealersRepo->getDealers($city, $state, $paginate);
        return AppUtil::returnResults($dealers);
    }

    /** return a dealer by dealer id
     * @param int $id
     * @return null
     */
    public function getDealer($id)
    {
        $dealer = $this->dealersRepo->getDealer($id);
        return isset($dealer) ? $dealer : null;

    }

    /** updates a dealer info
     * @param int $id
     * @param string $name
     * @param string $address1
     * @param string $address2
     * @param string $city
     * @param string $state
     * @param int $pincode
     * @param int $mobile
     * @param int $phone
     * @param bool $is_small_appliance
     * @param bool $is_large_appliance
     * @param bool $is_active
     */
    public function updateDealer($id, $name, $address1, $address2, $city, $state, $pincode, $mobile,
                                 $phone, $is_small_appliance, $is_large_appliance, $is_active, $sequence,$contact_person,$email_id,$type)
    {
        $this->dealersRepo->updateDealer($id, $name, $address1, $address2, $city, $state, $pincode, $mobile,
            $phone, $is_small_appliance, $is_large_appliance, $is_active, $sequence,$contact_person,$email_id,$type);
    }


    /** updates the appliance type
     * @param int $id dealer_id
     * @param string $type small|large
     * @param bool $status 0|1
     */
    public function updateApplianceType($id, $type, $status)
    {
        $this->dealersRepo->updateApplianceType($id, $type, $status);
    }

    /** accepts dealer_id and activate or deactivate the corresponding dealer
     * @param int $id dealer_id
     * @param bool $status 0|1
     */
    public function activateOrDeactivate($id, $status)
    {
        $this->dealersRepo->activateOrDeactivate($id, $status);
    }


    /** accept dealer id and deletes the dealers
     * @param array $id
     */
    public function deleteDealers($id = array())
    {
        $this->dealersRepo->deleteDealers($id);
    }

    /** returns all the unique states
     * @return mixed
     */
    public function getUniqueStates($type)
    {
        $states = $this->dealersRepo->getUniqueStates($type);
        return AppUtil::returnResults($states);
    }

    /** accepts the state name and return the cities for that corresponding state
     * @param string $state name of the state
     * @return mixed
     */
    public function getCitiesForState($state,$type)
    {
        return $this->dealersRepo->getCitiesForState($state,$type);
    }

  /** accepts the city name and return the locations for that corresponding state
     * @param string $state name of the state
     * @return mixed
     */
    public function getLocationsForCities($state,$city,$type)
    {
        return $this->dealersRepo->getLocationsForCities($state,$city,$type);
    }



    /** accepts the state name and return the cities for that corresponding state
     * @param string $state name of the state
     * @return mixed
     */
    public function getSubcategoriesForCategories($category)
    {
        return $this->dealersRepo->getSubcategoriesForCategories($category);
    }

    /** accepts city,state,is_small_appliance or is_large appliance and return all the dealers
     * @param string $city
     * @param string $state
     * @param bool $is_small_appliance
     * @param bool $is_large_appliance
     * @return mixed
     */
    public function getData($city, $state,$location, $is_small_appliance, $is_large_appliance,$type)
    {
        $data = $this->dealersRepo->getData($city, $state,$location, $is_small_appliance, $is_large_appliance,$type);
        return ($data) ? AppUtil::returnResults($data) : false;
    }

    /** accepts dealer_id and returns the position of the corresponding dealer
     * @param $id
     * @return mixed|null|string
     */
    public function getDealerPosition($id)
    {
        $dealer = $this->getDealer($id);
        $sequence = $dealer->sequence;
        if (isset($sequence)) {
            $max_sequence = Dealer::max('sequence');
            $min_sequence = Dealer::min('sequence');

            $after = Dealer::where('sequence', '<', $sequence)->orderBy('sequence', 'desc')->first();
            $after_id = isset($after) ? $after->id : null;

            if ($sequence == $min_sequence) {
                return 'top';
            } elseif ($sequence == $max_sequence) {
                return 'bottom';
            } else {
                return $after_id;
            }
        } else {
            return null;
        }

    }

    public function getTruncateDealer()
    {
        Dealer::truncate();
    }

    public function getDealersCsv($data)
    {

        $header = "id,name,address1,address2,city,state,pincode,mobile,phone,is_small_appliance,is_large_appliance,created_at,updated_at,is_active,deleted_at,sequence\r\n";
        $string = "";
        $string = $string . $header;
        if (isset($data)) {
            foreach ($data as $row) {
                if (isset($row->id)) {
                    $string = $string . "\"$row->id\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->name)) {
                    $string = $string . "\"$row->name\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->address1)) {
                    $string = $string . "\"$row->address1\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->address2)) {
                    $string = $string . "\"$row->address2\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->city)) {
                    $string = $string . "\"$row->city\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->state)) {
                    $string = $string . "\"$row->state\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->pincode)) {
                    $string = $string . "\"$row->pincode\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->mobile)) {
                    $string = $string . "\"$row->mobile\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->phone)) {
                    $string = $string . "\"$row->phone\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->is_small_appliance)) {
                    $string = $string . "\"$row->is_small_appliance\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->is_large_appliance)) {
                    $string = $string . "\"$row->is_large_appliance\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }
                if (isset($row->created_at)) {
                    $created_at = date("Y-m-d", strtotime($row->created_at));
                    $string = $string . "\"$created_at\"" . ",";
                }
                if (isset($row->updated_at)) {
                    $updated_at = date("Y-m-d", strtotime($row->updated_at));
                    $string = $string . "\"$updated_at\"" . ",";
                }
                if (isset($row->is_active)) {
                    $string = $string . "\"$row->is_active\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }

                if (isset($row->deleted_at)) {
                    $deleted_at = date("Y-m-d", strtotime($row->deleted_at));
                    $string = $string . "\"$deleted_at\"" . ",";
                } else {
                    $string = $string . "-" . ",";
                }

                if (isset($row->sequence)) {
                    $string = $string . "\"$row->sequence\"" . "\r\n";
                } else {
                    $string = $string . "-" . ",";
                }

            }
        }
        $filename = "Dealers.csv";

        //header for creation of csv
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        print $string;
    }

} 