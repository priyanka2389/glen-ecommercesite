<?php

class Product extends Eloquent
{
    protected $guarded = array();

    public static $rules = array();

    public function category()
    {
        return $this->belongsTo('Category');
    }

    public function attributes()
    {
        return $this->belongsToMany('Attribute', 'productattributes')->withPivot(array('notes', 'value'))->withTimestamps();
    }

    public function images()
    {
        return $this->belongsToMany('Image', 'productimages')
            ->withPivot(array('name', 'title', 'caption', 'notes', 'is_primary'))->withTimestamps();
    }

    public function tags()
    {
        return $this->belongsToMany('Tag', 'producttags')->orderBy('created_at', 'desc')->withPivot('offer_price')->withTimestamps();
    }

    public function documents()
    {
        return $this->belongsToMany('Document', 'productdocuments')->orderBy('created_at', 'desc')->withPivot('name', 'title', 'label', 'notes')->withTimestamps();
    }

    public function videos()
    {
        return $this->belongsToMany('Video', 'productvideos')->orderBy('created_at', 'desc')->withPivot('name', 'title', 'label', 'notes')->withTimestamps();
    }

    public function productSpecificAttributes()
    {
        return $this->hasMany("ProductSpecificAttribute");
    }

    public function combos()
    {
        return $this->belongsToMany('Combo', 'combo_products')->withPivot('combo_price')->withTimestamps();
    }

//    public function demos()
//    {
//        return $this->hasMany('Demo');
//    }

}
