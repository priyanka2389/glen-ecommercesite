<?php

class Attribute extends Eloquent
{
    protected $guarded = array();

    public static $rules = array();

    public function category()
    {
        return $this->belongsTo('Category');
    }

    public function products()
    {
        return $this->belongsToMany('Product', 'productattributes')->withPivot('notes', 'value');
    }

}
