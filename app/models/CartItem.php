<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 4/23/14
 * Time: 11:46 AM
 */
class CartItem extends Eloquent
{
    protected $softDelete = true;

    public function cart()
    {
        return $this->belongsTo('Cart');
    }

} 