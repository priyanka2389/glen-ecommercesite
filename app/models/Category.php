<?php

class Category extends Eloquent
{
    protected $guarded = array();

    public static $rules = array();

    public function products()
    {
        return $this->hasMany('Product');
    }

    public function attributes()
    {
        return $this->hasMany('Attribute');
    }

    public function documents()
    {
        return $this->belongsToMany('Document', 'categorydocuments')->orderBy('created_at', 'desc')->withPivot(array('name', 'title', 'label', 'notes'))->withTimestamps();
    }

    public function videos()
    {
        return $this->belongsToMany('Video', 'categoryvideos')->orderBy('created_at', 'desc')->withPivot(array('name', 'title', 'label', 'notes'))->withTimestamps();
    }

    public function main_coupons()
    {
        return $this->belongsToMany('CouponMain', 'coupon_main_categories')->orderBy('created_at', 'desc');
    }
}
