<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/29/14
 * Time: 12:58 PM
 */
class CouponMain extends Eloquent
{
    protected $table = "coupon_main";

    public function coupons()
    {
        $this->hasMany('Coupon');
    }

    public function categories()
    {
        return $this->belongsToMany('Category', 'coupon_main_categories')->orderBy('created_at', 'desc');
    }
} 