<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 4/8/14
 * Time: 12:10 PM
 */
class DbCart extends Eloquent
{
    protected $table = 'cart';

    protected $softDelete = true;

    public function cartItems()
    {
        return $this->hasMany('CartItem', 'cart_id');
    }

} 