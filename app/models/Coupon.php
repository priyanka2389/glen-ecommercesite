<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/29/14
 * Time: 12:54 PM
 */
class Coupon extends Eloquent
{

    public function categories()
    {
        return $this->belongsToMany('Category', 'coupon_catgeories')->withTimestamps();
    }

    public function main_coupon()
    {
        return $this->belongsTo('CouponMain');
    }


}