<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 3:50 PM
 */
class UrlList extends Eloquent
{
    protected $softDelete = true;

    protected $table = "url_list";

    public function getData(){
        $results = UrlList::where('is_mapped','=',0)->orderBy('created_at', 'desc')->paginate(20);
        return $results;
    }

    public function getUnmappedUrls(){
        $results = UrlList::where('is_mapped','=',0)->orderBy('created_at', 'desc')->get();
        return $results;
    }

    public function getMappedUrls(){
        $results = UrlList::where('is_mapped','=',1)->orderBy('updated_at', 'desc')->paginate(20);
        return $results;
    }

    public function getUrl($urlId){
        $url = UrlList::find($urlId);
        return $url;
    }

    public function updateUrl($urlId,$searched_url,$mapped_url){
        $url = UrlList::find($urlId);
        $url->searched_url = $searched_url;
        $url->mapped_url = $mapped_url;
        $url->is_mapped = 1;
        $url->save();

        return $url;
    }

    public function deleteUrl($id){
        $url = UrlList::find($id);
        $url->delete();
    }

    public function addUrl($searched_url){
        try {
            $url = new UrlList();
            $url->searched_url = $searched_url;
            $url->is_mapped = 0;
            $url->save();

            return $url;

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }
}