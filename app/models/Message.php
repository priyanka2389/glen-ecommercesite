<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 3:50 PM
 */
class Message extends Eloquent
{
    protected $softDelete = true;

    protected $table = "messages";
}