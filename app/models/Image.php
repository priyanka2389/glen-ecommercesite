<?php

class Image extends Eloquent
{
    protected $guarded = array();

    public static $rules = array();

    public function products()
    {
        return $this->belongsToMany('Product', 'productimages')
            ->withPivot(array('name', 'title', 'caption', 'notes', 'is_primary'))->withTimestamps();
    }

    public function combos()
    {
        return $this->belongsToMany('Combo', 'comboimages')
            ->withPivot(array('name', 'title', 'caption', 'notes', 'is_primary'))->withTimestamps();
    }

}
