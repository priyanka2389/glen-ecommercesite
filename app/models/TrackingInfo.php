<?php

class TrackingInfo extends \Eloquent {
	protected $fillable = [];

    protected $table="trackinginfos";

    public function order()
    {
        return $this->belongsTo('Order','order_id');
    }

}