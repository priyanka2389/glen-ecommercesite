<?php

class Video extends Eloquent
{
    protected $guarded = array();

    public static $rules = array();

    public function categories()
    {
        return $this->belongsToMany('Category', 'categoryvideos')->withPivot('name', 'title', 'label', 'notes')->withTimestamps();
    }

    public function products()
    {
        return $this->belongsToMany('Product', 'productvideos')->withPivot('name', 'title', 'label', 'notes')->withTimestamps();
    }
}
