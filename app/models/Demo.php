<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 3:50 PM
 */
class Demo extends Eloquent
{

    protected $softDelete = true;

    public function product()
    {
        return $this->belongsTo('Product');
    }

} 