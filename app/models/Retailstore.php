<?php

class Retailstore extends Eloquent
{
    protected $guarded = array();

    protected $softDelete = true;

    public static $rules = array();

    protected $table = "retailStores";

}
