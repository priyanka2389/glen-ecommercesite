<?php

class Combo extends Eloquent
{
    protected $guarded = array();

    public static $rules = array();

    public function products()
    {
        return $this->belongsToMany('Product', 'combo_products')->withPivot('combo_price')->withTimestamps();
    }

    public function images()
    {
        return $this->belongsToMany('Image', 'comboimages')
            ->withPivot(array('name', 'title', 'caption', 'notes', 'is_primary'))->withTimestamps();
    }

}
