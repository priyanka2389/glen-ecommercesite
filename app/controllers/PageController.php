<?php

class PageController extends BaseController
{

    function __construct(PageService $pageService)
    {
        $this->pageService = $pageService;
    }

    public function getPage($short_code, $view_name)
    {
        $data['page'] = $this->pageService->getPage(null, $short_code, 1);
        $data['searchProducts'] = AppUtil::getSearchProducts();
        if (!empty($data['page'])) {
            return View::make("frontoffice.$view_name", $data);
        } else {
            return Redirect::to('/');
        }
    }

}
