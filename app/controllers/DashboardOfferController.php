<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 4:54 PM
 */
class DashboardOfferController extends BaseController
{
    function __construct(OfferService $offerService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->offerService = $offerService;
    }

    public function getIndex()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;

        $data['enquiries'] = $this->offerService->getEnquiries($from_date, $to_date, Constants::DASHBOARD_ENQUIRES_PAGE_COUNT);
        return View::make('dashboard.enquiries', $data);
    }

    public function getDownloadCsv()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;

        $demos = $this->offerService->getEnquiries($from_date, $to_date, Constants::DASHBOARD_ENQUIRES_PAGE_COUNT);
        $this->offerService->getCsv($demos);
    }

    public function getDelete($id)
    {
        $this->offerService->deleteEnquiry($id);
        return Redirect::to("dashboard/enquiries");
    }

} 