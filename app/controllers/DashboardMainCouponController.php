<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/29/14
 * Time: 3:54 PM
 */
class DashboardMainCouponController extends BaseController
{

    function __construct(CouponService $couponService, CategoryService $categoryService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->couponService = $couponService;
        $this->categoryService = $categoryService;
    }

    //get all the main coupons
    public function getIndex()
    {
        $data['main_coupons'] = $this->couponService->getMainCoupons(null, null, null, Constants::DASHBOARD_COUPONS_PAGE_COUNT);
        return View::make('dashboard.main_coupon.index', $data);
    }

    //renders create main coupon view
    public function getCreate()
    {
        $data['categories'] = $this->categoryService->getCategories(null, null, null, null, null);
        return View::make('dashboard.main_coupon.create', $data);
    }

    public function postCreate()
    {

        try {

            $validation = new \services\Validators\MainCouponValidator();
            if ($validation->passes()) {
                $name = Input::get("name");
                $percentage = Input::get("percentage");
                $min_value = Input::get("min_value");
                $expiry_date = Input::get("expiry_date");
                $is_unique = Input::get("unique");
                $coupon_qty = Input::get('coupon_qty');
                $coupon_code = Input::get('coupon_code');
                $category_id = Input::get('category_id');

                if(is_null($coupon_qty) && !empty($coupon_code)){
                    $coupon_qty = 1;
                }

                $this->couponService->createMainCoupon($name, $percentage, $min_value, $expiry_date, $is_unique,
                    $coupon_qty,$coupon_code, $category_id);
                Notification::success("Coupon generated successfully");
                return Redirect::to('dashboard/main-coupon');
            } else {

                $error = $validation->getErrors();
                return Redirect::to('dashboard/main-coupon/create')->withInput(Input::all())->withErrors($error);

            }


        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    //renders main coupon edit view
    public function getEdit($id)
    {
        $data['main_coupon'] = $this->couponService->getMainCoupon($id);
        return View::make('dashboard.main_coupon.edit', $data);
    }

    public function postEdit($id)
    {
        try {

            $validation = new \services\Validators\MainCouponValidator();
            if ($validation->passes()) {
                $name = Input::get("name");
                $percentage = Input::get("percentage");
                $min_value = Input::get("min_value");
                $expiry_date = Input::get("expiry_date");
                $is_unique = Input::get("unique");
                $coupon_qty = Input::get('coupon_qty');
                $category_id = Input::get('category_id');

                $this->couponService->updateMainCoupon($name, $percentage, $min_value, $expiry_date);
                Notification::success("Coupon updated successfully");
                return Redirect::to('dashboard/main-coupon');
            } else {

                $error = $validation->getErrors();
                return Redirect::to('dashboard/main-coupon/create')->withInput(Input::all())->withErrors($error);

            }


        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    //get all the coupons of the main_coupon
    public function getCodes($id)
    {
        try {

            $data['codes'] = $this->couponService->getCoupons($id, null, null);
            return View::make("dashboard.coupons.all_codes", $data);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getMainCouponCategories($id)
    {
        $main_coupon = $this->couponService->getMainCoupon($id);
        $data['categories'] = $main_coupon->categories;
        return View::make('dashboard.main_coupon.coupon_categories', $data);
    }

    public function getDownloadCsv($id)
    {
        $data['codes'] = $this->couponService->getCoupons($id, null, null);

        $this->couponService->getCouponCodesCsv($data['codes'],$id);
    }

} 