<?php

class RetailStoreController extends BaseController
{
    function __construct(RetailStoresService $retailStores,ProductService $productService)
    {
        $this->retailStoresService = $retailStores;
        $this->productService = $productService;
    }


    function getIndex()
    {
        $data['retail_stores'] = $this->retailStoresService->getRetailStores(null, null, 1, null);

        $data['searchProducts'] = $this->productService->getSearchProducts();

        return View::make('frontoffice.retail_stores', $data);
    }
} 