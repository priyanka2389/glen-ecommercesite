<?php

class RemindersController extends Controller
{

    /**
     * Display the password reminder view.
     *
     * @return Response
     */
    public function getRemind()
    {
        return View::make('password.remind');
    }

    /**
     * Handle a POST request to remind a user of their password.
     *
     * @return Response
     */
    public function postRemind()
    {
        $data = Input::all();
        $rules = array(
            'email' => 'required|email'
        );

        $validation = Validator::make($data, $rules);
        if ($validation->passes()) {
            switch ($response = Password::remind(Input::only('email'))) {
                case Password::INVALID_USER:
                    Notification::error(Lang::get('responsemessages.reset_password_error'));
                    return Redirect::back()->with('error', Lang::get($response));

                case Password::REMINDER_SENT:
                    Notification::success(Lang::get('responsemessages.new_password_email_sent'));
                    return Redirect::back()->with('status', Lang::get($response));
            }
        } else {
            return Redirect::back()->withErrors($validation);
        }
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string $token
     * @return Response
     */
    public function getReset($token = null)
    {
        if (is_null($token)) App::abort(404);

        return View::make('frontoffice.user.reset_password')->with('token', $token);
    }

    /**
     * Handle a POST request to reset a user's password.
     *
     * @return Response
     */
    public function postReset()
    {
        $credentials = Input::only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($user, $password) {
            $user->password = Hash::make($password);

            $user->save();
        });

        switch ($response) {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                Notification::error(Lang::get($response));
                return Redirect::back()->with('error', Lang::get($response));

            case Password::PASSWORD_RESET:
                Notification::success('Your password has been reset successfully');
                return Redirect::back();
        }
    }

}
