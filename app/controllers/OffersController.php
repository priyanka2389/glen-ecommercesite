<?php

class OffersController extends BaseController
{

    function __construct(ProductService $productService, CategoryService $categoryService, ComboService $comboService, OfferService $offerService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
        $this->comboService = $comboService;
        $this->offerService = $offerService;
    }

    public function getMothersDaySpecialOffers()
    {

        $product_id = array(100, 102, 63, 14, 9, 24, 12, 183, 294, 28);
        $data['products_block1'] = $this->productService->getSelectedProducts($product_id, 1);
//        echo "<pre>";print_r($data['products_block1']);echo "</pre>";exit;

        $data['searchProducts'] = $this->productService->getSearchProducts();
        return View::make('frontoffice.offerzone.mothers_days_special', $data);
    }

    public function getBreakfastMadeEasy()
    {

        $product_id = array(280, 45, 13, 12, 292, 51, 253, 256);
        $data['products_block1'] = $this->productService->getSelectedProducts($product_id, 1);
//        echo "<pre>";print_r($data['products_block1']);echo "</pre>";exit;
        $product_id = array(255, 22, 183, 89);
        $data['products_block2'] = $this->productService->getSelectedProducts($product_id, 1);

        $product_id = array(8, 24, 87, 92, 94, 96, 257);
        $data['products_block3'] = $this->productService->getSelectedProducts($product_id, 1);

        $data['searchProducts'] = $this->productService->getSearchProducts();
        return View::make('frontoffice.offerzone.breakfast_made_easy', $data);
    }

    public function getChimneyOffers()
    {
        //get offer product
        $category_id = array(27, 28, 29, 30);
        $data['products_block1'] = $this->productService->getOfferProducts($category_id, 1, false);

        //get combo offer product
        $data['chimney_combos'] = $this->productService->getOfferProducts($category_id, 1, true);

        $data['searchProducts'] = $this->productService->getSearchProducts();
        return View::make('frontoffice.offerzone.chimney_offers', $data);
    }

    public function getCooktopOffers()
    {
        //get offer product
        $category_id = array(32, 34, 35);
        $data['products_block1'] = $this->productService->getOfferProducts($category_id, 1, false);

        //get combo offer product
        $data['cooktop_combos'] = $this->productService->getOfferProducts($category_id, 1, true);

        $data['searchProducts'] = $this->productService->getSearchProducts();
        return View::make('frontoffice.offerzone.cooktop_offers', $data);
    }

    public function getBuiltInHobOffers()
    {
        //get offer product
        $category_id = array(37, 38, 39);
        $data['products_block1'] = $this->productService->getOfferProducts($category_id, 1, false);

        //get combo offer product
        $data['hob_combos'] = $this->productService->getOfferProducts($category_id, 1, true);

        $data['searchProducts'] = $this->productService->getSearchProducts();
        return View::make('frontoffice.offerzone.built-in-hob-offers', $data);
    }


    public function getComboOffers()
    {
        $data['combos'] = $this->comboService->getCombos(null, 1);

        $data['searchProducts'] = $this->productService->getSearchProducts();

        return View::make('frontoffice.offerzone.combo_offers', $data);
    }

    public function getRequestCall()
    {
        $data['source'] = Input::get('source',null);
        return View::make('frontoffice.request_call',$data);
    }

    public function postRequestCall()
    {


        $name = Input::get('name', null);
        $email = Input::get('email', null);
        $phone = Input::get('mobile', null);
        $city = Input::get('city', null);
        $message = Input::get('message', null);
        $source = Input::get('source',null);
        $input = Input::all();

        $validation = new \services\Validators\RequestCallValidator();

        if ($validation->passes()) {
            $this->offerService->create($name, $email, $phone, $city, $message,$source);
            Notification::success('Thank you! Your request has been registered and will be acted upon shortly.');

            return Redirect::to('request-a-call');
        } else {
            $error = $validation->getErrors();
            print_r($error);
            return Redirect::back()->withInput($input)->withErrors($error);
        }
    }


}
