<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 3/26/14
 * Time: 1:16 PM
 */
class DashboardVariantController extends BaseController
{
    function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }


    public function getCreate($product_id)
    {
        $data['active'] = 'product_info';
        $data['product'] = $this->productService->getProduct($product_id, null, null, null);
        $data['categories'] = $this->categoryService->getCategories(null, null, null, null, null);
//        $data['sequence'] = $this->productService->getProductPosition($id);
        $data['products'] = $this->productService->getProducts(null, null, null, null, null, null, null, null);
        return View::make('dashboard.variant.index', $data);

    }

    // during the creation of variant, info from product table and info from product images table is duplicated
    // in the variant.
    public function postCreate($product_id)
    {
        try {

            $validation = new \services\Validators\ProductValidator();
            $validation::$rules['sap_code'] = '';
            $validation::$rules['category'] = '';
            $validation::$rules['offer_price'] = '';
            $validation::$rules['weight'] = '';
            $validation::$rules['warranty'] = '';
            $validation::$rules['base_diff_text'] = 'required'; //base diff text is compulsory while creating variant

            if ($validation->passes()) {

                $name = Input::get('name');
                $code = Input::get('code');
                $shortcode = Input::get('shortcode');
                $sap_code = Input::get('sap_code');
                $description = Input::get('description');
                $description_secondary = Input::get('description_secondary');
                $active = Input::get('active');
                $is_delivered = Input::get('delivered');
                $is_ltw = Input::get('ltw');
                $is_cod = Input::get('cod');
                $is_available = Input::get('available');
                $warranty = Input::get('warranty');
                $list_price = Input::get('list_price');
                $offer_price = Input::get('offer_price');
                $weight = Input::get('weight');
                $sequence = Input::get('sequence');
                $base_product_id = $product_id; //base product id is same as the product
                $base_diff_text = Input::get('base_diff_text');
                $popularity = Input::get('popularity');
                $meta_title = Input::get('meta_title');
                $meta_description = Input::get('meta_description');
                $meta_keywords = Input::get('meta_keywords');

                $script = Input::get('script');
                $script = isset($script) ? htmlentities($script) : null;

                $css = Input::get('css');
                $css = isset($css) ? htmlentities($css) : null;

                $is_demo = Input::get('demo', null);

                if ($sequence == 'after') {
                    $sequence = Input::get('after');
                }

                $product = $this->productService->getProductBasicInfo($product_id, null, null, null);

                $category_id = $product->category_id;


                $variant_product = $this->productService->createProduct($name, $code, $shortcode, $description,
                    $description_secondary, $category_id, $base_product_id, $active, $is_delivered, $is_ltw, $is_cod,
                    $warranty, $list_price, $offer_price, $weight, $sequence, $base_diff_text, $popularity,
                    $sap_code, $meta_title, $meta_description, $meta_keywords, $script, $css, $is_demo,$is_available);

                $variant_product_id = $variant_product->id;

                $image = AppUtil::getProductPrimaryImage($product_id); //get product image info and add it to variant
                if (!is_null($image)) {
                    $image_path = $image['image_path'];
                    $image_name = $image['image_name'];
                    $image_title = $image['image_title'];
                    $image_caption = $image['image_caption'];
                    $image_notes = $image['image_notes'];
                    $this->productService->createProductImage($image_path, $image_name, $image_title, $image_caption, $image_notes, 1, $variant_product_id);
                }

                Notification::success("Variant has been created successfully");
                return Redirect::to("dashboard/products/edit/$variant_product_id")->with(array('variant_product_id' => $variant_product_id, 'product_id' => $product_id));

            } else {

                $error = $validation->getErrors();
                return Redirect::to("dashboard/variant/create/$product_id")->withInput()->withErrors($error);
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    public function store()
    {
        //
    }


} 