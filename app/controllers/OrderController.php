<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 2/8/14
 * Time: 11:22 AM
 */
class OrderController extends BaseController
{

    const ACCESS_CODE = 'A88559E3';
    const SECURE_SECRET = "908E0989D7FE75DE5892D942957119B3";
    const MERCHANT_ID = '00100336';


    function __construct(ProductService $productService, OrderService $orderService,
                         CartService $cartService, ComboService $comboService, UserService $userService,
                         PgResponseService $pgResponseService, CouponService $couponService, SmsSend $smsSend, SettingsService $settingsService, MailRecordsService $mailRecordsService)
//
    {
        $this->beforeFilter('auth_user');

        $this->productService = $productService;
        $this->orderService = $orderService;
        $this->cartService = $cartService;
        $this->comboService = $comboService;
        $this->userService = $userService;
        $this->pgResponseService = $pgResponseService;
        $this->couponService = $couponService;
        $this->SmsSend = $smsSend;
        $this->settingservice = $settingsService;
        $this->mailService = $mailRecordsService;

    }

    public function getIndex()
    {
        $items = $this->cartService->getCartContents();
        $total_price = $this->cartService->getTotalPrice();
        $final_price = $total_price;
        $total_discount = 0;


        //coupon logic starts here
        $coupon_code = Session::get('coupon_info.coupon_code');
        $main_coupon_id = Session::get('coupon_info.main_coupon_id');

        $coupon_applied = false;

        if (isset($coupon_code) && isset($main_coupon_id)) { //check whether the coupon is applicable on cart items or not
            if (isset($items)) {
                foreach ($items as $item) {
                    $custom_id = $item['id'];
                    $array = explode("_", $custom_id);
                    $item_type = $array[0];
                    $id = $array[1];
                    $item_price = $item['price'];
                    if ($item_type == 'product') {
                        $product = $this->productService->getProductBasicInfo($id, null, null, null);

                        //check whether the product is excluded from offer or not
                        if ($product->exclude_offer == 1) {
                            continue;
                        }

                        $category_id = $product->category->id;
                        $main_coupon_category = $this->couponService->getMainCouponCategory($main_coupon_id, $category_id);

                        if (isset($main_coupon_category)) { //check that whether main coupon is applicable on product category
                            $main_coupon_info = $this->couponService->getMainCoupon($main_coupon_id);
                            $main_coupon_min_value = $main_coupon_info->min_value;
                            $main_coupon_discount = $main_coupon_info->percentage;

                            if ($total_price >= $main_coupon_min_value) { //total order value should be greater then main coupon min_value
                                $discounted_price = ($item_price * ($main_coupon_discount / 100));
                                $final_price = $final_price - $discounted_price;
                                $total_discount = $total_discount + $discounted_price;
                                $coupon_applied = true;
                            }
                        }
                    }
                }
            }
        }

        //if no coupon is applied then unset the coupon code and main coupon id in session
        if ($coupon_applied == false) {
            Session::set("coupon_info", null);
        }

        //coupon logic ends here

        Session::set('final_price', $final_price);

        $data['total_price'] = $total_price;
        $data['final_price'] = $final_price;
        $data['total_discount'] = $total_discount;

        if (isset($items)) {
            $data['items'] = $items;
            $is_cod_applicable = $this->orderService->isCodApplicable($items);
            $is_cod_available = Session::get('is_cod_available');

            if ($is_cod_applicable == true && $is_cod_available == true) {
                $data['cod_applicable'] = true;
            } else {
                $data['cod_applicable'] = false;
            }

        }

        $user_id = AppUtil::getCurrentUserId();
        $data['user'] = $this->userService->getUser($user_id, null, null);
		
		$data['searchProducts'] = $this->productService->getSearchProducts();
        return View::make('frontoffice.user.order_summary', $data);
    }

    public function getRemoveCoupon()
    {
        Session::set("coupon_info", null);
        return Redirect::to('/order');
    }

    public function postApplyCoupon()
    {
        $coupon_code = Input::get('coupon_code');
        $is_coupon_valid = $this->couponService->is_coupon_valid($coupon_code);
        if ($is_coupon_valid == true) {
            $coupon = $this->couponService->getCoupon(null, $coupon_code);

            $is_coupon_used = $coupon->order_id;
            $main_coupon_id = $coupon->coupon_main_id;
            $main_coupon = $this->couponService->getMainCoupon($main_coupon_id);
            $is_coupon_unique = $main_coupon->is_unique;
            $is_date_expire = ($main_coupon->expiry_date > date("Y-m-d")) ? true : false;

            if ($is_date_expire == true) {
                if ($is_coupon_unique == 1) {
                    $main_coupon_id = $coupon->coupon_main_id;
                    $coupon_id = $coupon->id;
                    Session::set("coupon_info.coupon_code", $coupon_code);
                    Session::set("coupon_info.coupon_id", $coupon_id);
                    Session::set("coupon_info.main_coupon_id", $main_coupon_id);
                    Session::set("coupon_info.is_unique", $is_coupon_unique);

                    return Redirect::to('/order');
                } else {
                    if (isset($is_coupon_used)) {
                        Notification::error("Applied coupon is already used.Apply another coupon code to get discount on following product.");
                        return Redirect::to('/order');
                    } else {
                        $main_coupon_id = $coupon->coupon_main_id;
                        $coupon_id = $coupon->id;
                        Session::set("coupon_info.coupon_code", $coupon_code);
                        Session::set("coupon_info.coupon_id", $coupon_id);
                        Session::set("coupon_info.main_coupon_id", $main_coupon_id);
                        Session::set("coupon_info.is_unique", $is_coupon_unique);

                        return Redirect::to('/order');
                    }
                }
            } else {
                Notification::error("Applied coupon is expired");
                return Redirect::to('/order');
            }
        } else {
            Notification::error("Applied coupon is invalid");
            return Redirect::to('/order');
        }
    }

    //todo:check how will be item notes be created
    public function getCreateOrder() //this function creates the order
    {
        try {

            $payment_type = Input::get('payment_type');
            $user_id = AppUtil::getCurrentUserId();
            $cart_id = $this->cartService->getCartId();
            $cart_items = $this->cartService->getCartContents();
            $final_price = Session::get('final_price');
            $coupon_id = Session::get('coupon_info.coupon_id');
            $is_coupon_unique = Session::get('coupon_info.is_unique');

            $net_value = $this->cartService->getTotalPrice();
            $final_value = $final_price; //todo:check whether net value and final value will be same
            $order_notes = Input::get('comments');
            if (empty($order_notes)) {
                $order_notes = null;
            }

            $order_status = 'new';
            $payment_status = 'pending';

            $settings = $this->settingservice->getSettingByLabel('COD_limit');
            $cod_limit = $settings->value;

            //no cart exist corresponding to user return back
            if (!isset($cart_id)) {
                return "No cart exist";
            }

            //check if shipping address and billing address are present in session
            $shipping_address_id = Session::get("shipping_address_id");
            $billing_address_id = Session::get("billing_address_id");
            if ($shipping_address_id == false || $billing_address_id == false) {
                return Redirect::to("user/address"); //if not return user to address page
            }

            //generate verification code if the payment mode is cod
            if ($payment_type == 'cod') {
                if ($final_value > $cod_limit) {
                    Notification::error('Sorry we can not accept order above Rs. 15000. You are requested to please choose Pre Payment Option. For further detail call us at 18001801222.');
                    return Redirect::back();
                } else {
                    $verification_code = OrderService::generateVerificationCode();
                    $payment_mode = 'cod';
                }

            } else {
                $verification_code = null;
                $payment_mode = 'online';
            }

            $items_array = array();
            foreach ($cart_items as $item) {

                $custom_id = $item['id'];
                $array = explode("_", $custom_id);
                $item_type = $array[0];
                $id = $array[1];

                if ($item_type == "product") {

                    $product = $this->productService->getProduct($id, null, null, null);
                    $data = array(
                        'item_id' => $id,
                        'item_type' => 'product',
                        'offer_price' => $product->offer_price, //todo:change this list price to offer price
                        'list_price' => $product->list_price,
                        'qty' => $item['qty']
                    );
                    $items_array[] = $data;

                } elseif ($item_type == "combo") {

                    $combo = $this->comboService->getCombo($id, null);
                    $data = array(
                        'item_id' => $id,
                        'item_type' => 'combo',
                        'offer_price' => $combo->combo_price,
                        'list_price' => null,
                        'qty' => $item['qty']
                    );
                    $items_array[] = $data;
                }
            }

//            $reference = Str::random(10);
            $reference = substr(number_format(time() * mt_rand(), 0, '', ''), 0, 10);

            $order = $this->orderService->createOrder($user_id, $net_value, $final_value, $order_status, $payment_status, $order_notes,
                $reference, $cart_id, $payment_mode, $shipping_address_id, $billing_address_id,
                $verification_code, $coupon_id, $items_array);

            //update order id in coupon table

            if (isset($coupon_id) && $is_coupon_unique == 0) {
                $this->couponService->updateCoupon($coupon_id, $order->id);
                Session::set("coupon_info", null);
            }

            Event::fire("order.created", array('order' => $order));

            //if the payment mode is COD confirm mobile verification
            if ($payment_mode == 'cod') {
                $order_id = $order->id;
                return Redirect::to("/order/resend-verification-code/$order_id");

            } else {
                $final_value = $order->final_value;
                $data['total_price'] = $final_value;
                $data['vpc_MerchTxnRef'] = $reference;
                $data['vpc_OrderInfo'] = $order->id;
                return View::make('frontoffice.user.create_order', $data);
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


//    public function getMobileConfirmation($order_id)
//    {
//        try {
//            $order = $this->orderService->getOrder($order_id);
//
//            if (isset($order)) {
//                $address = $this->userService->getAddress($order['billing_address_id']);
//                if (!empty($address)) {
//                    if (isset($address->mobile)) {
//                        Session::set("order_confirmation_mobile", $address->mobile);
//                        $data['mobile'] = $address->mobile;
//                    } else {
//                        $user = $this->userService->getUser($order->user_id, null, null);
//                        Session::set("order_confirmation_mobile", $user->mobile);
//                        $data['mobile'] = $user->mobile;
//                    }
//
//                }
//                $data['order_id'] = $order_id;
//                return View::make("frontoffice.cod.mobile_confirmation", $data);
//            } else {
//                return View::make('404');
//            }
//        } catch (Exception $ex) {
//            Log::error($ex);
//            throw $ex;
//        }
//    }


    public function postMobileConfirmation($order_id)
    {
        try {

            $mobile = Input::get("mobile");
            $is_number_changed = Input::get('is_number_changed');
            if (!empty($is_number_changed)) {
                Session::set("order_confirmation_mobile", $is_number_changed);
            } else {
                Session::set("order_confirmation_mobile", $mobile);
            }

            $order = $this->orderService->getOrder($order_id);
            $user = $this->userService->getUser($order->user_id, null, null);
            $this->userService->updateUserMobile($order->user_id, Session::get("order_confirmation_mobile"));
//            Session::set("order_confirmation_mobile", $user->mobile);

            $data['mobile'] = $user->mobile;
            $data['order_id'] = $order_id;
            if (isset($order)) {
                $verification_code = OrderService::generateVerificationCode();
                $this->orderService->updateVerificationCode($order_id, $verification_code);

                $sms = array();
                $sms['sender_id'] = AppUtil::getCurrentUserId();
                $sms['mobile'] = Session::get("order_confirmation_mobile");

                $message = urlencode('Your one time password for user account with GLEN is ' . $verification_code . ', valid for 2 hours. Kindly enter this OTP as prompted by IVR.');
                $message = str_replace("%2C", ",", $message);
                $sms['message'] = $message;

                $event = Event::fire("verification_code.send", array($sms)); //fire verification code sending event
//
                if ($event == false) {
                    Notification::error('Sorry,your one time password could not sent via SMS.To again get verification code please click resend code');
                } else {
                    Notification::success('Your one time password has been resent to your mobile successfully');
                }
//                return View::make("frontoffice.cod.mobile_confirmation", $data);
                return Redirect::to("/order/verify-code/$order_id");
            } else {
                return View::make('404');
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getVerifyCode($order_id)
    {
        try {

            $order = $this->orderService->getOrder($order_id);
            if (isset($order)) {
                $data['order_id'] = $order_id;
                $data['mobile'] = Session::get("order_confirmation_mobile");
				$data['searchProducts'] = $this->productService->getSearchProducts();
//                return View::make("frontoffice.cod.verify_code", $data);
                return View::make("frontoffice.cod.mobile_confirmation", $data);
            } else {
                return View::make('404');
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function postVerifyCode($order_id)
    {
        try {

            $verification_code = Input::get('verification_code');
            $order = $this->orderService->getOrder($order_id);
            $reference = $order->reference;

            if (isset($order)) {
                $saved_verification_code = $order->verification_code;
                if ($verification_code == $saved_verification_code) {

//                    $this->orderService->updateStatus($order_id, "new");
//                    $this->orderService->updatePaymentStatus($order_id, "pending");

                    $order_date = $order->created_at;

                    $order_data = array('order_id' => $reference, 'order_date' => $order_date, 'payment_method' => $order->payment_mode, 'net_value' => $order->net_value, 'final_value' => $order->final_value, 'notes' => $order->notes);

                    $cart = $this->cartService->getCartContents();

                    //send email to customer //Todo::change email id
                    $email = AppUtil::getUserEmail();
                    $msgdata = array(
                        'to_emailId' => $email,
                        'from_emailId' => 'enquiry@glenindia.com',
                        'email_subject' => 'Order Confirmation - #' . $reference,
                        'email_template' => 'emails.order.order_customer_template',
                        'order' => $order_data,
                        'order_items' => $cart
                    );
                    Event::fire("order.success", array($msgdata));

                    $user_name = AppUtil::getUserName();
                    $mail_data = array();
                    $mail_data = $msgdata['order'] + array('order_items' => $msgdata['order_items']);
                    $view = View::make($msgdata['email_template'], $mail_data);
                    $data['email_content'] = $view->render();
                    $this->mailService->createMailRecord($user_name, $msgdata['to_emailId'], $msgdata['from_emailId'], $msgdata['email_subject'], $data['email_content']);

                    $message = urlencode('Your order is placed and order no. is ' . $reference . ', you will receive a confirmation call for order, kindly confirm for timely dispatch.');
                    $message = str_replace("%2C", ",", $message);

                    $smsdata = array(
                        'mobile' => Session::get("order_confirmation_mobile"),
                        'sender_id' => AppUtil::getCurrentUserId(),
                        'message' => $message
                    );

                    Event::fire("order.success_sendSMS", array($smsdata));

                    //send email to admin
                    $msgdata = array(
                       'to_emailId' => array('enquiry@glenindia.com', 'cr5@glenindia.com', 'arun@glenindia.com'),
//                        'to_emailId' => array('ptailor@greenapplesolutions.com'),
//                        'cc_emailID' => 'arun@glenindia.com',
//                        'cc_emailID' => 'ngupta@greenapplesolutions.com',
                        'from_emailId' => 'enquiry@glenindia.com',
                        'email_subject' => 'Order Confirmation - #' . $reference,
                        'email_template' => 'emails.order.order_admin_template',
                        'order' => $order_data,
                        'order_items' => $cart
                    );
                    Event::fire("order.success", array($msgdata));


                    $view = View::make($msgdata['email_template'], $mail_data);
                    $data['email_content'] = $view->render();
                    $this->mailService->createMailRecord('Admin', join(',', $msgdata['to_emailId']), $msgdata['from_emailId'], $msgdata['email_subject'], $data['email_content']);

                    $cart_id = $order->cart_id;
                    if (isset($cart_id)) { //destroy the cart items
                        $this->cartService->destroyDbCart($cart_id);
                    }

                    return Redirect::to("/order/success/" . $order_id);

                } else {
                    Event::fire("order.fail", array('order' => $order));
                    Notification::error('Sorry! You have entered an incorrect One Time Password. Please re-enter the correct One Time Password.');
                    return Redirect::to("/order/verify-code/$order_id");
                }
            } else {
                return View::make('404');
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getResendVerificationCode($order_id)
    {
        $verification_code = OrderService::generateVerificationCode();
        $this->orderService->updateVerificationCode($order_id, $verification_code);

        $sms = array();
        $sms['sender_id'] = AppUtil::getCurrentUserId();
        if (Session::get("order_confirmation_mobile") == null) {
            $user_id = AppUtil::getCurrentUserId();
            $user = $this->userService->getUser($user_id, null, null);
            $sms['mobile'] = $user->mobile;
            Session::set("order_confirmation_mobile", $user->mobile);
        } else {
            $sms['mobile'] = Session::get("order_confirmation_mobile");
        }

        $message = urlencode('Your one time password for user account with GLEN is ' . $verification_code . ', valid for 2 hours. Kindly enter this OTP as prompted by IVR.');
        $message = str_replace("%2C", ",", $message);
        $sms['message'] = $message;

        $event = Event::fire("verification_code.send", array($sms)); //fire verification code sending event
        if ($event == false) {
            Notification::error('Sorry,your one time password could not sent via SMS.To again get verification code please click resend code');
        } else {
            Notification::success('Your one time password has been sent to your mobile successfully');
        }

        return Redirect::to("/order/verify-code/$order_id");
    }


    /** accepts all the hidden variables and redirect to the payment gateway
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function postCreateOrder()
    {
        try {

            $vpcURL = $_POST["virtualPaymentClientURL"] . "?";
            unset($_POST["virtualPaymentClientURL"]);
            unset($_POST["SubButL"]);

            $md5HashData = self::SECURE_SECRET;
            ksort($_POST);

            // set a parameter to show the first pair in the URL
            $appendAmp = 0;

            foreach ($_POST as $key => $value) {

                // create the md5 input and URL leaving out any fields that have no value
                if (strlen($value) > 0) {

                    // this ensures the first parameter of the URL is preceded by the '?' char
                    if ($appendAmp == 0) {
                        $vpcURL .= urlencode($key) . '=' . urlencode($value);
                        $appendAmp = 1;
                    } else {
                        $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
                    }
                    $md5HashData .= $value;
                }
            }

            // Create the secure hash and append it to the Virtual Payment Client Data if
            // the merchant secret has been provided.
            if (strlen(self::SECURE_SECRET) > 0) {
                $vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
            }
//            Event::fire("order.success", array());
            return Redirect::to($vpcURL);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    /** triggered when the payment gateway responds to the transaction
     * @return string
     * @throws Exception
     */
    public function getPgResponse()
    {
        try {

            $amount = (Request::query('vpc_Amount') / 100);
            $batch_no = Request::query('vpc_BatchNo');
            $command = Request::query('vpc_Command');
            $locale = Request::query('vpc_Locale');
            $merchant_txn_ref = Request::query('vpc_MerchantTxnRef');
            $merchant = Request::query('vpc_Merchant');
            $message = Request::query('vpc_Message');
            $vpc_order_info = Request::query('vpc_OrderInfo'); //contains the order_id
            $transaction_no = Request::query('vpc_TransactionNo');
            $txn_response_code = Request::query('vpc_TxnResponseCode');
            $version = Request::query('vpc_Version');

            $cart_id = $this->cartService->getCartId();

            $pg_response = $this->pgResponseService->addResponse($amount, $batch_no, $command, $locale, $merchant_txn_ref,
                $merchant, $message, $vpc_order_info, $transaction_no, $txn_response_code, $version, null);

            $pg_response_id = $pg_response->id;
            $response_data = $this->pgResponseService->getResponse($pg_response_id, null, null);
            $pg_response_order_info = $response_data->order_info; //returns order id
            $pg_response_amount = $response_data->amount;

            $order = $this->orderService->getOrder($pg_response_order_info);
            $reference = $order->reference;
            $order_date = $order->created_at;

            if ($txn_response_code == 0) { //transaction is successful

                if (isset($order)) { //order exists in database

                    $order_id = $order->id;
                    $final_value = round($order->final_value, 2);

                    if ($final_value == $pg_response_amount) { //order amount is equal to the paid amount

                        //closed - dispatched
                        $this->orderService->updateStatus($order_id, "new");
                        $this->orderService->updatePaymentStatus($order_id, "paid", null);
                        $this->pgResponseService->updatePaymentStatus($order_id, "success");

                        $order_date = $order->created_at;

                        $order_data = array('order_id' => $reference, 'order_date' => $order_date, 'payment_method' => $order->payment_mode, 'net_value' => $order->net_value, 'final_value' => $order->final_value);

                        $cart = $this->cartService->getCartContents();

//                      //send email to customer
                        $email = AppUtil::getUserEmail();

                        $msgdata = array(
                            'to_emailId' => $email,
                            'from_emailId' => 'enquiry@glenindia.com',
                            'email_subject' => 'Order Confirmation - #' . $reference,
                            'email_template' => 'emails.order.order_customer_template',
                            'order' => $order_data,
                            'order_items' => $cart
                        );
                        Event::fire("order.success", array($msgdata));

                        $user_name = AppUtil::getUserName();
                        $mail_data = array();
                        $mail_data = $msgdata['order'] + array('order_items' => $msgdata['order_items']);
                        $view = View::make($msgdata['email_template'], $mail_data);
                        $data['email_content'] = $view->render();
                        $this->mailService->createMailRecord($user_name, $msgdata['to_emailId'], $msgdata['from_emailId'], $msgdata['email_subject'], $data['email_content']);

                        $order_date = strtotime($order_date . ' + 2 day');
                        $message = urlencode('Dear Customer,%0AThanks for placing order no. ' . $reference . ', EST DISPATCH DATE ' . $order_date . '. Once dispatched it will reach you in max 7-10 days. Please check email for details');
                        $message = str_replace("%2C", ",", $message);
                        $message = str_replace("%25", "%", $message);

                        $smsdata = array(
                            'mobile' => Session::get("order_confirmation_mobile"),
                            'sender_id' => AppUtil::getCurrentUserId(),
                            'message' => $message
                        );

                        Event::fire("order.success_sendSMS", array($smsdata));

                        //send email to admin
                        $msgdata = array(
                            'to_emailId' => array('enquiry@glenindia.com', 'cr5@glenindia.com', 'arun@glenindia.com'),
//                            'to_emailId' => array('ptailor@greenapplesolutions.com'),
//                            'cc_emailID' => 'ngupta@greenapplesolutions.com',
//                            'cc_emailID' => 'arun@glenindia.com',
                            'from_emailId' => 'enquiry@glenindia.com',
                            'email_subject' => 'Order Confirmation - #' . $reference,
                            'email_template' => 'emails.order.order_admin_template',
                            'order' => $order_data,
                            'order_items' => $cart
                        );
                        Event::fire("order.success", array($msgdata));


                        $view = View::make($msgdata['email_template'], $mail_data);
                        $data['email_content'] = $view->render();
                        $this->mailService->createMailRecord('Admin', join(',', $msgdata['to_emailId']), $msgdata['from_emailId'], $msgdata['email_subject'], $data['email_content']);

                        if (isset($cart_id)) { //destroy the cart i tems
                            $this->cartService->destroyDbCart($cart_id);
                        }

                        return Redirect::to('/order/success/' . $order_id);

                    } else { //user has done fraud by changing amount closed-cancelled pending-failed
                        $this->orderService->updateStatus($order_id, "cancelled");
                        $this->orderService->updatePaymentStatus($order_id, "failed", null);

                        $this->pgResponseService->updatePaymentStatus($order_id, "user fraud");

                        Event::fire("order.fail", array('order' => $order));

                        Notification::error("Sorry some error occurred during the transaction");
                        return Redirect::to('/order/fail');
                    }
                } else { //order does not exist in database
//                    return "Sorry some error occurred during the transaction";

                    Event::fire("order.fail", array('order' => $order));
                    return Redirect::to('/order/fail');
                }

            } else { //error in transaction

                $response_description = $this->getResponseDescription($txn_response_code);

                if (isset($order)) { //order exists in database closed-cancelled unpaid-failed

                    $order_id = $order->id;
                    $this->orderService->updateStatus($order_id, "cancelled");
                    $this->orderService->updatePaymentStatus($order_id, "failed", null);
                    $this->pgResponseService->updatePaymentStatus($order_id, $response_description);
                }
                return Redirect::to('/order/fail');
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    /** returns payment gateway code description
     * @param $responseCode
     * @return string
     */
    private function getResponseDescription($responseCode)
    {

        switch ($responseCode) {
            case "0" :
                $result = "Transaction Successful";
                break;
            case "?" :
                $result = "Transaction status is unknown";
                break;
            case "1" :
                $result = "Unknown Error";
                break;
            case "2" :
                $result = "Bank Declined Transaction";
                break;
            case "3" :
                $result = "No Reply from Bank";
                break;
            case "4" :
                $result = "Expired Card";
                break;
            case "5" :
                $result = "Insufficient funds";
                break;
            case "6" :
                $result = "Error Communicating with Bank";
                break;
            case "7" :
                $result = "Payment Server System Error";
                break;
            case "8" :
                $result = "Transaction Type Not Supported";
                break;
            case "9" :
                $result = "Bank declined transaction (Do not contact Bank)";
                break;
            case "A" :
                $result = "Transaction Aborted";
                break;
            case "C" :
                $result = "Transaction Cancelled";
                break;
            case "D" :
                $result = "Deferred transaction has been received and is awaiting processing";
                break;
            case "F" :
                $result = "3D Secure Authentication failed";
                break;
            case "I" :
                $result = "Card Security Code verification failed";
                break;
            case "L" :
                $result = "Shopping Transaction Locked (Please try the transaction again later)";
                break;
            case "N" :
                $result = "Cardholder is not enrolled in Authentication scheme";
                break;
            case "P" :
                $result = "Transaction has been received by the Payment Adaptor and is being processed";
                break;
            case "R" :
                $result = "Transaction was not processed - Reached limit of retry attempts allowed";
                break;
            case "S" :
                $result = "Duplicate SessionID (OrderInfo)";
                break;
            case "T" :
                $result = "Address Verification Failed";
                break;
            case "U" :
                $result = "Card Security Code Failed";
                break;
            case "V" :
                $result = "Address Verification and Card Security Code Failed";
                break;
            default  :
                $result = "Unable to be determined";
        }
        return $result;
    }

    /** renders the success view
     * @return \Illuminate\View\View
     */
    public function getSuccess($order_id)
    {
        //echo $order_id;
        $order = $this->orderService->getOrder($order_id);
        if (!empty($order) && !empty($order['items'])) {
            $items = $order['items'];

            $item_ids = array();
            foreach ($items as $item) {
                array_push($item_ids, $item['item_id']);
            }

            $data['item_ids'] = $item_ids;
            $data['total_value'] = $order->final_value;
        }
		        $data['searchProducts'] = $this->productService->getSearchProducts();
//        echo "<pre>";print_r($order);echo "</pre>";exit;
        return View::make("frontoffice.order.success", $data);
    }

    /** renders the failure view
     * @return \Illuminate\View\View
     */
    public function getFail()
    {
        return View::make("frontoffice.order.fail");
    }


    public function getMyOrders()
    {
        $data['orders'] = $this->orderService->getOrders(AppUtil::getCurrentUserId(), null, null, null, null, null, null, false);
		
		$data['searchProducts'] = $this->productService->getSearchProducts();

        return View::make('frontoffice.user.order_history', $data);
    }

    public function getOrderItems($order_id)
    {
        $order = $this->orderService->getOrder($order_id);

        $items = array();
        foreach ($order->items as $i => $row) {

            $item_id = $row->item_id;
            $item_type = $row->item_type;

            $items[$i] = array(
                'item_id' => $item_id,
                'item_type' => $item_type,
                'offer_price' => $row->offer_price,
                'list_price' => $row->list_price,
                'qty' => $row->qty,
                'notes' => $row->notes,
            );

            if ($row->item_type == 'product') {
                $product = $this->productService->getProductBasicInfo($item_id, null, null, null);
                $product_name = isset($product) ? $product->name : "";
                $items[$i]['item_name'] = $product_name;
            } else {
                $combo = $this->comboService->getComboProducts($item_id);
                $combo_products = $combo->products;
                foreach ($combo_products as $single_product) {
                    $items[$i]['item_name'][] = $single_product->name;
                }

            }
        }
        $data['items'] = $items;

        return View::make('frontoffice.user.order_items', $data);
    }
}