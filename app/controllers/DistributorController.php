<?php

class DistributorController extends \BaseController
{

    function __construct(DistributorsService $distributorsService,ProductService $productService)
    {
        $this->disributorService = $distributorsService;
        $this->productService = $productService;
    }

    public function getIndex()
    {
        $data['states'] = $this->disributorService->getUniqueStates();

        $states = array();
        foreach ($data['states'] as $data['state']) {
            array_push($states, $data['state']->state);
        }

        $distributors = $this->disributorService->getDistributors(null, null, 1, null);
        $data['distributors'] = array();
        $distributor = array();
        for ($i = 0; $i < count($distributors); $i++) {
            if (in_array($distributors[$i]->state, $states)) {

                if (!empty($data['distributors'][$distributors[$i]->state])) {
                    array_push($data['distributors'][$distributors[$i]->state], $distributors[$i]);
                } else {
                    if (empty($data['distributors'][$distributors[$i]->state])) {
                        $data['distributors'][$distributors[$i]->state] = array();
                    }
                    array_push($data['distributors'][$distributors[$i]->state], $distributors[$i]);
                }
            }
        }

        $data['searchProducts'] = $this->productService->getSearchProducts();

        return View::make('frontoffice.distributors', $data);
    }


}
