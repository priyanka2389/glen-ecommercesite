<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 2/28/14
 * Time: 11:14 AM
 */
class DashboardSettingsController extends BaseController
{
    function __construct(SettingsService $settingsService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->settingsService = $settingsService;
    }

    public function getIndex()
    {
        try {

            $data['settings'] = $this->settingsService->getSettings();
            return View::make('dashboard.settings.index', $data);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getCreate()
    {
        return View::make('dashboard.settings.create');
    }

    public function postCreate()
    {
        try {

            $validation = new \services\Validators\settingsValidator();
            if ($validation->passes()) {
                $setting_label = Input::get('setting_label');
                $setting_value = Input::get('setting_value');

                $this->settingsService->createSetting($setting_label,$setting_value);

                Notification::success("Setting has been saved successfully");
                return Redirect::to("dashboard/settings");

            } else {
                $errors = $validation->getErrors();
                return Redirect::to("dashboard/settings/create")->withInput()->withErrors($errors);
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getEdit($id)
    {
        $data['setting'] =  $this->settingsService->getSetting($id);
        return View::make('dashboard.settings.edit', $data);
    }

    public function postEdit($id)
    {
        try {

            $validation = new \services\Validators\settingsValidator();

            if ($validation->passes()) {
                $setting_label = Input::get('setting_label');
                $setting_value = Input::get('setting_value');

                $this->settingsService->updateSetting($id, $setting_label, $setting_value);
                Notification::success("Settings has been updated successfully");
                return Redirect::to('dashboard/settings');

            } else {

                $errors = $validation->getErrors();
                return Redirect::to("dashboard/settings/edit/$id")->withInput()->withErrors($errors);
            }


        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }
//
//    public function getDelete($id)
//    {
//        $this->tagService->deleteTag($id);
//        return Redirect::to("dashboard/tags");
//    }
} 