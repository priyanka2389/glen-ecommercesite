<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 9/15/14
 * Time: 4:54 PM
 */
class DashboardPageController extends BaseController
{
    function __construct(PageService $pageService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->pageService = $pageService;
    }

    public function getIndex()
    {
        $data['pages'] = $this->pageService->getPages();

        return View::make('dashboard.page.index', $data);
    }

    public function getCreate()
    {
        return View::make('dashboard.page.create');
    }

    public function postCreate()
    {
        try {
            $validator = new \services\Validators\PageValidator();
            if ($validator->passes()) {
                $title = Input::get('title');
                $short_code = Input::get('short_code');
                $source_code = Input::get('source_code');

                $active = Input::get('active');
                $active = isset($active) ? true : false;

                $this->pageService->createPage($title, $short_code, $source_code, $active);
                Notification::success("Page has been added successfully");
                return Redirect::to("dashboard/pages");
            } else {
                $error = $validator->getErrors();
                return Redirect::to("dashboard/pages/create")->withInput(Input::all())->withErrors($error);
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getEdit($id)
    {
        $data['page'] = $this->pageService->getPage($id,null,null);
        return View::make('dashboard.page.edit', $data);
    }

    public function postEdit($id)
    {
        try {

            $validator = new \services\Validators\PageValidator();
            $validator::$rules['title'] .= ",$id";
            $validator::$rules['short_code'] .= ",$id";
            if ($validator->passes()) {
                $title = Input::get('title');
                $short_code = Input::get('short_code');
                $source_code = Input::get('source_code');

                $active = Input::get('active');
                $active = isset($active) ? true : false;

                $this->pageService->updatePage($id,$title, $short_code, $source_code, $active);
                Notification::success("Page has been edited successfully");
                return Redirect::to("dashboard/pages");
            } else {
                $error = $validator->getErrors();
                return Redirect::to("dashboard/pages/edit/$id")->withErrors($error);
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getDelete($id)
    {
        $this->pageService->deletePages(array($id));
        return Redirect::to("dashboard/pages");
    }

    public function getActivateOrDeactivate($id, $status)
    {
        $this->pageService->activateOrDeactivate($id, $status);
        return Redirect::to('dashboard/pages');
    }

} 