<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 4:54 PM
 */
class DashboardDemoController extends BaseController
{
    function __construct(DemoService $demoService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->demoService = $demoService;
    }

    public function getIndex()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? "0000-00-00" : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $data['demos'] = $this->demoService->getDemos($from_date, $to_date, Constants::DASHBOARD_DEMO_PAGE_COUNT);
        return View::make('dashboard.demo', $data);
    }

    public function getDownloadCsv()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? "0000-00-00" : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $demos = $this->demoService->getDemos($from_date, $to_date, Constants::DASHBOARD_DEMO_PAGE_COUNT);
        $this->demoService->getDemoCsv($demos);
    }

    public function getDelete($id)
    {
        $this->demoService->deleteDemo($id);
        return Redirect::to("dashboard/demo");
    }

} 