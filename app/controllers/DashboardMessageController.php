<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 9/15/14
 * Time: 4:54 PM
 */
class DashboardMessageController extends BaseController
{
    function __construct(MessageService $messageService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->messageService = $messageService;
    }

    public function getIndex()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? "0000-00-00" : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $data['messages'] = $this->messageService->getMessages($from_date, $to_date, Constants::DASHBOARD_MESSAGE_PAGE_COUNT);
        return View::make('dashboard.message', $data);
    }

    public function getDownloadCsv()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? "0000-00-00" : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $demos = $this->messageService->getMessages($from_date, $to_date, Constants::DASHBOARD_MESSAGE_PAGE_COUNT);
        $this->messageService->getMessageCsv($demos);
    }

    public function getDelete($id)
    {
        $this->messageService->deleteMessage($id);
        return Redirect::to("dashboard/message");
    }

} 