<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/26/14
 * Time: 3:32 PM
 */
class DashboardFeedbackController extends BaseController
{

    function __construct(FeedbackService $feedbackService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->feedbackService = $feedbackService;
    }

    public function getIndex()
    {
        try {

            $from_date = Input::get('from_date', null);
            $to_date = Input::get('to_date', null);
            $from_date = is_null($from_date) ? "0000-00-00" : $from_date;
            $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

            $data['feedbacks'] = $this->feedbackService->getFeedbacks($from_date, $to_date, Constants::DASHBOARD_ENQUIRES_PAGE_COUNT);

            return View::make("dashboard.feedback",$data);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getDownloadCsv()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? "0000-00-00" : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $demos = $this->feedbackService->getFeedbacks($from_date, $to_date, Constants::DASHBOARD_DEMO_PAGE_COUNT);
        $this->feedbackService->getFeedbackCsv($demos);
    }


    public function getDelete($id)
    {
        $this->feedbackService->deleteFeedback($id);
        return Redirect::to('dashboard/feedback');
    }

}