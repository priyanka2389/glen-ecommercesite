<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 19-Feb-15
 * Time: 1:56 PM
 */

class DashboardMailController extends \BaseController{
    function __construct(MailRecordsService $mailRecordsService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->mailService = $mailRecordsService;
    }

    public function getIndex(){
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? "0000-00-00" : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $data['mails'] = $this->mailService->getMails($from_date, $to_date,Constants::DASHBOARD_MAIL_PAGE_COUNT);

        return View::make('dashboard.mails.index',$data);
    }

    public function getInfo($mail_id){
        $data['mail'] = $this->mailService->getMail($mail_id);

        $view =  View::make('dashboard.mails.mail_info',$data);
        return $view->render();
    }
}