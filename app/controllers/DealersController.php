<?php

class DealersController extends BaseController
{

    function __construct(DealersService $dealers, RetailStoresService $retailStores, CategoryService $category)
    {
        $this->dealersService = $dealers;
        $this->retailStoresService = $retailStores;
        $this->categoryService = $category;
    }

    public function getIndex()
    {
        $data['rstates'] = $this->dealersService->getUniqueStates('retail_store');
        $data['dstates'] = $this->dealersService->getUniqueStates('dealer');
        $data['categories'] = $this->categoryService->getCategories(1, null, null, null, null);
        return View::make('frontoffice.retail_outlets', $data);
    }

    public function getAjaxStates($type)
    {
        $states = $this->dealersService->getUniqueStates($type);
        return Response::json($states);
    }

    public function getAjaxCities($state, $type)
    {
//        $state = Input::get('state');
        $cities = $this->dealersService->getCitiesForState($state, $type);
        return Response::json($cities);
    }

    public function getAjaxLocations($state,$city, $type)
    {
//        $state = Input::get('state');
        $cities = $this->dealersService->getLocationsForCities($state,$city, $type);
        return Response::json($cities);
    }

    public function postAjaxSubcategories()
    {
        $category = Input::get('category');
        $subcategories = $this->dealersService->getSubcategoriesForCategories($category);
        return Response::json($subcategories);
    }

    public function postAjaxDealers()
    {
        $category = Input::get('category');
        $state = Input::get('state1');
        $city = Input::get('city1');
        $location = (Input::get('location1') != 'Select Location') ? Input::get('location1') : null;
        $type = Input::get('dtype');
        if (isset($category)) {
            $large_appliance = ($category == 7) ? false : true;
            $small_appliances = ($category == 7) ? true : false;
        }

        $dealers = $this->dealersService->getData($city, $state,$location, $small_appliances, $large_appliance, $type);

        return Response::json($dealers);
    }

    public function postAjaxGalleries()
    {
        $state = Input::get('state');
        $city = Input::get('city');
        $location = (Input::get('location') != 'Select Location') ? Input::get('location') : null;
        $type = Input::get('rtype');
        $large_appliance = true;
        $small_appliances = true;

        $dealers = $this->dealersService->getData($city, $state,$location, $small_appliances, $large_appliance, $type);

        return Response::json($dealers);
    }

    public function getMigrateData()
    {
        $retail_stores = $this->retailStoresService->getRetailStores(null, null, null, null);

        foreach ($retail_stores as $retail_store) {

            $address1 = $retail_store->address;
            $city = $retail_store->city;
            $state = $retail_store->state;
            $pincode = $retail_store->pincode;
            $mobile = $retail_store->mobile;
            $phone = $retail_store->phone;
            $small_appliance = 1;
            $large_appliance = $retail_store->is_large_appliance;
            $active = $retail_store->is_active;
            $email_id = $retail_store->email_id;
            $contact_person = $retail_store->contact_person;
            $sequence = $retail_store->sequence;
            $type = 'retail_store';

            $this->dealersService->createDealer(null, $address1, null, $city, $state, $pincode, $mobile,
                $phone, $small_appliance, $large_appliance, $active, $sequence, $contact_person, $email_id, $type);
        }

    }
}
