<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/12/14
 * Time: 10:30 AM
 */
class DashboardServicesCentersController extends BaseController
{
    function __construct(ProductService $productService, ServicesCentersService $servicecentersService,DealersService $dealerService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->productService = $productService;
        $this->servicesCentersService = $servicecentersService;
        $this->dealersService = $dealerService;
    }

    public function getIndex()
    {
        $data['servicecenters'] = $this->servicesCentersService->getServiceCenters(null, null, Constants::DASHBOARD_SERVICECENTERS_PAGE_COUNT);

        return View::make('dashboard.Servicecenters.index', $data);
    }

    public function getCreate()
    {
        $data['servicecenters'] = $this->servicesCentersService->getServiceCenters(null, null, null);

        return View::make('dashboard.Servicecenters.create',$data);
    }

    public function postCreate()
    {
        try {
            $validator = new \services\Validators\ServiceCentersValidator();
            if ($validator->passes()) {
                $shop_name = Input::get('name');
                $address1 = Input::get('address1');
                $address2 = Input::get('address2', null);
                $city = Input::get('city');
                $state = Input::get('state');
                $pincode = Input::get('pincode');
                $mobile = Input::get('mobile');
                $contact_type = Input::get('contact_type');
                $phone = Input::get('phone', null);
                $contact_person = Input::get('contact_person');
                $email = Input::get('email', null);
                $sequence = Input::get('sequence');
                $small_appliance = Input::get('small_appliance');
                $small_appliance = isset($small_appliance) ? true : false;

                $large_appliance = Input::get('large_appliance');
                $large_appliance = isset($large_appliance) ? true : false;

                $active = Input::get('active');
                $active = isset($active) ? true : false;

                if ($sequence == 'after') {
                    $sequence = Input::get('after');
                }

                if($phone == null){
                    $contact_type = null;
                }
                $this->servicesCentersService->createServiceCenter($shop_name,$email, $address1, $address2, $city, $state, $pincode, $mobile,
                    $phone, $small_appliance, $large_appliance, $active,$sequence,$contact_type,$contact_person);
                Notification::success("Service Center has been added successfully");
                return Redirect::to("dashboard/service-centers");
            } else {
                $error = $validator->getErrors();
                return Redirect::to("service-centers/create")->withInput(Input::all())->withErrors($error);
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getEdit($id)
    {
        $data['servicecenter'] = $this->servicesCentersService->getServiceCenter($id);
        $data['sequence'] = $this->servicesCentersService->getServiceCenterPosition($id);
        $data['servicecenters'] = $this->servicesCentersService->getServiceCenters(null, null, null);
        return View::make('dashboard.Servicecenters.edit', $data);
    }

    public function postEdit($id)
    {
        try {

            $validator = new \services\Validators\ServiceCentersValidator();
            if ($validator->passes()) {
                $shop_name = Input::get('name');
                $address1 = Input::get('address1');
                $address2 = Input::get('address2', null);
                $city = Input::get('city');
                $state = Input::get('state');
                $pincode = Input::get('pincode');
                $mobile = Input::get('mobile');
                $contact_type = Input::get('contact_type');
                $phone = Input::get('phone', null);
                $contact_person = Input::get('contact_person');
                $email = Input::get('email');
                $sequence = Input::get('sequence');
                $small_appliance = Input::get('small_appliance');
                $small_appliance = isset($small_appliance) ? true : false;

                $large_appliance = Input::get('large_appliance');
                $large_appliance = isset($large_appliance) ? true : false;

                $active = Input::get('active');
                $active = isset($active) ? true : false;

                if ($sequence == 'after') {
                    $sequence = Input::get('after');
                }

                $this->servicesCentersService->updateServiceCenter($id,$email, $shop_name, $address1, $address2, $city, $state, $pincode, $mobile,
                    $phone, $small_appliance, $large_appliance, $active,$sequence,$contact_type,$contact_person);
                Notification::success("Service Center has been edited successfully");
                return Redirect::to("dashboard/service-centers");
            } else {
                $error = $validator->getErrors();
                return Redirect::to("dashboard/service-centers/edit/$id")->withErrors($error);
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getUpdateApplianceType($id, $type, $status)
    {
        $this->servicesCentersService->updateApplianceType($id, $type, $status);
        return Redirect::to('dashboard/service-centers');
    }

    public function getActivateOrDeactivate($id, $status)
    {
        $this->servicesCentersService->activateOrDeactivate($id, $status);
        return Redirect::to('dashboard/service-centers');
    }

    public function getDelete($id)
    {
        $this->servicesCentersService->deleteServiceCenters(array($id));
        return Redirect::to("dashboard/service-centers");
    }


} 