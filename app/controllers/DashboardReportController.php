<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 09-Mar-15
 * Time: 1:31 PM
 */
class DashboardReportController extends BaseController
{
    function __construct(CartService $cartService, OrderService $orderService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->cartService = $cartService;
        $this->orderService = $orderService;
    }

    //cart abandonment report
    public function getCartReport()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $page = Input::get('page');

        $results = $this->cartService->getAllCartData($from_date, $to_date, Constants::DASHBOARD_REPORT_PAGE_COUNT, $page, 0);
        $data['results'] = $results['cart'];
        $data['links'] = $results['links'];

        //echo "<pre>";print_r($data['results']);echo "</pre>";exit;
        return View::make('dashboard.reports.cart_report', $data);
    }

    public function getCartTrashReport()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d 24:00:00") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $page = Input::get('page');
        $deleted_by_admin = Input::get('deleted_by_admin');

        $results = $this->cartService->getAllCartTrashData($from_date, $to_date, Constants::DASHBOARD_REPORT_PAGE_COUNT, $page, $deleted_by_admin);

        $data['results'] = $results['cart'];
        $data['links'] = $results['links'];

        //echo "<pre>";print_r($data['results']);echo "</pre>";exit;
        if ($deleted_by_admin == 0) {
            return View::make('dashboard.reports.cart_trash_report', $data);
        } else {
            return View::make('dashboard.reports.admin_cart_trash_report', $data);
        }

    }

    public function getDownloadCartCsv()
    {

        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $results = $this->cartService->getAllCartData($from_date, $to_date, null, null, 0);
        $data['results'] = $results['cart'];
//        $data['links'] = $results['links'];
        $this->cartService->getCartCsv($data['results'], 0);
    }

    public function getDownloadCartTrashCsv()
    {

        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $deleted_by_admin = Input::get('deleted_by_admin');

        $results = $this->cartService->getAllCartTrashData($from_date, $to_date, null, null, $deleted_by_admin);
        $data['results'] = $results['cart'];
        $this->cartService->getCartCsv($data['results'], 1);
    }

    public function getDeleteCartItem($cart_item_id, $cart_id)
    {
        try {

            $result = $this->cartService->getCart($cart_id, null);

            $this->cartService->deleteCartItem($cart_item_id, $cart_id, 1);

            return Redirect::to("dashboard/reports/cart-trash-report?deleted_by_admin=1");

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getUndoDeleteCartItem($cart_item_id, $cart_id)
    {
        try {

            $result = $this->cartService->getCart($cart_id, 1);

            $this->cartService->DeleteCartItem($cart_item_id, $cart_id, 0);
            $is_deleted = ($result->deleted_at) ? $result->deleted_at : '';
            if (empty($is_deleted)) {
                return Redirect::to("dashboard/reports/cart-report");
            } else {
                return Redirect::to("dashboard/reports/cart-trash-report");
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    //sales report
    public function getSalesReport()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-01') : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d 24:00:00") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $category_id = array(27, 28, 29, 30);
        $data['chimneys'] = $this->orderService->getSalesByCategory($from_date, $to_date, $category_id);

        $category_id = array(32, 34, 35);
        $data['cooktops'] = $this->orderService->getSalesByCategory($from_date, $to_date, $category_id);

        $category_id = array(37, 38, 39, 40, 41);
        $data['built_in_hobs'] = $this->orderService->getSalesByCategory($from_date, $to_date, $category_id);

        $category_id = array(8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 42, 43, 44);
        $data['small_appliances'] = $this->orderService->getSalesByCategory($from_date, $to_date, $category_id);

        //merge all categories combos
        $combos = array();
        if (isset($data)) {
            $combos = $data['chimneys']['combos'] + $data['cooktops']['combos'] + $data['built_in_hobs']['combos'] + $data['small_appliances']['combos'];
//            echo "<pre>";            print_r($combos);            echo "</pre>";            exit;
            $combo_ids = array();
            $qty = 0;
            $combo_data = array();

            if (!empty($combos)) {
                foreach ($combos as $combo) {

                    if (in_array($combo['combo_id'], $combo_ids)) {
                        $combo_data[$combo['combo_id']]['qty'] = $combo_data[$combo['combo_id']]['qty'] + 1;
                    } else {
                        $qty = 1;
                        array_push($combo_ids, $combo['combo_id']);
                    }

                    if (!isset($combo_data[$combo['combo_id']])) {
                        $combo_data[$combo['combo_id']] = array();
                        $combo_data[$combo['combo_id']]['cod_sale'] = 0;
                        $combo_data[$combo['combo_id']]['online_sale'] = 0;
                    }
                    if ($combo['payment_method'] == 'cod') {
                        $combo_data[$combo['combo_id']]['cod_sale'] = $combo_data[$combo['combo_id']]['cod_sale'] + $combo['price'];
                    } else {
                        $combo_data[$combo['combo_id']]['online_sale'] = $combo_data[$combo['combo_id']]['online_sale'] + $combo['price'];
                    }

                    $combo_data[$combo['combo_id']] = $combo_data[$combo['combo_id']] + array('combo_id' => $combo['combo_id'], 'qty' => $qty,
                            'combo_name' => $combo['combo_name']);

                }
                $data['combos'] = $combo_data;
            }

        }

        //echo "<pre>";            print_r($combo_data);            echo "</pre>";            exit;
        return View::make('dashboard.reports.sales_report', $data);
    }

    public function getCategoryWiseOrders($category_id)
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-01') : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d 24:00:00") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $item_type = Input::get('item_type');

        $data['orders'] = $this->orderService->getOrdersByCategory($from_date, $to_date, $category_id, $item_type);

        return View::make('dashboard.reports.orders', $data);
    }

    public function getDownloadOrderSalesCsv()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-01') : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d 24:00:00") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $orders = $this->orderService->getOrdersByCategory($from_date, $to_date, null, null);
        $this->orderService->getOrderSalesCsv($orders);

    }

    public function getCartStatistics()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-01') : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d 24:00:00") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $category_id = array(27, 28, 29, 30);
        $data['chimneys'] = $this->cartService->getAllCartsStatistics($from_date, $to_date, $category_id);

        $category_id = array(32, 34, 35);
        $data['cooktops'] = $this->cartService->getAllCartsStatistics($from_date, $to_date, $category_id);

        $category_id = array(37, 38, 39, 40, 41);
        $data['built_in_hobs'] = $this->cartService->getAllCartsStatistics($from_date, $to_date, $category_id);

        $category_id = array(8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 42, 43, 44);
        $data['small_appliances'] = $this->cartService->getAllCartsStatistics($from_date, $to_date, $category_id);

        //merge all categories combos
        $combos = array();
        if (isset($data)) {
            $combos = $data['chimneys']['combos'] + $data['cooktops']['combos'] + $data['built_in_hobs']['combos'] + $data['small_appliances']['combos'];

            $combo_ids = array();
            $qty = 0;
            $combo_data = array();

            if (!empty($combos)) {
                foreach ($combos as $combo) {

                    if (in_array($combo['combo_id'], $combo_ids)) {
                        $combo_data[$combo['combo_id']]['qty'] = $combo_data[$combo['combo_id']]['qty'] + 1;
                    } else {
                        $qty = 1;
                        array_push($combo_ids, $combo['combo_id']);
                    }

                    if (!isset($combo_data[$combo['combo_id']])) {
                        $combo_data[$combo['combo_id']] = array();
                        $combo_data[$combo['combo_id']]['total_sale'] = 0;
                    }

                    $combo_data[$combo['combo_id']]['total_sale'] = $combo_data[$combo['combo_id']]['total_sale'] + $combo['price'];

                    $combo_data[$combo['combo_id']] = $combo_data[$combo['combo_id']] + array('combo_id' => $combo['combo_id'], 'qty' => $qty,
                            'combo_name' => $combo['combo_name']);
                }
                $data['combos'] = $combo_data;
            }
        }

        //echo "<pre>";            print_r($combo_data);            echo "</pre>";            exit;
        return View::make('dashboard.reports.cart_sales_report', $data);
    }

    public function getCategoryWiseCartData($category_id)
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-01') : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d 24:00:00") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $item_type = Input::get('item_type');

        $data['cart'] = $this->cartService->getCategoryWiseCartData($from_date, $to_date, $category_id, $item_type);

        return View::make('dashboard.reports.cart_item_list', $data);
    }

    public function getDownloadCartStatisticsCsv()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-01') : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d 24:00:00") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $cartData = $this->cartService->getCategoryWiseCartData($from_date, $to_date, null, null);
        $this->cartService->getOrderSalesCsv($cartData);

    }

}