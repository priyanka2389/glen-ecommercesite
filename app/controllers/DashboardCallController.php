<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 9/15/14
 * Time: 4:54 PM
 */
class DashboardCallController extends BaseController
{
    function __construct(CallService $callService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->callService = $callService;
    }

    public function getIndex()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? "0000-00-00" : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $data['calls'] = $this->callService->getCalls($from_date, $to_date, Constants::DASHBOARD_CALL_PAGE_COUNT);
        return View::make('dashboard.call', $data);
    }

    public function getDownloadCsv()
    {
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? "0000-00-00" : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $calls = $this->callService->getCalls($from_date, $to_date, Constants::DASHBOARD_CALL_PAGE_COUNT);
        $this->callService->geCallCsv($calls);
    }

    public function getDelete($id)
    {
        $this->callService->deleteCall($id);
        return Redirect::to("dashboard/call");
    }

} 