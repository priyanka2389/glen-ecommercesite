<?php

class HomeController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    function __construct(CategoryService $categoryService, ProductService $productService)
    {
        $this->categoryService = $categoryService;
        $this->productService = $productService;
    }


    public function getIndex()
    {
        //get latest offers
        $data['latest_offers'] = $this->productService->getProductsByTag("latest_offers", 6);
        //get featured products
        $data['featured_products'] = $this->productService->getProductsByTag("featured_products", 10);
        //get upcoming products
        $data['upcoming_products'] = $this->productService->getProductsByTag("upcoming_products", 8);
//echo "<pre>";print_r($data['upcoming_products']);echo "</pre>";exit;
        //get search products
        $data['products'] = $this->getProducts();

        return View::make('frontoffice.home', $data);
    }

    function getProducts()
    {
        $products = $this->productService->getProducts(null, null, null, null, null, 1, null, null);
//        echo "<pre>";
//        print_r($products);
//        echo "</pre>";
//        exit;

        $categorywiseProduct = array();
        $productsarr = array();
        foreach ($products as $product) {

            if (isset($product['category']['parent_category_id'])) {
                $data['category'] = $this->categoryService->getCategory($product['category']['parent_category_id'], null, null);
                $category_name = $data['category']['name'] . " > ";

                $category = '';
                $category = $category_name . $product['category']['name'];
                if ($category == $category_name . $product['category']['name']) {
                    if (isset($categorywiseProduct[$category_name . $product['category']['name']])) {
                        array_push($categorywiseProduct[$category_name . $product['category']['name']], $product['shortcode']);
                    } else {
                        $categorywiseProduct = $categorywiseProduct + array($category_name . $product['category']['name'] => array($product['shortcode']));
                    }
                } else {
                    $categorywiseProduct = $categorywiseProduct + array($category_name . $product['category']['name'] => array($product['shortcode']));
                }
            }
//            else {
//                $category_name = '';
//            }
        }

        $productshtml = '';
        $productshtml = '<select name="search_query" id="search_query_top" class="form-control input-sm m-bot15" data-placeholder="Select Product">';
        $productshtml .= '<option value="select">Select Product</option>';
        foreach ($categorywiseProduct as $key1 => $cproduct) {
            $productshtml .= '<OPTGROUP LABEL="' . $key1 . '">';

            for ($i = 0; $i < count($cproduct); $i++) {

                $name = str_replace("-", " ", $cproduct[$i]);
                $productshtml .= '<option value="' . $cproduct[$i] . '">' . $name . '</option>';
            }
            $productshtml .= '</OPTGROUP>';
        }
        $productshtml .= '</select>';
        return $productshtml;

    }
}