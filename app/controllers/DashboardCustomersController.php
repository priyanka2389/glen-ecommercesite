<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/7/14
 * Time: 3:48 PM
 */
class DashboardCustomersController extends BaseController
{

    public static $newsletters = array('yes' => 1, 'no' => 0);
    public static $special_offers = array('yes' => 1, 'no' => 0);


    function __construct(UserService $userService, OrderService $orderService,
                         ProductService $productService, ComboService $comboService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->userService = $userService;
        $this->orderSerive = $orderService;
        $this->productService = $productService;
        $this->comboService = $comboService;
    }


    public function getIndex()
    {
        $newletters = Input::get('newsletters', null);
        $special_offers = Input::get('special_offers', null);

        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $data['newsletters'] = self::$newsletters;
        $data['special_offers'] = self::$special_offers;

        $data['users'] = $this->userService->getUsers($newletters, $special_offers, $from_date, $to_date, Constants::DASHBOARD_CUSTOMERS_PAGE_COUNT);

        $data['selected_newsletter'] = $newletters;
        $data['selected_special_offers'] = $special_offers;
        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;
        return View::make('dashboard.customers.index', $data);
    }

    public function postIndex()
    {
        $newletters = Input::get('newsletters', null);
        $special_offers = Input::get('special_offers', null);

        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $data['newsletters'] = self::$newsletters;
        $data['special_offers'] = self::$special_offers;

        $data['users'] = $this->userService->getUsers($newletters, $special_offers, $from_date, $to_date, Constants::DASHBOARD_CUSTOMERS_PAGE_COUNT);

        $data['selected_newsletter'] = $newletters;
        $data['selected_special_offers'] = $special_offers;
        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;
        return View::make('dashboard.customers.index', $data);
    }

    public function getDownloadCsv()
    {
        $newletters = Input::get('newsletters') != 'undefined' ? Input::get('newsletters') : null;
        $special_offers = Input::get('special_offers') != 'undefined' ? Input::get('special_offers') : null;

        $show_trash = Input::get('show_trash');
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d 24:00:00") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $users = $this->userService->getUsers($newletters, $special_offers, $from_date, $to_date, null);

        if (count($users) > 0) {
            $this->userService->getCustomerCsv($users);
        } else {
            Notification::error('No data found for the date range you have selected');
            return Redirect::back();
        }

    }


    public function getInfo($user_id)
    {
        $user = $this->userService->getUser($user_id, null, null);
        $data['user'] = $user;
        $data['shipping_addresses'] = $this->userService->getUserAddresses($user_id, 'shipping');
        $data['billing_addresses'] = $this->userService->getUserAddresses($user_id, 'billing');
        $orders = $user->orders;
        $orderData = array_values(array_sort($orders, function ($value) {
            return $value['created_at'];
        }));
        $data['orders'] = array_reverse($orderData);
        return View::make('dashboard.customers.info', $data);
    }

    public function getOrderItems($order_id)
    {
        $order = $this->orderSerive->getOrder($order_id);

        $items = array();
        foreach ($order->items as $i => $row) {

            $item_id = $row->item_id;
            $item_type = $row->item_type;

            $items[$i] = array(
                'item_id' => $item_id,
                'item_type' => $item_type,
                'offer_price' => $row->offer_price,
                'list_price' => $row->list_price,
                'qty' => $row->qty,
                'notes' => $row->notes,
            );

            if ($row->item_type == 'product') {
                $product = $this->productService->getProductBasicInfo($item_id, null, null, null);
                $product_name = isset($product) ? $product->name : "";
                $items[$i]['item_name'] = $product_name;
            } else {
                $combo = $this->comboService->getComboProducts($item_id);
                $combo_products = $combo->products;
                foreach ($combo_products as $single_product) {
                    $items[$i]['item_name'][] = $single_product->name;
                }

            }
        }
        $data['items'] = $items;
        echo View::make('dashboard.customers.order_items', $data);
    }

    public function postAjaxUpdateOrderStatus($order_id, $user_id)
    {
        $order_status = Input::get('order_status');
        $payment_status = Input::get('paymentStatus');
        $notes = Input::get('notes');
        $this->orderSerive->updateStatus($order_id, $order_status);
        $this->orderSerive->updatePaymentStatus($order_id, $payment_status, $notes);
        $data = array("success" => 'true');
        return Response::json($data);
    }
}