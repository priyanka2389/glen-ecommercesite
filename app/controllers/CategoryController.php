<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 2/27/14
 * Time: 12:00 PM
 */
class CategoryController extends BaseController
{
    public static $sort = null;
    public static $price_filter = null;

    function __construct(CategoryService $categoryService, ProductService $productService)
    {
        $this->categoryService = $categoryService;
        $this->productService = $productService;
    }

    public function getIndex()
    {

    }

    public function getData($id, $view_name)
    {
        try {
            $shortcode = Request::segment(1);
            $category = $this->categoryService->getCategory(null, null, $shortcode);
            $data['meta_title'] = ($category->meta_title) ? $category->meta_title : '';
            $data['meta_description'] = ($category->meta_description) ? $category->meta_description : '';
            $data['meta_keywords'] = ($category->meta_keywords) ? $category->meta_keywords : '';
//            $category_id = $category->id;

            $category_id = $id;
            $input = Input::all();
            $filter = array();
            $filter_param = array(); //filter param will contain the price range and sorting type
            $products_ids = array();
            $active_tab = Input::get('active_tab', null);

            if (sizeof($input) != 0 && isset($input['filter'])) { //if the url has filters

                if (isset($input['filter']) && $input['filter'] == 'true') {

                    //removing unnecessary variables from url
                    unset($input['filter']);
                    unset($input['page']);
                    unset($input['active_tab']);


                    foreach ($input as $key => $value) {
                        if ($key == 'sort' || $key == "price") {
                            continue;
                        }

                        $key = str_replace('_', ' ', $key);
                        $filter[$key] = $value; //retrieving all the necessary filters
                    }
                }

                $sort_by = isset($input['sort']) ? $input['sort'] : null;
                $price = isset($input['price']) ? $input['price'] : 0;

                $products = $this->productService->getFilteredProducts($filter, $price, $category_id, $sort_by, null);

                $filter_param['sort'] = $sort_by;
                $filter_param['price'] = $price;
            } else {
                $products = $this->productService->getProducts($category_id, Constants::NULL_VALUE, null, null, null, 1, null, null);
            }

            if (!is_null($products)) {
                foreach ($products as $product) {
                    $products_ids[] = (int)$product->id;
                }
            } else {
                $products = null;
            }


            //get the filters according to the products
//            if (!is_null($products) && sizeof($products_ids) != 0) {
//                $data['filters_html'] = $this->productService->getProductsFilterHtml($products_ids, $filter, $filter_param, $active_tab);
//                if(isset($filter_products) && sizeof($input) != 0){
//                    $data['products'] = $filter_products;
//                }
////                else if(empty($filter_products) && sizeof($input) == 0){
////                    $products = null;
////                }
//                else{
//                    $data['products'] = $products;
////                    echo "No products found";exit;
//                }
//                $data['products'] = (isset($filter_products) && sizeof($input) != 0) ? $filter_products : $products;
//                $data['product_ids'] = $products_ids;
//            } else {
//                if (isset($filter_param['price'])) {
//                    $data['filters_html'] = $this->productService->getPriceFilterHtml($filter_param['price']);
//                } else {
//                    $data['filters_html'] = '';
//                }
//
//                $data['products'] = null;
//                $data['product_ids'] = null;
//            }

            if (!empty($products) && sizeof($products_ids) != 0) {
                $filter_products = $this->productService->getProducts($category_id, Constants::NULL_VALUE, null, null, null, 1, null, null);
                if (!is_null($filter_products)) {
                    foreach ($filter_products as $product) {
                        $products_ids[] = (int)$product->id;
                    }
                } else {
                    $filter_products = null;
                }
                $data['filters_html'] = $this->productService->getProductsFilterHtml($category_id, $products_ids, $filter, $filter_param, $active_tab);
                $data['products'] = $products;
                $data['product_ids'] = $products_ids;
            } else {
                if (isset($filter_param['price'])) {
                    $data['filters_html'] = $this->productService->getPriceFilterHtml($category_id, $filter_param['price']);
                } else {
                    $data['filters_html'] = '';
                }

                $data['products'] = null;
                $data['product_ids'] = null;
            }

            $data['sort'] = isset($filter_param['sort']) ? $filter_param['sort'] : null;

            //creation of filter for html
            $data['input'] = sizeof($input) != 0 ? $input : null;

            $data['facebook_meta_tags'] = $this->categoryService->getFacebookMetaTags($category_id);
            $data['body_class'] = "category_$category_id";

            $enable_sidebar = Input::get('enable_sidebar');
            if ($enable_sidebar == true) {
                $data['sidebar_html'] = '<div class="floating-menu">' .
                    '<h3>Small Appliances</h3>' .
                    '<div>
                                    <a href=' . URL::to("food-processors") . '><span class="layered_subtitle">Food Processor</span></a>
                                    <a href=' . URL::to("juicer-mixer-grinders") . '><span class="layered_subtitle">Juicer Mixer Grinder</span></a>
                                    <a href=' . URL::to("mixer-grinders") . '><span class="layered_subtitle">Mixer Grinder</span></a>
                                    <a href=' . URL::to('choppers') . '><span class="layered_subtitle">Chopper</span></a>
                                    <a href=' . URL::to('blender-grinders') . '><span class="layered_subtitle">Blender &amp; Grinder</span></a>
                                    <a href=' . URL::to('hand-mixers') . '><span class="layered_subtitle">Hand Mixer</span></a>
                                    <a href=' . URL::to('kettles-&-tea-makers') . '><span class="layered_subtitle">Kettle &amp; Tea Maker</span></a>
                                    <div class="clear"></div>
                                    </div>
                                    </div>';
            } else {
                $data['sidebar_html'] = '';
            }

            $data['searchProducts'] = $this->productService->getSearchProducts();

            return View::make("frontoffice.$view_name", $data);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    private function getFilteredProducts()
    {

    }
}