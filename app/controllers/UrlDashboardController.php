<?php
/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 10/15/14
 * Time: 11:00 AM
 */
class UrlDashboardController extends BaseController
{
    function __construct()
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->urlList = new UrlList();
    }

    public function getIndex(){

        $data['urls'] = $this->urlList->getData();
        return View::make('dashboard.urls', $data);
    }

    public function getMappedUrls(){
        $data['urls'] = $this->urlList->getMappedUrls();
        return View::make('dashboard.mapped_urllist', $data);
    }

    public function postUpdateUrl($searched_url,$mapped_url,$mapped_id){

        $res = $this->urlList->updateUrl($mapped_id,$searched_url,$mapped_url);
        $data = array("success" => 'true');
        return Response::json($data);
    }

    public function getDeleteUrl($id)
    {
        $this->urlList->deleteUrl($id);
        return Redirect::back();
    }
}

