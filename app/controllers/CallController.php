<?php

/**
 * Created by PhpStorm.
 * User: Priyanka
 * Date: 9/15/14
 * Time: 6:55 PM
 */
class CallController extends BaseController
{
    function __construct(CallService $callService)
    {
        $this->callService = $callService;
    }

    public function postData()
    {
        $name = Input::get('call_name', null);
        $email = Input::get('call_email',null);
        $mobile = Input::get('call_mobile', null);

        $this->callService->createCall($name,$mobile,$email);

//        $msgdata = array(
//            'to_emailId' => $friend_email,
//            'from_emailId' => 'sales@glenindia.com',
//            'from_name' => 'Glen India',
//            'email_subject' => 'Call from Glen',
//            'email_template' => 'emails.send_message_to_friend',
//            'product_link' => $product_link,
//            'product_message' => $message
//        );
//        Event::fire("sendMessageToFriend.success", array($msgdata));

        return Response::json("true");
    }

}