<?php
/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 7/28/14
 * Time: 5:57 PM
 */

class ComboController extends BaseController {
    function __construct(ProductService $productService, ComboService $comboService) {
        $this->productService = $productService;
        $this->comboService = $comboService;
    }

    public function getIndex()
    {

    }

    public function getCombo(){
        $short_code =  Request::segment(2);
//        $comboName = rawurldecode($combo_name);

        $combo = $this->comboService->getCombo(null,$short_code);
        if(!empty($combo)){
            $combo_id = $combo->id;
            $data['combo'] =  $combo;
            $data['related_combos'] = $this->comboService->getRelatedCombos($combo_id);

            return View::make('frontoffice.combo_info', $data);
        }else{
            App::abort(404);
        }

    }

} 