<?php
/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 7/8/14
 * Time: 3:18 PM
 */


class ServicesCentersController extends BaseController
{

    function __construct(CategoryService $categoryService, ServicesCentersService $servicecentersService,DealersService $dealerService,ProductService $productService)
    {
        $this->categoryService = $categoryService;
        $this->servicesCentersService = $servicecentersService;
        $this->dealersService = $dealerService;
        $this->productService = $productService;
    }

    public function getIndex()
    {
        $data['states'] = $this->servicesCentersService->getUniqueStates();
        $data['categories'] = $this->categoryService->getCategories(1,null,null,null,null);
        $data['searchProducts'] = $this->productService->getSearchProducts();
        return View::make('frontoffice.service_centers', $data);
    }

    public function postAjaxCities()
    {
        $state = Input::get('state');
//        $state = str::lower($state);
        $cities = $this->servicesCentersService->getCitiesForState($state);
        return Response::json($cities);
    }

    public function postAjaxSubcategories(){
        $category = Input::get('category');
        $subcategories = $this->servicesCentersService->getSubcategoriesForCategories($category);
        return Response::json($subcategories);
    }

    public function postAjaxServiceCenters()
    {
        $category = Input::get('category');

        $state = Input::get('state');
        $city = Input::get('city');

        if(isset($category)){
            $large_appliance = ($category ==  7)? false : true;
            $small_appliances = ($category ==  7) ? true : false;
        }
//        $large_appliance = Input::get('large_appliance');
//        $large_appliance = isset($large_appliance) ? true : false;
//        $small_appliances = Input::get('small_appliances');
//        $small_appliances = isset($small_appliances) ? true : false;

        $servicecenters_data = $this->servicesCentersService->getData($city, $state, $small_appliances, $large_appliance);
        return Response::json($servicecenters_data);
    }
}
