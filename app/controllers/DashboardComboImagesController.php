<?php

class DashboardComboImagesController extends BaseController
{

    public function __construct(ComboService $comboService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->ComboService = $comboService;
    }

    public function getIndex($combo_id)
    {
        $data['active'] = 'images';
        $data['combo'] = $this->ComboService->getCombo($combo_id, null);
        return View::make('dashboard.comboimages.index', $data);
    }

    public function getCreate($combo_id)
    {
        $data['active'] = 'images';
        $data['combo'] = $this->ComboService->getCombo($combo_id, null);
        return View::make('dashboard.comboimages.create', $data);
    }

    public function postCreate($combo_id)
    {
        try {

            $validation = new \services\Validators\ImageValidator();
            if ($validation->passes()) {

                $name = Input::get('name');
                $title = Input::get('title');
                $caption = Input::get('caption');
                $notes = Input::get('notes', '');
                $is_primary = Input::get('primary', 0);
                $img = Input::file('img');

                //upload,resize and move image
                $img_info = AppUtil::resizeAndMoveImage($img);
                $path = $img_info['path'];

                $product_image = $this->ComboService->createComboImage($path, $name, $title, $caption, $notes, $is_primary, $combo_id);
                $img_id = $product_image->id;

                if ($is_primary == 1) {
                    $this->ComboService->setComboPrimaryImage($combo_id, $img_id);
                }

                Notification::success("Image has been added successfully");
                return Redirect::to("dashboard/combo-images/$combo_id");

            } else {
                $errors = $validation->getErrors();
                return Redirect::to("dashboard/combo-images/create/$combo_id")->withInput()->withErrors($errors);
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getEdit($combo_id, $image_id)
    {
        $data['active'] = 'images';
        $data['combo'] = $this->ComboService->getCombo($combo_id, null);
        $data['image'] = $this->ComboService->getComboImage($combo_id, $image_id);

        return View::make('dashboard.comboimages.edit', $data);
    }

    public function postEdit($combo_id, $image_id)
    {
        try {

            $validation = new \services\Validators\ImageValidator();
            $validation::$rules['img'] = 'mimes:jpeg,jpg,png';
            if ($validation->passes()) {

                $name = Input::get('name');
                $title = Input::get('title');
                $caption = Input::get('caption');
                $notes = Input::get('notes', '');
                $is_primary = Input::get('primary', 0);
                $img = Input::file('img');

                if (Input::hasFile('img')) {

                    //upload and move file
                    $img_info = AppUtil::resizeAndMoveImage($img);
                    $path = $img_info['path'];

                    $this->ComboService->updateComboImage($combo_id, $image_id, $name, $title, $caption, $notes, $is_primary, $path);

                    if ($is_primary == 1) {
                        $this->ComboService->setComboPrimaryImage($combo_id, $image_id);
                    }

                } else {
                    $this->ComboService->updateComboImage($combo_id, $image_id, $name, $title, $caption, $notes, null, null);
                }

                return Redirect::to("dashboard/combo-images/$combo_id");

            } else {

                $errors = $validation->getErrors();
                return Redirect::to("dashboard/combo-images/edit/$combo_id/$image_id")->withInput(Input::all())->withErrors($errors);

            }


        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }


    public function getDestroy($combo_id, $image_id)
    {
        $this->ComboService->deleteComboImage($combo_id, array($image_id));
        return Redirect::to("dashboard/combo-images/$combo_id");
    }


    public function getSetPrimaryImage($combo_id, $image_id)
    {
        $this->ComboService->setComboPrimaryImage($combo_id, $image_id);
        return Redirect::to("dashboard/combo-images/$combo_id");
    }

}
