<?php

class ProductsController extends BaseController
{

    function __construct(ProductService $productService, CategoryService $categoryService, CartService $cartService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
        $this->cartService = $cartService;
    }

    public function getIndex($category, $shortcode)
    {
        try {
//            $product_id = explode('-', $product);
//            $product_id = $product_id[1];
            $product = $this->productService->getProduct(null, null, null, $shortcode);

            if (!empty($product)) {
                $product_id = $product->id;

                $base_product_id = $product->base_product_id;
                if (isset($base_product_id)) {
                    $base_product = $this->productService->getProduct($base_product_id, null, null, null);
                } else {
                    $base_product = null;
                }
                $data['meta_title'] = ($product->meta_title) ? $product->meta_title : '';
                $data['meta_description'] = ($product->meta_description) ? $product->meta_description : '';
                $data['meta_keywords'] = ($product->meta_keywords) ? $product->meta_keywords : '';

                $data['product'] = $product;
                $data['base_product'] = $base_product;
                $data['related_products'] = $this->productService->getRelatedProducts($product_id);
                $data['best_sellers'] = $this->productService->getProductsByTag("best_seller", 3);
                $data['combos'] = $this->productService->getProductCombos($product_id);
                $data['variants'] = $this->productService->getProductVariants($product_id);
                $data['specifications'] = $this->productService->getProductAttributesValue($product_id);
//                echo "<pre>";print_r($data['specifications'] );echo "</pre>";exit;

                $product_shortcode = $product->shortcode;
                $category_id = $product->category_id;

                $data['body_class'] = "product,product_$product_id,product_$product_shortcode,category_$category_id";
                $data['facebook_meta_tags'] = $this->productService->getFacebookMetaTags($product_id);

                $data['searchProducts'] = $this->productService->getSearchProducts();

                return View::make('frontoffice.product_info', $data);
            }else {
                App::abort(404);
            }


        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    // renders product comparison page
    public function getCompare()
    {
        try {
            $input = Input::all();
            if (isset($input['id'])) {
                $ids = explode(',', $input['id']);

                if (sizeof($ids) > 3) {
                    return Redirect::to('/');
                }
                //todo:check whether ancestors should be taken or just the above parent
                //$data['categories'] = $this->productService->getProductAncestors($ids[0]);
                $data['product_category'] = $this->productService->getProductImmediateCategory($ids[0]);
                $data['products'] = $this->productService->compare($ids);
                $data['product_tags'] = $this->productService->getComparableProductTags($ids);
                $data['searchProducts'] = AppUtil::getSearchProducts();
                return View::make('frontoffice.compare', $data);
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getAjaxProducts($category_id)
    {
        return $this->productService->getProducts($category_id, null, null, null, null, 1, null, null);
    }

    // remove product from product comparison page
    public function getRemoveCompareId()
    {
        try {

            $ids = explode(',', Input::get('id'));
            $remove_id = Input::get('remove');

            $ids = array_diff($ids, array($remove_id));
            $session_ids = Session::get('id');
            if (!empty($session_ids)) {
                Session::forget('id');
                foreach ($ids as $id) {
                    Session::push('id', $id);
                }

            }
            $ids = implode(',', $ids);

            return Redirect::to("product/compare?id=$ids");

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }


    //  returns the compare bar html
    public function getCompareBar($id)
    {
        try {

            if (!$this->isProductComparable($id)) {
                return "false";
            } else {
                Session::push('id', $id);
                $data = $this->productService->getCompareBarHtml();
                return $data;
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    //    removes item from comparison list
    public function getRemoveItem($id)
    {
        $product_ids = Session::get('id');
        $key = array_search($id, $product_ids);
        unset($product_ids[$key]);
        Session::forget('id');

        if (sizeof($product_ids) != 0) {
            foreach ($product_ids as $row) {
                Session::push('id', $row);
            }
        }
    }


    public function getRemoveAllItems()
    {
        Session::forget('id');
    }

    /** check the product with currently existing product whether they are comparable
     * @param int $id product_id
     * @return bool
     */
    private function isProductComparable($id)
    {
        $existing_id = Session::get('id', null);
        if (!is_null($existing_id)) {

            //$existing_product = $this->productService->getProductTopCategory($existing_id[0]);
            $existing_product = $this->productService->getProductBasicInfo($existing_id[0], null, null, null);
            $existing_product_parent = $existing_product->category_id;

            $new_product = $this->productService->getProductBasicInfo($id, null, null, null);
            $new_product_parent = $new_product->category_id;
            if ($existing_product_parent == $new_product_parent) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }

    }

    public function getCreateCompareBarHtml()
    {
        return $this->productService->getCompareBarHtml();
    }

    public function getFreeShippingContent()
    {
        return View::make('frontoffice.free_shipping');
    }


}
