<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 4/22/14
 * Time: 6:30 PM
 */
class DashboardOrderController extends BaseController
{
    public static $payment_status = array('paid', 'failed', 'pending', 'refunded', 'to be refunded');
    public static $order_status = array('new', 'cancelled', 'in-transit', 'delivered');
    public static $payment_method = array('cod', 'online');

    function __construct(OrderService $orderService, ProductService $productService, ComboService $comboService, UserService $userService, MailRecordsService $mailRecordsService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->orderService = $orderService;
        $this->productService = $productService;
        $this->comboService = $comboService;
        $this->userService = $userService;
        $this->mailService = $mailRecordsService;

    }

    public function getIndex()
    {

        $order_status = Input::get('status');
        $payment_status = Input::get('payment_status');
        $payment_method = Input::get('payment_method');
//        if (!empty($payment_status)) {
//            $payment_status = array_merge($payment_status, Input::get('payment_status'));
//        }

        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $data['orders'] = $this->orderService->getOrders(null, $order_status, $payment_status, $payment_method, $from_date, $to_date, Constants::DASHBOARD_ORDERS_PAGE_COUNT, false);
        $data['order_status'] = self::$order_status;
        $data['payment_status'] = self::$payment_status;
        $data['payment_method'] = self::$payment_method;

        $data['selected_order_status'] = $order_status;
        $data['selected_payment_status'] = $payment_status;
        $data['selected_payment_method'] = $payment_method;
        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;
        return View::make('dashboard.orders.index', $data);
    }

    public function postIndex()
    {
        $payment_status = Input::get('payment_status');
        $order_status = Input::get('status');
        $payment_method = Input::get('payment_method');

        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);

        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;

        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $data['orders'] = $this->orderService->getOrders(null, $order_status, $payment_status, $payment_method, $from_date, $to_date, Constants::DASHBOARD_ORDERS_PAGE_COUNT, false);
        $data['order_status'] = self::$order_status;
        $data['payment_status'] = self::$payment_status;
        $data['payment_method'] = self::$payment_method;

        $data['selected_order_status'] = $order_status;
        $data['selected_payment_status'] = $payment_status;
        $data['selected_payment_method'] = $payment_method;

        return View::make('dashboard.orders.index', $data);
    }

    public function getDownloadCsv()
    {
        $payment_status = Input::get('payment_status') ? explode(',', Input::get('payment_status')) : null;
        $order_status = Input::get('status') ? explode(',', Input::get('status')) : null;
        $payment_method = Input::get('payment_method') ? explode(',', Input::get('payment_method')) : null;

        $show_trash = Input::get('show_trash');
        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);
        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d 24:00:00") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $orders = $this->orderService->getOrders(null, $order_status, $payment_status, $payment_method, $from_date, $to_date, null, $show_trash);

        if (count($orders) > 0) {
            $this->orderService->getOrderCsv($orders, $show_trash);
        } else {
            Notification::error('No data found for the date range you have selected');
            return Redirect::back();
        }

    }

    public function getInfo($id)
    {
        $order = $this->orderService->getOrder($id);

        $items = array();
        foreach ($order->items as $i => $row) {

            $item_id = $row->item_id;
            $item_type = $row->item_type;

            $items[$i] = array(
                'item_id' => $item_id,
                'item_type' => $item_type,
                'offer_price' => $row->offer_price,
                'list_price' => $row->list_price,
                'qty' => $row->qty,
                'notes' => $row->notes,
            );

            if ($row->item_type == 'product') {
                $product = $this->productService->getProductBasicInfo($item_id, null, null, null);
                $product_name = isset($product) ? $product->name : "";
                $items[$i]['item_name'] = $product_name;
            } else {
                $combo = $this->comboService->getCombo($row->item_id, null);
                $combo_products = $this->comboService->getComboProducts($row->item_id, null);
                foreach ($combo_products['products'] as $product) {
                    $product_name[] = $product['name'];
                }

                $items[$i]['item_name'] = $combo->name . "  (" . join('+', $product_name) . ")";

            }
        }


        $shipping_address_id = $order->shipping_address_id;
        $billing_address_id = $order->billing_address_id;
        $data['order'] = $order;
        $data['items'] = $items;
        $data['shipping_address'] = $this->orderService->getOrderAddress($shipping_address_id, 'shipping');
        $data['billing_address'] = $this->orderService->getOrderAddress($billing_address_id, 'billing');
        return View::make('dashboard.orders.order_info', $data);
    }

    public function getTracking($id)
    {
        $order = $this->orderService->getOrder($id);
        $tracking = $order->trackingInfo;

        if ($tracking == null) {
            $tracking = new TrackingInfo;
        }
        $data['trackingInfo'] = $tracking;
        $data['order'] = $order;
        return View::make('dashboard.orders.order_tracking', $data);
    }

    public function postTracking($id)
    {
        try {

            $validation = new \services\Validators\TrackingInfoValidator();
            if ($validation->passes()) {
                $provider = Input::get("provider");
                $code = Input::get("code");
                $tracking_url = Input::get("tracking_url");
                $notes = Input::get("notes");

                $this->orderService->updateTrackingInfo($id, $provider, $code, $tracking_url, $notes);

                $order = $this->orderService->getOrder($id);
                if (!empty($order)) {
                    $payment_mode = $order->payment_mode;
                    if($payment_mode == 'cod'){
                        $payment_status = 'pending';
                    }else{
                        $payment_status = 'paid';
                    }
                    $this->orderService->updateStatus($id, 'in-transit');
                    $this->orderService->updatePaymentStatus($id, $payment_status, null);
                }

                Notification::success("Tracking Information updated successfully.");
                return Redirect::to('dashboard/order/info/' . $id);
            } else {

                $error = $validation->getErrors();
                return Redirect::to('dashboard/order/tracking/' . $id)->withInput(Input::all())->withErrors($error);

            }


        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getMailtracking($id)
    {
        $order = $this->orderService->getOrder($id);
        $tracking = $order->trackingInfo;

        if ($tracking == null) {
            $tracking = new TrackingInfo;
        }
        $data['trackingInfo'] = $tracking;
        $data['order'] = $order;

//        echo"<pre>";print_r($data);echo"</pre>";exit;

        $user = $this->userService->getUser($order->user_id, null, null);

        $msgdata = array(
            'to_emailId' => $user->email,
            'from_emailId' => 'enquiry@glenindia.com',
            'email_subject' => 'Order Tracking Detail',
            'email_template' => 'emails.order.order_tracking_template',
            'tracking_url' => $tracking->tracking_url,
            'customer_name' => $user->first_name
        );

        Event::fire("order.track", array($msgdata));

        $view = View::make($msgdata['email_template'], $msgdata);
        $data['email_content'] = $view->render();
        $this->mailService->createMailRecord('admin to ' . $msgdata['customer_name'], $msgdata['to_emailId'], $msgdata['from_emailId'], $msgdata['email_subject'], $data['email_content']);

        $smsdata = array(
            'tracking_id' => $data['trackingInfo']->id,
            'order_id' => $order->reference,
            'time' => $data['trackingInfo']->notes,
            'mobile' => $user->mobile
        );
        Event::fire("order.track_sendSMS", array($smsdata));

        //Login to email tracking information
        Notification::success("Tracking Information emailed successfully.");
        return Redirect::to('dashboard/order/info/' . $id);
    }

    public function getDownloadOrder($id)
    {
        $order = $this->orderService->getOrder($id);

        $items = array();
        foreach ($order->items as $i => $row) {

            $item_id = $row->item_id;
            $item_type = $row->item_type;

            $items[$i] = array(
                'item_id' => $item_id,
                'item_type' => $item_type,
                'offer_price' => $row->offer_price,
                'list_price' => $row->list_price,
                'qty' => $row->qty,
                'notes' => $row->notes,
            );

            if ($row->item_type == 'product') {
                $product = $this->productService->getProductBasicInfo($item_id, null, null, null);
                $product_name = isset($product) ? $product->name : "";
                $items[$i]['item_name'] = $product_name;
            } else {
                $combo = $this->comboService->getCombo($row->item_id, null);
                $combo_products = $this->comboService->getComboProducts($row->item_id, null);
                foreach ($combo_products['products'] as $product) {
                    $product_name[] = $product['name'];
                }

                $items[$i]['item_name'] = $combo->name . "  (" . join('+', $product_name) . ")";

            }
        }


        $shipping_address_id = $order->shipping_address_id;
        $billing_address_id = $order->billing_address_id;
        $order_no = $order->reference;
        $data['order'] = $order;
        $data['items'] = $items;
        $data['shipping_address'] = $this->orderService->getOrderAddress($shipping_address_id, null);
        $data['billing_address'] = $this->orderService->getOrderAddress($billing_address_id, null);

        $view = View::make('dashboard.orders.order_info_pdf', $data);
        $order_info_html = $view->render();

        $headers = array('Content-Type' => 'application/pdf');
        Config::set('PDF::config.DOMPDF_ENABLE_CSS_FLOAT', true);
        Config::set('PDF::config.DOMPDF_ENABLE_REMOTE', true);
        Config::set('PDF::config.DOMPDF_ENABLE_PHP', true);

        $pdfPath = public_path() . '/uploads/order/' . 'order#' . $order_no . '.pdf';
        $filename = 'order#' . $order_no . '.pdf';
        File::put($pdfPath, PDF::load($view, 'A4', 'portrait')->output());


        return Response::make(file_get_contents($pdfPath), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; ' . $filename,
        ]);

//return Response::download($pdfPath,'order-' . $order_no.'.pdf',$headers);
        // return Response::make (PDF::load($order_info_html, 'A4', 'portrait')->download('order-'.$order_no), 200, $headers);
//
//        $pdf = PDF::load($order_info_html, 'A4', 'portrait')->show('order','compress');
//        echo $pdf;
        //, $name, $headers);
        //return PDF::load($order_info_html, 'A4', 'portrait')->download('order#' . $order_no);
//        return PDF::load($order_info_html, 'A4', 'portrait')->show();
//
    }

    public function getOrderTrashReport()
    {
        $payment_status = Input::get('payment_status');
        $order_status = Input::get('status');
        $payment_method = Input::get('payment_method');

        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);

        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;

        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d 24:00:00") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $data['orders'] = $this->orderService->getOrders(null, $order_status, $payment_status, $payment_method, $from_date, $to_date, Constants::DASHBOARD_ORDERS_PAGE_COUNT, true);
        $data['order_status'] = self::$order_status;
        $data['payment_status'] = self::$payment_status;
        $data['payment_method'] = self::$payment_method;

        $data['selected_order_status'] = $order_status;
        $data['selected_payment_status'] = $payment_status;
        $data['selected_payment_method'] = $payment_method;

        return View::make('dashboard.orders.admin_order_trash_report', $data);
    }

    public function postOrderTrashReport()
    {
        $payment_status = Input::get('payment_status');
        $order_status = Input::get('status');
        $payment_method = Input::get('payment_method');

        $from_date = Input::get('from_date', null);
        $to_date = Input::get('to_date', null);

        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;

        $from_date = is_null($from_date) ? date('Y-m-d', strtotime($to_date . ' - 7 days')) : $from_date;
        $to_date = is_null($to_date) ? date("Y-m-d 24:00:00") : date('Y-m-d H:i:s', strtotime($to_date . ' + 1 day'));

        $data['orders'] = $this->orderService->getOrders(null, $order_status, $payment_status, $payment_method, $from_date, $to_date, Constants::DASHBOARD_ORDERS_PAGE_COUNT, true);
        $data['order_status'] = self::$order_status;
        $data['payment_status'] = self::$payment_status;
        $data['payment_method'] = self::$payment_method;

        $data['selected_order_status'] = $order_status;
        $data['selected_payment_status'] = $payment_status;
        $data['selected_payment_method'] = $payment_method;

        return View::make('dashboard.orders.admin_order_trash_report', $data);
    }

    public function getDeleteOrder($order_id)
    {
        try {

            $this->orderService->getDeleteOrderByAdmin($order_id, true);

            return Redirect::to("dashboard/order/order-trash-report");

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getUndoDeleteOrder($order_id)
    {
        try {

            $this->orderService->getRestoreOrderByAdmin($order_id, false);

            return Redirect::to("dashboard/order");

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }
}
//564,562,561,416,406,403,398,397,396,363,362,294,264,263,259,55,11,10,9,8,7,6,5,4,3,2,1