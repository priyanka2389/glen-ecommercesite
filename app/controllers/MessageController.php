<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 4:55 PM
 */
class MessageController extends BaseController
{
    function __construct(MessageService $messageService,MailRecordsService $mailRecordsService)
    {
        $this->messageService = $messageService;
        $this->mailService = $mailRecordsService;
    }

    public function postData()
    {
        $your_name = Input::get('your_name', null);
        $your_email = Input::get('your_email', null);
        $friend_name = Input::get('friend_name', null);
        $friend_email = Input::get('friend_email', null);
        $message = Input::get('message');
        if (empty($message) || $message == '' || $message == null || !isset($message)) {
            $message = "I thought you would be interested in following glen product.";
        }
        $product_link = Input::get('link');

        $this->messageService->createMessage($your_name, $your_email, $friend_name, $friend_email, $message, $product_link);

        $msgdata = array(
            'to_emailId' => $friend_email,
            'from_emailId' => $your_email,
            'to_name' => $friend_name,
            'from_name' => $your_name,
            'email_subject' => 'Invitation from Glen',
            'email_template' => 'emails.send_message_to_friend',
            'product_link' => $product_link,
            'product_message' => $message
        );

        Event::fire("sendMessageToFriend.success", array($msgdata));

        $view = View::make($msgdata['email_template'],$msgdata);
        $data['email_content'] = $view->render();
        $mail = $this->mailService->createMailRecord($your_name . ' to ' . $friend_name, $friend_email,$your_email, $msgdata['email_subject'], $data['email_content']);

        if($mail){
            return Response::json("true");
        }else{
            return Response::json("false");
        }

    }

    public function getCallUs()
    {
        $call_name = Input::get('call_name', null);
        $call_mobile = Input::get('call_mobile', null);

        $this->messageService->createCall($call_name, $call_mobile);

//        $msgdata = array(
//            'to_emailId' => $friend_email,
//            'from_emailId' => $your_email,
//            'from_name' => $your_name,
//            'email_subject' => 'Invitation from Glen',
//            'email_template' => 'emails.send_message_to_friend',
//            'product_link' => $product_link,
//            'product_message' => $message
//        );
//        Event::fire("sendMessageToFriend.success", array($msgdata));

        return Response::json("true");
    }

} 