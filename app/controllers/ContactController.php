<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/15/14
 * Time: 11:19 AM
 */
class ContactController extends BaseController
{
    function __construct(EnquiryService $enquiryService, CareerService $careerService,ProductService $productService,
                         ServicesEnquiryService $servicesEnquiryService,MailRecordsService $mailRecordsService)
    {
        $this->enquiryService = $enquiryService;
        $this->careerService = $careerService;
        $this->servicesEnquiryService = $servicesEnquiryService;
        $this->mailService = $mailRecordsService;
        $this->productService = $productService;
    }

    public function getIndex()
    {
        $data['enquiry_type'] = Input::get('enquiry_type', null);
        $data['page'] = Request::query('page');
        $data['searchProducts'] = $this->productService->getSearchProducts();
        return View::make('frontoffice.contact_us', $data);
    }

    public function postData()
    {
        $enquiry_type = Input::get('enquiry_type');
        $error = false;
        $input = Input::all();
        switch ($enquiry_type) {
            case "bulk_purchase":
                $validation = new \services\Validators\BulkPurchaseEnquiryValidator();

                if ($validation->passes()) {
                    $this->addBulkPurchase();
                } else {
                    $error = true;
                }
                break;

            case "product_info":        //product enquiry
                $validation = new \services\Validators\ProductInfoEnquiryValidator();

                if ($validation->passes()) {
                    $this->addProductInformation();
                } else {
                    $error = true;
                }
                break;
            case 'trade_partner':
                $validation = new \services\Validators\TradePartnerEnquiryValidator();

                if ($validation->passes()) {
                    $this->addTradePartner();
                } else {
                    $error = true;
                }
                break;

            case 'career':
                $validation = new \services\Validators\CareerValidator();

                $input = \Input::except('resume');
                if ($validation->passes()) {
                    $this->addCareer();
                } else {
                    $error = true;
                }
                break;

            case "service":         //service request
                $validation = new \services\Validators\ServicesEnquiryValidator();

                if ($validation->passes()) {
                    $this->addService();
                } else {
                    $error = true;
                }
                break;

            case "institutional_purchase":
                $validation = new \services\Validators\InstitutionalPurchaseEnquiryValidator();

                if ($validation->passes()) {
                    $this->addInstitutionalPurchase();
                } else {
                    $error = true;
                }
                break;
        }

        if ($error == true) {
            $error = $validation->getErrors();

            return Redirect::to("contact-us?page=$enquiry_type")->withInput($input)->withErrors($error);
        } else {
            return Redirect::to('contact-us');
        }
    }

    private function addInstitutionalPurchase()
    {
        $name = Input::get('name', null);
        $company_name = Input::get('company_name', null);
        $email = Input::get('email', null);
        $phone = Input::get('mobile', null);
        $country = Input::get('country', null);
        $state = Input::get('state', null);
        $city = Input::get('city', null);
        $requirement = Input::get('requirement', null);
        $message = Input::get('message', null);
        $enquiry_type = "institutional_purchase";

//        ($name, $company_name, $email, $phone, $country,$state, $city, $requirement, $interest, $purpose, $message, $enquiry_type)
        $this->enquiryService->createEnquiry($name, $company_name, $email, $phone, $country, $state, $city, $requirement, null, null, $message, $enquiry_type);

        $mail_data = array(
            'name' => $name,
            'companyName' => $company_name,
            'email' => $email,
            'mobile' => $phone,
            'country' => $country,
            'city' => $city,
            'state' => $state,
            'requirement' => $requirement,
            'comment' => $message
        );

//        $data['admin_from_emailId'] = 'sales@glenindia.com';
        $data['admin_from_emailId'] = 'institutionalsales1@glenindia.com';
        $data['admin_to_emailId'] = array('institutionalsales1@glenindia.com' => 'Administrator');
//        $data['admin_to_emailId'] = array('ptailor@greenapplesolutions.com' => 'Administrator');
        $data['admin_email_subject'] = 'Query Institutional Purchase';
        $data['admin_email_template'] = 'emails.institutional_purchase_admin_mail_template';

        $this->sendMailToAdmin($mail_data, $data);

        $data['customer_emailId'] = $email;
        $data['customer_email_subject'] = 'Query Institutional Purchase submitted';
        $data['customer_email_template'] = 'emails.institutional_purchase_mail_template';

        $this->sendMailToCustomer($mail_data, $data);

        Notification::success('Institutional Purchase Query  has been submitted successfully');
    }


    private function addBulkPurchase()
    {
        $name = Input::get('name', null);
        $company_name = Input::get('company_name', null);
        $email = Input::get('email', null);
        $phone = Input::get('mobile', null);
        $country = Input::get('country', null);
        $state = Input::get('state', null);
        $city = Input::get('city', null);
        $requirement = Input::get('requirement', null);
        $message = Input::get('message', null);
        $enquiry_type = "bulk_purchase";

//        ($name, $company_name, $email, $phone, $country,$state, $city, $requirement, $interest, $purpose, $message, $enquiry_type)
        $this->enquiryService->createEnquiry($name, $company_name, $email, $phone, $country, $state, $city, $requirement, null, null, $message, $enquiry_type);

        $mail_data = array(
            'name' => $name,
            'companyName' => $company_name,
            'email' => $email,
            'mobile' => $phone,
            'country' => $country,
            'city' => $city,
            'state' => $state,
            'requirement' => $requirement,
            'comment' => $message
        );

//        $data['admin_from_emailId'] = 'sales@glenindia.com';
        $data['admin_from_emailId'] = 'enquiry@glenindia.com';
        $data['admin_to_emailId'] = array('enquiry@glenindia.com' => 'Administrator');
//        $data['admin_to_emailId'] = array('ptailor@greenapplesolutions.com' => 'Administrator');
        $data['admin_email_subject'] = 'Query Distribution & Gallery Request submitted';
        $data['admin_email_template'] = 'emails.bulk_purchase_admin_mail_template';

        $this->sendMailToAdmin($mail_data, $data);

        $data['customer_emailId'] = $email;
        $data['customer_email_subject'] = 'Query Distribution & Gallery Request submitted';
        $data['customer_email_template'] = 'emails.bulk_purchase_mail_template';

        $this->sendMailToCustomer($mail_data, $data);

        Notification::success('Distribution & Gallery request has been submitted successfully');
    }

    private function addProductInformation()
    {
        $name = Input::get('name', null);
        $email = Input::get('email', null);
        $phone = Input::get('mobile', null);
        $country = Input::get('country', null);
        $state = Input::get('state', null);
        $city = Input::get('city', null);
        $interest = Input::get('interest', null);
        $message = Input::get('message', null);
        $enquiry_type = "product_info";

        $this->enquiryService->createEnquiry($name, null, $email, $phone, $country, $state,
            $city, null, $interest, null, $message, $enquiry_type);

        $mail_data = array(
            'name' => $name,
            'email' => $email,
            'mobile' => $phone,
            'country' => $country,
            'city' => $city,
            'state' => $state,
            'interest' => $interest,
            'comment' => $message
        );

//        $data['admin_from_emailId'] = 'sales@glenindia.com';
        $data['admin_from_emailId'] = 'enquiry@glenindia.com';
        $data['admin_to_emailId'] = array('enquiry@glenindia.com');
//        $data['admin_to_emailId'] = array('ptailor@greenapplesolutions.com' , 'amulchandani@greenapplesolutions.com' => 'Administrator');
        $data['admin_email_subject'] = 'Product Enquiry';
        $data['admin_email_template'] = 'emails.product_information_admin_mail_template';

         $this->sendMailToAdmin($mail_data, $data);

        $data['customer_emailId'] = $email;
        $data['customer_email_subject'] = 'Product query submitted';
        $data['customer_email_template'] = 'emails.product_information_mail_template';

        $this->sendMailToCustomer($mail_data, $data);

        Notification::success('Product query Information has been submitted successfully');

    }

    private function addTradePartner()
    {
//        $company = Input::get('company', null);
        $name = Input::get('name', null);
        $email = Input::get('email', null);
        $phone = Input::get('mobile', null);
        $country = Input::get('country', null);
        $state = Input::get('state', null);
        $city = Input::get('city', null);
        $interest = Input::get('interest', null);
        $purpose = Input::get('purpose', null);
        $message = Input::get('message', null);
        $enquiry_type = "trade_partner";

        $this->enquiryService->createEnquiry($name, null, $email, $phone, $country, $state,
            $city, null, $interest, $purpose, $message, $enquiry_type);

        $mail_data = array(
            'name' => $name,
            'email' => $email,
            'mobile' => $phone,
            'country' => $country,
            'city' => $city,
            'state' => $state,
            'interest' => $interest,
            'purpose' => $purpose,
            'comment' => $message
        );

//        $data['admin_from_emailId'] = 'sales@glenindia.com';
        $data['admin_from_emailId'] = 'enquiry@glenindia.com';
        $data['admin_to_emailId'] = array('enquiry@glenindia.com');
//        $data['admin_to_emailId'] = 'ptailor@greenapplesolutions.com';
        $data['admin_email_subject'] = 'Request for Trade Partner ';
        $data['admin_email_template'] = 'emails.trade_partner_admin_mail_template';

        $this->sendMailToAdmin($mail_data, $data);

        $data['customer_emailId'] = $email;
        $data['customer_email_subject'] = 'Request for Trade Partner submitted';
        $data['customer_email_template'] = 'emails.trade_partner_mail_template';
        $this->sendMailToCustomer($mail_data, $data);

        Notification::success('Request for Trade Partner Enquiry has been submitted successfully');

    }

    private function addCareer()
    {

        $name = Input::get('name', null);
        $email = Input::get('email', null);
        $phone = Input::get('mobile', null);
        $address = Input::get('address', null);
        $dob = Input::get('dob', null);
        $applying_department = Input::get('applying_department', null);
        $educational_qualification = Input::get('educational_qualification', null);
        $professional_qualification = Input::get('professional_qualification', null);
        $primary_skill = Input::get('primary_skill', null);
        $career_highlights = Input::get('career_highlights', null);
        $work_Exp = Input::get('work_exp', null);
        $resume = Input::file('resume', null);

        $extension = $resume->getClientOriginalExtension();
        $resume_name = $resume->getClientOriginalName();
        $random_string = str_random(6);
        $resume_name = str_replace("." . $extension, $random_string, $resume_name) . "." . $extension;
        $resume_path = public_path() . "/" . Constants::RESUME_UPLOAD_PATH;

        $resume->move($resume_path, $resume_name);

        $this->careerService->addCareer($name, $email, $phone, $address, $dob, $applying_department,
            $educational_qualification, $professional_qualification, $primary_skill, $career_highlights,
            $work_Exp, $resume_path);

        $mail_data = array(
            'name' => $name,
            'email' => $email,
            'mobile' => $phone,
            'address' => $address,
            'applying_department' => $applying_department,
            'educational_qualification' => $educational_qualification,
            'professional_qualification' => $professional_qualification,
            'primary_skill' => $primary_skill,
            'career_highlights' => $career_highlights,
            'work_Exp' => $work_Exp
        );

//        $data['admin_from_emailId'] = 'sales@glenindia.com';
        $data['admin_from_emailId'] = 'enquiry@glenindia.com';
//        'arun@glenindia.com', 'cr@glenindia.com' => 'Administrator'
        $data['admin_to_emailId'] = 'enquiry@glenindia.com';
//        $data['admin_to_emailId'] = 'ptailor@greenapplesolutions.com';
        $data['admin_email_subject'] = 'Resume';
        $data['admin_email_template'] = 'emails.career_admin_mail_template';
        $data['attachment'] = $resume_path . '/' . $resume_name;

        $this->sendMailToAdmin($mail_data, $data);

        $data['customer_emailId'] = $email;
        $data['customer_email_subject'] = 'Resume submitted';
        $data['customer_email_template'] = 'emails.career_mail_template';

        $this->sendMailToCustomer($mail_data, $data);

        Notification::success('Resume has been submitted successfully');

    }

    private function addService()
    {
        $name = Input::get('name', null);
        $email = Input::get('email', null);
        $phone = Input::get('mobile', null);
        $address = Input::get('address', null);
        $product = Input::get('product', null);
        $bill_no = Input::get('bill_no', null);
        $bill_date = Input::get('bill_date', null);
        $shop_name = Input::get('shop_name', null);
        $message = Input::get('message', null);

        $this->servicesEnquiryService->createServiceEnquiry($name, $email, $phone, $product, $address, $bill_no,
            $shop_name, $bill_date, $message);

        $mail_data = array(
            'name' => $name,
            'email' => $email,
            'mobile' => $phone,
            'address' => $address,
            'product' => $product,
            'bill_no' => $bill_no,
            'bill_date' => $bill_date,
            'shop_name' => $shop_name,
            'comment' => $message,
        );

//        $data['admin_from_emailId'] = 'sales@glenindia.com';
        $data['admin_from_emailId'] = 'cr@glenindia.com';
        $data['admin_to_emailId'] = array('arun@glenindia.com', 'cr@glenindia.com' => 'Administrator');
//        $data['admin_to_emailId'] = 'ptailor@greenapplesolutions.com';
        $data['admin_email_subject'] = 'Service Complaint';
        $data['admin_email_template'] = 'emails.service_request_admin_mail_template';

        $this->sendMailToAdmin($mail_data, $data);

        $data['customer_emailId'] = $email;
        $data['customer_email_subject'] = 'Service Complaint submitted';
        $data['customer_email_template'] = 'emails.service_request_mail_template';
        $this->sendMailToCustomer($mail_data, $data);

        Notification::success('Product Service Request has been submitted successfully');
    }

    private function sendMailToAdmin($mail_data, $data)
    {
        //make entry in mail records table
        $view = View::make($data['admin_email_template'],$mail_data);
        $data['admin_email_content'] = $view->render();
     //   $this->mailService->createMailRecord('Admin',join(',',$data['admin_to_emailId']),$data['admin_from_emailId'],$data['admin_email_subject'],$data['admin_email_content']);

        Mail::send($data['admin_email_template'], $mail_data, function ($message) use ($data) {
            $message->from($data['admin_from_emailId'], 'Glen India');
            $message->to($data['admin_to_emailId'])->subject($data['admin_email_subject']);
            if (isset($data['attachment'])) {
                $message->attach($data['attachment'], array('as' => 'resume'));
            }
        });
    }

    private function sendMailToCustomer($mail_data, $data)
    {
        //make entry in mail records table
        $view = View::make($data['customer_email_template'],$mail_data);
        $data['customer_email_content'] = $view->render();
     //   $this->mailService->createMailRecord($mail_data['name'],$data['customer_emailId'],$data['admin_from_emailId'],$data['customer_email_subject'],$data['customer_email_content']);

        Mail::send($data['customer_email_template'], $mail_data, function ($message) use ($data) {
            $message->from($data['admin_from_emailId'], 'Glen India');
            $message->to($data['customer_emailId'])->subject($data['customer_email_subject']);
        });
    }
}