<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/26/14
 * Time: 3:03 PM
 */
class FeedbackController extends BaseController
{

    function __construct(FeedbackService $feedbackService)
    {
        $this->feedbackService = $feedbackService;
    }

    public function postAjaxData()
    {
        $email = Input::get("feedback_email", null);
        $mobile = Input::get('feedback_mobile', null);
        $category = Input::get('feedback_category', null);
        $message = Input::get('feedback_message', null);

        $this->feedbackService->createFeedback($email, $mobile, $category, $message);
        $data = array('success' => 'true');
        return Response::json($data);
    }

} 