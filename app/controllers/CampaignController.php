<?php

class CampaignController extends BaseController
{

    function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }


    public function getBreakfastMadeEasy()
    {

        $product_id = array(280,45,13,12,253,51,256);
        $data['products_block1'] = $this->productService->getSelectedProducts($product_id,1);

        $product_id = array(255,22,183,89);
        $data['products_block2'] = $this->productService->getSelectedProducts($product_id,1);

        $product_id = array(8,24,87,92,94,96,257);
        $data['products_block3'] = $this->productService->getSelectedProducts($product_id,1);
        return View::make('frontoffice.campaigns.breakfast_made_easy', $data);
    }

    public function getComboOfferZone()
    {

        $product_id = array(280,45,13,12,253,51,256);
        $data['products_block1'] = $this->productService->getSelectedProducts($product_id,1);

        $product_id = array(255,22,183,89);
        $data['products_block2'] = $this->productService->getSelectedProducts($product_id,1);

        $product_id = array(8,24,87,92,94,96,257);
        $data['products_block3'] = $this->productService->getSelectedProducts($product_id,1);
        return View::make('frontoffice.campaigns.combo_offers', $data);
    }


}
