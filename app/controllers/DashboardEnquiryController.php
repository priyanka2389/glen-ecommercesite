<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/14/14
 * Time: 5:08 PM
 */
class DashboardEnquiryController extends BaseController
{
    function __construct(EnquiryService $enquiryService, CareerService $careerService,
                         ServicesEnquiryService $servicesEnquiryService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->enquiryService = $enquiryService;
        $this->careerService = $careerService;
        $this->servicesEnquiryService = $servicesEnquiryService;
    }

    public function getIndex()
    {
        $from_date = Input::get("from_date", null);
        $to_date = Input::get("to_date", null);
        $enquiry_type = Input::get("enquiry_type", null);

        if ($enquiry_type == 'services') {
            $data['enquires'] = $this->servicesEnquiryService->getServiceEnquiries($from_date, $to_date, Constants::DASHBOARD_ENQUIRES_PAGE_COUNT);
        } elseif ($enquiry_type == 'career') {
            $data['enquires'] = $this->careerService->getCareers($from_date, $to_date, Constants::DASHBOARD_ENQUIRES_PAGE_COUNT);
        } else {
            $data['enquires'] = $this->enquiryService->getEnquires($from_date, $to_date,
                $enquiry_type, Constants::DASHBOARD_ENQUIRES_PAGE_COUNT);
        }
        return View::make('', $data);
    }

    public function getDownload()
    {
        $from_date = Input::get("from_date", null);
        $to_date = Input::get("to_date", null);
        $enquiry_type = Input::get("enquiry_type", null);

        $enquires = $this->enquiryService->getEnquires($from_date, $to_date,
            $enquiry_type, null);

        if ($enquiry_type == "bulk_purchase") {
            $this->enquiryService->getBulkPurchaseCsv($enquires);
        } elseif ($enquiry_type == 'trade_partner') {
            $this->enquiryService->getTradePartnerCsv($enquires);
        } elseif ($enquiry_type == 'product_info') {
            $this->enquiryService->getProductInfoCsv($enquires);
        } elseif ($enquiry_type == 'career') {
            $this->careerService->getCareersCsv($enquires);
        } elseif ($enquiry_type == 'service') {
            $this->servicesEnquiryService->getServicesEnquiryCsv($enquires);
        }

    }

    public function getDelete($id, $enquiry_type)
    {
        if ($enquiry_type == 'career') {
            $this->careerService->deleteCareer($id);
        } elseif ($enquiry_type == 'services') {
            $this->servicesEnquiryService->deleteServiceEnquiry($id);
        } else {
            $this->enquiryService->deleteEnquiry($id);
        }

    }

} 