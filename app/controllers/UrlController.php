<?php

/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 10/15/14
 * Time: 11:00 AM
 */
class UrlController extends BaseController
{
    function __construct()
    {
        $this->urlList = new UrlList();
    }

    public function getRedirectUrl($entered_segment1, $entered_segment2 = null)
    {
        try {
            if (Cache::has('urls')) {
                $urls = Cache::get('urls');
                $query_string = Input::all();
                $site_url = URL::to('/');

                if (isset($urls)) {                                //if url exist redirect to that url
                    foreach ($urls as $url) {
                        $entered_segment = ($entered_segment2 != null) ? $entered_segment1 . '/' . $entered_segment2 : $entered_segment1;
                        if ($url->searched_url == $entered_segment) {
                            $redirect_url = $url->mapped_url;
                            break;
                        }
                    }

                    if (isset($redirect_url)) {
                        if ($query_string) {
                            //return Redirect::to(url::to($redirect_url));
                            return Redirect::to($site_url . '/' . $redirect_url . '?' . http_build_query($query_string));
                        } else {
                            return Redirect::to($site_url . '/' . $redirect_url);
                        }
                    } else {                                    //if keyword is not found in db it'll add unique keyword in db it'll not add duplicate keyword
                        $flag = false;
                        $urls = $this->urlList->getUnmappedUrls();
                        foreach ($urls as $url) {
                            $entered_segment = ($entered_segment2 != null) ? $entered_segment1 . '/' . $entered_segment2 : $entered_segment1;
                            //$url->searched_url == $entered_segment1 ||
                            if ($url->searched_url == $entered_segment) {
                                $flag = true;
//                                break;
                            }
                        }

                        if ($flag == false) {
                            $this->urlList->addUrl($entered_segment);
                        }

                        //return Redirect::to(url::to());
                        return Redirect::to($site_url);
                    }

                } else {                                          //if url does not exist redirect to root and store it in database
                    $entered_segment = ($entered_segment2 != null) ? $entered_segment1 . '/' . $entered_segment2 : $entered_segment1;
                    $this->urlList->addUrl($entered_segment);

                    //return Redirect::to(url::to());
                    return Redirect::to($site_url);
                }
            } else {
                Cache::remember('urls', 60, function () {
                    return DB::table('url_list')->get();
                });
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

}

//$url = Request::segment(1);
//$url = stristr($entered_url, 'gl');

//            $query = UrlList::query();
//
//            if (!is_null($entered_url)) {
//                $query->where('old_url', 'LIKE', "%$entered_url%");
//            }

//            $res = $query->first();
//
//            if (isset($res)) {
//                $redirect_url = $res->new_url;
////                return Redirect::to(url::to($redirect_url));
//                return Redirect::to('http://alpha.glenindia.com/public/' . $redirect_url);
//            } else {
//                //return Redirect::to(url::to());
//                return Redirect::to('http://alpha.glenindia.com/public/');
//            }