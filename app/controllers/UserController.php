<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 1/13/14
 * Time: 12:21 PM
 */
class UserController extends BaseController
{
    function __construct(UserService $userService, ProductService $productService)
    {
        //, 'getLogout'
        $this->userService = $userService;
        $this->productService = $productService;
        $this->beforeFilter('auth_user',
            array('except' => array('getLogin', 'postLogin', 'getRegister', 'postRegister', 'getForgotPassword', 'postForgotPassword', 'getUpdateUsersMobile','postSubscribe')));
    }

    public function getLogin()
    {
        if (AppUtil::isUserLoggedIn() && !AppUtil::isUserAdmin()) {
            return Redirect::to('/user/my-account');
        }
        $data['searchProducts'] = $this->productService->getSearchProducts();
        return View::make('frontoffice.user.login', $data);
    }

    public function postLogin()
    {
//        Session::flush();
        $data = Input::all();
        $rules = array(
            'email' => 'required|email|regex:/^(?:\w+[\.])*\w+@(?:\w+[\.])*\w+\.\w+$/',
            'password' => 'required'
        );

        $validation = Validator::make($data, $rules);
        if ($validation->passes()) {
            $email = Input::get('email');
            $password = Input::get('password');

            $credentials = array('email' => $email, 'password' => $password);
            if (Auth::attempt($credentials) && !AppUtil::isUserAdmin()) {
                $item_exists = Event::fire("cart.transfer");
                if (empty($item_exists)) {
                    return Redirect::to("home");
                } else {
                    return Redirect::to("user/address");
                }

            } else {
                Notification::error("The Email or Password you have entered is incorrect");
                return Redirect::to('user/login');
            }

        } else {
            return Redirect::to('user/login')->withErrors($validation);
        }

    }

    public function getMyAccount()
    {
        $data['searchProducts'] = $this->productService->getSearchProducts();
        return View::make('frontoffice.user.my_account', $data);
    }

    public function getRegister()
    {

//        if (Auth::check()) {
//            return Redirect::to('/user/address');
//        }
        try {
            $data['searchProducts'] = $this->productService->getSearchProducts();
            $data['register_email'] = Input::get('register_email');
            if (isset($data['register_email'])) {
                $rules = array(
                    'register_email' => 'required|email|unique:users,email|regex:/^(?:\w+[\.])*\w+@(?:\w+[\.])*\w+\.\w+$/',
                );
                $validation = Validator::make($data, $rules);
                if ($validation->passes()) {
                    $data['email'] = Input::get('register_email');
                    return View::make('frontoffice.user.register', $data);
                } else {
                    return Redirect::to('user/login')->withErrors($validation)->withInput();
                }

            } else {
                return View::make('frontoffice.user.register', $data);
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function postRegister()
    {
        try {
            $data = Input::all();
            $rules = array(
                'first_name' => 'required|alpha',
                'last_name' => 'required|alpha',
                'mobile' => 'required|numeric|digits:10',
                'email' => 'required|email|unique:users,email|regex:/^(?:\w+[\.])*\w+@(?:\w+[\.])*\w+\.\w+$/',
                'password' => 'required|min:6|max:20',
                'password_confirmation' => 'required|same:password|min:6|max:20'
            );

            $validation = Validator::make($data, $rules);
            if ($validation->passes()) {
                $first_name = Input::get('first_name');
                $last_name = Input::get('last_name');
                $email = Input::get('email');
                $mobile = Input::get('mobile');
                $password = Hash::make(Input::get('password'));
                $newsletters = Input::get('newsletters', 0);
                $special_offers = Input::get('special_offers', 0);
                $user = $this->userService->createUser($first_name, $last_name, $email, $mobile, $password, $newsletters, $special_offers);
                $user_id = $user->id;

                if($newsletters==1 || $special_offers==1){
                    $this->postSubscribe();
                }

                Event::fire('user.register', array('user_id', $user_id)); //fires user registration success event

                Auth::loginUsingId($user_id);
//                Event::fire("cart.transfer");
                $item_exists = Event::fire("cart.transfer");
                if (empty($item_exists)) {
                    return Redirect::to("home");
                } else {
                    return Redirect::to("user/address");
                }
            } else {
                return Redirect::to('user/register')->withInput($data)->withErrors($validation);
            }

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

    }

    public function getAddress()
    {
        try {

            $user_id = AppUtil::getCurrentUserId();

            //todo:check whether user will have multiple addresses or single, in the address view currently the address is retrieved by using [0]

            $user = $this->userService->getUser($user_id, null, null);

            if (!$user) {
                return Redirect::to("user/login");
            }
            $data['user'] = $user;
            $data['states'] = $this->userService->getStates();

//            echo "<pre>";print_r($data['states']);echo "</pre>";exit;
            $shipping_addresses = $this->userService->getUserAddresses($user_id, 'shipping');
            $billing_addresses = $this->userService->getUserAddresses($user_id, 'billing');

            if (count($billing_addresses) == 0 && count($shipping_addresses) == 0) {
                $data['shipping_addresses'] = $this->userService->getUserAddresses($user_id, null);
                $data['billing_addresses'] = $this->userService->getUserAddresses($user_id, null);
            } else {
                $data['shipping_addresses'] = $shipping_addresses;
                $data['billing_addresses'] = $billing_addresses;
            }

            $data['cart_items'] = AppUtil::getTotalCartItems();
            $data['searchProducts'] = $this->productService->getSearchProducts();

            return View::make("frontoffice.user.address", $data);


        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function postAddress()
    {
        try {
            $user_id = AppUtil::getCurrentUserId();
            $data = Input::all();

            $saved_shipping_address_id = Input::get('saved_shipping_address');
            if (!isset($saved_shipping_address_id)) {

                $shipping_address_rules = array(
                    'shipping_first_name' => 'required',
                    'shipping_last_name' => 'required',
                    'shipping_address_line1' => 'required|regex:/^[a-zA-Z0-9-_.,\/\s]*$/',
                    'shipping_state' => 'required',
                    'shipping_city' => 'required',
                    'shipping_pincode' => 'required|numeric',
                    'shipping_mobile' => 'required|numeric',
                );
                $validation = Validator::make($data, $shipping_address_rules);
                if ($validation->passes()) {
                    $this->addShippingAddress($validation, $user_id);
                } else {
                    return Redirect::to("user/address/")->withInput($data)->withErrors($validation);
                }
            } else {
                Session::set("shipping_address_id", $saved_shipping_address_id);
                $address = $this->userService->getAddress($saved_shipping_address_id);
                $pincode_address = $this->userService->getCityAndStateByPincode($address['pincode']);
                $is_cod_available = $pincode_address->is_cod_available;

                if ($is_cod_available == 0) {
                    Session::set('is_cod_available', false);
                    Notification::error('COD is not available for this pincode');
                } else {
                    Session::set('is_cod_available', true);
                }
//                return Redirect::to('order');
            }


            $billing_address = Input::get("billing_address");
            $saved_billing_address_id = Input::get("saved_billing_address");
            if (!isset($billing_address)) {
                if (isset($saved_billing_address_id)) {
                    Session::set("billing_address_id", $saved_billing_address_id);
                } else {
                    $billing_address_rules = array(
                        'billing_first_name' => 'required',
                        'billing_last_name' => 'required',
                        'billing_address_line1' => 'required',
                        'billing_state' => 'required',
                        'billing_city' => 'required',
                        'billing_pincode' => 'required|numeric',
                        'billing_mobile' => 'required|numeric',
                    );
                    $validation = Validator::make($data, $billing_address_rules);
                    if ($validation->passes()) {
                        $this->addBillingAddress($validation, $user_id);
                    } else {
                        return Redirect::to("user/address/")->withInput($data)->withErrors($validation);
                    }
                }

            } else {
                if (!empty($saved_shipping_address_id)) {
                    Session::set("billing_address_id", $saved_shipping_address_id);
                } else {
                    Session::set("billing_address_id", Session::get("shipping_address_id"));
                }

            }
            return Redirect::to('order');

//            $user_id = AppUtil::getCurrentUserId();
//
//            $saved_shipping_address_id = Input::get('saved_shipping_address');
//            if (!isset($saved_shipping_address_id)) {
//
//                Notification::error('Please Select your shipping address');
//                return Redirect::to("user/address/");
//
//            } else {
//                Session::set("shipping_address_id", $saved_shipping_address_id);
//
//                $address = $this->userService->getAddress($saved_shipping_address_id);
//                $pincode_address = $this->userService->getCityAndStateByPincode($address['pincode']);
//                $is_cod_available = $pincode_address->is_cod_available;
//
//                if ($is_cod_available == 0) {
//                    Session::set('is_cod_available', false);
//                    Notification::error('COD is not available for this pincode');
//                } else {
//                    Session::set('is_cod_available', true);
//                }
//
//                //$this->userService->updateUserAddressType($user_id,$saved_shipping_address_id,'shipping');
//            }
//
//            $billing_address = Input::get("billing_address");
//
//            if (isset($billing_address)) {
//                if (!empty($saved_shipping_address_id)) {
//                    Session::set("billing_address_id", $saved_shipping_address_id);
//                } else {
//                    Session::set("billing_address_id", Session::get("shipping_address_id"));
//                }
////                $address = $this->userService->getAddress($saved_shipping_address_id);
////                $this->addBillingAddress($address, $user_id);
//            } else {
//                $saved_billing_address_id = Input::get('saved_billing_address');
//                if (!isset($saved_billing_address_id)) {
//
//                    Notification::error('Please Select your billing address');
//                    return Redirect::to("user/address/");
//
//                } else {
////                    $address = $this->userService->getAddress($saved_billing_address_id);
////                    $this->addBillingAddress($address, $user_id);
//                    Session::set('billing_address_id', $saved_billing_address_id);
//                }
//            }
//            return Redirect::to('order');

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }

//        try {
//
//            $user_id = AppUtil::getCurrentUserId();
//            $data = Input::all();
//
//            $saved_shipping_address_id = Input::get('saved_shipping_address');
//            if (!isset($saved_shipping_address_id)) {
//                $address = $this->userService->getCityAndStateByPincode($data['shipping_pincode']);
//                $is_cod_available = $address->is_cod_available;
//
//                if ($is_cod_available == 0) {
//                    Session::set('is_cod_available', false);
//                    Notification::error('COD is not available for this pincode');
////                    return Redirect::back();
//                } else {
//                    Session::set('is_cod_available', true);
//                }
//                $shipping_address_rules = array(
//                    'shipping_first_name' => 'required',
//                    'shipping_last_name' => 'required',
//                    'shipping_address_line1' => 'required|regex:/^[a-zA-Z0-9-_.,\/\s]*$/',
//                    'shipping_state' => 'required',
//                    'shipping_city' => 'required',
//                    'shipping_pincode' => 'required|numeric',
//                    'shipping_mobile' => 'required|numeric',
//                );
//                $validation = Validator::make($data, $shipping_address_rules);
//                if ($validation->passes()) {
//                    $this->addShippingAddress($validation, $user_id);
//                } else {
//                    return Redirect::to("user/address/")->withErrors($validation);
//                }
//
//
//            } else {
//                Session::set("shipping_address_id", $saved_shipping_address_id);
//                $address = $this->userService->getAddress($saved_shipping_address_id);
//                $pincode_address = $this->userService->getCityAndStateByPincode($address['pincode']);
//                $is_cod_available = $pincode_address->is_cod_available;
//
//                if ($is_cod_available == 0) {
//                    Session::set('is_cod_available', false);
//                    Notification::error('COD is not available for this pincode');
////                    return Redirect::back();
//                } else {
//                    Session::set('is_cod_available', true);
//                }
////                return Redirect::to('order');
//            }
//
//
//            $billing_address = Input::get("billing_address");
//            if (!isset($billing_address)) {
//                $billing_address_rules = array(
//                    'billing_first_name' => 'required',
//                    'billing_last_name' => 'required',
//                    'billing_address_line1' => 'required',
//                    'billing_state' => 'required',
//                    'billing_city' => 'required',
//                    'billing_pincode' => 'required|numeric',
//                    'billing_mobile' => 'required|numeric',
//                );
//                $validation = Validator::make($data, $billing_address_rules);
//                if ($validation->passes()) {
//                    $this->addBillingAddress($validation, $user_id);
//                } else {
//                    return Redirect::to("user/address/")->withErrors($validation);
//                }
//
//            } else {
//                if (!empty($saved_shipping_address_id)) {
//                    Session::set("billing_address_id", $saved_shipping_address_id);
//                } else {
//                    Session::set("billing_address_id", Session::get("shipping_address_id"));
//                }
//
//            }
//            return Redirect::to('order');
//
//        } catch (Exception $ex) {
//            Log::error($ex);
//            throw $ex;
//        }

    }

    public function getDeleteAddress($address_id)
    {
        $res = $this->userService->deleteUserAddress($address_id);

        if ($res == 0) {
            Notification::error("This address is used in your order so you can't delete this address.");
        } else {
            Notification::success("Your address is deleted successfully.");
        }
        return Redirect::to('user/my-addresses');
//        return View::make('frontoffice.user.address');
    }


    /** accepts pincode and returns the corresponding city and state
     * @param int $pincode
     * @return null
     */
    public function getAjaxCityAndState($pincode)
    {
        return $this->userService->getCityAndStateByPincode($pincode);
    }

    /** accepts state and returns the corresponding cities
     * @param int $pincode
     * @return null
     */
    public function getAjaxCitiesByState($state)
    {
        $state = urldecode($state);
        return $this->userService->getCitiesByState($state);
    }

    /** accepts city and returns the corresponding pincode
     * @param int $pincode
     * @return null
     */
    public function getAjaxPincodesByCity($city)
    {
        $city = urldecode($city);
        return $this->userService->getPincodesByCity($city);
    }

    public function getChangePassword()
    {
        $data['searchProducts'] = AppUtil::getSearchProducts();
        return View::make('frontoffice.user.change_password', $data);
    }

    public function postChangePassword()
    {
        $id = AppUtil::getCurrentUserId();

        try {
            $data = Input::all();
            $rules = array(
                'current_password' => 'required',
                'password' => 'required|Confirmed',
                'password_confirmation' => 'required',
            );

            $validation = Validator::make($data, $rules);
            if ($validation->passes()) {

                $old_password = Input::get('current_password');
                $new_password = Input::get('password_confirmation');

                $user = $this->userService->getUser($id, null, null);

                if ($user) {

                    $saved_password = $user->password;
                    if (Hash::check($old_password, $saved_password)) {
                        $new_password = Hash::make($new_password);
                        $this->userService->changePassword($id, null, $new_password);
//                        Auth::logout();
                        Notification::success(Lang::get('responsemessages.change_password_success'));
                        return Redirect::to("user/change-password");

                    } else {
                        Notification::error(Lang::get('responsemessages.change_password_error'));
                        return Redirect::to('user/change-password');
                    }
                } else {

                    //todo:send proper message to view here
                }

            } else {
                return Redirect::to('user/change-password')->withErrors($validation);
            }
        } catch (Exception $ex) {
            Log::error($ex);
        }
    }

    public function getForgotPassword()
    {
        return View::make('frontoffice.user.forgotpassword');
    }

    public function postForgotPassword()
    {
        $data = Input::all();
        $rules = array(
            'email' => 'required|email'
        );

        $validation = Validator::make($data, $rules);
        if ($validation->passes()) {

            $email = Input::get("email");
            $user = $this->userService->getUser(null, $email, null);

            if ($user) {

                $user_id = $user->id;
                $new_password = str_random(8);
                $new_password = Hash::make($new_password);
                //fires forgot password event
                Event::fire('user.forgot_password', array('password' => $new_password, 'email' => $email, 'user_id' => $user_id));
                Notification::success(Lang::get('responsemessages.new_password_email_sent'));
                return Redirect::to('user/login');

            } else {

                Notification::error(Lang::get('responsemessages.reset_password_error'));
                return Redirect::to("user/forgot-password");
            }


        } else {
            return Redirect::to('user/forgot-password')->withErrors($validation);
        }
    }

    public function getLogout()
    {
        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }

    private function addShippingAddress($validation, $user_id)
    {
        $first_name = Input::get('shipping_first_name');
        $last_name = Input::get('shipping_last_name');
        $address_line1 = Input::get('shipping_address_line1');
        $address_line2 = Input::get('shipping_address_line2');
        $state = Input::get('shipping_state');
        $city = Input::get('shipping_city');
        $landmark = Input::get('shipping_landmark');
        $pincode = Input::get('shipping_pincode');
        $landline = Input::get('shipping_landline');
        $mobile = Input::get('shipping_mobile');
        $type = "shipping";

        //check whether the pincode is valid
        $existing_pincode = $this->userService->getCityAndStateByPincode($pincode);
        if (!isset($existing_pincode)) {
            $validation->getMessageBag()->add('pincode', 'Delivery not available in this area.');
            return Redirect::to("user/address/")->withErrors($validation);
        }
        $address = $this->userService->addUserAddress($user_id, $address_line1, $address_line2, $state, $city, $landmark,
            $pincode, $first_name, $last_name, $landline, $mobile, $type);

        if (isset($address)) {
            Session::set('shipping_address_id', $address->id);
        }

        $user_address = $this->userService->getAddress($address->id);
        $pincode_address = $this->userService->getCityAndStateByPincode($user_address['pincode']);
        $is_cod_available = $pincode_address->is_cod_available;

        if ($is_cod_available == 0) {
            Session::set('is_cod_available', false);
            Notification::error('COD is not available for this pincode');
        } else {
            Session::set('is_cod_available', true);
        }


    }

    private function addBillingAddress($address, $user_id)
    {
//        $address_line1 = $address->line1;
//        $address_line2 = $address->line2;
//        $state = $address->state;
//        $city = $address->city;
//        $landmark = $address->landmark;
//        $pincode = $address->pincode;
////        $user_id = $address->user_id;
//        $type = 'billing';
//        $first_name = $address->first_name;
//        $last_name = $address->last_name;
//        $landline = $address->phone_no;
//        $mobile = $address->mobile;
        $first_name = Input::get('billing_first_name');
        $last_name = Input::get('billing_last_name');
        $address_line1 = Input::get('billing_address_line1');
        $address_line2 = Input::get('billing_address_line2');
        $state = Input::get('billing_state');
        $city = Input::get('billing_city');
        $landmark = Input::get('billing_landmark');
        $pincode = Input::get('billing_pincode');
        $landline = Input::get('billing_landline');
        $mobile = Input::get('billing_mobile');
        $type = "billing";

        $address = $this->userService->addUserAddress($user_id, $address_line1, $address_line2, $state, $city, $landmark,
            $pincode, $first_name, $last_name, $landline, $mobile, $type);

        if (isset($address)) {
            Session::set('billing_address_id', $address->id);
        }

    }

    public function getMyAddresses()
    {
        try {

            $action = Input::get('action');
            if (!empty($action) && $action == 'edit') {
                $address_id = Input::get('address_id');
                $data['action'] = 'Edit Address';
                Session::set('action', $data['action']);
                $data['address'] = $this->userService->getAddress($address_id);
                if (!empty($data['address'])) {
                    $data['cities'] = $this->getAjaxCitiesByState($data['address']->state);
                    $data['pincodes'] = $this->getAjaxPincodesByCity($data['address']->city);
                }

            } else if (!empty($action) && $action == 'add') {
                $data['action'] = 'Add Address';
                Session::set('action', $data['action']);
            }

            $user_id = AppUtil::getCurrentUserId();

            $user = $this->userService->getUser($user_id, null, null);

            if (!$user) {
                return Redirect::to("user/login");
            }
            $data['user'] = $user;
            $data['states'] = $this->userService->getStates();

//            echo "<pre>";print_r($data['states']);echo "</pre>";exit;
            $data['addresses'] = $this->userService->getUserAddresses($user_id, null);
            return View::make("frontoffice.user.my_addresses", $data);

        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getMyAddress($address_id)
    {
        $address = $this->userService->getAddress($address_id);
        return $address;
    }

    public function postMyAddress()
    {
        $action = Input::get('action');

        $first_name = Input::get('first_name');
        $last_name = Input::get('last_name');
        $address_line1 = Input::get('address_line1');
        $address_line2 = Input::get('address_line2');
        $state = Input::get('state');
        $city = Input::get('city');
        $landmark = Input::get('landmark');
        $pincode = Input::get('pincode');
        $landline = Input::get('landline');
        $mobile = Input::get('mobile');
        $type = "";
        $user_id = AppUtil::getCurrentUserId();

        $address_rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'address_line1' => 'required|regex:/^[a-zA-Z0-9-_.,\/\s]*$/',
            'state' => 'required',
            'city' => 'required',
            'pincode' => 'required|numeric',
            'mobile' => 'required|numeric',
        );

        if ($action == 'edit') {
            $addres_id = Input::get('address_id');

            $validation = Validator::make(input::all(), $address_rules);
            if ($validation->passes()) {
                $this->userService->updateUserAddress($addres_id, $user_id, $address_line1, $address_line2, $state, $city, $landmark,
                    $pincode, $first_name, $last_name, $landline, $mobile, $type);
            }

        } else if ($action == 'add') {

            $validation = Validator::make(input::all(), $address_rules);
            if ($validation->passes()) {
                $this->userService->addUserAddress($user_id, $address_line1, $address_line2, $state, $city, $landmark,
                    $pincode, $first_name, $last_name, $landline, $mobile, $type);
            }
        }
        $action = Session::get('action');
        if (isset($action) && $action == 'Edit Address' || $action == 'Add Address') {
            return Redirect::to('user/address');
        } else {
            return Redirect::to('user/my-addresses');
        }

    }

    public function getUpdateUsersMobile()
    {
        $users = $this->userService->getUsers(null);
        foreach ($users as $user) {
            $user_id = $user->id;
            $mobile = $user->mobile;
            if ($mobile == null) {
                $addresses = $this->userService->getUserAddresses($user_id, null);
                if (!empty($addresses)) {
                    for ($i = 0; $i < 1; $i++) {
                        $mobile = $addresses[$i]->mobile;
                    }
                }
                $this->userService->updateUserMobile($user_id, $mobile);
            }
        }
        echo "done";
    }

    public function postSubscribe()
    {
        $sendy_url = 'http://mails.saberforms.com';

        $email = Input::get('email');
        $list = (Input::get('list') == null)? 's6gvbNcsXtCbuKOCUJb2Lw' : Input::get('list');

        $postdata = http_build_query(
            array(
                'email' => $email,
                'list' => $list,
                'boolean' => 'true'
            )
        );
        $opts = array('http' => array('method' => 'POST', 'header' => 'Content-type: application/x-www-form-urlencoded', 'content' => $postdata));
        $context = stream_context_create($opts);
        $result = file_get_contents($sendy_url . '/subscribe', false, $context);

        echo $result;
    }

}