<?php
class DashboardDocumentationController extends BaseController{

//    function __construct()
//    {
//        $this->beforeFilter("auth_admin");
//    }

    public function getIndex()
    {
       return View::make('dashboard.documentation.index');
    }

    Public function getCategories(){
        $data['active'] = 'Categories';
        return View::make('dashboard.documentation.categories',$data);
    }

    Public function getProducts(){
        $data['active'] = 'Products';
        return View::make('dashboard.documentation.products',$data);
    }

    Public function getTags(){
        $data['active'] = 'Tags';
        return View::make('dashboard.documentation.tags',$data);
    }

    Public function getCombos(){
        $data['active'] = 'Combos';
        return View::make('dashboard.documentation.combos',$data);
    }

    Public function getOrders(){
        $data['active'] = 'Orders';
        return View::make('dashboard.documentation.orders',$data);
    }

    Public function getCustomers(){
        $data['active'] = 'Customers';
        return View::make('dashboard.documentation.customers',$data);
    }

    Public function getDealers(){
        $data['active'] = 'Dealers';
        return View::make('dashboard.documentation.dealers',$data);
    }

    Public function getServiceCenters(){
        $data['active'] = 'Service Centers';
        return View::make('dashboard.documentation.service_centers',$data);
    }

    Public function getDemos(){
        $data['active'] = 'Demos';
        return View::make('dashboard.documentation.demos',$data);
    }

    Public function getCoupons(){
        $data['active'] = 'Coupons';
        return View::make('dashboard.documentation.coupons',$data);
    }

    public function getMessages(){
        $data['active'] = 'Messages';
        return View::make('dashboard.documentation.messages',$data);
    }

    public function getCalls(){
        $data['active'] = 'Calls';
        return View::make('dashboard.documentation.calls',$data);
    }

}
?>
