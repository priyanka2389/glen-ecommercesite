<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/20/14
 * Time: 4:55 PM
 */
class DemoController extends BaseController
{
    function __construct(EloquentDemoRepository $eloquentDemoRepository)
    {
        $this->eloquentDemoRepository = $eloquentDemoRepository;
    }

    public function postData()
    {
        $name = Input::get('name', null);
        $email = Input::get('email', null);
        $phone = Input::get('mobile', null);
        $state = Input::get('state', null);
        $city = Input::get('city', null);
        $message = Input::get('message', null);
        $product_id = Input::get('product');

        $this->eloquentDemoRepository->createDemo($name, $email, $phone, $state, $city, $message, $product_id);
        return Response::json("true");
    }


} 