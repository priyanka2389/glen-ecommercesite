<?php

use SoapBox\Formatter\Formatter;

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 5/12/14
 * Time: 10:30 AM
 */
class DashboardDealersController extends BaseController
{
    function __construct(ProductService $productService, DealersService $dealersService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->productService = $productService;
        $this->dealersService = $dealersService;
    }

    public function getIndex()
    {
        $data['dealers'] = $this->dealersService->getDealers(null, null, Constants::DASHBOARD_DEALERS_PAGE_COUNT);

        return View::make('dashboard.dealers.index', $data);
    }

    public function getCreate()
    {
        $data['dealers'] = $this->dealersService->getDealers(null, null, null);

//        foreach($data['dealers'] as $dealer){
//            DB::table('dealers')
//                ->where('id', $dealer['id'] )
//                ->update(array('sequence' => Dealer::max('sequence') + 1 ));
//        }

        return View::make('dashboard.dealers.create', $data);
    }

    public function postCreate()
    {
        try {
            $validator = new \services\Validators\DealerValidator();
            if ($validator->passes()) {
                $shop_name = Input::get('name');
                $address1 = Input::get('address1');
                $address2 = Input::get('address2', null);
                $city = Input::get('city');
                $state = Input::get('state');
                $pincode = Input::get('pincode');
                $mobile = Input::get('mobile');
                $phone = Input::get('phone', null);
                $sequence = Input::get('sequence');
                $small_appliance = Input::get('small_appliance');
                $small_appliance = isset($small_appliance) ? true : false;

                $large_appliance = Input::get('large_appliance');
                $large_appliance = isset($large_appliance) ? true : false;

                $email_id = Input::get('email');
                $contact_person = Input::get('contact_person');
                $type = Input::get('type');

                $active = Input::get('active');
                $active = isset($active) ? true : false;

                if ($sequence == 'after') {
                    $sequence = Input::get('after');
                }

                $this->dealersService->createDealer($shop_name, $address1, $address2, $city, $state, $pincode, $mobile,
                    $phone, $small_appliance, $large_appliance, $active, $sequence,$contact_person,$email_id,$type);
                Notification::success("Dealer has been added successfully");
                return Redirect::to("dashboard/dealers");
            } else {
                $error = $validator->getErrors();
                return Redirect::to("dashboard/dealers/create")->withInput(Input::all())->withErrors($error);
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getEdit($id)
    {
        $data['dealer'] = $this->dealersService->getDealer($id);
        $data['sequence'] = $this->dealersService->getDealerPosition($id);
        $data['dealers'] = $this->dealersService->getDealers(null, null, null);
        return View::make('dashboard.dealers.edit', $data);
    }

    public function postEdit($id)
    {
        try {

            $validator = new \services\Validators\DealerValidator();
            if ($validator->passes()) {
                $shop_name = Input::get('name');
                $address1 = Input::get('address1');
                $address2 = Input::get('address2', null);
                $city = Input::get('city');
                $state = Input::get('state');
                $pincode = Input::get('pincode');
                $mobile = Input::get('mobile');
                $phone = Input::get('phone', null);
                $sequence = Input::get('sequence');
                $small_appliance = Input::get('small_appliance');
                $small_appliance = isset($small_appliance) ? true : false;

                $large_appliance = Input::get('large_appliance');
                $large_appliance = isset($large_appliance) ? true : false;

                $email_id = Input::get('email');
                $contact_person = Input::get('contact_person');
                $type = Input::get('type');

                $active = Input::get('active');
                $active = isset($active) ? true : false;

                if ($sequence == 'after') {
                    $sequence = Input::get('after');
                }

                $this->dealersService->updateDealer($id, $shop_name, $address1, $address2, $city, $state, $pincode, $mobile,
                    $phone, $small_appliance, $large_appliance, $active, $sequence,$contact_person,$email_id,$type);
                Notification::success("Dealer has been updated successfully");
                return Redirect::to("dashboard/dealers");
            } else {
                $error = $validator->getErrors();
                return Redirect::to("dealers/edit/$id")->withErrors($error);
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getUpdateApplianceType($id, $type, $status)
    {
        $this->dealersService->updateApplianceType($id, $type, $status);
        return Redirect::to('dashboard/dealers');
    }

    public function getActivateOrDeactivate($id, $status)
    {
        $this->dealersService->activateOrDeactivate($id, $status);
        return Redirect::to('dashboard/dealers');
    }

    public function getDelete($id)
    {
        $this->dealersService->deleteDealers(array($id));
        return Redirect::to("dashboard/dealers");
    }

    public function postUploadCsv()
    {
        $rules = array(
            'file' => 'required|mimes:xls,txt,csv'
        );
        $validator = Validator::make(Input::all(), $rules);
        $file = Input::file('file');


        if ($validator->passes()) {

            if (Input::hasFile('file')) {
                $file_info = AppUtil::moveFile($file, 'dealer_list');
                if (strtolower($file_info['type']) == 'csv') {

                    $header = NULL;
                    $dealers = array();
                    $csvFile = public_path() . "/" . $file_info['path'];
                    if (($file_handle = fopen($csvFile, 'r')) !== FALSE) {
                        while (($row = fgetcsv($file_handle, 1000)) !== FALSE) {
                            if (!$header)
                                $header = $row;
                            else
                                $dealers[] = array_combine($header, $row);
                        }
                        fclose($file_handle);
                    }
                    $this->dealersService->getTruncateDealer();

                    foreach ($dealers as $dealer) {

                        $shop_name = ($dealer['name']) ? $dealer['name'] : '';
                        $address1 = ($dealer['address1']) ? $dealer['address1'] : '';
                        $address2 = ($dealer['address2']) ? $dealer['address2'] : '';
                        $city = ($dealer['city']) ? $dealer['city'] : '';
                        $state = ($dealer['state']) ? $dealer['state'] : '';
                        $pincode = ($dealer['pincode']) ? $dealer['pincode'] : '';
                        $mobile = ($dealer['mobile']) ? $dealer['mobile'] : '';
                        $phone = ($dealer['phone']) ? $dealer['phone'] : '';

                        $small_appliance = ($dealer['is_small_appliance']) ? $dealer['is_small_appliance'] : '';
                        $small_appliance = isset($small_appliance) ? true : false;

                        $large_appliance = ($dealer['is_large_appliance']) ? $dealer['is_large_appliance'] : '';
                        $large_appliance = isset($large_appliance) ? true : false;

                        $active = ($dealer['is_active']) ? $dealer['is_active'] : '';
                        $active = isset($active) ? true : false;

                        $sequence = ($dealer['sequence']) ? $dealer['sequence'] : '';


                        $this->dealersService->createDealer($shop_name, $address1, $address2, $city, $state, $pincode, $mobile,
                            $phone, $small_appliance, $large_appliance, $active, $sequence);


                    }

                    // echo "<pre>";print_r($line_of_text);echo "</pre>";exit;

                    Notification::success("Dealers has been added successfully");
                    return Redirect::to('dashboard/dealers');
                }
            }
        } else {
            $errors = $validator->errors();
            return Redirect::to('dashboard/dealers')->withErrors($errors);
        }

    }

    public function getDownloadCsv()
    {
        $dealers = $this->dealersService->getDealers(null, null, null);
        $this->dealersService->getDealersCsv($dealers);
    }


} 