<?php

/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 12/30/13
 * Time: 4:30 PM
 */
class TestController extends BaseController
{
    function __construct(ProductService $productRepo, CategoryService $categoryService,
                         UserService $userService, EloquentComboRepository $comboRepo,
                         EloquentTagRepository $tagRepo, EloquentImageRepository $imageRepo,
                         CartService $cartService, DealersService $dealersService, CouponService $couponService)
    {
        $this->productRepo = $productRepo;
        $this->categoryService = $categoryService;
        $this->userService = $userService;
        $this->comboRepo = $comboRepo;
        $this->tagRepo = $tagRepo;
        $this->imageRepo = $imageRepo;
        $this->cartService = $cartService;
        $this->dealersService = $dealersService;
        $this->couponService = $couponService;
    }

    function getAttribute()
    {
        $result = Attribute::where('name', '<=', 'size')->get();
        $productRepo = new EloquentProductRepository();

    }

    function getCategoryAttributes()
    {
        //$result = $this->categoryService->getCategoryAttributesWithValues(1,null,null,null);
        //$result = $this->categoryService->getCategoryAttributes(1, null, null, null);
        //$result= $this->categoryService->getCategoryParent(3);
        $this->categoryService->deleteCategoryVideo(1, array(1, 2));
        //dd($result);
    }

    function getCompare()
    {
//        $result = $this->productRepo->compare(array(1, 2, 3));
//        dd($result);
//        $result = $this->productRepo->getProductTopCategory(9);
//        dd($result);
        Session::flush();
        $session = [1, 2, 3];
        foreach ($session as $row) {

        }
        $result = Session::get('id');
        dd($result);
//        Session::flush();


    }


    function getProduct()
    {
        $product = $this->productRepo->getProductBasicInfo(1, null, null, null, null);

        $images = AppUtil::getProductPrimaryImage(500);
        dd($images);
        //$this->categoryService->getCategoryFilters($)
//        $results = $this->productRepo->getProducts(1, null, null, null, null, null, null, 10);
        //$result = $this->productRepo->getProductTopCategory(2);
        //$product = new EloquentProductRepository();
        //$result = $product->getProducts(1, null, null, null, null, null, null, null);
        //$result = $this->productRepo->getProduct(3, null, null, null);
        //return View::make('test', $data);
    }

    function getProductAttributes()
    {

        //$this->productRepo->createProductAttributes(1, 2, 50, "weight is 50kg");
        $this->productRepo->createOrUpdateProductAttributes(2, 2, 90);
        //$this->productRepo->deleteProductAttribute(2, array(1));
//        $result = $this->productRepo->getProductAttributesValue(2);
//        dd($result);
    }

    function getGenerateHash()
    {
        $file1 = public_path("video.mkv");
        $file2 = public_path("testsong2.mp3");
        $hash1 = AppUtil::generateDocumentHash($file1);
        $hash2 = AppUtil::generateDocumentHash($file2);

        var_dump($hash1);
        var_dump($hash2);

    }

    public function getPrimaryImage()
    {
//        $result = AppUtil::getProductPrimaryImage(1);
//        dd(($result));
        //$this->productRepo->setProductPrimaryImage(1, 2);

    }

    public function getProductDocuments()
    {
        // $result = $this->productRepo->getProductDocuments(1);
        $result = $this->productRepo->getProductDocument(1, 1);
        return $result;
    }

    public function getProductTag()
    { // TURN ON ERROR REPORTING
//        ini_set("display_errors", "On");
//        ini_set("log_errors", "On");
//        error_reporting(E_ALL);
//        echo file_get_contents("http://enterprise.smsgupshup.com/GatewayAPI/rest?method=sendMessage&send_to=9971295558&msg=Your+one+time+password+for+user+account+with+GLEN+is+o7QScy,+valid+for+2+hours.+Kindly+enter+this+OTP+as+prompted+by+IVR.&msg_type=TEXT&userid=2000116253&auth_scheme=PLAIN&password=Glen@1234&v=1.1&format=text");
//        exit;
//        $_h = curl_init();
//        curl_setopt($_h, CURLOPT_HEADER, 1);
//        curl_setopt($_h, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($_h, CURLOPT_HTTPGET, 1);
//        curl_setopt($_h, CURLOPT_SSL_VERIFYHOST, false);
//        curl_setopt($_h, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($_h, CURLOPT_URL, 'http://google.com');
//        curl_setopt($_h, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
//        curl_setopt($_h, CURLOPT_DNS_CACHE_TIMEOUT, 2);
//
//        var_dump(curl_exec($_h));
//        var_dump(curl_getinfo($_h));
//        var_dump(curl_error($_h));
    }

    public function getDeleteDocument()
    {
        $this->productRepo->deleteProductDocument(1, 1);
    }


    public function getUserRole()
    {
        $result = $this->userService->getUserRole(1);
        dd($result);
    }

    public function getLoggedInUser()
    {
        $result = AppUtil::getUserEmail();
        dd($result);
    }

    public function getRelatedProducts()
    {
        $result = $this->productRepo->getRelatedProducts(1);
        dd($result);
    }

    public function getFilters()
    {
        $final_array = [];
        $array = array(
            'size' => 123,
        );
        $results = $this->productRepo->getFilteredProducts($array, null, 3, null, null);
        dd($results);
        if (!is_null($results)) {
            foreach ($results as $i => $row) {
                if ($row->images->count() != 0) {
                    $image_path = $row->images[0]->path;
                } else {
                    $image_path = "";
                }

                if ($row->combos->count() != 0) {
                    $combo = "true";
                } else {
                    $combo = 'false';
                }

                $data = array(
                    'name' => $row->name,
                    'image' => $image_path,
                    'combo' => $combo
                );
                $final_array[$i] = $data;
            }

            dd($final_array);

        } else {
            return null;
        }

    }


    //return products after filtering
    public function getFilteredProducts()
    {
        $array = array(
            'size' => 123,
            'air_flow' => 200,
        );

        $category_id = 1;

        $price = array('min' => 10, 'max' => 100);

        $array_size = sizeof($array);

        $result = ProductAttribute::join("products", 'productattributes.product_id', '=', 'products.id', 'left')
            ->join("attributes", 'productattributes.attribute_id', '=', 'attributes.id', 'left')
            ->where(function ($query) use ($array) {

                foreach ($array as $key => $value) {

                    $query->orwhere(function ($inner) use ($key, $value) {

                        $inner->where("attributes.name", '=', $key)->where("productattributes.value", '=', $value);
                    });
                }
            })
            ->groupBy("product_id")->havingRaw("COUNT(product_id)=$array_size")
            ->where(function ($query) use ($price) {

                if (sizeof($price) != 0) {
                    $query->where('list_price', '>=', $price['min'])->where('list_price', '<=', $price['max']);
                }

            })->where('products.category_id', '=', $category_id)->get();


        dd($result);
        //dd(DB::getQueryLog());
        foreach ($result as $row) {

            var_dump($row);
        }
    }


    public function getCreateCombo()
    {

    }

    public function getProductCombos()
    {
        $result = $this->comboRepo->getComboProducts(1);
        dd($result);
    }

    public function getTag()
    {
        $productrepo = new EloquentProductRepository();
        //$productrepo->createProductTag('bad offer', "BAD", "this is bad offer", '10', 2);
        //$productrepo->updateProductTag(1, 4, 'cool offer', 'COOL', 'this is cool offer', '200');
        //$productrepo->deleteProductTag(array(7));
    }

    public function getSequence()
    {

//        $this->productRepo->createProduct("grinder", "GRIND895", "GRID", "this is grinder", 'grinder secondary desc', 1, null,
//            true, true, true, true, 1, 200, 150, 50, 20, 'it grinds',5);

        $sequence = AppUtil::getSequence(50);

        //dd($sequence);
        //$this->productRepo->updateProduct(2, "chimney_beauty", "qwerty", "CHM987", "this is test chimeny 2", 'secondary description', 2, 2, true, true, true, true, 1, 500, 10, 100, $sequence, 'this chimney is beautiful', 2,);

    }

    public function postUploadImage()
    {

        $image = Input::file("image");
        $name = Input::get('name');
        $title = Input::get('title');
        $caption = Input::get('caption');
        $notes = Input::get('notes');
        $is_primary = true;


        $ext = $image->getClientOriginalExtension();
        $file_name = $name . "." . $ext;
        $path = public_path("uploads/products/img/org/");

        $image->move($path, $file_name);

        $product = new EloquentProductRepository();
        $result = $product->createProductImage($path, $name, $title, $caption, $notes, $is_primary, 1);

        $image_path = public_path("uploads/products/img/org/$file_name");
        $image_id = $result->id;
        AppUtil::resizeImage($image_path, $image_id);


    }

    public function getImage()
    {
        $result = AppUtil::getProductPrimaryImage(1);
        dd($result);
//        $productrepo = new EloquentProductRepository();
//        $productrepo->createProductImage("fake_path1", "prodcut name 1", "product title 1",
//            "product caption 1", "this is notes", true, 1);
//
//        $productrepo->createProductImage("fake_path2", "prodcut name 2", "product title 2",
//            "product caption 1", "this is notes", true, 2);
    }

    public function getCategoryDocuments()
    {
        //$this->categoryService->getCategoryDocuments(1, null);
        $this->categoryService->createCategoryDocuments('pdf', 'fakepath', 1, null, 'test_label', 'custom name', 'title', 'tyest notes', 1);
    }

    public function getCart()
    {
        $result = $this->cartService->getCartContents(2);
        dd($result);

    }

    public function postValidation()
    {
        $validation = new \services\Validators\ProductValidator();
        if ($validation->passes()) {
            echo "success";
        } else {
            dd($validation->getErrors());
        }
    }

    public function getDealer()
    {
        $dealer = new EloquentDealersRepository();
        $result = $dealer->getDealers(null, null, 100);
        dd(AppUtil::returnResults($result));
    }

    public function getTree()
    {
        $result = $this->getCategoryTree();
        dd($result);
        //return View::make('test');
    }

    public function getImages()
    {
        for ($i = 1; $i <= 7; $i++) {
            $image = new Image();
            $image->path = "http://placehold.it/300x300";
            $image->save();
        }
    }

    public function getHtml()
    {
        $tree = $this->getCategoryTreeHtml();
        echo $tree;
    }

    /** returns the formatted html of the category tree
     * @return string
     */
    public function getCategoryTreeHtml()
    {

        $tree = "";

        $array = $this->categoryService->getCategoryTree();
        foreach ($array as $i => $row) {

            $tree = $tree . '<li> <i class="fa-li fa fa-arrow-circle-o-right"> </i>' . $row['name'];
            if (isset($row['children'])) {
                $tree = $this->buildChildHtml($row['children'], $tree);
            }
            $tree = $tree . '</li>';

        }

        $data['tree'] = $tree;
        return View::make('test', $data);

    }


    private function buildChildHtml($category, $tree)
    {
        $tree = $tree . '<ul class=fa-ul>';
        foreach ($category as $i => $row) {

            $tree = $tree . '<li class="hide child"> <i class="fa-li fa fa-arrow-circle-o-right"> </i>' . $row['name'];
            if (isset($row['children'])) {
                $tree = $this->buildChildHtml($row['children'], $tree);
            }
            $tree = $tree . '</li>';
        }
        $tree = $tree . '</ul>';
        return $tree;
    }


    public function getTest()
    {
        $result = $this->productRepo->getProductsFilterHtml(array(1, 2));
        echo $result;
        exit;


        $filter_array = array();
        $attribute_value = array();
        $price = array();

        $attributes = $this->productRepo->getProductsFilter(array(1, 2));
        $min_price_product = Product::whereIn('id', array(1, 2))->min('list_price');
        $max_price_product = Product::whereIn('id', array(1, 8))->max('list_price');

        $price_range = $this->getPriceRange($min_price_product, $max_price_product);

        if (!is_null($attributes)) {
            foreach ($attributes as $attribute) {
                if ($attribute->products->count() != 0) {

                    $products = $attribute->products;
                    $attribute_id = $attribute->id;
                    $attribute_name = $attribute->name;
                    foreach ($products as $product) {
                        $attribute_value[] = trim($product->pivot->value);
                        $price[] = $product->list_price;
                    }
                    $data = array('id' => $attribute_id, 'values' => array_unique($attribute_value));
                    $filter_array[$attribute_name] = $data;
                    $attribute_value = array();
                }
            }
        } else {
            $filter_array = null;
        }

        dd(array_merge($filter_array, $price_range));
    }

    public function getLatestOffers()
    {
        $limit = null;
        $query = Product::query();
        $query->select(DB::raw('*, `list_price` - `offer_price` as remaining'))
            ->having('remaining', '>=', 100)->orderBy('created_at', "DESC");

        if (!is_null($limit)) {
            return $query->limit($limit);
        } else {
            $results = $query->get();
            $image = $results[1]->images;
        }
    }

    private function getPriceRange($min_price_product, $max_price_product)
    {
        $price_range = array();
        $remainder = floor($min_price_product) % 10;
        $min_price_product = $min_price_product - $remainder;

        $range = Constants::PRICE_RANGE;

        for ($i = $min_price_product; $i < $max_price_product; $i = $i + $range) {

            $max = $min_price_product + $range;
            $price_range['price']['id'] = 0;
            $price_range['price']['values'][] = (int)$min_price_product . '-' . (int)$max;
            $min_price_product = $max;
        }
        return $price_range;
    }

    public function getDelete()
    {
        if (Request::getMethod() == 'POST') {
            $rules = array('captcha' => array('required', 'captcha'));
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                echo '<p style="color: #ff0000;">Incorrect!</p>';
            } else {
                echo '<p style="color: #00ff30;">Matched :)</p>';
            }
        }

        $content = Form::open(array(URL::to(Request::segment(1))));
        $content .= '<p>' . HTML::image(Captcha::img(), 'Captcha image') . '</p>';
        $content .= '<p>' . Form::text('captcha') . '</p>';
        $content .= '<p>' . Form::submit('Check') . '</p>';
        $content .= '<p>' . Form::close() . '</p>';
        echo $content;

        //return View::make('frontoffice.contact_us');
    }

    public function getSend()
    {
        $data = array();
        Mail::queue('emails.test', $data, function ($message) {
            $message->to('ankumar@greenapplesolutions.com', 'Test')->subject('Welcome!');
        });
        echo "success";
    }

    public function getMail()
    {
        $data = array();
        Mail::queue('emails.test', $data, function ($message) {
            $message->to('ankumar@greenapplesolutions.com', 'Test')->subject('Welcome!');
        });
        echo "success";
    }


    public function getProductAncestors($id)
    {
//        $result = $this->productRepo->getProductAncestors($id);
//        Log::error("test");
//        dd($result);
    }

//    public function getSetSequence()
//    {
//        $products = Product::all();
//        DB::beginTransaction();
//        $i = 1;
//        foreach ($products as $product) {
//
//            Product::where("id", '=', $product->id)->update(array('sequence' => $i));
//            $i++;
//
//        }
//        DB::commit();
//    }

    public function getSetProduct()
    {
        //bring product up
        $product_sequence_id = 6;
        $after_sequence_id = 2;

        $product = Product::query();
        echo Product::max('sequence');
//        $result = $product->where("id", '=', 1)->get();
//        dd($result);
//
//        if ($product_sequence_id > $after_sequence_id) {
//
//            $product = Product::where('sequence', '=', $product_sequence_id)->first();
//            Product::where("sequence", '>', $after_sequence_id)->where('sequence', '<', $product_sequence_id)->increment('sequence', 1);
//            $product->sequence = $after_sequence_id + 1;
//            $product->save();
//
//        } else {
//
//            Product::where('sequence', '>', $product_sequence_id)->where('sequence', '<=', $after_sequence_id)->decrement('sequence', 1);
//            Product::where('sequence', '=', $product_sequence_id)->update(array('sequence' => $after_sequence_id));
//        }

        // AppUtil::setSequence($product, $product_sequence_id, $after_sequence_id);


    }

//    public function getAddDealers()
//    {
//        $alda_dealers = DB::table('alda_dealers')->get();
//
//        DB::beginTransaction();
//        foreach ($alda_dealers as $row) {
//
//            $name = $row->shop_name;
//            $address1 = $row->address;
//            $city = $row->city;
//            $state = $row->state;
//            $pincode = $row->pin_code;
//            $phone = $row->phone;
//            $small_appliance = $row->is_cookware;
//            $large_appliance = $row->is_large_appliance;
//            $this->dealersService->createDealer($name, $address1, null, $city, $state, $pincode, null, $phone,
//                $small_appliance, $large_appliance, 1);
//
//        }
//        DB::commit();
//    }

    public function getValue()
    {
        //$data['order_id'] = 1;
//        return View::make("frontoffice.order.success");
        // Feedback::all();
//        $result = $this->couponService->getMainCouponCategory(1, 28);
//        dd($result);
        //Session::push('session_info.session_code', 'developers');
        //Session::push('session_info.session_value', '123');
        (Session::set('session_info', null));
        var_dump(Session::get("session_info"));

    }


}
