<?php

class DashboardRetailStoresController extends \BaseController
{

    function __construct(RetailStoresService $retailStoreService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->retailStoreServie = $retailStoreService;
    }

    public function getIndex()
    {
        $data['retailStores'] = $this->retailStoreServie->getRetailStores(null, null, null, Constants::DASHBOARD_RETAILSTORES_PAGE_COUNT);

        return View::make('dashboard.retailstores.index', $data);
    }

    public function getCreate()
    {
        $data['retailStores'] = $this->retailStoreServie->getRetailStores(null, null, null, null);

//        foreach($data['dealers'] as $dealer){
//            DB::table('dealers')
//                ->where('id', $dealer['id'] )
//                ->update(array('sequence' => Dealer::max('sequence') + 1 ));
//        }

        return View::make('dashboard.retailstores.create', $data);
    }

    public function postCreate()
    {
        try {
            $validator = new \services\Validators\RetailStoresValidator();
            if ($validator->passes()) {
//                $shop_name = Input::get('name');
                $email = Input::get('email');
                $address = Input::get('address1');
//                $address2 = Input::get('address2', null);
                $city = Input::get('city');
                $state = Input::get('state');
                $pincode = Input::get('pincode');
                $mobile = Input::get('mobile');
                $phone = Input::get('phone', null);
                $contact_person = Input::get('contact_person');
                $sequence = Input::get('sequence');
//                $small_appliance = Input::get('small_appliance');
//                $small_appliance = isset($small_appliance) ? true : false;
//
//                $large_appliance = Input::get('large_appliance');
//                $large_appliance = isset($large_appliance) ? true : false;

                $active = Input::get('active');
                $active = isset($active) ? true : false;

                if ($sequence == 'after') {
                    $sequence = Input::get('after');
                }

                $this->retailStoreServie->createRetailStore($email, $address, $city, $state, $pincode, $mobile, $phone, $contact_person, $active, $sequence);
                Notification::success("Retail Store has been added successfully");
                return Redirect::to("dashboard/retail-stores");
            } else {
                $error = $validator->getErrors();
                return Redirect::to("dashboard/retail-stores/create")->withInput(Input::all())->withErrors($error);
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getEdit($id)
    {
        $data['retailStore'] = $this->retailStoreServie->getRetailStore($id);
        $data['sequence'] = $this->retailStoreServie->getRetailStoresPosition($id);
        $data['retailStores'] = $this->retailStoreServie->getRetailStores(null, null, null, null);
        return View::make('dashboard.retailstores.edit', $data);
    }

    public function postEdit($id)
    {
        try {

            $validator = new \services\Validators\RetailStoresValidator();
            if ($validator->passes()) {
//                $shop_name = Input::get('name');
                $email = Input::get('email');
                $address = Input::get('address1');
//                $address2 = Input::get('address2', null);
                $city = Input::get('city');
                $state = Input::get('state');
                $pincode = Input::get('pincode');
                $mobile = Input::get('mobile');
                $phone = Input::get('phone', null);
                $contact_person = Input::get('contact_person');
                $sequence = Input::get('sequence');
//                $small_appliance = Input::get('small_appliance');
//                $small_appliance = isset($small_appliance) ? true : false;
//
//                $large_appliance = Input::get('large_appliance');
//                $large_appliance = isset($large_appliance) ? true : false;

                $active = Input::get('active');
                $active = isset($active) ? true : false;

                if ($sequence == 'after') {
                    $sequence = Input::get('after');
                }

                $this->retailStoreServie->updateRetailStore($id, $email, $address, $city, $state, $pincode, $mobile, $phone, $contact_person, $active, $sequence);
                Notification::success("Retail Store has been edited successfully");
                return Redirect::to("dashboard/retail-stores");
            } else {
                $error = $validator->getErrors();
                return Redirect::to("retail-stores/edit/$id")->withErrors($error);
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getUpdateApplianceType($id, $type, $status)
    {
        $this->retailStoreServie->updateApplianceType($id, $type, $status);
        return Redirect::to('dashboard/retail-stores');
    }

    public function getActivateOrDeactivate($id, $status)
    {
        $this->retailStoreServie->activateOrDeactivate($id, $status);
        return Redirect::to('dashboard/retail-stores');
    }

    public function getDelete($id)
    {
        $this->retailStoreServie->deleteRetailStores(array($id));
        return Redirect::to("dashboard/retail-stores");
    }


}
