<?php

class DashboardDistributorsController extends \BaseController
{

    function __construct(DistributorsService $distributorsService)
    {
        $this->beforeFilter("auth_admin", array('except' => array()));
        $this->disributorService = $distributorsService;
    }

    public function getIndex()
    {
        $data['distributors'] = $this->disributorService->getDistributors(null, null, null, Constants::DASHBOARD_DISTRIBUTORS_PAGE_COUNT);

        return View::make('dashboard.distributors.index', $data);
    }

    public function getCreate()
    {
        $data['distributors'] = $this->disributorService->getDistributors(null, null, null, null);

//        foreach($data['dealers'] as $dealer){
//            DB::table('dealers')
//                ->where('id', $dealer['id'] )
//                ->update(array('sequence' => Dealer::max('sequence') + 1 ));
//        }

        return View::make('dashboard.distributors.create', $data);
    }

    public function postCreate()
    {
        try {
            $validator = new \services\Validators\DistributorValidator();
            if ($validator->passes()) {
                $shop_name = Input::get('name');
                $email = Input::get('email');
                $address1 = Input::get('address1');
                $address2 = Input::get('address2', null);
                $city = Input::get('city');
                $state = Input::get('state');
                $pincode = Input::get('pincode');
                $mobile = Input::get('mobile');
                $phone = Input::get('phone', null);
                $contact_person = Input::get('contact_person');
                $sequence = Input::get('sequence');
                $small_appliance = Input::get('small_appliance');
                $small_appliance = isset($small_appliance) ? true : false;

                $large_appliance = Input::get('large_appliance');
                $large_appliance = isset($large_appliance) ? true : false;

                $active = Input::get('active');
                $active = isset($active) ? true : false;

                if ($sequence == 'after') {
                    $sequence = Input::get('after');
                }

                $this->disributorService->createDistributor($shop_name, $email, $address1, $address2, $city, $state, $pincode, $mobile,
                    $phone, $contact_person, $small_appliance, $large_appliance, $active, $sequence);
                Notification::success("Distributors has been added successfully");
                return Redirect::to("dashboard/distributors");
            } else {
                $error = $validator->getErrors();
                return Redirect::to("distributors/create")->withInput(Input::all())->withErrors($error);
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getEdit($id)
    {
        $data['distributor'] = $this->disributorService->getDistributor($id);
        $data['sequence'] = $this->disributorService->getDistributorsPosition($id);
        $data['distributors'] = $this->disributorService->getDistributors(null, null, null, null);
        return View::make('dashboard.distributors.edit', $data);
    }

    public function postEdit($id)
    {
        try {

            $validator = new \services\Validators\DistributorValidator();
            if ($validator->passes()) {
                $shop_name = Input::get('name');
                $email = Input::get('email');
                $address1 = Input::get('address1');
                $address2 = Input::get('address2', null);
                $city = Input::get('city');
                $state = Input::get('state');
                $pincode = Input::get('pincode');
                $mobile = Input::get('mobile');
                $phone = Input::get('phone', null);
                $contact_person = Input::get('contact_person');
                $sequence = Input::get('sequence');
                $small_appliance = Input::get('small_appliance');
                $small_appliance = isset($small_appliance) ? true : false;

                $large_appliance = Input::get('large_appliance');
                $large_appliance = isset($large_appliance) ? true : false;

                $active = Input::get('active');
                $active = isset($active) ? true : false;

                if ($sequence == 'after') {
                    $sequence = Input::get('after');
                }

                $this->disributorService->updateDistributor($id, $shop_name, $email, $address1, $address2, $city, $state, $pincode, $mobile,
                    $phone, $contact_person, $small_appliance, $large_appliance, $active, $sequence);
                Notification::success("Dealer has been edited successfully");
                return Redirect::to("dashboard/distributors");
            } else {
                $error = $validator->getErrors();
                return Redirect::to("distributors/edit/$id")->withErrors($error);
            }
        } catch (Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }

    public function getUpdateApplianceType($id, $type, $status)
    {
        $this->disributorService->updateApplianceType($id, $type, $status);
        return Redirect::to('dashboard/distributors');
    }

    public function getActivateOrDeactivate($id, $status)
    {
        $this->disributorService->activateOrDeactivate($id, $status);
        return Redirect::to('dashboard/distributors');
    }

    public function getDelete($id)
    {
        $this->disributorService->deleteDistributors(array($id));
        return Redirect::to("dashboard/distributors");
    }


}
