@extends('layouts.frontoffice.default')
@section('content')

    <script type="text/javascript"
            src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#form').validate();
        });
    </script>
    <style type="text/css">
        strong {
            color: #17479d;
        }

        table {
            line-height: 21px;
        }
    </style>

    <section id="columns" class="clearfix">
        <div class="container">
            <div class="row-fluid">
                <section id="center_column" class="span12">
                    <div class="contenttop row-fluid">
                        <h1 id="cart_title" class="title_category">Request A Call Back</h1>


                        <div class="row-fluid">
                            <div class="span12">
                                <div class="col-lg-12">
                                    {{Notification::showSuccess()}}
                                </div>

                                <div class="col-lg-12">
                                    {{Notification::showError()}}
                                </div>

                                <form action="{{URL::to('request-a-call')}}" method="post"
                                      class="std form-horizontal" id="form" enctype="multipart/form-data">
                                    <fieldset>
                                        <h3 class="title_content">Please share your details below, our team will get in
                                            touch shortly :</h3>

                                        <div class="control-group text">
                                            <label for="name" class="control-label">Name<sup>*</sup></label>

                                            <div class="controls">
                                                <input type="text" name="name" class="required" id="name"
                                                       value="{{Input::old('name')}}">
                                                <span class="error">{{$errors->first('name')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group text">
                                            <label for="mobile" class="control-label">Mobile<sup>*</sup></label>

                                            <div class="controls">
                                                <input type="text" name="mobile" class="required digits" id="mobile"
                                                       value="{{Input::old('mobile')}}">
                                                <span class="error">{{$errors->first('mobile')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group text">
                                            <label for="email" class="control-label">Email</label>

                                            <div class="controls">
                                                <input type="text" name="email" class="email" id="email"
                                                       value="{{Input::old('email')}}">
                                                <span class="error">{{$errors->first('email')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group text">
                                            <label for="city" class="control-label">City<sup>*</sup></label>

                                            <div class="controls">
                                                <input type="text" name="city" class="required" id="city"
                                                       value="{{Input::old('city')}}">
                                                <span class="error">{{$errors->first('city')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group text">
                                            <label for="message" class="control-label">Message</label>

                                            <div class="controls">
                                                <textarea name="message" cols="30" rows="4"
                                                          id="message"> {{Input::old('message')}}</textarea>
                                                <input type="hidden" name="source" id="source"
                                                       value="{{$source or ''}}">
                                                <span class="error">{{$errors->first('message')}}</span>
                                            </div>
                                        </div>


                                        <div class="control-group submit2">
                                            <div class="controls">
                                                <input type="submit" class="submit exclusive standard-checkout"
                                                       value="Submit"/>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>

                        </div>
                    </div>
                    <!-- end div block_home -->
                </section>
            </div>
        </div>
    </section>

@stop