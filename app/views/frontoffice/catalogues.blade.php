@extends('layouts.frontoffice.default')

@section('content')
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<style type="text/css">

    th {
        color: #57575A;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
    }

    .gl-sub-menu2 a:link, .gl-sub-menu2 a:visited {
        color: #57575A;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        text-decoration: none;
    }

    .gl-sub-menu2 a:hover {
        color: #57575A;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        text-decoration: underline;
    }

    .no_border {
        border: none !important;
    }

    a:after{
        width:300px;
        height:400px;
    }

</style>

<script type="text/javascript">
$('document').ready(function(){
//    $('a').click(function(e) {
//           alert('hi');
//           e.stopPropagation();
//           var url = $(this).attr('href');
//           myWindow = window.open(url, this.target, "width=600,scrollbars=yes");
////           myWindow.resizeTo(250, 250);
//           myWindow.focus();
//
////           window.resizeTo(300,300);
////           window.focus();
//        });
});

</script>

<div class="container">
<div class="row">
    <div class="span12">
        <h1>Catalogues</h1>
    </div>
</div>

<br/><br/>

<div class="row justify">

<div class="span12">
<table class="table" align="center" border="0" cellspacing="0" cellpadding="0">
<tbody>


<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;" >
            <tbody>
            <tr>
                <th align="left" colspan="8">Catalogues</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                href="{{asset('/uploads/product/documents/GLen LA Catalogue_2014.pdf')}}" target="_blank"
                >Large Appliances</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/documents/Glen Small Appliances Catalogue - 2013.pdf')}}" target="_blank">Small Appliances</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">Folder</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/documents/Small Appliances.pdf')}}"
                        target="_blank">Small Appliances</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/documents/Hob Folder.pdf')}}" target="_blank">Hob</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/documents/Heater Folder.pdf')}}" target="_blank">Heater</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">Leaflets</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/documents/Cooktop.pdf')}}" target="_blank">Cooktop</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/documents/Chimney leaflet.pdf')}}" target="_blank">Chimney</a>
                </td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/documents/Cooking Range leaflet.pdf')}}" target="_blank">Cooking Range</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

</tbody>
</table>
</div>

</div>

<br/><br/>

</div>


@stop