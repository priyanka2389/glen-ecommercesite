@extends('layouts.frontoffice.default')

@section('content')

<style type="text/css">
  table td, table td strong{
      line-height: 26px;
      font-size: 14px;
  }
</style>
<div class="container">
    <div class="row">
        <div class="span12">
            <h1>Corporate</h1>
        </div>
    </div>

    <br/><br/>



    <div class="row justify">
        <div class="span12">

            <table cellspacing="10" cellpadding="0" width="100%">
                <tbody>
                <tr>
                    <td><strong>Company Name</strong></td>
                    <td><strong>Glen Appliances Pvt. Ltd.</strong></td>
                </tr>
                <tr>
                    <td><strong>Founded</strong></td>
                    <td>1999</td>
                </tr>
                <tr>
                    <td><strong>Headquarters</strong></td>
                    <td>I-34, DLF Industrial Area, Phase I, Faridabad(Haryana)</td>
                </tr>
                <tr>
                    <td><strong>Major Products</strong></td>
                    <td>Chimneys, Cooktops, Cooking Range, Built-in-Hob and Small Appliances</td>
                </tr>
                </tbody>
            </table>
            <br/><br/>
            <h3>About Us</h3>

            <p>
                Glen is an inspiration from the needs of the discerning buyers of modern home appliances. Acknowledging the fact
                that quality services are the only two parameters for acceptance of a brand in the market, Glen is the answer to
                market dynamics.
            </p>
            <p>
                At Glen the key operational words are professional excellence and integrity. In essence the approach of designing
                and developing products is primarily consumer led and is backed by the unflinching support of continuous Research
                and development. Our products are an ideal combination of aesthetics and performance. Guaranteed for long and
                trouble-free usage, Glen comes across as a household's delight.
            </p>

            <h3>Brand Mission</h3>

            <p>"To be most sought  after brand of home appliances that offers technologically superior world class products &
                service, and to build a long term relationship with customers and business associates for stronger standing in the
                market."</p>

            <br/><br/>
        </div>
    </div>


</div>


@stop