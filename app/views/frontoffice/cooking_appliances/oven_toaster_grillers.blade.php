@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">

</style>

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">

                        <img src="{{asset('frontoffice/img/oven_toaster_grillers/otg.jpg')}}">
                    </div>
                    <div class="span7">
<!--                        margin-top80-->
                        <h1 class="top-header-heading ">Oven toaster grillers</h1>

                        <p class="top-header-p ">Glen India brings you Oven Toaster Grill, the kitchen appliances, which help you to cook tastier
			 and healthier. OTG comes with smart design and versatile functions. As the name suggests it can be used for baking, grilling
			 and roasting. A motorized rotisserie lets you cook delicious grilled delicacies hands free. OTG can also be used to keep food 
			warm and for thawing frozen food items.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/oven_toaster_grillers/Back-Convection-Oven.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Back Convection Oven</h5></div>
                    <div class="row-fluid">
			<p class="feature-description"> Glen Oven Toaster Griller bears India's first-ever full back convection oven. The convection
			 fan is placed at the back and throws the air through 4 corners to evenly distribute the heat throughout the oven cavity. It helps
			 in uniform baking/grilling. The food is cooked thoroughly at lower temperatures and hence is healthier than other methods.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/oven_toaster_grillers/Motorised-Rotisserie.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Motorised Rotisserie</h5></div>
                    <div class="row-fluid">
 			<p class="feature-description"> Rotisserie helps you to grill things with no hassles of manually turning the food
			 again and again as the same is done automatically by the motor.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/oven_toaster_grillers/Turbo-Convection.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Turbo convection</h5></div>
                    <div class="row-fluid">
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/oven_toaster_grillers/Self-Cleaning-Function.PNG')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Self Cleaning Function</h5></div>
                    <div class="row-fluid">
			<p class="feature-description"> Glen's 60 Litre OTG comes with a self clean coating in the cavity. It cleans all the residues
			 and the splashed food automatically, thus saving you from cleaning the cavity after cooking.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>
            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop