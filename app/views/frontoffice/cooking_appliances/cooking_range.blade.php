@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<?php $session_ids = Session::get('id'); ?>
<style type="text/css">
    /*.nav-tabs > .active > a, .nav-tabs > .active > a:hover, .nav-tabs > .active > a:focus{line-height: 9px;}*/
    @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/cooking-ranges/indian-cooking-range1.png')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Cooking ranges</h1>

                        <p class="top-header-p ">Glen’s Classic range of cookers combine an exceptionally solid and
                            strong structure with
                            superb all-round performance. They offer a variety of features, such as the high energy
                            triple ring burner for
                            faster cooking, a motorised rotisserie for perfect grilling and a convenient multi-spark
                            auto ignition. There is
                            a light in the oven for easy viewing and a minute-minder to keep a tab on the time.
                            Experience the pleasure of
                            cooking with the sheer form, function and capabilities of the Glen Cooking Range.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/cooking-ranges/Large-Capacity-Oven.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Large capacity oven</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Conventional brass burner, sturdy and strong, fuel efficient. Easy to clean and maintain,
                            last long.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/cooking-ranges/International-Height.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>International Height</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">A large oven for a hassle free preparation of almost any meal, no matter how big.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/cooking-ranges/Motorised-Rotisserie.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Motorised Rotisseries</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">It helps in even distribution of the heat in the cooking range for better baking results. The
                            hot air seals the outer surface of the food being grilled to make it more juicy.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/cooking-ranges/Minute-Minder.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Minute Minder</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The minute minder signals the end of cooking for each dish, as the set time elapses, and
                            keeps you free for other chores. A maximum period of 60 minutes covers most possible
                            dishes.</p>
                    </div>
                </div>

            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop