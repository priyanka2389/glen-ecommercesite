@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<?php $session_ids = Session::get('id'); ?>
<style type="text/css">
     @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/cooking-appliances/glass-cooktops/header-banner.jpg')}}"
                             class="margin-top-20px">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top40">Glass Cooktops</h1>

                        <p class="top-header-p ">Cooking top is an irreplaceable kitchen appliance. LPG has been used since decades in the Indian
 kitchen. Glen India brings you designer cooktops, which let you use LPG with maximum efficiency. Equipped with toughened glass, these high quality
 cooktops are a culinary and visual delight.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">

            <div class="">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/cooking-appliances/glass-cooktops/Matt-steel-and-cool-black.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Rich matt steel and tempered black glass</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">8 mm thick toughened glass top, a matt steel frame offers a freedom never experienced before.
 The glass cooktop is easy to clean and resistant to scratches, stains and heat. The Stainless Steel frame provides a rust proof experience for a 
longer life.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/cooking-appliances/glass-cooktops/Alloy-burners.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Alloy burners</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Pressure die-cast, ultra light burners increase the longevity of the cooktop.
 The holes in the burner rim are designed at a special angle for higher thermal efficiency. Moreover the burners come with five-year warranty.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/cooking-appliances/glass-cooktops/Freedom-of-Space.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Freedom of Space</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">To ensure liberty in hand movements while cooking, the glass cooktop is designed
 to be extra spacious. With its ability to accommodate different sized vessels and pans, it is the cooktop with widest space between burners.
 The burners are integrated to offer the most convenient burner access.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/cooking-appliances/glass-cooktops/Auto-Ignition-option.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Auto Ignition option</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Glen cooktops come with multi-spark battery operated auto-ignition option, so 
that you don't have to search for a lighter every time you cook.</p>
                    </div>
                </div>

            </div>

        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop