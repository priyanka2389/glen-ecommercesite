@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/cooking-appliances/stainless-steel-cooktops/stainless-steel.jpg')}}">
                    </div>
                    <div class="span7">
                        <!--                        margin-top80-->
                        <h1 class="top-header-heading ">Stainless Steel Cooktops</h1>

                        <p class="top-header-p ">Glen India offers you unmatchable freedom when it comes to choosing a cooking appliance.
                            Glen cooktops come with an array of models. Designed for enhanced access and space economy; the stainless steel
                            cooktops come with two, three, four or five cooking zones. You can also pick the number of burners according to your
                            needs- triple ring, high flame, sealed, aluminum alloy or conventional. Also, there are options for sizes: 60, 70 or 90 cm,
                            so that you get the designer stainless steel cooktop which fits best for your counter space. Whether you want a traditional
                            square shape or modern round, stainless steel or matt steel finish; you get what you prefer.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/cooking-appliances/glass-cooktops/Alloy-burners.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Alloy burners</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Pressure die-cast, ultra light burners increase the longevity of the cooktop. The holes in the burner
                            rim are designed at a special angle for higher thermal efficiency. Moreover the burners come with five-year warranty.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/cooking-appliances/stainless-steel-cooktops/Brass-Burner.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Brass Burner</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The ISI marked Gas Stoves are fitted with Brass Burners. The brass burners can take higher
                            temperatures and has been used traditionally in the Gas Stoves.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/cooking-appliances/glass-cooktops/Auto-Ignition-option.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Auto ignition Option</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Glen cooktops come with multi-spark battery operated auto-ignition option, so that you don’t
                            have to search for a lighter every time you cook.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/built-in-series/Glass-hob/Triple-ring-burner.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Triple Ring Burner</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Triple Ring Burner gives three rings of flames and is used whenever the requirement of
                            high energy is required in a short span, like frying, saute etc.</p>
                    </div>
                </div>

            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop