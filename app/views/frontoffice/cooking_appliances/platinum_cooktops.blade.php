@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
    .span3.chimney-category-features-content{margin-left: 15px;}
    /*.nav-tabs > .active > a, .nav-tabs > .active > a:hover, .nav-tabs > .active > a:focus{line-height: 9px;}*/
    @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/cooking-appliances/platinum-cooktops/header-banner.jpg')}}"
                             class="margin-top-20px">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top40">Platinum Cooktops</h1>

                        <p class="top-header-p ">Glen India brings you the latest cooktop made up of international quality brush-finish stainless steel
			 deep drawn into sleek and compact shapes. Now you don�t have to see the same old shiny steel. Platinum series cooktops are premium
			 cooktops, which come with plush look matt finish steel. Unibody steel makes imparts a sleek and streamlined look besides making
			 cooktop stronger and sturdier.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">

            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/cooking-appliances/glass-cooktops/Alloy-burners.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Alloy burners</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Pressure die-cast, ultra light burners increase the longevity of the cooktop. The holes in the burner
			 rim are designed at a special angle for higher thermal efficiency. Moreover the burners come with five-year warranty.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/cooking-appliances/platinum-cooktops/Ultra-Spacious.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Ultra Spacious</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The platinum cooktops come with ergonomically designed burner access so that there is ample 
			space for vessels of all sizes.</p>
                    </div>
                </div>

                <!--                <div class="span3 chimney-category-features-content">-->
                <!---->
                <!--                    <div class="row-fluid">-->
                <!--                        <img class="span12 padding5"-->
                <!--                             src="{{asset('frontoffice/img/cooking-appliances/platinum-cooktops/Ultra-Slim.jpg')}}">-->
                <!--                    </div>-->
                <!--                    <div class="row-fluid">-->
                <!--                        <h5>Ultra Slim</h5></div>-->
                <!--                    <div class="row-fluid">-->
                <!--                        <p>The slimmest cooktop in India (just 4 cms). The looks of the future.</p>-->
                <!--                    </div>-->
                <!--                </div>-->

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/cooking-appliances/glass-cooktops/Auto-Ignition-option.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Auto Ignition option</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Glen India gas cooktops come with multi-spark battery operated auto-ignition option, so
			 that you don�t have to search for a lighter every time you cook.</p>
                    </div>
                </div>


                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/Glass-hob/Triple-ring-burner.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Triple Ring Burner</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The triple ring burner offers high energy in a short time, so in any form of cooking where
		 high flames are required instantly this is very useful. It is especially suitable for deep frying and tempering.</p>
                    </div>
                </div>

            </div>

        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop