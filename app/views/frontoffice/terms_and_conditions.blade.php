@extends('layouts.frontoffice.default')

@section('content')

<div class="container">
    <div class="row">
        <div class="span12">
            <h1>Terms & Conditions</h1>
        </div>
    </div>

    <br/><br/>

    <div class="row justify">
        <div class="span12">

            <h3>User Agreement</h3>

            <p>
                These Terms of Use govern your use of the websites, content and community services offered through
                <a href="http://glenindia.com" target="_blank">Glen India</a>. You (the user) agree
                to access "the site", subject to the terms and conditions of use as set out here.
                Glen may add to or change or update these Terms of Use, from time to time entirely at its own
                discretion. You are responsible for checking these Terms of
                Use periodically to remain in compliance with these terms. Your use of a Site after any amendment to the
                Terms of Use shall constitute your acceptance of
                these terms and you also agree to be bound by any such changes/revisions.
            </p>


            <h3>Changes</h3>

            <p>
                Glen reserves the right to suspend / cancel, or discontinue any or all channels, products or service at
                any time without notice , make modifications and
                alterations in any or all of the content, products and services contained on the site without prior
                notice. Such changes will be posted to (Link where we
                post all changes) for your reference and convenience so as to enable you to understand your
                responsibility as a user.
            </p>

            <h3>Charges</h3>

            <p>Glen reserves the right to charge subscription and / or membership fees from a user, by giving reasonable
                prior notice, in respect of any product, service or
                any other aspect of this Site.</p>

            <h3>Copyright Notice of Trademark</h3>

            <p>Copyright and other relevant intellectual property rights exists on all text relating to the Company’s
                services and the full content of this website. This
                Company’s logo is a registered trademark of this Company in the India and other countries. The brand
                names and specific services of this Company
                featured on this web site are trademarked.</p>

            <h3>Links from this website</h3>

            <p>We do not monitor or review the content of other party’s websites which are linked to from this website.
                Opinions expressed/material appearing on such
                websites are not necessarily shared or endorsed by us and should not be regarded as the publisher of
                such opinions or material. Please be aware that we
                are not responsible for the privacy practices, or content, of these sites.</p>

            <h3>Communication</h3>

            <p>We have several different e‐mail addresses for different queries. These, & other contact information, can
                be found on our Contact Us link on our website
                or via Company literature or via the Company’s stated telephone, facsimile or mobile telephone
                numbers.</p>

            <h3>Registration</h3>

            <p>This company is registered in India with registration no. 036809 dated 06.01.2000. And registered address
                is Glen Appliances (P) Ltd. I‐34, DLF Industrial
                Area, Phase‐I, Faridabad, Haryana Pin Code ‐ 121003</p>

            <h3>Privacy Statement</h3>

            <p>We are committed to protecting your privacy. Authorized employees within the company on a need to know
                basis only use any information collected from
                individual customers. We constantly review our systems and data to ensure the best possible service to
                our customers. Parliament has created specific
                offences for unauthorized actions against computer systems and data. We will investigate any such
                actions with a view to prosecuting and/or taking civil
                proceedings to recover damages against those responsible.</p>

            <h3>Confidentiality</h3>

            <p>We are registered under the Data Protection Act 1998 and as such, any information concerning the Client
                and their respective Client Records may be
                passed to third parties. However, Client records are regarded as confidential and therefore will not be
                divulged to any third party, other than [our
                manufacturer/supplier(s) and] if legally required to do so to the appropriate authorities. Clients have
                the right to request sight of, and copies of any and all
                Client Records we keep, on the proviso that we are given reasonable notice of such a request. Clients
                are requested to retain copies of any literature
                issued in relation to the provision of our services. Where appropriate, we shall issue Client’s with
                appropriate written information, handouts or copies of
                records as part of an agreed contract, for the benefit of both parties. <br/>
                <b>We will not sell, share, or rent your personal information to any third party or use your e‐mail
                    address for unsolicited mail. Any emails sent by this
                    Company will only be in connection with the provision of agreed services and products.</b>
            </p>


            <h3>General</h3>

            <p>The laws of India govern these terms and conditions. By accessing this website [and using our
                services/buying our products] you consent to these terms
                and conditions and to the exclusive jurisdiction of the Indian courts in all disputes arising out of
                such access. If any of these terms are deemed invalid or
                unenforceable for any reason (including, but not limited to the exclusions and limitations set out
                above), then the invalid or unenforceable provision will
                be severed from these terms and the remaining terms will continue to apply. Failure of the Company to
                enforce any of the provisions set out in these
                Terms and Conditions and any Agreement, or failure to exercise any option to terminate, shall not be
                construed as waiver of such provisions and shall not
                affect the validity of these Terms and Conditions or of any Agreement or any part thereof, or the right
                thereafter to enforce each and every provision.
                These Terms and Conditions shall not be amended, modified, varied or supplemented except in writing and
                signed by duly authorized representatives of
                the Company.</p>

            <h3>Indian Law</h3>

            <p>The Agreement shall be governed by the Laws of India. The Courts of law at Faridabad shall have exclusive
                jurisdiction over any disputes arising under this
                agreement.</p>

            <h3>Termination of Account</h3>

            <p>Glen reserves its right to refuse service, restrict, suspend, terminate your account; (Terminate this
                Agreement; Terminate or suspend your access to the
                Glen’s Web Sites; Refuse, move or remove for any reason any Content / Image that you submit on or
                through the Services; Refuse, move, or remove any
                Content / Image that is available on or through the Services; Deactivate or delete your accounts and all
                related information and files in your account;
                Establish general practices and limits concerning use of the Services) at any time and, remove or edit
                contents or cancel orders (entered by you) in its sole
                discretion with or without cause, and with or without any prior notice for any violation of the Terms of
                Use. Upon such termination or suspension, your
                right to use the Glen’s Web Sites will immediately cease. <br/>
                You can also terminate your account at any time but your information may remain stored in archive on our
                servers even after the deletion or the
                termination of your account.
            </p>

            <h3>No unlawful or prohibited use</h3>

            <p>As a condition of your use of the Services, you will not use the Services for any purpose that is
                unlawful or prohibited by these terms, conditions, and
                notices. You may not use the Services in any manner that could damage, disable, overburden, or impair
                any Glen’s server, or the network(s) connected to
                any Glen server, or interfere with any other party's use and enjoyment of any Services. You may not
                attempt to gain unauthorized access to any Services,
                other accounts, Computer systems or to any of the Services, through hacking, password mining or any
                other means. You may not obtain or attempt to
                obtain any materials or information through any means not intentionally made available through the
                Services.</p>

            <h3>Termination of Agreements and Refunds Policy</h3>

            <p>For full information please go to this link (<a href="{{URL::to('return-policy')}}">{{URL::to('return-policy')}}</a>)</p>


            <h4 style="line-height: 22px">These terms and conditions form part of the Agreement between the Client and
                ourselves. Your accessing
                of this website and/or undertaking of a
                booking or Agreement indicates your understanding, agreement to and acceptance, of the Disclaimer Notice
                and the full Terms and Conditions
                contained herein. Your statutory Consumer Rights are unaffected.</h4>

        </div>
    </div>


</div>


@stop