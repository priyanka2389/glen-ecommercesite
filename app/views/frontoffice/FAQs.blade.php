@extends('layouts.frontoffice.default')

@section('content')
<style type="text/css">
    .shipping_FAQs_div ul li {
        list-style-type: disc;
        margin-left: 25px;
    }

    strong {
        text-decoration: underline;
        font-size: 14px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="span12">
            <h1>FAQs</h1>
        </div>
    </div>

    <br/><br/>

    <div class="row justify">

        <div class="span12 shipping_FAQs_div">

            <h3>Why Should I buy a Glen Chimney?</h3>

            <p><strong>Quality:</strong> Glen uses only the highest quality materials. Glen range hoods are finished
                using a power coat finishing system that is environmentally safe and produces a long lasting, durable
                finish. Glen uses appliance grade stainless steel, not painted metal. </p>

            <p><strong>Easy to use:</strong> Glen has designed the range hoods for easy to use. The range hood
                controls are hidden and designed in a way that makes them easy to clean. Glen exclusive adjustable
                mounting system allows for easy installations, saving your time and money. </p>

            <p><strong>Functionality:</strong> Glen hoods are effective at removing contaminants from your kitchen and
                home. With low service rates and over 15 years experience, Glen hoods will add comfort and piece of mind
                to any home because the hoods are reliable and Glen ventilation technology is proven to be effective.
            </p>

            <p><strong>History:</strong> Since 1999 Glen has been producing high quality kitchen ventilation. Through
                our
                over 15 years of history Glen has always been on the cutting edge of technology. Our experience in
                ventilation helps us to produce the finest premium ventilation products on the market today.</p>

            <p><strong>Safety:</strong> Glen hoods do not have sharp edges. The external and internal edges of the
                body are bent to avoid being cut. The glass is tempered on the glass hood. If the glass was ever broken
                by mistake, it breaks into small pieces to avoid large sharp glass surfaces because the glass is
                toughened.</p>

            <p>Glen hobs do not cause fires. All the wires, housing and components made with flame retardant, low smoke
                ingredients. Thermal Overload protector (TOP), Overheating of motor is now no longer a danger. The
                thermostatic protection mechanism of these chimneys cuts of the supply and prevents motor burnout.</p>

            <p><strong>Design:</strong> There is a Glen range of hood that will look perfect in any kitchen, from
                traditional to contemporary to professional. </p>

            <h3>How does a kitchen hood (Chimney) work?</h3>

            <p>A kitchen hood chimney is fitted right above the cooking appliance. When switched “on” it sucks all the
                smoke, oil,spices etc generated during cooking. All smokes, oils spices etc gets trapped into filters &
                odour/smoke is re-circulated into kitchen. In case of ducting mode they are thrown out of room through
                duct.</p>

            <h3>Is ducting compulsory?</h3>

            <p>No, but it is always advisable as the efficiency and suction capacity of the kitchen hood/chimney
                increase and there is no recurring cost of filters. </p>


            <h3>Who will fit the Chimney?</h3>

            <p>A trained skilled service engineer from the authorised service centre should be call to install the
                chimney.</p>

            <h3>How far from the cooktop should the hood be mounted?</h3>

            <p>Specific standard exist that set the minimum distance from the hood to the cooktop. The distance depends
                on the kind of cook top:65 cm from the electric top 75 cm from the gas top. However, it’s always
                consulting the instruction manual that comes with the product since the manufacture may; following tests
                or safety verification, specify different height applications. Not all houses have the same height of
                ceiling: what Glen solution? All Glen hoods come with telescopic chimneys that are long enough to reach
                to required height.</p>

            <h3>What's the best way to clean the surface of hood?</h3>

            <p>Stainless steel hoods should be washed regularly with a clean cloth, warm water and mild soap or dish
                detergent. Clean in the direction of the polish lines. Rinse well with clear water and wipe dry
                immediately. You may wish to apply light oil used for furniture polishing to emphasize it's bright
                finish. Wiping regularly with mild soap/detergent and warm water should protect painted hood surfaces. Be
                cautious about using "New and Improved" cleaning agents. Your hood is installed over hot cooking
                equipment. </p>

            <h3>How often should clean the filters?</h3>

            <p>The aluminum mesh grease filters should be washed in your dishwasher approximately every month depending
                on the amount of usage. Wash often if your cooking style generates greater grease like frying foods or
                wok cooking. You cannot clean the non-duct carbon filters; they must be replaced.</p>


            <p>Baffle filters, which are used in professional style range hoods products, are designed to operate longer
                in high grease conditions without loss of performance. Generally, these need to be cleaned with soap and
                water whenever the surfaces become unsightly.</p>

        </div>

    </div>

    <br/><br/>

</div>
@stop