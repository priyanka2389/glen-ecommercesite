@extends('layouts.frontoffice.default')

@section('content')

    {{HTML::style('frontoffice/css/blocklayered-15.css')}}

    {{--{{HTML::script('frontoffice/js/cloud-zoom.1.0.2.min.js')}}--}}
    {{HTML::script('frontoffice/js/zoomsl-3.0.min.js')}}
    {{HTML::style('frontoffice/css/cloud-zoom.css')}}

    {{HTML::script('frontoffice/js/jquery/plugins/jquery.idTabs.js')}}
    {{HTML::script('frontoffice/js/cart_modal.js')}}
    {{HTML::script('frontoffice/js/request_demo.js')}}
    {{HTML::script('frontoffice/js/send_to_friend.js')}}

    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">
    <script type="text/javascript"
            src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_overlay.png">
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite.png">
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite@2x.png">
    {{HTML::script('frontoffice/js/accordian.js')}}

    <style type="text/css">
        #pb-left-column h1 {
            font-size: 13px;
        }

        @media print {
            .slide-out-div {
                display: none;
            }
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#myModal,#myModal1').on('hidden', function () {
                $(this).data('modal', null);
            });

            var site_url = $.url();
            var host = site_url.attr('host');

            $("#more_info_block ul").idTabs();

            $('.variant').change(function () {

                $variant_url = $(this).find(':selected').data('url');
                if ($variant_url != 'select') {
                    //console.log($variant_url);
                    window.location.href = $variant_url;
                }

            });

//        trigger q-tip
            $('.question_image_product_block').live('click', function (event) {
                // Bind the qTip within the event handler
                $(this).qtip({
                    overwrite: false, // Make sure the tooltip won't be overridden once created
                    content: {
                        title: {
                            text: 'Description',
                            button: true
                        },
                        text: $(this).find('.tooltip_val')[0].innerHTML
                    },
                    show: {
                        event: event.type, // Use the same show event as the one that triggered the event handler
                        ready: true,// Show the tooltip as soon as it's bound, vital so it shows up the first time you hover!
                        solo: true
                    },
                    hide: {event: false},
                    position: {
                        at: 'bottom left',
                        my: 'top left',
                        adjust: {
                            method: 'flip'
                        },
                        viewport: true
                    }, style: {
                        width: 400, // Overrides width set by CSS (but no max-width!)
                        classes: 'qtip-light',
                        overflow: 'scroll'
                    }

                }, event); // Pass through our original event to qTip
            });

            $('#layered_form span.layered_close a').click(function (e) {
                if ($(this).html() == '&lt;') {
                    $('#' + $(this).attr('rel')).show();
                    $(this).html('v');
                    $(this).parent().removeClass('closed');
                }
                else {
                    $('#' + $(this).attr('rel')).hide();
                    $(this).html('&lt;');
                    $(this).parent().addClass('closed');
                }

                e.preventDefault();
            });

            $('#wrap').css('z-index', '1');

            var window_width = window.innerWidth;
            if (window_width == 768) {
                var width = 300;
                var height = 300;
                var is_innerzoom = false;
            } else if (window_width == 640) {
                var width = 230;
                var height = 230;
                var is_innerzoom = false;
            } else if (window_width == 480 || window_width == 320) {
                var is_innerzoom = true;
            } else {
                var width = 400;
                var height = 400;
                var is_innerzoom = false;
            }
            $(".container").imagezoomsl({
//          descarea: ".zoomarea",
                zoomrange: [1, 10],
//          zoomstart: 1.68,
                cursorshadeborder: "3px solid black",
                magnifiereffectanimate: "fadeIn",
                magnifiersize: [width, height],
                statusdivfont: "bold 13px Open Sans",
                statusdivbackground: "#EFEFEF",
                innerzoom: is_innerzoom
            });
        });
    </script>

    @include('_partials.frontoffice.compare_bar')

    <?php
    $combo_name = $combo->name;
    $combo_id = $combo->id;
    $description = $combo->description;
    $price = $combo->combo_price;
    ?>

    <?php $session_ids = Session::get('id'); ?>

    <section id="columns" class="clearfix">
        <div class="container">
            <div class="row-fluid">


                <!--left categories accordian starts here-->
                @include('_partials.frontoffice.left_categories_accordian')
                        <!--left categories accordian ends here-->


                <section id="center_column" class="span9">
                    <div class="contenttop row-fluid">


                        <!-- Breadcrumbs -->
                        <div id="breadcrumb">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="{{URL::to('/')}}" title="return to Home">Home</a>
                                    <span class="divider">&gt;</span>
                                </li>
                                <li class="active">Combos
{{--                                    <a href="{{URL::to('/combo-offers')}}" title="">Combos</a>--}}
                                    <span class="navigation-pipe">&gt;</span>
                                    {{$combo_name or ''}}
                                </li>
                            </ul>
                        </div>
                        <!-- /Breadcrumb -->

                        <div id="product-detail">
                            <div id="primary_block" class="row-fluid">


                                <!-- right infos-->
                                <div id="pb-right-column" class="span5">
                                    <div class="images-block">

                                        <div id="image-block">

                                            <!--  product primary image  -->
                                            <?php $images = $combo->images; ?>
                                            <?php $image = HtmlUtil::getPrimaryImage($images); ?>
                                            <?php $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE;
                                            $zoom_image_path = str_replace('org', '600', $path);
                                            ?>


                                            <span id="view_full_size">
              {{--<a href="{{asset($zoom_image_path)}}" class="cloud-zoom"  id='zoom1' rel="position:'right',adjustX:10, adjustY:0" >--}}
                                                {{--<img src="{{asset($path)}}" title="{{$combo_name}}" >--}}
                                                {{--</a>--}}
                                                <img class="container" src="{{asset($path)}}"
                                                     data-help="Use the wheel Mouse Zoom +/- "
                                                     data-large="{{asset($zoom_image_path)}}"
                                                     data-title="{{$combo_name}}" alt="{{$combo_name}}" width="250"/>
			</span>

                                        </div>
                                        <!-- thumbnails -->
                                        <div id="views_block" class="clearfix ">
                                            <div class="content_prices clearfix">
                                                <!-- prices -->
                                                <p class="online_only font-17px">Price &nbsp;</p>

                                                <div class="price">
                                                    <p class="our_price_display font-17px">


                        <span style="padding-left:5px; color:#333;">
                            <span class="WebRupee"> Rs.</span>{{$price}}</span>

                                                    </p>

                                                </div>


                                                <div class="clear"></div>


                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- left infos-->
                                <div id="pb-left-column" class="span7">

                                    <h1>

                                        {{$combo_name or ''}}

                                        @if(isset($session_ids) && sizeof($session_ids))
                                            <?php $key = array_search($combo_id, $session_ids);
                                            $disabled = is_int($key) ? "disabled" : "";?>
                                        @else
                                            <?php $disabled = "" ?>
                                        @endif


                                        <div style="float:right; margin-right:10px; font-size:13px; font-weight:700">

                                            <!--            <button style="font-family: sans-serif;font-size: 14px" class="dark_button add_to_compare"-->
                                            <!--                    id="compare_{{$combo_id}}" data-id="{{$combo_id}}"-->
                                            <!--            {{$disabled}}>Add to compare</button>-->
                                        </div>

                                    </h1>

                                    <!-- usefull links-->
                                    <ul id="usefull_link_block">

                                        <li id="">
                                            <div class="fb-like" data-href="" data-layout="button"
                                                 data-action="like" data-show-faces="true" data-share="true">
                                            </div>
                                        </li>

                                        <li class="sendtofriend">
                                            <a id="send_friend_button" href="#" data-toggle="modal"
                                               data-target="#myModal1">Send to a friend</a>
                                        </li>

                                        <li class="print"><a href="javascript:print();">Print</a></li>
                                    </ul>
                                    <!-- end usefull links-->


                                    <!-- quick overview starts here -->
                                    <div id="short_description_block">
                                        <h3>Quick Overview</h3>

                                        <div id="short_description_content" class="rte align_justify">
                                            <ul class="product-features">
                                                {{$description or 'No description available'}}
                                            </ul>
                                        </div>
                                        <br/>

                                        <div class="span12">
                                            <a href="{{URL::to('/retail-outlets')}}" class="submit exclusive">Find a
                                                local dealer </a>
                                            <a href="{{URL::to('/request-a-call?source='.$combo->name)}}" class="submit exclusive">Request a
                                                call back</a>
                                        </div>
                                        <br/>
                                    </div>
                                    <!-- quick overview ends here -->

                                    <div id="product_custom_block">
                                        <div class="row-fluid">


                                            <div class="span12">
                                                <ul class="buy-view-list">


                                                    <li class="question_image_product_block" data-hasqtip="0"
                                                        aria-describedby="qtip-0">
                                                        <strong> Free Shipping</strong>
                                                        <img src="{{asset('frontoffice/img/question_icon.gif')}}"
                                                             style="margin:0px 5px 0px 0px">

                                                        <!--                        free shipping qtip content starts here-->
                                                        <div class="tooltip_val" style="display:none;"><p>
                                                                <strong>How is the delivery time determined?</strong>
                                                            </p>

                                                            <p>The delivery time tells the approximate time band, in
                                                                number of days, that we will take
                                                                to deliver the product to you.</p>

                                                            <p>Business days exclude Saturdays, Sundays and any public
                                                                holidays.</p>

                                                            <p>We normally deliver our TabTop PCs by air across India
                                                                wherever possible. The bigger
                                                                items like Robots are sent out either via Air or via
                                                                Surface, depending on your
                                                                location.</p>

                                                            <p>Glen works with the best in class carriers like FedEx,
                                                                DHL, Blue Dart, Aramex, DTDC
                                                                and Speed Post.</p>

                                                            <p>All efforts are made by us to inform you of the progress
                                                                by constantly updating the
                                                                status of shipment on our website.</p>

                                                            <p>The delivery times vary sometimes due to distances and
                                                                circumstances beyond our
                                                                control.</p>

                                                            <p style="text-align:right"><a
                                                                        href="{{URL::to('shipping-and-delivery-faqs')}}"
                                                                        target="_blank">Read More..</a></p>
                                                        </div>
                                                        <!--                        free shipping qtip content ends here-->

                                                    </li>

                                                    @if($combo->is_cod == 1)
                                                        <li class="question_image_product_block">
                                                            <strong>Cash on Delievery</strong>
                                                        </li>
                                                    @endif

                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <?php $i = 0; ?>


                                            <!--variants starts here-->

                                    <!-- variants ends here -->


                                    <!-- add to cart form-->
                                    <form id="buy_block" action="" method="post">

                                        <!-- content prices -->
                                        <div class="content_prices clearfix">
                                            <!-- prices -->

                                            <p class="buttons_bottom_block">
                                                <!--                Add to Compare-->
                                                <label style="width: 29%; margin-top: 2px;"></label>
                                            </p>

                                            <div class="product_attributes">

                                                <!-- quantity wanted -->
                                                <p id="quantity_wanted_p">
                                                    <label>Qty:</label>
                                                    <input type="text" data-item-type="combo" data-id="{{$combo_id}}"
                                                           name="qty"
                                                           id="quantity_wanted"
                                                           class="text qty_{{$combo_id}}" value="1"
                                                           size="2">
                                                </p>

                                            </div>


                                            <a class="ajax_add_to_cart_button right" href="#" data-id="{{$combo_id}}"
                                               data-item-type="combo">
                                                Add to cart</a>

                                            <div class="clear"></div>
                                        </div>
                                        <!-- content prices -->

                                        <!-- attributes -->

                                        <!-- end attributes -->


                                    </form>

                                </div>
                            </div>


                            <!-- description and features tabs starts here -->
                            <div id="more_info_block" class="clear">
                                <ul id="more_info_tabs" class="idTabs idTabsShort clearfix">
                                    <li><a class="more_info_tab" href="#idTab1">More info</a></li>
                                    <li><a href="#idTab2" class="idTabHrefShort">Specifications</a></li>


                                </ul>
                                <div id="more_info_sheets" class="sheets align_justify">
                                    <!-- full description -->
                                    <div id="idTab1" class="">
                                        <div class="product-overview-full">
                                            <ul class="product-features">
                                                {{$description or 'No more info for the moment'}}
                                            </ul>
                                        </div>
                                    </div>

                                    <div id="idTab2">
                                        <div id="product_comments_block_tab">
                                            <p class="align_center">
                                                No specifications are available for the moment
                                            </p>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <!-- description and feature tabs end here-->

                        </div>


                        <!-- module Related products starts here -->
                        @include('_partials.frontoffice.related_combos')
                                <!--module related products ends here-->


                    </div>
                    <!-- end div block_home -->
                </section>

            </div>
        </div>
    </section>

    <!--module send to friend starts here-->
    @include('_partials.frontoffice.send_to_friend')
            <!--module send to friend  ends here-->

@stop