@extends('layouts.frontoffice.default')

@section('content')

<script type="text/javascript">
    $(document).ready(function () {


    });
</script>

<style type="text/css">

    .states_list a {
        line-height: 18px;
    }

    .state_row {
        background-color: #e8e8e8;
        padding: 5px;
        text-align: center !important;
        /*border-top:16px solid white;*/
        /*border-bottom:16px solid white;*/
    }

    .hr {
        border-bottom: 1px solid #e8e8e8;
        margin-top: 2px;
        margin-bottom: 2px;
    }

    strong {
        font-family: Arial, Helvetica, sans-serif;
    }

    .no_border_row > td {
        border: none !important;
    }

    .no_border_row > td > a {
        color: #17479d;
    }

    .no_border_row > td > a:hover {
        text-decoration: underline;
    }

    .right {
        float: right !important;
    }

    a[href^='mailto:'] {
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 13px;
        letter-spacing: 0.1px;
        color: #17479d;
    }


</style>

<div class="container">
<div class="row">
    <div class="span12">
        <h1>Distributors</h1>
    </div>
</div>

<br/>

<div class="row justify">

<div class="span12">
<table cellspacing="10" cellpadding="0" width="100%" class="table">
<tbody>
<tr class="no_border_row">
    <td colspan="3" align="center"><a name="begin"></a>
        <table cellspacing="0" cellpadding="0" width="100%">
            <tbody>

                <tr class="states_list no_border_row">
                    <?php
                    $k = 6;
                    $j = 0;
                    ?>
                    @for($i = 0;$i < 4;$i++)
                        <td valign="top">
                            @while($j < $k)
                             @if($j == count($states)) <?php break; ?> @endif
                                <a href="#{{$states[$j]->state }}">{{strtoupper($states[$j]->state )}}</a><br>
                                <?php $j = $j + 1;?>
                            @endwhile
                            <?php
                            $j = $k;
                            $k = $k + 6;
                            ?>
                        </td>
                    @endfor
                </tr>


            </tbody>
        </table>
    </td>
</tr>
<!--<tr>-->
<!--    <td>&nbsp;</td>-->
<!--</tr>-->
<!--<tr></tr>-->
<tr class="no_border_row">
    <td width="20%" align="left" class="no_border"><strong>Location</strong></td>
    <td width="28%" align="left"><strong>Product Category</strong></td>
    <td width="52%" align="left"><strong>Name &amp; Address</strong></td>
</tr>
<!--<tr>-->
<!--    <td>&nbsp;</td>-->
<!--</tr>-->

@foreach($distributors as $key=>$value)
<tr>
    <td class="state_row" colspan="3" align="center"><a name="{{$key}}"><strong>{{strtoupper($key)}}</strong></a></td>
</tr>
@for($i=0;$i<count($value);$i++)
<tr>
    <td align="left"><strong>{{$value[$i]->city}} </strong></td>
    <td align="left">
    @if($value[$i]->is_small_appliance == 1 && $value[$i]->is_large_appliance == 0) Small Appliances
    @elseif($value[$i]->is_small_appliance == 0 && $value[$i]->is_large_appliance == 1) Large Appliances
    @elseif($value[$i]->is_small_appliance == 1 && $value[$i]->is_large_appliance == 1) All Products
    @endif
    </td>
    <td align="left"><strong> {{$value[$i]->name}}</strong><br>
        {{$value[$i]->address1}} @if($value[$i]->pincode)– {{$value[$i]->pincode}} @endif<br>
        @if($value[$i]->phone)Ph : {{$value[$i]->phone}} <br>@endif
        @if($value[$i]->mobile)Mob: {{$value[$i]->mobile}} <br>@endif
        @if($value[$i]->email_id)EmailId: {{$value[$i]->email_id}} <br>@endif
        @if($value[$i]->contact_person)Contact Person: {{$value[$i]->contact_person}} <br>@endif
    </td>
</tr>
@endfor
@endforeach

</tbody>
</table>
</div>

</div>

<br/><br/>

</div>


@stop
