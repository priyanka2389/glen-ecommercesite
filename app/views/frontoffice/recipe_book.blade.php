@extends('layouts.frontoffice.default')

@section('content')
    <script type="text/javascript">
        $(document).ready(function () {

//            $(".fancybox")
////            .attr('rel', 'gallery')
//                    .fancybox({
//                        openEffect: 'none',
//                        closeEffect: 'none',
//                        nextEffect: 'none',
//                        prevEffect: 'none',
//                        padding: 0,
//                        margin: [20, 60, 20, 60] // Increase left/right margin
//                    });
        });
    </script>
    <style type="text/css">
        h4 {
            color: #17479d;
        }
    </style>

    <div class="container product-demos">
        <div class="row">
            <div class="span12">
                <h1>Cook Book</h1>
            </div>
        </div>

        <br/><br/>

        <div class="row-fluid">

            <div class="span12">
                <div class="span4 product_detail_box padding5">
                    <div class="row-fluid">
                        <a target="_blank" href="{{asset('/uploads/glen_cook_book/air_fryer.pdf')}}">
                            <img src="{{asset('/frontoffice/img/cook-book/airfryer.jpg')}}" width="300" height="232">
                        </a>
                    </div>
                </div>

                <div class="span4 product_detail_box padding5">
                    <div class="row-fluid">
                        <a target="_blank"
                           href="{{asset('/uploads/glen_cook_book/bread_maker.pdf')}}">
                            <img src="{{asset('/frontoffice/img/cook-book/bread-maker.jpg')}}">
                        </a>
                    </div>
                    {{--<div class="row-fluid">--}}
                    {{--<br/>--}}
                    {{--<h4>Bread Maker Recipe Book</h4>--}}
                    {{--</div>--}}
                </div>

                <div class="span4 product_detail_box padding5">
                    <div class="row-fluid">
                        <a target="_blank"
                           href="{{asset('/uploads/glen_cook_book/food_preparation.pdf')}}">
                            <img src="{{asset('/frontoffice/img/cook-book/food-preparation.jpg')}}">
                        </a>
                    </div>
                    {{--<div class="row-fluid">
                        <br/>
                        <h4>Food Processor Recipe Book</h4>
                    </div>--}}
                </div>

            </div>
        </div>

        <div class="clearfix"></div>
        <br>

        <div class="row-fluid">
            <div class="span12">
                <div class="span4 product_detail_box padding5">
                    <div class="row-fluid">
                        <a target="_blank"
                           href="{{asset('/uploads/glen_cook_book/glass_grill.pdf')}}">
                            <img src="{{asset('/frontoffice/img/cook-book/glass-grill.jpg')}}">
                        </a>
                    </div>
                    {{--<div class="row-fluid">--}}
                    {{--<br/>--}}
                    {{--<h4>Glass Grill Recipe Book</h4>--}}
                    {{--</div>--}}
                </div>

                <div class="span4 product_detail_box padding5">
                    <div class="row-fluid">
                        <a target="_blank"
                           href="{{asset('/uploads/glen_cook_book/OTG.pdf')}}">
                            <img src="{{asset('/frontoffice/img/cook-book/otg.jpg')}}">
                        </a>
                    </div>
                    {{--<div class="row-fluid">--}}
                        {{--<br/>--}}
                        {{--<h4>Oven Toaster Griller Recipe Book</h4>--}}
                    {{--</div>--}}
                </div>

                <div class="span4 product_detail_box padding5">
                    <div class="row-fluid">
                        <a target="_blank"
                           href="{{asset('/uploads/glen_cook_book/rice_cooker.pdf')}}">
                            <img src="{{asset('/frontoffice/img/cook-book/rice-cooker.jpg')}}">
                        </a>
                    </div>
                    {{--<div class="row-fluid">--}}
                    {{--<br/>--}}
                    {{--<h4>Rice Cooker Recipe Book</h4>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <br>

        <div class="row-fluid">
            <div class="span12">
                <div class="span4 product_detail_box padding5">
                    <div class="row-fluid">
                        <a href="{{asset('/uploads/glen_cook_book/Steam Cooker.pdf')}}">
                            <img src="{{asset('/frontoffice/img/cook-book/steam-cooker.jpg')}}">
                        </a>
                    </div>
                    {{--<div class="row-fluid">--}}
                    {{--<br/>--}}
                    {{--<h4>Steam Cooker Recipe Book</h4>--}}
                    {{--</div>--}}
                </div>

                <div class="span4 product_detail_box padding5">
                    <div class="row-fluid">
                        <a target="_blank"
                           href="{{asset('/uploads/glen_cook_book/Tandoor.pdf')}}">
                            <img src="{{asset('/frontoffice/img/cook-book/tandoor.jpg')}}">
                        </a>
                    </div>
                    {{--<div class="row-fluid">--}}
                    {{--<br/>--}}
                    {{--<h4>Tandoor Recipe Book</h4>--}}
                    {{--</div>--}}
                </div>

            </div>
        </div>

        {{--</div>--}}
    </div>


    <br/>

@stop

