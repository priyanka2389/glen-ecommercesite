@extends('layouts.frontoffice.default')

@section('content')
<script type="text/javascript">
    $(document).ready(function () {

        $(".fancybox")
//            .attr('rel', 'gallery')
            .fancybox({
                openEffect  : 'none',
                closeEffect : 'none',
                nextEffect  : 'none',
                prevEffect  : 'none',
                padding     : 0,
                margin      : [20, 60, 20, 60] // Increase left/right margin
            });
    });
</script>
<style type="text/css">
    h4{color:#17479d;}
</style>

<div class="container product-demos">
<div class="row">
    <div class="span12">
        <h1>Product Demos</h1>
    </div>
</div>

<br/><br/>

<div class="row-fluid">

    <div class="span12">
        <div class="span4 product_detail_box padding5">
                <div class="row-fluid" >
                    <a class="fancybox fancybox.iframe"  href="https://www.youtube.com/embed/zlP9_Dio8wo">
                        <img src="{{asset('/uploads/media/product_demos/mqdefault.jpg')}}"  width="300" height="232">
                    </a>
                </div>
                <div class="row-fluid">
                    <br/>
                    <h4>Glen Electric Tandoor GL 5014</h4>
                </div>
        </div>

        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/MXh8UggOx7Q">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Blender_Grinder_GL_4045.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Blender Grinder GL 4045</h4>
            </div>
        </div>

        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/0rrmnxqp-CU">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Mini_Chopper_GL_4043.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Mini Chopper GL 4043</h4>
            </div>
        </div>

    </div>
</div>

<div class="clearfix"></div>
<br>
<div class="row-fluid">

    <div class="span12">
        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/S7hm4PCY7wE">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Glass_Grill_GL_3033.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Glass Grill GL 3033</h4>
            </div>
        </div>

        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/MrhZ6holBIA">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Bread_Maker_GL_3034.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Bread Maker GL 3034</h4>
            </div>
        </div>

        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/kZ6dysMOaJU">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Tea_Maker_and_Kettle_GL_9010.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Tea Maker & Kettle GL 9010</h4>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<br>
<div class="row-fluid">

    <div class="span12">
        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/4ypwDmqR1Fo">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Air_Fryer_GL_3041.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Air Fryer GL 3041</h4>
            </div>
        </div>

        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/O7S4iXTKKXM">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Steam_Cooker.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Steam Cooker</h4>
            </div>
        </div>

        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/5-OxR4Cnph8">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Food_Processor_GL_4051_Eng.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Food Processor GL 4051 Eng</h4>
            </div>
        </div>
    </div>
</div>


<div class="clearfix"></div>
<br>
<div class="row-fluid">

    <div class="span12">
        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/9pbRz4ArtbE">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Food_Processor_GL_4051_Eng.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Food Processor GL 4051 Hindi</h4>
            </div>
        </div>

        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/lYTNqeuCYVA">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Food_Processor_GL_4052_Eng_Part_1.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Food Processor GL 4052 Eng Part 1</h4>
            </div>
        </div>

        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/QBRF63J4Wts">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Food_Processor_GL_4052_Eng_Part_2.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Food Processor GL 4052 Eng Part 2</h4>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<br>
<div class="row-fluid">

    <div class="span12">
        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/2UqZRmTm4bA">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Food_Processor_GL_4052_Hindi_Part_1.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Food Processor GL 4052 Hindi Part 1</h4>
            </div>
        </div>

        <div class="span4 product_detail_box padding5">
            <div class="row-fluid">
                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/NyyLfN3z1BU">
                    <img src="{{asset('/uploads/media/product_demos/Glen_Food_Processor_GL_4052_Hindi_Part_2.jpg')}}">
                </a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4>Glen Food Processor GL 4052 Hindi Part 2</h4>
            </div>
        </div>

    </div>
</div>

{{--</div>--}}
</div>


<br/>

@stop

