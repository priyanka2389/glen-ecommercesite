@extends('layouts.frontoffice.default')

@section('content')

<style type="text/css">

    .privacy_policy_div h3 {
        text-decoration: underline;
    }

    .information_use {
        padding-left: 40px;
    }

    .information_use li {
        list-style: circle !important;
        padding-bottom: 5px;
    }

</style>

<div class="container">
    <div class="row">
        <div class="span12">
            <h1>Privacy Policy</h1>
        </div>
    </div>

    <br/><br/>

    <div class="row justify">

        <div class="span12 privacy_policy_div">

            <h3>Online Privacy Policy Only</h3>

            <p>
                The Privacy Policy provided below is with respect to our use and protection of any personal information
                you provide to us through the
                <a href="http://glenindia.com/" target="_blank">glenindia.com</a> website.
                Glen Appliances Pvt. Ltd. is the sole owner of the website
                <a href="http://glenindia.com/" target="_blank">www.glenindia.com </a>.The Policy is applicable
                to the website.
                <br/>
                You may be required to provide personally identifiable information at several different points on our
                website. By accepting the policy at the
                time of registration, you expressly approve and consent to our collection, storage, use and disclosure
                of your personal information as described
                in this Policy.
            </p>

            <h3>What information do we collect?</h3>

            <p>You can visit/surf our website without providing any personal information. However, if you choose to
                avail of certain services on our website,
                you shall be required to provide certain information for the registration or ordering or access to such
                services.
                <br/>
                Such information may include, without limitation, your name, email address, contact address,
                mobile/telephone number(s), sex, age,
                occupation, interests, financial information, content, IP address, standard web log information and such
                other information as may be required
                for your interaction with the services
            </p>

            <h3>What do we use your information for?</h3>

            <p>
                Any of the information we collect from you may be used in one of the following ways:
            </p>
            <ul class="information_use">
                <li>To personalize your experience : Your information helps us to better respond to your individual
                    needs and also for Improve and customize our services, content and other commercial /non commercial
                    features on the website.
                </li>
                <li>To improve customer service: your information helps us to more effectively respond to our customer
                    service requests and support needs. And also to send you service-related announcements on rare
                    occasions when it is necessary to do so.
                </li>
                <li>To process transactions : Your information, whether public or private, will not be sold, exchanged,
                    transferred, or given to any other company for any reason whatsoever, without your consent, other
                    than for the express purpose of delivering the purchased product or service requested.
                </li>
                <li>To improve our website: Improve our website and enable us to provide you the most user-friendly
                    experience which is safe, smooth and customized.
                </li>
                <li>To administer a contest, promotion, survey or other site feature: Provide you the opportunity to
                    participate in contests or surveys on our website (If you participate, we may request certain
                    additional personally identifiable information from you. Moreover, participation in these surveys or
                    contests is shall be completely voluntary and you therefore shall have a choice whether or not to
                    disclose such additional information);
                </li>
                <li>To send you information: Provide service updates and promotional offers related to our
                    services/products.
                </li>
                <li>The email address you provide may be used to send you information, respond to inquiries, and/or
                    other requests or questions.
                </li>
                <li>Send you marketing/promotional communications (If you do not wish to receive such
                    marketing/promotional communications from us you may indicate your preferences at the time of
                    registration or by following the instructions provided on the website or by providing instructions
                    to this effect);
                </li>
            </ul>

            <h3></h3>

            <p>We implement a variety of security measures to maintain the safety of your personal information when you
                place an order or enter, submit, or
                access your personal information.</p>

            <p>
                We offer the use of a secure server. All supplied sensitive/credit information is transmitted via Secure
                Socket Layer (SSL) technology and then
                encrypted into our Payment gateway providers database only to be accessible by those authorized with
                special access rights to such systems,
                and are required to? Keep the information confidential.
            </p>

            <p>After a transaction, your private information (credit cards, social security numbers, financials, etc.)
                will not be stored on our servers.</p>


            <h3>Do we use cookies?</h3>

            <p>Yes Cookies are small files that a site or its service provider transfers to your computer's hard drive
                through your Web browser (if you allow) that enables the sites or service providers systems to recognize
                your browser and capture and remember certain information. <br/>
                We use cookies to help us remember and process the items in your shopping cart and understand and save
                your preferences for future visits.
            </p>

            <h3>We don’t disclose any information to outside parties?</h3>

            <p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information.
                This does not include trusted third
                parties who assist us in operating our website, conducting our business, or servicing you, so long as
                those parties agree to keep this information
                confidential. We may also release your information when we believe release is appropriate to comply with
                the law, enforce our site policies, or
                protect ours or others rights, property, or safety. However, non‐personally identifiable visitor
                information may be provided to other parties for
                marketing, advertising, or other uses.</p>

            <h3>Children Online Privacy Protection Act Compliance</h3>

            <p>We are in compliance with the requirements of COPPA (Children Online Privacy Protection Act), we do not
                collect any information from
                anyone under 13 years of age. Our website, products and services are all directed to people who are at
                least 13 years old or older.</p>

            <h3>Your Consent</h3>

            <p>You shall be responsible for all the activities happening under your username and you shall be
                responsible for keeping your password secure.
                Do not disclose your password to anyone. If you share your password or your personal information with
                others, you shall be responsible for all
                actions taken under your username and you may lose substantial control over your personal information
                and may be subject to legally binding
                actions taken on your behalf. Therefore, if your password has been compromised for any reason, you
                should immediately change your
                password.</p>

            <h3>Changes to our Privacy Policy</h3>

            <p>If we decide to change our privacy policy, we will post those changes to this privacy statement.</p>


            <h3>Business Transition</h3>

            <p>In the event Glen Appliance Pvt. Ltd. goes through a business transition, such as a merger, acquisition
                by another company, or sale of all or a
                portion of its assets, your personally identifiable information will likely be among the assets
                transferred. Where your information is transferred
                you will be notified via email/prominent notice on our website for 30 days of any such change in
                ownership or control of your personal
                information.</p>

            <h3>Contacts</h3>

            <p>If you have any questions or suggestions regarding our privacy policy, please
                <a href="{{URL::to('/contact')}}"></a> contact us
            </p>

            <h3>Security</h3>

            <p>The security of your personal information is important to us. When you enter your personal information we
                treat the data as an asset that must
                be protected and use tools (encryption, passwords, physical security, etc.) to protect your personal
                information against unauthorized access
                and disclosure. We work hard to protect your information, we do not promise, and you should not expect,
                that your personal information or
                private communications will always remain private.</p>

            <h3>Indian Law</h3>

            <p>The Agreement shall be governed by the Laws of India. The Courts of law at Faridabad shall have exclusive
                jurisdiction over any disputes arising
                under this agreement.</p>

        </div>

    </div>

    <br/><br/>

</div>


@stop