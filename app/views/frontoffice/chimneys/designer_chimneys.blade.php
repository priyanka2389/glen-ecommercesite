@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}
<style type="text/css">
    .margin-right-0px {
        margin-right: -5px !important;
    }
    @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>


<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/chimney-category/designer-hood/designer-chimney-banner.png')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Designer Hoods</h1>

                        <p class="top-header-p ">Glen�s designer chimney adds to the dapper deco of your kitchen. Made with high-end
 glass and matt finish steel, this latest kitchen chimney from Glen accentuates the modern functionality of kitchen space. Shop among
 vast choice of features and design.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">
            <div class="span12">
                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"  src="{{asset('frontoffice/img/chimney-category/designer-hood/LED-lighting.jpg')}}"/>
                    </div>
                    <div class="row-fluid">
                        <h5>LED lighting</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The inbuilt LED lights in designer hood ensure that you enjoy cooking with bright
 illumination. With low power requirement and long life, LED lights help in hygienic cooking making it the best chimney for kitchen in this
 price segment.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/chimney-category/designer-hood/Intelligent-controls.png')}}"/>
                    </div>
                    <div class="row-fluid">
                        <h5>Intelligent Controls</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Soft touch electronic push button controls let you select between three convenient
 exhaust speeds and operate the two powerful LED lights. This push button chimney comes with a smart auto switch off timer that gives you
 ability to serve your family without having to worry about switching off.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/chimney-category/straight-line/Baffle_Filter.png')}}"/>
                    </div>
                    <div class="row-fluid">
                        <h5>Baffle Filter</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The next generation baffle filter has a dynamic airflow system, which works
 with high efficiency and helps reduce power consumption. Baffle filter chimneys are easier to clean at home, ensuring hygienic
 cooking.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/chimney-category/designer-hood/PDCA-Housing.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>PDCA Housing</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">A pressure die cast aluminium (PDCA) housing boosts the durability of the Italian motor
 while minimizing noise generation.</p>
                    </div>
                </div>

            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop