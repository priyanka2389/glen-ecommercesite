@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}



<?php $session_ids = Session::get('id'); ?>
<style type="text/css">
    /*img.span12.padding5{*/
        /*width: 220px;*/
        /*height: 194px;*/
    /*}*/
    @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>
<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img
                            src="{{asset('frontoffice/img/chimney-category/straight-line/straight-line-header.jpg')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Straight - Line Chimneys</h1>

                        <p class="top-header-p ">Straight line traditional chimneys from Glen India are quite powerful in single and twin motor
			 options. With flat top, push buttons and matt finish steel make, they are value for money option to get the smoke and 
			grease out of your kitchen space.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/chimney-category/straight-line/TOP.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>TOP</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The thermal overload protector prevents motor burnout. Chimney housing made with flame-retardant 
			plastics so are quite safe.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/chimney-category/straight-line/BIO-Rotational-Technology.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>BIO Rotational Technology</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The revolutionary bi-rotational technology in the twin motor options, where one motor runs clock-wise
			 and the other anti- clockwise to trap all the fumes and make the kitchen absolutely free of unwanted smoke and smell.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/chimney-category/straight-line/Baffle_Filter.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Baffle Filter</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The next generation baffle filter has a dynamic airflow system, which works with high 
			efficiency and helps reduce power consumption. Stainless steel chimney equipped with baffle filters is easier to clean
			 at home ensuring hygienic cooking.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/chimney-category/straight-line/Intelligent-Controls.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Intelligent controls</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Has options of sliding and push button controls and is fitted with 2 powerful 40 W lamps,
			 so that you can relish smoke free cooking in light.</p>

                    </div>
                </div>
            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop