@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
    @media (min-width: 768px) and (max-width: 979px) {
        .top-header-heading {
            margin-top: 10px;
        }
    }
</style>


<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img
                            src="{{asset('frontoffice/img/chimney-category/designer-hood/designer-chimney-banner.png')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Split Chimneys</h1>

                        <p class="top-header-p ">
                            A famous cook once said, “My favorite way to cook, especially when I cook alone, is in
                            silence. I like the sounds of cooking: the chopping, the sizzling and the thwacking of a
                            wooden spoon against the bowl when I cream butter and sugar.” Our novel concept
                            takes the noise straight out of the kitchen. Introducing Split Chimney, state of art
                            electric
                            kitchen chimney from Glen, with separate outdoor and indoor units so you enjoy the
                            sounds of cooking with lower noise.
                        </p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">

        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/chimney-category/split-chimney/curved-glass.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Curved Glass</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The strength of toughened glass and the graceful curved design
                            lend an elegant look and add a touch of class that’s hard to beat. The split kitchen chimney
                            from Glen doubles as a marvel of design in your kitchen.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/chimney-category/split-chimney/intelligent-control.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Intelligent controls</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">A three speed push button control panel, allows to switch among
                            three exhaustion speeds and two powerful 40 W lamps for perfectly illuminated culinary
                            experience.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/chimney-category/split-chimney/split-chimney.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Split chimney</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">A new concept to reduce the noise from the chimney by taking the
                            motor outdoors, so keep the smoke and grime as well as sound out. This stylish wall mounted
                            kitchen hood, tastefully clad in glass and brush steel. Contemporary styling, sophisticated
                            appearance and compact design offer the best of both worlds - wondrous looks and exceptional
                            performance at a relatively low noise.
                        </p>

                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/chimney-category/island/PDCA-Housing.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>PDCA Housing</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Pressure die cast aluminium (PDCA) housing increases the life of
                            the Italian motor while minimizing noise generation due to less vibrations.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop