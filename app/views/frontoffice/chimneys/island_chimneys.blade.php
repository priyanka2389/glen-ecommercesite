@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
    @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>


<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img
                            src="{{asset('frontoffice/img/chimney-category/island/island-chimney-header.png')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Island Chimneys</h1>

                        <p class="top-header-p ">An exclusive range of Island chimney hoods, designed to enhance functionality of your modular kitchen.
			 With modern European design, glass and brush steel make, the Glen Island chimney will add to aesthetics of your kitchen space.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">

    <!--category sidebar -->
    {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5" src="{{asset('frontoffice/img/chimney-category/island/design.jpg')}}">
                </div>
                <div class="row-fluid">
                    <h5>Design</h5></div>
                <div class="row-fluid">
<!--                    <p>Bold and distinctive. Premium materials are combined to create a statement hood that provides an-->
<!--                        unrivalled blend of performance and looks.</p>-->
                    <p class="feature-description">Ergonomic design and high finish stainless steel make of Glen Island chimney,
			 offer style combined with excellent performance for longer run. The island installation ensures that 
			your kitchen remains smoke free.</p>
                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/chimney-category/island/led-lighting.jpg')}}">
                </div>
                <div class="row-fluid">
                    <h5>LED lighting</h5></div>
                <div class="row-fluid">
                    <p class="feature-description">The inbuilt LED lights in kitchen chimney ensure that you enjoy cooking with bright
		 illumination. With low power requirement and long life, LED lights of electric kitchen chimney ensure hygienic cooking.</p>
                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/chimney-category/island/digital-control.jpg')}}">
                </div>
                <div class="row-fluid">
                    <h5>Digital Control</h5></div>
                <div class="row-fluid">
                    <p class="feature-description">Soft touch electronic push button controls let you select between three
			 convenient exhaust speeds and operate the two powerful LED lights. A clever auto switch off timer makes you
			 free from worries of switching off the chimney.</p>

                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/chimney-category/island/PDCA-Housing.png')}}">
                </div>
                <div class="row-fluid">
                    <h5>PDCA Housing</h5></div>
                <div class="row-fluid">
                    <p class="feature-description">A pressure die cast aluminium (PDCA) housing enhances the durability of Italian motor
			 of electric chimney by resisting dirt and grease deposition. Also, it reduces noise generation significantly.</p>

                </div>
            </div>

        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>
@stop