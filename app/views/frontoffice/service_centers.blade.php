@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}

{{HTML::script('frontoffice/js/storelocator/handlebars-1.0.0.js')}}
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC3V1dEtD8JYeB3AuKv9z1EsDE2nyVDWg8&sensor=false"></script>
<!--<script src="http://maps.google.com/maps/api/js?sensor=false&region=INDIA"></script>-->

{{HTML::script('frontoffice/js/storelocator/jquery.storelocator.js')}}
{{HTML::script('frontoffice/js/service_centers.js')}}

<style type="text/css">

    .find_dealers {
        background-color: #17479d !important;
        padding: 0px 10px;
        border: none;
        font-size: 13px;
        color: #fff;
        line-height: 27px;
    }

    .retail-store-button-red {
        color: white !important;
    }

    #promotetop {
        height: 5px !important;
        margin-bottom: 15px !important;
    }

    #loader,#loader1 {
        background:url({{URL::to('frontoffice/img/loader.gif')}}) no-repeat;
        padding-right: 5px;

    }

    .shadow:hover {
        -moz-box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
        -webkit-box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
    }

    .shadow {
        border: solid 2px #eee;
        transition: all .5s;
        -webkit-transition: all .5s;

    }

    .address_box {
        margin-bottom: 20px;
        padding: 12px;
        text-align: center;
    }

    .scroll_dealers {
        max-height: 453px !important;
        overflow: auto !important;
    }

    .map_height {
        height: 500px;
    }

    .address_box:hover {
        cursor: pointer;
    }

    #results {
        background-color: #17479d;
        text-align: center;
        color: white;
        padding-top: 10px
    }

    #map-canvas img {
        max-width: none
    }

</style>

<!--<pre>--><?php //print_r($categories); ?><!--</pre>-->

<section id="columns" class="clearfix">
    <div class="container">
        <div class="row-fluid">

            <div class="span12">
                <h1>Service Centers</h1>

                <div class="row-fluid">

                    <div class="span12 well center-block"  style="border: 3px solid;background-color: #ddd">

                        <form action="{{URL::to('service-centers/ajax-service-centers')}}" method="post" id="form">
                            <h3 style="text-align: center">Select category and sub-category </h3><br/>


                            <div class="row">

                                <div class="span4 offset1">

                                    <div class="form-group">

                                        <select name="category" id="" class="form-control category">
                                            <option value="">Select Category...</option>
                                            @foreach($categories as $category)
                                                 @if($category->parent_category_id == null)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                 @endif
                                            @endforeach
                                        </select>

                                    </div>


                                </div>
                                {{--style="width:2.333333%;padding: 0"--}}
                                <div class="span1" >
                                    <div class="hidden" id="loader1" style="color: #ccc;margin-top: 8px;">.</div>
                                </div>
                                <div class="span4">

                                    <div class="form-group">

                                        <select name="subcategory" id="" class="form-control subcategory">

                                        </select>

                                    </div>

                                </div>

                            </div>
<!--                        </form>-->


                            <h3 style="text-align: center">Select state and city to locate the service centers near  you.</h3><br/>


                            <div class="row">

                                <div class="span4 offset1">

                                    <div class="form-group">

                                        <select name="state" id="" class="form-control state">
                                            <option value="">Select State...</option>
                                            @foreach($states as $state)
                                            <option value="{{$state->state}}">{{ucfirst(strtolower($state->state))}}</option>
                                            @endforeach
                                        </select>

                                    </div>


                                </div>
                                {{--style="width:2.333333%;padding: 0"--}}
                                <div class="span1" >
                                    <div class="hidden" id="loader" style="color: #ccc;margin-top: 8px;">.</div>
                                </div>
                                <div class="span4">

                                    <div class="form-group">

                                        <select name="city" id="" class="form-control city">

                                        </select>

                                    </div>

                                </div>

                                <div class="span2" style="margin-left:0;">
                                    <input type="submit" value="Find Service Centers" class="find_dealers"/>
                                </div>
                            </div>




<!--                            <div class="row">-->
<!--                                <div class="span3 offset1">-->
<!--                                    <div class="form-group">-->
<!--                                        <div class="checkbox">-->
<!--                                            <label>-->
<!--                                                <input type="checkbox" checked name="large_appliance"> Large-->
<!--                                                Appliance-->
<!--                                            </label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="span3">-->
<!--                                    <div class="form-group">-->
<!--                                        <div class="checkbox">-->
<!--                                            <label>-->
<!--                                                <input type="checkbox" checked name="cookware"> Cookware-->
<!--                                            </label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                            </div>-->
                        </form>


                    </div>

                    <div class="hidden" id="loading">
                        <h3>Loading dealers please wait....</h3>
                    </div>

                </div>

            </div>

        </div>

        <div class="row-fluid default_text">

            <div class="span8 offset2">
                <p class="initialCopy" style="text-align: center">
                    No matter where you are located on the map, there'll be an authorized GLEN INDIA
                    service centers near you. Select your city or the city closest to you and hit the
                    "Find Service Centers" button. <br/> <b>Happy Shopping</b>!!
                </p>
            </div>

            <div class="clearfix"></div>
            <div class="span6 offset3" id="no_product" style="text-align: center"></div>
        </div>

        <div class="row-fluid complete_result_div">


            <div class="span4">
                <h4 class="hidden" id="results">
                    Search Results</h4>
                <br/>

                <div class="scroll_dealers" id="dealers_list"></div>
            </div>

            <div class="span8 map_height" id="map-canvas"></div>

        </div>


    </div>
</section>

@stop
