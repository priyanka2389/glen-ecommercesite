@extends('layouts.frontoffice.default')

@section('content')
<style type="text/css">
    .product_detail_box{
        border: none;
    }
</style>
<div class="container">
    <div class="row">
        <div class="span12">
            <h1>HT Brunch SA Add</h1>
        </div>
    </div>

    <br/><br/>
    <div class="row-fluid">
        <div class="span12">
            <img src="{{asset('/uploads/offerzone/banner_ht.gif')}}" style="width: 100%;">
        </div>
    </div>
    <br/>

    <div class="row-fluid">

        <div class="span12">
            <div class="span6">
                <div class="product_detail_box padding5">
                    <div class="row-fluid">
                        <a href="{{URL::to('glass-grill')}}">
                            <img src="{{asset('/uploads/offerzone/Glass Grill.png')}}" width="460" height="348">
                        </a>
                    </div>

                </div>
            </div>

            <div class="span6">
                <div class="product_detail_box padding5">
                    <div class="row-fluid">
                        <a href="{{URL::to('bread-maker/GL-3034')}}">
                            <img src="{{asset('/uploads/offerzone/Bread Maker.png')}}" width="460" height="348">
                        </a>
                    </div>

                </div>
            </div>


        </div>
    </div>

    <div class="clearfix"></div>
    <br>

    <div class="row-fluid">

        <div class="span12">
            <div class="span6">
                <div class="product_detail_box padding5">
                    <div class="row-fluid" >
                        <a href="{{URL::to('kettles-&-tea-maker/GL-9010')}}">
                            <img src="{{asset('/uploads/offerzone/Kettle.png')}}" width="460" height="348">
                        </a>
                    </div>

                </div>
            </div>

            <div class="span6">
                <div class="product_detail_box padding5">
                    <div class="row-fluid">
                        <a href="{{URL::to('air-fryer/GL-3041')}}">
                            <img src="{{asset('/uploads/offerzone/Air Fryer.png')}}" width="460" height="348">
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
</div>

<br/>
@stop