@extends('layouts.frontoffice.default')

@section('content')
    <section id="columns" class="clearfix">
        <div class="container" id="bme-content">

            <div class="row">
                <div class="span12">
                    <img class="img-responsive" src="{{asset('frontoffice/img/offerzone/ido-banner.png')}}">
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row-fluid">
                <div class="span4">
                    <div class="row-fluid">
                        <a href="{{URL::to('combo/GL-6075-SS-IDO-Offer')}}">
                            <img class="img-responsive"
                                 src="{{asset('frontoffice/img/offerzone/product-1.png')}}">
                        </a>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <a href="{{URL::to('combo/GL-6054-SS-IDO-Offer')}}">
                            <img class="img-responsive"
                                 src="{{asset('frontoffice/img/offerzone/product-2.png')}}">
                        </a>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <a href="{{URL::to('combo/GL-6071-SX-IDO-Offer')}}">
                            <img class="img-responsive"
                                 src="{{asset('frontoffice/img/offerzone/product-3.png')}}">
                        </a>
                    </div>
                </div>
            </div>

            <br/>

            <div class="row-fluid">
                <div class="span4">
                    <div class="row-fluid">
                        <a href="{{URL::to('combo/GL-6071-SSGF-IDO-Offer')}}">
                            <img class="img-responsive"
                                 src="{{asset('frontoffice/img/offerzone/product-4.png')}}">
                        </a>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <a href="{{URL::to('combo/GL-6071-BL-IDO-Offer')}}">
                            <img class="img-responsive"
                                 src="{{asset('frontoffice/img/offerzone/product-5.png')}}">
                        </a>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <a href="{{URL::to('combo/GL-6071-SS-GF-IDO-Offer')}}">
                            <img class="img-responsive"
                                 src="{{asset('frontoffice/img/offerzone/product-6.png')}}">
                        </a>
                    </div>
                </div>
            </div>

            <br/>

            <div class="row-fluid">
                <div class="span4">
                    <div class="row-fluid">
                        <a href="{{URL::to('combo/GL-6071-TS-IDO-Offer')}}">
                            <img class="img-responsive"
                                 src="{{asset('frontoffice/img/offerzone/product-7.png')}}">
                        </a>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <a href="{{URL::to('combo/GL-6062-TS-IDO-Offer')}}">
                            <img class="img-responsive"
                                 src="{{asset('frontoffice/img/offerzone/product-8.png')}}">
                        </a>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <a href="{{URL::to('combo/GL-6077-BL-IDO-Offer')}}">
                            <img class="img-responsive"
                                 src="{{asset('frontoffice/img/offerzone/product-9.png')}}">
                        </a>
                    </div>
                </div>
            </div>
            <br/>

            <div class="row-fluid">
                <div class="span4">
                    <div class="row-fluid">
                        <a href="{{URL::to('combo/GL-6062-SS-IDO-Offer')}}">
                            <img class="img-responsive"
                                 src="{{asset('frontoffice/img/offerzone/product-10.png')}}">
                        </a>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <a href="{{URL::to('combo/GL-1033-GT-XL-Combo-Offer')}}">
                            <img class="img-responsive"
                                 src="{{asset('frontoffice/img/offerzone/product-11.png')}}">
                        </a>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <a href="{{URL::to('combo/GL-1044-GT-XL-Combo-Offer')}}">
                            <img class="img-responsive"
                                 src="{{asset('frontoffice/img/offerzone/product-12.png')}}">
                        </a>
                    </div>
                </div>
            </div>
        </div>

<br>
        <section class="footer-label">
            <div class="container">
                <div class="col-lg-3 footer-text">
                    <a href="{{URL::to('/retail-outlets')}}" class="submit exclusive idobtn"  >Find a local dealer </a>
                    <a href="{{URL::to('/request-a-call?source=idoPage')}}" class="submit exclusive idobtn" >Request a call back</a>
                </div>
            </div>
        </section>

        <br>
    </section>


    <!--ajax loader-->
    <div class="ajax_loader_div hidden">
        <img src="{{asset('frontoffice/img/loader.gif')}}" alt="" width="35" height="35"/>
        Loading
    </div>

    <script type="text/javascript" src="{{asset('frontoffice/js/cart_modal.js')}}"></script>
    {{HTML::style('frontoffice/css/accordian.css')}}
    {{HTML::script('frontoffice/js/accordian.js')}}
    <style type="text/css">
        .add_to_cart_image {
            margin-right: 10px;
            float: right;
        }

        .footer-label {
            background: #FFF;
            margin: 12px 0;
            padding: 30px;
            font-size: 20px;
        }

        .footer-text {
            font-size: 20px;
            color: #000;
            text-align: center;
            line-height: 24px;
        }

        #columns {
            background: url('frontoffice/img/offerzone/background.png') no-repeat;
            margin: -15px 0 0 0;
            padding: 0;
            background-color: #D1DE3E;
        }

        .idobtn{
            font-size: 17px !important;
            padding: 10px !important;
        }

    </style>

@stop