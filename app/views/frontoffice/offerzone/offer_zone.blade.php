@extends('layouts.frontoffice.default')

@section('content')
    <style type="text/css">
        .footer_text {
            background-color: #17479d;
            color: #FFF;
            font-weight: bold;
            text-align: center;
            line-height: 35px;
            font-size: 15px;
        }
    </style>
    {{$page['source_code']}}
@stop