@extends('layouts.frontoffice.default')

@section('content')

    <section class="pink-top-banner">
        <div class="container">
            <div class="row-fluid">

                <div id="homecontent-displayPromoteTop" class="leo-manage">
                    <div class="row-fluid">
                        <div class="span12">
                            <h2>Explore our Latest Offers</h2>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <div class="container" id="bme-content">
        <div class="row">
            <div class="span12 ">
                <div class="offer-menu">

                    <div class="main-menu">
                        <a href="{{URL::to('chimney-offers')}}" class="offer-menu-a active">Chimneys</a>
                        <a href="{{URL::to('built-in-hob-offers')}}" class="offer-menu-a">Built-In Hobs</a>
                        <a href="{{URL::to('cooktop-offers')}}" class="offer-menu-a">Cooktops</a>
                        <a href="{{URL::to('breakfast-made-easy')}}" class="offer-menu-a ">Small Appliances</a>
                        <a href="{{URL::to('combo-offers')}}" class="offer-menu-a" style="border-right:none;">Combo
                            Offers</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="row">
            <div class="span12">
                <img class="img-responsive" src="{{asset('frontoffice/img/offerzone/chimney-banner.jpg')}}">
            </div>
        </div>

        @if(!is_null($products_block1))
            <div class="row-fluid media">
                <div class="span9 offset3">
                    <div class="span2">
                        <hr class="heading-hr">
                    </div>
                    <div class="offer-heading span4">Discounted Products</div>
                    <div class="span2">
                        <hr class="heading-hr">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

            @for ($i = 0; $i < count($products_block1); $i=$i+3)

                <div class="row-fluid">

                    @for($j=0;$j<3;++$j)
                        @if($i+$j < count($products_block1))
                            <?php

                            $id = $products_block1[$i + $j]->id;
                            $ids = array();
                            array_push($ids, $id);

                            $category_name = $products_block1[$i + $j]->category->name;
                            $category_shortcode = $products_block1[$i + $j]->category->shortcode;
                            $product_name = $products_block1[$i + $j]->name;
                            $product_shortcode = $products_block1[$i + $j]->shortcode;
                            $url = URL::to("$category_shortcode/$product_shortcode");

                            ?>

                            <div class="span4">

                                <?php $images = $products_block1[$i + $j]->images; ?>
                                @if(isset($images))
                                    <?php $image = HtmlUtil::getPrimaryImage($images); ?>
                                    <?php $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE;
                                    $title = isset($image['title']) ? $image['title'] : $products_block1[$i + $j]->name;
                                    ?>
                                @else
                                    <?php $path = Constants::DEFAULT_300_IMAGE;;
                                    $title = $products_block1[$i + $j]->name;
                                    ?>
                                @endif

                                <div class="product_detail_box padding5 productModule">

                                    <?php if (!empty($products_block1[$i + $j]->combos)) {
                                        $combos = $products_block1[$i + $j]->combos;
                                        $no_of_combos = count($combos);
                                    } ?>
                                    @if($no_of_combos  > 0)
                                        <div class="newofferTag DISCOUNT"><span class="flap"></span> <span
                                                    class="offerLogo"></span>
                                            <!--<div class="offerText">
                                                  40% Off
                                              </div>-->
                                            <div class="moreOffers"><span
                                                        class="offerCount">@if($no_of_combos > 1)
                                                        OFFERS @else OFFER @endif </span>

                                                <div class="listOfOffers"></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    @endif


                                    <div class="row-fluid">
                                        <a href="{{$url}}"><img class="img-responsive" src="{{URL::to($path)}}"/></a>
                                    </div>
                                    <div class="row-fluid">
                                        <br/>
                                        <h4 class="blue-heading">{{$products_block1[$i+$j]->name}} </h4>
                                        <!--                <p>{{$products_block1[$i + $j]->sequence}}</p>-->

                                        {{--<p class="margin-top10">A sleek and stylish new hood with an interplay of glass and matt--}}
                                        {{--steel</p>--}}
                                    </div>
                                    <div class="row-fluid">
                                        <div class="accordionButton margin-top10">
                                            <span class="plusMinus">+</span> <span>Features</span>
                                        </div>
                                        <div class="accordionContent margin-top10">

                                            <ul class="product-features">
                                                {{$products_block1[$i+$j]->description}}
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    @if(isset($session_ids) && sizeof($session_ids))
                                        <?php $key = array_search($id, $session_ids);
                                        $disabled = is_int($key) ? "disabled" : "";?>
                                    @else
                                        <?php $disabled = "" ?>
                                    @endif

                                    <div class="row-fluid margin-top-20px">
                                        <div class="span8">
                                            {{--<button data-id="{{$id}}" id="compare_{{$id}}"--}}
                                            {{--class="add_to_compare dark_button"--}}
                                            {{--{{$disabled}} >Add to Compare--}}
                                            {{--</button>--}}
                                        </div>
                                        {{--onclick="var res = add_to_compare(this); if(res == true) {this.disabled = true;}else{this.disabled = false;}"--}}
                                        <div class="span4">
                                            <?php $is_upcoming_product = false;
                                            //  echo "<pre>";print_r($products_block1->tags);echo "</pre>";
                                            ?>
                                            @if(isset($products_block1[$i+$j]->tags))
                                                @for($k=0;$k < count($products_block1[$i+$j]->tags);$k++)
                                                    <?php $tags = $products_block1[$i + $j]->tags; ?>
                                                    @if($tags[$k]->name == 'upcoming_products')
                                                        <?php $is_upcoming_product = true; ?>
                                                    @endif
                                                @endfor
                                            @endif
                                            @if($is_upcoming_product == true)
                                                <img src="{{asset('frontoffice/img/coming-soon.png')}}"
                                                     title="Coming Soon"/>
                                                <?php $is_upcoming_product = false; ?>
                                            @else
                                                @if($products_block1[$i+$j]->availability ==1)
                                                    <a href="#" data-id="{{$id}}" class="add_to_cart_image">
                                                        <input type="hidden" data-item-type="product" data-id="{{$id}}"
                                                               name="qty" id="quantity_wanted"
                                                               class="text qty_{{$id}}" value="1" size="2">
                                                        <img src="{{asset('frontoffice/img/add-cart.png')}}"
                                                             title="Add to Cart"/>
                                                    </a>
                                                @else
                                                    <ul>
                                                        <li class="outstock cart">Out of stock</li>
                                                    </ul>
                                                @endif

                                            @endif

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-fluid">
                                        <?php  $offer_price = $products_block1[$i + $j]->offer_price;
                                        $list_price = $products_block1[$i + $j]->list_price;?>
                                        @if($products_block1[$i+$j]->offer_price!=null && $products_block1[$i+$j]->offer_price!=0)
                                            <?php $strike_through = "price-line-through";

                                            $price_font = "font-14px";
                                            $offer_font = "font-17px pull-right price";
                                            $discount = ($list_price - $offer_price) / $list_price * 100;
                                            $discount_label = "(" . round($discount) . "% OFF)";
                                            ?>
                                        @else
                                            <?php $strike_through = "";
                                            $offer_price = null;
                                            $offer_font = "";
                                            $price_font = "font-17px pull-right price";
                                            $discount = 0;
                                            ?>
                                        @endif

                                        <p class="price_container {{$price_font}}">
                                                        <span class=" {{$price_font}} {{$strike_through}}">
                                                            <span class="WebRupee {{$strike_through}}"> Rs. </span>
                                                            {{number_format($list_price)}}
                                                        </span>
                                            @if(!empty($discount)) {{$discount_label}}  @endif
                                            @if($list_price != $offer_price && $list_price > $offer_price && $offer_price>0 && $list_price>0)
                                                <span class="{{$offer_font}}" style="float:right;"><span
                                                            class="WebRupee"> Rs. </span>{{number_format($offer_price)}}</span>
                                            @endif
                                        </p>


                                    </div>

                                </div>
                            </div>

                        @endif
                    @endfor

                </div>

                <br/>
            @endfor

        @endif

        @if(count($chimney_combos)!=0)
            <div class="row-fluid media">
                <div class="span8 offset4">
                    <div class="span2">
                        <hr class="heading-hr">
                    </div>
                    <div class="offer-heading span3">Combo offers</div>
                    <div class="span2 hr-margin-left-none">
                        <hr class="heading-hr">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row-fluid media" id="combo-offers">
                <div class="span12">
                    @foreach($chimney_combos as $combo)
                        <div class="span6 product_detail_box">
                            <?php
                            $combo_id = $combo->id;
                            $products = $combo['products'];
                            $images = $combo['images'];
                            $combo_url = URL::to('combo/' . $combo['shortcode']);
                            $product_list_price_array = array();
                            ?>

                            <?php $j = 1; ?>
                            <a href="{{$combo_url}}">
                                <div class="span9">

                                    @foreach($products as $product)
                                        <?php

                                        $images = $product['images'];
                                        $combo_product_category = $product['category']['shortcode'];
                                        $combo_product_name = $product['name'];
                                        $combo_product_code = $product['shortcode'];
                                        $combo_product_id = $product['id'];
                                        $combo_product_url = URL::to("$combo_product_category/$combo_product_code");
                                        $total_products = count($products);
                                        if ($total_products == 3) {
                                            $class = "span4";
                                            $font = '';
                                        } else {
                                            $class = "span5";
                                            $font = '';

                                        }
                                        ?>
                                        @if(count($images)!=0)
                                            <?php $image = HtmlUtil::getPrimaryImage($images);
                                            $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE;
                                            $title = isset($image['title']) ? $image['title'] : "";
                                            ?>
                                        @else
                                            <?php $path = Constants::DEFAULT_300_IMAGE;
                                            $title = "";
                                            ?>
                                        @endif

                                        <?php $product_list_price_array[] = $product['list_price']; ?>


                                        <div class="combo-unit unit unit-4 combo-unit-1 {{$class}}">
                                            <div class="pu-image fk-product-thumb bmargin5">
                                                <img class="img-responsive" src="{{URL::to($path)}}" alt=""
                                                     title="{{$title}}">
                                            </div>

                                            <div class="tpadding5">
                                                <form>
                                                    <label for="" class="{{$font}}">
                                                        <span class="category-name bpadding5 fk-inline-block ">{{$product['name']}}</span>
                        <span class="fk-display-block fk-bold lmargin2 pu-border-top tpadding5 bpadding5 ">
                           <span class="WebRupee">Rs.</span>  {{number_format($product['list_price'])}}
                        </span>
                                                    </label>

                                                </form>
                                            </div>

                                        </div>

                                        <?php $j++; ?>
                                    @endforeach
                                </div>

                                <?php $products_list_price_total = array_sum($product_list_price_array);
                                $combo_price_diff = $products_list_price_total - $combo['combo_price'];
                                ?>
                            </a>

                            <div class="span3">
                                <div class="unitExt product-unit bundle-details">
                                    <div class="offers">
                                        <div class="pu-banner pu-offer fk-uppercase">Combo Offer
                                        </div>
                                    </div>
                                    <div class="pu-price">
                                        <div class="bmargin5 with-santa">
                                            <div class="pu-final pu-discount">
                                                            <span class="font-14px pu-old">
                                                                <span class="WebRupee">Rs.</span> {{number_format($products_list_price_total)}}</span>
                                            </div>
                                            <div class="pu-border-top">
                                                <div class="pu-final fk-font-17 "><span
                                                            class="WebRupee">Rs.</span>{{number_format($combo['combo_price'])}}
                                                </div>
                                            </div>
                                        </div>

                                        <input type="hidden" data-item-type="combo"
                                               data-id="{{$combo_id}}" name="qty"
                                               id="quantity_wanted"
                                               class="text combo_qty_{{$combo_id}}" value="1"
                                               size="2">
                                        <a data-id="{{$combo_id}}" data-item-type="combo"
                                           class="btn combo_btn btn-orange fk-buy-now fkg-pp-buy-btn bundle-buy tab-buyBtn-1">
                                            Buy Now
                                        </a>

                                    </div>
                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        @endif

    </div>

    <section class="footer-label">
        <div class="container">
            <div class="col-lg-3">
                {{--Copyright 2015, Capella India--}}
            </div>
            <div class="col-lg-6"></div>
            <div class="col-lg-3 footer-text">
                Need help choosing the right product ? Call us at <strong>9266655555</strong>
            </div>
        </div>
    </section>

    <!--ajax loader-->
    <div class="ajax_loader_div hidden">
        <img src="{{asset('frontoffice/img/loader.gif')}}" alt="" width="35" height="35"/>
        Loading
    </div>

    <script type="text/javascript" src="{{asset('frontoffice/js/cart_modal.js')}}"></script>
    {{HTML::style('frontoffice/css/accordian.css')}}
    {{HTML::script('frontoffice/js/accordian.js')}}
    <style type="text/css">
        .add_to_cart_image {
            margin-right: 10px;
            float: right;
        }

        .footer-label {
            background: #6E6F70;
            margin: 20px 0;
            padding: 30px;
            font-size: 20px;
        }

        .footer-text {
            font-size: 20px;
            color: #FFF;
            text-align: center;
            line-height: 24px;
        }
    </style>

@stop