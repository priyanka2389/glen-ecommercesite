@extends('layouts.frontoffice.default')

@section('content')

    <section class="pink-top-banner">
        <div class="container">
            <div class="row-fluid">

                <div id="homecontent-displayPromoteTop" class="leo-manage">
                    <div class="row-fluid">
                        <div class="span12">
                            <h2>Explore our Latest Offers</h2>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="span12 ">
                <div class="offer-menu">

                    <div class="main-menu">
                        <a href="{{URL::to('chimney-offers')}}" class="offer-menu-a ">Chimneys</a>
                        <a href="{{URL::to('built-in-hob-offers')}}" class="offer-menu-a">Built-In Hobs</a>
                        <a href="{{URL::to('cooktop-offers')}}" class="offer-menu-a">Cooktops</a>
                        <a href="{{URL::to('breakfast-made-easy')}}" class="offer-menu-a ">Small Appliances</a>
                        <a href="{{URL::to('combo-offers')}}" class="offer-menu-a active" style="border-right:none;">Combo
                            Offers</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="row-fluid">
            <img class="img-responsive" src="{{asset('frontoffice/img/offerzone/combo-offer-banner.jpg')}}">
        </div>

        <div class="clearfix"></div>
        <?php // echo "<pre>";print_r($combos);echo "</pre>";exit;?>
        @if(count($combos)!=0)
            <div class="row-fluid media" id="combo-offers">
                <div class="span12">
                    @foreach($combos as $combo)
                        <div class="span6 product_detail_box">
                            <?php
                            $combo_id = $combo->id;
                            $products = $combo['products'];
                            $images = $combo['images'];
                            $combo_url = URL::to('combo/' . $combo['shortcode']);
                            $product_list_price_array = array();
                            ?>

                            <?php $j = 1; ?>
                            <a href="{{$combo_url}}">
                                <div class="span9">
                                    @if(isset($products))
                                        @foreach($products as $product)
                                            <?php

                                            $images = $product['images'];
                                            $combo_product_category = $product['category']['shortcode'];
                                            $combo_product_name = $product['name'];
                                            $combo_product_code = $product['shortcode'];
                                            $combo_product_id = $product['id'];
                                            $combo_product_url = URL::to("$combo_product_category/$combo_product_code");
                                            $total_products = count($products);
                                            if ($total_products == 3) {
                                                $class = "span4";
                                                $font = '';
                                            } else {
                                                $class = "span5";
                                                $font = '';

                                            }
                                            ?>
                                            @if(count($images)!=0)
                                                <?php $image = HtmlUtil::getPrimaryImage($images);
                                                $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE;
                                                $title = isset($image['title']) ? $image['title'] : "";
                                                ?>
                                            @else
                                                <?php $path = Constants::DEFAULT_300_IMAGE;
                                                $title = "";
                                                ?>
                                            @endif

                                            <?php $product_list_price_array[] = $product['list_price']; ?>


                                            <div class="combo-unit unit unit-4 combo-unit-1 {{$class}}">
                                                <div class="pu-image fk-product-thumb bmargin5">
                                                    <img class="img-responsive" src="{{URL::to($path)}}" alt=""
                                                         title="{{$title}}">
                                                </div>

                                                <div class="tpadding5">
                                                    <form>

                                                        <label for="" class="{{$font}}">
                                                            <span class="category-name bpadding5 fk-inline-block ">{{$product['name']}}</span>
                        <span class="fk-display-block fk-bold lmargin2 pu-border-top tpadding5 bpadding5 ">
                           <span class="WebRupee">Rs.</span>  {{number_format($product['list_price'])}}
                        </span>
                                                        </label>

                                                    </form>
                                                </div>

                                            </div>

                                            @if($j<count($products))
                                                {{--<div class="unit plus span1">+</div>--}}
                                            @endif


                                            <?php $j++; ?>
                                        @endforeach
                                    @endif
                                </div>

                                <?php $products_list_price_total = array_sum($product_list_price_array);
                                $combo_price_diff = $products_list_price_total - $combo['combo_price'];
                                ?>
                            </a>

                            <div class="span3">
                                <div class="unitExt product-unit bundle-details">
                                    <div class="offers">
                                        <div class="pu-banner pu-offer fk-uppercase">Combo Offer
                                        </div>
                                    </div>
                                    <div class="pu-price">
                                        <div class="bmargin5 with-santa">
                                            <div class="pu-final pu-discount">
                                                            <span class="font-14px pu-old">
                                                                <span class="WebRupee">Rs.</span> {{number_format($products_list_price_total)}}</span>
                                                {{--<span class="santa-discount fk-bold"><span--}}
                                                {{--class="WebRupee">Rs.</span>{{$combo_price_diff}}</span>--}}
                                            </div>
                                            <div class="pu-border-top">
                                                <div class="pu-final fk-font-17 "><span
                                                            class="WebRupee">Rs.</span>{{number_format($combo['combo_price'])}}
                                                </div>
                                            </div>
                                        </div>

                                        <input type="hidden" data-item-type="combo"
                                               data-id="{{$combo_id}}" name="qty"
                                               id="quantity_wanted"
                                               class="text combo_qty_{{$combo_id}}" value="1"
                                               size="2">
                                        <a data-id="{{$combo_id}}" data-item-type="combo"
                                           class="btn combo_btn btn-orange fk-buy-now fkg-pp-buy-btn bundle-buy tab-buyBtn-1">
                                            Buy Now
                                        </a>

                                    </div>
                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        @endif

    </div>
    <section class="footer-label">
        <div class="container">
            <div class="col-lg-3">
                {{--Copyright 2015, Capella India--}}
            </div>
            <div class="col-lg-6"></div>
            <div class="col-lg-3 footer-text">
                Need help choosing the right product ? Call us at <strong>9266655555</strong>
            </div>
        </div>
    </section>
    <!--ajax loader-->
    <div class="ajax_loader_div hidden">
        <img src="{{asset('frontoffice/img/loader.gif')}}" alt="" width="35" height="35"/>
        Loading
    </div>

    <script type="text/javascript" src="{{asset('frontoffice/js/cart_modal.js')}}"></script>
    {{HTML::style('frontoffice/css/accordian.css')}}
    {{HTML::script('frontoffice/js/accordian.js')}}
    <style type="text/css">
        .footer-label {
            background: #6E6F70;
            margin: 20px 0;
            padding: 30px;
            font-size: 20px;
        }

        .footer-text {
            font-size: 20px;
            color: #FFF;
            text-align: center;
            line-height: 24px;
        }

    </style>

@stop