@extends('layouts.frontoffice.default')

@section('content')

<style type="text/css">
  table td, table td strong,table th{
      line-height: 26px;
      font-size: 14px;
  }
  .notes_div ul {
       padding-left: 40px;
  }

  .notes_div ul li {
      list-style: circle !important;
      padding-bottom: 5px;
      font-size: 12px;
      line-height: 20px;
  }
</style>
<div class="container">
    <div class="row">
        <div class="span12">
            <h1>GLEN Customer Care</h1>
        </div>
    </div>

    <br/><br/>



    <div class="row justify">
        <div class="span12 notes_div">

            <table cellspacing="10" cellpadding="0" width="100%">
                <tbody>
                    <tr>
                        <td><strong>Schedule of Service Charges </strong></td>
                        <td align="right"><strong>Date: January 1, 2013 </strong></td>
                    </tr>
                </tbody>
            </table>
            <p>Given below are the charges applicable on various service activities by the Authorized Service
               provider </p>

            <br/><br/>
            <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0" width="100%"
                                  cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">

                <tr>
                    <th>SL.No</th>
                    <th>Part Name</th>
                    <th>Customer Price(Rs)</th>
                </tr>
                {{--<tbody>--}}
                    <tr>
                        <td>1</td>
                        <td>Chimney Installation Within Municpal Limits</td>
                        <td>400</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>2 Chimney Installation‐ Outside Municpal Limit(up to 25 KM)</td>
                        <td>500</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Chimney Regular Quarterly Service (around 30 minutes)</td>
                        <td>250</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Chimney Overhauling (Around 60 – 70 Minutes)</td>
                        <td>500</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Island chimney / Down Draft Hood installation</td>
                        <td>1500</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Built‐in‐Hob/Oven/Microwave Oven/ Warmer Drawer Installation</td>
                        <td>300</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Built –in‐Hob/Gas Stove Conversion from LPG to PNG</td>
                        <td>250</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Cooking range/ Built‐In‐Dishwasher</td>
                        <td>250</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Installation for chimney over 3 Ft.</td>
                        <td>2500</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>Cooking range Conversion from LPG to PNG</td>
                        <td>500</td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>Down Dwarf/ Twin Chimney service</td>
                        <td>1500</td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>Dish Washer Service</td>
                        <td>500</td>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td>Gas stove/Induction/Hob Service within Municipal limits</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td>Gas stove/Induction/Hob Service outside Municipal limits</td>
                        <td>300</td>
                    </tr>
                    <tr>
                        <td>15</td>
                        <td>Visit Charges ‐ Out of Warranty within Municipal Limits </td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <td>16</td>
                        <td>Visit Charges ‐ Out of Warranty Outside Municipal Limits </td>
                        <td>300</td>
                    </tr>
                {{--</tbody>--}}
            </table>
            <h3>Notes:</h3>
             <ul>
                   <li>These Service Charges are valid up to December 31, 2013. </li>

                   <li>Authorized service center can’t charge more than above prescribe rates from customer under any circumstances </li>

                   <li>Service technician must give the cash memo/receipt to the customer. </li>

                   <li>These charges do not include any type of civil work. The hole for the exhaust pipe to go
                       out and the cutting of the stone/slab for Hob installation to be arranged by customers
                       only. </li>

                   <li>In case the customer enters into any arrangement to do any civil work with the service
                       provider, it will not be with in the scope of any warranty or service arrangement and the
                       Company will not be responsible for any deficiency in the service on this count.</li>

                   <li>It is recommended that a 6” Dia PVC Pipe is installed by the customer for ducting to take
                    the smoke out of the kitchen, The pipes, bends and clips etc needs to be provided by the
                    customer only. In case the customer uses the services of the service provider in
                    procuring the same then the commercial terms should be settled well in advance to avoid
                    any future mis-understanding.  </li>

                   <li>Wherever the site visit is required for marking the civil work or stone cutting, 50% of the
                        installation charges will need to be paid in advance, these will be adjusted at the time of
                        final installation on completion of the job</li>

                   <li>Calls beyond 25 KM of Municipal limits or Out-station calls will be attended as per the
                        case-to-case basis depending on the distance and the actual fare will be charged. All
                        commercials will be discussed before the job is taken to avoid any confusion. </li>

                   <li>Re-installation will be considered as new installation and above charges will be
                       applicable.</li>

                   <li>If Installation done by other than Authorized person, any complaint related to the
                       installation will be treated as new installation and charges as above will be applicable.</li>

                   <li>Demonstration of any product like Food Processor/ Juicer Mixer Grinder / Mixer Grinder /
                       Mini Chopper / Induction Cooker / OTG at home the visit charges will be applicable.
                       However customer may visit the nearest service center along with product for Free Demo
                       after fixing an appointment.</li>

                   <li>Plastic / Rubber and Glass parts are not covered under warranty</li>

                   <li>Cost of the Spare Parts if used any will be extra. </li>

            </ul>

            <br/><br/>
        </div>
    </div>


</div>


@stop