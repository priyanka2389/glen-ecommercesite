@extends('layouts.frontoffice.default')

@section('content')
<script type="text/javascript">
    $(document).ready(function () {

        $(".fancybox")
//            .attr('rel', 'gallery')
            .fancybox({
                openEffect  : 'none',
                closeEffect : 'none',
                nextEffect  : 'none',
                prevEffect  : 'none',
                padding     : 0,
                margin      : [20, 60, 20, 60] // Increase left/right margin
            });
    });
</script>
{{$page['source_code']}}
<br/>

@stop

