@extends('layouts.frontoffice.default')

@section('content')

<style type="text/css">

    .privacy_policy_div h3 {
        text-decoration: underline;
    }

    .information_use {
        padding-left: 40px;
    }

    .information_use li {
        list-style: circle !important;
        padding-bottom: 5px;
    }

    th {
        color: #57575A;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
    }

    .gl-sub-menu2 a:link, .gl-sub-menu2 a:visited {
        color: #57575A;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        text-decoration: none;
    }

    .gl-sub-menu2 a:hover {
        color: #57575A;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        text-decoration: underline;
    }

    .no_border {
        border: none !important;
    }

</style>

<div class="container">
<div class="row">
    <div class="span12">
        <h1>Product Manuals</h1>
    </div>
</div>

<br/><br/>

<div class="row justify">

<div class="span12">
<table class="table" align="center" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="6">CHIMNEY MANUAL</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="185"><a
                        href="{{asset('/uploads/product/manuals/Life Time Warranty.pdf')}}" target="_blank">Life Time Warranty</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="185"><a
                        href="{{asset('/uploads/product/manuals/5 Year Warranty.pdf')}}" target="_blank">5 Years Warranty</a></td>

            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">BUILT IN HOB</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual hob.pdf')}}" target="_blank">Hob</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL 1013 Induction Hob.pdf')}}" target="_blank">GL 1013
                        Induction Hob</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="3">BUILT IN OVEN</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="185"><a
                        href="{{asset('/uploads/product/manuals/Built-in-Oven-Manual-2002-2012.pdf')}}" target="_blank">Built
                        in Oven</a></td>
                <td>&nbsp;</td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="3">BUILT IN MICROWAVE OVEN</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="185"><a
                        href="{{asset('/uploads/product/manuals/Microwave-Oven-GL-671.pdf')}}" target="_blank">Built in
                        Microwave</a></td>
                <td>&nbsp;</td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">CHIMNEY</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-chimney-hood-without-ltw-ONE-YEAR.pdf')}}"
                        target="_blank">Chimney Hood without Life Time Warranty</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/chimney.pdf')}}" target="_blank">Chimney Hood</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-chimney-straight-line-new.pdf')}}" target="_blank">Chimney
                        Straight Line</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">COOKTOP</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-cooktop-AL.pdf')}}" target="_blank">Cooktop AL</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-cooktop-ISI.pdf')}}" target="_blank">Cooktop ISI</a>
                </td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/MANUAL-EASY-COOK.pdf')}}" target="_blank">Easy-Cook</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="14">FOOD PROCESSOR</th>
            </tr>

            <tr>
<!--                <td width="18">&nbsp;</td>-->
                <td width="24" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-GL-4051.pdf')}}" target="_blank">GL-4051</a></td>

                <td width="24" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-GL-4052-SX.pdf')}}" target="_blank">GL-4052-SX</a></td>
                <td width="18">&nbsp;</td>
                <td width="24" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-GL-4052.pdf')}}" target="_blank">GL-4052</a></td>
                <td colspan="3">&nbsp;</td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="3">BREAD MAKER</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="185"><a
                        href="{{asset('/uploads/product/manuals/GL_3034.pdf')}}" target="_blank">GL- 3034</a></td>
                <td>&nbsp;</td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">INDUCTION COOKER</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/MANUAL-GL-3070.pdf')}}" target="_blank">GL- 3070</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/MANUAL-GL-3074.pdf')}}" target="_blank">GL- 3074</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_3075.pdf')}}" target="_blank">GL- 3075</a></td>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/MANUAL GL 3076.pdf')}}" target="_blank">GL- 3076</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_3078.pdf')}}" target="_blank">GL- 3078</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_3079.pdf')}}" target="_blank">GL- 3079</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="12">IRON</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-gl-8021.pdf')}}" target="_blank">GL-8021</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-gl-8024.pdf')}}" target="_blank">GL-8024</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-gl-8025.pdf')}}" target="_blank">GL-8025</a></td>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-gl-8026.pdf')}}" target="_blank">GL-8026</a></td>
                <td colspan="6">&nbsp;</td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">JUICER, MIXER AND GRINDER</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-4013.pdf')}}" target="_blank">4013</a></td>

                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/manual-4015.pdf')}}" target="_blank">4015JU &amp; JMG</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td>
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="18">KETTLE</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_9002.pdf')}}" target="_blank">GL-9002 Lx</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_9007.pdf')}}" target="_blank">GL-9007</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_9008.pdf')}}" target="_blank">GL-9008</a></td>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_9009.pdf')}}" target="_blank">GL-9009</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_9010.pdf')}}" target="_blank">GL-9010</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">MINI CHOPPER</th>
            </tr>
            <tr>
                <td width="25" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td width="177" align="left" valign="middle" bgcolor="#dddddd"><a
                        href="{{asset('/uploads/product/manuals/MANUAL-GL-4043.pdf')}}" target="_blank">GL-4043</a></td>
                <td width="472">&nbsp;</td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="18">MIXER GRINDER</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/MANUAL-GL-4021.pdf')}}" target="_blank">GL-4021</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/MANUAL-GL-4022.pdf')}}" target="_blank">GL-4022</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/MANUAL-GL-4023.pdf')}}" target="_blank">GL-4023</a></td>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/MANUAL-GL-4025.pdf')}}" target="_blank">GL-4025</a></td>

                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/MANUAL-GL-4026-4026-PLUS.pdf')}}" target="_blank">GL-4026
                        &amp; GL-4026-Plus</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_4046.pdf')}}" target="_blank">GL- 4046</a></td>
      </tr>

            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">OVEN TOASTER GRILLER</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_5009.pdf')}}" target="_blank">GL- 5009</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL 5020 OTG Manual.pdf')}}" target="_blank">GL- 5020</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL 5030 OTG Manual.pdf')}}" target="_blank">GL- 5030</a></td>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL 5048 OTG Manual.pdf')}}" target="_blank">GL- 5048</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_5060_OTG_Manual_Upload.pdf')}}" target="_blank">GL- 5060</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">POP-UP TOSTER</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_3015.pdf')}}" target="_blank">GL- 3015</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL%203018%20Toaster%20Manual.pdf')}}" target="_blank">GL-
                        3018</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_3019.pdf')}}" target="_blank">GL- 3019</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">RICE COOKER</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="185"><a
                        href="{{asset('/uploads/product/manuals/GL%203055%20Rice%20Cooker%20Manual.pdf')}}" target="_blank">GL-3055</a>
                </td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="176"><a
                        href="{{asset('/uploads/product/manuals/GL 3056-57 Rice Cooker Manual.pdf')}}" target="_blank">GL-3056
                        &amp; GL-3057</a></td>
                <td width="234" colspan="3">&nbsp;</td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">STEAM COOKER</th>
            </tr>
            <tr>
                <td width="21" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td width="661" align="left" valign="middle" bgcolor="#dddddd"><a
                        href="{{asset('/uploads/product/manuals/Steam_Cooker GL-3051.pdf')}}" target="_blank">GL-3051</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">SANDWICH MAKER</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_3025.pdf')}}" target="_blank">GL-3025</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_3026.pdf')}}" target="_blank">GL-3026</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_3027dx.pdf')}}" target="_blank">GL-3027</a></td>
            </tr>


            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">GLASS GRILL</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_3033.pdf')}}" target="_blank">GL- 3033</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_3035.pdf')}}" target="_blank">GL- 3035</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="3">TANDOOR</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="185"><a
                        href="{{asset('/uploads/product/manuals/GL 5014 Tandoor Manual_Final.pdf')}}" target="_blank">Tandoor
                        GL 5014</a></td>
                <td>&nbsp;</td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="3">AIR FRYER</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="185"><a
                        href="{{asset('/uploads/product/manuals/GL 3041 Air Fryer Manual.pdf')}}" target="_blank">Air
                        Fryer</a></td>
                <td>&nbsp;</td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td class="no_border">
        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
            <tbody>
            <tr>
                <th align="left" colspan="8">HEATERS</th>
            </tr>
            <tr>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_7011.pdf')}}" target="_blank">GL- 7011</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_7012.pdf')}}" target="_blank">GL- 7012</a></td>
                <td width="15">&nbsp;</td>
                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                        href="{{asset('/uploads/product/manuals/GL_7013.pdf')}}" target="_blank">GL- 7013</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

</tbody>
</table>
</div>

</div>

<br/><br/>

</div>


@stop