@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/css/blocklayered-15.css')}}

{{--{{HTML::script('frontoffice/js/cloud-zoom.1.0.2.min.js')}}--}}
{{HTML::script('frontoffice/js/zoomsl-3.0.min.js')}}
{{HTML::style('frontoffice/css/cloud-zoom.css')}}

{{HTML::script('frontoffice/js/jquery/plugins/jquery.idTabs.js')}}
{{HTML::script('frontoffice/js/cart_modal.js')}}
{{HTML::script('frontoffice/js/request_demo.js')}}
{{HTML::script('frontoffice/js/send_to_friend.js')}}
{{HTML::script('frontoffice/js/call_us.js')}}

<!--<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">-->
<!--<script type="text/javascript"-->
<!--        src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>-->
<!--<link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_overlay.png">-->
<!--<link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite.png">-->
<!--<link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1-->
<!--.5/fancybox_sprite@2x.png">-->
{{HTML::script('frontoffice/js/accordian.js')}}

<script type="text/javascript">var switchTo5x = true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script
    type="text/javascript">stLight.options({publisher: "d6ab3056-2035-428e-a509-e24d7ce2fbb4", doNotHash: false, doNotCopy: false, hashAddressBar: true});</script>
<style type="text/css">
    #promotetop {
        height: 5px !important;
        margin-bottom: 15px !important;
    }

    .instock {
        color: #1B910B;
        font-weight: bold;
    }

    .outstock {
        color: #d21f45;
        font-weight: bold;
    }

    .outstock.cart {
        margin-left: 10px;
        font-size: 14px;
    }

    .jqzoom {
        text-decoration: none;
        float: left;
    }

    .enabledlink {
        pointer-events: auto;
        cursor: pointer;
    }

    .disabledlink {
        pointer-events: none;
        cursor: auto;
    }

    @media print {
        .slide-out-div {
            display: none;
        }
    }

    .online_only {
        color: #333;
    }

    .light_strip {
        background-color: #f5f5f5;
        line-height: 25px;
        padding: 5px !important;
    }

    .dark_strip {
        background-color: #DDDDDD;
        line-height: 25px;
        padding: 5px !important;
    }

    .strip {
        line-height: 25px;
        padding: 5px !important;
    }

    h4 {
        padding: 5px;
        color: #FFF !important;
    }

    #layered_block_left span.closed a {
        background: url("../frontoffice/img/category/menu_down.jpg") no-repeat scroll 0 0 transparent;
        border: 0 none;
        color: #333333;
        cursor: pointer;
        display: block;
        float: right;
        height: 15px;
        padding: 0;
        width: 15px;
    }

    #product-detail .layered_subtitle {
        background: none !important;
        background-color: #17479d !important;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {

        $('#myModal,#myModal1').on('hidden', function () {
            $(this).data('modal', null);
        });

        $('#call_us').click(function () {
            $('#myModal2').modal()
        });

        var site_url = $.url();
        var host = site_url.attr('host');

        $("#more_info_block ul").idTabs();

        $('.variant').change(function () {

            $variant_url = $(this).find(':selected').data('url');
            console.log($variant_url);

            if ($variant_url != 'select') {
                window.location.href = $variant_url;
            }

        });

//        trigger q-tip
        $('.question_image_product_block').live('click', function (event) {
            // Bind the qTip within the event handler
            $(this).qtip({
                overwrite: false, // Make sure the tooltip won't be overridden once created
                content: {
                    title: {
                        text: 'Description',
                        button: true
                    },
                    text: $(this).find('.tooltip_val')[0].innerHTML
                },
                show: {
                    event: event.type, // Use the same show event as the one that triggered the event handler
                    ready: true,// Show the tooltip as soon as it's bound, vital so it shows up the first time you hover!
                    solo: true
                },
                hide: {event: false},
                position: {
                    at: 'bottom left',
                    my: 'top left',
                    adjust: {
                        method: 'flip'
                    },
                    viewport: true
                }, style: {
                    width: 400, // Overrides width set by CSS (but no max-width!)
                    classes: 'qtip-light',
                    overflow: 'scroll'
                }

            }, event); // Pass through our original event to qTip
        });

        $('#layered_form span.layered_close a').click(function (e) {
            if ($(this).html() == '&lt;') {
                $('#' + $(this).attr('rel')).show();
                $(this).html('v');
                $(this).parent().removeClass('closed');
            }
            else {
                $('#' + $(this).attr('rel')).hide();
                $(this).html('&lt;');
                $(this).parent().addClass('closed');
            }

            e.preventDefault();
        });

        $('#wrap').css('z-index', '1');

        //console.log(window.innerWidth);

        var window_width = window.innerWidth;
        if (window_width == 768) {
            var width = 300;
            var height = 300;
            var is_innerzoom = false;
        } else if (window_width == 640) {
            var width = 230;
            var height = 230;
            var is_innerzoom = false;
        } else if (window_width == 480 || window_width == 320) {
            var is_innerzoom = true;
        } else {
            var width = 400;
            var height = 400;
            var is_innerzoom = false;
        }
        $(".container").imagezoomsl({
//          		descarea: ".zoomarea",
            zoomrange: [1, 10],
//          		zoomstart: 1.68,
            cursorshadeborder: "3px solid black",
            magnifiereffectanimate: "fadeIn",
            magnifiersize: [width, height],
            statusdivfont: "bold 13px Open Sans",
            statusdivbackground: "#EFEFEF",
            innerzoom: is_innerzoom
        });
//        $('#view_full_size').on('mouseover',function(){
//            $('.magnifier').addClass('span2');
//        });

    });
</script>

@include('_partials.frontoffice.compare_bar')


<?php $session_ids = Session::get('id'); ?>
<?php $category_name = $product->category->name;
$category_shortcode = $product->category->shortcode;
$product_name = $product->name;
$product_shortcode = $product->shortcode;
$product_price = ($product->offer_price > 0) ? (float)$product->offer_price : (float)$product->list_price;
$product_id = (int)$product->id;
$script = $product->script;
$css = $product->css;
$url = URL::to("$category_shortcode/$product_shortcode");
$is_demo = $product->is_demo;
?>




<section id="columns" class="clearfix">
<div class="container">
<div class="row-fluid">


<!--left categories accordian starts here-->
@include('_partials.frontoffice.left_categories_accordian')
<!--left categories accordian ends here-->


<section id="center_column" class="span9">
<div class="contenttop row-fluid">


<!-- Breadcrumbs -->
<?php $category = Request::segment(1); ?>
<div id="breadcrumb">
    <ul class="breadcrumb">
        <li>
            <a href="{{URL::to('/')}}" title="return to Home">Home</a>
            <span class="divider">&gt;</span>
        </li>
        <li class="active"><a href="{{URL::to('/'.$category_shortcode)}}"
                              title="">{{$category_name or ''}}</a><span
                class="navigation-pipe">&gt;</span>
            {{$product_name or ''}}
        </li>
    </ul>
</div>
<!-- /Breadcrumb -->
<div id="product-detail">
<div id="primary_block" class="row-fluid">


<!-- right infos-->
<div id="pb-right-column" class="span5">
    <div class="images-block">

        <div id="image-block">

            <!--  product primary image  -->

            <?php $images = $product->images;
            //            echo "<pre>";print_r($images);echo "</pre>";exit;
            ?>
            <?php $image = HtmlUtil::getPrimaryImage($images); ?>
            <?php $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE;
            $zoom_image_path = str_replace('org', '600', $path);
            $title = $category_name ."/". $product_name
            ?>


            <span id="view_full_size">
                                                {{--<a href="{{asset($zoom_image_path)}}" class='cloud-zoom' id='zoom1'--}}
                                                {{--rel="position:'right',adjustX:10, adjustY:0,zoomWidth:300px,zoomHeight:300px ">--}}
                                                {{--<img src="{{asset($path)}}" alt='' title="{{$product->name}}"/>--}}
                                                {{--</a>--}}
                                                {{--data-help="Используйте колесико мыши для Zoom +/-" data-text-bottom="Осень-зима 2013/14 / НЕДЕЛЯ МОДЫ: <span
                    style='text-decoration:underline;color:#CC3300;'>Нью-Йорк</span>" --}}
                                                <img class="container" src="{{asset($path)}}"
                                                     data-help="Use the wheel Mouse Zoom +/- "
                                                     data-large="{{asset($zoom_image_path)}}"
                                                     data-title="{{$title}}" alt="{{$title}}"
                                                     width="250"/>
			</span>

        </div>
        <!-- thumbnails -->
        <div id="views_block" class="clearfix ">
            <div class="content_prices clearfix">
                <!-- prices -->
                <?php $offer_price = $product->offer_price;
                $list_price = $product->list_price;
                ?>
                @if(isset($offer_price) && $offer_price>0 && $list_price>0 && $list_price != $offer_price && $list_price
                > $offer_price)
                <?php $strike_through = "price-line-through";
                $price_font = "font-14px";
                $offer_font = "font-17px pull-right price";
                $discount = ($list_price - $offer_price) / $list_price * 100;
                $discount_label = "(" . round($discount) . "% OFF)";
                ?>
                @else
                <?php $strike_through = "";
                $offer_font = "";
                $price_font = "font-17px price";
                $discount = 0;
                ?>
                @endif
                <p class="online_only font-17px">Price &nbsp;</p>

                <p class="price_container {{$price_font}}">
                                                        <span class=" {{$price_font}} {{$strike_through}}">
                                                            <span class="WebRupee {{$strike_through}}"> Rs. </span>
                                                            {{number_format($list_price)}}
                                                        </span>
                    @if(!empty($discount)) {{$discount_label}} @endif
                    @if($list_price != $offer_price && $list_price > $offer_price && $offer_price>0 && $list_price>0)
                                                        <span class="{{$offer_font}}" style="float:right;"><span
                                                                class="WebRupee"> Rs. </span>{{number_format($offer_price)}}</span>
                    @endif
                </p>


                <div class="clear"></div>


            </div>
        </div>
    </div>

</div>

<!-- left infos-->
<div id="pb-left-column" class="span7">

<h1>

    {{$product->name}}

    @if(isset($session_ids) && sizeof($session_ids))
    <?php $key = array_search($product->id, $session_ids);
    $disabled = is_int($key) ? "disabled" : "";
    $stclass = "disabledlink"; ?>
    @else
    <?php $disabled = null;
    $stclass = "enabledlink";
    ?>
    @endif


    <div style="float:right; margin-right:10px; font-size:13px; font-weight:700">

        @if(isset($is_demo)&&($is_demo==true))

        <button style="font-family: sans-serif;font-size: 14px"
                class="dark_button request_demo_btn"
                data-toggle="modal" data-target="#myModal"
                data-id="{{$product->id}}">Request Demo
        </button>

        @endif

        <button style="font-family: sans-serif;font-size: 14px"
                class="dark_button add_to_compare"
                id="compare_{{$product_id}}" data-id="{{$product->id}}"
                onclick="this.disabled=true;"
        {{$disabled}}>Add to compare
        </button>
    </div>

</h1>

<!-- usefull links-->
<ul id="usefull_link_block">

    <li id="">
<!--        <div class="fb-like" data-href="{{$url}}" data-layout="button"-->
<!--             data-action="like" data-show-faces="true" data-share="true"></div>-->
        <span class='st_facebook' title='Facebook'></span>
        <span class='st_googleplus' title='Google+'></span>
        <span class='st_linkedin' title='LinkedIn'></span>
        <span class='st_twitter' title='Tweet'></span>
        <span class='st_pinterest' title='Pinterest'></span>

    </li>

    <li class="sendtofriend">
        <a id="send_friend_button" href="#" data-toggle="modal"
           data-target="#myModal1">Send to a friend</a>
    </li>

    <li class="print"><a href="javascript:print();">Print</a></li>
</ul>
<!-- end usefull links-->


<!-- quick overview starts here -->
<div id="short_description_block">
    <h3>Quick Overview</h3>
    <?php // echo "<pre>";print_r($product);echo "</pre>";exit;?>

    <div id="short_description_content" class="rte align_justify">
        <ul class="product-features">
            {{$product->description or 'No description available'}}
        </ul>
    </div>
</div>
<!-- quick overview ends here -->

<div id="product_custom_block">
    <div class="row-fluid">


        <div class="span12">
            <ul class="buy-view-list">

                @if($product->ltw)
                <li class="question_image_product_block">
                    Life Time Warranty

                </li>
                @endif


                <li class="question_image_product_block" data-hasqtip="0"
                    aria-describedby="qtip-0">
                    <strong>Free Shipping</strong>
                    <img src="{{asset('frontoffice/img/question_icon.gif')}}"
                         style="margin:0px 5px 0px 0px">

                    <!--                        free shipping qtip content starts here-->
                    <div class="tooltip_val" style="display:none;"><p>
                            <strong>What are the delivery charges?</strong></p>

                        <p>Glen gives you free shipping on the products bought
                            online.</p>

                        <strong>What is the estimated delivery time?</strong></p>

                        <p>We generally procure and ship the items within the time.
                            Business days exclude public
                            holidays and Sundays.</p>

                        <p>Estimated delivery time depends on the following factors:
                            <br/>
                            • The Product <br/>
                            • Product's availability <br/>
                            • The destination to which you want the order shipped to
                            and location of the courier partner
                            provided
                        </p>

                        <p>The product will be delivered to you in maximum 7 to 10
                            days from the date of dispatch.
                            Dispatch may vary as per your location or any unforeseen
                            conditions which are beyond our
                            control.</p>


                        <p style="text-align:right"><a
                                href="{{URL::to('shipping-and-delivery-faqs')}}"
                                target="_blank">Read More..</a></p>
                    </div>
                    <!--                        free shipping qtip content ends here-->

                </li>

                @if($product->is_cod == 1)
                <li class="question_image_product_block">
                    <strong>Cash on Delievery</strong>
                </li>
                @endif

                @if($product->availability ==1)
                <li class="instock">In stock</li>
                @endif
            </ul>
        </div>
    </div>
</div>

<?php $i = 0; ?>


<!--variants starts here-->
@if(!is_null($variants))

<div class="row-fluid">
    <div class="span12">
        <select class="variant">
            <option value="select" data-url="select">-Select-</option>
            @foreach($variants as $variant)

            @if($product->id==$variant->id)
            <?php $selected = 'selected'; ?>
            @else
            <?php $selected = ''; ?>
            @endif


            <option value="{{$variant->id}}"
                    data-url="{{URL::to($category_shortcode.'/'.$variant->shortcode)}}"
            {{$selected}}>
            @if(!isset($variant->base_diff_text)||($variant->base_diff_text==''))
            {{$variant->name}}
            @else
            {{str_replace('^3','<sup>3</sup>',$variant->base_diff_text)}}
            @endif
            </option>
            @endforeach
        </select>
    </div>
</div>

@endif
<!-- variants ends here -->


<!-- add to cart form-->
<form id="buy_block" action="" method="post">

    <!-- content prices -->
    <div class="content_prices clearfix">
        <!-- prices -->


        <p class="buttons_bottom_block compare_btn">
            {{--<label style="width: 29%; margin-top: 2px;">--}}
                <a href="javascript:void(0);" class="add_to_compare {{$stclass}}"
                   id="comparebtn_{{$product_id}}"
                   data-id="{{$product->id}}" {{$disabled}} >Add to Compare</a>
                {{--</label>--}}
        </p>


        <div class="product_attributes">

            <!-- quantity wanted -->
            <p id="quantity_wanted_p">
                <label>Qty:</label>
                <input type="text" data-item-type="product"
                       data-id="{{$product->id}}" name="qty"
                       id="quantity_wanted"
                       class="text qty_{{$product->id}}" value="1"
                       size="2">
            </p>

        </div>

        <?php $is_upcoming_product = false; ?>
        @if(isset($product->tags))
        @for($k=0;$k < count($product->tags);$k++)
        <?php $tags = $product->tags; ?>
        @if($tags[$k]->name == 'upcoming_products')
        <?php $is_upcoming_product = true; ?>
        @endif
        @endfor
        @endif
        @if($is_upcoming_product == true)
        <!--            <a class="ajax_add_to_cart_button right" href="#" >  Coming Soon</a>-->
        <img class="right"
             src="{{asset('frontoffice/img/coming-soon.png')}}"
             title="Coming Soon"/>
        <?php $is_upcoming_product = false; ?>
        @else
        @if($product->availability ==1)
        <a class="ajax_add_to_cart_button right" href="#"
           data-id="{{$product->id}}" data-item-type="product">
            Add to cart</a>
        @else
        <ul>
            <li class="outstock cart">Out of stock</li>
        </ul>
        @endif

        @endif

        <div class="clear"></div>
    </div>
    <!-- content prices -->

    <!-- attributes -->

    <!-- end attributes -->


</form>

</div>
</div>

<?php $total_documents = $product->documents->count(); ?>
<?php $total_combos = $product->combos->count(); ?>
<?php $total_video = $product->videos->count(); ?>

<!-- description and features tabs starts here -->
<div id="more_info_block" class="clear">
<ul id="more_info_tabs" class="idTabs idTabsShort clearfix">
    <li><a class="more_info_tab" href="#idTab1">More info</a></li>
    <li><a href="#idTab2" class="idTabHrefShort">Specifications</a></li>
    @if($total_video != 0)
    <li><a href="#idTab3" class="idTabHrefShort">Video</a></li>
    @endif
    @if($total_documents!=0)
    <li><a href="#idTab4" class="idTabHrefShort">Support</a></li>
    @endif

    <?php $i = 5;
    $j = 1 ?>
    @if($total_combos!=0 && isset($combos))
    @foreach($combos as $combo)
    @if($j == 1)
    <?php $class = 'selected'; ?>
    @else
    <?php $class = ''; ?>
    @endif
    <li><a href="#idTab{{$i}}" class="idTabHrefShort {{$class}}">Combo
            Offer {{$j}}</a>
    </li>
    <?php $i++;
    $j++; ?>
    @endforeach
    @endif

</ul>
<div id="more_info_sheets" class="sheets align_justify">
<!-- full description -->
<div id="idTab1" class="">
    <div class="product-overview-full">
        <ul class="product-features">
            {{$product->description_secondary or 'No more info for the moment'}}
        </ul>
    </div>
</div>

<div id="idTab2">
    <div id="layered_block_left" class="row-fluid">

        <form action="#" id="layered_form">
            @if(isset($specifications))
            <?php $j = 1; ?>
            @foreach($specifications['attribute_info'] as $key=>$specification)
            <div class="">
                                                        <span class="layered_subtitle span12">
                                                            <div class="span11"><h4>{{$key}}</h4></div>
                                                            <span class="layered_close closed span1">
                                                        <a href="#" rel="ul_layered_id_attribute_group_{{$j}}"
                                                           style="margin: 5px;">
                                                            &lt;</a>
                                                        </span>
                                                        </span>


                <div class="clear"></div>
                <ul id="ul_layered_id_attribute_group_{{$j}}"
                    style="display:none">
                    <li class="nomargin hiddable">
                        <ul>
                            @for($i=0;$i
                            <count
                            ($specification);$i++)
                            @if($i%2==0)
                            <?php $class = 'strip'; ?>
                            @else
                            <?php $class = 'light_strip'; ?>
                            @endif

                            <li class="{{$class}}">
                                <strong>{{$specification[$i]['name']}}</strong>
                                : {{$specification[$i]['value']}}
                            </li>
                            @endfor
                        </ul>

                    </li>
                </ul>
            </div>
            &nbsp;
            <?php $j++; ?>
            @endforeach
            @else
            No specifications are available for the moment
            @endif
        </form>

        <script type="text/javascript">
            $('.layered_id_attribute_group').show();
        </script>

    </div>
</div>

@if($total_video != 0)
<div id="idTab3">
    @foreach($product->videos as $video)
    @if($video->type == 'url')
    <iframe width="300" height="300" src="{{$video->path}}"
            frameborder="0" allowfullscreen
            style="padding:3px;"></iframe>
    @else
    <video width="300" height="240" controls="" id="video1">
        <source type="video/mp4" src="{{asset($video->path)}}"></source>
        Your browser does not support HTML5 video.
    </video>
    @endif
    @endforeach
</div>
@endif

@if($total_documents!=0)
<div id="idTab4">
    <div class="row-fluid">
        <div class="span9">
            <div class="product-overview-full">
                <h3>Manuals &amp; Documentation</h3>

                <div class="content-column-left">
                    <ul>
                        <?php $documents = $product->documents; ?>
                        @foreach($documents as $document)
                        <?php $path = $document->path; ?>
                        <li class="downloadlink  pdf icon">
                            <a href="{{URL::to($path)}}"
                               target="_blank">{{$document->pivot->name}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="span3">
            <div class="content-smallright">
                <div id="ServiceBar">
                    <div class="ServiceBarComponent">
                        <div class="title"><h3>Contact</h3></div>
                        <ul id="ContactOptions">
                            <li class="call">
                                <!--                                       id="call_us"  data-toggle="modal" data-target="#myModal2"-->
                                <a href="{{URL::to('contact-us')}}">Call us</a>
                            </li>
                            <li class="mail">
                                <a href="{{URL::to('contact-us')}}" target="">Send
                                    email</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
@endif

@if($total_combos!=0 && isset($combos))
<?php $i = 5; ?>
@foreach($combos as $combo)

<?php $combo_id = $combo['combo_info']['id']; ?>
<div id="idTab{{$i}}">
    <div class="line rposition bundle-content">

        <?php $products = $combo['product']; ?>

        @if(sizeof($products!=0))

        <?php $j = 1; ?>
        @foreach($products as $product)

        <?php $images = $product['images'];
        $combo_product_category = $product['category'];
        $combo_product_name = $product['name'];
        $combo_product_code = $product['shortcode'];
        $combo_product_id = $product['id'];
        $combo_product_url = URL::to("$combo_product_category/$combo_product_code");
        $title = $combo_product_category ."/". $combo_product_name
        ?>
        @if($images->count()!=0)
        <?php $image = HtmlUtil::getPrimaryImage($images);
        $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE;

        ?>
        @else
        <?php $path = Constants::DEFAULT_300_IMAGE;

        ?>
        @endif

        <?php $product_list_price_array[] = $product['list_price']; ?>

        <div class="combo-unit unit unit-4 combo-unit-1">
            <a class="pu-image fk-product-thumb bmargin5"
               href="{{$combo_product_url}}">
                <img src="{{URL::to($path)}}" alt="{{$title}}"
                     title="{{$title}}">
            </a>

            <div class="tpadding5">
                <form>
                    <input type="checkbox" name="bundle11"
                           class="bundle-check bind-bundle-check"
                           checked="checked" disabled>

                    <label for="" class="font-14px">
                        <span class="category-name bpadding5 fk-inline-block ">{{$product['name']}}</span>
                                <span class="fk-display-block fk-bold lmargin20 pu-border-top tpadding5 bpadding5 ">
                                   <span class="WebRupee">Rs.</span>  {{number_format($product['list_price'])}}
                                </span>
                    </label>

                </form>
            </div>

        </div>

        @if($j!=sizeof($products))
        <div class="unit plus">+</div>
        @endif

        <?php $j++; ?>
        @endforeach
        @endif


        <?php $products_list_price_total = array_sum($product_list_price_array);
        $combo_price_diff = $products_list_price_total - $combo['combo_info']['combo_price'];
        ?>


        <div class="unitExt product-unit bundle-details">
            <div class="offers">
                <div class="pu-banner pu-offer fk-uppercase">Combo Offer
                </div>
            </div>
            <div class="pu-price">
                <div class="bmargin5 with-santa">
                    <div class="pu-final pu-discount">
                                                                    <span class="font-14px pu-old">
                                                                        <span class="WebRupee">Rs.</span> {{number_format($products_list_price_total)}}</span>
                        {{--<span class="santa-discount fk-bold"><span--}}
                                                                    {{--class="WebRupee">Rs.</span>{{$combo_price_diff}}</span>
                        --}}
                    </div>
                    <div class="pu-border-top">
                        <div class="pu-final fk-font-17 "><span class="WebRupee">Rs.</span>{{number_format($combo['combo_info']['combo_price'])}}
                        </div>
                    </div>
                </div>

                <input type="hidden" data-item-type="combo"
                       data-id="{{$combo_id}}" name="qty"
                       id="quantity_wanted"
                       class="text combo_qty_{{$combo_id}}" value="1"
                       size="2">
                <a data-id="{{$combo_id}}" data-item-type="combo"
                   class="btn combo_btn btn-orange fk-buy-now fkg-pp-buy-btn bundle-buy tab-buyBtn-1">
                    Buy {{sizeof($products)}} Items
                </a>

            </div>
        </div>

    </div>
</div>

<?php $i++; ?>
@endforeach
@endif


</div>
</div>
<!-- description and feature tabs end here-->

</div>

<!-- module Related products starts here -->
@include('_partials.frontoffice.related_products')
<!--module related products ends here-->


<!-- module testimonials starts here -->
@include('_partials.frontoffice.testimonials')
<!--module testimonials ends here-->


</div>
<!-- end div block_home -->
</section>

</div>
</div>
</section>

<!--module request demo starts here-->
@include('_partials.frontoffice.request_demo')
<!--module request demo ends here-->

<!--module send to friend starts here-->
@include('_partials.frontoffice.send_to_friend')
<!--module send to friend  ends here-->

<!--module call us starts here-->
@include('_partials.frontoffice.call_us')
<!--module call us  ends here-->

<script type="text/javascript">
    var google_tag_params = {
        ecomm_pagetype: 'product',
        ecomm_prodid: <?php echo $product_id; ?>,
        ecomm_totalvalue: <?php echo $product_price; ?>,
        ecomm_category: '<?php echo $category_name; ?>'
    };
    console.log(google_tag_params);
</script>
<!--include page script-->

@if(isset($script))

{{ html_entity_decode($script) }}

@endif

@stop

<!--page essential script starts here-->
@section('page_script')

@stop
<!--page essential script ends here-->
