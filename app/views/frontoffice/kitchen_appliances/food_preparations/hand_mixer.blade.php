@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
    @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/hand-mixer/hand-mixer.png')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Hand Mixer</h1>

                        <p class="top-header-p ">Glen India brings you an innovative electric hand mixer, which lets you whisk like professionals.
                            Whether it is perfectly prepared omelet mix or mayonnaise or smooth fluffy whipped cream; you can make it all. With the Glen
                            Hand Mixer you can whisk, whip, mix or froth a delicious treat in seconds, making it the perfect addition to your kitchen.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/hand-mixer/Dough-Hooks.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Dough Hooks</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description"> The hand mixer comes with splendidly designed dough hooks, so that there is no spillage and you get the perfect kneaded dough in a matter of seconds. The dough is homogenous and helps you to make fluffy chapatis.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/hand-mixer/5_Speed_Setting_with_turbo.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>5 Speeds with turbo</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The intuitive hand mixer comes with five speed levels to cater to all your mixing needs.
                            In case the requirement during the operations is for instant higher speed, it’s provided with a Turbo setting also, so you
                            get the required power at the twist of a finger.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/hand-mixer/Beaters.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Beaters</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description"> The beater attachments in this hand mixer are designed for smooth mixing of yolk,
                            cream and batters.  They are designed ergonomically for the best results and you get uniformly beaten processed product.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/hand-mixer/2-years-extended-warranty-on-motor.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>2 years extended warranty on motor</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The appliance comes with a 2 year warranty on motor.</p>
                    </div>
                </div>

            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop