@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
    @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/slow-juicer/GL-4017.png')}}" >
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Slow Juicer</h1>

                        <p class="top-header-p ">Slow juicer is a revolutionary fruit and vegetable juicer that gives you Wonder Juice. You get the
                            benefit of the goodness of fruit and vegetables just like they are naturally. Based on the unique Slow Squeezing Mechanism,
                            the Slow Juicer preserves all the good nutrients of fruits and vegetables. A glass of Wonder Juice will help you to keep your
                            family healthy and fit. Slow Juicer is Health Insurance for the entire family.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/slow-juicer/Squeezing_not_grinding.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Squeezing not grinding</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The originative Slow Squeezing System extracts juice at a low RPM, which prevents disruption of
                            cellular structure of fruits & vegetables.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/slow-juicer/Preserves_natural_flavor_and_nutrition.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Preserves natural flavor and nutrition</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Slow Squeezing Mechanism of slow juicer prevents oxidation and separation of good enzymes and
                            produces 100% living juice overflowing with essential vitamins and nutrients.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/slow-juicer/Online_clog_free_operation_easy_to_clean.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Clog-free operation, easy to clean</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">So that you can enjoy continuous juicing, automatic pulp removal is facilitated through a
                            separate outlet. For juicing a variety of fruits and vegetables, all you need is to put the cut fruits or vegetables through
                            feeding tube. It is also very easy to clean; a rinse with clean water does it all. The superior grade plastic doesn’t allow
                            residue of flavor. Using this electric juicer is easy, convenient and hassle free.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/slow-juicer/More_juice_more_vitamins_for_healthy_living.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>More juice, more vitamins for healthy living</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The innovative slow juicer squeezes the juice instead of cutting it. Glen slow juicer preserves natural
                            vitamins, minerals and enzymes. All the goodness of fruits is unlocked into the juice. These nutrients play a vital role in nutrition
                            and make us feel better and look healthier.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop