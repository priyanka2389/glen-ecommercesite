@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}


<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/mixer-grinder/mixer-grinder.jpg')}}">
                    </div>
                    <div class="span7">
                    {{--margin-top80--}}
                        <h1 class="top-header-heading ">Mixer grinders</h1>

                        <p class="top-header-p ">Glen brings you a wide selection of mixer grinders. With these versatile small 
			kitchen appliances, you can effortlessly prepare a variety of dishes and drinks in no time. From delicious shakes 
			to tangling sauces and soups, Glen mixer grinder is easy to use and clean, offering you all round convenience. You 
			can always rely on these innovative mixer grinders for the best culinary results.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/juicer-mixer-grinder/Fruit-Filter.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Fruit filter</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The mixer grinder comes with a unique fruit filter attachment so that you can prepare
			 seedless super smooth juices. It is especially useful for juicing seeded grapes and pureeing tomato.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/food-processor/Most-Powerful-Motor-MG.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Most Powerful Motor</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">A 750 W motor with its strong and sturdy function makes the toughest of tasks
			 like grinding soaked rice & dals fast, simple and convenient.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/food-processor/Food-Grade-Blade-&-Jars.PNG')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Food Grade Blade & Jars</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Glen�s latest mixer grinder comes with three different capacity jars to facilitate
			 smooth grinding of wet and dry ingredients. The blades are made of special food grade 304 stainless steel, which do
			 not affect the food quality even when they get heated up.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/mixer-grinder/Unberakable_Lids.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Unberakable lids</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The lids of all the jars in the mixer grinder are made of polycarbonate for
			 a break proof longer life. Transparent lids ensure that you know the status of food being processed.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop