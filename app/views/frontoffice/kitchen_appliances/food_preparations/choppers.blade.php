@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
@media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>
<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('/frontoffice//img/chopper/chopper.jpg')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Choppers</h1>

                        <p class="top-header-p ">Glen India, the leading innovative manufacturers of small kitchen appliances bring you the
 food chopper, a compact vegetable cutting machine for chopping, mashing and pureeing small quantities of food. With the powerful stainless
 steel blade you can prepare granular chutneys, mash bananas for baby food or even make tomato puree. You can chop nuts in millimeter size
 for garnishing.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('/frontoffice/img/chopper/tomato-Puree.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Makes tomato puree</h5></div>
                    <div class="row-fluid">
<p class="feature-description">Indian vegetarian dishes are incomplete without tomato puree. You need to puree tomato almost on a daily basis.
 To make this task convenient and quick, Glen India vegetable chopper comes handy and makes smooth and consistent puree.</p>
<!--                        <p>This unique attachment helps prepare juices without seeds. It is also very useful for grape juices, seedless-->
<!--                            tomato purees and coconut milk..</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/chopper/Chops-Onion-and-Vegetables.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Chops onion and vegetables</h5></div>
                    <div class="row-fluid">
			<p class="feature-description"> Chopping onions and vegetables is a routine in Indian homes. The food chopper does this conveniently.
 			Leave the age-old method of using knife and chopping board and buy the best chopper at unbelievably low prices.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/chopper/Chops-small-quantity-of-nuts-instantly-for-grating.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Chops small quantity of nuts instantly for grating</h5></div>
                    <div class="row-fluid">
			<p class="feature-description">For garnishing desserts or traditional sweet dishes, this electric chopper is
			 very useful. It does coarse chopping for nuts so that the look of the dish is maintained.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/chopper/Whisks-eggs-in-a-jiffy.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Whisks eggs in a jiffy</h5></div>
                    <div class="row-fluid">
			<p class="feature-description"> Whisking of eggs in this food chopper is as easy as it can get. You get smooth
			 and consistent whisked egg instantly to make a delicious omelet or a French toast. Also, it prevents any mess due to spillage.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>


            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop