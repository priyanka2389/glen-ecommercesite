@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<?php $session_ids = Session::get('id'); ?>
<style type="text/css">
    #myTabContent #Speed_settings .span3{width:auto};
</style>


<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('/uploads/product/img/org/09A6A9ED-492C-418B-BB87-F32CD0EF1013.png')}}">
                    </div>
                    <div class="span7">
<!--                        margin-top80-->
                        <h1 class="top-header-heading ">Juicer Mixer Grinder</h1>

                        <p class="top-header-p ">The Glen India juicer mixer grinder is a multi functional kitchen appliance. With 500W
			 motor and three level speed control, you get the ultimate control over grinding operations. The super efficient 
			juicer comes with feeding tubes and innovative pusher to add additional ingredients while juicing. The detachable 
			pulp chamber facilitates easy emptying of waste for continouos juicing.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/juicer-mixer-grinder/Fruit-Filter.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Fruit Filter</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">A fruit filter attachment in liquidizer jar allows making super smooth seedless
			 juices of grapes etc. and tomato purees.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/juicer-mixer-grinder/Large_Online_Pulp_Collector.PNG')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Large Pulp Collector</h5></div>
                    <div class="row-fluid">
 			<p class="feature-description">An extra spacious pulp collector lets you collect fruit pulp. As the same can be
			 removed without dis-manteling the cover, emptying the pulp is an easy task. This feature allows you to prepare 
			serving to your family in a single batch.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/juicer-mixer-grinder/3-Speed-Settings-with-pulse.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>3 Speed Settings with pulse</h5></div>
                    <div class="row-fluid">
 			<p class="feature-description">The Glen juicer mixer grinder comes with three intuitive speed settings with 
			pulse function, so that you can select the speed, which suits the task. You can adjust speeds in the same run, 
			so that you get the texture that you need.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/juicer-mixer-grinder/2-years-extended-warranty.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>2 Year Extended warranty</h5></div>
                    <div class="row-fluid">
			<p class="feature-description"> The Juicer mixer grinder comes with an extended warranty of two years on motor
			 so that you can enjoy this premium kitchen appliance without any worry. It serves as an investment for a healthy life.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>
            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop