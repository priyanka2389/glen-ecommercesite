@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
@media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/blender-grinder/blender.jpg')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Blender grinders</h1>

                        <p class="top-header-p ">In kitchen instant processing of small quantities of food is routine. Glen brings you a range of
 compact wonders ideal for blending, grinding, mashing and pureeing small quantities. Make frothy shakes with the hand blender and seedless tomato
 puree with the mini blender. The mini grinder is ideal for fresh grinding of coffee beans and spices. Enjoy healthier lifestyle with Glen India.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/blender-grinder/Prepare-frothy-shakes-instantly.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Prepare frothy shakes instantly</h5></div>
<div class="row-fluid">
                    <p class="feature-description">
With Glen India�s hand mixer, you can prepare super smooth and frothy shake in no time. The natural flavor remains intact and you can prepare several batches in quick succession for the breakfast or after a heavy workout.
</p>
</div>
</div>

                
<div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/blender-grinder/Conveniently-grape-juice-&-seedless-tomato-puree.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Conveniently juice grapes and make seedless tomato puree</h5></div>
<div class="row-fluid">
                    <p class="feature-description">
Glen India�s mini blender lets you make difficult juices like that of grapes and get the consistent tomato puree, without any seeds. The coarseness of the fruit is gone after blending and you get a homogeneous puree.
</p>
</div>
</div>

               

 <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/blender-grinder/Grind-fresh-aromatic-spices-in-a-jiffy.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Grind fresh aromatic spices in a jiffy</h5></div>
<div class="row-fluid">
                    <p class="feature-description">
The mini grinder can be easily used to grind aromatic spices. The aroma of the spices is preserved along with their natural goodness. The texture of ground spices adds to the flavor of the dish and stays fresh for long.
</p>
</div>
</div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/blender-grinder/Freshly-ground-beans-for-the-perfect-cuppa.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Get freshly ground beans for the perfect cuppa</h5></div>
<div class="row-fluid">
                    <p class="feature-description">
Coffee tastes the best with freshly ground beans. With Glen India�s mini grinder, you can obtain the perfect cup of coffee, which smells and tastes incredibly good.
</p>
</div>
</div>

            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop