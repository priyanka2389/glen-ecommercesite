@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<?php $session_ids = Session::get('id'); ?>

<style type="text/css">
    #promotetop1 {
        background:url("frontoffice/img/induction-cooker/web-bg.png") no-repeat left top;
        margin-top: 15px;
    }

    #promotetop{
        background-color: #fff !important;
    }
    /*background:url("public/frontoffice/img/induction-cooker/web-bg.png") no-repeat left top;*/
</style>

<section id="promotetop1">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
<!--                <div class="row-fluid">-->
<!--                    <div class="span5">-->
<!--                        <img src="{{asset('frontoffice/img/induction-cooker/induction-cooker.png')}}">-->
<!--                    </div>-->
<!--                    <div class="span7">-->
<!--<!--                        margin-top80-->
<!--                        <h1 class="top-header-heading">Induction Cookers</h1>-->
<!---->
<!--                        <p class="top-header-p ">Induction is a powerful cooking medium, just like or even better than-->
<!--                            gas, which is till today known as the most powerful cooking medium. Instant & precise heat-->
<!--                            control, unlike electric cooking where the elements take time to heat and cool, resulting in-->
<!--                            lack of proper control on the dishes being cooked. No heat wastage as the energy is supplied-->
<!--                            directly to the cooking vessel unlike gas or conventional electric cookers where they end up-->
<!--                            heating your kitchen and you, instead of heating up the food.</p>-->
<!---->
<!--                        <div class="top-header-p-border"></div>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="span12" >
                    <img src="{{asset('frontoffice/img/induction-cooker/web-banner.png')}}">
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/induction-cooker/high-safety.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>High Safety</h5></div>
                    <div class="row-fluid">
			<p class="feature-description"> Induction cookers can heat only ferromagnetic utensils, which means that you are
			 safe from accidental burns. Since induction stoves are free from flame generation, the possibility of burning and
			 heat damage is negated. Also, there is no concern about gas leakages. The glass top doesn�t heat up much and hence it
			 is safer when kids are around.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has-->
<!--                            been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/induction-cooker/no-heat-wastage.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>No Heat Wastage</h5></div>
                    <div class="row-fluid">
 			<p class="feature-description">Since induction cooktop directly imparts heat to utensils, there is no diversion to
			 heat the surroundings. It helps save energy and is actually economical over LPG. Also, you can set the timing and spend
			 more time in following your hobbies.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has-->
<!--                            been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/induction-cooker/Pre-Set-cooking-function1.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Pre- Set cooking function</h5></div>
                    <div class="row-fluid">
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has-->
<!--                            been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/induction-cooker/Large-LED-Display.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Large LED Display</h5></div>
                    <div class="row-fluid">
 			<p class="feature-description"> A larger LED display at induction cooktop helps you see more of everything.
			 The four digital display lets you select a preset cooking function, temperature setting and timer.</p>

<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has-->
<!--                            been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>


            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop