@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}



<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/glass-grill/glass-grill.jpg')}}">
                    </div>
                    <div class="span7">
<!--                        margin-top80-->
                        <h1 class="top-header-heading ">Glass Grill</h1>

                        <p class="top-header-p ">Glen India brings you the most sophisticated way of healthy cooking, the
 Glass Grill. The Glass Grill is a durable ceramic glass surface, which evenly distributes heat for consistent cooking. Due to
 non-porous nature of glass-ceramic, nutrients and natural food juices remain intact. The resulting grilled food is juicy, delicious
 and succulent with wonderful flavor and fragrance. The electric barbecue grill is portable and safer alternative to conventional grilling.
 Glass doesn�t emit any harmful gasses or reacts with the food, therefore is totally safe for everyday use.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/glass-grill/grill-directly-on-the-glass.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Grill directly on the glass</h5></div>
                    <div class="row-fluid">
			<p class="feature-description"> The electrical grill doesn�t need anything more to grill on it. The ceramic glass
			 surface lets you grill directly over it, without any hassle. As you grill directly on Glass there is no wastage of 
			heat and its more energy efficient.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has-->
<!--                            been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/glass-grill/cook-directly-on-the-glass.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Cook directly on the glass</h5></div>
                    <div class="row-fluid">
			<p class="feature-description">The German Glass can be used for any type of direct cooking and you can make
			 an omelet, saute vegetables, make cutlets etc. It doesn�t require pans to cook and the glass surface can be 
			used just like that.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has-->
<!--                            been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/glass-grill/easy-to-clean-detachable-oil-tray.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Easy to clean detachable oil tray</h5></div>
                    <div class="row-fluid">
			<p class="feature-description">The electrical glass grill comes with a detachable oil tray. It collects the entire
			 oil residue without any mess. The tray can be detached easily and cleaned like any Stainless Steel utensil.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has-->
<!--                            been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/glass-grill/temperature-control-knob.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Temperature control knob</h5></div>
                    <div class="row-fluid">
			<p class="feature-description">The glass grill has a thermostat which is controlled by the temperature control knob.
			 You can select the required temperature from 6 settings, as required by your recipe. The grilling on controlled 
				temperature doesn�t allow the oil to break and therefore is a healthier option.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has-->
<!--                            been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>
            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop