@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}


<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/steam-cooker/steam-cooker.jpg')}}" style="width:83%">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Steam cooker</h1>

                        <p class="top-header-p ">Good health is a derivative of healthy eating. Nature has gifted us with nutrient rich food, but our 
			cooking practices destroy them. Food cooked with steam retains a higher level of nutrients, vitamins and minerals than by any 
			other method. Since additional fat is not required for cooking, this helps reduce cholesterol and blood pressure. The Glen Steam 
			Cooker helps you cook healthy steam cooked food easily. Made from high quality food grade plastic and with sleek design, this electric 
			steam cooker lets you enjoy tasty food with low calories.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/steam-cooker/Ultra-Compact-Storage.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Ultra Compact Storage</h5></div>
                    <div class="row-fluid">
			<p class="feature-description"> The food steamer doesn�t occupy much kitchen space. Thanks to its compact design, it can be 
			easily fitted in small spaces. It�s also easy to clean and carry when on travel, because of its light weight and planate design.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/steam-cooker/60-Minute-Timer-with-Alarm.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>60 Minute Timer with Alarm</h5></div>
                    <div class="row-fluid">
 			<p class="feature-description"> The cooking steamer comes with timer to allow you select a time as required by 
			the recipe. The maximum time is 60 minutes, which suffices for almost all recipe requirements.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/steam-cooker/Online-water-filling.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Online water filling</h5></div>
                    <div class="row-fluid">
			<p class="feature-description">The steam cooker comes with an ergonomically designed inlet to pour water without any need 
			to stop the cooking process, so you can fill the water while the cooking is on. The markings for minimum and maximum water 
			levels are provided in the water container so that you can fill just the right amount for smooth operation of the appliance.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/steam-cooker/Slot-for-holding-egg.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Slots for holding eggs</h5></div>
                    <div class="row-fluid">
 				<p class="feature-description"> Steam cooked eggs are the most nutritive. With steam cooking most of the protein
			 is retained in cooked yet natural state. To facilitate easy cooking, there are slots provided for eggs. It holds eggs and 
			ensures even cooking of the yolk and albumen.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>


            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop