@extends('layouts.frontoffice.default')

@section('content')
{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
    @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/sanwich-maker/sandwich-maker.jpg')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Sandwich makers</h1>

                        <p class="top-header-p ">Sandwiches are arguably the easiest fix for hunger. To make sandwich yummy and fast, Glen India sandwich makers are handy. Equipped with non-stick scissor cut plates, locking mechanism and cool-touch handles; they are the best sandwich maker. The scissor cut plates cut the sandwiches precisely, keeping the filling inside the sandwich. The sandwich maker’s handle is heat-resistant, so that you can move the sandwich maker when it’s still hot.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/sanwich-maker/Sandwich Plate.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Sandwich Plate</h5></div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/sanwich-maker/Grill Plate.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Grill Plate</h5></div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/sanwich-maker/Waffle Plate.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Waffle Plate</h5></div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/sanwich-maker/NS Removable Plates.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Non-stick, removable, reversible cooking plates</h5></div>
                </div>

            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>
@stop