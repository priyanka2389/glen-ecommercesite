﻿@extends('layouts.frontoffice.default')

@section('content')

<!--fb slide partial view-->
@include('_partials.frontoffice.fb_slide_box')

<section id="columns" class="clearfix">
    <div class="container">
        <div class="row-fluid">

            <!-- Left -->

            <!-- Center -->
            <section id="center_column" class="span12">
                <div class="contenttop row-fluid">

                    <!-- Breadcrumb -->
                    <?php $current_page = "Kitchen Appliances"; ?>
                    @include('_partials.frontoffice.breadcrumb')
                    <!-- /Breadcrumb -->


                    <div class="content_scene_cat">
                        <!-- Category image -->
                        <div class="align_center"><img
                                src="{{asset('frontoffice/img/designer-island-hood/tea-maker-kettles.jpg')}}" alt=""
                                title="Glen Tea Maker &amp; Kettles" id="categoryImage"/></div>
                    </div>
                    <div class="chimney-category">
                        <p>There is no Indian evening without a cup of tea. To make tea brewing simple and fast, Glen India has
			 a range of Kettle and Tea makers. Designed for the perfect brew, this tea maker lets you experiment new
			 blends or enjoy the same old taste. All you need to do is move the stainless steel tea filter assembly up and down,
			 gently agitating the leaves to precisely infuse your tea. This electric kettle is made of glass and comes with detachable
			 filter assembly. Now you can enjoy perfect tea at the utmost convenience.</p>
</div>
                    <div class="row-fluid chimney-category-features">
                        <div class="span12 chimney-category-features-content">
                            <div class="row-fluid">

                                <div class="span3">

                                    <div class="row-fluid"><img class="span12 padding5"
                                                                src="{{asset('frontoffice/img/kettle-tea-maker/cordless.jpg')}}"/>
                                    </div>
                                    <div class="row-fluid">
                                        <p>360° Degree Rotational base</p>
                                    </div>
					<div class="row-fluid">
 						<p class="feature-description">The Kettle is a cordless appliance and its base can be swiveled
					 for whole 360 degrees. The rotating base makes it convenient to place the kettle anywhere on the base.</p>
					</div>

                                </div>


                                <div class="span3">

                                    <div class="row-fluid"><img class="span12 padding5"
                                                                src="{{asset('frontoffice/img/kettle-tea-maker/detach-filter.jpg')}}"/>
                                    </div>
                                    <div class="row-fluid">
                                        <p>Lockable hinged lid</p>
                                    </div>
					<div class="row-fluid">
					 <p class="feature-description"> The lid of the electric kettle is push fit and gets locked once pushed
					 inside. The lid is designed in such a way that the tea is poured in clean streams without any spillage.</p>
					</div>
                                </div>

                                <div class="span4">

                                    <div class="row-fluid"><img class="span12 padding5"
                                                                src="{{asset('frontoffice/img/kettle-tea-maker/cordless.jpg')}}"/>
                                    </div>
                                    <div class="row-fluid">
                                        <p>Water level gauge easy monitoring</p>
                                    </div>
					<div class="row-fluid">
 					<p class="feature-description"> The electric kettle comes with scale markings to measure the level
					 of water. It helps you prepare the exact serving volume, no extra and no less. You can measure volume
					 between 0.5L and 1L.</p>
						</div>
					</div>


                                <div class="span3">

                                    <div class="row-fluid"><img class="span12 padding5"
                                                                src="{{asset('frontoffice/img/kettle-tea-maker/stainless.jpg')}}"/>
                                    </div>
                                    <div class="row-fluid">
                                        <p>Comfortable & stylish handle</p>
                                    </div>
					<div class="row-fluid">
 						<p class="feature-description">The handle of the tea maker is designed to support your hand while you
					 pour or swirl the kettle jar. There is no spillage during pouring as the handle allows the right angle for
					 spillage proof pouring. The handle remains cool at elevated temperatures so that you can pour tea without any worry.</p>
					</div>
				  </div>
                            </div>

                        </div>

                    </div>


                    <a href="category-test.html" class="button_large pull-right margin-top10 margin-bottom10">View
                        Products </a>

                    <div class="clearfix"></div>
                    <hr/>


                    <!-- end div block_home -->

                </div>
        </div>
</section>


<!--feedback slide box partial-->
@include('_partials.frontoffice.feedback_slide_box')

@stop