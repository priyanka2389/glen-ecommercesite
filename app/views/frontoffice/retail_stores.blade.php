@extends('layouts.frontoffice.default')

@section('content')

<style type="text/css">

    .state_row {
        background-color: #e8e8e8;
        padding: 5px;
        text-align: center !important;
    }

    strong {
        font-family: Arial, Helvetica, sans-serif;
    }

</style>

<div class="container">
<div class="row">
    <div class="span12">
        <h1>Retail Stores</h1>
    </div>
</div>

<br/>

<div class="row justify">
    <div class="span12">
        <table cellspacing="10" cellpadding="0" width="100%" class="table">
            <tbody>
                <tr>
                    <td valign="middle" colspan="2" class="state_row"><strong>Location</strong></td>
                </tr>
                @foreach($retail_stores as $retail_store)
                <tr>
                    <td width="244"><strong>{{$retail_store->city or ''}}</strong></td>
                    <td width="297">{{$retail_store->address or ''}}, {{$retail_store->city or ''}},<br>
                         {{$retail_store->state or ''}}  @if($retail_store->pincode) - {{$retail_store->pincode or ''}} <br> @else @if($retail_store->state)<br> @endif @endif
                        @if($retail_store->phone)Ph: {{$retail_store->phone or ''}}<br> @endif  @if($retail_store->mobile) Mob: {{$retail_store->mobile or ''}}<br> @endif
                        @if($retail_store->email_id) EmailId: {{$retail_store->email_id or ''}}<br> @endif @if($retail_store->contact_person) Contact Person: {{$retail_store->contact_person or ''}}<br> @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<br/><br/>

</div>


@stop