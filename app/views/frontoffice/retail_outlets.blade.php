@extends('layouts.frontoffice.default')

@section('content')

{{--{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}--}}

{{--{{HTML::script('frontoffice/js/storelocator/handlebars-1.0.0.js')}}--}}
<script type="text/javascript"
        src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC3V1dEtD8JYeB3AuKv9z1EsDE2nyVDWg8&sensor=false"></script>

{{HTML::script('frontoffice/js/storelocator/jquery.storelocator.js')}}
{{HTML::script('frontoffice/js/dealers.js')}}
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"/>

<style type="text/css">
    .find_dealers {
        background-color: #17479d !important;
        padding: 0px 10px;
        border: none;
        font-size: 13px;
        color: #fff;
        line-height: 27px;
    }

    .retail-store-button-red {
        color: white !important;
    }

    #promotetop {
        height: 5px !important;
        margin-bottom: 15px !important;
    }

    .loader {
        padding-right: 5px;
        background: url({{URL::to('frontoffice/img/loader.gif')}}) no-repeat;
    }

    .shadow:hover {
        -moz-box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
        -webkit-box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
    }

    .shadow {
        border-bottom: solid 1px #e7e8ea;
        transition: all 0.5s;
        -webkit-transition: all .5s;

    }

    .address_box {
        margin-bottom: 20px;
        padding: 12px;
        text-align: left;
    }

    .scroll_gallery {
        max-height: 333px !important;
        overflow-y: auto !important;
        overflow-x: hidden;
    }

    .scroll_dealers {
        max-height: 269px !important;
        overflow-y: auto !important;
        overflow-x: hidden;
    }

    .map_height {
        height: 598px;
    }

    .address_box:hover {
        cursor: pointer;
    }

    .results {
        background-color: #17479d;
        text-align: center;
        color: white;
        padding-top: 10px
    }

    #map-canvas img {
        max-width: none
    }

    .gallery_div .results {
        background-color: #d21f45;
    }

    .popover {
        position: relative;
        top: 10px;
        left: 0;
        display: block;
        max-width: 292px;
        padding: 1px;
        text-align: left;
        background-color: #d21f45;
        border-radius: 0;
    }

    .popover.top .arrow {
        left: 19%;
    }

    .popover-title {
        background-color: #d21f45;
        border-bottom: none;
        color: #FFF;
    }

    .popover.top .arrow:after {
        border-top-color: #d21f45;
    }

    .row-fluid .span9:first-child {
        margin-left: 7px;
    }

    .tab-content {
        overflow: hidden;
    }

    .fa-map-marker {
        color: #d21f45;
    }
</style>

<!--<pre>--><?php //print_r($categories); ?><!--</pre>-->

<section id="columns" class="clearfix">
<div class="container">
<div class="row-fluid">

<div class="span12">
<h1>Dealer Locator</h1>

<div class="row-fluid">

<div class="span4 center-block"
     style="border: 3px solid;background-color: #ddd;min-height: 600px;">
    <div class="popover top">
        <div class="arrow"></div>
        <h3 class="popover-title" id="popover-top">Search Results
            <a class="anchorjs-link" href="#popover-top">
                <span class="anchorjs-icon"></span></a>
        </h3>
    </div>
    <br><br>

    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Retail Store</a>
            </li>
            <li role="presentation">
                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Dealer
                    Network</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">
                <form action="{{URL::to('dealers/ajax-galleries')}}" method="post" class="form">

                    <div class="row">
                        <div class="span11 offset1">
                            <div class="form-group span9">
                                {{--onchange="getCities(this.value, 'city', 'loader')"--}}
                                <select name="state" id="state" class="form-control state required"
                                        onchange="getCities(this, 'city', 'loader1');"
                                        data-item-type="retail_store">
                                    <option value="">Select State</option>
                                    @if(isset($rstates))
                                    @foreach($rstates as $state)
                                    <option value="{{$state->state}}">{{$state->state}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="span2">
                                <div class="loader hidden" id="loader1"
                                     style="color: #ccc;margin-top: 8px;">.
                                </div>
                            </div>
                        </div>
                        <div class="span12 offset1 hidden" id="stateError"></div>
                        <div class="clearfix"></div>

                        <div class="span11 offset1">
                            <div class="form-group span9" style="margin-left:0;">
                                <select name="city" id="city" class="form-control city required"
                                        data-item-type="retail_store"
                                        onchange="getLocations('state',this, 'location', 'loader2');">
                                    <option value="">Select City</option>
                                </select>
                            </div>
                            <div class="span2">
                                <div class="loader hidden" id="loader2"
                                     style="color: #ccc;margin-top: 8px;">.
                                </div>
                            </div>
                        </div>
                        <div class="span12 offset3 hidden" id="cityError" style="margin-left:32px;"></div>

                        <div class="span11 offset1">
                            <div class="form-group">
                                <select name="location" id="location"
                                        class="form-control location" >
                                    <option value="">Select Location</option>
                                </select>
                            </div>
                        </div>

                        <div class="span4 offset1">
                            <input type="hidden" name="rtype" class="type"
                                   value="retail_store"/>
                            <input type="submit" value="Find Dealers" id="find_dealers" class="find_dealers"/>
                        </div>

                    </div>
                </form>
                <div class="span11">
                    <div class="gallery_div complete_result_div">
                        {{--<h4 class="hidden results"> Exclusive Display Centers</h4>--}}
                        <br/>

                        <div class="scroll_gallery" id="gallery_list"></div>
                    </div>
                </div>
            </div>


            <div role="tabpanel" class="tab-pane" id="profile">
                <form action="{{URL::to('dealers/ajax-dealers')}}" method="post" class="form">

                    <div class="row">
                        <div class="span11 offset1">
                            <div class="form-group span9">
                                <select name="category" id="" class="form-control category required">
                                    <option value="">Select Category</option>
                                    @if(isset($categories))
                                    @foreach($categories as $category)
                                    @if($category->parent_category_id == null && $category->name!='Alda Cookware')
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endif
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="span2">
                                <div class="loader hidden" id="loader3"
                                     style="color: #ccc;margin-top: 8px;">.
                                </div>
                            </div>
                        </div>

                        <div class="span11 offset1">
                            <div class="form-group">
                                <select name="subcategory" id=""
                                        class="form-control subcategory required">
                                    <option value="">Select Subcatgeory</option>
                                </select>
                            </div>
                        </div>

                        <div class="span11 offset1">
                            <div class="form-group span9" style="margin-left:0;">
                                <select name="state1" id="state1" class="form-control state required"
                                        onchange="getCities(this, 'city1', 'loader4');"
                                        data-item-type="dealer">
                                    <option value="">Select State</option>
                                    @if(isset($dstates))
                                    @foreach($dstates as $state)
                                    <option value="{{$state->state}}">{{$state->state}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="span2">
                                <div class="loader hidden" id="loader4"
                                     style="color: #ccc;margin-top: 8px;">.
                                </div>
                            </div>
                        </div>

                        <div class="span11 offset1">
                            <div class="form-group span9" style="margin-left:0;">
                                <select name="city1" id="city1" class="form-control city required"
                                        data-item-type="dealer"
                                        onchange="getLocations('state1',this, 'location1', 'loader5');">
                                    <option value="">Select City</option>
                                </select>
                            </div>
                            <div class="span2">
                                <div class="loader hidden" id="loader5"
                                     style="color: #ccc;margin-top: 8px;">.
                                </div>
                            </div>
                        </div>

                        <div class="span11 offset1" style="margin-left:33px;">
                            <div class="form-group">
                                <select name="location1" id="location1"
                                        class="form-control location"
                                    >
                                    <option value="">Select Location</option>
                                </select>
                            </div>
                        </div>

                        <div class="span4 offset1">

                            <input type="hidden" name="dtype" class="type" value="dealer"/>
                            <input type="submit" value="Find Dealers" id="find_dealers1" class="find_dealers" />
                        </div>
                    </div>
                </form>
                <div class="span12">
                    <div class="dealers_div complete_result_div">
                        {{--<h4 class="hidden results"> Retail Outlets</h4>--}}
                        <br/>

                        <div class="scroll_dealers" id="dealers_list"></div>
                    </div>
                </div>
            </div>
            <br/>

            <div class="hidden offset1" id="loading">
                <h3>Loading dealers please wait....</h3>
            </div>

            <div class="span12" id="no_product" style="text-align: center"></div>
        </div>
    </div>
</div>
<div class="span8 map_height" id="map-canvas"></div>
</div>
</div>
</div>

{{--<div class="row-fluid default_text">--}}

    {{--<div class="span8 offset2">--}}
        {{--<p class="initialCopy" style="text-align: center">--}}
            {{--No matter where you are located on the map, there'll be an authorized GLEN INDIA--}}
            {{--retailer near you. Select your city or the city closest to you and hit the--}}
            {{--"Find Dealers" button. <br/> <b>Happy Shopping</b>!!--}}
            {{--</p>--}}
        {{--</div>--}}

    {{--<div class="clearfix"></div>--}}
    {{--<div class="span6 offset3" id="no_product" style="text-align: center"></div>--}}
    {{--</div>--}}


</div>
</section>

@stop