@extends('layouts.frontoffice.default')

@section('content')

<style type="text/css">

    th {
        color: #57575A;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
    }

    .gl-sub-menu2 a:link, .gl-sub-menu2 a:visited {
        color: #57575A;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        text-decoration: none;
    }

    .gl-sub-menu2 a:hover {
        color: #57575A;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        text-decoration: underline;
    }

    .no_border {
        border: none !important;
    }

</style>

<div class="container">
    <div class="row">
        <div class="span12">
            <h1>Downloads</h1>
        </div>
    </div>

    <br/><br/>

    <div class="row justify">

        <div class="span12">
            <table class="table" align="center" border="0" cellspacing="0" cellpadding="0">
                <tbody>


                <tr>
                    <td class="no_border">
                        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
                               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
                            <tbody>

                            <tr>
                                <td width="2" align="center"><img align="middle" width="16px" height="20px"
                                                                  src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{URL::to('catalogues')}}" >Catalogues</a></td>

                            </tr>
                            <tr>
                                <td width="2" align="center"><img align="middle" width="16px" height="20px"
                                                                  src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{URL::to('product-manuals')}}" >Product manual</a></td>

                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>


                </tbody>
            </table>
        </div>

    </div>

    <br/><br/>

</div>


@stop