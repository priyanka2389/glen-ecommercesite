@extends('layouts.frontoffice.default')
@section('content')

<script type="text/javascript"  src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var hidden_fields = $('#hidden_fields');
//        var enquiry_type = getUrlParameter('enquiry_type');
//        console.log(enquiry_type);

        $('#form').validate();
        var pageval = "{{$page}}";
        if(pageval){
            var html = $('#' + pageval).html();

            hidden_fields.html(html);
            $('#form input,textarea').removeAttr('disabled');
        }
        $('#enquiry_type').change(function () {
            $enquiry_type = $(this).val();
            var html = $('#' + $enquiry_type).html();
            hidden_fields.html(html);
            $('#form input,textarea').removeAttr('disabled');

        });

    });
</script>
<style type="text/css">
    strong{color: #17479d;}
    table{line-height: 21px;}
</style>


<section id="columns" class="clearfix">
    <div class="container">
        <div class="row-fluid">
            <section id="center_column" class="span12">
                <div class="contenttop row-fluid">
                    <h1 id="cart_title" class="title_category">Contact Us</h1>


                    <div class="row-fluid">
                        <div class="span12">
                            <table cellspacing="10" cellpadding="0" width="100%">
                                <tbody>
                                <tr>
                                    <td><strong>Head Office :</strong></td>
                                </tr>
                                <tr>
                                    <td><strong class="">Glen Appliances Pvt. Ltd.</strong></td>
                                </tr>
                                <tr>   <td>I - 34, DLF Industrial Area, Phase I, Faridabad</td>   </tr>
                                <tr>   <td>Tel. : 0129-2254191-93 , Fax : 0129-227 4344</td>   </tr>
                                <tr>   <td>Toll Free : 1800 180 1998</td>   </tr>
                                <tr>   <td><strong>Email : </strong>info@glenindia.com</td> </tr>
                                <tr>   <td><strong>Website : </strong>www.glenindia.com</td> </tr>
                                </tbody>
                            </table>
                            <br/><br/>

                            <div class="col-lg-12">
                                {{Notification::showSuccess()}}
                            </div>

                            <div class="col-lg-12">
                                {{Notification::showError()}}
                            </div>

                            <form action="{{URL::to('contact-us/data')}}" method="post"
                                  class="std form-horizontal" id="form" enctype="multipart/form-data">
                                <fieldset>
                                    <h3 class="title_content">Please fill in your details in the form given below:</h3>


                                    <div class="control-group text">
                                        <label for="name" class="control-label">Name<sup>*</sup></label>

                                        <div class="controls">
                                            <input type="text" name="name" class="required" id="name"
                                                   value="{{Input::old('name')}}">
                                            <span class="error">{{$errors->first('name')}}</span>
                                        </div>
                                    </div>

                                    <div class="control-group text">
                                        <label for="email" class="control-label">Email<sup>*</sup></label>

                                        <div class="controls">
                                            <input type="text" name="email" class="required email" id="email"
                                                   value="{{Input::old('email')}}">
                                            <span class="error">{{$errors->first('email')}}</span>
                                        </div>
                                    </div>

                                    <div class="control-group text">
                                        <label for="mobile" class="control-label">Mobile<sup>*</sup></label>

                                        <div class="controls">
                                            <input type="text" name="mobile" class="required digits" id="mobile"
                                                   value="{{Input::old('mobile')}}">
                                            <span class="error">{{$errors->first('mobile')}}</span>
                                        </div>
                                    </div>

                                    <div class="control-group text">
                                        <label for="enquiry_type" class="control-label">Enquiry Type<sup>*</sup></label>

                                        <div class="controls">
                                            <select name="enquiry_type" id="enquiry_type" class="required">
                                                <option value="">--Select Enquiry Type--</option>
                                                <option value="bulk_purchase" @if(isset($page) && ($page=='bulk_purchase')) selected @endif>Distribution / Gallery Request</option>
                                                <option value="career" @if(isset($page) && ($page=='career')) selected @endif>Careers</option>
                                                <option value="service" @if(isset($page) && ($page=='service')) selected @endif>Service Request</option>
                                                <option value="trade_partner" @if(isset($page) && ($page=='trade_partner')) selected @endif>Trade Partner</option>
                                                <option value="product_info" @if(isset($page) && ($page=='product_info')) selected @endif>Product Enquiry</option>
												<option value="institutional_purchase" @if(isset($page) && ($page=='institutional_purchase'))
                                                            selected @endif>Institutional Purchase
                                                </option>
                                            </select>
                                            <!--                                            <select name="enquiry_type" id="enquiry_type" class="required">-->
                                            <!--                                                <option value="">--Select Enquiry Type--</option>-->
                                            <!--                                                <option value="bulk_purchase">Institutional/Bulk Purchase</option>-->
                                            <!--                                                <option value="career">Careers</option>-->
                                            <!--                                                <option value="service">Services</option>-->
                                            <!--                                                <option value="trade_partner">Trade Partner</option>-->
                                            <!--                                                <option value="product_info">Product Information</option>-->
                                            <!--                                            </select>-->
                                        </div>
                                    </div>

                                    <div id="hidden_fields">

                                    </div>

                                    <div class="captcha">

                                    </div>

                                    <div class="control-group submit2">
                                        <div class="controls">
                                            <input type="submit" class="submit exclusive standard-checkout"
                                                   value="Submit"/>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>

                    </div>
                </div>
                <!-- end div block_home -->
            </section>
        </div>
    </div>
</section>

<!--bulk purchase hidden form-->
<div class="hidden" id="bulk_purchase">

    <div class="control-group text">
        <label for="company_name" class="control-label">Company Name<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="company_name" class="required" id="company_name"
                   value="{{Input::old('company_name')}}" disabled>
            <span class="error">{{$errors->first('company_name')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="country" class="control-label">Country<sup>*</sup></label>

        <div class="controls">
            <select name="country" id="country" class="required">
                <option value="">--Select Country--</option>
                <option>India</option>
            </select>
            <span class="error">{{$errors->first('country')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="state" class="control-label">State<sup>*</sup></label>

        <div class="controls">
            <select name="state" id="state" class="required">
                <option value="">-- Select State --</option>
                <option value="Andhra Pradesh">Andhra Pradesh</option>
                <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                <option value="Assam">Assam</option>
                <option value="Bihar">Bihar</option>
                <option value="Chhattīsgarh">Chhattīsgarh</option>
                <option value="Goa">Goa</option>
                <option value="Gujarat">Gujarat</option>
                <option value="Haryana">Haryana</option>
                <option value="Himachal Pradesh">Himachal Pradesh</option>
                <option value="Jammu and Kashmīr">Jammu and Kashmīr</option>
                <option value="Jharkhand">Jharkhand</option>
                <option value="Karnataka">Karnataka</option>
                <option value="Kerala">Kerala</option>
                <option value="Madhya Pradesh">Madhya Pradesh</option>
                <option value="Maharashtra">Maharashtra</option>
                <option value="Manipur">Manipur</option>
                <option value="Meghalaya">Meghalaya</option>
                <option value="Mizoram">Mizoram</option>
                <option value="Nagaland">Nagaland</option>
                <option value="Orissa">Orissa</option>
                <option value="Punjab">Punjab</option>
                <option value="Rajasthan">Rajasthan</option>
                <option value="Sikkim">Sikkim</option>
                <option value="Tamil Nadu">Tamil Nadu</option>
                <option value="Tripura">Tripura</option>
                <option value="Uttaranchal">Uttaranchal</option>
                <option value="Uttar Pradesh">Uttar Pradesh</option>
                <option value="West Bengal">West Bengal</option>
                <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                <option value="Chandīgarh">Chandīgarh</option>
                <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                <option value="Daman and Diu">Daman and Diu</option>
                <option value="Delhi">Delhi</option>
                <option value="Lakshadweep">Lakshadweep</option>
                <option value="Pondicherry">Pondicherry</option>
            </select   class="required">
            <span class="error">{{$errors->first('state')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="city" class="control-label">City<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="city" id="city" value="{{Input::old('city')}}" disabled>
            <span class="error">{{$errors->first('city')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="requirement" class="control-label">Requirement<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="requirement" class="required" value="{{Input::old('requirement')}}"
                   id="requirement" disabled>
            <span class="error">{{$errors->first('requirement')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="message" class="control-label">Message<sup>*</sup></label>

        <div class="controls">
            <textarea name="message" cols="30" rows="4" id="message" class="required"
                      disabled>{{Input::old('message')}}</textarea>
            <span class="error">{{$errors->first('message')}}</span>
        </div>
    </div>

    {{--<div class="control-group text">--}}
        {{--<label for="captcha" class="control-label">Captcha<sup>*</sup></label>--}}

        {{--<div class="controls">--}}
            {{--<img src="{{Captcha::img()}}" alt="Captcha Img"/><br/>--}}
            {{--<input type="text" name="captcha" class="required" id="captcha" style="margin-top: 10px" disabled/>--}}
            {{--<span class="error">{{$errors->first('captcha')}}</span>--}}
        {{--</div>--}}
    {{--</div>--}}

</div>

<!--career hidden form-->
<div class="hidden" id="career">

    <div class="control-group text">
        <label for="address" class="control-label">Address<sup>*</sup></label>

        <div class="controls">
            <textarea name="address" cols="30" rows="4" id="address" class="address" disabled>{{Input::old('address')}}</textarea>
            <span class="error">{{$errors->first('address')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="dob" class="control-label">Date Of Birth<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="dob" class="required date" id="dob" value="{{Input::old('dob')}}" disabled>
            <span class="error">{{$errors->first('dob')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="applying_department" class="control-label">Applying Department<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="applying_department" class="required" id="applying_department" value="{{Input::old('applying_department')}}" disabled>
            <span class="error">{{$errors->first('applying_department')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="educational_qualification" class="control-label">Educational Qualifications<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="educational_qualification" class="required" id="educational_qualification"
                   value="{{Input::old('educational_qualification')}}" disabled >
            <span class="error">{{$errors->first('educational_qualification')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="professional_qualification" class="control-label">Professional Qualifications<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="professional_qualification" class="required" id="professional_qualification"
                   value="{{Input::old('professional_qualification')}}" disabled >
            <span class="error">{{$errors->first('professional_qualification')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="primary_skill" class="control-label">Primary Skill<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="primary_skill" class="required" id="primary_skill" value="{{Input::old('primary_skill')}}" disabled>
            <span class="error">{{$errors->first('primary_skill')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="career_highlights" class="control-label">Career Highlights<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="career_highlights" class="required" id="career_highlights" value="{{Input::old('career_highlights')}}" disabled>
            <span class="error">{{$errors->first('career_highlights')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="work_exp" class="control-label">Work Experience<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="work_exp" class="required" id="work_exp" value="{{Input::old('work_exp')}}" disabled>
            <span class="error">{{$errors->first('work_exp')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="resume" class="control-label">Resume<sup>*</sup><br>(doc,txt,docx,ppt,pdf)</label>

        <div class="controls">
            <input type="file" name="resume" class="required" id="resume" disabled>
            <span class="error">{{$errors->first('resume')}}</span>
        </div>
    </div>

    {{--<div class="control-group text">--}}
        {{--<label for="captcha" class="control-label">Captcha<sup>*</sup></label>--}}

        {{--<div class="controls">--}}
            {{--<img src="{{Captcha::img()}}" alt="Captcha Img"/><br/>--}}
            {{--<input type="text" name="captcha" class="required" id="captcha" style="margin-top: 10px" disabled/>--}}
            {{--<span class="error">{{$errors->first('captcha')}}</span>--}}
        {{--</div>--}}
    {{--</div>--}}

</div>

<!--service hidden form-->
<div class="hidden" id="service">

    <div class="control-group text">
        <label for="address" class="control-label">Address<sup>*</sup></label>

        <div class="controls">
            <textarea name="address" cols="30" rows="4" id="address" class="address">{{Input::old('address')}}</textarea>
            <span class="error">{{$errors->first('address')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="product" class="control-label">Product<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="product" class="required" id="product" value="{{Input::old('product')}}" disabled>
            <span class="error">{{$errors->first('product')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="bill_no" class="control-label">Bill No.<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="bill_no" class="required" id="bill_no" value="{{Input::old('bill_no')}}" disabled>
            <span class="error">{{$errors->first('bill_no')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="bill_date" class="control-label">Bill Date<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="bill_date" class="required date" id="bill_date" value="{{Input::old('bill_date')}}" disabled>
            <span class="error">{{$errors->first('bill_date')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="shop_name" class="control-label">Shop Name<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="shop_name" class="required" id="shop_name" value="{{Input::old('shop_name')}}" disabled>
            <span class="error">{{$errors->first('shop_name')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="message" class="control-label">Message<sup>*</sup></label>

        <div class="controls">
            <textarea name="message" cols="30" rows="4" id="message" class="required" disabled>{{Input::old('message')}}</textarea>
            <span class="error">{{$errors->first('message')}}</span>
        </div>
    </div>

    {{--<div class="control-group text">--}}
        {{--<label for="captcha" class="control-label">Captcha<sup>*</sup></label>--}}

        {{--<div class="controls">--}}
            {{--<img src="{{Captcha::img()}}" alt="Captcha Img"/><br/>--}}
            {{--<input type="text" name="captcha" class="required" id="captcha" style="margin-top: 10px" disabled/>--}}
            {{--<span class="error">{{$errors->first('captcha')}}</span>--}}
        {{--</div>--}}
    {{--</div>--}}

</div>

<!--trade partner form-->
<div class="hidden" id="trade_partner">

<!--    <div class="control-group text">-->
<!--        <label for="state" class="control-label">State<sup>*</sup></label>-->
<!---->
<!--        <div class="controls">-->
<!--            <input type="text" name="state" class="required" id="state" disabled>-->
<!--            <span class="error">{{$errors->first('state')}}</span>-->
<!--        </div>-->
<!--    </div>-->

    <div class="control-group text">
        <label for="country" class="control-label">Country<sup>*</sup></label>

        <div class="controls">
            <select name="country" id="country" class="required">
                <option value="">--Select Country--</option>
                <option>India</option>
            </select>
            <span class="error">{{$errors->first('country')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="state" class="control-label">State<sup>*</sup></label>

        <div class="controls">
            <select name="state" id="state" class="required">
                <option value="">-- Select State --</option>
                <option value="Andhra Pradesh">Andhra Pradesh</option>
                <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                <option value="Assam">Assam</option>
                <option value="Bihar">Bihar</option>
                <option value="Chhattīsgarh">Chhattīsgarh</option>
                <option value="Goa">Goa</option>
                <option value="Gujarat">Gujarat</option>
                <option value="Haryana">Haryana</option>
                <option value="Himachal Pradesh">Himachal Pradesh</option>
                <option value="Jammu and Kashmīr">Jammu and Kashmīr</option>
                <option value="Jharkhand">Jharkhand</option>
                <option value="Karnataka">Karnataka</option>
                <option value="Kerala">Kerala</option>
                <option value="Madhya Pradesh">Madhya Pradesh</option>
                <option value="Maharashtra">Maharashtra</option>
                <option value="Manipur">Manipur</option>
                <option value="Meghalaya">Meghalaya</option>
                <option value="Mizoram">Mizoram</option>
                <option value="Nagaland">Nagaland</option>
                <option value="Orissa">Orissa</option>
                <option value="Punjab">Punjab</option>
                <option value="Rajasthan">Rajasthan</option>
                <option value="Sikkim">Sikkim</option>
                <option value="Tamil Nadu">Tamil Nadu</option>
                <option value="Tripura">Tripura</option>
                <option value="Uttaranchal">Uttaranchal</option>
                <option value="Uttar Pradesh">Uttar Pradesh</option>
                <option value="West Bengal">West Bengal</option>
                <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                <option value="Chandīgarh">Chandīgarh</option>
                <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                <option value="Daman and Diu">Daman and Diu</option>
                <option value="Delhi">Delhi</option>
                <option value="Lakshadweep">Lakshadweep</option>
                <option value="Pondicherry">Pondicherry</option>
            </select   class="required">
            <span class="error">{{$errors->first('state')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="city" class="control-label">City<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="city" class="required" id="city" value="{{Input::old('city')}}" disabled>
            <span class="error">{{$errors->first('city')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="interest" class="control-label">You are interested in ?<sup>*</sup></label>

        <div class="controls">
            <select name="interest" id="interest">
                <option value="small_appliances">Small Appliances</option>
                <option value="large_appliances">Large Appliances</option>
            </select>
            <span class="error">{{$errors->first('interest')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="purpose" class="control-label">What is your purpose ?<sup>*</sup></label>

        <div class="controls">
            <select name="purpose" id="purpose">
                <option value="gallery">Gallery</option>
                <option value="dealer">Dealer</option>
                <option value="distributor">Distributor</option>
            </select>
            <span class="error">{{$errors->first('purpose')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="message" class="control-label">Message</label>

        <div class="controls">
            <textarea name="message" cols="30" rows="4" id="message" class="message" disabled>{{Input::old('message')}}</textarea>
        </div>
    </div>

    {{--<div class="control-group text">--}}
        {{--<label for="captcha" class="control-label">Captcha<sup>*</sup></label>--}}

        {{--<div class="controls">--}}
            {{--<img src="{{Captcha::img()}}" alt="Captcha Img"/><br/>--}}
            {{--<input type="text" name="captcha" class="required" id="captcha" style="margin-top: 10px" disabled/>--}}
            {{--<span class="error">{{$errors->first('captcha')}}</span>--}}
        {{--</div>--}}
    {{--</div>--}}


</div>

<!--product info form-->
<div class="hidden" id="product_info">
    <div class="control-group text">
        <label for="country" class="control-label">Country<sup>*</sup></label>

        <div class="controls">
            <select name="country" id="country" class="required">
                <option value="">--Select Country--</option>
                <option>India</option>
            </select>
            <span class="error">{{$errors->first('country')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="state" class="control-label">State<sup>*</sup></label>

        <div class="controls">
            <select name="state" id="state" class="required">
                <option value="">-- Select State --</option>
                <option value="Andhra Pradesh">Andhra Pradesh</option>
                <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                <option value="Assam">Assam</option>
                <option value="Bihar">Bihar</option>
                <option value="Chhattīsgarh">Chhattīsgarh</option>
                <option value="Goa">Goa</option>
                <option value="Gujarat">Gujarat</option>
                <option value="Haryana">Haryana</option>
                <option value="Himachal Pradesh">Himachal Pradesh</option>
                <option value="Jammu and Kashmīr">Jammu and Kashmīr</option>
                <option value="Jharkhand">Jharkhand</option>
                <option value="Karnataka">Karnataka</option>
                <option value="Kerala">Kerala</option>
                <option value="Madhya Pradesh">Madhya Pradesh</option>
                <option value="Maharashtra">Maharashtra</option>
                <option value="Manipur">Manipur</option>
                <option value="Meghalaya">Meghalaya</option>
                <option value="Mizoram">Mizoram</option>
                <option value="Nagaland">Nagaland</option>
                <option value="Orissa">Orissa</option>
                <option value="Punjab">Punjab</option>
                <option value="Rajasthan">Rajasthan</option>
                <option value="Sikkim">Sikkim</option>
                <option value="Tamil Nadu">Tamil Nadu</option>
                <option value="Tripura">Tripura</option>
                <option value="Uttaranchal">Uttaranchal</option>
                <option value="Uttar Pradesh">Uttar Pradesh</option>
                <option value="West Bengal">West Bengal</option>
                <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                <option value="Chandīgarh">Chandīgarh</option>
                <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                <option value="Daman and Diu">Daman and Diu</option>
                <option value="Delhi">Delhi</option>
                <option value="Lakshadweep">Lakshadweep</option>
                <option value="Pondicherry">Pondicherry</option>
            </select   class="required">
            <span class="error">{{$errors->first('state')}}</span>
        </div>
    </div>

    <div class="control-group text">
        <label for="city" class="control-label">City<sup>*</sup></label>

        <div class="controls">
            <input type="text" name="city" class="required" id="city" disabled>
            <span class="error">{{$errors->first('city')}}</span>
        </div>
    </div>
    <div class="control-group text">
        <label for="interest" class="control-label">You are looking for?<sup>*</sup></label>

        <div class="controls">
            <select name="interest" id="interest">
                <option value="small_appliances">Small Appliances</option>
                <option value="large_appliances">Large Appliances</option>
            </select>
            <span class="error">{{$errors->first('')}}</span>
        </div>
    </div>
    <div class="control-group text">
        <label for="message" class="control-label">How can we help you?<sup>*</sup></label>

        <div class="controls">
            <textarea name="message" cols="30" rows="4" id="message" class="message" disabled>{{Input::old('message')}}</textarea>
        </div>
    </div>

    {{--<div class="control-group text">--}}
        {{--<label for="captcha" class="control-label">Captcha<sup>*</sup></label>--}}

        {{--<div class="controls">--}}
            {{--<img src="{{Captcha::img()}}" alt="Captcha Img"/><br/>--}}
            {{--<input type="text" name="captcha" class="required" id="captcha" style="margin-top: 10px" disabled/>--}}
            {{--<span class="error">{{$errors->first('captcha')}}</span>--}}
        {{--</div>--}}
    {{--</div>--}}

</div>

<div class="hidden" id="institutional_purchase">

        <div class="control-group text">
            <label for="company_name" class="control-label">Company Name<sup>*</sup></label>

            <div class="controls">
                <input type="text" name="company_name" class="required" id="company_name"
                       value="{{Input::old('company_name')}}" disabled>
                <span class="error">{{$errors->first('company_name')}}</span>
            </div>
        </div>

        <div class="control-group text">
            <label for="country" class="control-label">Country<sup>*</sup></label>

            <div class="controls">
                <select name="country" id="country" class="required">
                    <option value="">--Select Country--</option>
                    <option>India</option>
                </select>
                <span class="error">{{$errors->first('country')}}</span>
            </div>
        </div>

        <div class="control-group text">
            <label for="state" class="control-label">State<sup>*</sup></label>

            <div class="controls">
                <select name="state" id="state" class="required">
                    <option value="">-- Select State --</option>
                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                    <option value="Assam">Assam</option>
                    <option value="Bihar">Bihar</option>
                    <option value="Chhattīsgarh">Chhattīsgarh</option>
                    <option value="Goa">Goa</option>
                    <option value="Gujarat">Gujarat</option>
                    <option value="Haryana">Haryana</option>
                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                    <option value="Jammu and Kashmīr">Jammu and Kashmīr</option>
                    <option value="Jharkhand">Jharkhand</option>
                    <option value="Karnataka">Karnataka</option>
                    <option value="Kerala">Kerala</option>
                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                    <option value="Maharashtra">Maharashtra</option>
                    <option value="Manipur">Manipur</option>
                    <option value="Meghalaya">Meghalaya</option>
                    <option value="Mizoram">Mizoram</option>
                    <option value="Nagaland">Nagaland</option>
                    <option value="Orissa">Orissa</option>
                    <option value="Punjab">Punjab</option>
                    <option value="Rajasthan">Rajasthan</option>
                    <option value="Sikkim">Sikkim</option>
                    <option value="Tamil Nadu">Tamil Nadu</option>
                    <option value="Tripura">Tripura</option>
                    <option value="Uttaranchal">Uttaranchal</option>
                    <option value="Uttar Pradesh">Uttar Pradesh</option>
                    <option value="West Bengal">West Bengal</option>
                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                    <option value="Chandīgarh">Chandīgarh</option>
                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                    <option value="Daman and Diu">Daman and Diu</option>
                    <option value="Delhi">Delhi</option>
                    <option value="Lakshadweep">Lakshadweep</option>
                    <option value="Pondicherry">Pondicherry</option>
                </select   class="required">
                <span class="error">{{$errors->first('state')}}</span>
            </div>
        </div>

        <div class="control-group text">
            <label for="city" class="control-label">City<sup>*</sup></label>

            <div class="controls">
                <input type="text" name="city" id="city" value="{{Input::old('city')}}" disabled>
                <span class="error">{{$errors->first('city')}}</span>
            </div>
        </div>

        <div class="control-group text">
            <label for="requirement" class="control-label">Requirement<sup>*</sup></label>

            <div class="controls">
                <input type="text" name="requirement" class="required" value="{{Input::old('requirement')}}"
                       id="requirement" disabled>
                <span class="error">{{$errors->first('requirement')}}</span>
            </div>
        </div>

        <div class="control-group text">
            <label for="message" class="control-label">Message<sup>*</sup></label>

            <div class="controls">
            <textarea name="message" cols="30" rows="4" id="message" class="required"
                      disabled>{{Input::old('message')}}</textarea>
                <span class="error">{{$errors->first('message')}}</span>
            </div>
        </div>

        {{--<div class="control-group text">--}}
        {{--<label for="captcha" class="control-label">Captcha<sup>*</sup></label>--}}

        {{--<div class="controls">--}}
        {{--<img src="{{Captcha::img()}}" alt="Captcha Img"/><br/>--}}
        {{--<input type="text" name="captcha" class="required" id="captcha" style="margin-top: 10px" disabled/>--}}
        {{--<span class="error">{{$errors->first('captcha')}}</span>--}}
        {{--</div>--}}
        {{--</div>--}}

    </div>
@stop