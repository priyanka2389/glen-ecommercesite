@extends('layouts.frontoffice.default')

@section('content')

    {{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
    {{HTML::script('frontoffice/js/cart_modal.js')}}

    <script type="text/javascript">

        $(document).ready(function () {

            var site_url = $.url();
            var host = site_url.attr('host');
            var id_url_param = site_url.param('id');


//        triggers when the type of product is changed
            $('.type').change(function () {
                $category_id = $(this).val();
                if ($category_id == "") {
                    $('.products').html(" ");
                    return false;
                }

                $url = "<?php echo URL::to('product/ajax-products/') ?>";
                $url = $url + '/' + $category_id;

                $('.ajax_loader').removeClass('hidden');
                $('.products').attr("disabled", "disabled");

                var products_request = $.get($url, function (data) {
                    $('.products').html(" ");
                    if (data) {
                        var option = "<option value='select'>-Select Product-</option>";
                        var flag = false;

                        $(data).each(function (key, value) {
                            if (id_url_param.indexOf(value['id']) == -1) {
                                option += "<option value=" + value['id'] + ">" + value['name'] + "</option>";
                                flag = true;
                            }

//                        var option = $('<option></option>');
//                        option.attr('value', value['id']);
//                        option.html(value['name']);
//                        $('.products').append(option);
                        });
                        if (flag == false) {
                            $('.products').html('<option>No products found</option>');
                            $('.products').attr("disabled", "disabled");
                            return false;
                        } else {
                            $('.products').html(option);
                        }

                    }
                });

                products_request.success(function () {
                    $('.products').removeAttr("disabled");
                    $('.ajax_loader').addClass("hidden");
                });
            });

//        triggers when the product is selected
            $('.products').live('change', function () {

                var site_url = base_url;
                var product_id = $(this).val();
                window.location.href = site_url + '/product/compare?id=' + id_url_param + ',' + product_id;
            });


        });

    </script>

    <style type="text/css">
        .retail-store-button-red {
            color: white !important;
        }

        #promotetop {
            height: 5px !important;
            margin-bottom: 15px !important;
        }

        a.product_image {
            margin: -1px 0 0 8px;
        }
    </style>


    <?php $total_products = sizeof($products['product_info']);
    $attributes_info = isset($products['attribute_info']) ? $products['attribute_info'] : null;
    $products_info = isset($products['product_info']) ? $products['product_info'] : null;

    ?>

    <?php $url_ids = Input::get('id'); ?>



    <section id="columns" class="clearfix">
        <div class="container">
            <div class="row-fluid">
                <section id="center_column" class="span12">
                    <div class="contenttop row-fluid">
                        <h1>Product Comparision</h1>

                        <div class="authentication-page row-fluid">
                            <div class="span12">
                                <div class="products_block">
                                    <table id="product_comparison">
                                        <tbody>

                                        <!-- image row starts here-->

                                        @if(isset($products_info))
                                            <tr>
                                                <td width="20%" class="td_empty"></td>

                                                @foreach($products_info as $p_info)
                                                    <?php $id = $p_info['id'];

                                                    ?>

                                                    <td width="26%" class="ajax_block_product comparison_infos">
                                                        <div class="product-container">
                                                            @if($total_products>1)
                                                                <div class="row">
                                                                    <a href="{{URL::to('product/remove-compare-id?id='.$url_ids.'&remove='.$id)}}">
                                                                        <img class="product-camparision-close"
                                                                             src="{{asset('frontoffice/img/close.png')}}">
                                                                    </a>
                                                                </div>

                                                                @endif
                                                                        <!--                                                <div class="row">-->
                                                                {{--<a href="#" title="" class="product_image">--}}
                                                                <?php $images = $p_info['image']; ?>
                                                                <?php $image = HtmlUtil::getPrimaryImage($images);
                                                                $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE;
                                                                ?>
                                                                <img class="product_image" src="{{URL::to($path)}}"
                                                                     width="300" height="300">
                                                                {{--</a>--}}
                                                                <!--                                                </div>-->

                                                                <h3 class="s_title_block text-center">
                                                                    <a href="javascript:void(0);">{{$p_info['name']}}</a>
                                                                </h3>
                                                                <div class="span5">
                                                                    <?php $is_upcoming_product = false; ?>
                                                                    @if(isset($product_tags))
                                                                        @for($i=0; $i<count($product_tags); $i++)
                                                                            <?php $product = $product_tags[$i]; ?>
                                                                            @if($product->id == $p_info['id'])
                                                                                <?php $tags = $product->tags; ?>
                                                                                @for($k=0;$k < count($tags);$k++)
                                                                                    @if($tags[$k]->name == 'upcoming_products')
                                                                                        <?php $is_upcoming_product = true; ?>
                                                                                    @endif
                                                                                @endfor
                                                                            @endif
                                                                        @endfor
                                                                    @endif

                                                                    @if($is_upcoming_product == true)
                                                                        <label class="retail-store-button-red disabled"
                                                                           href="#" data-id="{{$id}}"> Coming soon</label>

                                                                    @elseif($p_info['availability'] == 0)
                                                                        <label class="retail-store-button-red disabled" > Out of stock</label>
                                                                    @else
                                                                        <input type="hidden" data-item-type="product"
                                                                               data-id="{{$id}}" name="qty"
                                                                               id="quantity_wanted"
                                                                               class="text qty_{{$id}}" value="1"
                                                                               size="2">
                                                                        <a class="lnk_more retail-store-button-red buy_online"
                                                                           href="#" data-id="{{$id}}"> Buy Online</a>
                                                                    @endif

                                                                </div>
                                                                <div class="span5" style="float:right"
                                                                     class="retail_stores"><a class="lnk_more exclusive"
                                                                                              href="{{URL::to('/retail-outlets')}}"
                                                                                              title="View">Retail
                                                                        Stores</a></div>
                                                                <div class="clearfix"></div>

                                                                <div class="comparison_product_infos">
                                                                    <?php $offer_price = $p_info['offer_price'];
                                                                    $list_price = $p_info['list_price'];
                                                                    ?>
                                                                    @if(isset($offer_price) && $offer_price>0 && $list_price>0  && $list_price != $offer_price && $list_price > $offer_price)
                                                                        <?php $strike_through = "price-line-through";
                                                                        $price_font = "font-14px";
                                                                        $offer_font = "font-17px pull-right price";
                                                                        $discount = ($list_price - $offer_price) / $list_price * 100;
                                                                        $discount_label = "(" . round($discount) . "% OFF)";
                                                                        ?>
                                                                    @else
                                                                        <?php $strike_through = "";
                                                                        $offer_font = "";
                                                                        $price_font = "font-17px price";
                                                                        $discount = 0;
                                                                        ?>
                                                                    @endif
                                                                    <div class="prices_container">
                                                                        <p class="price_container {{$price_font}}"
                                                                           style="padding:5px;">
                                                                            <span class=" {{$price_font}} {{$strike_through}}">
                                                                                <span class="WebRupee {{$strike_through}}"> Rs. </span>
                                                                                {{number_format($list_price)}}
                                                                            </span>

                                                                            @if(!empty($discount)) {{$discount_label}}  @endif
                                                                            @if($list_price != $offer_price && $list_price > $offer_price && $offer_price>0 && $list_price>0)
                                                                                <span class="{{$offer_font}}"
                                                                                      style="float:right;"><span
                                                                                            class="WebRupee"> Rs. </span>{{number_format($offer_price)}}</span>
                                                                            @endif
                                                                        </p>

                                                                        {{--<p class="price_container text-center margin-top-20px">--}}

                                                                        {{--<span class="span5 price {{$strike_through}}">--}}
                                                                        {{--<span class="WebRupee"> Rs. </span>{{$p_info['list_price']}}</span>--}}

                                                                        {{--@if(isset($offer_price) && $offer_price != 0.00)--}}
                                                                        {{--<span class="span5 price price-gray"--}}
                                                                        {{--style="float:right;">--}}
                                                                        {{--<span class="WebRupee"> Rs. </span>{{$p_info['offer_price']}}</span>--}}
                                                                        {{--@endif--}}
                                                                        {{--</p>--}}

                                                                        <div class="product_discount"></div>
                                                                        &nbsp; </div>
                                                                    <!-- availability -->
                                                                    <p class="comparison_availability_statut"></p>

                                                                </div>
                                                        </div>
                                                    </td>
                                                @endforeach


                                                @if($total_products<3)
                                                    <td width="26%" class="ajax_block_product comparison_infos">
                                                        <div class="product-container">
                                                            <h4 class="margin-bottom10 margin-top10 text-center">Add to
                                                                Compare</h4>

                                                            <?php $margin_left = ($total_products == 1) ? 'margin_left_20' : ''; ?>
                                                            <div class="padding10">
                                                                <span class="span4" style="padding-bottom:5px;">Select Type</span>
                                                                {{--{{$margin_left}}--}}
                                                                <select class=" type">
                                                                    <option value="">Select</option>

                                                                    @if(isset($product_category))

                                                                        <option value="{{$product_category->category->id}}">
                                                                            {{$product_category->category->name}}
                                                                        </option>

                                                                    @endif

                                                                </select>
                                                            </div>

                                                            <div class="padding10 margin-top10">
                                                                <span class="span5" style="padding-bottom:5px;">Select Product</span>
                                                                <select class="products">


                                                                </select>
                                                    <span class="hidden ajax_loader">
                                                        <img src="{{asset('frontoffice/img/ajax-loader.gif')}}"
                                                             alt=""/> Loading products...</span>
                                                            </div>

                                                            <!--<div class="product_desc"><a href="#"> MacBook Air is ultrathin, ultraportable, and ultra...</a></div>-->

                                                            <div class="comparison_product_infos">
                                                                <div class="prices_container">
                                                                    <!--<p class="price_container"><span class="price">$1,504.18</span></p>-->
                                                                    <div class="product_discount"></div>
                                                                    &nbsp; </div>
                                                                <!-- availability -->
                                                                <p class="comparison_availability_statut"></p>

                                                                <!--<span class="exclusive">Add to cart</span>--> </div>
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                            @endif

                                                    <!--image row ends here-->

                                            <!-- blank first row starts here-->

                                            <tr class="comparison_header">
                                                <td> Features:</td>

                                                @foreach($products_info as $p_info)
                                                    <td></td>
                                                @endforeach

                                                @if($total_products<3)
                                                    <td></td>
                                                @endif
                                            </tr>

                                            <!-- blank first row ends here-->

                                            <!--  main feature rows starts here-->

                                            @if(isset($attributes_info))

                                                @foreach($attributes_info as $key=>$value)
                                                    <tr>

                                                        <td>{{$key}}</td>

                                                        @for($i=0;$i<$total_products;$i++)

                                                            <td>@if(isset($value[$i]['value'])) {{$value[$i]['value']}} @else
                                                                    - @endif</td>

                                                        @endfor

                                                        @if($total_products<3)
                                                            <td>&nbsp;</td>
                                                        @endif
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                            <!-- main feature rows ends here-->

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end div block_home -->
                </section>
            </div>
        </div>
    </section>

@stop