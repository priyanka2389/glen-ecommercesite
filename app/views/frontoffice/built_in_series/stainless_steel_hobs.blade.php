@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
 @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/built-in-series/steel-hob/ss-hob.png')}}">
                    </div>
                    <div class="span7">
                        <!--                        margin-top80-->
                        <h1 class="top-header-heading ">Stainless Steel Hobs</h1>

                        <p class="top-header-p ">Glen's stainless steel hobs integrate outstanding aesthetics with efficient capability in cooking.
			 Their minimalistic exteriors are backed with the promise of high performance cooking. With Matt finish Stainless Steel body,
			 easy to operate knobs and easily removable pan supports, cleaning of these cooking hobs never becomes a burden. A profusion
			 of burner arrangement gives complete control. Moreover, you have the choice of flames to tailor in accord with the vessel
			 or the dish that you are cooking.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">

            <div class="">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/steel-hob/Triple-Ring-Burner.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Triple Ring Burner</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Glen's hobs for kitchen with triple ring burners offer you the perks of perfect rings of
			 flame. With extra energy and high efficiency you can make masterpieces of Indian cuisine. It is especially suitable for deep
			 frying and tempering.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/steel-hob/Italian-Gas-Valves.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Italian Gas Valve</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Your safety is paramount to Glen. We have integrated high precision Italian gas valves
			 for total safety and perfect flame control. So you can focus on cooking and leave safety concerns to us.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/steel-hob/Integrated-Auto-Ignition.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Integrated Auto Ignition</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Stainless steel gas hob with integrated auto ignition allows you to ignite the flame by just
			 turning on the knob so you don't have to look for matchsticks/lighters.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/steel-hob/Cast-Iron-Pan-Support.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Cast Iron Pan Support</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Option of Cast Iron pan supports provide robust and sturdy hold, to easily accommodate
			 large and heavy vessels. 4.0 MM thick MS Pan Supports also available in some models.</p>
                    </div>
                </div>
            </div>

        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop