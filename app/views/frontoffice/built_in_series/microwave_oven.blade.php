@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
 @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>
<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img
                            src="{{asset('frontoffice/img/built-in-series/microwave/Built-in-Microwave-banner.png')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Built-In Microwaves</h1>

                        <p class="top-header-p ">Glen's built in microwave ovens are specially designed to stylize your modern kitchen. The
microwaves provide for all your culinary needs of heating, roasting, cooking or baking while saving your counter space. As the microwave is 
installed in a cabinet it leaves the counter space free, Glen's stainless steel microwaves make your kitchen space look sleek and contemporary.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/microwave/Digital_Display.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Digital Display</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">A wide panel with interactive digital display offers for the ease of operation and maximum
 user convenience.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/microwave/14_Temperature_Settings.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>14 Temperature Settings</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">14 presets temperature settings let you cook for a variety of recipes. Be it heating, 
roasting, baking or just reheating, Glen's built in microwave has got it all.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">
                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/microwave/90_Minute_Digital_Timer.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>90 Minutes Digital Timer</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">A 90 minutes timer allows you to perfectly time the extra long baking recipes without 
any hassle.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">
                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/microwave/Jog Wheel Control.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Jog Wheel Control</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">With an effortless digital jog wheel type control; you can operate the microwave oven smoothly.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">

            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/built-in-series/built-in-oven/trio-header.jpg')}}"
                             class="margin-top-20px"
                             style="margin-top:40px !important;">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80" style="margin-top:40px;">the ultimate trio
                            built-in micro | steam | oven</h1>

                        <p class="top-header-p ">A linear arrangement of a conventional oven, a steam oven and a microwave,
 it imparts an elegant look to your ultra modern kitchen space. Build to matching proportions; this set gives you the ultimate
 freedom with multiple cuisine cooking. Now you can bake or roast, cook healthy crisp steamed foods, turbo grill, cook quick micro
 dishes and defrost just in arms distance.</p>

                        <div class="top-header-p-border margin-bottom10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@stop