@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
 @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/built-in-series/induction-hob/header-banner.jpg')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Buit-In-Induction Hobs</h1>

                        <p class="top-header-p ">Induction hobs are energy efficient, cleaner and safer option than cooktops. They work by creating a 
magnetic field between the hob and the pan. The heat generation is instant and easy to control. The surface of induction hob cools down as soon as the 
pan is removed. This feature is especially useful when kids are around. Induction hobs help save energy by heating only pan area, rather than all around it.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">

            <div class="">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/induction-hob/induction.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Induction</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">With no flame generation and heat confinement to pan only, induction is arguably the
 safest method of cooking. Instant heat genesis, fine controls of heating levels ensures that your dish is a culinary masterpiece. The surface
 cools down promptly negating any possibility of accidental burns. In addition to all these amazing features, you save energy as the induction 
hob heats only the pan area-keeping surface around at room temperature.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/induction-hob/Precision-built-in-led-touch-control.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>LED Touch Control</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Glen's built in Induction hobs come with nine precise power levels for each cooking station,
 enabling fine-tuning of heat requirement with ease. The LED touch controls set against the backdrop of German Schott Ceramic Glass accentuate the
 looks besides providing smooth operational control. </p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/induction-hob/Schoott-Ceran-Ceramic-Glass.PNG')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Schoott Ceran Ceramic Glass</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Glen's Induction Hobs come with World's No. 1 German Schott Ceramic Glass for supreme 
durability and safety. It doesn't crack up at elevated temperatures and is easier to clean and maintain.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/induction-hob/Digital-display.PNG')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Digital display</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Induction is the safest way of cooking as it negates any chance of accidental burn.
 Glens' built in induction hobs are the safest as they come with World's No. 1 German Schott Ceramic glass. These are best built in hobs in
 India with digital display, LED touch control and highly efficient faster induction. They are compatible with all induction compatible utensils
 and help you cook the most delicious dishes in no time. So, shop online built in oven from Glen to enjoy the benefits of induction cooking.</p>
                    </div>
                </div>
            </div>

        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop