@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
 @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>


<?php $session_ids = Session::get('id'); ?>
<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img
                            src="{{asset('frontoffice/img/built-in-series/built-in-oven/oven-header.png')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Built-In Ovens</h1>

                        <p class="top-header-p ">Glen's built in ovens can make a big style statement for your contemporary kitchen.
 Innovative touch control technology, precise functioning and minimalistic design make it a piece of art. With spacious cavity, self
 cleaning function and futuristic LCD display, Glen's electric oven are bound to sweep away your feet. Its pre set auto cooking menus
 allow you to make gourmet level dishes.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/built-in-oven/led-touch-control.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>LCD Display</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Now you can access all the functions of your kitchen oven conveniently, just with
 a gentle tap at the smart LCD display. Be it electronic timer, the minute- minder or the clock, you have everything at your fingertips. 
Extremely easy to read, the LCD display also informs you about cooking time left and the temperature.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/built-in-oven/pre-set-auto-cooking.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Pre-set Auto Cooking</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">With pre set auto cook menus you can cook delectable gourmet style food.
 All you have to do is select the quantity of material and rest is done automatically. You can also customize your own menu preferences
 and save as a program.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">
                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/cooking-ranges/Motorised-Rotisserie.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Motorised Rotisserie</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Equipped with slow speed motor driven skewer for effective and uniform grilling of food,
 Glen's built in electric oven offer the best grilling experience.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">
                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/built-in-oven/Turbo-Convection.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Turbo Convection</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">With constant flow of hot air over the food, the goodness of natural food juices is preserved.
 Also you get a uniformly cooked dish, which has a taste as good inside as it does outside.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">

            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/built-in-series/built-in-oven/trio-header.jpg')}}"
                             class="margin-top-20px"
                             style="margin-top:40px !important;">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80" style="margin-top:40px;">the ultimate trio
                            built-in micro | steam | oven</h1>

                        <p class="top-header-p ">A linear arrangement of a conventional oven, a steam oven and a microwave, it imparts an 
elegant look to your ultra modern kitchen space. Build to matching proportions; this set gives you the ultimate freedom with multiple cuisine
 cooking. Now you can bake or roast, cook healthy crisp steamed foods, turbo grill, cook quick micro dishes and defrost just in arms distance.</p>

                        <div class="top-header-p-border margin-bottom10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@stop