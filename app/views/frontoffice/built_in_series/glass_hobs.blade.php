@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
 @media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/built-in-series/Glass-hob/header-banner.jpg')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Glass Hobs</h1>

                        <p class="top-header-p ">Outstanding aesthetics and efficient capability in cooking. Yes, this
                            is precisely what has been kept in mind while designing the Glen Built-in Hobs. Behind their
                            minimalistic exteriors is the backing of the promise of high performance cooking.</p>

                        <p class="top-header-p">Just turn the dial. Glen gas hobs are incredibly intuitive to operate,
                            and have automatic ignition and a range of sizes and burner choices.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">

            <div class="">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/Glass-hob/Double-ring-burner.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Double ring burner</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">Specially designed & developed in Italy for Indian cooking needs. Available in 2 different sizes for different levels of cooking.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/Glass-hob/Metal-Knobs.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Triple Ring Burner</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The burner with three perfect rings of flame.  A high energy, high  efficiency burner specially designed for the Indian fondness for deep frying.</p>
                    </div>
                </div>


                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/Glass-hob/Toughened-Black-Glass-8-mm-Thick.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Toughened black glass 8 mm thick</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The elegant black glass surface doesn’t just look stunning – it makes your Glen hob a breeze to clean, and resists scratches, stains, impact and heat.</p>
                    </div>
                </div>
                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/built-in-series/Glass-hob/Italian-gas-valves.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Italian Gas Valves</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">High precision Italian gas valves for total safety and a perfect flame control. No more gas leakages and no more sim-off.</p>
                    </div>
                </div>
            </div>

        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop