@extends('layouts.frontoffice.default')

@section('content')


<div class="container">
    <div class="row-fluid">
        <div class="span12">
            {{Notification::showSuccess()}}
            {{Notification::showError()}}
        </div>
    </div>
</div>


<div class="container">

    <div class="row-fluid">

        <div class="span12">
            <h1>Transaction Failure</h1><br/>

            <div class="alert alert-danger">

                <h6>Sorry, your transaction was unsuccessfull. </h6>

            </div>


            <p>
                For any queries, complaints or suggestions, please email us at <a href="mailto:enquiry@glenindia.com">enquiry@glenindia.com</a>
                or call 0129-2254191-93 and 1800 180 1998.
            </p>


        </div>

    </div>

</div>


@stop
