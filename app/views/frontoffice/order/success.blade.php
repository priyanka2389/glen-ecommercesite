@extends('layouts.frontoffice.default')

@section('content')
    <script type="text/javascript">
        var product_ids = new Array();
        <?php foreach($item_ids as $key => $val){ ?>
        product_ids.push(<?php echo $val; ?>);
        <?php } ?>

        var google_tag_params = {
            ecomm_pagetype: 'purchase',
            ecomm_prodid: product_ids,
            ecomm_totalvalue: <?php echo $total_value; ?>
        };
        console.log(google_tag_params);
    </script>
    <script type="text/javascript">

        {{--window.onbeforeunload = function (evt) {--}}
            {{--if (typeof evt == 'undefined')--}}
                {{--evt = window.event;--}}
            {{--if (evt) {--}}
{{--//                alert(evt);--}}
                {{--return "If you leave this page, your information will not be updated.";--}}
{{--//                return false;--}}
            {{--}--}}
        {{--}--}}
        {{--window.onUnload = function(){--}}
            {{--alert('go to home');--}}
            {{--window.location.href = '{{URL::to('/')}}';--}}
        {{--}--}}

    </script>
    <!-- Google Code for Glen Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1006004267;
        var google_conversion_language = "en";
        var google_conversion_format = "2";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "nY-aCL_R3FkQq9DZ3wM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1006004267/?label=nY-aCL_R3FkQq9DZ3wM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
<script>(function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
        }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6027610256798', {'value':'0.01','currency':'INR'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6027610256798&amp;cd[value]=0.01&amp;cd[currency]=INR&amp;noscript=1" /></noscript>

    <div class="container">
        <div class="row-fluid">
            <div class="span12">
                {{Notification::showSuccess()}}
                {{Notification::showError()}}
            </div>
        </div>
    </div>


    <div class="container">

        <div class="row-fluid">

            <div class="span12">
                <h1>Transaction Success</h1><br/>

                <div class="alert alert-success">

                    <h6>Thank you for shopping with us. Your transaction is successful. We will be shipping your order
                        to you soon.</h6>

                </div>


                <p>
                    For any queries, complaints or suggestions, please email us at <a
                            href="mailto:enquiry@glenindia.com">enquiry@glenindia.com</a>
                    or call 0129-2254191-93 and 1800 180 1998.
                </p>


            </div>

        </div>

    </div>


@stop
