@extends('layouts.frontoffice.default')

@section('content')

<style type="text/css">

    .return_div ul {
        padding-left: 40px;
    }

    .return_div ul li {
        list-style: circle !important;
        padding-bottom: 5px;
    }
</style>

<div class="container">
    <div class="row">
        <div class="span12">
            <h1>Return Policy</h1>
        </div>
    </div>

    <br/><br/>

    <div class="row justify">

        <div class="span12 return_div">
            <h3>WHAT can be returned?</h3>
            <ul>
                <li>All products are returnable if the product is in the same condition as when purchased.
                    This includes the product packaging and original price tags.
                </li>
                <li>The product’s packing material must be in same condition as when received.
                    Products must not be used.
                </li>
                <li>Product must be returned along with all accessories.</li>
                <li>Serial number must be on product, product without serial number will not acceptable.</li>
                <li>Product need to be returned should be claimed in 48 hours. The customer will be guided for return or
                    exchange. The product once received by Glen appliances will be inspected for any other damages. If
                    the claim stated is genuine the product will be replaced or the
                    money will be reimbursed. Products claimed beyond 48 hours will only be exchanged not returned.
                </li>
            </ul>

            <h3>Procedure to return your product</h3>
            <ul>
                <li>Contact us via email or phone, our contact detail is (contact link). We will guide you about process
                    for the same.
                </li>
                <li>After obtaining a RETURN AUTHORIZATION, product can be returned to our head office: I‐34, DLF
                    Industrial Area, Phase‐ 1, Haryana, India (121003).
                </li>
                <li><strong>In store return option</strong> - ‐ After obtaining a RETURN AUTHORIZATION, product can be
                    returned to: I‐34, DLF Industrial Area, Phase‐ 1, Haryana, India.
                </li>
            </ul>

            <h3>SHIPPING for returns</h3>
            <ul>
                <li>Customer is responsible about shipping charges for return of products. Shipping charges
                    Will be borne by Glen Appliances only if the product received has a manufacturing defect
                    or was in damaged Condition
                </li>
                <li>Customer is responsible about damaged due to returning transit;It is advised that the return
                    packets should be strongly packaged.
                </li>
                <li>If any product which is delivered is not same as per order or in damaged condition then the same
                    will be replaced by us without any charge. (In condition of damaged product it is your
                    responsibility to contact us immediately.
                </li>
            </ul>

            <h3>CREDIT for returns</h3>
            <ul>
                <li>Your credit card will be refunded once the item is inspected.</li>
            </ul>


        </div>


    </div>

    <br/><br/>

</div>


@stop
