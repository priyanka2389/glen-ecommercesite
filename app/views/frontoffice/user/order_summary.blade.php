@extends('layouts.frontoffice.default')

@section('content')

    <script type="text/javascript" src="{{asset('frontoffice/js/cart_summary.js')}}"></script>
    <style type="text/css">
        a.exclusive:hover {
            color: white !important;
        }

        #discount_price {
            color: #3c763d;
            font-size: 13px
        }

        textarea {
            width: 252px;
        }

        .pay_block {
            padding: 10px;
            background-color: #adb5bc;
            margin-bottom: 10px;
            margin-left: 0px !important;
        }

        .paylabel {
            margin-top: 3px;
            font-size: 13px;
            color: #FFF;
        }


    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.standard-checkout').click(function () {
                var action = $(this).attr('href');
                var comments = $('#comments').val()
                $(this).attr('href', action + '&comments=' + comments);
            });

        });
    </script>

    <div class="container">
        <div class="row-fluid">
            <div class="span12">
                {{Notification::showSuccess()}}
                {{Notification::showError()}}
            </div>
        </div>
    </div>

    <?php $user_id = $user->id;
    $mobile = $user->mobile;
    ?>

    <section id="columns" class="clearfix">
        <div class="container">
            <div class="row-fluid">
                <section id="center_column" class="span12">
                    <div class="contenttop row-fluid">


                        <h1 id="cart_title" class="title_category custom_h1">Order Summary</h1>

                        <!-- Steps -->
                        <ul class="step" id="order_step">

                            <li class="step_todo"><span>1. Login</span></li>
                            <li class="step_done"><span><a href="{{URL::to('user/address/'.$user_id)}}">2.
                                        Address</a></span></li>
                            <li id="step_end" class="step_current"><span>3. Order Summary</span></li>
                        </ul>
                        <!-- /Steps -->

                        @if(isset($items) && count($items) != 0)

                            <p class="wrapper">Your shopping cart contains: <span
                                        id="summary_products_quantity">@if(sizeof($items) == 1){{sizeof($items)}}
                                    product @else{{sizeof($items)}} products @endif</span></p>

                            <div id="order-detail-content" class="table_block">
                                <table id="cart_summary" class="std">
                                    <thead>
                                    <tr>
                                        <th class="cart_product first_item">Product</th>
                                        <th class="cart_description item ">Item</th>
                                        <th class="cart_quantity item">Qty</th>
                                        <th class="cart_total item">Price</th>
                                        <th class="cart_total item">SubTotal</th>
                                        <th class="cart_delete last_item">&nbsp;</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @foreach($items as $item)

                                        <?php $custom_id = $item['id'];
                                        $custom_id = explode('_', $custom_id);
                                        $type = $custom_id[0];
                                        $id = $custom_id[1];

                                        $image = ($type == 'combo') ? AppUtil::getComboPrimaryImage($id) : AppUtil::getProductPrimaryImage($id);

                                        $path = isset($image['image_path']) ? $image['image_path'] : Constants::DEFAULT_300_IMAGE;
                                        $title = isset($image['image_title']) ? $image['image_title'] : "";
                                        ?>

                                        <tr id="product_4_0_0_0" class="cart_item first_item address_0 odd">
                                            <td class="cart_product">
                                                {{--<a href="#">--}}
                                                <img src="{{URL::to($path)}}" alt="{{$title}}" width="80"
                                                     height="80"/>
                                                {{--</a>--}}
                                            </td>
                                            <td class="cart_description">
                                                <p class="s_title_block">
                                                    {{--<a href="">--}}
                                                    {{$item['name']}}
                                                    {{--</a>--}}
                                                </p>
                                            </td>
                                            <td class="cart_quantity">
                                                <div class="cart_quantity_button">
                                                    <a class="qty_btn cart_quantity_up" id="cart_quantity_up"
                                                       data-id="{{$id}}" data-item-type="{{$type}}"
                                                       title="Add">
                                                        <img src="{{asset('frontoffice/themes/leometr/img/icon/quantity_up.gif')}}"
                                                             alt="Add"
                                                             width="14" height="9"/>
                                                    </a><br/>
                                                    <a class="qty_btn cart_quantity_down" id="cart_quantity_down"
                                                       data-id="{{$id}}" data-item-type="{{$type}}"
                                                       title="subtract">
                                                        <img
                                                                src="{{asset('frontoffice/themes/leometr/img/icon/quantity_down.gif')}}"
                                                                alt="Subtract"
                                                                width="14" height="9"/>
                                                    </a>
                                                </div>

                                                <input name="" size="2" type="text" autocomplete="off"
                                                       class="cart_quantity_input"
                                                       id="cart_quantity_input_{{$type}}_{{$id}}"
                                                       value="{{$item['qty']}}" readonly/>
                                                <a class="save_new_qty" data-item-type="{{$type}}" data-id="{{$id}}">Save</a>

                                            </td>
                                            <td class="cart_total">
		                                <span class="price-black" id="total_product_price_4_0_0">
											<span class="WebRupee">Rs.</span>     {{number_format($item['price'])}}
                                        </span>
                                            </td>

                                            <td class="cart_total">
		                                <span class="price-black" id="total_product_price_4_0_0">
											<span class="WebRupee">Rs.</span>     {{number_format($item['subtotal'])}}
                                        </span>
                                            </td>

                                            <td class="cart_delete">
                                                <div>
                                                    <a href="{{URL::to('cart/remove-item/'.$type.'/'.$id.'?redirect=true&order=true')}}"
                                                       class="cart_quantity_delete">x</a>
                                                </div>
                                            </td>
                                        </tr>


                                    @endforeach

                                    </tbody>
                                </table>
                                <div class="clearfix row-fluid" id="customer_cart_total">
                                    <div class="span12">

                                        <table class="std">
                                            <tfoot>


                                            <tr class="cart_total_price">
                                                <td class="" id="">

                                                        <textarea rows="5" placeholder=" Enter your comments here"
                                                                  id="comments" name="comments"></textarea>

                                                    <?php $coupon = Session::get('coupon_info.coupon_code'); ?>
                                                    {{--<div class="span4">--}}
                                                    <form action="{{URL::to('/order/apply-coupon')}}" method="post"
                                                          id="coupon_form"
                                                          style="display:inline-block;margin-left:10px;">
                                                        <fieldset>
                                                            <input id="" type="text" name="coupon_code"
                                                                   value="@if(isset($coupon)){{$coupon}}@endif"
                                                                   style="margin-top:8px">
                                                            <input type="submit" class="exclusive standard-checkout"
                                                                   value="Apply Coupon"/>
                                                            @if(isset($coupon))
                                                                <a href="{{URL::to('/order/remove-coupon')}}">Remove
                                                                    Coupon</a>
                                                            @endif
                                                        </fieldset>
                                                    </form>
                                                    {{--</div>--}}

                                                    <h3 style="float:right;" class="display-inline-block font-17px">

                                                <span id="total_price">Amount: &nbsp;<span
                                                            class="WebRupee"
                                                            style="display:inline-block; padding-left:6px;">
                                                        Rs.</span>{{number_format($total_price)}}</span> <br/>

                                                <span id="discount_price">Coupon Discount: &nbsp;
                                                       <span class="WebRupee"> Rs.</span> {{number_format($total_discount)}}</span>
                                                        <br/>
                                                <span>Total Amount Payable: &nbsp;
                                                    <span class="WebRupee"> Rs.</span>{{number_format($final_price)}} </span>
                                                        <br/>
                                                    </h3>

                                                </td>

                                            </tr>


                                            </tfoot>
                                        </table>


                                        {{--<div class="span12">--}}
                                        @if(isset($cod_applicable)&& $cod_applicable==true)
                                            <div class="span12 pay_block">
                                                <div class="span6 paylabel"><strong>Cash On Delievery (COD)</strong>
                                                </div>
                                                <a href="{{URL::to('/order/create-order?payment_type=cod')}}"
                                                   class="exclusive standard-checkout btn btn-large"
                                                   title="" style="float:right;">Confirm Payment</a>
                                            </div>
                                        @endif
                                        {{--&nbsp;&nbsp;--}}


                                        <div class="span12 pay_block">
                                            <div class="span6 paylabel"><strong>Credit Cards/Debit Cards/Net
                                                    Banking</strong><br/>You may user any of above option to make
                                                payment
                                            </div>
                                            <a href="{{URL::to('/order/create-order?payment_type=online')}}"
                                               class="exclusive standard-checkout btn btn-large"
                                               title="" style="float:right;">Confirm Payment</a>
                                        </div>


                                        {{--</div>--}}

                                    </div>
                                </div>

                            </div>


                        @else

                            <h4>Your shopping cart is empty.</h4>

                        @endif

                    </div>
                    <!-- end div block_home -->
                </section>
            </div>
        </div>
    </section>

@stop