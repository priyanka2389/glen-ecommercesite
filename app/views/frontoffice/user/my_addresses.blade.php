@extends('layouts.frontoffice.default')
@section('content')

    <script type="text/javascript"
            src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"/>

    <script type="text/javascript">
        $(document).ready(function () {

            $('.address_content').hide();
            var action = '<?php if(!empty($action)) {echo $action;} ?>';
            var addressId = '<?php if(!empty($address)) {echo $address->id;} ?>';

            $('#address_box_' + addressId).css('border-color', '#17479d');
            if (action == 'Edit Address') {
                $('.address_content').show();
                $('#address_form').attr('action', '{{URL::to('user/my-address?action=edit&address_id=')}}' + addressId);

            } else if (action == 'Add Address') {
                $('.address_content').show();
                $('#address_form').attr('action', '{{URL::to('user/my-address?action=add')}}');
            }

            var no_of_address = '<?php if(isset($addresses)){ echo count($addresses);}else{ echo 0;} ?>';
            if (no_of_address == 0) {
                $('.address_content').show();
                $('#address_form').attr('action', '{{URL::to('user/my-address?action=add')}}');
            }

            var is_pincode_valid = true;
            var form = $('#address_form');
            var site_url = base_url;

            form.validate({
                rules: {
                    mobile: {
                        required: true,
                        minlength: 10,
                        maxlength: 10
                    }
                },
                messages: {
                    mobile: {
                        required: "Mobile number is required",
                        minlength: 'Please enter a valid 10 digit mobile number',
                        maxlength: 'Please enter a valid 10 digit mobile number'
                    }
                }
            });

            $('.address_check').change(function (e) {
                if ($(this).is(':checked')) {
                    $('.address_content').addClass('hidden');
                    $('.address_check').prop('disabled', 'disabled');
                    $(this).prop('disabled', false);
                } else {
                    $('.address_content').removeClass('hidden');
                    $('.address_check').prop('disabled', false);
                }

            });

            $('.addbtn').click(function (e) {
                e.preventDefault();
                $('.address_content').show();

                $('#address_form').attr('action', '{{URL::to('user/my-address?action=add')}}');
            });

            $('#cancel').click(function (e) {
                if (action != null) {
                    window.location.href = '{{URL::to('user/address')}}';
                } else {
                    window.location.reload();
                }

            });
        });

        function getCities(state, city_control, loader) {

            var site_url = base_url;

            $('#' + loader).removeClass('hidden');

            var get_location_data_url = site_url + '/user/ajax-cities-by-state/' + state;
            $.get(get_location_data_url, function (data) {

                $('#' + loader).addClass('hidden');
                if (data) {
//                    console.log(data);
                    var cities = '<option>Select City</option>';
                    for (var i = 0; i < data.length; i++) {
                        var city = data[i].city.charAt(0).toUpperCase() + data[i].city.substring(1).toLowerCase(); //convert string first character in uppercase and other characters in lowercase
                        cities += '<option value="' + data[i].city + '">' + city + '</option>';
                    }
                    cities += '<option value="others">Others</option>';
                    $('#' + city_control).html(cities);
                }
            });
        }

        function getPincodes(city, pincode_control, loader) {

            if (city == 'others') {
                alert('This service is not available in your city. Please contact to 9266655555. And our representative will assist you');
            } else {

                $('#' + loader).removeClass('hidden');

                var site_url = base_url;
                var get_location_data_url = site_url + '/user/ajax-pincodes-by-city/' + city;
                $.get(get_location_data_url, function (data) {

                    $('#' + loader).addClass('hidden');
                    if (data) {
                        var pincodes = '<option>Select Pincode</option>';
                        for (var i = 0; i < data.length; i++) {
                            var postal_code = data[i].postal_code.charAt(0).toUpperCase() + data[i].postal_code.substring(1).toLowerCase(); //convert string first character in uppercase and other characters in lowercase
                            pincodes += '<option value=' + data[i].postal_code + '>' + postal_code + '</option>';
                        }
                        $('#' + pincode_control).html(pincodes);
                    }
                });
            }
        }


    </script>
    <style type="text/css">
        .address_box {
            padding: 10px;
            text-align: center;
            border: 1px solid #ccc;
        }

        .address_box b {
            font-family: Arial, Helvetica, sans-serif;
        }

        .deletebtn {
            float: right;
        }

        a.btn-action, a.btn-action:active, a.btn-action:visited {
            color: #ffffff;
        }

        .addbtn {
            margin-top: 14px;
        }

        /*.row-fluid [class*="span"]:nth-child(5), .row-fluid [class*="span"]:nth-child(9){margin-left:10px;}*/
    </style>

    <section id="columns" class="clearfix">
        <div class="container">
            <div class="row-fluid">
                <section id="center_column" class="span12">
                    <div class="contenttop row-fluid">
                        @if(isset($addresses))
                            <div class="span10"><h1 id="cart_title" class="title_category">My Addresses</h1></div>
                            <div class="span2">
                                <a href="" class="addbtn btn btn-action" title="add Address">
                                    <i class="fa fa-plus"></i>
                                    Add New Address
                                </a>
                            </div>
                        @else
                            <h1 id="cart_title" class="title_category">My Addresses</h1>
                        @endif
                        <div class="clearfix"></div>
                        <div class="col-lg-12">
                            {{Notification::showSuccess()}}
                        </div>

                        <div class="col-lg-12">
                            {{Notification::showError()}}
                        </div>

                        <form action="#" method="post" class="std form-horizontal"
                              id="address_form">

                            <!--                        shipping address div starts here-->
                            <div class="authentication-page row-fluid">
                                <div class="span12">


                                    @if(isset($addresses))
                                        @for ($i = 0; $i < count($addresses); $i=$i+3)
                                            <div class="row-fluid">

                                                @for($j=0;$j<3;++$j)
                                                    @if($i+$j < count($addresses))
                                                        <div class="span4 address_box"
                                                             id="address_box_{{$addresses[$i + $j]->id}}">
                                                            <h4>Address {{($i+$j) + 1}}</h4>
                                                            <ul>

                                                                <li>
                                                                    <b>Name:</b>{{ucwords($addresses[$i + $j]->first_name.' '.$addresses[$i + $j]->last_name)}}
                                                                </li>
                                                                <li><b>Address(Line
                                                                        1): </b> {{$addresses[$i + $j]->line1}}</li>
                                                                <li><b>Address(Line
                                                                        2): </b> {{$addresses[$i + $j]->line1}}</li>
                                                                <li>
                                                                    <b>Landmark: </b> {{$addresses[$i + $j]->landmark}}
                                                                </li>
                                                                <li><b>Zip
                                                                        Code: </b> {{$addresses[$i + $j]->pincode}}
                                                                </li>
                                                                <li><b>City </b> {{$addresses[$i + $j]->city}}</li>
                                                                <li><b>State: </b> {{$addresses[$i + $j]->state}}
                                                                </li>
                                                            </ul>
                                                            <div class="span3" style="float: right;">
                                                                <a href="{{URL::to('user/my-addresses/?action=edit&address_id='.$addresses[$i + $j]->id)}}"
                                                                   class="editbtn btn btn-xs btn-action"
                                                                   data-address-id="{{$addresses[$i + $j]->id}}"
                                                                   data-toggle="tooltip" title="Edit Address">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>
                                                                <a href="{{URL::to('user/delete-address/'.$addresses[$i + $j]->id)}}"
                                                                   class="deletebtn btn btn-xs btn-action"
                                                                   onclick="return confirm('If you click ok your address will deleted permenantly')"
                                                                   data-toggle="tooltip" title="Delete Address">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                            </div>
                                                            <br/>
                                                        </div>
                                                    @endif
                                                @endfor

                                            </div>
                                            <br/>
                                        @endfor
                                    @endif


                                    <fieldset class="address_content">
                                        <h3 class="title_content">Please enter your address here</h3>

                                        <div class="control-group span5">
                                            <label for="first_name" class="control-label">First name
                                                <sup>*</sup></label>

                                            <div class="controls">
                                                <input type="text" name="first_name" class="required"
                                                       id="first_name"
                                                       value="{{$address->first_name or ''}}">
                                                <span class="error">{{$errors->first('first_name')}}</span>
                                            </div>
                                        </div>
                                        <div class="control-group span5">
                                            <label for="last_name" class="control-label">Last name
                                                <sup>*</sup></label>

                                            <div class="controls">
                                                <input type="text" id="last_name" name="last_name" class="required"
                                                       value="{{$address->last_name or ''}}">
                                                <span class="error">{{$errors->first('last_name')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group  span5">
                                            <label for="address_line1" class="control-label">Address
                                                <sup>*</sup></label>

                                            <div class="controls">
                                                <input type="text" id="address_line1" name="address_line1"
                                                       class="required" value="{{$address->line1 or ''}}">
                                                <span class="error">{{$errors->first('address_line1')}}</span>
                                            </div>
                                        </div>
                                        <div class="control-group  span5">
                                            <label for="address_line2" class="control-label">Address (Line
                                                2)</label>

                                            <div class="controls">
                                                <input type="text" id="address_line2" name="address_line2"
                                                       value="{{$address->line2 or ''}}">
                                                <span class="error">{{$errors->first('address_line2')}}</span>
                                            </div>
                                        </div>
                                        <div class="control-group  span5">
                                            <label for="landmark" class="control-label">Landmark</label>

                                            <div class="controls">
                                                <input type="text" name="landmark" id="landmark" class=""
                                                       value="{{$address->landmark or ''}}">
                                            </div>
                                        </div>
                                        <div class="control-group select span6">
                                            <label for="id_country" class="control-label">Country</label>

                                            <div class="controls" style="padding-top: 8px">
                                                <b>India</b> (Service available only in India)
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="control-group  span5" style="">
                                            <label for="state" class="control-label">State <sup>*</sup></label>

                                            <div class="controls">
                                                <select name="state" id="state" class="required state"
                                                        onchange="getCities(this.value,'city','ajax_loader1');">
                                                    <option value="">Select a state</option>
                                                    <?php if (!empty($address) && $address->state) {
                                                        $astate = $address->state;
                                                    } else {
                                                        $astate = '';
                                                    } ?>
                                                    @foreach($states as $state)
                                                        <option value="{{$state->state}}" @if($state->state == $astate)
                                                                selected="selected" @endif>{{ucfirst(strtolower($state->state))}}</option>
                                                    @endforeach
                                                </select>
                                                 <span class="ajax_loader hidden" id="ajax_loader1">
                                                    <img src="{{asset('frontoffice/img/ajax-loader.gif')}}" alt=""/> Wait ...
                                                </span>
                                                <span class="error">{{$errors->first('state')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group  span5">
                                            <label for="city" class="control-label">City <sup>*</sup></label>

                                            <div class="controls">
                                                <select name="city" id="city" class="required city"
                                                        onchange="getPincodes(this.value,'pincode','ajax_loader2');">
                                                    <?php if (!empty($address) && $address->city) {
                                                        $acity = $address->city;
                                                    } else {
                                                        $acity = '';
                                                    } ?>
                                                    @if(isset($cities))
                                                        @foreach($cities as $city)
                                                            <option value="{{$city->city}}" @if($city->city == $acity)
                                                                    selected="selected" @endif>{{ucwords($city->city)}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value=""></option>
                                                    @endif
                                                </select>
                                                 <span class="ajax_loader hidden" id="ajax_loader2">
                                                    <img src="{{asset('frontoffice/img/ajax-loader.gif')}}" alt=""/> Wait ...
                                                </span>
                                                <span class="error">{{$errors->first('city')}}</span>
                                            </div>
                                        </div>
                                        <div class="control-group span5 postcode " style="">
                                            <label for="pincode" class="control-label">Zip / Postal Code
                                                <sup>*</sup></label>

                                            <div class="controls">
                                                <select id="pincode" name="pincode" class="required">
                                                    <?php if (!empty($address) && $address->pincode) {
                                                        $apincode = $address->pincode;
                                                    } else {
                                                        $apincode = '';
                                                    } ?>
                                                    @if(isset($pincodes))
                                                        @foreach($pincodes as $pincode)
                                                            <option value="{{$pincode->postal_code}}" @if($pincode->postal_code == $apincode)
                                                                    selected="selected" @endif>{{$pincode->postal_code}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value=""></option>
                                                    @endif

                                                </select>

                                                <span class="shipping_pincode_not_found"></span>
                                                <span class="error">{{$errors->first('pincode')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group span5">
                                            <label for="landline" class="control-label">Home phone</label>

                                            <div class="controls">
                                                <input type="text" id="landline" name="landline"
                                                       value="{{$address->phone_no or ''}}">
                                            </div>
                                        </div>
                                        <div class="control-group span5">
                                            <label for="mobile" class="control-label">Mobile phone <sup>*</sup>
                                            </label>

                                            <div class="controls">
                                                <input type="text" id="mobile" name="mobile" class="required digits"
                                                       value="{{$address->mobile or ''}}">
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="clearfix"></div>
                                        <div class="control-group submit2">
                                            <div class="controls">
                                                <input type="submit" class="submit exclusive standard-checkout"
                                                       value="Save"/>

                                                <input type="submit" class="submit exclusive standard-checkout"
                                                       id="cancel"
                                                       value="Cancel"/>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <!--                        shipping address div ends here-->


                        </form>
                    </div>
                </section>
            </div>
        </div>
        <!-- end div block_home -->
    </section>
    </div>
    </div>
    </section>

@stop