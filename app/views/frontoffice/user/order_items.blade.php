<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">


    <title></title>


    <!--Core CSS -->
    {{HTML::style('frontoffice/css/bootstrap.min.css')}}
    {{HTML::style('frontoffice/css/bootstrap-reset.css')}}
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"/>

    <!-- Custom styles for this template -->
    {{--{{HTML::style('backoffice/css/style.css')}}--}}
    {{HTML::style('frontoffice/css/style-responsive.css')}}
    {{HTML::style('frontoffice/css/custom.css')}}
    {{HTML::style('frontoffice/css/rupee.css')}}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        h3{
            margin-left: 30px;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="row-fluid"><h3>Order Items</h3></div>
            <table class="table table-stripped table-hover">
                <tbody>

                <tr>
                    <th>#</th>
                    <th>Item Type</th>
                    <th>Item Name</th>
                    <th>Quantity</th>
                    <th>Offer Price</th>
                    <th>List Price</th>
                </tr>

                @foreach($items as $i=> $row)

                    <tr>
                        <td>{{$i+1}}</td>
                        <td>{{ucwords($row['item_type'])}}</td>
                        <td>{{$row['item_name']}}</td>
                        <td>{{$row['qty']}}</td>
                        <td>{{$row['offer_price']}}</td>
                        <td>{{$row['list_price']}}</td>
                    </tr>

                @endforeach

                </tbody>

            </table>
        </div>
    </div>
</div>

</body>
</html>