@extends('layouts.frontoffice.default')
@section('content')

    <script type="text/javascript"
            src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"/>
    <script type="text/javascript">
        $(document).ready(function () {

            var is_pincode_valid = true;
            var form = $('#address_form');
            var site_url = base_url;

            $('.shipping_address_check').change(function (e) {

                if ($(this).is(':checked')) {
                    $('.shipping_address_content').addClass('hidden');
                    $('.shipping_address_check').prop('disabled', 'disabled');
                    $(this).prop('disabled', false);

                    $('#shipping_address_content_form').addClass('hidden');
                } else {
                    $('.shipping_address_content').removeClass('hidden');
                    $('.shipping_address_check').prop('disabled', false);
                }
//                checkAddresses();
            });
            $('.billing_address_check').change(function (e) {

                if ($(this).is(':checked')) {
//                    $('.billing_address_content').addClass('hidden');
                    $('.billing_address_check').prop('disabled', 'disabled');
                    $(this).prop('disabled', false);

                    $('.billing_address_content_form').addClass('hidden');
                } else {
//                    $('.billing_address_content').removeClass('hidden');
                    $('.billing_address_check').prop('disabled', false);
                }
//                checkAddresses();
            });

            $('#billing_address_check').change(function (e) {
                if ($(this).is(':checked')) {
                    $('.billing_address_content').addClass('hidden');
                    $( "#address_form" ).validate().resetForm();
                } else {
                    $('.billing_address_content').removeClass('hidden');
                }
            });

            $('#add-shipping-address-btn').click(function (e) {
                $('.shipping_address_content_form').toggleClass('hidden');
            });

            $('#add-billing-address-btn').click(function () {
                $('.billing_address_content_form').toggleClass('hidden');
            });

            $('form').submit(function (e) {
                e.stopPropagation();
                var validate = true;

                var cart_items = <?php if(!empty($cart_items) && $cart_items > 0) {echo $cart_items;} else {echo 0;} ?>;

                if (cart_items > 0) {
                    var message;
                    var res = $('#shipping_address_content_form').hasClass('hidden');

                    if (res == true) {
                        $( "#address_form" ).validate().resetForm();
                        var selectedS = [];
                        $('input:checkbox:checked.shipping_address_check').each(function () {
                            selectedS.push($(this).val());
                        });
                        if (selectedS.length <= 0) {
                            message = 'Please select your shipping address.';
                            validate = false;
                        }
                    } else {
                        $('#address_form').validate({
                            rules: {
                                shipping_first_name: "required",
                                shipping_last_name: "required",
                                shipping_address_line1: "required",
                                shipping_state: "required",
                                shipping_city: "required",
                                shipping_pincode: "required"
                            }
                        });
                        if($('#address_form').valid() == true){
                            validate = true;
                        }else{
                            validate = false;
                        }
                    }

                    var res = $('#billing_address_content_form').hasClass('hidden');
                    if (res == true) {
                        $( "#address_form" ).validate().resetForm();
                        var selectedB = [];
                        $('input:checkbox:checked.billing_address_check').each(function () {
                            selectedB.push($(this).val());
                        });
                        if ($("#billing_address_check").prop('checked') == true) {
                            selectedB.push(1);
                        }

                        if (selectedB.length <= 0) {
                            if (validate == false) {
                                message = 'Please select your shipping and billing address.';
                            } else {
                                message = 'Please select your billing address.';
                            }
                            validate = false;
                        }
                    }else {
                        $('#address_form').validate({
                            rules: {
                                billing_first_name: "required",
                                billing_last_name: "required",
                                billing_address_line1: "required",
                                billing_state: "required",
                                billing_city: "required",
                                billing_pincode: "required"
                            }
                        });
                        if($('#address_form').valid() == true){
                            validate = true;
                        }else{
                            validate = false;
                        }
                    }
                    if (message != null) {
                        $('.alert-danger').html(message).removeClass('hidden');
                    }
                } else {
                    $('.alert-danger').html('There is no item in the cart.').removeClass('hidden');
                    validate = false;
                }

                if (validate == false) {
                    return false
                } else {
                    return true;
                }
            });

        });

        function getCities(state, city_control, loader) {

            var site_url = base_url;

            $('#' + loader).removeClass('hidden');

            var get_location_data_url = site_url + '/user/ajax-cities-by-state/' + state;
            $.get(get_location_data_url, function (data) {

                $('#' + loader).addClass('hidden');
                if (data) {
//                    console.log(data);
                    var cities = '<option>Select City</option>';
                    for (var i = 0; i < data.length; i++) {
                        var city = data[i].city.charAt(0).toUpperCase() + data[i].city.substring(1).toLowerCase(); //convert string first character in uppercase and other characters in lowercase
                        cities += '<option value="' + data[i].city + '">' + city + '</option>';
                    }
                    cities += '<option value="others">Others</option>';
                    $('#' + city_control).html(cities);
                }
            });
        }

        function getPincodes(city, pincode_control, loader) {

            if (city == 'others') {
                alert('This service is not available in your city. Please contact us at 9266655555, our representative will assist you.');
            } else {

                $('#' + loader).removeClass('hidden');

                var site_url = base_url;
                var get_location_data_url = site_url + '/user/ajax-pincodes-by-city/' + city;
                $.get(get_location_data_url, function (data) {

                    $('#' + loader).addClass('hidden');
                    if (data) {
                        var pincodes = '<option>Select Pincode</option>';
                        for (var i = 0; i < data.length; i++) {
                            var postal_code = data[i].postal_code.charAt(0).toUpperCase() + data[i].postal_code.substring(1).toLowerCase(); //convert string first character in uppercase and other characters in lowercase
                            pincodes += '<option value=' + data[i].postal_code + '>' + postal_code + '</option>';
                        }
                        $('#' + pincode_control).html(pincodes);
                    }
                });
            }
        }

    </script>
    <style type="text/css">
        .shipping_address_box {
            padding: 10px;
            text-align: center;
            border: 1px solid #ccc;
        }

        .shipping_address_box b {
            font-family: Arial, Helvetica, sans-serif;
        }

        .add_block {
            height: 170px;
        }

        .deletebtn {
            float: right;
        }

        a.btn-action, a.btn-action:active, a.btn-action:visited {
            color: #ffffff;
        }

        .addbtn {
            float: right !important;
            margin: 3px 0 0 5px;
        }

        /*.row-fluid [class*="span"]:nth-child(5), .row-fluid [class*="span"]:nth-child(9){margin-left:10px;}*/
    </style>

    <section id="columns" class="clearfix">
        <div class="container">
            <div class="row-fluid">
                <section id="center_column" class="span12">
                    <div class="contenttop row-fluid">
                        <h1 id="cart_title" class="title_category">Address</h1>

                        <div class="clearfix"></div>

                        <!-- Steps -->
                        <ul class="step" id="order_step">

                            <li class="step_todo"><span>1. Login</span></li>
                            <li class="step_current"><span>2. Address</span></li>
                            <li id="step_end" class="step_todo"><span>3. Order Summary</span></li>
                        </ul>
                        <!-- /Steps -->

                        <div class="col-lg-12 notification">
                            <div class="alert alert-danger hidden" role="alert"></div>

                            {{Notification::showSuccess()}}
                            {{Notification::showError()}}
                        </div>


                        <form action="{{URL::to('user/address/')}}" method="post" class="std form-horizontal"
                              id="address_form">

                            <!--                        shipping address div starts here-->
                            <div class="authentication-page row-fluid">
                                <div class="span12">

                                    @if(isset($shipping_addresses))
                                        <?php
                                        $hidden = 'hidden';
                                        //$hidden = empty($errors) ? 'hidden' : '';
                                        ?>
                                        <fieldset class="shipping_addresses_content">

                                            <div class="span12">
                                                <h3 class="title_content">
                                                    Select your shipping address here
                                                    <a href="javascript:void(0)"
                                                       class="pull-right btn address-add-button"
                                                       id="add-shipping-address-btn"
                                                       title="add Address"> <i class="fa fa-plus"></i> Add New Address
                                                    </a>
                                                </h3>
                                            </div>

                                            <div class="clearfix"></div>
                                            @for ($i = 0; $i < count($shipping_addresses); $i=$i+3)
                                                <div class="row-fluid">
                                                    @for($j=0;$j<3;++$j)
                                                        @if($i+$j < count($shipping_addresses))
                                                            <div class="span4 shipping_address_box ">
                                                                <h4><input type="checkbox" name="saved_shipping_address"
                                                                           class="shipping_address_check"
                                                                           @if(count($shipping_addresses) == 1)checked="checked"
                                                                           @endif
                                                                           value="{{$shipping_addresses[$i + $j]->id}}"/>
                                                                    Address {{($i+$j) + 1}}</h4>
                                                                <ul>

                                                                    <li>
                                                                        <b>Name:</b>
                                                                        @if($shipping_addresses[$i + $j]->first_name != null &&  $shipping_addresses[$i + $j]->last_name != null)
                                                                            {{ucfirst($shipping_addresses[$i + $j]->first_name) .' '. $shipping_addresses[$i + $j]->last_name}}
                                                                        @else
                                                                            {{ucfirst($user->first_name).' '.$user->last_name}}
                                                                        @endif
                                                                    </li>
                                                                    <li>
                                                                        <b>Address: </b> {{$shipping_addresses[$i + $j]->line1}}
                                                                        @if(!empty($shipping_addresses[$i + $j]->line2))
                                                                            {{$shipping_addresses[$i + $j]->line2}}
                                                                        @endif
                                                                    </li>
                                                                    <li>
                                                                        <b>Landmark: </b> @if(!empty($shipping_addresses[$i + $j]->landmark)){{$shipping_addresses[$i + $j]->landmark}}@else
                                                                            - @endif
                                                                    </li>

                                                                    <li><b>Zip
                                                                            Code: </b> {{$shipping_addresses[$i + $j]->pincode}}
                                                                    </li>
                                                                    <li>
                                                                        <b>City: </b> {{$shipping_addresses[$i + $j]->city}}
                                                                    </li>
                                                                    <li>
                                                                        <b>State: </b> {{$shipping_addresses[$i + $j]->state}}
                                                                    </li>
                                                                </ul>
                                                                {{--@if(count($addresses[$i + $j]) > 1)--}}
                                                                {{--<div class="span2" style="float:right;">--}}
                                                                {{--<a href="{{URL::to('user/my-addresses/?action=edit&address_id='.$addresses[$i + $j]->id)}}"--}}
                                                                {{--class="editbtn btn btn-xs btn-action"--}}
                                                                {{--data-toggle="tooltip" title="Edit Address">--}}
                                                                {{--<i class="fa fa-pencil"></i>--}}
                                                                {{--</a>--}}
                                                                {{--<a href="{{URL::to('user/delete-address/'.$address->id)}}" class="deletebtn" onclick="return confirm('If you click ok your address will deleted permenantly')">--}}
                                                                {{--<img src="{{asset('frontoffice/img/delete.png')}}" />--}}
                                                                {{--</a>--}}
                                                                {{--</div>--}}
                                                                {{--@endif--}}
                                                            </div>
                                                        @endif

                                                    @endfor

                                                </div>
                                                <br/>
                                            @endfor

                                        </fieldset>
                                    @else
                                        <?php $hidden = ''; ?>
                                    @endif
                                    <fieldset class="shipping_address_content_form {{$hidden}}"
                                              id="shipping_address_content_form">
                                        <h3 class="title_content">Enter your shipping address here</h3>

                                        <div class="control-group text span5">
                                            <label for="shipping_first_name" class="control-label">First name
                                                <sup>*</sup></label>

                                            <div class="controls">
                                                <input type="text" name="shipping_first_name" class="required"
                                                       id="shipping_first_name" value="{{$user->first_name}}">
                                                <span class="error">{{$errors->first('shipping_first_name')}}</span>
                                            </div>
                                        </div>
                                        <div class="control-group text span5">
                                            <label for="shipping_last_name" class="control-label">Last name
                                                <sup>*</sup></label>

                                            <div class="controls">
                                                <input type="text" id="shipping_last_name"
                                                       value="{{$user->last_name}}"
                                                       name="shipping_last_name" class="required">
                                                <span class="error">{{$errors->first('shipping_last_name')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group text span5">
                                            <label for="shipping_address_line1" class="control-label">Address
                                                <sup>*</sup></label>

                                            <div class="controls">
                                                <input type="text" id="shipping_address_line1"
                                                       value="{{Input::old('shipping_address_line1')}}"
                                                       name="shipping_address_line1" class="required">
                                                <span class="error">{{$errors->first('shipping_address_line1')}}</span>
                                            </div>
                                        </div>
                                        <div class="control-group text span5">
                                            <label for="shipping_address_line2" class="control-label">Address
                                                (Line 2)</label>

                                            <div class="controls">
                                                <input type="text" id="shipping_address_line2"
                                                       name="shipping_address_line2"
                                                       value="{{Input::old('shipping_address_line2')}}">
                                                <span class="error">{{$errors->first('shipping_address_line2')}}</span>
                                            </div>
                                        </div>
                                        <div class="control-group text span5">
                                            <label for="shipping_landmark"
                                                   class="control-label">Landmark</label>

                                            <div class="controls">
                                                <input type="text" name="shipping_landmark"
                                                       id="shipping_landmark" class=""
                                                       value="{{Input::old('shipping_landmark')}}">
                                            </div>
                                        </div>

                                        <div class="control-group select span6">
                                            <label for="id_country" class="control-label">Country</label>

                                            <div class="controls" style="padding-top: 8px">
                                                <b>India</b> (Service available only in India)
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="control-group  span5" style="">
                                            <label for="state" class="control-label">State <sup>*</sup></label>

                                            <div class="controls">
                                                <select name="shipping_state" id="shipping_state"
                                                        class="required state"
                                                        onchange="getCities(this.value,'shipping_city','ajax_loader1');">
                                                    <option value="">Select a state</option>
                                                    <?php if (!empty($address) && $address->state) {
                                                        $astate = $address->state;
                                                    } else {
                                                        $astate = '';
                                                    } ?>
                                                    @foreach($states as $state)
                                                        <option value="{{$state->state}}" @if($state->state == $astate)
                                                                selected="selected" @endif>{{ucfirst(strtolower($state->state))}}</option>
                                                    @endforeach
                                                </select>
                                                 <span class="ajax_loader hidden" id="ajax_loader1">
                                                    <img src="{{asset('frontoffice/img/ajax-loader.gif')}}" alt=""/> Wait ...
                                                </span>
                                                <span class="error">{{$errors->first('shipping_state')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group  span5">
                                            <label for="city" class="control-label">City <sup>*</sup></label>

                                            <div class="controls">
                                                <select name="shipping_city" id="shipping_city"
                                                        class="required shipping_city"
                                                        onchange="getPincodes(this.value,'shipping_pincode','ajax_loader2');">
                                                    <?php if (!empty($address) && $address->city) {
                                                        $acity = $address->city;
                                                    } else {
                                                        $acity = '';
                                                    } ?>
                                                    @if(isset($cities))
                                                        @foreach($cities as $city)
                                                            <option value="{{$city->city}}" @if($city->city == $acity)
                                                                    selected="selected" @endif>{{ucwords($city->city)}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value=""></option>
                                                    @endif
                                                </select>
                                                 <span class="ajax_loader hidden" id="ajax_loader2">
                                                    <img src="{{asset('frontoffice/img/ajax-loader.gif')}}" alt=""/> Wait ...
                                                </span>
                                                <span class="error">{{$errors->first('shipping_city')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group span5 postcode " style="">
                                            <label for="shipping_pincode" class="control-label">Zip / Postal Code
                                                <sup>*</sup></label>

                                            <div class="controls">
                                                <select id="shipping_pincode" name="shipping_pincode"
                                                        class="required">
                                                    <?php if (!empty($address) && $address->pincode) {
                                                        $apincode = $address->pincode;
                                                    } else {
                                                        $apincode = '';
                                                    } ?>
                                                    @if(isset($pincodes))
                                                        @foreach($pincodes as $pincode)
                                                            <option value="{{$pincode->postal_code}}" @if($pincode->postal_code == $apincode)
                                                                    selected="selected" @endif>{{$pincode->postal_code}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value=""></option>
                                                    @endif

                                                </select>

                                                <span class="shipping_pincode_not_found"></span>
                                                <span class="error">{{$errors->first('shipping_pincode')}}</span>
                                            </div>
                                        </div>


                                        <div class="control-group text span5">
                                            <label for="shipping_landline" class="control-label">Home
                                                phone</label>

                                            <div class="controls">
                                                <input type="text" id="shipping_landline"
                                                       name="shipping_landline"
                                                       value="{{Input::old('shipping_landline')}}">
                                            </div>
                                        </div>
                                        <div class="control-group text span5">
                                            <label for="shipping_mobile" class="control-label">Mobile phone
                                                <sup>*</sup> </label>

                                            <div class="controls">
                                                <input type="text" id="shipping_mobile" name="shipping_mobile"
                                                       value="{{$user->mobile}}"
                                                       class="required digits" readonly>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                    </fieldset>
                                </div>
                            </div>

                            <!--                        shipping address div ends here-->

                            <!--                        billing address div starts here-->

                            <div class="authentication-page row-fluid">
                                <div class="span12">

                                    <div class="offset2">
                                        <input type="checkbox" name="billing_address"
                                               id="billing_address_check"
                                               checked/>
                                        &nbsp;My billing information is the same as my delivery information
                                    </div>
                                    <br/>

                                    <div class="billing_address_content hidden">
                                        @if(isset($billing_addresses))
                                            <?php $hidden = 'hidden'; ?>
                                            <fieldset>
                                                <h3 class="title_content">Select your billing address here <a
                                                            href="javascript:void(0)" id="add-billing-address-btn"
                                                            class="pull-right btn address-add-button"
                                                            title="add Address"> <i class="fa fa-plus"></i> Add New
                                                        Address
                                                    </a></h3>
                                                @for ($i = 0; $i < count($billing_addresses); $i=$i+3)
                                                    <div class="row-fluid">
                                                        @for($j=0;$j<3;++$j)
                                                            @if($i+$j < count($billing_addresses))
                                                                <div class="span4 shipping_address_box ">
                                                                    <h4><input type="checkbox"
                                                                               name="saved_billing_address"
                                                                               class="billing_address_check"
                                                                               value="{{$billing_addresses[$i + $j]->id}}"/>
                                                                        Address {{($i+$j) + 1}}</h4>
                                                                    <ul>

                                                                        <li>
                                                                            <b>Name:</b>
                                                                            @if($billing_addresses[$i + $j]->first_name != null &&  $billing_addresses[$i + $j]->last_name != null)
                                                                                {{ucfirst($billing_addresses[$i + $j]->first_name) .' '. $billing_addresses[$i + $j]->last_name}}
                                                                            @else
                                                                                {{ucfirst($user->first_name).' '.$user->last_name}}
                                                                            @endif
                                                                        </li>
                                                                        <li>
                                                                            <b>Address: </b> {{$billing_addresses[$i + $j]->line1}}
                                                                            @if(!empty($billing_addresses[$i + $j]->line2))
                                                                                {{$billing_addresses[$i + $j]->line2}}
                                                                            @endif
                                                                        </li>

                                                                        <li>
                                                                            <b>Landmark: </b> @if(!empty($billing_addresses[$i + $j]->landmark)){{$billing_addresses[$i + $j]->landmark}}@else
                                                                                - @endif
                                                                        </li>
                                                                        <li><b>Zip
                                                                                Code: </b> {{$billing_addresses[$i + $j]->pincode}}
                                                                        </li>
                                                                        <li>
                                                                            <b>City: </b> {{$billing_addresses[$i + $j]->city}}
                                                                        </li>
                                                                        <li>
                                                                            <b>State: </b> {{$billing_addresses[$i + $j]->state}}
                                                                        </li>
                                                                    </ul>
                                                                    {{--@if(count($addresses[$i + $j]) > 1)--}}
                                                                    {{--<div class="span2" style="float:right;">--}}
                                                                    {{--<a href="{{URL::to('user/my-addresses/?action=edit&address_id='.$addresses[$i + $j]->id)}}"--}}
                                                                    {{--class="editbtn btn btn-xs btn-action"--}}
                                                                    {{--data-toggle="tooltip"--}}
                                                                    {{--title="Edit Address">--}}
                                                                    {{--<i class="fa fa-pencil"></i>--}}
                                                                    {{--</a>--}}
                                                                    {{--<a href="{{URL::to('user/delete-address/'.$address->id)}}" class="deletebtn" onclick="return confirm('If you click ok your address will deleted permenantly')">--}}
                                                                    {{--<img src="{{asset('frontoffice/img/delete.png')}}" />--}}
                                                                    {{--</a>--}}
                                                                    {{--</div>--}}
                                                                    {{--@endif--}}
                                                                </div>
                                                            @endif
                                                        @endfor

                                                    </div>
                                                    <br/>
                                                @endfor

                                            </fieldset>

                                        @else
                                            <?php $hidden = ''; ?>
                                        @endif
                                        <fieldset class="billing_address_content_form {{$hidden}}"
                                                  id="billing_address_content_form">
                                            <h3 class="title_content">Enter your billing address here</h3>

                                            <div class="control-group text span5">
                                                <label for="billing_first_name" class="control-label">First name
                                                    <sup>*</sup></label>

                                                <div class="controls">
                                                    <input type="text" name="billing_first_name"
                                                           value="{{$user->first_name}}"
                                                           class="required" id="billing_first_name">
                                                    <span class="error">{{$errors->first('billing_first_name')}}</span>
                                                </div>
                                            </div>
                                            <div class="control-group text span5">
                                                <label for="billing_last_name" class="control-label">Last name
                                                    <sup>*</sup></label>

                                                <div class="controls">
                                                    <input type="text" id="billing_last_name"
                                                           value="{{$user->last_name}}"
                                                           name="billing_last_name" class="required">
                                                    <span class="error">{{$errors->first('billing_last_name')}}</span>
                                                </div>
                                            </div>

                                            <div class="control-group text span5">
                                                <label for="billing_address_line1" class="control-label">Address
                                                    <sup>*</sup></label>

                                                <div class="controls">
                                                    <input type="text" id="billing_address_line1"
                                                           name="billing_address_line1" class="required">
                                                    <span class="error">{{$errors->first('billing_address_line1')}}</span>
                                                </div>
                                            </div>
                                            <div class="control-group text span5">
                                                <label for="billing_address_line2" class="control-label">Address
                                                    (Line 2)</label>

                                                <div class="controls">
                                                    <input type="text" id="billing_address_line2"
                                                           name="billing_address_line2">
                                                    <span class="error">{{$errors->first('billing_address_line2')}}</span>
                                                </div>
                                            </div>

                                            <div class="control-group select span6">
                                                <label for="id_country" class="control-label">Country</label>

                                                <div class="controls" style="padding-top: 8px">
                                                    <b>India</b> (Service available only in India)
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="control-group  span5" style="">
                                                <label for="state" class="control-label">State <sup>*</sup></label>

                                                <div class="controls">
                                                    <select name="billing_state" id="billing_state"
                                                            class="required state"
                                                            onchange="getCities(this.value,'billing_city','ajax_loader3');">
                                                        <option value="">Select a state</option>
                                                        <?php if (!empty($address) && $address->state) {
                                                            $astate = $address->state;
                                                        } else {
                                                            $astate = '';
                                                        } ?>
                                                        @foreach($states as $state)
                                                            <option value="{{$state->state}}" @if($state->state == $astate)
                                                                    selected="selected" @endif>{{ucfirst(strtolower($state->state))}}</option>
                                                        @endforeach
                                                    </select>
                                                 <span class="ajax_loader hidden" id="ajax_loader3">
                                                    <img src="{{asset('frontoffice/img/ajax-loader.gif')}}" alt=""/> Wait ...
                                                </span>
                                                    <span class="error">{{$errors->first('billing_state')}}</span>
                                                </div>
                                            </div>

                                            <div class="control-group  span5">
                                                <label for="city" class="control-label">City <sup>*</sup></label>

                                                <div class="controls">
                                                    <select name="billing_city" id="billing_city"
                                                            class="required billing_city"
                                                            onchange="getPincodes(this.value,'billing_pincode','ajax_loader4');">
                                                        <?php if (!empty($address) && $address->city) {
                                                            $acity = $address->city;
                                                        } else {
                                                            $acity = '';
                                                        } ?>
                                                        @if(isset($cities))
                                                            @foreach($cities as $city)
                                                                <option value="{{$city->city}}" @if($city->city == $acity)
                                                                        selected="selected" @endif>{{ucwords($city->city)}}</option>
                                                            @endforeach
                                                        @else
                                                            <option value=""></option>
                                                        @endif
                                                    </select>
                                                 <span class="ajax_loader hidden" id="ajax_loader4">
                                                    <img src="{{asset('frontoffice/img/ajax-loader.gif')}}" alt=""/> Wait ...
                                                </span>
                                                    <span class="error">{{$errors->first('billing_city')}}</span>
                                                </div>
                                            </div>

                                            <div class="control-group span5 postcode " style="">
                                                <label for="pincode" class="control-label">Zip / Postal Code
                                                    <sup>*</sup></label>

                                                <div class="controls">
                                                    <select id="billing_pincode" name="billing_pincode"
                                                            class="required">
                                                        <?php if (!empty($address) && $address->pincode) {
                                                            $apincode = $address->pincode;
                                                        } else {
                                                            $apincode = '';
                                                        } ?>
                                                        @if(isset($pincodes))
                                                            @foreach($pincodes as $pincode)
                                                                <option value="{{$pincode->postal_code}}" @if($pincode->postal_code == $apincode)
                                                                        selected="selected" @endif>{{$pincode->postal_code}}</option>
                                                            @endforeach
                                                        @else
                                                            <option value=""></option>
                                                        @endif

                                                    </select>

                                                    <span class="billing_pincode_not_found"></span>
                                                    <span class="error">{{$errors->first('billing_pincode')}}</span>
                                                </div>
                                            </div>


                                            <div class="control-group text span5">
                                                <label for="billing_landline" class="control-label">Home
                                                    phone</label>

                                                <div class="controls">
                                                    <input type="text" id="billing_landline"
                                                           name="billing_landline">
                                                </div>
                                            </div>
                                            <div class="control-group text span5">
                                                <label for="billing_mobile" class="control-label">Mobile phone
                                                    <sup>*</sup> </label>

                                                <div class="controls">
                                                    <input type="text" id="billing_mobile" name="billing_mobile"
                                                           class="required digits" value="{{$user->mobile}}"
                                                           readonly>
                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>

                                </div>
                            </div>

                            <!--                    billing address div ends here-->

                            <div class="clearfix"></div>
                            <div class="control-group submit2">

                                {{--<div class="span11">--}}
                                {{--@if(count($addresses) > 0)--}}
                                <div class="controls">
                                    <input type="submit" class="addbtn submit exclusive standard-checkout"
                                           value="Continue"/>
                                </div>
                                {{--@endif--}}


                            </div>
                            {{--</div>--}}

                        </form>
                    </div>
                </section>
            </div>
        </div>
        <!-- end div block_home -->
        {{--
          </div>
          </div>--}}
    </section>

@stop