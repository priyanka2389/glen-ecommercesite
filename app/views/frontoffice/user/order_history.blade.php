@extends('layouts.frontoffice.default')

@section('content')


    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">
    <script type="text/javascript"
            src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_overlay.png">
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite.png">
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite@2x.png">

    <style type="text/css">
        a.exclusive:hover {
            color: white !important;
        }

        #discount_price {
            color: #3c763d;
            font-size: 13px
        }

        .table th {
            color: black;
            font-weight: 500;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $("[data-toggle='tooltip']").tooltip();

            $('.view_order').click(function (e) {
                e.preventDefault();
                $order_id = $(this).attr('data-order-id');

                $.fancybox.showLoading({
                    helpers: {
                        overlay: {closeClick: false}
                    }
                });
                $.fancybox.helpers.overlay.open({parent: $('body'), closeClick: false});

                $url = "{{URL::to('order/order-items')}}" + '/' + $order_id;

                $.fancybox({
                    href: $url,
                    type: 'ajax',
                    autoSize: true,
                    helpers: {
                        overlay: {
                            closeClick: true
                        }
                    },
                    afterClose: function () {
                        location.reload();
                        return;
                    },
                    afterOpen: function () {
                        location.reload();
                        return;
                    },
                });

//            $('#order_history_div').css({'margin': '0 auto','width':'940px'});
                $('#order_history_div').css({'margin': '0 auto', 'width': window.innerWidth - 15});
                $('#columns>div').css('float', 'initial');

            });
        });
    </script>

    <div class="container">
        <div class="row-fluid">
            <div class="span12">
                {{Notification::showSuccess()}}
                {{Notification::showError()}}
            </div>
        </div>
    </div>

    <section id="columns" class="clearfix">
        <div class="container" id="order_history_div">
            <div class="row-fluid">
                <section id="center_column" class="span12">
                    <div class="contenttop row-fluid">

                        <h1 id="cart_title" class="title_category custom_h1">Order History</h1>

                        @if(isset($orders) && count($orders) != 0)
                            <div class="span12">
                                <table class="gl-sub-menu2 table" align="left" border="1" cellspacing="0"
                                       cellpadding="3"
                                       style="border:1px solid #bbbbbb; border-collapse:collapse;font-size:14px;">
                                    <tbody>
                                    <tr>
                                        <th>Reference</th>
                                        <th>Net Price</th>
                                        <th>Final Price</th>
                                        <th>Status</th>
                                        <th>Payment Status</th>
                                        <th>Payment Method</th>
                                        <th>Order date</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td>
                                                <p class="s_title_block">
                                                    <a href="">{{$order['reference']}}</a>
                                                </p>
                                            </td>
                                            <td>
                                                <p class="s_title_block">
                                                    <a href="">{{$order['net_value']}}</a>
                                                </p>
                                            </td>
                                            <td>
                                                <p class="s_title_block">
                                                    <a href="">{{round($order['final_value'],2)}}</a>
                                                </p>
                                            </td>
                                            <td>
                                                <p class="s_title_block">
                                                    @if($order['payment_status'] == 'pending' && $order['payment_mode'] == 'cod')
                                                        <a href="">Under Process</a>
                                                    @else
                                                        <a href="">{{$order['status']}}</a>
                                                    @endif
                                                </p>
                                            </td>
                                            <td>
                                                <p class="s_title_block">
                                                    <a href="">{{$order['payment_status']}}</a>
                                                </p>
                                            </td>
                                            <td>
                                                <p class="s_title_block">
                                                    <a href="">{{$order['payment_mode']}}</a>
                                                </p>
                                            </td>
                                            <td>
                                                <p class="s_title_block">
                                                    <a href="">{{$order['created_at']}}</a>
                                                </p>
                                            </td>

                                            <td class="cart_delete">
                                                <p>
                                                    <a href="javascript:void(0);"
                                                       class="cart_quantity_delete view_order" id="view_order"
                                                       data-order-id="{{$order->id}}"
                                                       data-toggle="tooltip" title="View Order Detail">+</a>
                                                </p>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <h4>Your Order History is empty.</h4>
                        @endif
                        <p class="clear">
                            <a href="{{URL::to('user/my-account')}}" title="Back to Login" rel="nofollow"
                               class="button_large">Back to My Account</a>
                        </p>
                    </div>

                    <!-- end div block_home -->
                </section>
            </div>
        </div>
    </section>

@stop