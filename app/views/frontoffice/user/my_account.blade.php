@extends('layouts.frontoffice.default')

@section('content')
{{HTML::style('frontoffice/css/global.css')}}
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<section id="columns" class="columns-container">
    <div class="container">
        <div class="row">
            <!-- Center -->
            <section id="center_column" class="col-md-12">

                <h1 class="custom_h1">My account</h1>

                <p class="info-account">Welcome to your account. Here you can manage all of your personal information
                    and orders.</p>

                <div class="row addresses-lists" id="my-account">
                    <div class="span6">
                        <ul class="myaccount-link-list">
<!--                            <li><a href="http://demo4leotheme.com/prestashop/leo_metro/index.php?controller=address"-->
<!--                                   title="Add my first address"><i-->
<!--                                        class="icon-building"></i><span>Add my first address</span></a></li>-->
                            <li><a href="{{URL::to('order/my-orders')}}"
                                   title="Orders"><i class="icon-list"></i><span>Order history and details</span></a>
                            </li>
                            <li><a href="{{URL::to('user/change-password')}}"
                                   title="Orders"><i class="fa fa-key"></i><span>Change Password</span></a>
                            </li>
                            <li><a href="{{URL::to('user/address')}}"
                                   title="Addresses"><i class="icon-building"></i><span>My addresses</span></a></li>
<!--                            <li><a href="http://demo4leotheme.com/prestashop/leo_metro/index.php?controller=order-slip"-->
<!--                                   title="Credit slips"><i class="icon-ban-circle"></i><span>My credit slips</span></a>-->
<!--                            </li>-->

                        </ul>
                    </div>
                    <div class="span6">
                        <ul class="myaccount-link-list">
<!--                            <li><a href="http://demo4leotheme.com/prestashop/leo_metro/index.php?controller=discount"-->
<!--                                   title="Vouchers"><i class="icon-barcode"></i><span>My vouchers</span></a></li>-->
<!---->
<!--                            <!-- MODULE WishList -->
<!--                            <li class="lnk_wishlist">-->
<!--                                <a href="http://demo4leotheme.com/prestashop/leo_metro/index.php?fc=module&amp;module=blockwishlist&amp;controller=mywishlist&amp;id_lang=1"-->
<!--                                   title="My wishlists">-->
<!--                                    <i class="icon-heart"></i>-->
<!--                                    <span>My wishlists</span>-->
<!--                                </a>-->
<!--                            </li>-->
                            {{--<li><a href="{{URL::to('user/address')}}"--}}
                                   {{--title="Addresses"><i class="icon-building"></i><span>My addresses</span></a></li>--}}
                            {{--<li><a href="{{URL::to('user/address')}}"--}}
                                   {{--title="Information"><i class="icon-user"></i><span>My personal information</span></a>--}}
                            {{--</li>--}}
                            <!-- END : MODULE WishList -->
                        </ul>
                    </div>
                </div>
                <ul class="footer_links clearfix">
                    <li><a href="{{URL::to('/')}}" title="Home"><span><i
                                    class="icon-home"></i> Home</span></a></li>
                </ul>
            </section>

        </div>
    </div>
</section>
@stop