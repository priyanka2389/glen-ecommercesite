<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            function myfunc() {

                $('#form').submit();
            }

            window.onload = myfunc;

        });
    </script>
</head>
<body>

<!--staring of content-->
<div class="container">
    <br><br><br>

    <div class="row ">
        <div class="column twelve centered">
            <h2>Processing you order <img src="{{asset('frontoffice/img/dots.gif')}}" alt=""></h2>

            <p>You will be redirected to the payment gateway shortly to complete the order.</p>


            <p style="color:red">Please do not refresh the page or click the back button of your browser.</p>
        </div>

        <form action="{{URL::to('/order/create-order')}}" method="post" id="form">
            <input type="hidden" name="vpc_Merchant" value="00100336">
            <input type="hidden" name="vpc_Amount" value="{{$total_price*100}}">
            <input type="hidden" name="vpc_MerchTxnRef" value="{{$vpc_MerchTxnRef}}">
            <input type="hidden" name="virtualPaymentClientURL"
                   value="https://migs.mastercard.com.au/vpcpay">
            <input type="hidden" name="vpc_ReturnURL" value="{{URL::to('/order/pg-response')}}">
            <input type="hidden" name="vpc_AccessCode" value="A88559E3">
            <input type="hidden" name="vpc_Version" value="1">
            <input type="hidden" name="vpc_Command" value="pay">
            <input type="hidden" name="vpc_OrderInfo" value="{{$vpc_OrderInfo}}">
            <input type="hidden" name="vpc_Locale" value="en">
        </form>

    </div>
</div>

<!--ending of content-->


</body>
</html>