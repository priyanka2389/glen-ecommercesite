@extends('layouts.frontoffice.default')

@section('content')

<section id="columns" class="clearfix">
    <div class="container">
        <div class="row-fluid">
            <section id="center_column" class="span12">
                <div class="contenttop row-fluid">

                    <div class="authentication-page row-fluid">
                        <div class="span12">
                            <div class="span12">
                                <h1 class="custom_h1">Change password</h1>

                                <div>
                                    <div class="span12">
                                        {{Notification::showSuccess()}}
                                        {{Notification::showError()}}
                                    </div>
                                </div>

                                <form action="{{URL::to('user/change-password')}}" method="post"
                                      class="std form-horizontal" id="form_forgotpassword">
                                    <fieldset>
                                        <div class="control-group text">
                                            <label for="email" class="control-label">Current Password :</label>

                                            <div class="controls">
                                                <input type="password" name="current_password" id="current_password" class="input-xlarge">
                                                <span class="error">{{$errors->first('current_password')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group text">
                                            <label for="email" class="control-label">New Password :</label>

                                            <div class="controls">
                                                <input type="password" name="password" id="password"
                                                       class="input-xlarge" value="">
                                                <span class="error">{{$errors->first('password')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group text">
                                            <label for="email" class="control-label">Confirm Password :</label>

                                            <div class="controls">
                                                <input type="password" name="password_confirmation"
                                                       id="password_confirmation" class="input-xlarge" value="">
                                                <span class="error">{{$errors->first('password_confirmation')}}</span>
                                            </div>
                                        </div>

                                        <div class="control-group submit">
                                            <div class="controls">
                                                <input type="submit" class="button" value="Change Password">
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                                <p class="clear">
                                    <a href="{{URL::to('user/my-account')}}" title="Back to Login" rel="nofollow"
                                       class="button_large">Back to My Account</a>
                                </p>
                            </div>

                        </div>
                    </div>
                    <!-- end div block_home -->
            </section>
        </div>
    </div>
</section>

@stop