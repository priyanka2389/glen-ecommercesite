@extends('layouts.frontoffice.default')

@section('content')

    <script type="text/javascript"
            src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#mobile').prop('readonly', true);

            var form = $('#form');
            form.validate({
                rules: {
                    mobile: {
                        required: true,
                        minlength: 10,
                        maxlength: 10
                    }
                },
                messages: {
                    mobile: {
                        required: "Mobile number is required",
                        minlength: 'Please enter a valid 10 digit mobile number',
                        maxlength: 'Please enter a valid 10 digit mobile number'
                    }
                },
                errorPlacement: function (error, element) {
                    error.appendTo('#mobileError');
                }
            });

            var form = $('#verificationForm');
            form.validate({
                rules: {
                    verification_code: {
                        required: true,
                        minlength: 6,
                        maxlength: 6
                    }
                },
                messages: {
                    mobile: {
                        required: "Verification code is required",
                        minlength: 'Please enter a valid 6 digit verification code',
                        maxlength: 'Please enter a valid 6 digit verification code'
                    }
                },
                errorPlacement: function (error, element) {
                    error.appendTo('#codeError');
                }
            });

            $('#edit').click(function (e) {
                e.stopPropagation();
                var form = $('#form');
                if ($(this).val() == 'Done') {
                    $(this).val('Edit');
                    $('#mobile').prop('readonly', true);
                    $('#mobile').focus();
                    $('#send').prop('readonly', false);
                    $('#is_number_changed').val($('#mobile').val());
                    form.attr('action', '{{URL::to('order/mobile-confirmation/'.$order_id)}}');
                    form.submit();
                    return true;
                    {{--window.location.href = "{{URL::to('order/mobile-confirmation/'.$order_id)}}";--}}
                } else {
                    $(this).val('Done');
                    $('#mobile').prop('readonly', false);
                    return false;
                }
            });

        });
    </script>
    <style type="text/css">
        #verificationForm {
            margin-left: 27px;
        }

        /*label.error{display: block;}*/
    </style>

    <div class="container">
        <div class="row-fluid">
            <div class="span12">
                {{Notification::showSuccess()}}
                {{Notification::showError()}}
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row-fluid">
            <div class="span12">
                <h1>Order Confirmation</h1>
            </div>
        </div>

        <br/><br/>

        <div class="row-fluid justify">

            <form action="#" class="form-horizontal" method="post" id="form">

                <div class="control-group">
                    <label class="control-label" for="mobile">Enter Mobile Number:</label>

                    @if(Session::get('order_confirmation_mobile') != null)
                        <?php $stored_mobile = Session::get('order_confirmation_mobile');  ?>
                    @elseif($mobile != null)
                        <?php $stored_mobile = $mobile; ?>
                    @endif
                    <div class="controls">
                        +91- <input type="text" name="mobile" id="mobile" class="required digits" maxlength="10"
                                    placeholder="Mobile No" value="@if(!empty($stored_mobile)){{$stored_mobile}}@endif">

                        <input type="hidden" name="is_number_changed" id="is_number_changed" value="">
                        <input type="submit" class="submit exclusive standard-checkout" id="edit"
                               value="Edit">

                        {{--<input type="submit" class="submit exclusive standard-checkout" id="send"--}}
                        {{--value="Send Verification Code">--}}
                    </div>
                </div>
                <div class="span3 offset3" id="mobileError"></div>
            </form>
        </div>
        <br/>

        <div class="row-fluid justify">

            <div class="row span12 offset2">
                Enter the <strong>Verification code</strong> sent to your mobile number <strong>@if(!empty($stored_mobile)){{$stored_mobile}}@endif</strong> and click <strong>Verify
                    Code</strong>
            </div>
            <form action="{{URL::to('order/verify-code/'.$order_id)}}" class="form-horizontal"
                  method="post" id="verificationForm">

                <div class="control-group">
                    <label class="control-label" for="verification_code">Enter Verification Code:</label>

                    <div class="controls">
                        <input type="text" name="verification_code" id="verification_code" class="required digits"
                               placeholder="Verification Code" maxlength="6">
                        <input type="submit" class="submit exclusive standard-checkout"
                               value="Verify Code">
                        <a href="{{URL::to('order/resend-verification-code/'.$order_id)}}"
                           class="submit exclusive standard-checkout">Resend Code</a>
                    </div>
                    <div class="span3 offset3" id="codeError"></div>
                </div>
            </form>

        </div>

        <br/><br/>

    </div>


@stop
