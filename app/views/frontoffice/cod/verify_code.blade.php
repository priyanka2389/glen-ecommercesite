@extends('layouts.frontoffice.default')

@section('content')

<script type="text/javascript"
        src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var form = $('#form');
        form.validate();
    });
</script>

<div class="container">
    <div class="row-fluid">
        <div class="span12">
            {{Notification::showSuccess()}}
            {{Notification::showError()}}
        </div>
    </div>
</div>


<div class="container">
    <div class="row-fluid">
        <div class="span12">
            <h1>Code Verification</h1>
        </div>
    </div>

    <br/><br/>

    <div class="row-fluid justify">

        <form action="{{URL::to('order/verify-code/'.$order_id)}}" class="form-horizontal"
              method="post" id="form">

            <div class="control-group">
                <label class="control-label" for="verification_code">Enter Verification Code:</label>

                <div class="controls">
                    <input type="text" name="verification_code" id="verification_code" class="required"
                           placeholder="Verification Code">
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <input type="submit" class="submit exclusive standard-checkout"
                           value="Verify Code">
                    <a href="{{URL::to('order/resend-verification-code/'.$order_id)}}"
                       class="submit exclusive standard-checkout">Resend Code</a>
                </div>
            </div>
        </form>


    </div>

    <br/><br/>

</div>


@stop
