@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}



<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="http://placehold.it/380X442">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Lorem Ipsum</h1>

                        <p class="top-header-p ">Lorem Ipsum is simply dummy text of the printing and typesetting
                            industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when
                            an unknown printer took a galley of type and scrambled it to make a type specimen book. It
                            has survived not only five centuries, but also the leap into electronic typesetting,
                            remaining essentially unchanged.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">


        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span4 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="http://placehold.it/300x268.gif">
                    </div>
                    <div class="row-fluid">
                        <h5>Lorem Ipsum</h5></div>
                    <div class="row-fluid">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                </div>

                <div class="span4 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="http://placehold.it/300x268.gif">
                    </div>
                    <div class="row-fluid">
                        <h5>Lorem Ipsum</h5></div>
                    <div class="row-fluid">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                </div>

                <div class="span4 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="http://placehold.it/300x268.gif">
                    </div>
                    <div class="row-fluid">
                        <h5>Lorem Ipsum</h5></div>
                    <div class="row-fluid">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                </div>


            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop