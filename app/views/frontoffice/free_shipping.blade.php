@extends('layouts.frontoffice.default')

@section('content')
<style type="text/css">
    .shipping_FAQs_div ul li {
        list-style-type: disc;
        margin-left: 25px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="span12">
            <h1>Shipping FAQs</h1>
        </div>
    </div>

    <br/><br/>

    <div class="row justify">

        <div class="span12 shipping_FAQs_div">

            <h3>What are the delivery charges?</h3>

            <p>Glen gives you free shipping on the products bought online.</p>

            <h3>What is the estimated delivery time?</h3>

            <p>We generally procure and ship the items within the time. Business days exclude public holidays and
                Sundays.</p>

            <p>Estimated delivery time depends on the following factors:
            <ul>
                <li>The Product</li>
                <li>Product's availability</li>
                <li>The destination to which you want the order shipped to and location of the courier partner
                    provided.
                </li>
            </ul>
            </p>

            <p>The product will be delivered to you in maximum 7 to 10 days from the date of dispatch. Dispatch may vary
                as per your location or any unforeseen conditions which are beyond our control.</p>

            <h3>Why does the delivery date not correspond to the delivery timeline of X-Y business days?</h3>

            <p>It is possible that our courier partners have a holiday between the day your placed your order and the
                date of delivery, which is based on the timelines shown on the product page. In this case, we add a day
                to the estimated date. Some courier partners do not work on Sundays and this is factored in to the
                delivery dates.</p>

            <h3>Are there any hidden costs (sales tax, octroi etc) on items sold by Glen?</h3>

            <p>There are NO hidden charges when you make a purchase on our website. List prices are final and
                all-inclusive. The price you see on the product page is exactly what you would pay.</p>

            <p>Delivery charges are not hidden charges and are charged (if at all) extra depending on shipping terms and
                conditions.</p>

            <h3>Shipment is not given in my area?</h3>

            <p>Please enter your pincode on the product page (you don't have to enter it every single time) to know
                whether the product can be delivered to your location.</p>

            <p>If you haven't provided your pincode until the checkout stage, the pincode in your shipping address will
                be used to check for serviceability.</p>

            <p>
                Whether your location can be serviced or not depends on
            <ul>
                <li>Whether our courier partner ships to your location</li>
                <li>Legal restrictions, if any, in shipping particular products to your location</li>
                <li>The availability of reliable courier partners in your location</li>
            </ul>
            </p>

            <h3>How can I Order on COD?</h3>

            <p>For Orders on COD a confirmation call will be given by us, order will be dispatched once you confirm the
                same. If COD is not applicable in your area, you can buy the product online on pre-payment and it will
                be delivered.</p>

            <h3>Why is the CoD option not offered in my location?</h3>

            <p>Availability of CoD depends on the ability of our courier partner servicing your location to accept cash
                as payment at the time of delivery.</p>

            <p>Our courier partners have limits on the cash amount payable on delivery depending on the destination and
                your order value might have exceeded this limit. Please enter your pin code on the product page to check
                if CoD is available in your location.</p>

            <h3>I need to return an item, how do I arrange for a pick-up?</h3>

            <p>Returns are easy. Contact Us at 1800-180-1222 to initiate a return. You will receive a call explaining
                the process, once you have initiated a return.
                Return fees are borne by the Glen.

            </p>

            <p>For any details feel free to contact us at – 1800-180-1222, TnC apply</p>
        </div>

    </div>

    <br/><br/>

</div>
@stop