@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}



<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/toaster/toaster.png')}}"
                             class="margin-top-20px">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top40">Toasters</h1>

                        <p class="top-header-p ">Toasts come to our rescue when we are to run for office or just want a quick fix to hunger.
			 To make these toasts upper tasty and super easy, Glen has a range of Toasters. The pop up toasters from Glen India
			 are versatile kitchen appliance with smart settings like browning control, defrost setting, reheat setting, cancel button,
			 hi-lift facility and removable crumb tray. The multiple browning settings provide consistent browning at just the level you prefer.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">

            <div class="">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/toaster/6-level-of-browning-control.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>6 level of browning control</h5></div>
                    <div class="row-fluid">
                        <p></p>
			<p class="feature-description"> Some love their toasts to be sunset brown while some want their toast to be crisp and dark brown.
			 To cater to your personal choice of toasting there are 6 levels of browning in the electric toaster. So now you can get the perfect
			 toast, which as much brown as you want it to be.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/toaster/Defrost-Reheat-&-Cancel-function.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Defrost, Reheat & Cancel function </h5></div>
                    <div class="row-fluid">
                        <p></p>
			<p class="feature-description"> Integrated defrost control in the toaster lets you defrost even the refrigerated bread in no
			 time. With reheat option, you can reheat the toasts if required. And the cancel button comes handy when you want to abort
			 any operation.</p>
                    </div>
                </div>


                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/toaster/Perfect-Browning.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Perfect Browning</h5></div>
                    <div class="row-fluid">
                        <p></p>
			<p class="feature-description"> With Glen India�s bread toaster, you get evenly brown, crisp toasts. Unlike ordinary toasters,
			 it doesn�t allow the burnt flavor and tastes as good as it can get.</p>
                    </div>
                </div>
                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/toaster/Full-Length-Crumbs-Tray.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Full Length Crumbs Tray</h5></div>
                    <div class="row-fluid">
                        <p></p>
			<p class="feature-description"> The toaster comes with spacious extra long crumbs tray so that there is no mess after
			 you have enjoyed the ultimate toast. It is easy to clean and collects all the crumbs.</p>
                    </div>
                </div>
            </div>

        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop