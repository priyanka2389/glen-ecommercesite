@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<?php $session_ids = Session::get('id'); ?>

<style type="text/css">
    #myTabContent #Juicers .span3{width:auto;}

</style>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img
                            src="{{asset('frontoffice/img/food-processor/food-processor.jpg')}}">
                    </div>
                    <div class="span7">
<!--                        margin-top80-->
                        <h1 class="top-header-heading">Food Processor</h1>

                        <p class="top-header-p ">The new Glen food processor GL 4052 is a versatile kitchen machine
                            designed to make working in the kitchen fast, convenient and pleasant. It takes care of all
                            your pre-cooking tasks, making them simple and convenient. It chips, chops, slices, shreds,
                            whisks, kneads, shreds coconut and does much much more. Moreover the smart mixer grinder
                            attachment, with three jars, takes care of all blending, grinding tasks and prepares tongue
                            tickling chutneys too. Prepare fresh & healthy fruit & vegetable juices for friends and
                            family with the juice extractor and the new design citrus juicer</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5" src="{{asset('frontoffice/img/food-processor/Compact-Storage.png')}}">
                </div>
                <div class="row-fluid">
                    <h5>Compact Storage</h5></div>
                <div class="row-fluid">
                    <p class="feature-description">A large capacity revolutionary new bowl, designed without a centre tube. It has a specially
                        designed spindle, which gets locked in the bowl and seals it perfectly. This prevents overflow
                        and spillage of liquids while working or while removing the processing blades.</p>
                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/food-processor/Most-Powerful-Motor.png')}}">
                </div>
                <div class="row-fluid">
                    <h5>Most Powerful Motor</h5></div>
                <div class="row-fluid">
                    <p class="feature-description">Strong & Sturdy 700 W motor makes the toughest of tasks like grinding soaked rice & dals fast,
                        simple and convenient. </p>

                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/food-processor/low-rpm.jpg')}}">
                </div>
                <div class="row-fluid">
                    <h5>Low RPM citrus press</h5></div>
                <div class="row-fluid">
                    <p class="feature-description">The RPM of this specially designed citrus press has been reduced to a minimum for a convenient
                        operation & better juicing of citrus fruits. Moreover the cone is specially positioned at an
                        off-centre position to allow for more space & a better grip while juicing.</p>
                </div>
            </div>


            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/food-processor/Food-Grade-Blade-&-Jars.PNG')}}">
                </div>
                <div class="row-fluid">
                    <h5>Food Grade Blade & Jars </h5></div>
                <div class="row-fluid">
                    <p class="feature-description">3 Jars for liquidising, wet & dry grinding and chutney making. The blades and jars are made of
                        special food grade 304 stainless steel which do not effect the food quality even when they get
                        heated up.</p>

                </div>
            </div>

        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop