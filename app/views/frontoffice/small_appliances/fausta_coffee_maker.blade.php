@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}



<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/coffee-maker/coffee-machine.png')}}">
                    </div>
                    <div class="span7">
<!--                        margin-top80-->
                        <h1 class="top-header-heading ">Coffee maker</h1>

                        <p class="top-header-p ">It stimulates, it refreshes. It can be combined and blended to make a
                            variety of beverages. Coffee has possibilities galore, provided you have a Fausta.
                            This fully Automatic Specialty Coffee Machine combines innovation, technology and aesthetics
                            to present a dependable gadget that gives perfect coffee every time.
                            Simple to use, it has multiple programming options so you can have a different coffee every
                            day: create your own personal version, or follow a popular blended speciality.
                        </p>

                        <p>Fausta: From an Americano to a Latte Macchiato to a Ristretto – the perfect cup of coffee at
                            the touch of a button! </p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/coffee-maker/Feature-1.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Intelligent touch control system </h5></div>
                    <div class="row-fluid">
                        <!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/coffee-maker/Feature-2.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Convenient rotary button</h5></div>
                    <div class="row-fluid">
                        <!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/coffee-maker/Feature-3.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Double heating system</h5></div>
                    <div class="row-fluid">
                        <!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/coffee-maker/Feature-4.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Accurate grinding system</h5></div>
                    <div class="row-fluid">
                        <!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop