@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
    .top-header-p1{font-size: 13px;}
</style>

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/egg-master/egg-master.png')}}">
                    </div>
                    <div class="span7">
                        <!--                        margin-top80-->
                        <h1 class="top-header-heading ">Egg Master</h1>

                        <p class="top-header-p ">Now it’s very easy to get the health benefits of eggs without any hassle. Glen India makers of
                            smart kitchen appliances bring you Egg Master which helps you cook eggs perfectly every time. It makes use of vertical cooking technology, which
                            combines a cylindrical non-stick cooking surface surrounded by a revolutionary heating element. The unique silicone rubber grip allows safe handling
                            and makes it extremely easy to clean. Just pour the eggs in the special non-stick chamber, switch on the egg master and watch the eggs pop up in the
                            shape of a roll, perfectly cooked and easy to carry. No more getting late for office. Now omelets can be cooked by anyone without any risk and mess.
                            You can also cook bacon, eggs, burritos, cinnamon rolls and more.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/egg-master/feature_1.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Non-stick coating</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The egg maker has an effective non-stick coating, which makes it easy to clean and
                            cook with least amount of oil.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/egg-master/feature_2.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Power</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The egg master has an intuitive power indicator. The red light indicator gets on
                            when power is on. When egg master is ready, the green light turns ON. Now, you no longer have to put a timer for your egg preparation.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/egg-master/feature_3.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Safe handling</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The electric egg roll maker has the unique silicone rubber grip, which acts as a heat
                            insulator for safe handling.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/egg-master/feature_4.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Material</h5></div>
                    <div class="row-fluid">
                        <p class="feature-description">The egg roll maker is made of thermoplastic material, which is free of any health hazard.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop