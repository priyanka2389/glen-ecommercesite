@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}



<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img
                            src="{{asset('frontoffice/img/tandoor/tandoor.png')}}">
                    </div>
                    <div class="span7">
<!--                        margin-top80-->
                        <h1 class="top-header-heading ">Tandoors</h1>

                        <p class="top-header-p ">Tandoor is the new buzzword for healthier food. The Glen electric tandoor redefines the tender
			 with its stainless steel, sturdy framework, and smart appearance. And behind this hides a robust heating system that cooks
			 food thoroughly at very high temperature, for that real tandoori taste.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5" src="{{asset('frontoffice/img/tandoor/Saves-Energy-only-1100-W.png')}}">
                </div>
                <div class="row-fluid">
                    <h5>Saves energy, Only 1100W</h5></div>
                <div class="row-fluid">
		<p class="feature-description"> Unlike ordinary electric tandoors, Glen India�s tandoor is not power hungry. It works only
		 at 1100W and hence helps conservation of energy. You enjoy delectable tandoori kebabs and save energy too!</p>
<!--                    <p>Bold and distinctive. Premium materials are combined to create a statement hood that provides an-->
<!--                        unrivalled blend of performance and looks.</p>-->
                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5" src="{{asset('frontoffice/img/tandoor/traditional-clay-oven.jpg')}}">
                </div>
                <div class="row-fluid">
                    <h5>Similar to traditional Clay Oven</h5></div>
                <div class="row-fluid">
			<p class="feature-description"> The Glen stainless steel tandoor is made of the highest quality stainless steel to make it
			 longer lasting, as the normal problem of ordinary Tandoors is that these get rusted within a couple of months. Glen uses all parts,
			 even the screws in stainless steel to avoid the rusting. Also with its superior technology as it provides the same age old homely
			 taste of clay oven, which makes you go nostalgic.</p>
                    <p class="feature-description">Bold and distinctive. Premium materials are combined to create a statement hood that provides an
                        unrivalled blend of performance and looks.</p>
                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/tandoor/heavy-duty.jpg')}}">
                </div>
                <div class="row-fluid">
                    <h5>SS Heating Elements</h5></div>
                <div class="row-fluid">
                    <p class="feature-description">Stainless steel tubular heating elements of the tandoor make the food crisp while 
			retaining the natural food juices. You get the same texture of food as you cook over the clay in the comfort of your home.</p>
                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/tandoor/three-position.jpg')}}">
                </div>
                <div class="row-fluid">
                    <h5>Three cooking positions</h5></div>
                <div class="row-fluid">
                    <p class="feature-description">Three heat selections are provided in the Glen tandoor, for effective cooking of different
		 delicacies. Whether you want tandoori chicken or tandoori sweet potato or tandoori paneer, there is an appropriate cooking
		 position for the best taste.</p>

                </div>
            </div>

        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop