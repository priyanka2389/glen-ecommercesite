@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
@media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/air-fryer/air-fryer.jpg')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Air fryers</h1>

                        <p class="top-header-p ">Indian cuisine is incomplete without fried food. But fried food is laden with calories and is not recommended
 for sedentary lifestyle. Glen India brings you a revolutionary air fryer, which instead of using fat to make fries uses superheated air. It fries food to the
 same golden brown color like oil, but cuts calories as much as 70-85%. You can fry practically anything; be it fries, snacks, chicken, burgers or meat, all
 taste same as in oil based frying except with less calories. With this innovative electric fryer you can relish that same mouth-watering taste "crispy on the
 outside, moist on the inside" - without the oil.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/air-fryer/Large-LCD-Display.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Large LCD Display</h5></div>
                    <div class="row-fluid">
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
 text ever since the 1500s.</p>-->
<p class="feature-description">
                        The Glen India electric air fryer comes with a large LCD display, where you can get information about the progress of frying,
 time left and much more. This interactive LCD display lets you toggle between six temperature settings and keep time.
                    </p>

                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/air-fryer/Basket-release-button.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Basket release button</h5></div>
                    <div class="row-fluid">
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
 dummy text ever since the 1500s.</p>-->
                    <p class="feature-description">
The Glen India snack maker comes with a smart basket release button. You can eject the basket with fried food very easily. All you need to do is push the button and the basket gets separated easily.
</p>

                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/air-fryer/Removable-basket.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Removable basket</h5></div>
                    <div class="row-fluid">
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
 text ever since the 1500s.</p>-->
                    <p class="feature-description">
The electric air fryer has a spacious food basket, which can be removed to serve the contents directly from it or for cleaning purposes. Now you don�t have
 to dig into the basket to take the contents out. You can remove or insert back the basket conveniently.
</p>


                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('frontoffice/img/air-fryer/Food-separator.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Food separator</h5></div>
                    <div class="row-fluid">
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
 text ever since the 1500s.</p>-->
<p class="feature-description">
Glen Air fryer comes with food separator so that you can fry two different items simultaneously, without any mix up. The compartments of the basket are
 spacious enough to house two items with serving size for four.
</p>

                    </div>
                </div>


            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop