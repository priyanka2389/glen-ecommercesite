@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}



<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img
                            src="{{asset('frontoffice/img/kettle-tea-maker/kettle.jpg')}}">
                    </div>
                    <div class="span7">
<!--                        margin-top80-->
                        <h1 class="top-header-heading ">Kettles & Tea Makers</h1>

                        <p class="top-header-p ">The Kettle That Makes Tea, Perfectly For the perfectly balanced brew,
                            different teas need to be steeped for different time durations.
                            The Glen Tea Maker makes this convenient, just move the stainless steel tea filter assembly
                            up and down, gently agitating the leaves to precisely infuse your tea. Now, enjoy a perfect
                            cup of tea, every time.
                            Detach the filter assembly and it's a glass kettle. Perfect kettle. Perfect tea.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5" src="{{asset('frontoffice/img/kettle-tea-maker/360-degree-rotational-base.png')}}">
                </div>
                <div class="row-fluid">
                    <h5>360° Degree Rotational base</h5></div>
                <div class="row-fluid">
                    <p></p>
                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/kettle-tea-maker/Lockable-hinge-lid.png')}}">
                </div>
                <div class="row-fluid">
                    <h5>Lockable hinged lid</h5></div>
                <div class="row-fluid">
                    <p></p>
                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/kettle-tea-maker/Water-level-gauge-easy-monitoring.png')}}">
                </div>
                <div class="row-fluid">
                    <h5>Water level gauge easy monitoring</h5></div>
                <div class="row-fluid">
                    <p></p>

                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/kettle-tea-maker/Comfortable-&-stylish-handle.png')}}">
                </div>
                <div class="row-fluid">
                    <h5>Comfortable & stylish handle</h5></div>
                <div class="row-fluid">
                    <p></p>

                </div>
            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop