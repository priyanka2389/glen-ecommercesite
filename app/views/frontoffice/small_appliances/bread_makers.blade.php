@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}

<style type="text/css">
@media (min-width: 768px) and (max-width: 979px) {.top-header-heading{margin-top:10px;}}
</style>

<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img
                            src="{{asset('frontoffice/img/bread-maker/bread-maker.jpg')}}">
                    </div>
                    <div class="span7">
                        <h1 class="top-header-heading margin-top80">Bread Maker</h1>

                        <p class="top-header-p ">Enjoy the delicious aroma of freshly baked bread in your own kitchen
                            every day: with the Glen bread maker.
                            This intelligent appliance takes care of every stage of dough preparation for you. All you
                            have to do is pour in the ingredients and, after an hour or two, your favourite, aromatic
                            bread will be ready.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5" src="{{asset('frontoffice/img/bread-maker/detachable-pan.jpg')}}">
                </div>
                <div class="row-fluid">
                    <h5>Detachable non-stick baking pan</h5></div>
                <div class="row-fluid">
                    <p></p>
                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/bread-maker/electronic-panel.jpg')}}">
                </div>
                <div class="row-fluid">
                    <h5>Electronic control panel</h5></div>
                <div class="row-fluid">
                    <p></p>
                </div>
            </div>
            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/bread-maker/Large-Viewing-Window1.png')}}">
                </div>
                <div class="row-fluid">
                    <h5>Large Viewing Window</h5></div>
                <div class="row-fluid">
                    <p></p>
                </div>
            </div>

            <div class="span3 chimney-category-features-content margin-right-0px">

                <div class="row-fluid">
                    <img class="span12 padding5"
                         src="{{asset('frontoffice/img/bread-maker/Keeps-bread-warm-function.png')}}">
                </div>
                <div class="row-fluid">
                    <h5>Keeps bread warm function</h5></div>
                <div class="row-fluid">
                    <p></p>

                </div>
            </div>

        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop