@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}



<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('/frontoffice/img/heaters/ptc-heater/ptc-heater.jpg')}}">
                    </div>
                    <div class="span7">
<!--                        margin-top80-->
                        <h1 class="top-header-heading ">PTC heaters</h1>

                        <p class="top-header-p ">The revolutionary PTC (Positive Temperature Coefficient) technology is the most efficient way of room heating.
			 It uses small ceramic stones with self-limiting temperature characteristics, which have fast heating response times and plateau at
			 pre-defined temperature. PTC heating elements automatically vary wattage to maintain pre-set temperature, thereby saving power. They are
			 absolutely safe, as irrespective of magnitude of current applied to the PTC, it will never surpass the set temperature. PTC ceramic stones
			 are highly reliable, have long life and are compact in design so PTC Heaters not just look good but perform better.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('/frontoffice/img/heaters/ptc-heater/Multi-function-remote-control.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Multi-function remote control</h5></div>
                    <div class="row-fluid">
			<p class="feature-description">Why get up from your cozy quilt to control temperature? The Glen�s PTC room heaters come with
			 a multi-function remote control, to switch on or off the heater and change the heating level.</p>
                        <!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('/frontoffice/img/heaters/ptc-heater/PTC-Technology.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>PTC Technology</h5></div>
                    <div class="row-fluid">
			<p class="feature-description"> PTC heaters don�t let the temperature go beyond the set temperatures, unlike ordinary heaters where
			 the temperature can shoot up to dangerous levels. It means that it's safer to use PTC room heaters and save on the power bill also. 
			The PTC ceramic stones cool down promptly unlike resistance wires.</p>
                        <!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>


                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('/frontoffice/img/heaters/ptc-heater/90-degree-oscillation.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>90 degree oscillation</h5></div>
                    <div class="row-fluid">
			<p class="feature-description"> Ordinary heaters are stationary and allow development of cold pockets behind them. The innovative
			 PTC ceramic heaters can swing by 90 degrees and hence warm up the room uniformly. Also, you can control the action by multi-function
			 remote control.</p>
                        <!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('/frontoffice/img/heaters/ptc-heater/Tlit_Sensor_for_Safety.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Tlit Sensor for Safety</h5></div>
                    <div class="row-fluid">
 			<p class="feature-description">For your safety, the positive temperature coefficient heater comes with a tilt sensor.
			 It provides automatic switching off of the electric room heater, when it falls or is not placed over a uniform surface.</p>
                        <!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>


            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop