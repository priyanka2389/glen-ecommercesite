@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}



<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('/frontoffice/img/heaters/oil-filled/heater.jpg')}}">
                    </div>
                    <div class="span7">
<!--                        margin-top80-->
                        <h1 class="top-header-heading ">OFR heaters</h1>

                        <p class="top-header-p ">Oil Filled Radiators are convection room heaters, which work by electrically heating oil
			 that acts as a heat reservoir. The hot oil heats the metal walls and in turn the surroundings via convection and 
			radiation. The thin fin columns provide a large surface area to allow more air to be in contact with the heater and help
			 rapid transfer of heat into the room. OFR room heater is a safe environment friendly method of heating that does not involve
			 any fuel burning. Oil filled radiators do not reduce oxygen nor do they cause any dryness in the room. Glen Oil Filled Radiator
			 Heaters are designed to provide fast and flexible heating to any indoor space. They are highly efficient, safe and provide long
			 lasting heating. With a 400 W ceramic fan they spread their warmth instantly.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
        {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('/frontoffice/img/heaters/oil-filled/ISI-Certified.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>ISI Certified</h5></div>
                    <div class="row-fluid">
				<p class="feature-description">The electric heaters are certified from Indian standards and comply with
				 high standards of manufacturing and safety. The OFR air heater is safe to use even for longer durations and
				 does not produce any toxic fumes.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('/frontoffice/img/heaters/oil-filled/Turbo-Boost-Ceramic-Fan.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Turbo Boost Ceramic Fan</h5></div>
                    <div class="row-fluid">
			 <p class="feature-description"> The Glen oil radiator room heater comes with a turbo PTC fan. It boasts of superior heat
			 dispersion and the quickest heating possible. Also the fan doesn�t allow deposition of grime and dust because of ceramic 
			material that it is built of.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('/frontoffice/img/heaters/oil-filled/Fins.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Thermal Fins</h5></div>
                    <div class="row-fluid">
			<p class="feature-description">The oil room heater bears thermal fins, which provide large surface for speedy heating
			 instantly. The air around electric heater is warmed up uniformly and no cold pockets remain after a few minutes.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('/frontoffice/img/heaters/oil-filled/In-built-handle.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>In-built handle</h5></div>
                    <div class="row-fluid">
			<p class="feature-description"> The Glen OFR electric room heater is provided with in built handle, so that you can move
			 and place the OFR heater elsewhere conveniently. It comes with castor wheels and is very ease to move.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop