@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}



<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('/frontoffice/img/steam-iron/iron.jpg')}}">
                    </div>
                    <div class="span7">
<!--                        margin-top80-->
                        <h1 class="top-header-heading ">Irons</h1>

                        <p class="top-header-p ">Glen steam iron lets you adjust steam settings for a variety of fabrics.
			 The fine mist spray gives sharper creases and the non-stick coated sole plate provides for smooth ironing results.
			 Glen India iron with steam heats up fast, is lightweight, and has an anti slip handle. The soleplate features full-length
			 button grooves, which make ironing around buttons easier.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row-fluid">
            <div class="span12">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('/frontoffice/img/steam-iron/360-degree-Swivel-Cord.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>360 degree Swivel Cord</h5></div>
                    <div class="row-fluid">
 			<p class="feature-description">The Glen steam iron is provided with a cord that can swivel on its base by 360 degrees.
			 Now you don�t have to bear entangled cords, which become too short to carry iron from the power plug to ironing table.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('/frontoffice/img/steam-iron/Online-Water-Filling.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Easy Water Filling</h5></div>
                    <div class="row-fluid">
 			<p class="feature-description">The water inlet is ergonomically designed to provide spill proof, water streaming
			 into the iron.  The markings on the steam iron let you pour the exact amount of water, no more and no less.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('/frontoffice/img/steam-iron/Burst-of-Steam.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Burst of Steam </h5></div>
                    <div class="row-fluid">
			<p class="feature-description">Steam iron for clothes comes with an innovative mechanism to produce a uniform
			 burst of steam, which uniformly moistens the clothes for taut ironing.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5" src="{{asset('/frontoffice/img/steam-iron/Vertical-Ironing.png')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Vertical Ironing </h5></div>
                    <div class="row-fluid">
			<p class="feature-description">The steam iron can be efficiently doubled as dry iron. It also allows vertical
			 ironing for quick fixes to your wrinkled shirt or saree. The steam effusion is equally effective in vertical mode.</p>
<!--                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>-->
                    </div>
                </div>


            </div>
        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop