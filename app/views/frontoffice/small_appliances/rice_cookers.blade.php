@extends('layouts.frontoffice.default')

@section('content')

{{HTML::style('frontoffice/themes/leometr/cache/7908d2ebd930903fc4a31e0ff2a9ac57_all.css')}}
{{HTML::style('frontoffice/css/blocklayered-15.css')}}
{{HTML::style('frontoffice/css/accordian.css')}}
{{HTML::script('frontoffice/js/accordian.js')}}



<?php $session_ids = Session::get('id'); ?>

<section id="promotetop">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayPromoteTop" class="leo-manage">
                <div class="row-fluid">
                    <div class="span5">
                        <img src="{{asset('frontoffice/img/rice-cooker/rice-cooker.jpg')}}"
                             class="margin-top-20px">
                    </div>
                    <div class="span7">
<!--                        margin-top40-->
                        <h1 class="top-header-heading">Rice Cooker</h1>

                        <p class="top-header-p ">Rice is a staple diet and it is probably the food item, which is prepared in the maximum quantity.
			 Glen India brings you the rice cooker, which allows the enthusiastic cook to steam up a perfect pot of rice at the flick of
			 a switch. This smart rice steamer can be used to cook desserts, boil eggs, and prepare dim sum, steam fish, meat and vegetables.
			 By cooking in electric rice cooker, the food tastes the freshest. The rice cooker comes with design features like cool touch body
			 and sleek handles. With durable, nonstick cooking pans, lockable hinged lids and single switch operation; the Glen rice cookers
			 are simply the best choice for healthy cooking.</p>

                        <div class="top-header-p-border"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="columns" class="clearfix">

    <div class="container">
        <!--category sidebar  -->
         {{$sidebar_html}}

        <!-- features showcase starts here -->
        <div class="row">

            <div class="">
                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/rice-cooker/cool-touch-handle.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Cool Touch Handle</h5></div>
                    <div class="row-fluid">
                        <p></p>
			<p class="feature-description"> Unlike ordinary steam cookers, the Glen India steam rice cooker comes with cool touch handle.
			 It allows you to freely touch the handle for moving appliance or any other purpose even while cooking.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/rice-cooker/control-panel.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Control Panel</h5></div>
                    <div class="row-fluid">
                        <p></p>
			<p class="feature-description"> The rice cooker comes with a control panel to indicate the cook or warm functions. It makes it
			 easy to toggle when you want to cook rice or warm it before serving. The different color LED makes it easy to understand even 
			from a distance.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/rice-cooker/cooking-pans.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Durable non stick cooking pan</h5></div>
                    <div class="row-fluid">
                        <p></p>
			<p class="feature-description"> The rice cooker is provided with a durable nonstick cooking pan. The round and rimmed
		 	edges prevent from any spilling.</p>
                    </div>
                </div>

                <div class="span3 chimney-category-features-content margin-right-0px">

                    <div class="row-fluid">
                        <img class="span12 padding5"
                             src="{{asset('frontoffice/img/rice-cooker/inner-steamer.jpg')}}">
                    </div>
                    <div class="row-fluid">
                        <h5>Inner Steamer</h5></div>
                    <div class="row-fluid">
                        <p></p>
			<p class="feature-description"> The electric rice cooker comes with a steamer attachment. This attachment can be used
			 for steaming foods like vegetables, idly, dhokla etc.</p>
                    </div>
                </div>

            </div>

        </div>
        <!-- features showcase ends -->

        <div class="clearfix"></div>

        <!--    products partial starts here-->
        @include('_partials.frontoffice.category_page_products')

    </div>
</section>

@stop