@extends('layouts.frontoffice.default')

@section('content')

<style type="text/css">

    .states_list a {
        line-height: 18px;
    }

    .state_row {
        background-color: #e8e8e8;
        padding: 5px;
        text-align: center !important;
        /*border-top:16px solid white;*/
        /*border-bottom:16px solid white;*/
    }

    .hr {
        border-bottom: 1px solid #e8e8e8;
        margin-top: 2px;
        margin-bottom: 2px;
    }

    strong {
        font-family: Arial, Helvetica, sans-serif;
    }

    .no_border_row > td {
        border: none !important;
    }

    .no_border_row > td > a {
        color: #17479d;
    }

    .no_border_row > td > a:hover {
        text-decoration: underline;
    }

    .right {
        float: right !important;
    }

    a[href^='mailto:'] {
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 13px;
        letter-spacing: 0.1px;
        color: #17479d;
    }


</style>

<div class="container">
<div class="row">
    <div class="span12">
        <h1>Service Centers</h1>
    </div>
</div>

<br/>

<div class="row justify">
<div class="span12">

<table cellspacing="10" cellpadding="0" width="100%" class="table">
<tbody>
<tr class="no_border_row">
    <td align="center" colspan="3"><a name="begin"></a>
        <table cellspacing="0" cellpadding="0" width="100%">
            <tbody>

            <tr class="states_list no_border_row">
                <td valign="top"><a href="#andhra">ANDHRA PRADESH</a><br>
                    <a href="#assam">ASSAM</a><br>
                    <a href="#bihar">BIHAR</a><br>
                    <a href="#chattishgarh">CHATTISHGARH</a><br>
                    <a href="#Delhi-NCR">DELHI-NCR</a><br>
                    <a href="#goa">GOA</a><br>
                    <a href="#gujarat">GUJARAT</a></td>
                <td valign="top"><a href="#himachalpradesh">HIMANCHAL PRADESH</a><br>
                    <a href="#haryana">HARYANA</a><br>
                    <a href="#jammu">JAMMU &amp; KASHMIR</a><br>
                    <a href="#jharkhand">JHARKHAND</a><br>
                    <a href="#kerala">KERALA</a><br>
                    <a href="#karnataka">KARNATAKA</a><br></td>
                <td valign="top"><a href="#madhyapradesh">MADHYA PRADESH</a><br>
                    <a href="#maharashtra">MAHARASHTRA</a><br>
                    <a href="#orissa">ORRISA</a><br>
                    <a href="#punjab">PUNJAB</a><br>
                    <a href="#rajasthan">RAJASTHAN</a><br>
                    <a href="#tamilnadu">TAMIL NADU</a><br></td>




                <td valign="top"><a href="#uttarpradesh">UTTAR PRADESH</a><br>
                    <a href="#uttaranchal">UTTARANCHAL</a><br>
                    <a href="#westbengal">WEST BENGAL</a><br></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
</tr>
<tr class="no_border_row">
    <td width="17%"><strong>Location</strong></td>
    <td width="17%">&nbsp;</td>
    <td width="66%"><strong>Name &amp; Address</strong></td>
</tr>

<tr>
    <td align="center" class='state_row' colspan="3"><a name="andhra"><strong>ANDHRA PRADESH</strong></a></td>
</tr>

<tr>
    <td><strong>Hyderabad</strong></td>
    <td>Small Appliances</td>
    <td><strong>Bright Engineering Works Pvt Ltd</strong><br>
        # 7-4-195/1 &amp; 3 Geetha Nagar Beside Bharat Lodge,<br>
        Near BHEL R&amp;D Balanager<br>
        Secunderabad -011<br>
        Ph.: 040-23774648 , 040-23771842<br>
    </td>
</tr>
<tr>

</tr>

<tr>
    <td><strong>Hyderabad</strong></td>
    <td>All Products</td>
    <td><strong>Pioneer Enterprises</strong><br>
        No. 12-12-208, Ravindra Nagar Nolony,<br>
        Seethephal Mandi, Secundrabad<br>
        Mob.: 9849809254<br>
        Ph.: 40077093<br>
        Contact Person :- Mr. Riyaz
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Tirupati</strong></td>
    <td>All Products</td>
    <td><strong>Tirumala Ceramics</strong><br>
        Rayala Cheru Road,Opp. Vinayakaswamy Temple,<br>
        Tirupati - 517501<br>
        Opp. Vinayakaswamy Temple,Tirupati - 517501<br>
        Ph.: 0877-2000188 <br>
        Mob.: 9346981505<br>
        Contact Person:- Mr. Doriraj
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Vijaywada</strong></td>
    <td>All Products</td>
    <td><strong>Businex</strong><br>
        24-14-42, Sougandhika Durgapuram<br>
        Vijawada - 52003<br>
        Mob.: 9849999995<br>
        Contact Person:- Mr. Srikanth<br>
        E-Mail:- <a href="mailto:srikanthnallamothu@yahoo.com">srikanthnallamothu@yahoo.com</a></td>
</tr>
<tr>

</tr>
<tr>

</tr>
<tr>
    <td><strong>Vijaywada</strong></td>
    <td>All Products</td>
    <td><strong>Mithra Marketing</strong><br>
        Shop no-3,beside federal bank,<br>
        Near Nirmala high school,<br>
        Near NTR circle, Vijayawada-520010,<br>
        Mob.: 09494399626<br>
        Contact Person:- Mr.Prabhu vikas<br>
        E-Mail:- <a href="mailto:mithramarketing1@gmail.com">mithramarketing1@gmail.com</a></td>
</tr>


<tr>

</tr>

<tr>
    <td><strong>Rajahmundry</strong></td>
    <td>All Products</td>
    <td><strong>Kitchen Friends Marketing</strong><br>
        35-1-84, Kanchumartyvaristreet,<br>
        Opp Big Masque,<br>
        Beside Manyam Naveen Electricals
        Rajamundry.<br>
        Mob.: 8374695097<br>
        Contact Person:- A.chiranjeevi<br>
        E-Mail:- <a href="mailto:akula.chiranjeevi@gmail.com">akula.chiranjeevi@gmail.com</a></td>
</tr>


<tr>

</tr>
<tr>
    <td><strong>Vishakhapatnam</strong></td>
    <td>All Products</td>
    <td><strong>M/s Ankush Marketing</strong><br>
        Branch: 31-31-6, Ground Floor,<br>
        Sai Baba Street, Dabagardens,<br>
        Vishakhapatnam - 530020,<br>
        Ph.: 0891- 6056999,<br>
        Mob.: 8970002021<br>
        Contact Person:- Mr. Vishal Jain<br>
        E-Mail:- <a href="mailto:INFOANKUSHMARKEITNG@GMAIL.COM">INFOANKUSHMARKEITNG@GMAIL.COM</a></td>
</tr>


<!--<tr>

  </tr>
<tr>
    <td><strong>Vishakhapatnam</strong></td>
    <td>Large Appliances</td>
    <td><strong>M/s Sri Lalitambica Marketing</strong><br />
      D.No: 43-11-5, First Floor,<br />
      Beside Indian Oil Petrol Pump,<br />
      Subbalaxmi Nagar, Vishakhapatnam-530016<br />
      Mob.: 9705435777<br />
      Contact Person:- Mr. PV Kanaka Kumar</td></tr>-->

<tr>
    <td class='state_row' align="center" colspan="3"><a name="assam"><strong>ASSAM</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Guwahati</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Global Associates</strong><br>
        Garima Royale,<br>
        2nd Lift, 1st Floor,<br>
        Opp, DIG Office,<br>
        Danish Road, Pan Bazar,<br>
        Guwahati- 781001.<br>
        Mob.: 9613872504<br>
        E-mail:- globalservicecentre1@gmail.com
    </td>
</tr>
<tr>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Jorhat</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Solar-Tech</strong><br>
        L.M.Market,J.B.Road,<br>
        Jorhat-785001.<br>
        Mob.: 8876635141 , 9678372324<br>
        E-mail :- solartechmultiservice@gmail.com
    </td>
</tr>
<tr>

    <td align="center" colspan="3" class='state_row'><a name="bihar"><strong>BIHAR</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Patna</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Global Sales &amp; service</strong><br>
        Shivpuri (Chitkohra),<br>
        Anisabad, Patna - 800002 <br>
        Mob.: 9431422024, 9304363341<br>
        E-mail:- <a href="mailto:salesserviceglobal@gmail.com  ">salesserviceglobal@gmail.com </a><br>
        Contact Person:- Mr. Akhilesh kumar
    </td>
</tr>
<tr>

</tr>

<tr>
    <td align="center" colspan="3" class='state_row'><a name="chattishgarh"><strong>CHATTISHGARH</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Jabalpur</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Nigam Gas &amp; Stove Centre</strong><br>
        305, Karamchand Chowk, Jabalpur<br>
        Mob.: 9893006059<br>
        Contact Person:- Mr. Vishnu Nigam
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Raipur</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>M/S Shashikala Enterprises</strong><br>
        House No. 682, Sundar Nagar,<br>
        Raipur, C.G – 492013<br>
        <!--Ph.: 0771-4075178<br />
       Mob.: 7869233842<br/>
       E-mail:- <a href="mailto:shashikalaenterprises66@gmail.com">shashikalaenterprises66@gmail.com</a></td>-->
        Contact Person- Mr. Amit Jha<br>
        Mob.: 7898993563, 9200559200<br>
        E-mail:- <a href="mailto:mithilasolutions2013@gmail.com">mithilasolutions2013@gmail.com</a></td>

</tr>
<tr>
    <td align="center" colspan="3" class='state_row'><a name="Delhi-NCR"><strong>DELHI-NCR </strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Delhi &amp; NCR</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Glen Appliances Pvt. Ltd.</strong><br>
        I-34, DLF Industrial Area,<br>
        Phase-1, Faridabad.<br>
        Toll Free No.: 18001801998<br>
        Contact Person:- Miss Mamta<br>
        E-mail:- <a href="mailto:service@glenindia.com">service@glenindia.com</a></td>
</tr>

<tr>

</tr>
<tr>
    <td align="left"><strong>North Delhi</strong></td>
    <td align="left">Large Appliances</td>
    <td align="left"><strong>M/s Pant Service</strong><br>
        WZ-97B, Tek Chand Complex,<br>
        Jwala Heri Market, Paschim Vihar<br>
        Delhi<br>
        Mob.: 8745017772, 8505975888, 9266673236<br>

        <!-- Mob.:  8744019111 , 8744004139<br />
         Ph.:  011-25258464 , 25274252<br />-->
        Contact Person.: Mr. LR Pant<br>
        E-mail:- <a href="mailto:delhi.pantpv.bajaj@gmail.com">delhi.pantpv.bajaj@gmail.com</a></td>
</tr>


<tr>

</tr>
<tr>
    <td align="left"><strong>West Delhi</strong></td>
    <td align="left">Small Appliances</td>
    <td align="left"><strong>M/s Pant Service</strong><br>
        WZ-97B, Tek Chand Complex,<br>
        Jwala Heri Market, Paschim Vihar<br>
        Delhi<br>
        Mob.: 8745017772, 8505975888, 9266673236<br>
        <!--Mob.:  8744019111 , 8744004139<br />
         Ph.:  011-25258464 , 25274252<br />-->
        Contact Person.: Mr. LR Pant<br>
        E-mail:- <a href="mailto:delhi.pantpv.bajaj@gmail.com">delhi.pantpv.bajaj@gmail.com</a></td>
</tr>

<tr>

</tr>
<tr>
    <td align="left"><strong>West Delhi</strong></td>
    <td align="left">Large Appliances</td>
    <td align="left"><strong>M/s MS Services</strong><br>
        C-2 Mahandera Park, Shop No-2,<br>
        Pankha Road, Opp. C-1, Janakpuri Bus Stand<br>
        New Delhi-59<br>
        Mob.: 9268827725 , 8826564248<br>
        Contact Person:- Mr. Murari Singh<br>
        E-mail:- <a href="mailto:murari766@gmail.com">murari766@gmail.com</a></td>
</tr>


<tr>
    <td align="center" colspan="3" class='state_row'><a name="goa"><strong>GOA</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Salcet Goa</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Jonlyn Enterprises</strong><br>
        Nuvem Enclave,<br>
        Belloy Near Rumder Busstop,<br>Nuvem, Salcet Goa <br>
        Ph.: 0832-279180<br>
        Mob.: 9923529873<br>
        Contact Person.: Mr. Vijaya Bendre<br>
        E-mail:- <a href="mailto:vijay.bendre46@gmail.com">vijay.bendre46@gmail.com </a></td>
</tr>
<tr>

</tr>
<tr>
    <td align="center" colspan="3" class='state_row'><a name="gujarat"><strong>GUJARAT</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Ahmedabad</strong></td>
    <td align="left">Small Appliances</td>
    <td align="left"><strong>Roop Marketing</strong><br>
        B / 1, Shaan Complex, B/H Sakar-4,<br>
        Nr. Sanyas Ashram, Ellis Bridge,<br>
        Ahmedabad,<br>
        <!--Ph.: 26583913<br/>-->
        Mob.: 9377761200
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Ahemedabad</strong></td>
    <td align="left">Large Appliances</td>
    <td align="left"><strong>Naresh Brothers</strong><br>
        Neptune Tower, First Floor,<br>
        Above Syndicate Bank,<br>
        Opp Nehru Bridge, Ashram Road,<br>
        Ahmedabad<br>
        Ph.: 079–26581068,69<br>
        Mob.: 92273 21206
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Baroda</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Parshwa Sales</strong><br>
        SB-46, Paradise Complex,<br>
        Opp Kala Ghoda, Nr Rajeshri Cinema<br>
        Sayajigung, Vadodara - 390005<br>
        Ph.: 2361352<br>
        Mob.: 9375717818
    </td>
</tr>
<!--<tr>-->
<!--    <td colspan="3" align="right"><a href="/shop/service-centres#begin">Top</a></td>-->
<!--</tr>-->
<tr>

</tr>
<tr>
    <td align="left"><strong>Gandhidham</strong></td>
    <td align="left">All Products</td>
    <td valign="middle" align="left"><strong>Mehta Kitchen Gallery</strong><br>
        Mehta Chambers, Plot No: 235, Ward no. 12-B,<br>
        Police Station Road, Gandhidham<br>
        Tel.: (O) 02836-231254, 232595<br>
        Mob.: 98250 25695<br>
        E-mail:- <a href="mailto:mehtakitchengallery@rediffmail.com">mehtakitchengallery@rediffmail.com</a></td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Rajkot</strong></td>
    <td align="left">All Products</td>
    <td valign="middle" align="left"><strong>Kothari Departmental Stores</strong><br>
        Sir Lakhajiraj Road,<br>
        Rajkot - 360 001.<br>
        Ph.: 0281 2223319 , 2232358
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Surat</strong></td>
    <td align="left">All Products</td>
    <td valign="middle" align="left"><strong>Manish Traders</strong><br>
        L-7/Chancellor Apt.,<br>
        Opp.R.T.O.Office,<br>
        Ring Road,Surat-395007<br>
        Ph.: (0261)3089391<br>
        Mob.: 9033600246
    </td>
</tr>


<tr>

</tr>
<tr>
    <td align="left"><strong> Surat </strong></td>
    <td>Small Appliances</td>
    <td align="left"><strong> M/S. Siddhivinayak Enterprise</strong><br>
        Hospital,Kadodara-Badroli Road,<br>
        Kadodara, Surat <br>
        Mob.: 09816062216<br>
        E-mail:- <a href="mailto: infosiddhivinayak@yahoo.in"> infosiddhivinayak@yahoo.in</a>
        <br></td>
</tr>


<tr>
    <td align="center" colspan="3" class='state_row'><a name="haryana"><strong>HARYANA</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Ambala</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Prime Agencies</strong><br>
        Sena Nagar, Ambala City<br>
        Mob.: 9896021576 <br>
        Ph.: 2521576<br>
        Contact Person:- Mr. Rohan
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Faridabad &amp; Gurgaon</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>M/s Glen Appliances Pvt. Ltd</strong><br>
        I-34, DLF Industrial Area Phase-1,<br>
        Faridabad<br>
        Ph.: 0129-4113401/02/03<br>
        Contact Person:- Ms. Payal Sharma<br>
        E-mail:- <a href="mailto:service@glenindia.com">cr1@glenindia.com</a></td>
</tr>


<!--<tr>

  </tr>
  <tr>
    <td align="left" ><strong>Hissar</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>M/s Aditya Sales Corporation</strong><br />
      114N, Model Town, Jindal Hospital Road<br />
      Hissar - 125005 (Haryana)<br />
    Ph.: 01662-247244<br/>
     Mob.: 9034668444</td></tr> -->


<tr>

</tr>
<tr>
    <td align="left"><strong>Kaithal</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Shri Om Kitchen Gallery</strong><br>
        Kurukshetra Road,<br>
        Near IG School, Kaithal<br>
        Ph.: 235543<br>
        Mob.: 9355594543
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Panipat</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>M/s Bright Electronics</strong><br>
        Ashandh Road<br>
        Panipat<br>
        Mob. : 9215652156<br></td>
</tr>


<tr>

</tr>
<tr>
    <td align="left"><strong>Sirsa</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Vardhman Traders</strong><br>
        Corner Of Jain School Street,<br>
        Noharia Bazar, Sirsa- 125055<br>
        Mob.: 9416723775<br>
        E-mail:- <a href="vardhmantraders112@gmail.com">vardhmantraders112@gmail.com</a></td>
</tr>


<tr>
    <td align="center" colspan="3" class='state_row'><a name="himachalpradesh"><strong>HIMANCHAL PRADESH</strong></a>
    </td>
</tr>
<tr>
    <td align="left"><strong>Palampur</strong></td>
    <td align="left">All Products</td>
    <td valign="middle" align="left"><strong>Aum Enterprises</strong><br>
        239, Syndicate Area, P.O.Palampur, H.P-176061<br>
        Mob.: 9736022555
    </td>
</tr>
<tr>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Parwanoo</strong></td>
    <td align="left">All Products</td>
    <td valign="middle" align="left"><strong>M/s B.R.Enterprises </strong><br>
        Shubhham Plaza , Old Kasauli Road,<br>
        Sector – 2, Parwanoo -173220.<br>
        Mob.: 09816154357
    </td>
</tr>
<tr>
    <td align="center" colspan="3" class='state_row'><a name="jammu"><strong>JAMMU &amp; KASHMIR</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Jammu</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Trikuta Agencies</strong><br>
        Opp. Iron Gate, BSF HQ Paloura Jammu 180002<br>
        Mob.: 09419181700<br>
        Contact Person:- Mr. J P Gupta
    </td>
</tr>
<tr>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Srinagar</strong></td>
    <td align="left">All Products</td>
    <td valign="middle" align="left"><strong>Delight Kitchen Ware</strong><br>
        H.O. Chowk Haba Kadal,Srinagar. <br>
        B.O. Karan Nagar,Srinagar. <br>
        Mob.: 09906593023 , 09697036491
    </td>
</tr>
<tr>
    <td align="center" colspan="3" class='state_row'><a name="jharkhand"><strong>JHARKHAND</strong></a></td>
</tr>

<tr>
    <td align="left"><strong>Ranchi</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>M/s Kashish Marketing</strong><br>
        Raja colony, Kanta toli,<br>
        Ranchi - 834001<br>
        Mob.: 9430155546, 8757864786<br>
        E-mail:- <a href="mailto:kashishmarketing@yahoo.com ">kashishmarketing@yahoo.com </a><br>
        Contact Person:- Mr. Perwez Alam Khan
    </td>
</tr>
<tr>

</tr>

<tr>
    <td align="left"><strong>Jamshedpur</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>New Ludhiana Woolen Depot</strong><br>
        42- New Development Area ( Near ABM College), Golmuri,<br>
        Jamshedpur- 831003<br>
        Mob.: 0657-2341856,<br>
        Contact Person :- Mr. Hardeep Singh Nand
    </td>
</tr>
<tr>
    <td align="center" colspan="3" class='state_row'><a name="karnataka"><strong>KARNATAKA</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Bangalore</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Shri Sai Service Center</strong><br>
        No. 30, 14th Cross, 5th Main,<br>
        Shrinidhi Layout,<br>
        Chunchungatta Main Road,<br>
        J.P.Nagar 7th Phase,<br>
        Near Maruthi School,<br>
        Bangalore – 62.<br>
        E-mail:- <a href="mailto:shreesaienterprises05@gmail.com">shreesaienterprises05@gmail.com</a><br>
        Mob: 09845980034<br>
        Contact Person:- Mrs. Rashmi
    </td>
</tr>
<!--<tr>-->
<!--    <td colspan="3" align="right"><a href="/shop/service-centres#begin">Top</a></td>-->
<!--</tr>-->
<tr>

</tr>
<tr>
    <td align="left"><strong>Bangalore</strong></td>
    <td>Small Appliances</td>
    <td align="left"><strong>Om Vinayaka Services</strong><br>
        #111, 2nd Main Road, <br>
        Shiva Nagar, Bangalore - 560010<br>
        Mob.: 96328 55323.<br>
        E-mail:- <a href="mailto:Chandrus734@gmail.com">Chandrus734@gmail.com</a>
        <br></td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Manglore</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>S R Services</strong><br>
        #5, 1st Floor, Raghavendra Kripa Complex, Car Street,<br>
        Mangalore-575001<br>
        Ph.: 0824-2443417<br>
        Contact Person :- Rajesh
    </td>
</tr>

<tr>

</tr>
<tr>
    <td align="left"><strong>Mysore</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Mysore Delight Home Services</strong><br>
        #4563/1, 5th Crs, St Mary's Road, N R Mohalla,<br>
        Mysore-570007<br>
        Ph.: 0821-2451958<br>
        Contact Person:- Fareed
    </td>
</tr>


<tr>

</tr>
<tr>
    <td align="left"><strong>Shimoga</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Sri Anantha Padmanabha Electrical &amp; Electronics</strong><br>
        Old Post Office Road,<br>
        Krishna Cafe Down,Shimoga - 577201 <br>
        Ph.: 08182-402456, 9481193069<br>
        Contact Person:- Praveen Kulal<br>
        E-mail:- <a href="mailto:sapelectrical87@gmail.com">sapelectrical87@gmail.com</a>
    </td>
</tr>

<tr>
    <td align="center" colspan="3" class='state_row'><a name="kerala"><strong>KERALA</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Calicut<br>Malappuram<br>Wayanad</strong></td>
    <td>All Product</td>
    <td align="left"><strong>Mas Enterprises</strong><br>
        Building No. XI-126, Chelari,<br>
        Mathapuzha Road,Thenhipalam P.O.,<br>
        Malappuram-673636 <br>
        Mob.: 09947544451<br>
        Contact Person:- Mr. Mujeeb<br>
    </td>
</tr>
<tr>

</tr>

<tr>
    <td align="left"><strong>Cochin</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>M/s One Touch Service Point</strong><br>
        4th Floor, South Square Building,<br>
        Near Manorama Junction,<br>
        Eranakulam-682036.<br>
        Ph.: 9946280419<br>
        Contact Person:- Mr. Akilesh Kumar
    </td>
</tr>

<tr>

</tr>
<tr>
    <td align="left"><strong>Kanoor</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Veeson Distribution</strong><br>
        Global Complex Podikundu,<br>
        Kannur - 4<br>
        Ph.: 04972747622<br>
    </td>
</tr>

<tr>

</tr>
<tr>
    <td align="left"><strong>Kottayam</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Evergreen Marketing Associates</strong><br>
        Evergreen Building, Deepthi Nagar Road, Kanjikuzhy,<br>
        Kottayam - 686004<br>
        Ph.: 0481-2573854, 2570578<br>
        Contact Person :- Nebu
    </td>
</tr>


<tr>

</tr>
<tr>
    <td align="left"><strong>Kottayam</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Kannan G</strong><br>
        Block No 22, Vazhappally West PO Changanacherry ,<br>
        Kottayam - 686103 <br>
        Mob.: 9847959778<br>
        E-Mail- <a href="mailto:kannangkb@gmail.com">kannangkb@gmail.com</a></td>
</tr>


<tr>

</tr>
<tr>
    <td align="left"><strong>Trivendrum</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Madhurima Service Centre</strong><br>
        PMK Building,Oottukuzhy ,<br>
        Trivandrum, Kerala - 695001<br>
        Mob.: 9567508611<br>
        Ph.: 0471-4013130<br>
        Contact Person:- Saji Mon <br>
        E-Mail- <a href="mailto:saji.nov22premier@yahoo.com">saji.nov22premier@yahoo.com</a></td>
</tr>


<tr>

</tr>
<tr>
    <td align="left"><strong>Trivendrum</strong></td>
    <td>Large Appliances</td>
    <td align="left"><strong>Oasis Marketing</strong><br>
        TC 67/428, Pachalloor P.O.<br>
        Trivandrum-695 027,<br>
        Ph.: 0471-3258713, 2458524
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Trivendrum</strong></td>
    <td>Small Appliances</td>
    <td align="left"><strong>M/s Products &amp; services</strong><br>
        Veda Bhawan, TC28/1995<br>
        Ramaswamy Kivil Street,<br>
        Fort PO Trivendrum<br>
        Ph.: 2475688
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="center" colspan="3" class='state_row'><a name="madhyapradesh"><strong>MADHYA PRADESH</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Bhopal</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Glen Gallery</strong><br>
        163/G-1, Rama Complex, Zone-I,<br>
        M.P.Nagar, Bhopal - 162001 (M.P)<br>
        Mob.: 9425359368
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Gwalior</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Trapti Kitchen Appliances</strong><br>
        Mayur Market, Thatipur<br>
        Mob.: 9827212579<br>
        Contact Person:- Mr R.K.Garg
    </td>
</tr>
<!--<tr>-->
<!--    <td colspan="3" align="right"><a href="/shop/service-centres#begin">Top</a></td>-->
<!--</tr>-->
<tr>

</tr>
<tr>
    <td align="left"><strong>Indore</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Ambor Appliances</strong><br>
        97, Maharani Road,<br>
        1st Floor Khandwawala Building<br>
        Near Broadgage Railway Station,<br>
        Indore- 452007.<br>
        Ph.: 2542122, 2542144<br>
        Mob.: 9303243220<br>
        E-Mail- <a href="mailto:amborappliances@rediffmail.com">amborappliances@rediffmail.com</a></td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Jabalpur</strong></td>
    <td align="left"><br>All Products</td>
    <td align="left"><strong>Nigam Gas Stove Centre</strong><br>
        305, Karamchand Chowk, Jabalpur<br>
        Ph.: 2665585
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Jabalpur</strong></td>
    <td align="left"><br>All Products</td>
    <td align="left"><strong>M/s Nigam</strong><br>
        Kitchen Gallery,<br>
        Hotel Sharda Building,<br>
        Tularam Chowk Jabalpur<br>
        Ph.: 0761-2413375-2413376<br>
        Mob.: 09425860980
    </td>
</tr>
<tr>
    <td align="center" colspan="3" class='state_row'><a name="maharashtra"><strong>MAHARASHTRA</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Central &amp; Navi Mumbai</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Has Enterprises</strong><br>
        Shivtej Niwas,Sector 9,<br>
        Plot No.826 /1.Diva Gaon,Diva Nagar,Airoli,<br>
        Navi Mumbai - 400 708<br>
        Tel. : 022-27692190, 65614121<br>
        Mob.: 882 882 8822<br>
        Contact Person:- Dhanraj Jadhav<br>
        E -Mail- <a href="mailto:hasenterprises1@gmail.com ">hasenterprises1@gmail.com <br></a>
        Website:- <a href="http://www.hasenterprises.com/ ">www.hasenterprises.com <br></a>
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Western Mumbai</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>OM Sai Solution</strong><br>
        Shop No 4, 1st Floor, <br>
        Nalanda Shopping Center, Station Road,<br>
        Goregaon (w)- Mumbai-400062<br>

        Ph.: 28766507 , 28766954<br>
        Mob.: 9867151123 , 9619858678<br>
        Contact Person :- Sachin More<br>
        E-Mail- <a href="mailto:omsaisolution08@gmail.com">omsaisolution08@gmail.com<br></a>
    </td>
</tr>
<tr>

</tr>

<tr>
    <td align="left"><strong>Nagpur</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Payal Agencies</strong><br>
        36 Pragati Colloney, Wardha Road, Nagpur<br>
        Ph.: 0712- 2250219<br>
        Mob.: 9422112994<br>
        Contact Person:- Mr. Rajendra Jaiswal<br></td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Nasik</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Super Distributors</strong><br>
        Shop No. 9, Riddhi siddhi Apptt.,<br>
        Indira Kund, Nasik - 422003<br></td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Pune</strong></td>
    <td>Large Appliances</td>
    <td align="left"><strong>Phoenix Services</strong><br>
        Khyati Heights,<br>
        Shop No. 6,<br>
        Opp Savant Vihar Phase-01 Katraj.<br>
        Tel.: 020 -32677555<br>
        Mob.: 8888846328 , 8888846330<br></td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Satara/ Kolhapur</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>M/s Yes Marketing</strong><br>
        Srujan Complex, Opp. Mane HospitalVisawa Naka<br>
        Satara-415002<br>
        Mob.: 9225347977
    </td>
</tr>
<tr>

</tr>

<tr>
    <td align="center" colspan="3" class='state_row'><a name="orissa"><strong>ORISSA</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Bhubaneshwar</strong></td>
    <td>All products</td>
    <td align="left"><strong>Vishal Plus Appliances</strong><br>
        63. Forest Park, 1st Floor,<br>
        Near Telephone Exchange<br>
        Bhubaneshwar-9<br>
        Mob.: 9337783178 , 9337106197<br>
        Contact Person:- Mr. Susanta Panigarhi<br>
        E-Mail: <a href="mailto:customercare@kitchengallerybbsr.com">customercare@kitchengallerybbsr.com</a>
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Cuttak</strong></td>
    <td>All Products</td>
    <td><strong>Nigam Enterprises</strong><br>
        Hindi Sikhya Samiti, Barik Sahi Lane, Shankarpur
        <br>
        Cuttack - 753012<br>
        Mob.: 9437026488 , 9338056235<br>
        Ph.: 0671 - 2423273 <br>
        Contact person :– Rudra Prasad Mishra<br>
    </td>

</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Rourkela</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Trade-In</strong><br>
        A / SCR-2, Basanti Nagar<br>
        Rourkela-769012<br>
        Ph.: 0661 - 2420002<br>
        Mob.: 9437048180<br>
        Contact Person:- Mr. Prakash Padhi.<br>
        E-mail:- <a href="mailto:pcptradein_rkl@rediffmail.com">pcptradein_rkl@rediffmail.com</a></td>
</tr>
<tr>
    <td align="center" colspan="3" class='state_row'><a name="punjab"><strong>PUNJAB</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Amritsar</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Harbans Enterprises</strong><br>
        Del. at SCF-2nd Floor, D-Block, Ranjit Opp.<br>
        Grindlays Bank Hall Bazar, Amritsar.<br>
        Ph.: 0183-2553590<br>
        Mob.: 9814051987<br>
        Contact Person:- Sanjay Dhingra
    </td>
</tr>
<!--<tr>-->
<!--    <td colspan="3" align="right"><a href="/shop/service-centres#begin">Top</a></td>-->
<!--</tr>-->
<tr>

</tr>
<tr>
    <td align="left"><strong>Bhatinda</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Bharat Electricals</strong><br>
        Jain Street,<br>
        Post Office Bazar, Bhatinda<br>
        Mob.: 9814539301<br>
        Ph.: 0164-2252828<br>
        Contact Person:- Mr. Bansal
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Chandigarh</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Sai Sales Corporation</strong><br>
        SCO 68-70, Chamber No. 66, Sec- 17A,<br>
        Chandigarh<br>
        Ph: 0172-5078028, 3064028<br>
        Contact Person:- Mr.Mahesh
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Jallandhar</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Punjab Electroworld</strong><br>
        Shop No 12 Monika Tower Basement<br>
        Milaap Chowk, Jalandhar City.<br>
        Ph.: 0181- 2210012 <br>
        Mob.: 09815669992<br>
        Contact Person:- Mr. Satnam Singh<br>
        E-mail:- <a href="mailto:punjab_inalsa@yahoo.com">punjab_inalsa@yahoo.com</a></td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Ludhiana</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Opel Marketing Associates</strong><br>
        SCO 25 AsianLac Tower Basement,<br>
        Sigma Scan Road<br>
        Gurudev Nagar, Ludhiana<br>
        Tel.: 0161-5087282, 4627070, 4646282<br>
        Mob.: +91-9316088555<br>
        E-mail:- <a href="mailto:opel_marketing_associates@yahoo.com">opelmarketingassociates@yahoo.com</a></td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Patiala</strong></td>
    <td>All Products</td>
    <td align="left"><strong>MA Enterprises</strong><br>
        SCO-3, Prakash Market<br>
        Opp HDFC Bank, Dharampura Bazar<br>
        Patiala- 147001<br>
        Mob.: 9988670892 , 9988670890<br>
        E-mail:- jagdishkumar.saini@yahoo.com
    </td>
</tr>
<tr>
    <td align="center" colspan="3" class='state_row'><a name="rajasthan"><strong>RAJASTHAN</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Ajmer</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Shree Marketing</strong> (City)<br>
        Near Martinal Bridge ,Ajmer<br>
        Ph.: 0145-2625490<br>
        Mob.: 9829072774<br>
        Contact Person:- Mr. K T Wadhwani
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Jodhpur</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Saluja's</strong><br>
        Opp. Minrwa Complex<br>
        Station Road<br>
        Ph.: 2625577
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Bikaner</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Ganpati Showroom </strong><br>
        G.D.Sales Modern Market,<br>Bikaner<br>
        Mob.: 9602687450 , 9829099053
    </td>
</tr>

<tr>

</tr>

<tr>
    <td align="left"><strong>Jaipur</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Goodwill Trading Centre</strong><br>
        A-1 Basement Of Jain Kunj Apartment,<br> Gopal Bari Mode, Nr. Ajmer Puliya,<br> Ajmer Road, Jaipur<br>
        Tel. : 0141-3293847<br>
        Mob.: 9828012202<br>
        Contact Person:- Mr. K M Khan
    </td>
</tr>

<tr>

</tr>
<tr>
    <td align="left"><strong>Jaipur</strong></td>
    <td>Large Appliances</td>
    <td align="left"><strong>R. R. Enterprises</strong><br>
        R-8/A, Yudhisthir Marg <br>
        C - Scheme, Jaipur - 302005<br>
        Ph.: 0141-4030853
    </td>
</tr>


<tr>

</tr>
<tr>
    <td align="left"><strong>Jaipur</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Shree Vinayak</strong><br>
        Plot No. 8, Indira Coloney <br>
        bani park <br>
        Ph.: 3240942
    </td>
</tr>

<tr>

</tr>
<tr>
    <td align="left"><strong>Jaipur</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Vijay Laxmi Enterprises</strong><br>
        5, Vijay Laxmi Mahima,<br>
        Sehkar Marg, Opp.J.P. Phatak,<br>
        Jaipur-302015<br>
        Ph.: 0141 3206068
        Mob.: 098280 15941<br>
        E-Mail:- <a href="mailto:arunsingh0851@yahoo.com">arunsingh0851@yahoo.com</a></td>
</tr>


<tr>

</tr>
<tr>
    <td align="left"><strong>Udaipur</strong></td>
    <td>All Products</td>
    <td align="left"><strong>M/s Punam Electrical</strong><br>
        Shastri Circle, Gurudwara Building<br>
        Udaipur<br>
        Mob.: 9828082059<br>
        Contact Person:- Mr. Mahesh Khatri
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Shree Ganga Nagar</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Delight Electricals</strong><br>
        C 45/46 Public Park<br>
        Shree Ganga Nagar<br>
        Ph.: 2442697
    </td>
</tr>
<!--<tr>-->
<!--    <td colspan="3" align="right"><a href="/shop/service-centres#begin">Top</a></td>-->
<!--</tr>-->
<tr>
    <td align="center" colspan="3" class='state_row'><a name="tamilnadu"><strong>TAMIL NADU</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Chennai</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Chakara</strong><br>
        37-A, Sir C.V.Raman Road,<br>
        Alwarpet, Chennai-600018.<br>
        Ph.: 044- 24660926 , 24661813 , 42112820<br>
        Fax.: 044 42112821<br>
        E-Mail Id: <a href="mailto:chakaraservice@gmail.com">chakaraservice@gmail.com</a>
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Coimbatore</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Glen Care</strong><br>
        18/19, Aruna Periya Karuppan Complex,<br>
        Devanga Pet, No : 1, Flower Market,<br>
        Coimbatore - 01<br>
        Mob : 7373712003 , 7373712007 <br>
        Ph.: 0422 – 3059700<br>
        E-Mail:- glencare11@yahoo.com
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Madurai</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Vasudev Hardware</strong><br>
        29, Sambanda Moorthy Street,<br>
        Near, West Masi Street,<br>
        Madurai - 625 001.<br>
        Mob.: 09952529707
    </td>
</tr>
<tr>

</tr>

<tr>
    <td align="left"><strong>Salem</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Mr. P. Rajkumar</strong><br>
        No-92-4/243,Alamara Kadu,<br>
        Ilaiya Nila Nagar,<br>
        Meyyanur,Salem-636004<br>
        Mob.: 09965317128, 8760565566<br>
        E-mail:- <a href="mailto:kitchenchimneyservice@gmail.com">kitchenchimneyservice@gmail.com</a></td>
</tr>
<tr>

</tr>

<tr>
    <td align="left"><strong>Trichi</strong></td>
    <td>All Products</td>
    <td align="left"><strong>Naghppa Trading Co.</strong><br>
        78 W B Road,<br>
        Trichi<br>
        Ph.: 2707390
    </td>
</tr>
<tr>
    <td align="center" colspan="3" class='state_row'><a name="uttarpradesh"><strong>UTTAR PRADESH</strong></a></td>
</tr>
<tr>
    <td align="left"><strong>Agra</strong></td>
    <td>All Products</td>
    <td><strong>M/s Home Applainces Care Center</strong><br>
        25/50, Gandhi Nagar<br>
        Nr. Paliwal Park, Agra-282002<br>
        Mob.: 9359350320.<br></td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Aligarh</strong></td>
    <td>All Products</td>
    <td><strong>Venus Sales</strong><br>
        Marrie Road<br>
        Ph.: 0571-429239<br>
        Contact Person:- Mr Manoj jain
    </td>
</tr>


<tr>

</tr>
<tr>
    <td align="left"><strong> Allahabad</strong></td>
    <td>All Products</td>
    <td align="left"><strong> Sudarshan Trading Co.</strong><br>
        2/36-H ,Ramanand Nagar,<br>
        Allahpur, Allahabad <br>
        Mob.: 9415663277<br>
        E-mail:- <a href="mailto: Gopalahuja69@gmail.com"> Gopalahuja69@gmail.com</a>
        <br></td>
</tr>


<tr>

</tr>
<tr>
    <td align="left"><strong>Bareilly</strong></td>
    <td>All Products</td>
    <td><strong>Nirmal Traders</strong><br>
        C-116/9,Rjendra Nagar.<br>
        Mob.: 9412290936<br>
        Contact Person:- Mr Harish Ratra
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Gorakhpur</strong></td>
    <td>All Products</td>
    <td><strong>Modern Crockery Museum</strong><br>
        18, Water Works Building, Golghar, Gorakhpur<br>
        <!--Ph.: 2337315 <br/>-->
        <!--Mob.:  9415283237<br />-->
        Mob.: 09889911515<br>
        Contact Person:- Mr Rajinder Gupta
    </td>
</tr>
<tr>

</tr>
<tr>
    <td align="left"><strong>Jhansi</strong></td>
    <td align="left">All Products</td>
    <td align="left"><strong>Union Radio &amp; Electric</strong><br>
        515/1, Sadar Bazar, Jhansi<br>
        Ph.: 2398581 , 2470823<br>
        Contact Person:- Mr Sanjay Arora
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Kanpur</strong></td>
    <td align="left">All Products</td>
    <td><strong>M/s Bharat Enterprises</strong><br>
        361,Harris Ganj, Shree Ram Mahadeo Mill Compound,<br>
        Kanpur.<br>
        Ph.: 0512-2326022 , 2321246
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Lucknow</strong></td>
    <td align="left">All Products</td>
    <td><strong>Houzoldz</strong><br>
        Basement of Dr. Onkar Singh's Clinic<br>
        (Near Bata Showroom)<br>
        Ram Nagar, Kanpur Road,<br>
        Alambagh, Lucknow.<br>
        Mob.: 9839710688
        Ph.: 0522-4022504<br>
        Contact Person:- Gurdeep Singh,<br>
        Narendra Pal Singh Grover<br>
        Mob.: 9415004779
    </td>
</tr>
<!--<tr>-->
<!--    <td colspan="3" align="right"><a href="/shop/service-centres#begin">Top</a></td>-->
<!--</tr>-->
<tr>

</tr>
<tr>
    <td><strong>Meerut</strong></td>
    <td align="left">All Products</td>
    <td><strong>Appliances Care Centre</strong><br>
        Shop No. G.N. 12,<br>
        Star Plaza, Begum Bridge Road,<br>
        Opp. City Centre, Meerut City- 250001.<br>
        Tel.: 9897036071<br>
        E-mail:- <a href="mailto:appliancescare2004@gmail.com">appliancescare2004@gmail.com</a> <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Muradabad</strong></td>
    <td align="left">All Products</td>
    <td><strong>Agarwal Udog</strong><br>
        Near Kotwali,<br>
        Bazar Gang, Modrabad<br>
        Mob.: 9360601496<br>
        Contact Person:- Mr Saket Agarwal
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Muzaffar nagar</strong></td>
    <td align="left">All Products</td>
    <td><strong>Ravi Enterprises</strong><br>
        10-Uday Market,<br>
        Kundanpura<br>
        Mob.: 9412212073<br>
        Contact Person:- Mr Ravi Agarwal
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Varanasi</strong></td>
    <td>Large appliances</td>
    <td><strong>Laxmi Marketing</strong><br>
        B-37/9, Birdopur,<br>
        Mahamoor Ganj, Varanasi<br>
        Mob.: 09415818194<br>
        Contact Person.: Mr.Viay Maurya
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Varanasi</strong></td>
    <td>small appliances</td>
    <td><strong>Krishna Traders</strong><br>
        N-4/7-A, Karoundi Crossing,<br>
        Varanasi (U.P)
    </td>
</tr>
<tr>
    <td align="center" colspan="3" class='state_row'><a name="uttaranchal"><strong>UTTARANCHAL</strong></a></td>
</tr>
<tr>
    <td><strong>Dehradun</strong></td>
    <td align="left">All Products</td>
    <td><strong>G.G. Sales</strong><br>
        13/11, Mahant Road,<br>
        Laxman Chowk, Dehradun<br>
        Mob.: 9719416135 , 09837070028<br>
        Ph.: 0135-2622665
        Contact Person:- Mr Roopak Goyal
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Dehradun</strong></td>
    <td align="left">Large Appliances</td>
    <td><strong>Glen Service Center</strong><br>
        Fs 58 Rajeev Gandhi Shooping Complex,<br>
        Dispansary Road Dehradun,248001<br>
        Ph.: 01352716677 , 9152214259
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Dehradun</strong></td>
    <td align="left">All Products</td>
    <td><strong>Sachdeva Agencies</strong><br>
        32, Ansari Road,Dehradun<br>
        Ph.: 2652316
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Kichha</strong></td>
    <td align="left">All Products</td>
    <td><strong>S.R. Traders</strong><br>
        Bareilly Road, Kichha (U.s. Nagar, Uttranchal)<br>
        Ph.: 05944-64418 9...<br>
        Contact Person:- Mr Siyaram Agarwal
    </td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Saharanpur</strong></td>
    <td align="left">All Products</td>
    <td><strong>Khurana Elect. Plaza</strong><br>
        Thril Complex,Court Road<br>
        Mob.: 9719113403<br>
        Contact Person:- Mr Vinod Khurana
    </td>
</tr>
<tr>
    <td align="center" colspan="3" class='state_row'><a name="westbengal"><strong>WEST BENGAL</strong></a></td>
</tr>

<tr>
    <td><strong>Kolkatta</strong></td>
    <td>Small Appliances</td>
    <td><strong>Rasika Impex Pvt. Ltd</strong>.<br>
        11, First Floor,<br>
        Prafulla Sarkar Street,<br>
        Kolkata-72<br>
        Ph.: 033-2234-9262, 40054336<br>
        E-mail:- <a href="mailto:rasikaimpex@hotmail.com">rasikaimpex@hotmail.com</a> <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:rasikaimpex@hotmail.com">pradipchow@hotmail.com</a></td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Kolkatta</strong></td>
    <td>Large Appliances</td>
    <td><strong>Loyal Services</strong><br>
        78A, Rajdanga Gold Park <br>
        Kolkata-700107<br>
        Near Location- Tribarna Bus Stop<br>
        Mob.: 9836666921/857<br>
        Contact person :- Mr. Sujit Baral<br>
        E- Mail :- <a href="mailto:glenservice.bke@gmail.com">glenservice.bke@gmail.com</a></td>
</tr>
<tr>

</tr>
<tr>
    <td><strong>Siliguri</strong></td>
    <td align="left">All Products</td>
    <td><strong>Prakash Enterprises</strong><br>
        240,Prakash deep Complex,<br>
        Sevoke Road, Siliguri - 734401<br>
        Ph.: 0353- 2432235, 6417970<br>
        Contact person :– Mr. Anil Agarwal<br>
        E- Mail:- <a href="mailto:prakashlites@hotmail.com">prakashlites@hotmail.com</a></td>
</tr>
<!--<tr>-->
<!--    <td colspan="3" align="right"><a href="/shop/service-centres#begin">Top</a></td>-->
<!--</tr>-->
</tbody>
</table>

</div>
</div>


</div>


@stop