@extends('layouts.frontoffice.homepage_default')
@section('content')

    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/jquery.caroufredsel/6.2.1/jquery.carouFredSel.packed.js"></script>


    <section id="columns" class="clearfix">
        <div class="container">
            <div class="row-fluid">

                <!-- Center -->
                <section id="center_column" class="span12">
                    <div class="contenttop row-fluid">
                        <div id="homecontent-displayHome">
                            <div class="leo-custom">
                                <div class="row-fluid">

                                    <div class="span4">
                                        <a href="http://club.glenindia.com/user" target="_blank">
<href="{{URL::to(' http://club.glenindia.com/user ')}}" title="Warranty Ragistration">

                                            <img src="{{asset('frontoffice/modules/leomanagemodules/img/img-adv3.png')}}"/>
                                        </a>
                                    </div>

                                    <div class="span4">
                                        <a href="{{URL::to('/retail-outlets')}}">
<href="{{URL::to('/retail-outlets')}}" title="Dealer Locator">

                                            <img src="{{asset('frontoffice/modules/leomanagemodules/img/img-adv1.jpg')}}"/>
                                        </a>
                                    </div>
                                    <div class="span4">
                                        <a href="{{URL::to('/service-centers')}}">
<href="{{URL::to('/service-centers')}}" title="Service Centres">

                                            <img src="{{asset('frontoffice/modules/leomanagemodules/img/img-adv2.jpg')}}"/>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- MODULE Home Latest Offers -->
                            @include('_partials.frontoffice.latest_products')
                            <!-- /MODULE Home Latest Offers -->
                        </div>


                        <!-- MODULE Home Featured Products -->
                        @include('_partials.frontoffice.featured_products')
                        <!-- /MODULE Home Featured Products -->

                        <div class="clearfix"></div>
                        <!--                     MODULE Home Upcoming Products-->
                        @include('_partials.frontoffice.upcoming_products')
                        <!--                     /MODULE Home Upcoming Products -->

                    </div>
                    <!-- end div block_home -->
                    <div id="contentbottom">
                        <div id="homecontent-displayContentBottom">
                            <div class="leo-custom">
                                <div class="row-fluid">
                                    <div class="span3" style="line-height:200px;">
                                        <p>
                                            <a href="{{URL::to('recipe-book')}}"
                                               target="_blank"> <img
                                                        src="{{asset('frontoffice/modules/leomanagemodules/img/cook-recipes.jpg')}}"
                                                        alt=""/></a>
                                        </p>
                                    </div>
                                    <div class="span7">
                                        <div class="fb-like-box" data-href="https://www.facebook.com/GlenIndia"
                                             data-width="540"
                                             data-height="200" data-colorscheme="light" data-show-faces="true"
                                             data-header="false"
                                             data-stream="false" data-show-border="false"></div>
                                    </div>
                                    
                                    <div style="line-height:220px;" class="span2">
                                        <p class="findout"><a href="http://www.facebook.com/GlenIndia"                                                              title="Facebook" target="_blank" style="color:#FFF;" >Stay Connected</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function () {
                                $('.carousel').each(function () {
                                    $(this).carousel({
                                        pause: true,
                                        interval: false
                                    });
                                });
                                $(".blockleoproducttabs").each(function () {
                                    $(".nav-tabs li", this).first().addClass("active");
                                    $(".tab-content .tab-pane", this).first().addClass("active");
                                });
                            });
                        </script>
                    </div>
                </section>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function () {
            $('.carousel').each(function () {
                $(this).carousel({
                    pause: true,
                    interval: false
                });
            });
//        // Using custom configuration
//        $('#custom_carousel').carouFredSel({
//            items: 2,
//            direction: "left",
//            scroll: {
//                items: 1,
//                easing: "elastic",
//                duration: 1000,
//                pauseOnHover: true
//            }
//        });

        });
    </script>


@stop
