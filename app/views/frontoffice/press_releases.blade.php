@extends('layouts.frontoffice.default')

@section('content')

<style type="text/css">

    /*.privacy_policy_div h3 {*/
    /*text-decoration: underline;*/
    /*}*/

    /*.information_use {*/
    /*padding-left: 40px;*/
    /*}*/

    /*.information_use li {*/
    /*list-style: circle !important;*/
    /*padding-bottom: 5px;*/
    /*}*/

    th {
        color: #57575A;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
    }

    .gl-sub-menu2 a:link, .gl-sub-menu2 a:visited {
        color: #57575A;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        text-decoration: none;
    }

    .gl-sub-menu2 a:hover {
        color: #57575A;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        text-decoration: underline;
    }

    .no_border {
        border: none !important;
    }

</style>

<div class="container">
    <div class="row">
        <div class="span12">
            <h1>Press & Media</h1>
        </div>
    </div>

    <br/><br/>

    <div class="row justify">

        <div class="span12">
            <table class="table" align="center" border="0" cellspacing="0" cellpadding="0">
                <tbody>


                <tr>
                    <td class="no_border">
                        <table class="gl-sub-menu2 table" bordercolor="#bbbbbb" align="left" border="1" cellspacing="0"
                               cellpadding="3" style="border:1px solid #bbbbbb; border-collapse:collapse;">
                            <tbody>
                            <tr>
                                <th align="left" colspan="8">Press Releases</th>
                            </tr>
                            <tr>
                                <td width="20" align="center">
                                    <img align="middle" width="16px" height="20px" src="{{asset('frontoffice/img/pdf_logo.gif')}}">
                                </td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200">
                                    <a href="{{asset('/uploads/media/press_releases/Glen_India_introduces_6071_Split_Chimney_for_a_Lower_Noise_in_the_Kitchen.pdf')}}" target="_blank">GL 6071 SS Split Chimney</a>
                                </td>
                                <td width="15">&nbsp;</td>
                                <td width="20" align="center">
                                    <img align="middle" width="16px" height="20px" src="{{asset('frontoffice/img/pdf_logo.gif')}}">
                                </td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200">
                                    <a href="{{asset('/uploads/media/press_releases/double ring burner-website.pdf')}}" target="_blank">Built-In-Hobs with Double Ring Burners</a>
                                </td>
                            </tr>

                            <tr>
                                <td width="20" align="center">
                                    <img align="middle" width="16px" height="20px"  src="{{asset('frontoffice/img/pdf_logo.gif')}}">
                                </td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200">
                                    <a href="{{asset('/uploads/media/press_releases/Electric Tandoor GL 5014.pdf')}}" target="_blank"> Electric Tandoor GL 5014</a></td>
                                <td width="15">&nbsp;</td>
                                <td width="20" align="center">
                                    <img align="middle" width="16px" height="20px" src="{{asset('frontoffice/img/pdf_logo.gif')}}">
                                </td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200">
                                    <a href="{{asset('/uploads/media/press_releases/Glen India Accredited 5 Star Ratings by Experts.pdf')}}" target="_blank">Glen India Accredited 5 Star Ratings by Experts</a></td>
                            </tr>

                            <tr>
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/Release Appeared In Navbharat Times.pdf')}}" target="_blank">Release Appeared In Navbharat Times</a></td>
                                <td width="15">&nbsp;</td>
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/AIR FRYER GL 3041.pdf')}}" target="_blank">Air Fryer GL 3041</a></td>
                            </tr>

                            <tr>
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/Release Appeared In Dainik Jagran.pdf')}}" target="_blank">Release Appeared In Dainik Jagran</a></td>
                                <td width="15">&nbsp;</td>
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/Glen India introduces Modern Appliances (1).pdf')}}" target="_blank">Glen Introduces Modern Appliances</a></td>
                            </tr>

                            <tr>
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/Glass Grill GL 3033.pdf')}}" target="_blank">Glass Grill GL 3033</a></td>
                                <td width="15">&nbsp;</td>
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/Steam20Cooker20GL203051-2.pdf')}}" target="_blank">Steam Cooker GL 3051</a></td>
                            </tr>

                            <tr>
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/Mixer20Grinder20GL204022.pdf')}}" target="_blank">Mixer Grinder GL 4022</a></td>
                                <td width="15">&nbsp;</td>
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/Mini20Chopper20GL20404320MC.pdf')}}" target="_blank">Mini Chopper GL 4043 MC</a></td>
                            </tr>

                            <tr>
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/Juicer20Mixer20Grinder20GL204015.pdf')}}" target="_blank">Juicer Mixer Grinder GL 4015</a></td>
                                <td width="15">&nbsp;</td>
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/Induction2Cooker.pdf')}}" target="_blank">Induction Cooker</a></td>
                            </tr>

                            <tr>
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/Glass20Cooktop20GL201049.pdf')}}" target="_blank">Glass Cooktop GL 1049</a></td>
                                <td width="15">&nbsp;</td>
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/Food20Processor20GL204052.pdf')}}" target="_blank">Food Processor GL 4052</a></td>
                            </tr>

                            <tr>
                                <!--                <td width="20" align="center"><img align="middle" width="16px" height="20px"-->
                                <!--                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>-->
                                <!--                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a-->
                                <!--                        href="/uploads/media/press_releases/GLen LA Catalogue_2014.pdf" target="_blank">Food Processor GL 4052</a></td>-->
                                <!--                <td width="15">&nbsp;</td>-->
                                <td width="20" align="center"><img align="middle" width="16px" height="20px"
                                                                   src="{{asset('frontoffice/img/pdf_logo.gif')}}"></td>
                                <td align="left" valign="middle" bgcolor="#dddddd" width="200"><a
                                        href="{{asset('/uploads/media/press_releases/Chimney20GL20605220_touch.pdf')}}" target="_blank">Chimney GL 6052 touch</a></td>
                            </tr>


                            </tbody>
                        </table>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>

    </div>

    <br/><br/>

</div>


@stop