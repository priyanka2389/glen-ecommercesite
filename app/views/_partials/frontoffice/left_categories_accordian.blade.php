<section id="left_column" class="column span3 sidebar">

<div id="layered_block_left" class="block block-verticalmenu">
<div class="block-title ma-new-vertscroller-title">
    <h3 class="title_block"><span class="tcolor">Refine</span></h3>
</div>
<div class="block-content left-categorys-container">
<form action="#" id="layered_form">
<div>
<!--  chimneys starts here-->
<div><span class="layered_subtitle">Chimneys</span> <span class="layered_close closed">
        <a href="#" rel="ul_layered_id_attribute_group_8">&lt;</a></span>

    <div class="clear"></div>
    <ul id="ul_layered_id_attribute_group_8" style="display:none">
        <li class="nomargin hiddable">

            <a href="{{URL::to('/island-chimneys')}}">Island Chimneys
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/split-chimneys')}}">Split Chimneys
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/designer-hood-chimneys')}}">Designer Chimneys
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/straight-line-chimneys')}}">Straight Line
                Chimneys
            </a>
        </li>
    </ul>
</div>
<script type="text/javascript">

    $('.layered_id_attribute_group').show();

</script>
<!--  chimneys ends here-->

<!--built in series starts here-->
<div><span class="layered_subtitle">Built In Series</span> <span class="layered_close closed">
        <a href="#" rel="ul_layered_id_attribute_group_10">&lt;</a></span>

    <div class="clear"></div>
    <ul id="ul_layered_id_attribute_group_10" style="display:none">
        <li class="nomargin hiddable">

            <a href="{{URL::to('/built-in-ovens')}}">Built-In-Ovens
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/glass-hobs')}}">Glass Hobs
                Grinders
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/induction-hobs')}}">Induction Hobs
            </a>
        </li>
    </ul>
</div>
<!--  built in series ends here-->


<!--  cooking appliances starts here-->
<div><span class="layered_subtitle">Cooking Appliances</span> <span class="layered_close closed">
        <a href="#" rel="ul_layered_id_attribute_group_11">&lt;</a></span>

    <div class="clear"></div>
    <ul id="ul_layered_id_attribute_group_11" style="display:none">
        <li class="nomargin hiddable">

            <a href="{{URL::to('/glass')}}">Glass & Induction Cooktops
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/platinum-cooktops')}}">Platinum Cooktops
                Grinders
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/stainless-steel-cooktops')}}">Stainless Steel Cooktops
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/indian-cooking-ranges')}}">Indian Cooking Ranges
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/international-cooking-ranges')}}">International Cooking Ranges
            </a>
        </li>
    </ul>
</div>
<!-- cooking appliances ends here-->


<!--   small appliances starts here-->
<div><span class="layered_subtitle">Small Appliances</span> <span class="layered_close closed">
        <a href="#" rel="ul_layered_id_attribute_group_12">&lt;</a></span>

    <div class="clear"></div>
    <ul id="ul_layered_id_attribute_group_12" style="display:none">
        <li class="nomargin hiddable">

            <a href="{{URL::to('/food-processors')}}">Food Processors
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/juicer-mixer-grinders')}}">Juicer Mixer
                Grinders
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/mixer-grinders')}}">Mixer Grinders
            </a>
        </li>

        <li class="nomargin hiddable">

            <a href="{{URL::to('/choppers')}}">Choppers
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/hand-mixers')}}">Hand Mixers
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/egg-master')}}">Egg Masters
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/coffee-maker')}}">Coffee Makers
            </a>
        </li>

        <li class="nomargin hiddable">

            <a href="{{URL::to('/kettles-&-tea-makers')}}">Kettles & Tea Makers
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/toasters')}}">Toasters
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/sandwich-makers')}}">Sandwich Makers
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/bread-makers')}}">Bread Makers
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/air-fryer')}}">Airfryers
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/steam-cookers')}}">Steam Cookers
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/rice-cookers')}}">Rice Cookers
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/glass-grill')}}">Glass Grill
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/oven-toaster-grillers')}}">Oven Toasters Grillers
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/tandoors')}}">Tandoors
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/induction-cookers')}}">Induction Cookers
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/OFR-heaters')}}">OFR Heaters
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/PTC-heaters')}}">PTC Heaters
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/irons')}}">Irons
            </a>
        </li>
        <li class="nomargin hiddable">

            <a href="{{URL::to('/blender-grinders')}}">Blender Grinders
            </a>
        </li>

    </ul>
</div>
<script type="text/javascript">

    $('.layered_id_attribute_group').show();

</script>
<!--   small appliances ends here-->

</div>
<input type="hidden" name="id_category_layered" value="6">
</form>
</div>

</div>


<ul class="products clearfix">
    <li class="clearfix first_item" style="margin-bottom:6px;">
        <a href="http://club.glenindia.com/user" target="_blank"><img
                src="{{asset('frontoffice/modules/leomanagemodules/img/img-adv3.png')}}"
                alt="Warranty Registration"></a>
    </li>

    <li class="clearfix first_item" style="margin-bottom:6px;">
        <a href="{{URL::to('/retail-outlets')}}"> <img
                src="{{asset('frontoffice/modules/leomanagemodules/img/img-adv1.jpg')}}"
                alt="Store Locator">
    </li>
    <li class="clearfix last_item" style="margin-bottom:6px;">
        <a href="{{URL::to('/service-centers')}}"> <img
                src="{{asset('frontoffice/modules/leomanagemodules/img/img-adv2.jpg')}}"
                alt="Service Centers">
    </li>
</ul>


</section>
