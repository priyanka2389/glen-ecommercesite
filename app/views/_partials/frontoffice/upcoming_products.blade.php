<script type="text/javascript">
    $(document).ready(function () {

        $('.upcoming_products').click(function () {

            var latest_product = $(this);
            var url = latest_product.data('url');
            window.location.href = url;

        });

    });
</script>


<!--        latest offers starts here-->
@if(!is_null($upcoming_products))
    <div class="leo-carousel">
        <div id="blockleohighlightcarousel-new"
             class="block products_block exclusive blockleohighlightcarousel">
            <h3 class="title_block">Upcoming Products</h3>

            <div class="block_content">

                <div class="carousel slide" id="new-8-carousel"><a class="carousel-control left"
                                                                   href="#new-8-carousel"
                                                                   data-slide="prev">&lsaquo;</a> <a
                            class="carousel-control right" href="#new-8-carousel"
                            data-slide="next">&rsaquo;</a>

                    <div class="carousel-inner">


                        @for ($i = 0; $i < count($upcoming_products); $i=$i+4)

                            @if($i==0)<?php $active = "active"; ?> @else <?php $active = ""; ?> @endif
                            <div class="item {{$active}}">
                                <div class="row-fluid">
                                    @for($j=0;$j<4;++$j)
                                        @if($i+$j < count($upcoming_products))

                                            <?php $images = $upcoming_products[$i + $j]->images; ?>
                                            <?php $id = $upcoming_products[$i + $j]->id;
                                            $category_shortcode = $upcoming_products[$i + $j]->category->shortcode;
                                            $category_name = $upcoming_products[$i + $j]->category->name;
                                            $product_name = $upcoming_products[$i + $j]->name;
                                            $product_shortcode = $upcoming_products[$i + $j]->shortcode;
                                            $url = URL::to("$category_shortcode/$product_shortcode");
                                            ?>

                                            <div
                                                    class="p-item upcoming_products product_block ajax_block_product first_item p-item clearfix span3"
                                                    data-url="{{$url}}">
                                                <div class="product-container">
                                                    <a href="{{$url}}" class="product_image">

                                                        @if(isset($images))
                                                            <?php $image = HtmlUtil::getPrimaryImage($images); ?>
                                                            <?php $path = $image['path']; ?>

                                                            @if(!is_null($path))
                                                                <img src="{{URL::to($path)}}" alt="" class="tab_image"/>
                                                            @else
                                                                <img src="http://placehold.it/300X300" alt=""/>
                                                            @endif
                                                        @endif
                                                    </a>
                                                    <h5 class="s_title_block">
                                                        <a href="{{$url}}" title="">
                                                            {{$upcoming_products[$i+$j]->name}}
                                                        </a>
                                                    </h5>

                                                    <div class="product_desc">
                                                        <a href="{{$url}}"
                                                           title="More">{{$upcoming_products[$i+$j]->description}}</a>
                                                    </div>
                                                    <?php $offer_price = round($upcoming_products[$i + $j]->pivot->offer_price);
                                                    $list_price = $upcoming_products[$i + $j]->list_price;
                                                    ?>
                                                    @if(isset($offer_price) && $offer_price>0 && $offer_price!=0 && $list_price != $offer_price && $list_price > $offer_price)
                                                        <?php $strike_through = "price-line-through";
                                                        $font = "font-14px";
                                                        $discount = ($list_price - $offer_price) / $list_price * 100;
                                                        $discount_label = "(" . round($discount) . "% OFF)";
                                                        ?>
                                                    @else
                                                        <?php $strike_through = "";
                                                        $font = "font-17px";
                                                        $discount = 0;
                                                        ?>
                                                    @endif

                                                    <div>

                                                        <p class="price_container {{$font}}">
                                                                <span class="{{$font}} {{$strike_through}}">
                                                                    <span class="WebRupee {{$strike_through}}"> Rs. </span>
                                                                    {{number_format($list_price)}}
                                                                </span>
                                                            @if(!empty($discount)) {{$discount_label}}  @endif
                                                            @if(isset($offer_price) && $offer_price>0 && $list_price != $offer_price && $list_price > $offer_price)
                                                            <span class="price font-17px" style="float:right;">
                                                                    <span class="WebRupee"> Rs. </span>
                                                                {{number_format($offer_price)}}
                                                                </span>
                                                            @endif
                                                        </p>
                                                    </div>
                                                    <br/>
                                                </div>
                                            </div>

                                        @endif
                                    @endfor
                                </div>
                            </div>
                        @endfor


                    </div>
                </div>
            </div>
        </div>
    </div>
@endif