<script type="text/javascript">
    $(document).ready(function () {

        var feedbackform = $('#feedbackForm');
        feedbackform.validate();
        $('#feedback_submit_button').click(function (e) {

            $('.ajax_loader').addClass('hidden');
            $('.form_result').html("");

            e.preventDefault();
            if (feedbackform.valid()) {
                var url = feedbackform.attr('action');
                $('.ajax_loader').removeClass('hidden');
                $.post(url, feedbackform.serialize(), function (data) {

                    if (data.success = "true") {

                        $('.ajax_loader').addClass('hidden');
                        $('.form_result').html('Feedback has been submitted successfully').css({color: "green"});
                        feedbackform[0].reset();

                    } else {

                        $('.ajax_loader').addClass('hidden');
                        $('.form_result').html('Some error occurred please try again later').css({color: "red"});
                        feedbackform[0].reset();

                    }

                });

            } else {
                return;

            }
        });
    });
</script>


<div class="slide-out-div hidden-phone"
     style="line-height: 1; position: fixed; height: 576px; top: 220px; right: -431px;">
    <a class="handle" href="#" style="display: block; outline: none; position: absolute; top: 0px; left: -40px;"></a>

    <div class="form-header-message">
        <h3>Got a Problem?</h3>

        <div class="header-message-disc"><a href="{{URL::to('contact-us')}}" target="_blank">
                <button>Contact Support</button>
            </a>

            <div class="top-spacer"> For <b>buying assistance</b> or any other <b>order related</b> query, kindly get in
                touch with our
                support team.
            </div>
            <div class="clearer"> &nbsp;</div>
        </div>
        <hr class="hr-break">
        <h3> Tell us what you think.</h3>

        <p> Love us / have suggestions / ideas / feature requests? Tell us how we can improve our website.</p>
    </div>
    <div class="feedback-success" style="display: none" id="feedback-success-message">Thank you for giving us the
        feedback
    </div>
    <form action="{{URL::to('feedback/ajax-data')}}" name="feedbackForm" id="feedbackForm" method="post">
        <fieldset>
            <div class="feedback-form-wrapper">
                <ul class="form-list">

                    <li class="li-feedback">
                        <label for="feedback_email" class="required"><em>*</em>Email</label>

                        <div class="input-box">
                            <input type="text" name="feedback_email" id="feedback_email" class="required email">
                        </div>
                    </li>


                    <li class="li-feedback">
                        <label for="feedback_mobile">Mobile</label>

                        <div class="input-box">
                            <input type="text" name="feedback_mobile" id="feedback_mobile" class="required digits" data-rule-maxlength="10" data-rule-minlength="10">
                        </div>
                    </li>

                    <li class="li-feedback">
                        <label for="feedback_category" class="required"><em>*</em>Category</label>

                        <div class="input-box">
                            <select name="feedback_category" id="feedback_category">
                                <option value="Improve this page" selected="selected">Improve this page</option>
                                <option value="Suggest new features/ideas">Suggest new features/ideas</option>
                                <option value="Suggest new products/categories">Suggest new products/categories</option>
                                <option value="Shopping experience">Shopping experience</option>
                                <option value="Feedback on prices/offers">Feedback on prices/offers</option>
                                <option value="Others - General Feedback">Others - General Feedback</option>
                            </select>
                    </li>

                    <li class="li-feedback">
                        <label for="feedback_message"><em>*</em>Message</label>

                        <div class="input-box">
                            <textarea name="feedback_message" id="feedback_message" class="required"></textarea>
                        </div>
                    </li>
                    <!--                    <div id="experienceRadio" style="display: none">-->
                    <!--                        <li class="li-feedback">-->
                    <!--                            <label for="experienceRadio" class="required"><em>*</em>How would you rate your overall-->
                    <!--                                shopping experience on Milagrow-->
                    <!--                                Humantech</label>-->
                    <!---->
                    <!--                            <div class="input-box"> <span class="horizontalRadio">-->
                    <!--                <input type="radio" name="experienceRadio" value="love" id="experience">-->
                    <!--                Love it</span> <span class="horizontalRadio">-->
                    <!--                <input type="radio" name="experienceRadio" value="good" id="experience">-->
                    <!--                Good</span> <span class="horizontalRadio">-->
                    <!--                <input type="radio" name="experienceRadio" value="average" id="experience">-->
                    <!--                Average</span> <span class="horizontalRadio">-->
                    <!--                <input type="radio" name="experienceRadio" value="bad" id="experience">-->
                    <!--                Bad</span>-->
                    <!---->
                    <!--                                <div id="feedback_radio_error"></div>-->
                    <!--                            </div>-->
                    <!--                        </li>-->
                    <!--                    </div>-->
                    <li>
                        <p class="required">*Required Fields</p>
                        <input type="submit" id="feedback_submit_button" name="submit"
                               class="submit exclusive standard-checkout" value="Submit">
                        <span class="ajax_loader hidden"><img
                                src="{{asset('frontoffice/img/ajax-loader.gif')}}" alt=""/>.</span>
                        <span class="form_result"></span>
                    </li>
                </ul>
            </div>
        </fieldset>
    </form>
</div>

