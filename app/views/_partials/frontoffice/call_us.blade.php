<script type="text/javascript"
        src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<style type="text/css">
    .modal {
        width: 560px !important;
        margin-left: -280px !important;
    }

    label.error {
        display: block;
    }
</style>
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel2"></h4>

                <h3 style="margin:0px;">
                    Call Us
                </h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="{{URL::to('call/data')}}" role="form" id="call_us_form"
                      method="post">

                    <div class="control-group text">
                        <label for="call_name" class="control-label">Name<sup>*</sup></label>

                        <div class="controls">
                            <input type="text" name="call_name" class="required" id="call_name">
                        </div>
                    </div>

                    <div class="control-group text">
                        <label for="call_email" class="control-label">Email<sup>*</sup></label>

                        <div class="controls">
                            <input type="text" name="call_email" class="required email" id="call_email">
                        </div>
                    </div>


                    <div class="control-group text">
                        <label for="call_mobile" class="control-label">Mobile No</label>

                        <div class="controls">
                            <input type="text" name="call_mobile" class="required digits" id="call_mobile">
                        </div>
                    </div>


                    <input type="hidden" name="link" value="{{Request::url()}}"/>

                    <div class="control-group text">
                        <div class="offset2">
                                    <span class="hide" id="ajax_loader">
                                        <img src="{{URL::to('frontoffice/img/ajax-loader.gif')}}" alt=""/>
                                        calling please wait...
                                    </span>

                            <span id="call_status"></span>

                        </div>
                    </div>


                    <div class="control-group submit2">
                        <div class="controls">
                            <input type="submit" class="submit submit_call exclusive standard-checkout"
                                   value="Submit"/>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>