<script type="text/javascript">
    $(document).ready(function () {

        $('.latest_products').click(function () {

            var latest_product = $(this);
            var url = latest_product.data('url');
            window.location.href = url;

        });

    });
</script>
<style type="text/css">
    .img-box-latest {
        min-height: 227px;
    }
</style>
<!--        latest offers starts here-->
<div class="leo-carousel">
    <div id="blockleohighlightcarousel-new"
         class="block products_block exclusive blockleohighlightcarousel span6 blue">
        <h3 class="title_block">Latest Offers</h3>

        <div class="block_content">

            <div class="carousel slide" id="new-7-carousel"><a class="carousel-control left"
                                                               href="#new-7-carousel"
                                                               data-slide="prev">&lsaquo;</a> <a
                    class="carousel-control right" href="#new-7-carousel"
                    data-slide="next">&rsaquo;</a>

                <div class="carousel-inner">

                    @if(!is_null($latest_offers))

                    @for ($i = 0; $i < count($latest_offers); $i=$i+2)

                    @if($i==0)<?php $active = "active"; ?> @else <?php $active = ""; ?> @endif
                    <div class="item {{$active}}">
                        <div class="row-fluid">
                            @for($j=0;$j<2;++$j)
                            @if($i+$j < count($latest_offers))

                            <?php $images = $latest_offers[$i + $j]->images; ?>
                            <?php $id = $latest_offers[$i + $j]->id;
                            $category_shortcode = $latest_offers[$i + $j]->category->shortcode;
                            $category_name = $latest_offers[$i + $j]->category->name;
                            $product_name = $latest_offers[$i + $j]->name;
                            $product_shortcode = $latest_offers[$i + $j]->shortcode;
                            $url = URL::to("$category_shortcode/$product_shortcode");
                            ?>


                            <div
                                class="p-item latest_products myspan6 product_block ajax_block_product first_item p-item clearfix"
                                data-url="{{$url}}">
                                <div class="product-container">
                                    <div class="img-box-latest">
                                        <a href="{{$url}}" title="{{$category_name}}" class="product_image">

                                            @if(isset($images))
                                            <?php $image = HtmlUtil::getPrimaryImage($images); ?>
                                            <?php $path = $image['path']; ?>

                                            @if(!is_null($path))
                                            <img src="{{URL::to($path)}}" alt="{{$category_name}}" class="tab_image"/>
                                            @else
                                            <img src="http://placehold.it/300X300" alt=""/>
                                            @endif
                                            @endif
                                        </a>
                                    </div>
                                    <h5 class="s_title_block">
                                        <a href="" title="">
                                            {{$latest_offers[$i+$j]->name}}
                                        </a>
                                    </h5>

                                    <div class="product_desc">
                                        <a href="" title="More">
                                            {{$latest_offers[$i+$j]->description}}
                                        </a>
                                    </div>
                                    <div>
                                        <?php $offer_price = round($latest_offers[$i + $j]->pivot->offer_price);
                                        $list_price = $latest_offers[$i + $j]->list_price;
                                        ?>
                                        @if(isset($offer_price) && $offer_price>0 && $list_price != $offer_price &&
                                        $list_price > $offer_price)
                                        <?php $strike_through = "price-line-through";
                                        $font = "font-14px";
                                        $discount = ($list_price - $offer_price) / $list_price * 100;
                                        $discount_label = "(" . round($discount) . "% OFF)";
                                        ?>
                                        @else
                                        <?php $strike_through = "";
                                        $font = "font-17px pull-right price";
                                        $discount = 0;
                                        ?>
                                        @endif

                                        <p class="price_container {{$font}}">
                                                                <span class="{{$font}} {{$strike_through}}">
                                                                    <span
                                                                        class="WebRupee {{$strike_through}}"> Rs. </span>
                                                                    {{number_format($latest_offers[$i+$j]->list_price)}}
                                                                </span>
                                            @if(!empty($discount)) {{$discount_label}} @endif
                                            @if(isset($offer_price) && $offer_price>0 && $list_price != $offer_price &&
                                            $list_price > $offer_price)
                                                                <span class="price font-17px" style="float:right;">
                                                                    <span class="WebRupee"> Rs. </span>
                                                                    {{number_format($latest_offers[$i+$j]->pivot->offer_price)}}
                                                                </span>
                                            @endif
                                        </p>
                                    </div>
                                    <br/>

                                </div>
                            </div>

                            @endif
                            @endfor
                        </div>
                    </div>
                    @endfor
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>