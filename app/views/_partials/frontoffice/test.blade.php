<!--filter products partial view-->
@include('_partials.frontoffice.filters')
<script type="text/javascript"  src="http://code.jquery.com/jquery-latest.js"></script>

<style type="text/css">
    .block {
        /*position: absolute;*/
        /*background: #eee;*/
        padding: 20px;
        width: 300px;
        /*border: 1px solid #ddd;*/
    }
    .span4{position: absolute; padding: 20px;}

 </style>
<script type="text/javascript">
    var colCount = 0;
    var colWidth = 300;
    var margin = 5;
    var spaceLeft = 0;
    var windowWidth = 0;
    var blocks = [];

    $(function(){
        setupBlocks();
        $(window).resize(setupBlocks);
    });

    function setupBlocks() {
        windowWidth = $('#product-block').width();
        blocks = [];

        // Calculate the margin so the blocks are evenly spaced within the window
        colCount = Math.floor(windowWidth/(colWidth+margin*2));
        spaceLeft = (windowWidth - ((colWidth*colCount)+(margin*(colCount-1)))) / 2;
        console.log(spaceLeft);

        for(var i=0;i<colCount;i++){
            blocks.push(margin);
            console.log(blocks);
        }
        positionBlocks();
    }

    function positionBlocks() {
        var j = 0;
        $('.span4').each(function(i){

            var window_left = $('#product-block').left;
            var min = Array.min(blocks);
            var index = $.inArray(min, blocks);

            if(j%2 == 1){
                var leftPos = margin+(index*(colWidth+margin+window_left));
            }else{
                var leftPos = margin+(index*(colWidth+margin));
            }
            $(this).css({
                'left':(leftPos)+spaceLeft+'px',
                'top':min+'px'
            });
            blocks[index] = min+$(this).outerHeight()+margin;

    //            var i=1;
//            var block = [];
//            block.push($(this).height());
//            if(i % 2 == 1 && i != 1){
//                $(this).css('top',block[i-2] + 5 + px);
//            }
            alert(j);
            j = j+1;
//            console.log("height  " + $(this).height() + "width   " + $(this).width()) ;
        });
    }

    // Function to get the Min value in Array
    Array.min = function(array) {
        return Math.min.apply(Math, array);
    };


</script>

<!--products starts here-->


@if(!is_null($products))

@for ($i = 0; $i < count($products); $i=$i+3)

<div class="row-fluid" id="product-block">


    @for($j=0;$j<3;++$j)
    @if($i+$j < count($products))
    <?php $id = $products[$i + $j]->id;
    $category_name = $products[$i + $j]->category->name;
    $category_shortcode = $products[$i + $j]->category->shortcode;
    $product_name = $products[$i + $j]->name;
    $product_shortcode = $products[$i + $j]->shortcode;
    $url = URL::to("$category_shortcode/$product_shortcode");

    ?>

    <div class="span4">

        <?php $images = $products[$i + $j]->images; ?>
        @if(isset($images))
        <?php $image = HtmlUtil::getPrimaryImage($images); ?>
        <?php $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE;
        $title = isset($image['title']) ? $image['title'] : $products[$i + $j]->name;
        ?>
        @else
        <?php $path = Constants::DEFAULT_300_IMAGE;;
        $title = $products[$i + $j]->name;
        ?>
        @endif

        <div class="product_detail_box padding5">
            <div class="row-fluid">
                <a href="{{$url}}"><img src="{{URL::to($path)}}" /></a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4 class="blue-heading">{{$products[$i+$j]->name}} </h4>
                <!--                <p>{{$products[$i + $j]->sequence}}</p>-->

                <p class="margin-top10">A sleek and stylish new hood with an interplay of glass and matt
                    steel</p>
            </div>
            <div class="row-fluid">
                <div class="accordionButton margin-top10">
                    <span class="plusMinus">+</span> <span>Features</span>
                </div>
                <div class="accordionContent margin-top10">

                    <ul class="product-features">
                        {{$products[$i+$j]->description}}
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>

            @if(isset($session_ids) && sizeof($session_ids))
            <?php $key = array_search($id, $session_ids);
            $disabled = is_int($key) ? "disabled" : "";?>
            @else
            <?php $disabled = "" ?>
            @endif

            <div class="row-fluid margin-top-20px">
                <div class="span9">
                    <button data-id="{{$id}}" id="compare_{{$id}}" class="add_to_compare dark_button"
                    {{$disabled}}>Add to Compare
                    </button>
                </div>
                <div class="span3">
                    <a href="#" data-id="{{$id}}" class="add_to_cart_image">
                        <input type="hidden" data-item-type="product" data-id="{{$id}}" name="qty" id="quantity_wanted"
                               class="text qty_{{$id}}" value="1" size="2">
                        <img src="{{asset('frontoffice/img/add-cart.png')}}" title="Add to Cart"/>
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row-fluid margin-top-20px">

                @if($products[$i+$j]->offer_price!=null)
                <?php $strike_through = "price-line-through";
                $offer_price = $products[$i + $j]->offer_price;
                ?>
                @else
                <?php $strike_through = "";
                $offer_price = null;
                ?>
                @endif

                <div class="span6">
                    <div class="price">
                        <p class="our_price_display font-17px">
                            <span id="our_price_display" class="{{$strike_through}}">
                                <span class="WebRupee"> Rs. </span>{{number_format(ceil($products[$i+$j]->list_price))}}</span>
                        </p>
                    </div>
                </div>


                @if(isset($offer_price))
                <div class="span6 text-right">
                    <div class="price">
                        <p class="our_price_display font-17px">
                            <span style="padding-left:5px; color:#333;">
                                <span class="WebRupee"> Rs. </span>{{number_format($offer_price)}}</span>
                        </p>
                    </div>
                </div>
                @endif


            </div>

        </div>
    </div>

    @endif
    @endfor

</div>

<br/>
@endfor

@else
<h4>No products found</h4>
@endif

<br/>


<!--products ends here-->


<!--compare bar partial view -->
@include('_partials.frontoffice.compare_bar')

<!--ajax loader-->
<div class="ajax_loader_div hidden">
    <img src="{{asset('frontoffice/img/loader.gif')}}" alt="" width="35" height="35"/>
    Loading
</div>

<script type="text/javascript" src="{{asset('frontoffice/js/cart_modal.js')}}"></script>

<style type="text/css">
    #gray_strip{
        display: none!important;
    }
</style>