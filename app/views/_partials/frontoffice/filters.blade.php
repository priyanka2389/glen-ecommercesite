<div class="row-fluid" id="content_container">

    <div class="span12 margin-top-20px">
        <form action="#" id="layered_form">
            {{$filters_html}}
        </form>

    </div>

    <div class="row-fluid">
        <div style="float: left">
            <br/>
            @if(!is_null($products))
            <p class="select">
                <label for="selectPrductSort">Sort by</label>&nbsp;
                <select id="selectPrductSort" class="selectProductSort">
                    <option value="" >-Select-</option>
                    <option value="popularity" >Popularity</option>
                    <option value="lowest">Price: Lowest first</option>
                    <option value="highest">Price: Highest first</option>
                </select>
            </p>
            @endif
        </div>

        <div class="" style="float:right;">
    <!--        <a href="#" class="button_large pull-right margin-top10 margin-bottom10 ">Search</a> margin-right10-->
            <a href="" id="reset_filter"
               class="button_large pull-right margin-top10 margin-bottom10 ">Reset</a>
        </div>
    </div>
</div>
