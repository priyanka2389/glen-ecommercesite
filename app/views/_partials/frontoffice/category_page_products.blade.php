<!--filter products partial view-->
@include('_partials.frontoffice.filters')


<!--products starts here-->
<style type="text/css">
    .outstock {
        color: #d21f45;
        font-weight: bold;
        margin-left: 10px;
        font-size: 14px;
    }

    .product-block {
        width: 31% !important;
        margin: 0 0 15px 31px !important;
    }
    .product-block:first-child {
        margin-left: 0 !important;
    }
</style>

@if(!is_null($products))

@for ($i = 0; $i < count($products); $i=$i+3)

<div class="row-fluid">

    @for($j=0;$j<3;++$j)
    @if($i+$j < count($products))
    <?php

    $id = $products[$i + $j]->id;
    $ids = array();
    array_push($ids, $id);

    $category_name = $products[$i + $j]->category->name;
    $category_shortcode = $products[$i + $j]->category->shortcode;
    $product_name = $products[$i + $j]->name;
    $product_shortcode = $products[$i + $j]->shortcode;
    $url = URL::to("$category_shortcode/$product_shortcode");
    $title = $category_name ."/". $product_name
    ?>

    <div class="span4 product-block">

        <?php $images = $products[$i + $j]->images; ?>
        @if(isset($images))
        <?php $image = HtmlUtil::getPrimaryImage($images); ?>
        <?php $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE;

        ?>
        @else
        <?php $path = Constants::DEFAULT_300_IMAGE;;

        ?>
        @endif

        <div class="product_detail_box padding5 productModule shadow-box">

            <?php if (!empty($products[$i + $j]->combos)) {
                $combos = $products[$i + $j]->combos;
                $no_of_combos = count($combos);
            } ?>
            @if($no_of_combos > 0)
            <div class="newofferTag DISCOUNT"><span class="flap"></span> <span
                    class="offerLogo"></span>
                <!--<div class="offerText">
                      40% Off
                  </div>-->
                <div class="moreOffers"><span
                        class="offerCount">@if($no_of_combos > 1)
                                                OFFERS @else OFFER @endif </span>

                    <div class="listOfOffers"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            @endif


            <div class="row-fluid">
                <a href="{{$url}}"><img src="{{URL::to($path)}}" alt="{{$products[$i + $j]->name}}"
                                        title="{{$title}}" alt="{{$title}}"/></a>
            </div>
            <div class="row-fluid">
                <br/>
                <h4 class="blue-heading">{{$products[$i+$j]->name}} </h4>
                <!--                <p>{{$products[$i + $j]->sequence}}</p>-->

                {{--<p class="margin-top10">A sleek and stylish new hood with an interplay of glass and matt--}}
                    {{--steel</p>--}}
            </div>
            <div class="row-fluid">
                <div class="accordionButton margin-top10">
                    <span class="plusMinus">+</span> <span>Features</span>
                </div>
                <div class="accordionContent margin-top10">

                    <ul class="product-features">
                        {{$products[$i+$j]->description}}
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>

            @if(isset($session_ids) && sizeof($session_ids))
            <?php $key = array_search($id, $session_ids);
            $disabled = is_int($key) ? "disabled" : "";?>
            @else
            <?php $disabled = "" ?>
            @endif

            <div class="row-fluid margin-top-20px">
                <div class="span8">
                    <button data-id="{{$id}}" id="compare_{{$id}}" class="add_to_compare dark_button"
                    {{$disabled}} >Add to Compare
                    </button>
                </div>
                {{--onclick="var res = add_to_compare(this); if(res == true) {this.disabled = true;}else{this.disabled =
                false;}"--}}
                <div class="span4">
                    <?php $is_upcoming_product = false;
                    //  echo "<pre>";print_r($products->tags);echo "</pre>";
                    ?>
                    @if(isset($products[$i+$j]->tags))
                    @for($k=0;$k < count($products[$i+$j]->tags);$k++)
                    <?php $tags = $products[$i + $j]->tags; ?>
                    @if($tags[$k]->name == 'upcoming_products')
                    <?php $is_upcoming_product = true; ?>
                    @endif
                    @endfor
                    @endif
                    @if($is_upcoming_product == true)
                    <img src="{{asset('frontoffice/img/coming-soon.png')}}" title="Coming Soon"
                         class="add_to_cart_image"/>
                    <?php $is_upcoming_product = false; ?>
                    @else
                    @if($products[$i+$j]->availability ==1)
                    <a href="#" data-id="{{$id}}" class="add_to_cart_image">
                        <input type="hidden" data-item-type="product" data-id="{{$id}}"
                               name="qty" id="quantity_wanted"
                               class="text qty_{{$id}}" value="1" size="2">
                        <img src="{{asset('frontoffice/img/add-cart.png')}}"
                             title="Add to Cart"/>
                    </a>
                    @else
                    <ul>
                        <li class="outstock cart">Out of stock</li>
                    </ul>
                    @endif

                    @endif

                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row-fluid">
                <?php  $offer_price = $products[$i + $j]->offer_price;
                $list_price = $products[$i + $j]->list_price;?>
                @if($products[$i+$j]->offer_price!=null && $products[$i+$j]->offer_price!=0)
                <?php $strike_through = "price-line-through";

                $price_font = "font-14px";
                $offer_font = "font-17px pull-right price";
                $discount = ($list_price - $offer_price) / $list_price * 100;
                $discount_label = "(" . round($discount) . "% OFF)";
                ?>
                @else
                <?php $strike_through = "";
                $offer_price = null;
                $offer_font = "";
                $price_font = "font-17px pull-right price";
                $discount = 0;
                ?>
                @endif

                {{--
                <div class="span6">--}}
                    {{--
                    <div class="price">--}}
                        {{--<p class="our_price_display {{$price_font}}">--}}
                            {{--<span id="our_price_display" class="{{$strike_through}}">--}}
                                {{--<span class="WebRupee"> Rs. </span>{{number_format(ceil($products[$i+$j]->list_price))}}</span>--}}
                            {{--@if(!empty($discount)) {{$discount_label}} @endif--}}
                            {{--</p>--}}
                        {{--
                    </div>
                    --}}
                    {{--
                </div>
                --}}


                {{--@if(isset($offer_price))--}}
                {{--
                <div class="span6 text-right">--}}
                    {{--
                    <div class="price">--}}
                        {{--<p class="our_price_display {{$offer_font}}">--}}
                            {{--<span style="padding-left:5px;">--}}
                                {{--<span class="WebRupee"> Rs. </span>{{number_format($offer_price)}}</span>--}}
                            {{--</p>--}}
                        {{--
                    </div>
                    --}}
                    {{--
                </div>
                --}}
                {{--@endif--}}

                <p class="price_container {{$price_font}}">
                                                        <span class=" {{$price_font}} {{$strike_through}}">
                                                            <span class="WebRupee {{$strike_through}}"> Rs. </span>
                                                            {{number_format($list_price)}}
                                                        </span>
                    @if(!empty($discount)) {{$discount_label}} @endif
                    @if($list_price != $offer_price && $list_price > $offer_price && $offer_price>0 && $list_price>0)
                                        <span class="{{$offer_font}}" style="float:right;"><span
                                                class="WebRupee"> Rs. </span>{{number_format($offer_price)}}</span>
                    @endif
                </p>


            </div>

        </div>
    </div>

    @endif
    @endfor

</div>

<br/>
@endfor

@else
<h4>No products found</h4>
@endif

<!--products ends here-->


<!--compare bar partial view -->
@include('_partials.frontoffice.compare_bar')

<!--ajax loader-->
<div class="ajax_loader_div hidden">
    <img src="{{asset('frontoffice/img/loader.gif')}}" alt="" width="35" height="35"/>
    Loading
</div>

<script type="text/javascript" src="{{asset('frontoffice/js/cart_modal.js')}}"></script>

<style type="text/css">
    #gray_strip {
        display: none !important;
    }

    .add_to_cart_image {
        margin-right: 10px;
        float: right;
    }

    #layered_form .row-fluid [class*="span"]:nth-child(5), .row-fluid [class*="span"]:nth-child(9) {
        margin-left: 0px;
    }

    /*@media (max-width: 320px) and  (max-width: 480px){*/
    /*#layered_form .nav-tabs > .active > a, .nav-tabs > .active > a:hover, .nav-tabs > .active > a:focus{padding-bottom: 15px !important;}*/
    /*}*/
</style>

<script type="text/javascript">
    var product_ids = new Array();
    <?php if(!empty($product_ids)) {
    foreach($product_ids as $key => $val){ ?>
    product_ids.push(<?php echo $val; ?>);
    <?php }
    } ?>

    <?php if(!empty($product_ids)) { ?>
    var google_tag_params = {
        ecomm_pagetype: 'category',
        ecomm_prodid: product_ids,
        ecomm_category: '<?php if(isset($category_name)){ echo $category_name; }?>'
    };
    console.log(google_tag_params);
    <?php } ?>
</script>