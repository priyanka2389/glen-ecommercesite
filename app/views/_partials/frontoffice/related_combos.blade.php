@if(!is_null($related_combos))
<div id="relatedproducts" class="block products_block exclusive blockleorelatedproducts">
    <h3 class="title_block"><span class="tcolor">Other</span> Combos</h3>

    <div class="block_content">
        <div class=" carousel slide" id="leorelatedcarousel">

            @if(!is_null($related_combos) && count($related_combos)>3)
            <div class="button-carousel">
                <a class="carousel-control left" href="#leorelatedcarousel" data-slide="prev">‹</a>
                <a class="carousel-control right" href="#leorelatedcarousel" data-slide="next">›</a>
            </div>
            @endif

            <div class="carousel-inner">

                @for ($i = 0; $i < count($related_combos); $i=$i+3)
                @if($i==0)<?php $active = "active"; ?> @else <?php $active = ""; ?> @endif
                <div class="item {{$active}}">
                    <div class="row-fluid">

                        @for($j=0;$j<3;++$j)
                        @if($i+$j < count($related_combos))

                        <?php $images = $related_combos[$i + $j]->images; ?>
                        <?php $image = HtmlUtil::getPrimaryImage($images); ?>
                        <?php $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE; ?>

                        <?php $id = $related_combos[$i + $j]->id;
                        $combo_name = $related_combos[$i + $j]->name;
                        ?>

                        <div class="p-item myspan4 product_block ajax_block_product first_item p-item clearfix">
                            <div class="product-container">
                                <a href="{{URL::to('combo/'.$related_combos[$i + $j]->shortcode)}}" title="{{$related_combos[$i + $j]->name}}"
                                   class="product_image">

                                    <img src="{{URL::to($path)}}">
                                    <!--                                    <span class="new">New</span>-->
                                </a>


                                <h5 class="s_title_block">
                                    <a href="{{URL::to('combo/'.$related_combos[$i + $j]->shortcode)}}" title="">{{$related_combos[$i + $j]->name}}</a></h5>

                                <div class="product_desc">{{$related_combos[$i + $j]->description}}</div>
                                <div>
                                    <input type="hidden" data-item-type="combo" data-id="{{$id}}" name="qty"
                                           id="quantity_wanted" class="text qty_{{$id}}" value="1"  size="2">
                                    <a class="exclusive ajax_add_to_cart_button cart_btn" href="#"
                                       style="background: white!important;"
                                       data-id="{{$id}}">Add to cart</a>
                                    <p class="price_container pull-right font-17px price" style="margin:6px 0;">
                                        <span class="price font-17px">
                                            <span class="WebRupee"> Rs. </span>{{number_format($related_combos[$i + $j]->combo_price)}}
                                        </span>
                                    </p>


                                </div>
                            </div>
                        </div>

                        @endif
                        @endfor

                    </div>
                </div>
                @endfor
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {
        $('.blockleorelatedproducts .carousel').each(function () {
            $(this).carousel({
                pause: true,
                interval: false
            });
        });

    });
</script>
@endif