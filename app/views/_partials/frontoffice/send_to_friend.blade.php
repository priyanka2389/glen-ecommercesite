<!--<script type="text/javascript"-->
<!--        src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>-->
<style type="text/css">
    .modal {
        width: 560px !important;
        margin-left: -280px !important;
    }

    label.error {
        display: block;
    }
</style>
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel1"></h4>

                <h3 style="margin:0px;">
                    Send To Friend
                </h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="{{URL::to('message/data')}}" role="form" id="send_to_friend_form"
                      method="post">

                    <div class="control-group text">
                        <label for="your_name" class="control-label">Your Name<sup>*</sup></label>

                        <div class="controls">
                            <input type="text" name="your_name" class="required" id="your_name">
                        </div>
                    </div>


                    <div class="control-group text">
                        <label for="your_email" class="control-label">Your Email</label>

                        <div class="controls">
                            <input type="text" name="your_email" class="required email" id="your_email">
                        </div>
                    </div>

                    <div class="control-group text">
                        <label for="friend_name" class="control-label">Friend Name<sup>*</sup></label>

                        <div class="controls">
                            <input type="text" name="friend_name" class="required" id="friend_name">
                        </div>
                    </div>

                    <div class="control-group text">
                        <label for="friend_email" class="control-label">Friend Email<sup>*</sup></label>

                        <div class="controls">
                            <input type="text" name="friend_email" class="required email" id="friend_email">
                        </div>
                    </div>

                    <div class="control-group text">
                        <label for="message" class="control-label">Message</label>

                        <div class="controls">
                            <textarea name="message" cols="30" rows="4" id="message" > </textarea>
                        </div>
                    </div>

                    <input type="hidden" name="link" value="{{Request::url()}}"/>

                    <div class="control-group text">
                        <div class="offset2">
                                    <span class="hide" id="ajax_loader">
                                        <img src="{{URL::to('frontoffice/img/ajax-loader.gif')}}" alt=""/>
                                        Sending message request please wait...
                                    </span>

                            <span id="messge_status"></span>

                        </div>
                    </div>


                    <div class="control-group submit2">
                        <div class="controls">
                            <input type="submit" class="submit submit_message exclusive standard-checkout"
                                   value="Submit"/>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>