<script type="text/javascript">

    $(document).ready(function () {

        $('.featured_products').click(function () {

            var feature_product = $(this);
            var url = feature_product.data('url');
            window.location.href = url;

        });

    });
</script>
<style type="text/css">
    .img-box {
        min-height: 164px;
    }
</style>

<div id="featured-products_block_center" class="block products_block clearfix span6 red">
    <h3 class="title_block">Featured products</h3>

    <div class="block_content">
        <div style="/*min-height:280px;*/" class=" carousel slide" id="homefeatured">
            <a class="carousel-control left" href="#homefeatured" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#homefeatured" data-slide="next">&rsaquo;</a>

            <div class="carousel-inner">

                @if(!is_null($featured_products))
                @for ($i = 0; $i < count($featured_products); $i=$i+2)
                @if($i==0)<?php $active = "active"; ?> @else <?php $active = ""; ?> @endif

                <div class="item {{$active}}">

                    @for($j=0;$j<2;++$j)
                    @if($i+$j < count($featured_products))

                    <?php $images = $featured_products[$i + $j]->images; ?>
                    <?php $id = $featured_products[$i + $j]->id;
                    $category_shortcode = $featured_products[$i + $j]->category->shortcode;
                    $category_name = $featured_products[$i + $j]->category->name;
                    $product_name = $featured_products[$i + $j]->name;
                    $product_shortcode = $featured_products[$i + $j]->shortcode;
                    ?>
                    @if(isset($images))
                    <?php $image = HtmlUtil::getPrimaryImage($images); ?>
                    <?php $path = isset($image['path']) ? $image['path'] : Constants::DEFAULT_300_IMAGE;
                    $title = isset($image['title']) ? $image['title'] : $featured_products[$i + $j]->name;
                    ?>
                    @else
                    <?php $path = Constants::DEFAULT_300_IMAGE;;
                    $title = $featured_products[$i + $j]->name;
                    ?>
                    @endif

                    <?php $url = URL::to("$category_shortcode/$product_shortcode"); ?>

                    <div class="p-item featured_products ajax_block_product first_item last_item_of_line"
                         data-url="{{$url}}">
                        <div class="product-container">
                            <div class="row-fluid">
                                <div class="center_block span6 img-box">
                                    <a href="{{$url}}" title="{{$category_name}}" class="product_image">
                                        <img src="{{URL::to($path)}}" alt="{{$category_name}}">
                                    </a>
                                </div>
                                <div class="right_block span6">
                                    <h5 class="s_title_block"><a href="{{$url}}">
                                            {{$featured_products[$i+$j]->name}}</a></h5>

                                    <div class="product_desc"><a href="{{$url}}" title="More">
                                            {{$featured_products[$i+$j]->description}}</a></div>

                                    <?php $offer_price = round($featured_products[$i + $j]->pivot->offer_price);
                                    $list_price = $featured_products[$i + $j]->list_price;
                                    ?>
                                    @if(isset($offer_price) && $offer_price>0 && $list_price != $offer_price &&
                                    $list_price > $offer_price)
                                    <?php $strike_through = "price-line-through";
                                    $font = "font-14px";
                                    $discount = ($list_price - $offer_price) / $list_price * 100;
                                    $discount_label = "(" . round($discount) . "% OFF)";
                                    ?>
                                    @else
                                    <?php $strike_through = "";
                                    $font = "font-17px pull-right price";
                                    $discount = 0;
                                    ?>
                                    @endif

                                    <p class="price_container {{$font}}">
                                                        <span class=" {{$font}} {{$strike_through}}">
                                                            <span class="WebRupee {{$strike_through}}"> Rs. </span>
                                                            {{number_format($featured_products[$i+$j]->list_price)}}
                                                        </span>
                                        @if(!empty($discount)) {{$discount_label}} @endif
                                        @if($list_price != $offer_price && $list_price > $offer_price)
                                                            <span class="price font-17px" style="float:right;"><span
                                                                    class="WebRupee"> Rs. </span>{{number_format($featured_products[$i+$j]->pivot->offer_price)}}</span>
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endfor

                </div>

                @endfor
                @endif

            </div>

        </div>
    </div>
</div>

