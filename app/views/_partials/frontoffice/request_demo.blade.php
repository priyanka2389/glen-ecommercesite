<script type="text/javascript"
        src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<style type="text/css">
    .modal {
        width: 560px !important;
        margin-left: -280px !important;
    }

    label.error {
        display: block;
    }
</style>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"></h4>

                <h3 style="margin:0px;">
                    Request Demo
                </h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="{{URL::to('demo/data')}}" role="form" id="request_demo_form"
                      method="post">

                    <div class="control-group text">
                        <label for="name" class="control-label">Name<sup>*</sup></label>

                        <div class="controls">
                            <input type="text" name="name" class="required" id="name">
                        </div>
                    </div>


                    <div class="control-group text">
                        <label for="email" class="control-label">Email</label>

                        <div class="controls">
                            <input type="text" name="email" class="required email" id="email">
                        </div>
                    </div>

                    <div class="control-group text">
                        <label for="mobile" class="control-label">Phone No.<sup>*</sup></label>

                        <div class="controls">
                            <input type="text" name="mobile" class="required digits" id="mobile">
                        </div>
                    </div>

                    <div class="control-group text">
                        <label for="city" class="control-label">City<sup>*</sup></label>

                        <div class="controls">
                            <input type="text" name="city" class="required" id="city">
                        </div>
                    </div>

                    <div class="control-group text">
                        <label for="state" class="control-label">State<sup>*</sup></label>

                        <div class="controls">
                            <input type="text" name="state" class="required" id="state">
                        </div>
                    </div>

                    <div class="control-group text">
                        <label for="message" class="control-label">Message</label>

                        <div class="controls">
                            <textarea name="message" cols="30" rows="4" id="message"></textarea>
                        </div>
                    </div>

                    <input type="hidden" name="product" value="{{$product_id}}"/>

                    <div class="control-group text">
                        <div class="offset2">
                                    <span class="hide" id="ajax_loader">
                                        <img src="{{URL::to('frontoffice/img/ajax-loader.gif')}}" alt=""/>
                                        Creating demo request please wait...
                                    </span>

                            <span id="order_status"></span>

                        </div>
                    </div>


                    <div class="control-group submit2">
                        <div class="controls">
                            <input type="submit" class="submit submit_demo exclusive standard-checkout"
                                   value="Submit"/>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

