<!-- Footer -->
<style type="text/css">
    #emailNews {
        margin: 0;
        width: 173px;
    }

    #submit {
        padding: 0px 5px 5px 5px;
        margin: 0;
    }

    .subscribe {
        text-align: justify;
        line-height: 17px;
    }
</style>
<section id="bottom" style="display: none">
    <div class="container">
        <div class="row-fluid">
            <div id="homecontent-displayBottom" class="leo-manage">
                <div class="leo-custom customhtml">
                    <h1 class="title_block">Our Special Offers</h1>

                    <div class="row-fluid">
                        <div class="span4"><img
                                src="{{asset('frontoffice/modules/leomanagemodules/img/img-offers1.jpg')}}" alt=""/>
                        </div>
                        <div class="span4"><img
                                src="{{asset('frontoffice/modules/leomanagemodules/img/img-offers2.jpg')}}" alt=""/>
                        </div>
                        <div class="span4"><img
                                src="{{asset('frontoffice/modules/leomanagemodules/img/img-offers3.jpg')}}" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $(document).ready(function () {
                    $('.carousel').each(function () {
                        $(this).carousel({
                            pause: true,
                            interval: false
                        });
                    });
                    $(".blockleoproducttabs").each(function () {
                        $(".htabs li", this).first().addClass("active");
                        $(".tab-content .tab-pane", this).first().addClass("active");
                    });

                    $("#subscribe-form").on('submit',function (e) {
                        e.preventDefault();

                        var emailId = $("input#emailNews").val();
                        var list = $("input#list").val();
                        var url = "<?php echo URL::to('user/subscribe'); ?>";

                        if (emailId.length > 0) {
                            var dataString = '&email=' + emailId + '&list=' + list;
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: dataString,
                                success: function (data) {
                                    if (data == 1) {
                                        alert("Thank you for subscribing to our newsletter!!")
                                    } else if (data == "Already subscribed.") {
                                        alert("You are already subscribed!!")
                                    }
                                }
                            });
                        }

                    });
                });
            </script>
        </div>
    </div>
</section>
<footer id="footer" class="omega clearfix">
    <section class="footer">
        <div class="container">
            <div class="row-fluid">
                <div style="clear:both"></div>
                <div id="lofadvafooterfooter" class="lofadvafooter">
                    <div id="lofadva-pos-1" class="lof-position" style="width:100%">
                        <div class="lof-position-wrap">
                            <div class="lofadva-block-1 lof-block" style="width:30%; float:left;">
                                <div class="lof-block-wrap">
                                    <h2>Socialize</h2>
                                    <ul class="lof-items">
                                        <li class="lof-text">
                                            <div class="social">
                                                <a class="facebook" href="http://www.facebook.com/GlenIndia"
                                                   title="Facebook" target="_blank">facebook</a>
                                                <a class="dribble" href="http://blog.glenindia.com/"
                                                   title="Blog" target="_blank">Blog</a>
                                                <a class="mail" href="mailto:enquiry@glenindia.com?Subject=enquiry"
                                                   title="Mail" target="_top">mail</a>
                                                <a class="youtube"
                                                   href="https://www.youtube.com/user/GlenAppliancesIndia"
                                                   title="Youtube" target="_blank">Youtube</a>
                                            </div>
                                        </li>
                                    </ul>
                                    <h3 title="Subscribe to our newsletter and receive updates about upcoming products & latest offers.">
                                        Subscribe to our newsletter!</h3>

                                    <div class="row-fluid">

                                        <div class="control-group text">
                                            <div class="controls">
                                                <form name="subscribe-form" id="subscribe-form" method="post">
                                                    <input type="text" name="emailNews" class="required email" id="emailNews"
                                                           placeholder="Enter your e-mail address">
                                                    <input type="hidden" value="s6gvbNcsXtCbuKOCUJb2Lw" name="list"
                                                           id="list">
                                                    <input type="submit" class="submit exclusive standard-checkout"
                                                           id="submit"
                                                           value="Submit"/>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="lofadva-block-2 lof-block" style="width:20%; float:left;">
                                <div class="lof-block-wrap">
                                    <h2>My Account</h2>
                                    <ul class="lof-items">
                                        <!--                                        <li class="link"><a href="{{Url::to("user/address")}}" title="Terms" target="_blank">My Order</a></li>-->
                                        <li class="link"><a href="{{URL::to('user/my-account')}}" title="My Account"
                                                            target="_self">My Account</a></li>
                                        <li class="link"><a href="{{URL::to('order/my-orders')}}" title="My Orders"
                                                            target="_self">My Order</a></li>

                                        <li class="link"><a href="{{URL::to('product-manuals')}}"
                                                            title="Product Manuals"
                                                            target="_self">Product Manuals</a>
                                        </li>
                                        <li class="link"><a href="{{URL::to('recipe-book')}}"
                                                            title="Recipe Book">Recipe Book</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="lofadva-block-3 lof-block" style="width:16%; float:left;">
                                <div class="lof-block-wrap">
                                    <h2>About Us</h2>
                                    <ul class="lof-items">
                                        <li class="link"><a href="{{URL::to('corporate')}}" title="Corporate"
                                                >Corporate</a></li>
                                        <li class="link"><a href="{{URL::to('catalogues')}}" title="Catalogues"
                                                >Catalogues</a></li>
                                        <li class="link"><a href="{{URL::to('media-press-&-magazine-ads')}}"
                                                            title="Press" target="_self">Press</a></li>
                                        <li class="link"><a href="{{URL::to('contact-us')}}?page=career" title="Careers"
                                                            target="_self">Careers</a></li>
                                        <li class="link"><a href="{{URL::to('faqs')}}" title="FAQs"
                                                            target="_self">FAQ's</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="lofadva-block-3 lof-block" style="width:17%; float:left;margin-top:29px;">
                                <div class="lof-block-wrap">
                                    <h2></h2>
                                    <ul class="lof-items">
                                        <li class="link"><a href="{{URL::to('terms-conditions')}}"
                                                            title="Terms & Conditions"
                                                            target="_self">T&amp;C</a></li>
                                        <li class="link"><a href="{{URL::to('return-policy')}}" title="Return Policy"
                                                            target="_self">Return Policy</a>
                                        </li>

                                        <li class="link"><a href="{{URL::to('privacy-policy')}}" title="Privacy Policy"
                                                            target="_self">Privacy Policy</a>
                                        </li>

                                        <li class="link"><a
                                                href="{{asset('uploads/product/manuals/Service Charges.pdf')}}"
                                                target="_blank" title="About us" target="_self">Service Charges</a>
                                        </li>
                                        <li class="link"><a href="{{URL::to('page/Site-Map')}}" title="Site Map"
                                                            target="_self">Site Map</a>
                                        </li>


                                    </ul>
                                </div>
                            </div>

                            <div class="lofadva-block-5 lof-block" style="width:17%; float:left;">
                                <div class="lof-block-wrap">
                                    <h2>Reach us</h2>
                                    <ul class="lof-items">
                                        <li class="lof-module">

                                            <!-- Block Newsletter module-->
                                            <div id="newsletter_block_left" class="">
                                                <h3 class="title_block"><span class="tcolor">Reach us</span></h3>

                                                <div class="block_content">
                                                    <ul class="lof-items">
                                                        <li class="link">
                                                            <a href="{{URL::to('retail-stores')}}"
                                                               title="Retail Stores">Retail Stores</a>
                                                        </li>
                                                        <li class="link">
                                                            <a href="{{URL::to('service-centers')}}"
                                                               title="Service Centres" target="_self">Service
                                                                Centers</a>
                                                        </li>
                                                        {{--
                                                        <li class="link">--}}
                                                            {{--<a href="{{URL::to('customer-service')}}"
                                                                   title="Customer Service" target="_self">Customer
                                                                Service</a>--}}
                                                            {{--
                                                        </li>
                                                        --}}
                                                        <li class="link">
                                                            <a href="{{URL::to('distributors')}}" title="Distributors"
                                                               target="_self">Distributors</a>
                                                        </li>
                                                        <li class="link">
                                                            <a href="{{URL::to('contact-us')}}" title="Complaints"
                                                               id="complaints" target="_self">Complaints</a></li>
                                                        <li class="link">
                                                            <a href="{{URL::to('contact-us')}}" title="Contact us"
                                                               target="_self">Contact us</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="footer-bottom">
        <div class="container">
            <div class="row-fluid">
                <div class="span6">
                    <div class="copyright"> Copyright 2015 Glen India. All Rights Reserved</div>
                </div>
            </div>
        </div>
    </section>
</footer>
<script type="text/javascript">
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-34709410-1', 'auto');
    ga('send', 'pageview');


</script>
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1006004267;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt=""
             src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1006004267/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>