<header id="header">
    <section class="topbar">
        <div class="container">

            <script type="text/javascript">
                /* Blockusreinfo */

                $(document).ready(function () {
                    $(".leo-groupe").each(function () {
                        var content = $(".groupe-content");
                        $(".groupe-btn", this).click(function () {
                            content.toggleClass("eshow");
                            $('.navbar .btn-navbar,#search_block_top .search-btn').toggleClass('hide');
                            $('.links li span').remove();
                        });
                    });

                    $(window).resize(function () {
                        if ($(window).width() > 600) {
                            $(".groupe-content").removeClass('eshow');
                        }
                    });
                });
            </script>
            <div id="header_user" class="pull-right leo-groupe g-dropdown">
                <a class="groupe-btn visible-xs" alt="">Quick link <span class="caret"></span></a>
                {{--id="header_user_info"--}}
                <div class="pull-left groupe-content hidden-xs">
                    {{--<div class="nav-item">--}}
                    {{--<div class="item-top hidden-phone">--}}
                    {{--Customer Care: 1800-180-1998 |--}}
                    {{--@if(AppUtil::isUserLoggedIn() && !Auth::guest() && !AppUtil::isUserAdmin())--}}
                    {{--<a href="{{URL::to('user/logout')}}">Logout</a>--}}
                    {{--@else--}}
                    {{--<a href="{{URL::to('user/login')}}">Login</a>--}}
                    {{--@endif--}}
                    {{--| <a href="http://blog.glenindia.com/" target="_blank">Blog</a> | <a href="{{URL::to('contact-us')}}">Contact Us</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="nav-item" id="your_account">--}}
                    {{--<div class="item-top"><a href="#" title="Your Account">Your Account</a></div>--}}
                    {{--</div>--}}
                    <ul class="links">
                        {{--<li><a href="http://demo4leotheme.com/prestashop/leo_metro/en/myaccount" title="View my customer account" class="name-customer" rel="nofollow">Welcome <span>Priyanka tailor</span></a></li>--}}
                        <li>Customer Care: 1800-180-1998<span> |</span></li>
                        <li>
                            @if(AppUtil::isUserLoggedIn() && !Auth::guest() && !AppUtil::isUserAdmin())
                                <a href="{{URL::to('user/logout')}}">Logout</a><span> |</span>
                            @else
                                <a href="{{URL::to('user/login')}}">Login</a><span> |</span>
                            @endif
                        </li>
                        <li><a href="http://blog.glenindia.com/" target="_blank">Blog</a><span> |</span></li>
                        <li><a href="{{URL::to('contact-us')}}">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <!-- /Block user information module HEADER -->
        </div>
    </section>
    <section class="header">
        <div class="container">
            <div class="row-fluid">
                <div class="row-fuild">
                    <div class="span3">
                        <a id="header_logo" href="{{URL::to('/')}}" title="Glen india">
                            <img class="logo" src="{{asset('frontoffice/img/logo.png')}}" alt="Glen India">
                        </a>
                    </div>

                    <div class="span9">
                        <div class="span11">
                            <nav id="topnavigation">
                                <div class="row-fluid">
                                    <div class="navbar">
                                        <div class="navbar-inner"><a data-target=".nav-collapse" data-toggle="collapse"
                                                                     class="btn btn-navbar"> <span
                                                        class="icon-bar"></span>
                                                <span class="icon-bar"></span> <span class="icon-bar"></span> </a>

                                            <div class="nav-collapse collapse">
                                                <ul class="nav megamenu">
                                                    <li class=" active"><a href="{{URL::to('/')}}"><span
                                                                    class="menu-icon"
                                                                    style="background:url('../frontoffice/img/icon-home.png') no-repeat;">
            <span class="menu-title">Home</span></span></a></li>
                                                    <li class="parent dropdown"><a class="dropdown-toggle"
                                                                                   data-toggle="dropdown"
                                                                                   onclick="e.stopPropagation();"><span
                                                                    class="menu-title">Consumer Products</span><b
                                                                    class="caret"></b></a>

                                                        <div class="dropdown-menu menu-content mega-cols cols3">
                                                            <div class="row-fluid">
                                                                <div class="mega-col span4 col-1">
                                                                    <ul>
                                                                        <li class="parent dropdown-submenu mega-group">
                                                                            <a
                                                                                    class="dropdown-toggle"
                                                                                    data-toggle="dropdown"><span
                                                                                        class="menu-title">Chimneys</span><b
                                                                                        class="caret"></b></a>
                                                                            <ul class="dropdown-mega level1">
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('island-chimneys')}}"><span
                                                                                                class="menu-title">Island Chimneys</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('split-chimneys')}}"><span
                                                                                                class="menu-title">Split Chimneys</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('designer-hood-chimneys')}}"><span
                                                                                                class="menu-title">Designer Chimneys</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('straight-line-chimneys')}}"><span
                                                                                                class="menu-title">Straight line Chimneys</span></a>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="mega-col span4 col-2">
                                                                    <ul>
                                                                        <li class="parent dropdown-submenu mega-group">
                                                                            <a
                                                                                    class="dropdown-toggle"
                                                                                    data-toggle="dropdown"><span
                                                                                        class="menu-title">Built-in Series</span><b
                                                                                        class="caret"></b></a>
                                                                            <ul class="dropdown-mega level1">
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('microwave-oven')}}"><span
                                                                                                class="menu-title">Built-In Microwaves</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('built-in-ovens')}}"><span
                                                                                                class="menu-title">Built-In Ovens</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('induction-hobs')}}"><span
                                                                                                class="menu-title">Built-in Hobs (Induction)</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('glass-hobs')}}"><span
                                                                                                class="menu-title">Built-in Hobs (Gas)</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('stainless-steel-hob')}}"><span
                                                                                                class="menu-title">Stainless Steel Hob</span></a>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="mega-col span4 col-3">
                                                                    <ul>
                                                                        <li class="parent dropdown-submenu mega-group">
                                                                            <a
                                                                                    class="dropdown-toggle"
                                                                                    data-toggle="dropdown"><span
                                                                                        class="menu-title">Cooking Appliances</span><b
                                                                                        class="caret"></b></a>
                                                                            <ul class="dropdown-mega level1">
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('glass')}}"><span
                                                                                                class="menu-title">Glass &amp; Induction Cooktops</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('platinum-cooktops')}}"><span
                                                                                                class="menu-title">Platinum Cooktops</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('stainless-steel-cooktops')}}"><span
                                                                                                class="menu-title">Stainless steel Cooktops</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('indian-cooking-ranges')}}"><span
                                                                                                class="menu-title">Indian Cooking Ranges</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('international-cooking-ranges')}}"><span
                                                                                                class="menu-title">International Cooking Ranges</span></a>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <hr>
                                                                <div class="mega-col span4 col-1"
                                                                     style="margin-left:0px;">
                                                                    <ul>
                                                                        <li class="parent dropdown-submenu mega-group">
                                                                            <a
                                                                                    class="dropdown-toggle"
                                                                                    data-toggle="dropdown"><span
                                                                                        class="menu-title">Small Appliances</span></a>
                                                                            <ul class="dropdown-mega level1">
                                                                                <li class=""><a class="dropdown-toggle"
                                                                                                data-toggle="dropdown"
                                                                                                href="{{URL::to('food-processors')}}"><span
                                                                                                class="menu-title">Food Processors</span></a>
                                                                                </li>
                                                                                <li class=""><a class="dropdown-toggle"
                                                                                                data-toggle="dropdown"
                                                                                                href="{{URL::to('juicer-mixer-grinders')}}"><span
                                                                                                class="menu-title">Juicer Mixer Grinders</span></a>
                                                                                </li>
                                                                                <li class=""><a class=""
                                                                                                href="{{URL::to('mixer-grinders')}}"><span
                                                                                                class="menu-title">Mixer Grinders</span></a>
                                                                                </li>
                                                                                <li class=""><a class=""
                                                                                                href="{{URL::to('choppers')}}"><span
                                                                                                class="menu-title">Choppers</span></a>
                                                                                </li>

                                                                                <li class=""><a class=""
                                                                                                href="{{URL::to('slow-juicer')}}"><span
                                                                                                class="menu-title">Slow Juicers</span></a>
                                                                                </li>
                                                                                <li class=""><a class=""
                                                                                                href="{{URL::to('blender-grinders')}}"><span
                                                                                                class="menu-title">Blender &amp; Grinders</span></a>
                                                                                </li>
                                                                                <li class=""><a class=""
                                                                                                href="{{URL::to('hand-mixers')}}"><span
                                                                                                class="menu-title">Hand Mixers</span></a>
                                                                                </li>
                                                                                <li class=""><a class=""
                                                                                                href="{{URL::to('egg-master')}}"><span
                                                                                                class="menu-title">Egg masters</span></a>
                                                                                </li>

                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="mega-col span4 col-2">
                                                                    <ul>
                                                                        <li class="parent dropdown-submenu mega-group">
                                                                            <a
                                                                                    class="dropdown-toggle"
                                                                                    data-toggle="dropdown"
                                                                                    href="#"><span
                                                                                        class="menu-title">&nbsp;</span></a>
                                                                            <ul class="dropdown-mega level1">
                                                                                <li class=""><a class=""
                                                                                                href="{{URL::to('coffee-maker')}}"><span
                                                                                                class="menu-title">Coffee makers</span></a>
                                                                                </li>
                                                                                <li class=""><a class=""
                                                                                                href="{{URL::to('kettles-&-tea-makers')}}"><span
                                                                                                class="menu-title">Kettles &amp; Tea Makers</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('toasters')}}"><span
                                                                                                class="menu-title">Toasters</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('sandwich-makers')}}"><span
                                                                                                class="menu-title">Sandwich Makers</span></a>
                                                                                </li>
                                                                                <li class="parent dropdown-submenu  "><a
                                                                                            href="{{URL::to('bread-makers')}}"><span
                                                                                                class="menu-title">Bread Makers</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('air-fryer')}}"><span
                                                                                                class="menu-title">Airfryers</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('steam-cookers')}}"><span
                                                                                                class="menu-title">Steam Cookers</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('rice-cookers')}}"><span
                                                                                                class="menu-title">Rice Cooker</span></a>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="mega-col span4 col-2">
                                                                    <ul>
                                                                        <li class="parent dropdown-submenu mega-group">
                                                                            <a
                                                                                    class="dropdown-toggle"
                                                                                    data-toggle="dropdown"
                                                                                    href="#"><span
                                                                                        class="menu-title">&nbsp;</span></a>
                                                                            <ul class="dropdown-mega level1">

                                                                                <li class="  "><a
                                                                                            href="{{URL::to('glass-grill')}}"><span
                                                                                                class="menu-title">Glass Grills</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('oven-toaster-grillers')}}"><span
                                                                                                class="menu-title">Oven Toaster Griller</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('tandoors')}}"><span
                                                                                                class="menu-title">Tandoors</span></a>
                                                                                </li>
                                                                                <li class="parent dropdown-submenu  "><a
                                                                                            href="{{URL::to('induction-cookers')}}"><span
                                                                                                class="menu-title">Induction Cookers</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('OFR-heaters')}}"><span
                                                                                                class="menu-title">OFR Heaters</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('PTC-heaters')}}"><span
                                                                                                class="menu-title">PTC Heaters</span></a>
                                                                                </li>
                                                                                <li class="  "><a
                                                                                            href="{{URL::to('irons')}}"><span
                                                                                                class="menu-title">Irons</span></a>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="no_border"><a class="no_border"
                                                                             href="{{URL::to('retail-stores')}}"
                                                                             style="border: none!important;"><span
                                                                    class="menu-title">Retail Stores</span></a></li>
                                                    <li class="parent dropdown"><a class="dropdown-toggle"
                                                                                   data-toggle="dropdown"><span
                                                                    class="menu-title">Media</span><b class="caret"></b></a>

                                                        <div class="dropdown-menu menu-content mega-cols">
                                                            <div class="row-fluid">
                                                                <div class="mega-col span12 col-1">
                                                                    <ul>
                                                                        <li class="parent dropdown-submenu mega-group">
                                                                            <a class="dropdown-toggle"
                                                                               data-toggle="dropdown"
                                                                               href="{{URL::to('media-press-&-magazine-ads')}}">
                                                                                <span class="menu-title">Press & Magazine Ads</span><b
                                                                                        class="caret"></b>
                                                                            </a>
                                                                        </li>
                                                                        <li class="parent dropdown-submenu mega-group">
                                                                            <a class="dropdown-toggle"
                                                                               data-toggle="dropdown"
                                                                               href="{{URL::to('media-tv-ads')}}">
                                                                                <span class="menu-title">TV Ads</span><b
                                                                                        class="caret"></b>
                                                                            </a>
                                                                        </li>
                                                                        <li class="parent dropdown-submenu mega-group">
                                                                            <a class="dropdown-toggle"
                                                                               data-toggle="dropdown"
                                                                               href="{{URL::to('media-product-demos')}}">
                                                                                <span class="menu-title">Product Demos</span><b
                                                                                        class="caret"></b>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class=""><a href="{{URL::to('breakfast-made-easy')}}"><span
                                                                    class="menu-title">Offer Zone</span></a>
                                                    </li>
                                                    <li>
                                                        <?php $items = AppUtil::getTotalCartItems(); ?>

                                                        <a href="{{URL::to('/cart')}}" class="padding-right0">
        <span class="menu-icon" style="background:url({{URL::to('frontoffice/img/cart.png')}}) no-repeat;">
           <span class="menu-title" style="display:inline;">Cart</span> <span class="cart-badge badge"
                                                                              style="float:right;">{{$items or ''}}</span>
        </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </nav>
                        </div>
                        @if(!empty($searchProducts))
                            <div class="span1">
                                <div id="search_block_top" class="">
                                    <p class="search-btn" title="Search"><i class="icon-search"></i></p>

                                    <form method="get" action="#" id="searchbox" class="search">
                                        <p>
                                            <label for="search_query_top"><!-- image on background --></label>
                                            {{$searchProducts}}
                                            <input type="submit" name="submit_search" id="submit_search" value="Search"
                                                   class="button">
                                        </p>
                                    </form>
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- #EndLibraryItem --></div>
            </div>
        </div>
    </section>
</header>
<script type="text/javascript">
    $('document').ready(function () {
        $("#search_query_top").chosen();
        $('#search_query_top_chosen').css({'width': '276px', 'font-size': '12px'});
    });

    var content = $("#search_block_top .search");
    $(".search-btn").click(function () {
        $('#searchbox').toggleClass("eshow");
    });

    $("#submit_search").click(function (e) {
        e.preventDefault();

        var selectedProduct = $("#search_query_top option:selected");
        var product = selectedProduct.val();
        var selectedCategory = selectedProduct.closest('optgroup').attr('label');
        var cat = selectedCategory.split('>')[1].trim().toLowerCase();
        var category = cat.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');

        window.location.href = '<?php echo URL::to(''); ?>' + '/' + category + '/' + product;
    });

</script>

