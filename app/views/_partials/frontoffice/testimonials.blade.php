@if(!is_null($related_products))
    <div id="testimonials" class="block products_block exclusive blockleotestimonials">
        <h3 class="title_block">Why our <span class="tcolor">customers</span> love us</h3>

        <div class="block_content">
            <div class="carousel slide" id="leotestimonialscarousel">

                <div class="button-carousel">
                    <a class="carousel-control left" href="#leotestimonialscarousel" data-slide="prev">‹</a>
                    <a class="carousel-control right" href="#leotestimonialscarousel" data-slide="next">›</a>
                </div>

                <div class="carousel-inner">

                    @for ($i = 0; $i < 6; $i=$i+3)
                        @if($i==0)<?php $active = "active"; ?> @else <?php $active = ""; ?> @endif
                        <div class="item {{$active}}">
                            <div class="media">

                                <div class="media-body">
                                    " Glen Food Processor GL 4052 is good indeed. If we compare it with other brands it
                                    considered more reliable and value for money. Number of attachments is more compared
                                    to other company. It reduces time and effort."
                                    {{--<div class="pull-right"><strong> -- ABC </strong></div>--}}
                                </div>
                            </div>
                            <hr class="hr-line">
                            <div class="media">

                                <div class="media-body">
                                    "Mini Chopper is very nice and useful product ...... easy to clean and occupy less
                                    space for storage , ideal for nuclear family. .... Easy and quick chopping of
                                    vegetables for cooking food. I can blend and beat liquid in it, even can make tomato
                                    puree. Love this product, it made my cooking very simple, hassle free and fast ."
                                    {{--<div class="pull-right"><strong> -- XYZ</strong></div>--}}
                                </div>
                            </div>
                        </div>

                    @endfor
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.blockleotestimonials .carousel').each(function () {
                $(this).carousel({
                    pause: true,
                    interval: false
                });
            });

        });
    </script>
    <style type="text/css">
        .media {
            margin: 20px !important;
        }
    </style>
@endif