<?php
if(isset($combo[0]->id)) $combo_id = $combo[0]->id;
if(isset($combo->id)) $combo_id = $combo->id;
?>

<!--side tab starts here-->
<div class="col-lg-2">
    <ul class="nav nav-tabs tabs-left">
        <li class="@if(isset($active) && $active=='combo_info')active @endif">
            <a href="{{URL::to('dashboard/combo/edit/'.$combo_id)}}">Combo Info</a>
        </li>
        <li class="@if(isset($active) && $active=='combo_products')active @endif">
            <a href="{{URL::to('dashboard/combo-products/'.$combo_id)}}">Combo Products</a>
        </li>
        <li class="@if(isset($active) && $active=='images')active @endif">
            <a href="{{URL::to('dashboard/combo-images/'.$combo_id)}}">Images</a>
        </li>
    </ul>
</div>
<!--side tabs ends here-->
