<div class="col-lg-2">
    <ul class="nav nav-tabs tabs-left">
        <li class="@if(isset($active)&&$active=='Categories')active @endif">
            <a href="{{URL::to('dashboard/documentation/categories')}}">Categories</a>
        </li>
        <li class="@if(isset($active)&&$active=='Products')active @endif">
            <a href="{{URL::to('dashboard/documentation/products')}}">Products</a>
        </li>
        <li class="@if(isset($active)&&$active=='Tags')active @endif">
            <a href="{{URL::to('dashboard/documentation/tags')}}">Tags</a>
        </li>
        <li class="@if(isset($active)&&$active=='Combos')active @endif">
            <a href="{{URL::to('dashboard/documentation/combos')}}">Combos</a>
        </li>
        <li class="@if(isset($active)&&$active=='Orders')active @endif">
            <a href="{{URL::to('dashboard/documentation/orders')}}">Orders</a>
        </li>
        <li class="@if(isset($active)&&$active=='Customers')active @endif">
            <a href="{{URL::to('dashboard/documentation/customers')}}">Customers</a>
        </li>
        <li class="@if(isset($active)&&$active=='Dealers')active @endif">
            <a href="{{URL::to('dashboard/documentation/dealers')}}">Dealers</a>
        </li>
        <li class="@if(isset($active)&&$active=='Service Centers')active @endif">
            <a href="{{URL::to('dashboard/documentation/service-centers')}}">Service Centers</a>
        </li>
        <li class="@if(isset($active)&&$active=='Demos')active @endif">
            <a href="{{URL::to('dashboard/documentation/demos')}}">Demos</a>
        </li>
        <li class="@if(isset($active)&&$active=='Coupons')active @endif">
            <a href="{{URL::to('dashboard/documentation/coupons')}}">Coupons</a>
        </li>
        <li class="@if(isset($active)&&$active=='Messages')active @endif">
            <a href="{{URL::to('dashboard/documentation/messages')}}">Messages</a>
        </li>

    </ul>
</div>
