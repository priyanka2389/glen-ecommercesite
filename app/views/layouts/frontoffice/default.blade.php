<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<head>
    <title>@if(!empty($meta_title)){{$meta_title}}@else Glen India @endif</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <meta name="keywords" content="{{$meta_keywords or ''}}">
    <meta name="description" content="{{$meta_description or ''}}">
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('frontoffice/img/favicon.ico')}}"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('frontoffice/img/favicon.ico')}}"/>

    {{HTML::style('frontoffice/themes/leometr/css/bootstrap.css')}}

    <!--    cache themes-->
    {{HTML::style('frontoffice/themes/leometr/cache/1b115dab49c4ab0941c2178bac6d2921_all.css')}}
    {{HTML::style('frontoffice/themes/leometr/cache/b2f75094dbbc0a4ad436345203310867_all.css')}}


    {{HTML::style('frontoffice/css/index-menu.css')}}

    {{HTML::style('frontoffice/css/rupee.css')}}
    {{HTML::style('frontoffice/css/custom.css')}}
    {{HTML::style('frontoffice/css/custom1.css')}}
    {{HTML::style('frontoffice/css/custom-index.css')}}

    {{HTML::style('frontoffice/css/compare.css')}}
    {{HTML::style('frontoffice/themes/leometr/css/bootstrap-responsive.css')}}
    {{HTML::style('frontoffice/themes/leometr/css/theme-responsive.css')}}
    <link rel="stylesheet" href="//cdn.jsdelivr.net/qtip2/2.2.0/basic/jquery.qtip.css"/>


    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>


    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'/>
    <!--[if IE 8]>
    <link href="http://demo4leotheme.com/prestashop/leo_metro/themes/leometr/css/ie8.css" rel="stylesheet"
          type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--    facebook meta tags starts here-->
    {{$facebook_meta_tags or ''}}

    <!--fancybox files starts here-->

    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_overlay.png">
    <link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite.png">
    <link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite@2x.png">
    <!--fancybox files ends here-->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>
    <link rel="stylesheet" href="http://harvesthq.github.io/chosen/chosen.css"/>

    {{HTML::script('frontoffice/js/get_products.js')}}
    {{HTML::script('frontoffice/js/compare.js')}}

    <style type="text/css">
        .morecontent span {
            display: none;
        }

        .morelink {
            display: block;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var showChar = 150; // How many characters are shown by default
            var ellipsestext = "...";
            var moretext = "Show more >";
            var lesstext = "< Show less";

            $('.feature-description').each(function () {

                var content = $(this).html();
                if (content.length > showChar) {

                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar, content.length - showChar);

                    var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';

                    $(this).html(html);
                }
            });

            $(".morelink").click(function () {
                if ($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle(1000);
                $(this).prev().toggle(1000);
                return false;
            });
        });

    </script>
    <script type="text/javascript"
            src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
</head>

<body id="index" class="fs12 {{$body_class or ''}}">
<div id="page" class="clearfix">

    @include('_partials.frontoffice.nav_menu')

    <div id="gray_strip"></div>
    @yield('content')

    @include('_partials.frontoffice.footer')

    <!--fb slide partial view -->
    @include('_partials.frontoffice.fb_slide_box')

    <!--feedback slide box partial-->
    @include('_partials.frontoffice.feedback_slide_box')


</div>

{{HTML::script('frontoffice/js/purl.js')}}
{{HTML::script('frontoffice/js/jquery/jquery-migrate-1.2.1.js')}}
{{HTML::script('frontoffice/js/jquery/plugins/jquery.easing.js')}}
{{HTML::script('frontoffice/js/feedback.js')}}
{{HTML::style('frontoffice/css/feedback.css')}}
{{HTML::script('frontoffice/js/tab-slideout.js')}}

{{HTML::script('frontoffice/js/tools.js')}}
<script type="text/javascript" src="//cdn.jsdelivr.net/qtip2/2.2.0/basic/jquery.qtip.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.scrollup/1.1/js/jquery.scrollUp.min.js"></script>


{{HTML::script('frontoffice/modules/carriercompare/carriercompare.js')}}
{{HTML::script('frontoffice/themes/leometr/js/tools/treeManagement.js')}}
{{HTML::script('frontoffice/js/jquery/plugins/autocomplete/jquery.autocomplete.js')}}
{{HTML::script('frontoffice/modules/favoriteproducts/favoriteproducts.js')}}
{{HTML::script('frontoffice/modules/lofminigallery/assets/jquery.prettyPhoto.js')}}
{{HTML::script('frontoffice/modules/blockwishlist/js/ajax-wishlist.js')}}
{{HTML::script('frontoffice/modules/leotempcp/bootstrap/js/bootstrap.js')}}
{{HTML::script('frontoffice/themes/leometr/info/assets/form.js')}}
{{HTML::script('frontoffice/modules/leocamera/js/camera.js')}}
{{HTML::script('frontoffice/themes/leometr/js/custom.js')}}


<div id="fb-root"></div>
<script type="text/javascript">(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    //code for scroll to top
    $(function () {
        $.scrollUp({
            scrollName: 'scrollUp', // Element ID
            topDistance: '300', // Distance from top before showing element (px)
            topSpeed: 300, // Speed back to top (ms)
            animation: 'fade', // Fade, slide, none
            animationInSpeed: 200, // Animation in speed (ms)
            animationOutSpeed: 200, // Animation out speed (ms)
            scrollText: 'Scroll to top', // Text for element
            activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
        });
    });

    var base_url = '<?php echo URL::to(''); ?>';

    $(document).scroll(function () {
        var scrollHeight = 861;
        if ($(this).scrollTop() > scrollHeight) {
//            if($('.floating-menu').position().top + $('.floating-menu').outerHeight() > $('#content_container').height()){
//                var bottom =$('#content_container').position().top + $('#content_container').outerHeight();
//                $('.floating-menu').css('top',$('#content_container').position().top);
//            }
            $('.floating-menu').fadeIn('slow');
        } else {
            $('.floating-menu').fadeOut('slow');

        }
    });
</script>
@yield('page_script')

</body>


</html>
