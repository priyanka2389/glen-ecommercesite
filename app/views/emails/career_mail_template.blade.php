<?php
/**
 * Created by PhpStorm.
 * User: priyanka
 * Date: 7/16/14
 * Time: 10:25 AM
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" >
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Message from {shop_name}</title>
</head>
<body>
<table style="font-family: Verdana,sans-serif; font-size: 11px; color: #374953; width: 550px;">
    <tbody>
    <tr>
        <td align="left">Dear<strong style="color: {color};">&nbsp;{{$name}}</strong>,</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Your resume has been successfully submitted. We will get back to you shortly.</td>
    </tr>
<!--    <tr>-->
<!--        <td align="left">Customer Name: <strong>{name}</strong> <br/><br/>-->
<!--        </td>-->
<!--    </tr>-->
<!--    <tr>-->
<!--        <td align="left">Company Name: <strong>{companyName}</strong> <br/><br/>-->
<!--        </td>-->
<!--    </tr>-->
<!--    <tr>-->
<!--        <td align="left">Customer e-mail address: <a href="mailto:{email}"><strong>{email}</strong></a><br/><br/>-->
<!--        </td>-->
<!--    </tr>-->
<!--    <tr>-->
<!--        <td align="left">Customer Mobile: <strong>{mobile}</strong> <br/><br/>-->
<!--        </td>-->
<!--    </tr>-->
<!--    <tr>-->
<!--        <td align="left">Country: <strong>{country}</strong> <br/><br/>-->
<!--        </td>-->
<!--    </tr>-->
<!--    <tr>-->
<!--        <td align="left">PinCode: <strong>{pincode}</strong> <br/><br/>-->
<!--        </td>-->
<!--    </tr>-->
<!--    <tr>-->
<!--        <td align="left">State: <strong>{state}</strong> <br/><br/>-->
<!--        </td>-->
<!--    </tr>-->
<!--    <tr>-->
<!--        <td align="left">City: <strong>{city}</strong> <br/><br/>-->
<!--        </td>-->
<!--    </tr>-->
<!--    <tr>-->
<!--        <td align="left">Category: <strong>{category}</strong> <br/><br/>-->
<!--        </td>-->
<!--    </tr>-->
<!--    <tr>-->
<!--        <td align="left">Product Name: <strong>{product}</strong> <br/><br/>-->
<!--        </td>-->
<!--    </tr>-->
<!--    <tr>-->
<!--        <td align="left">Product Quantity: <strong>{quantity}</strong> <br/><br/>-->
<!--        </td>-->
<!--    </tr>-->
<!--    <tr>-->
<!--        <td>&nbsp;</td>-->
<!--    </tr>-->
<!---->
<!--    <tr>-->
<!--        <td>&nbsp;</td>-->
<!--    </tr>-->
<!---->
<!--    <tr>-->
<!--        <td>For any queries,please  call 9266655555 </td>-->
<!--    </tr>-->
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Thanks</td>
    </tr>
    <tr>
        <td>Glen Appliances Pvt. Ltd.<br>
            <a style="color: #17479d; font-weight: bold; text-decoration: none;" href="www.glenindia.com">www.glenindia.com</a>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>