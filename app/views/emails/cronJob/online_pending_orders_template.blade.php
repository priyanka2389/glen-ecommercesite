<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<table style="width: 100%; font-family: Verdana,sans-serif; font-size: 12px; color: #374953;">
    <tr>
        <td>
            Dear Administrator,<br><br> Below is the list of today's online orders where payment is pending :
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>

            <table style="width: 100%; font-family: Verdana,sans-serif; font-size: 12px; color: #374953;">
                <!-- Title -->
                <tbody>
                <tr style="background-color:#17479d;color:#374953;width:900px">
                    <td colspan="9" style="font-weight:bold;color:white;line-height: 24px;padding:0 0 0 6px;"> Order
                        List :
                    </td>
                </tr>

                <tr style="background-color:#b9babe;text-align:center">
                    <th style="width:6%;padding:0.6em 0">Order No</th>
                    <th style="width:11%;padding:0.6em 0">Reference Id</th>
                    <th style="width: 18%;padding:0.6em 0">User Name</th>
                    <th style="width: 13%;padding:0.6em 0">Mobile</th>
                    <th style="width: 10%;padding:0.6em 0">Final Value</th>
                    <th style="width: 11%;padding:0.6em 0">Order Status</th>
                    <th style="width:10%;padding:0.6em 0">Payment Status</th>
                    <th style="width: 12%;padding:0.6em 0">Payment Mode</th>
                    <th style="width: 9%;padding:0.6em 0">Created At</th>
                </tr>

                @for ($i=0;$i<count($orderData);$i++)
                    <tr style="background-color:#ebecee;line-height:24px;">
                        <td style="width: 6%; padding: 0.6em 0 0 6px;">{{ $orderData[$i]['order_no'] }}</td>
                        <td style="width: 11%; padding: 0.6em 0 0 6px;">{{ $orderData[$i]['reference_id'] }}</td>
                        <td style="width: 18%; padding: 0.6em 0 0 6px;">{{ $orderData[$i]['user_name'] }}</td>
                        <td style="width: 13%; padding: 0.6em 0 0 6px;">{{ $orderData[$i]['mobile'] }}</td>
                        <td style="width: 10%; padding: 0.6em 0 0 6px;">{{ $orderData[$i]['final_value'] }}</td>
                        <td style="width: 11%; padding: 0.6em 0 0 6px;">{{ $orderData[$i]['status'] }}</td>
                        <td style="width: 10%; padding: 0.6em 0 0 6px;">{{ $orderData[$i]['payment_status'] }}</td>
                        <td style="width: 12%; padding: 0.6em 0 0 6px;">{{ $orderData[$i]['payment_mode'] }}</td>
                        <td style="width: 9%; padding: 0.6em 0 0 6px;">{{ $orderData[$i]['created_on'] }}</td>
                    </tr>
                @endfor


                </tbody>
            </table>
        </td>
    </tr>

    <tr>
        <td>&nbsp;</td>
    </tr>

    <tr style="font-size: 11px;">
        <td>For more details please visit this link <a
                    href="{{$page_url}}">{{$page_url}}</a>
        </td>
    </tr>

    {{--</table>--}}
    </td>
    </tr>


</table>
</body>
</html>