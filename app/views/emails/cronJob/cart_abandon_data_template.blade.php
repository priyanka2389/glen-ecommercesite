<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<table style="width: 100%; font-family: Verdana,sans-serif; font-size: 12px; color: #374953;">
    <tr>
        <td>
            Dear Administrator,<br><br> Below is the list of new abandoned orders at Glen website :
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>

            <table style="width: 100%; font-family: Verdana,sans-serif; font-size: 12px; color: #374953;">
                <!-- Title -->
                <tbody>
                <tr style="background-color:#17479d;color:#374953;width:900px">
                    <td colspan="6" style="font-weight:bold;color:white;line-height: 24px;padding:0 0 0 6px;"> Abandoned
                        Cart
                        Summary :
                    </td>
                </tr>

                <tr style="background-color: #b9babe; text-align: center;">
                    <th style="width:6%; padding: 0.6em 0;">Sr. No</th>
                    <th style="width:15%; padding: 0.6em 0;">Name</th>
                    <th style="width:25%; padding: 0.6em 0;">Email</th>
                    <th style="width:15%; padding: 0.6em 0;">Mobile</th>
                    <th style="width:24%; padding: 0.6em 0;">Cart Item</th>
                    <th style="width:15%; padding: 0.6em 0;">Created At</th>
                </tr>

                @for ($i=0;$i<count($cartData);$i++)
                    <tr style="background-color:#ebecee;line-height:24px;">
                        <td style="width: 6%; padding: 0.6em 0 0 6px;">{{$i+1}}</td>
                        <td style="width: 15%; padding: 0.6em 0 0 6px;">{{ $cartData[$i]['user_name'] }}</td>
                        <td style="width: 25%; padding: 0.6em 0 0 6px;">{{ $cartData[$i]['email'] }}</td>
                        <td style="width: 15%; padding: 0.6em 0 0 6px;">{{ $cartData[$i]['mobile'] }}</td>
                        <td style="width: 24%; padding: 0.6em 0 0 6px;">{{ $cartData[$i]['name'] }}</td>
                        <td style="width: 15%; padding: 0.6em 0 0 6px;">{{ $cartData[$i]['created_at'] }}</td>
                    </tr>
                @endfor


                </tbody>
            </table>
        </td>
    </tr>

    <tr>
        <td>&nbsp;</td>
    </tr>

    <tr style="font-size: 11px;">
        <td>For more details please visit this link  <a
                    href="{{$page_url}}">{{$page_url}}</a>
        </td>
    </tr>

    {{--</table>--}}
    </td>
    </tr>


</table>
</body>
</html>