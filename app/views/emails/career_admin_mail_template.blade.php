<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Message from {shop_name}</title>
</head>
<body>
<table style="font-family: Verdana,sans-serif; font-size: 11px; color: #374953; width: 550px;">
    <tbody>
    <tr>
        <td align="left">Dear<strong style="color: {color};">&nbsp;Administrator</strong>,</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            You have received a resume from following person<br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Customer Name: <strong>{{$name}}</strong> <br/><br/>
        </td>
    </tr>
     <tr>
        <td align="left">Customer e-mail address: <strong>{{$email}}</strong><br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Customer Mobile: <strong>{{$mobile}}</strong> <br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Address: <strong>{{$address}}</strong> <br/><br/>
        </td>
    </tr>
     <tr>
        <td align="left">Applying Department: <strong>{{$applying_department}}</strong> <br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Educational Qualification: <strong>{{$educational_qualification}}</strong> <br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Professional Qualification: <strong>{{$professional_qualification}}</strong> <br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Primary Skill: <strong>{{$primary_skill}}</strong> <br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Career Highlights: <strong>{{$career_highlights}}</strong> <br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Work Experience: <strong>{{$work_Exp}}</strong> <br/><br/>
        </td>
    </tr>

    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Glen India<br>
            <a style="color: #17479d; font-weight: bold; text-decoration: none;" href="www.glenindia.com">www.glenindia.com</a>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>