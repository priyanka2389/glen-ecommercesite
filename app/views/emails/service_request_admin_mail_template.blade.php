<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Message from {shop_name}</title>
</head>
<body>
<table style="font-family: Verdana,sans-serif; font-size: 11px; color: #374953; width: 550px;">
    <tbody>
    <tr>
        <td align="left">Dear<strong style="color: {color};">&nbsp;Administrator</strong>,</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            You received a service complaint from the following customer.<br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Customer Name: <strong>{{$name}}</strong> <br/><br/>
        </td>
    </tr>

    <tr>
        <td align="left">Customer e-mail address: <strong>{{$email}}</strong><br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Customer Mobile: <strong>{{$mobile}}</strong> <br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Address: <strong>{{$address}}</strong> <br/><br/>
        </td>
    </tr>
     <tr>
        <td align="left">Product: <strong>{{$product}}</strong> <br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Bill_no: <strong>{{$bill_no}}</strong> <br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Bill Date: <strong>{{$bill_date}}</strong> <br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Shop Name: <strong>{{$shop_name}}</strong> <br/><br/>
        </td>
    </tr>
    <tr>
        <td align="left">Message: <strong>{{$comment}}</strong> <br/><br/>
        </td>
    </tr>

    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Glen India<br>
            <a style="color: #17479d; font-weight: bold; text-decoration: none;" href="www.glenindia.com">www.glenindia.com</a>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>