<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<!--<h2>Order confirmation</h2>-->

<table style="width: 100%; font-family: Verdana,sans-serif; font-size: 12px; color: #374953;">
    <tr>
        <td>
            Dear {{$customer_name}},<br><br> Thanks for your order !
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            You can track your order status by clicking on the link  <a style="color:#17479d;" href="{{$tracking_url}}">{{$tracking_url}}</a>
        </td>
    </tr>
</table>
</body>
</html>