<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<table style="width: 100%; font-family: Verdana,sans-serif; font-size: 12px; color: #374953;">
    <tr>
        <td>
            Dear Customer,<br><br> Congratulation your order has been successfully submitted . your order detail are as
            follows:
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table style="font-family:Verdana,sans-serif;font-size:12px;color:#374953;width:550px">
                <tr style="background-color:#17479d;">
                    <td colspan="2" style="font-weight:bold;color:white;line-height: 24px;padding:0 0 0 6px;">Order Summary : </td>
                </tr>
                {{--<tr style="background-color:#ddd">--}}
                    {{--<td colspan="2" style="font-weight:bold;line-height: 24px;padding:0 0 0 6px;">Shipping Details : </td>--}}
                {{--</tr>--}}

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td style="font-weight:bold">Order #:</td>
                    <td>{{$order_id}}</td>
                </tr>

                <tr>
                    <td style="font-weight:bold">Order Date:</td>
                    <td>{{$order_date}}</td>
                </tr>

                <tr>
                    <td style="font-weight:bold">Shipping Method:</td>
                    <td>{{strtoupper($payment_method)}}</td>
                </tr>

                <tr>
                    <td style="font-weight:bold">Products Subtotal:</td>
                    <td>Rs.{{number_format($final_value)}}</td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td align="left" colspan="2">
                        <table style="width: 100%; font-family: Verdana,sans-serif; font-size: 12px; color: #374953;">
                            <!-- Title -->
                            <tbody>
                            <tr style="background-color: #b9babe; text-align: center;">
                                <th style="width: 30%; padding: 0.6em 0;">Product</th>
                                <th style="width: 20%; padding: 0.6em 0;">Unit price</th>
                                <th style="width: 15%; padding: 0.6em 0;">Quantity</th>
                                <th style="width: 20%; padding: 0.6em 0;">Total price</th>
                            </tr>

                            @for ($i=0;$i<count($order_items);$i++)
                            <tr style="background-color:#ebecee;line-height:24px;">
                                <td style="width: 30%; padding: 0.6em 0 0 6px;">{{ $order_items[$i]['name'] }}</td>
                                <td style="width: 20%; padding: 0.6em 0 0 6px;">{{ $order_items[$i]['price'] }}</td>
                                <td style="width: 15%; padding: 0.6em 0 0 6px;">{{ $order_items[$i]['qty'] }}</td>
                                <td style="width: 20%; padding: 0.6em 0 0 6px;">{{ $order_items[$i]['subtotal'] }}</td>
                            </tr>
                            @endfor

                            @if($net_value != $final_value)
                            <tr style="font-weight: bold;">
                                <td style="background-color:#ddd; padding: 0.6em 0.4em;text-align:right;" colspan="3">
                                    Discount
                                </td>
                                <td style="background-color:#ddd;padding: 0.6em 0.4em;">
                                    - {{number_format($net_value - $final_value)}}
                                </td>
                            </tr>
                            @endif

                            <tr style="font-weight: bold;">
                                <td style="background-color: #17479d;color:white; padding: 0.6em 0.4em;text-align:right;" colspan="3">
                                    Total Invoice Value
                                </td>
                                <td style="background-color: #17479d;color:white; padding: 0.6em 0.4em;">
                                    {{number_format($final_value)}}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="font-size: 11px;" >
            <a style="color:#17479d; font-weight: bold; text-decoration: none;" href="{shop_url}">Glenindia.com</a>
        </td>
    </tr>
    <tr style="font-size: 11px;" >
        <td><strong>Toll free :</strong> 18001801222</td>
    </tr>
    <tr style="font-size: 11px;">
        <td><strong>Timings : </strong>9.30 a.m to 6.00 p.ma(Sunday closed)</td>
    </tr>

</table>
</body>
</html>