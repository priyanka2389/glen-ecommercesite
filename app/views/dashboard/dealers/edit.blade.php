@extends('layouts.dashboard_default')

@section('content')

<script type="text/javascript">
    $(document).ready(function () {

        $('#form').validate();
        $('#sequence').chosen();
    });
</script>


<!-- page start-->
<div class="row">
    <div class="col-lg-10">
        <!--breadcrumbs start -->
        <ul class="breadcrumb edit">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/dealers')}}">Dealers</a></li>
            <li>Edit</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
    <div class="col-lg-2">
        <a href="{{URL::to('dashboard/dealers')}}" class="btn btn-primary btn-sm"><i
                class="fa fa-arrow-circle-left"></i> Back</a>
    </div>

</div>


<div class="row">

    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <h4>Edit Dealer</h4>
            </div>
            <div class="panel-body">
                <div class="form">
                    <form action="{{URL::to('dashboard/dealers/edit/'.$dealer->id)}}" class="cmxform form-horizontal"
                          id="form" method="post">
                        <div class="form-group ">
                            <label for="name" class="control-label col-lg-3">Shop Name</label>

                            <div class="col-lg-6">
                                <input name="name" type="text" class="form-control" id="name"
                                       value="{{$dealer->name or ''}}"/>
                                <span class="error">{{$errors->first('name')}}</span>
                            </div>

                        </div>

                        <div class="form-group ">
                            <label for="name" class="control-label col-lg-3">Email Id *</label>

                            <div class="col-lg-6">
                                <input name="email" type="text" class="form-control" id="email"
                                       value="{{$dealer->email_id or ''}}"/>
                                <span class="error">{{$errors->first('email')}}</span>
                            </div>

                        </div>


                        <div class="form-group">
                            <label for="address1" class="control-label col-lg-3">Address *</label>

                            <div class="col-lg-6">
                                <textarea name="address1" class="description form-control" id="address1"
                                          rows="3">{{$dealer->address1 or ''}}</textarea>
                                <span class="error">{{$errors->first('address1')}}</span>
                            </div>
                        </div>

                        <!--                        <div class="form-group">-->
                        <!--                            <label for="address2" class="control-label col-lg-3">Address 2</label>-->
                        <!---->
                        <!--                            <div class="col-lg-6">-->
                        <!--                                <textarea name="address2" class="description form-control" id="address2"-->
                        <!--                                          rows="3">{{$dealer->address2}}</textarea>-->
                        <!--                                <span class="error">{{$errors->first('address2')}}</span>-->
                        <!--                            </div>-->
                        <!--                        </div>-->

                        <div class="form-group">
                            <label for="city" class="control-label col-lg-3">City *</label>

                            <div class="col-lg-6">
                                <input name="city" type="text" class="required form-control" id="city"
                                       value="{{$dealer->city or ''}}"/>
                                <span class="error">{{$errors->first('city')}}</span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="state" class="control-label col-lg-3">State *</label>

                            <div class="col-lg-6">
                                <input name="state" type="text" class="required form-control" id="state"
                                       value="{{$dealer->state or ''}}"/>
                                <span class="error">{{$errors->first('state')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pincode" class="control-label col-lg-3">Pincode *</label>

                            <div class="col-lg-6">
                                <input name="pincode" type="text" class="form-control" id="pincode"
                                       value="{{$dealer->pincode or ''}}"/>
                                <span class="error">{{$errors->first('pincode')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mobile" class="control-label col-lg-3">Mobile </label>

                            <div class="col-lg-6">
                                <input name="mobile" type="text" class="form-control" id="mobile"
                                       value="{{$dealer->mobile or ''}}"/>
                                <span class="error">{{$errors->first('mobile')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="control-label col-lg-3">Phone</label>

                            <div class="col-lg-6">
                                <input name="phone" type="text" class="form-control" id="phone"
                                       value="{{$dealer->phone or ''}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="control-label col-lg-3">Contact Person</label>

                            <div class="col-lg-6">
                                <input name="contact_person" type="text" class="form-control" id="contact_person"
                                       value="{{$dealer->contact_person or ''}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="control-label col-lg-3">Sequence *</label>

                            <div class="col-lg-1">
                                <div class="radio">
                                    <input name="sequence" type="radio" class="required" value="top"
                                    @if(isset($sequence)&&$sequence=='top') checked @endif />At top
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="radio">
                                    <input type="radio" class="required" name="sequence" value="bottom"
                                    @if(isset($sequence)&&$sequence=='bottom') checked @endif/>At End
                                </div>
                            </div>

                            @if(!is_null($dealers))
                            <div class="col-lg-1">
                                <div class="radio">
                                    <input type="radio" class="required" name="sequence" value="after"
                                    @if(isset($sequence)&& is_int($sequence)) checked @endif/>After
                                </div>
                            </div>

                            <div class="col-lg-3">

                                <select name="after" id="sequence" class="form-control input-sm m-bot15" data-placeholder="Select Dealer">
                                    <option value=""></option>

                                    @foreach($dealers as $row)
                                    <option value="{{$row->sequence}}" @if(isset($sequence) && is_int($sequence) && ($sequence==$row->id) )
                                    selected
                                    @endif
                                    >{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif

                            <!--                            <span class="error">{{$errors->first('sequence')}}</span>-->
                        </div>

                        <div class="form-group">
                            <label for="small_appliance" class="control-label col-lg-3">Is Small Appliance ?</label>

                            <div class="col-lg-3">
                                <div class="checkbox">
                                    <input name="small_appliance" type="checkbox" id="small_appliance"
                                    @if($dealer->is_small_appliance==1) checked @endif
                                    />Yes
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="large_appliance" class="control-label col-lg-3">Is Large Appliance ? </label>

                            <div class="col-lg-3">
                                <div class="checkbox">
                                    <input name="large_appliance" type="checkbox" id="large_appliance"
                                    @if($dealer->is_large_appliance==1) checked @endif
                                    />Yes
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="" class="control-label col-lg-3">Type *</label>

                            <?php $type = $dealer->type; ?>
                            <div class="col-lg-1">
                                <div class="radio">
                                    <input type="radio"  name="type"  class="required" value="dealer"
                                    @if(isset($type)&& $type=='dealer') checked @endif  />Dealer
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="radio">
                                    <input type="radio" name="type" class="required"  value="retail_store"
                                    @if(isset($type)&& $type=='retail_store') checked @endif />Exclusive Gallery
                                </div>
                            </div>
                            <span class="error">{{$errors->first('type')}}</span>
                        </div>

                        <div class="form-group">
                            <label for="active" class="control-label col-lg-3">Is Active ? </label>

                            <div class="col-lg-3">
                                <div class="checkbox">
                                    <input name="active" type="checkbox" id="active"
                                    @if($dealer->is_active==true) checked @endif
                                    />Yes
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


@stop