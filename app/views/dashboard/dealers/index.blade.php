@extends('layouts.dashboard_default')

@section('content')

<script type="text/javascript">
    $(document).ready(function () {
        $('.upload_form').hide();
        $('.delete').click(function (e) {

            e.preventDefault();
            var url = $(this).attr("href");
            var delete_dealer = confirm("Are you sure you want to delete this dealer?");
            if (delete_dealer == true) {
                window.location.href = url;
            } else {
                return false;
            }
        });

        $('#upload_csv').click(function (e) {
            e.preventDefault();
            $('.upload_form').toggle();

            {{--$('.update_data').click(function(f){--}}
                {{--f.preventDefault();--}}
{{--//                $("#form").validate({--}}
{{--//                    rules: {--}}
{{--//                        file: {"required": true, extension: "csv" }--}}
{{--//                    }--}}
{{--//                });--}}
                {{--var form = $('#form');--}}
                {{--if(form.validate()){--}}
                 {{--$('.update_data').html("<i class=\"fa fa-spinner fa-spin\"></i>Uploading data..");--}}
{{--//                    var form = $('#form');--}}
                    {{--var url = "{{URL::to('dashboard/dealers/upload-csv')}}";--}}
                    {{--console.log(url);--}}
                    {{--$.post(url, form.serialize(), function (data) {--}}
                        {{--if (data.success == "true") {--}}
                            {{--$('.update_data').html("Update sucessfull");--}}
                            {{--window.location.href = "{{URL::to('dashboard/order')}}";--}}
                        {{--} else {--}}
                            {{--alert("Some error occurred please try again later");--}}
                            {{--$('.update_data').html("Save changes");--}}
                        {{--}--}}
                    {{--});--}}
                {{--}--}}

            {{--});--}}
        });

        $('#download_csv').click(function(e){

            e.preventDefault();
            var download_csv_url = "{{URL::to('dashboard/dealers/download-csv')}}";
            console.log(download_csv_url);
            window.location.href = download_csv_url;
        });

    });
</script>
<style type="text/css">
.notes{color:red;font-size:14px;margin-bottom:10px;}
</style>

<div class="row">
    <div class="col-lg-12"></div>
    {{ Notification::showSuccess() }}
</div>

<!-- page start-->
<div class="row">
    <div class="col-lg-8">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li>Dealers</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
    <div class="col-lg-3">
        <a href="{{URL::to('dashboard/dealers/create')}}" class="btn btn-primary btn-sm">
            <i class="fa fa-plus-circle"></i> Add Dealer</a>
         <a href="" class="btn btn-info btn-sm" id="upload_csv"><i class="fa fa-upload"></i> Upload Csv</a>
         <a href="" class="btn btn-info btn-sm" id="download_csv"><i class="fa fa-download"></i> Download Csv</a>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 upload_form">
       <div class="col-sm-3"></div>
       <div class="col-sm-6">
       <div class="panel">
           <div class="panel-heading">
               Upload CSV
           </div>
           <div class="panel-body">
                <div class="row notes col-lg-10">
                    <strong>Note:</strong>Upload new dealers list to overwrite the current one.
                </div>
                 <form action="{{URL::to('dashboard/dealers/upload-csv')}}" class="form-horizontal" role="form"  method="post" id="form" enctype="multipart/form-data">

                      <div class="form-group">
                          <label for="active" class="control-label col-lg-3">Upload CSV File*</label>

                          <div class="col-lg-6">
                              <input type="file" name="file" id="file" class="required"/>
                          </div>
                      </div>

                     <div class="form-group">
                           <div class="col-lg-offset-3 col-lg-6">
                                <button type="submit" class="btn btn-primary update_data">Update data</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                           </div>
                     </div>
                 </form>
           </div>
       </div>
       </div>

    </div>
    <!--    user info starts here-->
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Dealers
            </header>
            <div class="panel-body table-responsive">
                <table class="table table-hover general-table">
                    <thead>
                    <tr>

                        <th>Shop Name</th>
                        <th>Address 1</th>
                        <!--                        <th>Address 2</th>-->
                        <th>City</th>
                        <th>State</th>
                        <th>Pincode</th>
                        <th>Mobile</th>
                        <th>Phone</th>
                        <th>Sequence</th>
                        <th>Small Appliance</th>
                        <th>Large Appliance</th>
                        <th>Active</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>

                    @if(!is_null($dealers))

                    @foreach($dealers as $i=>$dealer)

                    <?php $id = $dealer->id; ?>

                    <tr>

                        <td>{{$dealer->name}}</td>
                        <td>{{$dealer->address1 or '-'}}</td>
                        <!--                        <td>-->
                        <!--                            @if(!isset($dealer->address2)) {{$dealer->address2}} @else - @endif-->
                        <!--                        </td>-->
                        <td>{{$dealer->city}}</td>
                        <td>{{$dealer->state }}</td>
                        <td>{{$dealer->pincode}}</td>
                        <td>{{$dealer->mobile}}</td>
                        <td>@if(isset($dealer->phone)) {{$dealer->phone}} @else - @endif</td>
                        <td>{{$dealer->sequence}}</td>
                        <td>
                            @if($dealer->is_small_appliance)
                            <a href="{{URL::to('dashboard/dealers/update-appliance-type/'.$id.'/small/0')}}">
                                <i class="fa fa-check green"></i>
                            </a>
                            @else
                            <a href="{{URL::to('dashboard/dealers/update-appliance-type/'.$id.'/small/1')}}">
                                <i class="fa fa-times red"></i>
                            </a>
                            @endif
                        </td>
                        <td>
                            @if($dealer->is_large_appliance)
                            <a href="{{URL::to('dashboard/dealers/update-appliance-type/'.$id.'/large/0')}}">
                                <i class="fa fa-check green"></i>
                            </a>
                            @else
                            <a href="{{URL::to('dashboard/dealers/update-appliance-type/'.$id.'/large/1')}}">
                                <i class="fa fa-times red"></i>
                            </a>
                            @endif
                        </td>
                        <td>
                            @if($dealer->is_active)
                            <a href="{{URL::to('dashboard/dealers/activate-or-deactivate/'.$id.'/0')}}">
                                <i class="fa fa-check green"></i>
                            </a>
                            @else
                            <a href="{{URL::to('dashboard/dealers/activate-or-deactivate/'.$id.'/1')}}">
                                <i class="fa fa-times red"></i>
                            </a>
                            @endif
                        </td>
                        <td><?php echo AppUtil::getParsedDate($dealer->created_at); ?></td>
                        <td>
                            <a href="{{URL::to('dashboard/dealers/edit/'.$id)}}"
                               class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
                            <a href="{{URL::to('dashboard/dealers/delete/'.$id)}}"
                               class="btn btn-xs btn-danger delete"><i class="fa fa-trash-o"></i></a>
                        </td>
                        <td>

                        </td>
                    </tr>

                    @endforeach

                    @endif

                    </tbody>

                </table>
            </div>
        </section>

    </div>
    <!--user info ends here-->

    @if(isset($dealers))
    <div class="text-center">
        <?php echo $dealers->links(); ?>
    </div>
    @endif

</div>


@stop