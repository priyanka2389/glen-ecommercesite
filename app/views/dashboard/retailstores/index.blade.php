@extends('layouts.dashboard_default')

@section('content')

<script type="text/javascript">
    $(document).ready(function () {

        $('.delete').click(function (e) {

            e.preventDefault();
            var url = $(this).attr("href");
            var delete_dealer = confirm("Are you sure you want to delete this Retail store?");
            if (delete_dealer == true) {
                window.location.href = url;
            } else {
                return false;
            }
        });

    });
</script>

<div class="row">
    <div class="col-lg-12"></div>
    {{ Notification::showSuccess() }}
</div>

<!-- page start-->
<div class="row">
    <div class="col-lg-10">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li>Retail stores</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
    <div class="col-lg-2">
        <a href="{{URL::to('dashboard/retail-stores/create')}}" class="btn btn-primary btn-sm">
            <i class="fa fa-plus-circle"></i> Add Retail store</a>
    </div>
</div>

<div class="row">

    <!--    user info starts here-->
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                RetailStores
            </header>
            <div class="panel-body table-responsive">
                <table class="table table-hover general-table">
                    <thead>
                    <tr>

                        {{--<th>Shop Name</th>--}}
                        <th>Address </th>
                        <!--                        <th>Address 2</th>-->
                        <th>City</th>
                        <th>State</th>
                        <th>Pincode</th>
                        <th>Mobile</th>
                        <th>Phone</th>
                        <th>Sequence</th>
                        <th>Active</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>

                    @if(!is_null($retailStores))

                    @foreach($retailStores as $i=>$retailStore)

                    <?php $id = $retailStore->id; ?>

                    <tr>

{{--                        <td>{{$retailStores->name}}</td>--}}
                        <td>{{$retailStore->address or '-'}}</td>

                        <td>{{$retailStore->city}}</td>
                        <td>{{$retailStore->state }}</td>
                        <td>{{$retailStore->pincode}}</td>
                        <td>{{$retailStore->mobile}}</td>
                        <td>@if(isset($retailStore->phone)) {{$retailStore->phone}} @else - @endif</td>
                        <td>{{$retailStore->sequence}}</td>

                        <td>
                            @if($retailStore->is_active)
                            <a href="{{URL::to('dashboard/retail-stores/activate-or-deactivate/'.$id.'/0')}}">
                                <i class="fa fa-check green"></i>
                            </a>
                            @else
                            <a href="{{URL::to('dashboard/retail-stores/activate-or-deactivate/'.$id.'/1')}}">
                                <i class="fa fa-times red"></i>
                            </a>
                            @endif
                        </td>
                        <td><?php echo AppUtil::getParsedDate($retailStore->created_at); ?></td>
                        <td>
                            <a href="{{URL::to('dashboard/retail-stores/edit/'.$id)}}"
                               class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
                            <a href="{{URL::to('dashboard/retail-stores/delete/'.$id)}}"
                               class="btn btn-xs btn-danger delete"><i class="fa fa-trash-o"></i></a>
                        </td>
                        <td>

                        </td>
                    </tr>

                    @endforeach

                    @endif

                    </tbody>

                </table>
            </div>
        </section>

    </div>
    <!--user info ends here-->

    @if(isset($retailStores))
    <div class="text-center">
        <?php echo $retailStores->links(); ?>
    </div>
    @endif

</div>


@stop