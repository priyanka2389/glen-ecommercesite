@extends('layouts.dashboard_default')

@section('content')

<script type="text/javascript">
    $(document).ready(function () {

        $('#form').validate();
        $('#sequence').chosen();
    });
</script>


<!-- page start-->
<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb add">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/distributors')}}">Retail Stores</a></li>
            <li>Add New</li>
        </ul>
        <!--breadcrumbs end -->
    </div>

</div>


<div class="row">

    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <h4>Add Retail Store</h4>
            </div>
            <div class="panel-body">
                <div class="form">
                    <form action="{{URL::to('dashboard/retail-stores/create')}}" class="cmxform form-horizontal"
                          id="form" method="post">

                        <div class="form-group ">
                            <label for="name" class="control-label col-lg-3">Email Id </label>

                            <div class="col-lg-6">
                                <input name="email" type="text" class="form-control" id="email"
                                       value="{{Input::old('email')}}"/>
                                <span class="error">{{$errors->first('email')}}</span>
                            </div>

                        </div>


                        <div class="form-group">
                            <label for="address1" class="control-label col-lg-3">Address </label>

                            <div class="col-lg-6">
                                <textarea name="address1" class="description form-control" id="address1"
                                          rows="3">{{Input::old('address1')}}</textarea>
                                <span class="error">{{$errors->first('address1')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="city" class="control-label col-lg-3">City *</label>

                            <div class="col-lg-6">
                                <input name="city" type="text" class="required form-control" id="city"
                                       value="{{Input::old('city')}}"/>
                                <span class="error">{{$errors->first('city')}}</span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="state" class="control-label col-lg-3">State *</label>

                            <div class="col-lg-6">
                                <input name="state" type="text" class="required form-control" id="state"
                                       value="{{Input::old('state')}}"/>
                                <span class="error">{{$errors->first('state')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pincode" class="control-label col-lg-3">Pincode </label>

                            <div class="col-lg-6">
                                <input name="pincode" type="text" class="form-control" id="pincode"
                                       value="{{Input::old('pincode')}}"/>
                                <span class="error">{{$errors->first('pincode')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mobile" class="control-label col-lg-3">Mobile *</label>

                            <div class="col-lg-6">
                                <input name="mobile" type="text" class="form-control" id="mobile"
                                       value="{{Input::old('mobile')}}"/>
                                <span class="error">{{$errors->first('mobile')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="control-label col-lg-3">Phone</label>

                            <div class="col-lg-6">
                                <input name="phone" type="text" class="form-control" id="phone"
                                       value="{{Input::old('phone')}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="control-label col-lg-3">Contact Person</label>

                            <div class="col-lg-6">
                                <input name="contact_person" type="text" class="form-control" id="contact_person"
                                       value="{{Input::old('contact_person')}}"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="" class="control-label col-lg-3">Sequence *</label>

                            <div class="col-lg-1">
                                <div class="radio">
                                    <input name="sequence" type="radio" class="required" value="top"/>At top
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="radio">
                                    <input type="radio" class="required" name="sequence" value="bottom"/>At End
                                </div>
                            </div>

                            @if(!is_null($retailStores))
                            <div class="col-lg-1">
                                <div class="radio">
                                    <input type="radio" class="required" name="sequence" value="after">After
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <select name="after" id="sequence" class="form-control input-sm m-bot15" data-placeholder="Select Retail Store">
                                    <option value=""></option>
                                    @foreach($retailStores as $retailStore)

                                    <option value="{{$retailStore->id}}">{{$retailStore->address}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif

<!--                            <span class="error">{{$errors->first('sequence')}}</span>-->
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<label for="small_appliance" class="control-label col-lg-3">Is Small Appliance ?</label>--}}

                            {{--<div class="col-lg-3">--}}
                                {{--<div class="checkbox">--}}
                                    {{--<input name="small_appliance" type="checkbox" id="small_appliance"/>Yes--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label for="large_appliance" class="control-label col-lg-3">Is Large Appliance ? </label>--}}

                            {{--<div class="col-lg-3">--}}
                                {{--<div class="checkbox">--}}
                                    {{--<input name="large_appliance" type="checkbox" id="large_appliance"/>Yes--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        {{--</div>--}}

                        <div class="form-group">
                            <label for="active" class="control-label col-lg-3">Is Active ? </label>

                            <div class="col-lg-3">
                                <div class="checkbox">
                                    <input name="active" type="checkbox" id="active"/>Yes
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


@stop