@extends('...layouts.dashboard_default')

@section('content')

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>


<script type="text/javascript">
    $(document).ready(function () {

        $('.delete').click(function (e) {

            $delete = confirm("Are you sure you want to delete this url ?");
            if ($delete) {
                $url = $(this).attr("href");
                window.location.href = $url;
            } else {
                e.preventDefault();
                return;
            }

        });

        $('.edit_url').click(function(){
            var mapped_id = $(this).attr("data-url-id");
            var searched_url = $(this).attr("data-searched-url");
            var mapped_url = $(this).attr("data-mapped-url");

            $('#searched_url').val(searched_url);
            $('#mapped_url').val(mapped_url);
            $('#myModal').modal();

            $('.update_btn').click(function (f) {

                f.preventDefault();
                $('.update_btn').html("<i class=\"fa fa-spinner fa-spin\"></i>Saving Changes..");
                var form = $('#form');

                var searched_url = $('#searched_url').val();
                var mapped_url = $('#mapped_url').val();
                var url = "{{URL::to('dashboard/url/update-url')}}" + '/' + searched_url + '/' + mapped_url + '/' + mapped_id;

                $.post(url, form.serialize(), function (data) {
                    if (data.success == "true") {
                        $('.update_btn').html("Update sucessfull");
                        window.location.href = "{{URL::to('dashboard/url/mapped-urls')}}";
                    } else {
                        alert("Some error occurred please try again later");
                        $('.update_btn').html("Save changes");
                    }
                });

            });

        });
    });
</script>
<style type="text/css">
    .control-label{font-weight:300;}
    .modal-content{color:#767676;}
</style>

<div class="row">
    <div class="col-lg-12">
        {{Notification::showSuccess()}}
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li>URL Mapping</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">
    <div class="col-lg-7 col-md-6 col-sm-8 col-xs-10 button_div form-group">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
            <a href="{{URL::to('dashboard/url')}}" class="btn btn-info" id="get_data">View Unmapped Urls</a>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <a href="{{URL::to('dashboard/url/mapped-urls')}}" class="btn btn-info" id="download_csv">View Mapped Urls</a>
        </div>
    </div>
    <br/>
</div>


<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Demos
            </header>
            <div class="panel-body table-responsive">
                <table class="table general-table">
                    <thead>
                    <tr>
                        <th>Searched UrL</th>
                        <th>Mapped url</th>
                        <th>Is Mapped</th>

                        <th>Created At</th>
                        <th>Updated At</th>
                        {{--<th>Deleted At</th>--}}
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>

                    <tbody>

                    @if(!is_null($urls))

                    @foreach($urls as $url)
                        <tr>
                            <td>{{$url->searched_url}}</td>
                            <td>{{$url->mapped_url or 'Not Found'}}</td>
                            <td>@if($url->is_mapped == 0)No @else Yes @endif</td>

                            <td>{{$url->created_at}}</td>
                            <td>{{$url->updated_at}}</td>
                            {{--<td>{{$url->deleted_at}}</td>--}}

                            <td><a class="edit_url" href="javascript:void(0);" data-url-id="{{$url->id}}"
                             data-searched-url="{{$url->searched_url}}" data-mapped-url="{{$url->mapped_url}}">Edit</a></td>
                            <td><a href="{{URL::to('dashboard/url/delete-url/'.$url->id)}}" class="delete">Delete</a></td>


                        </tr>
                    @endforeach

                    @endif

                    </tbody>

                </table>
            </div>
        </section>
    </div>
     <div class="text-center">
            <?php echo $urls->links(); ?>
        </div>
</div>
<!--edit order status modal-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Url</h4>
            </div>

            <form action="#" class="form-horizontal" role="form"  method="post" id="form">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="order_status" class="col-sm-3 control-label">Searched Url</label>

                        <div class="col-sm-9">
                            <input type="text" name="searched_url" id="searched_url" class="required form-control">
                        </div>
                    </div>

                    <div class="form-group" id="payment_status_block">
                        <label for="payment_status" class="col-sm-3 control-label">Mapped Url</label>

                        <div class="col-sm-9">
                            <input type="text" name="mapped_url" id="mapped_url" class="required form-control">
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary update_btn">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


@stop