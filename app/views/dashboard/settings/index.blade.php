@extends('layouts.dashboard_default')

@section('content')

<div class="row">
    <div class="col-lg-12">
      {{ Notification::showSuccess() }}
    </div>
</div>

<!-- page start-->
<div class="row">
    <div class="col-lg-10">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard/settings')}}"><i class="fa fa-home"></i>Home</a></li>
            <li>Settings</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
    <div class="col-lg-2">
        <a href="{{URL::to('dashboard/settings/create')}}" class="btn btn-sm btn-primary"><i
                class="fa fa-plus-circle"></i> Add New</a>
    </div>


</div>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Settings

            </header>
            <div class="panel-body table-responsive">
                <table class="table  table-hover general-table">
                    <thead>
                    <tr>

                        <th>Label</th>
                        <th>Value</th>
                        {{--<th>Description</th>--}}
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>

                    @if(!is_null($settings))

                    @foreach($settings as $setting)

                    <tr>

                        <td>{{$setting->label}}</td>
                        <td>{{$setting->value}}</td>

                        <td>
                            <?php $created_at = $setting->created_at;
                            echo date("d M Y", strtotime($created_at));
                            ?>
                        </td>
                        <td>
                            <a href="{{URL::to('dashboard/settings/edit/'.$setting->id)}}" class="btn btn-xs btn-info"><i
                                    class="fa fa-pencil"></i></a>
                            {{--<a href="{{URL::to('dashboard/settings/delete/'.$setting->id)}}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>--}}
                        </td>

                    </tr>


                    @endforeach

                    @else

                    @endif

                    </tbody>

                </table>
            </div>
        </section>

    </div>

    {{--@if(!is_null($tags))--}}
    <div class="text-center">
        <?php //echo $tags->links(); ?>
    </div>
    {{--@endif--}}


</div>


@stop