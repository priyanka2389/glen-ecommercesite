@extends('layouts.dashboard_default')

@section('content')

<script type="text/javascript">
    $(document).ready(function () {

        $('#form').validate();

    });

</script>

<div class="row">

    <div class="col-lg-12">

        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a href="{{URL::to('dashboard/tags')}}">Tags</a></li>
            <li>Edit</li>
        </ul>
        <!--breadcrumbs end -->

    </div>

    <div class="col-lg-12">

        <div class="panel">

            <div class="panel-heading">
                Edit
            </div>

            <div class="panel-body">

                <div class="form">
                    <form class="cmxform form-horizontal " id="form" method="post"
                          action="{{URL::to('dashboard/settings/edit/'.$setting->id)}}">
                        <div class="form-group ">
                            <label for="setting_label" class="control-label col-lg-3">Setting Label*</label>

                            <div class="col-lg-6">
                                <input name="setting_label" type="text" class="required form-control" id="setting_label"
                                       value="{{$setting->label}}"/>
                                <span class="error">{{$errors->first('label')}}</span>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="setting_value" class="control-label col-lg-3">Code *</label>

                            <div class="col-lg-6">
                                <input name="setting_value" type="text" class="required form-control" id="setting_value"
                                       value="{{$setting->value}}"/>
                                <span class="error">{{$errors->first('value')}}</span>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <button class="btn btn-default" type="button">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>


            </div>

        </div>

    </div>

</div>


@stop