@extends('layouts.dashboard_default')


@section('content')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var from_date;
            var to_date;

            var selected_from_date = $.url().param("from_date"); // parse the current page URL
            var selected_to_date = $.url().param("to_date"); // parse the current page URL

            //isUndefined is underscore js function
            if (!_.isUndefined(selected_from_date)) {
                from_date = selected_from_date;
            } else {
                from_date = -7;
            }

            if (!_.isUndefined(selected_to_date)) {
                to_date = selected_to_date;
            } else {
                to_date = 'today';
            }


            $(".datepicker").datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: new Date,
                changeMonth: true,
                changeYear: true
            });

            var selectedFromDate = '<?php echo $from_date; ?>';
            var selectedToDate = '<?php echo $to_date; ?>';
            if (selectedFromDate && selectedToDate) {
                $('#from_date').datepicker('setDate', selectedFromDate);
                $('#to_date').datepicker('setDate', selectedToDate);
            } else {
                $('#to_date').datepicker('setDate', to_date);
                $('#from_date').datepicker('setDate', from_date);

            }

            //triggers when download csv button is clicked
            $('#download_csv').click(function (e) {

                e.preventDefault();
                from_date = $('#from_date').val();
                to_date = $('#to_date').val();
                if (from_date == "") {
                    from_date = "0000-00-00";
                }
                var payment_method = [];
                $('input:checkbox:checked.payment_method').each(function () {
                    payment_method.push($(this).val());
                });

                var order_status = [];
                $('input:checkbox:checked.status').each(function () {
                    order_status.push($(this).val());
                });

                var payment_status = [];
                $('input:checkbox:checked.payment_status').each(function () {
                    payment_status.push($(this).val());
                });

                var download_csv_url = "{{URL::to('dashboard/order/download-csv')}}" + "?from_date=" + from_date + "&to_date=" + to_date + '&payment_method=' + payment_method + '&status=' + order_status + '&payment_status=' + payment_status + '&show_trash=0';
                window.location.href = download_csv_url;
            });

            //triggers when get data button is clicked

            $("[data-toggle='tooltip']").tooltip();

            $('.edit_order_status').click(function (e) {
                e.preventDefault();
                var order_id = $(this).attr("data-order-id");
                var user_id = $(this).attr("data-user-id");
                var order_status = $(this).attr("data-order-status");
                var payment_status = $(this).attr("data-payment-status");
                var payment_mode = $(this).attr('data-payment-mode');
                var order_notes = $(this).attr('data-order-notes');

                if (order_status == 'new') {
                    $("#order_status option[value='select']").attr("selected", 'selected');
                    $("#paymentStatus option[value='select']").attr("selected", 'selected');
                } else {
                    $("#order_status option[value='" + order_status + "']").attr("selected", "selected");
                    $("#paymentStatus option[value='" + payment_status + "']").attr("selected", "selected");
                }

                $('#myModal').modal();

                if (order_status == 'cancelled') {
                    if (order_notes != '') {
                        $('#notes').html(order_notes);
                    }
//                    $("#paymentStatus option[value='failed']").hide();
                } else {
                    $('#notes').html('');
                }

                $('.update_order_btn').click(function (f) {

                    f.preventDefault();

                    var order_current_status = $('#order_status option:selected').val();
                    var payment_current_status = $('#paymentStatus option:selected').val();
                    var notes = $('#notes').val();
                    var res = true;

                    if (order_current_status == 'select' && payment_current_status == 'select') {
                        $('#order_error').html('Please select order status');
                        $('#payment_error').html('Please select payment status');
                        $('#payment_error').show();
                        $('#order_error').show();
                        res = false;
                    } else if (order_current_status != 'select' && payment_current_status == 'select') {
                        $('#payment_error').show();
                        $('#payment_error').html('Please select payment status');
                        $('#order_error').hide();
                        res = false;
                    } else if (order_current_status == 'select' && payment_current_status != 'select') {
                        $('#order_error').show();
                        $('#order_error').html('Please select order status');
                        $('#payment_error').hide();
                        res = false;
                    }
                    if (order_current_status == 'cancelled') {
                        if (notes == '') {
                            $('#notes_error').show().html('Please enter notes');
                            res = false;
                        } else {
                            $('#notes_error').hide();
                        }
                    }

                    if (res == true) {
                        $('#payment_error').hide();
                        $('#order_error').hide();
                        var form = $('#form');
                        $('.update_order_btn').html("<i class=\"fa fa-spinner fa-spin\"></i>Saving Changes..");
                        var url = "{{URL::to('dashboard/customers/ajax-update-order-status')}}" + '/' + order_id + '/' + user_id;
                        $.post(url, form.serialize(), function (data) {
                            if (data.success == "true") {
                                $('.update_order_btn').html("Update sucessfull");
                                window.location.href = "{{URL::to('dashboard/order')}}";
                            } else {
                                alert("Some error occurred please try again later");
                                $('.update_order_btn').html("Save changes");
                            }
                        });
                    } else {
                        return false;
                    }

                });

                $('#order_status').change(function () {
                    var order_status = $(this).val();
                    var $sel = $('select[name="paymentStatus"]');

                    $('optgroup, optgroup > option,option', $sel).hide();
                    if (order_status == 'in-transit') {
                        var intransit_label = 'Select In-transit Status';
                        $("optgroup[label='" + intransit_label + "']", $sel).children().andSelf().show();
                        if (payment_mode == 'online') {
                            $("select optgroup[label='" + intransit_label + "'] option[value='pending']").hide();
                            $("select optgroup[label='" + intransit_label + "'] option[value='paid']").show();
                            $("select optgroup[label='" + intransit_label + "'] option[value='paid']").attr("selected", "selected");
                        } else {
                            $("select optgroup[label='" + intransit_label + "'] option[value='paid']").hide();
                            $("select optgroup[label='" + intransit_label + "'] option[value='pending']").show();
                            $("select optgroup[label='" + intransit_label + "'] option[value='pending']").attr("selected", "selected");
                        }
                        $('#notes').attr('placeholder', 'Please enter order notes');

                    } else if (order_status == 'delivered') {

                        var delievered_label = 'Select Delivered Status';
                        $("optgroup[label='" + delievered_label + "']", $sel).children().andSelf().show();
                        $("select optgroup[label='" + delievered_label + "'] option:first").attr("selected", "selected");
                        $('#notes').attr('placeholder', 'Please enter order notes');

                    } else if (order_status == 'cancelled') {
                        var cancelled_label = 'Select Cancelled Status';

                        $("optgroup[label='" + cancelled_label + "']", $sel).children().andSelf().show();

//                        if (payment_mode == 'online') {
//                            $("select optgroup[label='" + cancelled_label + "'] option[value='failed']").hide();
//                        } else {
//                            $("select optgroup[label='" + cancelled_label + "'] option[value='failed']").show();
//                        }
                        $("select optgroup[label='" + cancelled_label + "'] option:first").attr("selected", "selected");

                        $('#notes').attr('placeholder', 'Please mention the reason for cancellation.');
                        $("#notes::-webkit-input-placeholder").css({"color": "#ccc"});

                    }
                });

            });

            $('#myModal').on('shown.bs.modal', function () {
                $('#notes').attr('placeholder', 'Please enter order notes');

                $('.error').hide();
            });


        });
    </script>
    <style type="text/css">
        .pending {
            background-color: #d9edf7;
            border-color: #bce8f1;
            color: #31708f;
        }

        .unpaid {
            background-color: #f2dede;
            border-color: #ebccd1;
            color: #a94442;
        }

        .success {
            background-color: #dff0d8;
            border-color: #d6e9c6;
            color: #3c763d;
        }

        .col-lg-2 .checkbox {
            margin: 0;
        }

        .filters {
            overflow-x: auto;
        }

        #notes::-webkit-input-placeholder {
            color: #c2c2c2
        }

        #header_tab li.active a, #header_tab li a:hover {
            border-radius: 0 !important;
            -webkit-border-radius: 0 !important;
        }

        optgroup {
            font-weight: normal;
        }

        .disabled {
            cursor: not-allowed;
            background-color: #eee;
        }
    </style>

    <div class="row">
        <div class="col-lg-12">
            {{Notification::showSuccess()}}
            {{Notification::showError()}}
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
                <li>Orders</li>
            </ul>
            <!--breadcrumbs end -->
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel filters">
                <header class="panel-heading">
                    <i class="fa fa-filter"></i> Filters
                </header>
                <div class="panel-body table-responsive">
                    <form name="form" class="form form-group " id="paymentForm" method="post"
                          action="{{URL::to('dashboard/order')}}">
                        <div class="col-lg-12">
                            <div class="col-lg-4 form-horizontal">
                                <div class="form-group">
                                    <label for="from_date" class="col-sm-3 control-label">From Date:</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="datepicker form-control" id="from_date"
                                               name="from_date">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 form-horizontal">
                                <div class="form-group">
                                    <label for="to_date" class="col-sm-3 control-label">To Date:</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="datepicker form-control" id="to_date" name="to_date">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-12">
                            <div class="col-lg-3 form-horizontal" style="margin:0 89px 0 -53px;">
                                <div class="form-group">
                                    <label for="status" class="col-sm-6 control-label">Payment Method:</label>

                                    @for($i=0;$i<count($payment_method);$i++)
                                        <div class="col-sm-2">
                                            <div class="checkbox">
                                                <?php
                                                (isset($selected_payment_method) && in_array($payment_method[$i], $selected_payment_method)) ? $checked = 'checked' : $checked = '';
                                                ?>
                                                <input type="checkbox" name="payment_method[]" class="payment_method"
                                                       value="{{$payment_method[$i]}}" {{$checked}} >{{ucfirst($payment_method[$i])}}
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                            <div class="col-lg-4 form-horizontal">

                                <div class="form-group">
                                    <label for="status" class="col-sm-5 control-label">Order Status:</label>

                                    @for($i=0;$i<count($order_status);$i++)
                                        @if($i % 2 == 0)
                                            @if($i!=0)
                                                <label for="status" class="col-sm-5 control-label"></label>
                                            @endif
                                            <div class="col-lg-7" style="margin-left:-18px;">
                                                @endif
                                                <div class="col-lg-6">
                                                    <div class="checkbox">
                                                        <?php
                                                        (isset($selected_order_status) && in_array($order_status[$i], $selected_order_status)) ? $checked = 'checked' : $checked = '';
                                                        ?>
                                                        <input type="checkbox" name="status[]" class="status"
                                                               value="{{strtolower($order_status[$i])}}" {{$checked}} >{{ucfirst($order_status[$i])}}
                                                    </div>
                                                </div>
                                                @if($i % 2 == 1)
                                            </div>
                                        @endif
                                    @endfor
                                </div>

                            </div>
                            <div class="col-lg-4 form-horizontal">

                                <div class="form-group ">
                                    <label for="status" class="col-sm-4 control-label">Payment Status:</label>
                                    @for($i=0;$i<count($payment_status);$i++)
                                        @if($i % 2 == 0)
                                            @if($i!=0)
                                                <label for="status" class="col-sm-4 control-label"></label>
                                            @endif
                                            <div class="col-lg-8">
                                                @endif
                                                <div class="col-lg-6">
                                                    <div class="checkbox">
                                                        <?php
                                                        (isset($selected_payment_status) && in_array($payment_status[$i], $selected_payment_status)) ? $checked = 'checked' : $checked = '';
                                                        ?>
                                                        <input type="checkbox" name="payment_status[]"
                                                               class="payment_status"
                                                               value="{{strtolower($payment_status[$i])}}" {{$checked}} >{{ucfirst($payment_status[$i])}}
                                                    </div>
                                                </div>
                                                @if($i % 2 == 1)
                                            </div>
                                        @endif
                                    @endfor
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="col-lg-3 col-md-6 col-sm-8 col-xs-10 button_div pull-right">
                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                                    {{--<a href="" class="btn btn-info" id="get_data">Get Data</a>--}}
                                    <input type="submit" name="get_data" class="btn btn-info" id="get_data"
                                           value="Get Data">
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    <a href="" class="btn btn-info" id="download_csv">Download Csv</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <ul role="tablist" class="nav nav-tabs" id="header_tab">
                        <li role="presentation" class="active"><a href="{{URL::to('dashboard/order')}}">Orders</a></li>
                        <li role="presentation"><a href="{{URL::to('dashboard/order/order-trash-report')}}">Deleted By
                                Admin</a></li>
                    </ul>
                </header>
                <div class="panel-body table-responsive">
                    <table class="table general-table">
                        <thead>
                        <tr>
                            <th>Order No.</th>
                            <th>Reference Id</th>
                            <th>Customer Name</th>
                            <th>Final Value</th>
                            <th>Order Status</th>
                            <th>Payment Status</th>
                            <th>Payment Method</th>
                            <th>Created On</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        @if(!is_null($orders))
                            <?php $j = 0; ?>
                            @foreach($orders as $i=>$order)

                                <tr class="{{$order->payment_status}}">
                                    <td>{{$order->id}}</td>
                                    <td>{{$order->reference}}</td>
                                    <td>
                                        {{$order->user->first_name.' '.$order->user->last_name}}
                                    </td>
                                    <td><span class="WebRupee"> Rs.</span>{{$order->final_value}}</td>
                                    <td>
                                        {{ucfirst($order->status)}}
                                    </td>
                                    <td>{{ucfirst($order->payment_status)}}</td>

                                    <td>{{ucfirst($order->payment_mode)}}</td>

                                    <td>{{$order->created_at}}</td>
                                    <td>
                                        <a href="{{URL::to('dashboard/order/info/'.$order->id)}}"
                                           class="btn btn-info btn-xs" data-toggle="tooltip" title="View order">
                                            <i class="fa fa-search"></i>
                                        </a>
                                        @if($order->status=='cancelled' && $order->payment_status=='failed' && $order->payment_mode == 'online')
                                            <?php $disabled = "disabled"; ?>
                                        @else
                                            <?php $disabled = ""; ?>
                                        @endif
                                        <a href="javascript:void(0);"
                                           class="btn btn-success btn-xs edit_order_status {{$disabled}}"
                                           data-order-id="{{$order->id}}" data-payment-mode="{{$order->payment_mode}}"
                                           data-user-id="{{$order->user->id or ''}}"
                                           data-order-notes="{{$order->notes or ''}}"
                                           data-order-status="{{$order->status}}"
                                           data-payment-status="{{$order->payment_status}}" data-toggle="tooltip"
                                           title="Edit order">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a href="{{Url::to('dashboard/order/tracking/'.$order->id)}}"
                                           class="btn btn-info btn-xs" data-toggle="tooltip"
                                           title="Edit tracking detail"><i class="fa fa-pencil"></i></a>

                                        <a href="{{Url::to('dashboard/order/download-order/'.$order->id)}}"
                                           class="btn btn-success btn-xs" data-toggle="tooltip"
                                           title="Download Order" target="_blank"><i class="fa fa-download"></i></a>

                                        <a href="{{Url::to('dashboard/order/delete-order/'.$order->id)}}"
                                           class="btn btn-danger btn-xs  delete" data-toggle="tooltip"
                                           title="Delete Order"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                    <?php $j = $j + 1; ?>
                                </tr>

                            @endforeach

                        @else
                            <tr>
                                <td colspan="9" align="center">No data found</td>
                            </tr>
                        @endif

                        </tbody>

                    </table>
                </div>
            </section>
        </div>
        <div class="text-center">
            <?php if (!empty($orders)) {
                echo $orders->appends(array('from_date' => $from_date, 'to_date' => $to_date, 'payment_method' => $selected_payment_method, 'payment_status' => $selected_payment_status, 'status' => $selected_order_status))->links();
            } ?>
        </div>

    </div>

    <!--edit order status modal-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog modal-sm">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Order Status</h4>
                </div>

                <form action="#" class="form-horizontal" role="form"
                      method="post" id="form">
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="order_status" class="col-sm-3 control-label">Order Status *</label>

                            <div class="col-sm-9">
                                <select name="order_status" class="form-control required" id="order_status">
                                    <option value="select">Select Order Status</option>
                                    {{--<option value="in-transit">In-transit</option>--}}
                                    <option value="delivered">Delivered</option>
                                    <option value="cancelled">Cancelled</option>
                                    {{--<option value="closed">Closed</option>--}}
                                </select>
                                <span class="error" id="order_error"></span>
                            </div>
                        </div>

                        <div class="form-group" id="payment_status_block">
                            <label for="paymentStatus" class="col-sm-3 control-label">Payment Status *</label>

                            <div class="col-sm-9">
                                <select name="paymentStatus" class="form-control required" id="paymentStatus">
                                    <option value="select">Select Payment Status</option>
                                    {{--<optgroup label="Select In-transit Status">--}}
                                    {{--<option value="paid">Paid</option>--}}
                                    {{--<option value="pending">Pending</option>--}}
                                    {{--</optgroup>--}}
                                    <optgroup label="Select Delivered Status">
                                        <option value="paid">Paid</option>
                                    </optgroup>
                                    <optgroup label="Select Cancelled Status">
                                        <option value="to be refunded">To-be refunded</option>
                                        <option value="refunded">Refunded</option>
                                        <option value="failed">Failed</option>
                                    </optgroup>
                                </select>
                                <span class="error" id="payment_error"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="notes" class="col-sm-3 control-label">Notes</label>

                            <div class="col-sm-9">
                                <textarea name="notes" class="form-control" id="notes" rows="3"></textarea>
                                <span class="error" id="notes_error"></span>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary update_order_btn">Save changes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop

{{--SELECT orders.id,orders.reference,users.first_name,users.last_name,orders.final_value,orders.status,orders.payment_status--}}
{{--orders.payment_mode,orders.created_at--}}
{{--FROM `orders`--}}
{{--join users on orders.user_id = users.id--}}