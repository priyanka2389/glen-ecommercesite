@extends('layouts.dashboard_default')

@section('content')


    <style type="text/css" xmlns="http://www.w3.org/1999/html">
        .payment-method h4 {
            display: inline-block;
        }
    </style>

    <div class="row">
        <div class="col-lg-11">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="{{URL::to('dashboard/order')}}">Orders</a></li>
                <li>Order Info</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
        <div class="col-lg-1">
            <a href="{{URL::to('dashboard/order')}}" id="back" class="btn btn-primary">Back</a>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <div class="panel-body invoice">

                    <h2>{{ucwords($order->user->first_name.' '.$order->user->last_name)}}</h2> <br/>

                    <div class="row invoice-to">
                        <div class="col-md-4">


                            @if(isset($shipping_address))
                                <h4>Shipping Address:</h4>

                                <p>
                                    <strong>Shipped To
                                        : </strong>@if($shipping_address->first_name != null &&  $shipping_address->last_name != null) {{ucfirst($shipping_address->first_name) .' '. $shipping_address->last_name}}@else {{ucfirst($order->user->first_name).' '.$order->user->last_name}} @endif
                                    <br>
                                    <strong>Address Line 1 :</strong> {{$shipping_address->line1}}<br>
                                    <strong>Address Line 2 :</strong> @if(!empty($shipping_address->line2)) {{$shipping_address->line2}} @else - @endif<br>
                                    <strong>Landmark :</strong> {{$shipping_address->landmark or '-'}}
                                    <strong>Mobile :</strong>@if($order->user->mobile == null) {{$shipping_address->mobile}}@else {{$order->user->mobile}} @endif<br>
                                    <strong>Email :</strong> {{$order->user->email}} <br/>
                                    <strong>City :</strong> {{$shipping_address->city}} <br/>
                                    <strong>State :</strong> {{$shipping_address->state}} <br/>
                                    <strong>Pincode :</strong> {{$shipping_address->pincode}}
                                </p>
                            @endif

                        </div>
                        <div class="col-lg-4">


                            @if(isset($billing_address))
                                <h4>Billing Address</h4>

                                <p>

                                    <strong>Billed To
                                        : </strong>@if($billing_address->first_name != null &&  $billing_address->last_name != null) {{ucfirst($billing_address->first_name) .' '. $billing_address->last_name}}@else {{ucfirst($order->user->first_name).' '.$order->user->last_name}} @endif
                                    <br>
                                    <strong>Address Line 1 :</strong> {{$billing_address->line1}}<br>
                                    <strong>Address Line 2 :</strong>  @if(!empty($billing_address->line2)) {{$billing_address->line2}} @else - @endif<br>
                                    <strong>Landmark :</strong> {{$billing_address->landmark or '-'}}
                                    <strong>Mobile :</strong>@if($order->user->mobile == null) {{$billing_address->mobile or '-'}} @else {{$order->user->mobile}} @endif<br>
                                    <strong>Email :</strong> {{$order->user->email}} <br/>
                                    <strong>City :</strong> {{$billing_address->city}} <br/>
                                    <strong>State :</strong> {{$billing_address->state}} <br/>
                                    <strong>Pincode :</strong> {{$billing_address->pincode}}
                                </p>

                            @endif

                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-4 col-sm-5 inv-label">Reference #</div>
                                <div class="col-md-8 col-sm-7">{{$order->reference}}</div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-4 col-sm-5 inv-label">Date #</div>
                                <div class="col-md-8 col-sm-7">{{ AppUtil::getParsedDate($order->created_at)}}</div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-12 inv-label">
                                    <h3>TOTAL AMOUNT</h3>
                                </div>
                                <div class="col-md-12">
                                    <h1 class="amnt-value"><span class="WebRupee"> Rs.</span>{{$order->final_value}}
                                    </h1>
                                </div>
                            </div>


                        </div>
                    </div>
                    <table class="table table-invoice">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Item Type</th>
                            <th>Item Name</th>
                            <th class="text-center">Unit Cost</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center">Sub Total</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($items as $i=>$item)

                            <tr>
                                <td>{{$i+1}}</td>
                                <td>
                                    <h4>{{ucwords($item['item_type'])}}</h4>
                                </td>
                                <td class="text-center">{{$item['item_name']}}</td>
                                @if($item['offer_price']> 0)
                                    <td class="text-center"><span class="WebRupee"> Rs.</span>{{$item['offer_price']}}</td>
                                    <td class="text-center">{{$item['qty']}}</td>
                                    <td class="text-center">
                                        <span class="WebRupee"> Rs.</span>{{$item['offer_price']*$item['qty']}}
                                    </td>
                                @else
                                    <td class="text-center"><span class="WebRupee"> Rs.</span>{{$item['list_price']}}</td>
                                    <td class="text-center">{{$item['qty']}}</td>
                                    <td class="text-center">
                                        <span class="WebRupee"> Rs.</span>{{$item['list_price']*$item['qty']}}
                                    </td>
                                @endif
                            </tr>

                        @endforeach

                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-md-9 payment-method">
                            <h4>Order Status : </h4> {{$order->status}} <br/>
                            <h4>Payment Status : </h4> {{$order->payment_status}} <br/>
                            <h4>Payment Method : </h4> {{$order->payment_mode or '-'}}<br/>
                            <h4>Comments : </h4> {{$order->notes or '  ---'}}
                            <br>
                        </div>
                        <div class="col-md-2 invoice-block pull-right">
                            <ul class="unstyled amounts">
                                <!--                            <li>Sub - Total amount : 3820</li>-->
                                <li class="grand-total">Grand Total : <span
                                            class="WebRupee"> Rs.</span>{{$order->final_value}}
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <section class="panel">
                <div class="panel-body ">
                    @if(null==$order->trackingInfo)

                        <p>No tracking data found for this order.</p>

                        <a class="btn btn-primary" href="{{Url::to('dashboard/order/tracking/'.$order->id)}}">Add
                            Tracking Data</a>

                    @else
                        <div class="row">
                            <div class="col-lg-5"><h3>Tracking Information</h3></div>
                            <div class="col-lg-7">
                                <a class="btn btn-primary" href="{{Url::to('dashboard/order/tracking/'.$order->id)}}">Update
                                    Tracking Data</a>
                                <a class="btn btn-primary"
                                   href="{{Url::to('dashboard/order/mailtracking/'.$order->id)}}"><i class="fa fa-envelope"></i> Send Mail</a>
                            </div>

                        </div>

                        <table class="table table-bordered">

                            <tr>
                                <td>Provider</td>
                                <td>{{$order->trackingInfo->provider}}</td>
                            </tr>

                            <tr>
                                <td>Code</td>
                                <td>{{$order->trackingInfo->code}}</td>
                            </tr>

                            <tr>
                                <td>Tracking Url</td>
                                <td>{{$order->trackingInfo->tracking_url}}</td>
                            </tr>

                            <tr>
                                <td>Notes</td>
                                <td>{{$order->trackingInfo->notes}}</td>
                            </tr>
                        </table>


                    @endif

                </div>
        </div>
        </section>

    </div>

@stop