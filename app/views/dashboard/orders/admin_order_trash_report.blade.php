@extends('layouts.dashboard_default')


@section('content')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var from_date;
            var to_date;

            var selected_from_date = $.url().param("from_date"); // parse the current page URL
            var selected_to_date = $.url().param("to_date"); // parse the current page URL

            //isUndefined is underscore js function
            if (!_.isUndefined(selected_from_date)) {
                from_date = selected_from_date;
            } else {
                from_date = -7;
            }

            if (!_.isUndefined(selected_to_date)) {
                to_date = selected_to_date;
            } else {
                to_date = 'today';
            }


            $(".datepicker").datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: new Date,
                changeMonth: true,
                changeYear: true
            });

            var selectedFromDate = '<?php echo $from_date; ?>';
            var selectedToDate = '<?php echo $to_date; ?>';
            if (selectedFromDate && selectedToDate) {
                $('#from_date').datepicker('setDate', selectedFromDate);
                $('#to_date').datepicker('setDate', selectedToDate);
            } else {
                $('#to_date').datepicker('setDate', to_date);
                $('#from_date').datepicker('setDate', from_date);

            }

            //triggers when download csv button is clicked
            $('#download_csv').click(function (e) {

                e.preventDefault();
                from_date = $('#from_date').val();
                to_date = $('#to_date').val();
                if (from_date == "") {
                    from_date = "0000-00-00";
                }
                var payment_method = [];
                $('input:checkbox:checked.payment_method').each(function () {
                    payment_method.push($(this).val());
                });

                var order_status = [];
                $('input:checkbox:checked.status').each(function () {
                    order_status.push($(this).val());
                });

                var payment_status = [];
                $('input:checkbox:checked.payment_status').each(function () {
                    payment_status.push($(this).val());
                });

                var download_csv_url = "{{URL::to('dashboard/order/download-csv')}}" + "?from_date=" + from_date + "&to_date=" + to_date + '&payment_method=' + payment_method + '&status=' + order_status + '&payment_status=' + payment_status + '&show_trash=1';
                window.location.href = download_csv_url;
            });

            //triggers when get data button is clicked

            $("[data-toggle='tooltip']").tooltip();

        });
    </script>
    <style type="text/css">
        .pending {
            background-color: #d9edf7;
            border-color: #bce8f1;
            color: #31708f;
        }

        .unpaid {
            background-color: #f2dede;
            border-color: #ebccd1;
            color: #a94442;
        }

        .success {
            background-color: #dff0d8;
            border-color: #d6e9c6;
            color: #3c763d;
        }

        .col-lg-2 .checkbox {
            margin: 0;
        }

        .filters {
            overflow-x: auto;
        }

        #notes::-webkit-input-placeholder {
            color: #c2c2c2
        }

        #header_tab li.active a, #header_tab li a:hover {
            border-radius: 0 !important;
            -webkit-border-radius: 0 !important;
        }
    </style>

    <div class="row">
        <div class="col-lg-12">
            {{Notification::showSuccess()}}
            {{Notification::showError()}}
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
                <li>Orders</li>
            </ul>
            <!--breadcrumbs end -->
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel filters">
                <header class="panel-heading">
                    <i class="fa fa-filter"></i> Filters
                </header>
                <div class="panel-body table-responsive">
                    <form name="form" class="form form-group " id="paymentForm" method="post"
                          action="{{URL::to('dashboard/order/order-trash-report')}}">
                        <div class="col-lg-12">
                            <div class="col-lg-4 form-horizontal">
                                <div class="form-group">
                                    <label for="from_date" class="col-sm-3 control-label">From Date:</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="datepicker form-control" id="from_date"
                                               name="from_date">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 form-horizontal">
                                <div class="form-group">
                                    <label for="to_date" class="col-sm-3 control-label">To Date:</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="datepicker form-control" id="to_date" name="to_date">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-12">
                            <div class="col-lg-3 form-horizontal" style="margin:0 89px 0 -53px;">
                                <div class="form-group">
                                    <label for="payment_method" class="col-sm-6 control-label">Payment Method:</label>

                                    @for($i=0;$i<count($payment_method);$i++)
                                        <div class="col-sm-2">
                                            <div class="checkbox">
                                                <?php
                                                (isset($selected_payment_method) && in_array($payment_method[$i], $selected_payment_method)) ? $checked = 'checked' : $checked = '';
                                                ?>
                                                <input type="checkbox" name="payment_method[]" class="payment_method"
                                                       value="{{$payment_method[$i]}}" {{$checked}} >{{ucfirst($payment_method[$i])}}
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                            <div class="col-lg-4 form-horizontal">

                                <div class="form-group">
                                    <label for="status" class="col-sm-5 control-label">Order Status:</label>

                                    @for($i=0;$i<count($order_status);$i++)
                                        @if($i % 2 == 0)
                                            @if($i!=0)
                                                <label for="" class="col-sm-5 control-label"></label>
                                            @endif
                                            <div class="col-lg-7" style="margin-left:-18px;">
                                                @endif
                                                <div class="col-lg-6">
                                                    <div class="checkbox">
                                                        <?php
                                                        (isset($selected_order_status) && in_array($order_status[$i], $selected_order_status)) ? $checked = 'checked' : $checked = '';
                                                        ?>
                                                        <input type="checkbox" name="status[]" class="status"
                                                               value="{{strtolower($order_status[$i])}}" {{$checked}} >{{ucfirst($order_status[$i])}}
                                                    </div>
                                                </div>
                                                @if($i % 2 == 1)
                                            </div>
                                        @endif
                                    @endfor
                                </div>

                            </div>
                            <div class="col-lg-4 form-horizontal">

                                <div class="form-group ">
                                    <label for="payment_status" class="col-sm-4 control-label">Payment Status:</label>
                                    @for($i=0;$i<count($payment_status);$i++)
                                        @if($i % 2 == 0)
                                            @if($i!=0)
                                                <label for="" class="col-sm-4 control-label"></label>
                                            @endif
                                            <div class="col-lg-8">
                                                @endif
                                                <div class="col-lg-6">
                                                    <div class="checkbox">
                                                        <?php
                                                        (isset($selected_payment_status) && in_array($payment_status[$i], $selected_payment_status)) ? $checked = 'checked' : $checked = '';
                                                        ?>
                                                        <input type="checkbox" name="payment_status[]"
                                                               class="payment_status"
                                                               value="{{strtolower($payment_status[$i])}}" {{$checked}} >{{ucfirst($payment_status[$i])}}
                                                    </div>
                                                </div>
                                                @if($i % 2 == 1)
                                            </div>
                                        @endif
                                    @endfor
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="col-lg-3 col-md-6 col-sm-8 col-xs-10 button_div pull-right">
                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                                    {{--<a href="" class="btn btn-info" id="get_data">Get Data</a>--}}
                                    <input type="submit" name="get_data" class="btn btn-info" id="get_data"
                                           value="Get Data">
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    <a href="" class="btn btn-info" id="download_csv">Download Csv</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <ul role="tablist" class="nav nav-tabs" id="header_tab">
                        <li role="presentation"><a href="{{URL::to('dashboard/order')}}">Orders</a></li>
                        <li role="presentation" class="active"><a
                                    href="{{URL::to('dashboard/order/order-trash-report')}}">Deleted By Admin</a></li>
                    </ul>
                </header>
                <div class="panel-body table-responsive">
                    <table class="table general-table">
                        <thead>
                        <tr>
                            <th>Order no.</th>
                            <th>Reference Id</th>
                            <th>Customer Name</th>
                            <th>Final Value</th>
                            <th>Status</th>
                            <th>Payment Status</th>
                            <th>Payment Method</th>
                            <th>Created On</th>
                            <th>Deleted On</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        @if(!is_null($orders))
                            <?php $j = 0; ?>
                            @foreach($orders as $i=>$order)

                                <tr class="{{$order->payment_status}}">
                                    <td>{{$order->id}}</td>
                                    <td>{{$order->reference}}</td>
                                    <td>
                                        {{$order->user->first_name.' '.$order->user->last_name}}
                                    </td>
                                    <td><span class="WebRupee"> Rs.</span>{{$order->final_value}}</td>
                                    <td>
                                        {{ucfirst($order->status)}}
                                    </td>
                                    <td>{{ucfirst($order->payment_status)}}</td>

                                    <td>{{ucfirst($order->payment_mode)}}</td>

                                    <td>{{$order->created_at}}</td>

                                    <td>{{$order->deleted_by_admin}}</td>
                                    <td>
                                        <a href="{{URL::to('dashboard/order/info/'.$order->id)}}"
                                           class="btn btn-info btn-xs" data-toggle="tooltip" title="View order">
                                            <i class="fa fa-search"></i>
                                        </a>

                                        <a href="{{URL::to('dashboard/order/undo-delete-order/'.$order->id)}}"
                                           class="btn btn-xs btn-danger delete" title="Undo Delete"
                                           data-toggle="tooltip">
                                            <i class="fa fa-undo"></i>
                                        </a>
                                    </td>
                                    <?php $j = $j + 1; ?>
                                </tr>

                            @endforeach

                        @else
                            <tr>
                                <td colspan="9" align="center">No data found</td>
                            </tr>
                        @endif

                        </tbody>

                    </table>
                </div>
            </section>
        </div>
        <div class="text-center">
            <?php if (!empty($orders)) {
                echo $orders->appends(array('from_date' => $from_date, 'to_date' => $to_date, 'payment_method' => $selected_payment_method, 'payment_status' => $selected_payment_status, 'status' => $selected_order_status))->links();
            } ?>
        </div>

    </div>



@stop