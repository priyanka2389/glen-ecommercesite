@extends('layouts.dashboard_default')

@section('content')


<script type="text/javascript">
    $(document).ready(function () {

        $("form").validate();


    });
</script>



<div class="row">
    <div class="col-lg-12">
        {{ Notification::showSuccess() }}
        {{ Notification::showError() }}
    </div>
</div>


<div class="row">
    <div class="col-lg-11">
        <!--breadcrumbs start -->
        <ul class="breadcrumb edit">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/order')}}">Orders</a></li>
            <li><a href="{{URL::to('dashboard/order/info/'.$order->id)}}">Order Detail</a></li>
            <li>Order Tracking</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
    <div class="col-lg-1">
        <a href="{{URL::to('dashboard/order/info/'.$order->id)}}" id="back" class="btn btn-primary">Back</a>
    </div>

</div>



<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-heading">Update Tracking Info</div>
            <div class="panel-body ">
                {{ Form::open(array("class"=>"form-horizontal")) }}
                <div class="form-group ">
                    <label for="provider" class="control-label col-lg-3">Provider *</label>

                    <div class="col-lg-6">
                        <input name="provider" type="text" class="required form-control"
                               id="provider"
                               value="{{$trackingInfo->provider or Input::old('provider')}}"/>
                        <span class="error">{{$errors->first('provider')}}</span>
                    </div>
                </div>

                <div class="form-group ">
                    <label for="code" class="control-label col-lg-3">Code *</label>

                    <div class="col-lg-6">
                        <input name="code" type="text" class="required form-control"
                               id="code"
                               value="{{$trackingInfo->code or Input::old('code')}}"/>
                        <span class="error">{{$errors->first('code')}}</span>
                    </div>
                </div>

                 <div class="form-group ">
                    <label for="tracking_url" class="control-label col-lg-3">Tracking Url *</label>

                    <div class="col-lg-6">
                        <input name="tracking_url" type="text" class="required form-control"
                               id="tracking_url"
                               value="{{ $trackingInfo->tracking_url or Input::old('tracking_url') }}"/>
                        <span class="error">{{$errors->first('tracking_url')}}</span>
                    </div>
                </div>

                <div class="form-group ">
                    <label for="notes" class="control-label col-lg-3">Notes </label>

                    <div class="col-lg-6">
                        <textarea name="notes" type="text" class="form-control"
                               id="notes"
                               value="{{$trackingInfo->notes or Input::old('notes') }}">{{$trackingInfo->notes or Input::old('notes') }}</textarea>
                        <span class="error">{{$errors->first('notes')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-6">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div>
                </div>
                {{ Form::close() }}

    </div>
    </div>
    </section>

</div>

@stop