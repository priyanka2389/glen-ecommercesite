<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        /*.wrapper {display: inline-block;margin-top: -21px;padding: 15px;width: 100%;}*/
        /*.col-sm-12 {          width: 100%;       }*/
        /*.payment-method h4 {display: inline-block;}*/
        h1 {
            font-size: 36px;
        }

        h2 {
            font-size: 30px;
            font-weight: 500;
        }

        body {
            color: #767676;
            font-family: 'Open Sans', sans-serif;
            padding: 0px !important;
            margin: 0px !important;
            font-size: 13px;
            text-rendering: optimizeLegibility;
        }

        h4 {
            font-size: 18px;
            margin-top: 10px;
            margin-bottom: 10px;
            font-weight: 500;
            line-height: 1.1;
            display: inline-block;
        }

        .col-md-4 {
            width: 33.33333333333333%;
            float: left;
        }

        .row {
            margin-right: 15px;
            margin-left: 15px;
        }

        .invoice table tr td {
            line-height: 25px;
            color: #aeaeb1;
        }

        .inv-label {
            color: #1fb5ad;
        }

        .table-invoice thead tr th {
            background: #e8e9f0;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            vertical-align: middle;
            padding: 10px;
        }

        .table-invoice tbody tr td {
            margin-top: 10px;
            background: #f5f6f9;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            vertical-align: middle;
            padding: 10px;
        }

        .inv-label h3 {
            font-weight: 300;
            font-size: 20px;
            margin-top: 20px;
            margin-bottom: 10px;
        }

        .amounts.grand-total {
            font-weight: bold;
            background: #f5f6f9;
            margin-bottom: 5px;
            padding: 10px;
            border-radius: 4px;
            -webkit-border-radius: 4px;
            font-weight: 300;
            font-size: 16px;
            list-style: none;;
            float: right;
            margin-left: 10px;
        }

        .table-bordered tr td {
            border: 1px solid #DDD;
            border-collapse: collapse;
            border-spacing: 0;
        }


    </style>
</head>

<body class="full-width">
<table>
    <tr>
        <td><h2>{{ucwords($order->user->first_name.' '.$order->user->last_name)}}</h2> <br/></td>
    </tr>
    <tr>
        <td width="33%">
            <table>
                <tr>
                    <td>
                        @if(isset($shipping_address))
                            <h4>Shipping Address:</h4>

                            <table>
                                <tr>
                                    <td><strong>Address Line 1 :</strong> {{$shipping_address->line1}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Address Line 2 :</strong> {{$shipping_address->line2 or '-'}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Landmark :</strong> {{$shipping_address->landmark}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Mobile :</strong> {{$shipping_address->mobile}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Email :</strong> {{$order->user->email}}</td>
                                </tr>
                                <tr>
                                    <td><strong>City :</strong> {{$shipping_address->city}}</td>
                                </tr>
                                <tr>
                                    <td><strong>State :</strong> {{$shipping_address->state}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Pincode :</strong> {{$shipping_address->pincode}}</td>
                                </tr>
                            </table>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td width="33%">
            <table class="col-md-4" style="width:33%">
                <tr>
                    <td>
                        @if(isset($billing_address))
                            <h4>Billing Address:</h4>

                            <table>
                                <tr>
                                    <td><strong>Address Line 1 :</strong> {{$billing_address->line1}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Address Line 2 :</strong> {{$billing_address->line2 or '-'}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Landmark :</strong> {{$billing_address->landmark}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Mobile :</strong> {{$billing_address->mobile}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Email :</strong> {{$order->user->email}} </td>
                                </tr>
                                <tr>
                                    <td><strong>City :</strong> {{$billing_address->city}}</td>
                                </tr>
                                <tr>
                                    <td><strong>State :</strong> {{$billing_address->state}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Pincode :</strong> {{$billing_address->pincode}}</td>
                                </tr>
                            </table>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td width="33%">

            <table width="100%">
                <tr>
                    <td class="inv-label">Refernce #</td>
                    <td>{{$order->reference}}</td>
                </tr>

                <tr>
                    <td class="inv-label">Date #</td>
                    <td>{{AppUtil::getParsedDate($order->created_at)}}</td>
                </tr>
            </table>
            <table>
                <tr>
                    <td class="inv-label"><h3>TOTAL AMOUNT</h3></td>
                </tr>
                <tr>
                    <td>
                        <h1 class="amnt-value">
                            Rs.{{round($order->final_value,2)}}
                        </h1>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">
            <table class="table table-invoice" width="100%">
                <thead>
                <tr>
                    <th width="5%">#</th>
                    <th width="10%">Item Type</th>
                    <th width="30%">Item Name</th>
                    <th width="20%" class="text-center">Unit Cost</th>
                    <th width="15%" class="text-center">Quantity</th>
                    <th width="20%" class="text-center">Sub Total</th>
                </tr>
                </thead>
                <tbody>

                @foreach($items as $i=>$item)

                    <tr>
                        <td width="5%">{{$i+1}}</td>
                        <td width="10%">
                            <h4>{{ucwords($item['item_type'])}}</h4>
                        </td>
                        <td class="text-center" width="30%">{{$item['item_name']}}</td>
                        <td class="text-center" width="20%"><span
                                    class="WebRupee"> Rs.</span>{{$item['offer_price']}}</td>
                        <td class="text-center" width="15%">{{$item['qty']}}</td>
                        <td class="text-center" width="20%"><span
                                    class="WebRupee"> Rs.</span>{{$item['offer_price']*$item['qty']}}
                        </td>
                    </tr>

                @endforeach

                </tbody>
            </table>

        </td>
    </tr>
    <tr>
        <td width="65%" colspan="2">
            <table>
                <tr>
                    <td><h4>Order Status : {{$order->status}}</h4></td>
                </tr>
                <tr>
                    <td><h4>Payment Status : {{$order->payment_status}}</h4></td>
                </tr>
                <tr>
                    <td><h4>Payment Method :  {{$order->payment_mode or '-'}}</h4></td>
                </tr>
                <tr>
                    <td><h4>Comments :   {{$order->notes or '  ---'}}</h4></td>
                </tr>
            </table>
        </td>

        <td width="35%">
            <table>
                <tr>
                    <td class="unstyled amounts grand-total">
                        Grand Total :
                        <span class="WebRupee"> Rs.</span>{{round($order->final_value,2)}}
                    </td>
                </tr>
            </table>
        </td>

    </tr>


</table>
@if($order->trackingInfo != null)
    <table width="50%">
        <tr>
            <td><h3>Tracking Information</h3></td>
        </tr>
        <tr>
            <td width="100%">
                <table class="table table-bordered" width="100%"
                       style="border:1px solid #DDD;border-spacing:0;line-height:20px;">

                    <tr>
                        <td><strong>Provider</strong></td>
                        <td>{{$order->trackingInfo->provider}}</td>
                    </tr>

                    <tr>
                        <td><strong>Code</strong></td>
                        <td>{{$order->trackingInfo->code}}</td>
                    </tr>

                    <tr>
                        <td><strong>Tracking URL</strong></td>
                        <td>{{$order->trackingInfo->tracking_URL}}</td>
                    </tr>

                    <tr>
                        <td><strong>Notes</strong></td>
                        <td>{{$order->trackingInfo->notes}}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    @endif


            <!--footer start-->
    {{--<footer class="footer-section">--}}
    {{--<div class="text-center">--}}
    {{--2014 &copy; Powered by <a href="http://greenapplesolutions.com/" target="_blank" class="company_link">Green--}}
    {{--Apple Solutions</a>--}}
    {{--</div>--}}
    {{--</footer>--}}
    <!--footer end-->


</body>
</html>
