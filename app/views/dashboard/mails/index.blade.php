@extends('layouts.dashboard_default')

@section('content')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">
    <script type="text/javascript"
            src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_overlay.png">
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite.png">
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite@2x.png">
    <script type="text/javascript">
        $(document).ready(function () {

            var from_date;
            var to_date;

            var selected_from_date = $.url().param("from_date"); // parse the current page URL
            var selected_to_date = $.url().param("to_date"); // parse the current page URL

            //isUndefined is underscore js function
            if (!_.isUndefined(selected_from_date)) {
                from_date = selected_from_date;
            } else {
                from_date = '';
            }

            if (!_.isUndefined(selected_to_date)) {
                to_date = selected_to_date;
            } else {
                to_date = 'today';
            }


            $(".datepicker").datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: new Date,
                changeMonth: true,
                changeYear: true
            });

            $('#to_date').datepicker('setDate', to_date);
            $('#from_date').datepicker('setDate', selected_from_date);

            //triggers when get data button is clicked

            $('#get_data').click(function (e) {

                e.preventDefault();
                from_date = $('#from_date').val();
                to_date = $('#to_date').val();
                if (from_date == "") {
                    from_date = "0000-00-00";
                }
                var get_data_url = "{{URL::to('dashboard/mails')}}" + "?from_date=" + from_date + "&to_date=" + to_date;
                window.location.href = get_data_url;
            });

            {{--$('.view').click(function (e) {--}}
            {{--e.preventDefault();--}}
            {{--$mail_id = $(this).attr('data-mail-id');--}}

            {{--$.fancybox.showLoading({--}}
            {{--helpers: {--}}
            {{--overlay: {closeClick: false}--}}
            {{--}--}}
            {{--});--}}
            {{--$.fancybox.helpers.overlay.open({parent: $('body'), closeClick: false});--}}

            {{--$url = "{{URL::to('dashboard/mails/info')}}" + '/' + $mail_id;--}}

            {{--$.fancybox({--}}
            {{--href: $url,--}}
            {{--type: 'ajax',--}}
            {{--autoSize : true,--}}
            {{--helpers : {--}}
            {{--overlay : {--}}
            {{--closeClick: true--}}
            {{--}--}}
            {{--},--}}
            {{--afterClose : function() {--}}
            {{--location.reload();--}}
            {{--return;--}}
            {{--},--}}
            {{--afterOpen: function() {--}}
            {{--location.reload();--}}
            {{--return;--}}
            {{--}--}}
            {{--});--}}

            {{--//            $('#order_history_div').css({'margin': '0 auto','width':'940px'});--}}
            {{--//                $('#order_history_div').css({'margin': '0 auto','width':window.innerWidth - 15});--}}
            {{--//                $('#columns>div').css('float','initial');--}}

            {{--});--}}

            $("a.iframe").fancybox({
                'type': 'iframe',
                'frameWidth': 500,
                'frameHeight': 500
            });

        });
    </script>

    <div class="row">
        <div class="col-lg-12">
            {{Notification::showSuccess()}}
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
                <li>Messages</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 form-horizontal">
            <div class="form-group">
                <label for="from_date" class="col-sm-3 control-label">From Date</label>

                <div class="col-sm-6">
                    <input type="text" class="datepicker form-control" id="from_date">
                </div>
            </div>
        </div>

        <div class="col-lg-4 form-horizontal">
            <div class="form-group">
                <label for="to_date" class="col-sm-3 control-label">To Date</label>

                <div class="col-sm-6">
                    <input type="text" class="datepicker form-control" id="to_date">
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-8 col-xs-10 button_div">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                <a href="" class="btn btn-info" id="get_data">Get Data</a>
            </div>

            {{--<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">--}}
            {{--<a href="" class="btn btn-info" id="download_csv">Download Csv</a>--}}
            {{--</div>--}}
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Messages
                </header>
                <div class="panel-body table-responsive">
                    <table class="table general-table">
                        <thead>
                        <tr>
                            <th>User Name</th>
                            <th>To EmailId</th>
                            <th>From EmailId</th>
                            <th>Subject</th>
                            <th>Created At</th>
                            {{--<th>Actions</th>--}}
                        </tr>
                        </thead>

                        <tbody>

                        @if(!empty($mails))

                            @foreach($mails as $mail)

                                <tr>
                                    <td>{{$mail->user_name or ''}}</td>
                                    <td>{{$mail->to or ''}}</td>
                                    <td>{{$mail->from or ''}}</td>
                                    <td>{{$mail->subject or ''}}</td>
                                    <td>
                                        <?php echo AppUtil::getParsedDate($mail->created_at) ?>
                                    </td>
                                    {{--<td>--}}
                                    {{--<a href="{{URL::to('dashboard/mails/info/'.$mail->id)}}"--}}
                                    {{--class="btn btn-info btn-xs view iframe"--}}
                                    {{--data-toggle="tooltip" title="View Mail" data-mail-id="{{$mail->id}}">--}}
                                    {{--<i class="fa fa-search"></i>--}}
                                    {{--</a>--}}
                                    {{--</td>--}}
                                </tr>

                            @endforeach

                        @endif

                        </tbody>

                    </table>
                </div>
            </section>
        </div>
        <div class="text-center">
            <?php if (!empty($mails)) {
                echo $mails->links();
            } ?>
        </div>
    </div>


@stop