@extends('layouts.dashboard_default')

@section('content')

<!-- page start-->
<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <ul class="breadcrumb">
                <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="{{URL::to('dashboard/documentation')}}">Documentation</a></li>
                <li><a href="{{URL::to('dashboard/documentation/categories')}}">Categories</a></li>
            </ul>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">

    @include('_partials.dashboard.vertical_tabs_documentation')

    <div class="col-lg-10">
        <!--        <h3>dff</h3>-->

        <div class="panel">
            <div class="panel-heading">Categories</div>
            <div class="panel-body">
                <div class="row justify">

                    <div class="span12 privacy_policy_div">
                        <ul class="information_use">
                            <li>To view categories go to <b>Catalog tab</b> click on <b>Categories</b>
                                <img src="{{asset('uploads/documentation/add_category.png')}}">
                            </li>
                            <li>It will display all the categories. To add new category click on <b>Add New</b>
                                <img src="{{asset('uploads/documentation/view_categories.png')}}" title="View Category">
                                <img src="{{asset('uploads/documentation/dashboard_category_create.png')}}" title="Create Category">
                            </li>

                            <li>
                                <b>Sequence :</b>  Sequence defines the order of category.One can set its position from following three categories
                                <ul>
                                    <li> <b>At Top :</b> At top will move category at top of all the categories. </li>
                                    <li> <b>At End :</b> After will move category after the category which you have selected. </li>
                                    <li> <b>After :</b> At end will move category at end of all the categories. </li>
                                </ul>
                            </li>

                            <li>
                                <b>Parent Category :</b>If you wish to create a subcategory belonging to a category, choose the category under which it will appear.
                            </li>

                            <li>
                                <b>Is LTW? :</b>Select if category has life time warranty
                            </li>

                            <li>
                                <b>Is COD? :</b>Select if category has cash on delivery
                            </li>

                            <li>
                                <b>Is Demo Available? :</b>Select if demo is available for this category
                            </li>

                            <li>
                                <b>Warranty :</b>You can define warranty of category.if category has LTW (Life Time Warranty) then You dont have to enter warranty.
                            </li>

                        </ul>

                        <ul class="information_use">
                            <li>
                                To <b>Edit</b> category click on edit button
                                <img src="{{asset('uploads/documentation/dashboard_category.png')}}" title="Add Category">
                                <img src="{{asset('uploads/documentation/dashboard_category_edit_2.png')}}" title="Category Edit">
                            </li>
                            <li><b>Active ? : </b> Active allows you to enable or disable category.Enable will display category on front-end.</li>
                            <li><b>Is delivered ? : </b> Active allows you to enable or disable category.Enable will display category on front-end.</li>

                            <li>
                                To add documents in particular category, go to <b>Documents Tab</b> and click on <b>ADD DOCUMENT</b> button.It
                                will display a form to add document.

                                <img src="{{asset('uploads/documentation/dashboard_category_documents_2.png')}}" title="Add Document">
                                <img src="{{asset('uploads/documentation/category_documents_create.png')}}" title="Add Document Form">
                            </li>

                            <li>
                                <b>Active ? :</b> Active allows you to enable or disable category document.Enable will display document on front-end.
                                <img src="{{asset('uploads/documentation/dashboard_category_documents_view.png')}}" title="Add Document View">
                            </li>

                            <li>Multiple documents can be added to category.You can edit <i class="btn btn-xs btn-info fa fa-pencil"></i> and delete <i class="btn btn-xs btn-danger fa fa-trash-o"></i>  document too.</li>

                            <li>
                                To add video in particular category go to <b>Videos Tab</b> and click on <b>ADD VIDEO</b> button.It
                                will display a form to add video.

                                <img src="{{asset('uploads/documentation/dashboard_category_videos.png')}}" title="Category Video">
                                <img src="{{asset('uploads/documentation/dashboard_category_video_create.png')}}" title="Add Video Form">
                            </li>

                            <li>
                                <b>Active ? :</b> Active allows you to enable or disable category video.Enable will display video on front-end.
                                <img src="{{asset('uploads/documentation/dashboard_category_videos_view.png')}}" title="Add Video View">
                            </li>

                            <li>Multiple video can be added to category.You can edit <i class="btn btn-xs btn-info fa fa-pencil"></i> and delete <i class="btn btn-xs btn-danger fa fa-trash-o"></i> video too.</li>

                            <li>Features which are common in all products which belongs to this category will be added in this category</li>
                            <li>
                                To add feature in particular category go to <b>Feature Tab</b> and click on <b>ADD FEATURE</b> button.It
                                will display a form to add feature.

                                <img src="{{asset('uploads/documentation/dashboard_category_attributes.png')}}" title="Category Feature">
                                <img src="{{asset('uploads/documentation/category_attributes_create_1.png')}}" title="Add Video Form">
                            </li>

                            <li><b>Is Comparable?: </b> Select this feature when you want to compare feature with another category while comparing products </li>
                            <li><b>Is Filterable?: </b> Select this feature when you want to show it in filter on category pages in front end </li>
                            <li><b>Feature value type: </b> This feature defines the datatype of feature value.For .e.g <b>No of Burners</b> is defined as integer then you have to add integer value in this feature </li>


                        </ul>
                    </div>
                </div>


            </div>
        </div>
    </div>

</div>


@stop