@extends('layouts.dashboard_default')

@section('content')

<!-- page start-->
<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/documentation')}}">Documentation</a></li>
            <li><a href="{{URL::to('dashboard/documentation/orders')}}">Orders</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">

    @include('_partials.dashboard.vertical_tabs_documentation')
    <style type="text/css">
        p{margin:0 10px 0 21px}
    </style>
    <div class="col-lg-10">
        <!--        <h3>dff</h3>-->

        <div class="panel">
            <div class="panel-heading">Orders</div>
            <div class="panel-body">
                <div class="row justify">

                    <div class="span12 privacy_policy_div">
                        <ul class="information_use">
                            <li>
                                The <b>Orders</b> tab enables you to see all of the information about all the purchases
                                from your store.
                                All of your store's transactions are available there, organized by payment status
                            </li>
                            <li>You can filter the results and easily find the orders you're looking for by using the
                                payment status above. For instance,
                                On Selecting <b>PAID</b> payment status it will display all paid orders

                                <img src="{{asset('uploads/documentation/orders/dashboard_orders.png')}}"
                                     title="Orders">
                            </li>
                            <li>
                                In order to process the orders you receive, you have to view the information they
                                contain.
                                Click on the <i class="btn btn-info btn-xs fa fa-search"></i> button to the right of the
                                order.
                            </li>

                            <li>
                                The view order will display order info page.
                                <img src="{{asset('uploads/documentation/orders/dashboard_order_info.png')}}"
                                     title="Tag Menu">
                            </li>

                            <li>
                                When ship an order you have the ability to add shipment
                                information such as shipping provider,Code and tracking URL and Notes if any.

                                <img src="{{asset('uploads/documentation/orders/dashboard_order_tracking.png')}}"
                                     title="Tag List">

                                <img src="{{asset('uploads/documentation/orders/dashboard_order_tracking_info.png')}}"
                                     title="Tag List">
                            </li>

                            <li>On clicking Update tracking data,it'll display a form in which you can update order
                                tracking info.
                            </li>
                            <li>On clicking Email tracking data,it'll mail order tracking info to admin.</li>
                            <li>You can edit shipping info with <i class="btn btn-xs btn-success fa fa-pencil"></i> button
                            </li>

                            <li>
                                Having different order statuses enables you to easily manage your orders, and keep you
                                customers informed of the evolution of their purchase. To edit order status click on <i
                                    class="btn btn-xs btn-info fa fa-pencil"></i> button.it will show following form
                                <img src="{{asset('uploads/documentation/orders/dashboard_edit_order_status.png')}}"
                                     title="Update Order Status">
                            </li>
                        </ul>
                        <p>You can choose between the following order statuses</p>
                        <ul class="information_use">
                            <li>New</li>
                            <li>Open</li>
                            <li>Closed</li>
                        </ul>
                        <p>You can choose between the following payment statuses</p>
                        <ul class="information_use">
                            <li>Unpaid</li>
                            <li>Paid</li>
                            <li>Pending</li>
                        </ul>
                        <p>and add the notes regarding order status.</p>
                        <ul class="information_use">
                            <li>
                                Administrator can change Payment status only when transaction is done by Cash On Delivery(COD) payment mode.
                            </li>
                        </ul>
                    </div>
                </div>


            </div>
        </div>
    </div>

</div>


@stop