@extends('layouts.dashboard_default')

@section('content')

<!-- page start-->
<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/documentation')}}">Documentation</a></li>
            <li><a href="{{URL::to('dashboard/documentation/products')}}">Products</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">

@include('_partials.dashboard.vertical_tabs_documentation')

<style type="text/css">
    p{margin:0 10px 0 21px}
</style>
<div class="col-lg-10">
    <!--        <h3>dff</h3>-->

    <div class="panel">
        <div class="panel-heading">Products</div>
        <div class="panel-body">
            <div class="row justify">

                <div class="span12 privacy_policy_div">
                    <ul class="information_use">
                        <li>To view products go to <b>Catalog tab</b> click on <b>Products</b>
                            <img src="{{asset('uploads/documentation/products/dashboard_products_menu.png')}}"
                                 title="Go to products">
                        </li>
                        <li>It will display all the products. To add new products click on <b>Add New</b>
                            <img src="{{asset('uploads/documentation/products/view_dashboard_products.png')}}"
                                 title="View Products">
                            <img src="{{asset('uploads/documentation/products/dashboard_products_create_1.png')}}"
                                 title="Create Product">
                        </li>

                        <li>
                            <b>Category :</b>If you wish to create a subcategory belonging to a category, choose the
                            category under which it will appear.
                        </li>

                        <li>
                            <b>Is Delivered? :</b>
                        </li>

                        <li>
                            <b>Is LTW? :</b>Select if category has life time warranty
                        </li>

                        <li>
                            <b>Is COD? :</b>Select if category has cash on delivery
                        </li>

                        <li>
                            <b>Is Demo Available? :</b>Select if demo is available for this category
                        </li>

                        <li>
                            <b>Is Available? :</b>Select if product is available or not.
                        </li>

                        <li>
                            <b>Warranty :</b>You can define warranty of product.if product has LTW (Life Time
                            Warranty) then You don't have to enter warranty.
                        </li>

                        <li>
                            <b>List Price :</b>You can list price product.
                        </li>

                        <li>
                            <b>Offer Price :</b>You can define offer price of product.
                        </li>

                        <li>
                            <b>Weight :</b>You can define weight of product.
                        </li>

                        <li>
                            <b>Sequence :</b> Sequence defines the order of product.You can set its position from
                            following three categories
                            <ul>
                                <li><b>At Top :</b> At top will move category at top of all the categories.</li>
                                <li><b>At End :</b> After will move category after the category which you have
                                    selected.
                                </li>
                                <li><b>After :</b> At end will move category at end of all the categories.</li>
                            </ul>
                        </li>

                        <li>
                            <b>Script :</b>You can define script for a particular product.
                        </li>

                        <li>
                            <b>Css :</b>You can define css for a particular product.
                        </li>
                    </ul>

                    <ul class="information_use">
                        <li>
                            To <b>Edit</b> product click on edit button
                            <img src="{{asset('uploads/documentation/products/dashboard_products.png')}}"
                                 title="Edit Product">
                            <img src="{{asset('uploads/documentation/products/dashboard_products_edit.png')}}"
                                 title="Product Edit">
                        </li>

                        <li>
                            To View Document which are added in product,click on <b>DOCUMENTS</b> tab.it will
                            display all the documents which are added in particular product.
                            <img
                                src="{{asset('uploads/documentation/products/dashboard_product_documents_view.png')}}"
                                title="Product Edit">
                        </li>

                        <li>
                            To add documents in particular product go to <b>Documents Tab</b> and click on <b>ADD
                                DOCUMENT</b> button.It will display a form to add document.

                            <!--                                <img src="{{asset('uploads/documentation/dashboard_category_documents_2.png')}}" title="Add Document">-->
                            <img
                                src="{{asset('uploads/documentation/products/dashboard_product_documents_create.png')}}"
                                title="Add Document Form">
                        </li>

                        <li>
                            <b>Active ? :</b> Active allows you to enable or disable product document.Enable will
                            display document on front-end.
                        </li>

                        <li>Multiple documents can be added to product.You can edit <i
                                class="btn btn-xs btn-info fa fa-pencil"></i>
                            and delete <i class="btn btn-xs btn-danger fa fa-trash-o"></i> document too.
                        </li>

                        <li>
                            To add video in particular product go to <b>Videos Tab</b> and click on <b>ADD VIDEO</b>
                            button.It
                            will display a form to add video.

                            <img src="{{asset('uploads/documentation/products/dashboard_product_videos.png')}}"
                                 title="Category Video">
                            <img
                                src="{{asset('uploads/documentation/products/dashboard_product_videos_create_1.png')}}"
                                title="Add Video Form">
                        </li>

                        <li>
                            <b>Active ? :</b> Active allows you to enable or disable product video.Enable will
                            display video on front-end.
                            <img
                                src="{{asset('uploads/documentation/products/dashboard_product_videos_created.png')}}"
                                title="Add Video View">
                        </li>

                        <li>Multiple video can be added to product.You can edit <i
                                class="btn btn-xs btn-info fa fa-pencil"></i> and delete <i
                                class="btn btn-xs btn-danger fa fa-trash-o"></i> video too.
                        </li>

                        <li>Features which belongs to this product's category will be shown in this feature tab.You
                            can specify its value.
                            It will display as filter on category page front end.
                            <img src="{{asset('uploads/documentation/products/dashboard_product_attributes.png')}}"
                                 title="Product Feature">
                        </li>

                        <li>
                            If you want to add feature specific to product,you can add it in <b>specific
                                feature </b> tab .To add product specific
                            feature in particular product go to <b>Feature Tab</b> and click on <b>ADD</b> button.It
                            will display a form to add product specific
                            feature.
                            <img
                                src="{{asset('uploads/documentation/products/dashboard_product_specific_attributes_create.png')}}"
                                title="Category Feature">
                            <img
                                src="{{asset('uploads/documentation/products/dashboard_product_specific_attributes_create1.png')}}"
                                title="Category Feature">
                        </li>

                        <li>Multiple specific features can be added to product.You can edit <i
                                class="btn btn-xs btn-info fa fa-pencil"></i> and delete <i
                                class="btn btn-xs btn-danger fa fa-trash-o"></i> specific feature too.
                            <img
                                src="{{asset('uploads/documentation/products/dashboard_product_specific_attributes_saved.png')}}"
                                title="Category Feature">
                        </li>

                        <li>
                            The Fifth tab, <b>Images</b>, is for including photos on your product page.
                            <img
                                src="{{asset('uploads/documentation/products/dashboard_product_images.png')}}"
                                title="Product Image">
                        </li>

                        <li>
                            To add one or more images to your product click the <b>ADD IMAGES</b> button.it will display
                            a form.Fill all the details and select a photo from your computer to upload.
                            <img
                                src="{{asset('uploads/documentation/products/dashboard_product_images_create_1.png')}}"
                                title="Product Image">
                        </li>

                        <li>
                            Write image name and image title in the <b>Name</b> and <b>Title</b> field and also write notes in <b>Notes</b>
                            field.
                        </li>

                        <li>Write a file name in the <b>Caption</b> field once you have uploaded the image.
                            This is the name that search engines will use to reference your product. You must thus give
                            it a name that corresponds to the product description. This way, your product will appear
                            with ease in search engine results.
                        </li>

                        <li>
                            Check the box that says <b>Is Primary?</b> if you want this uploaded image to be used as the
                            default image, and appear automatically on the product page of your shop.
                        </li>

                    </ul>

                    <p>Once you have uploaded several images, you can choose to modify <i class="btn btn-xs btn-info fa fa-pencil"></i>
                        and delete <i class="btn btn-xs btn-danger fa fa-trash-o"></i> the image and define a different primary
                        image by clicking on the icons under the <b>Is Primary</b> column (see above screenshot).</p><br/>

                    <ul class="information_use">
                        <li>The Sixth tab, <b>Tags</b>, is for including tag to your product.Using tag,we can group products.</li>

                        <li>To add one or more tags to your product click the <b>ADD</b> button.it will display
                            a form.Fill offer price and select tag which is defined in <b>Catalog > Tags </b>.
                            <img src="{{asset('uploads/documentation/products/dashboard_product_tags_create.png')}}"
                                title="Product Tag"></li>

                        <li>For e.g You have created Featured tag in <b>Catalog > Tags </b> and it is added in product <b>GL 462 </b> then
                        it will show in the featured products
                            <img src="{{asset('uploads/documentation/products/featured_products.png')}}"
                                 title="Product Tag"></li>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

</div>


@stop