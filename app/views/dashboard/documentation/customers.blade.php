@extends('layouts.dashboard_default')

@section('content')

<!-- page start-->
<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/documentation')}}">Documentation</a></li>
            <li><a href="{{URL::to('dashboard/documentation/customers')}}">Customers</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">

    @include('_partials.dashboard.vertical_tabs_documentation')
    <style type="text/css">
        p {
            margin: 0 10px 0 21px;
        }
    </style>
    <div class="col-lg-10">
        <!--        <h3>dff</h3>-->

        <div class="panel">
            <div class="panel-heading">Customers</div>
            <div class="panel-body">
                <div class="row justify">

                    <div class="span12 privacy_policy_div">
                        <ul class="information_use">
                            <li>
                                The <b>Customers</b> menu enables you to check on your customers' details, create groups
                                to which you can apply
                                discounts, view the current shop carts, handle customer service, etc.
                            </li>
                            <li>
                                The first page under the <b>Customers</b> menu gives you a list of all the registered
                                users on your shop.This gives you a view of your customers, with some details:
                                <img src="{{asset('uploads/documentation/customers/dashboard_customers.png')}}"
                                     title="Customers">

                            </li>
                            <li><b>Newsletters : </b>Indicates whether the customer is subscribed to your shop's
                                newsletter or not. You can subscribe it clicking on the red checkmark.
                            </li>
                            <li><b>Special Offers : </b>Indicates whether the customer has given some offer or not. You
                                can enable it clicking on the red checkmark.
                            </li>
                            <li>In the case where you would like to have more information on a given customer, you can
                                click on the <b>view</b> button, located at the end of the row in the customer's list. A
                                new
                                page appears
                                <img src="{{asset('uploads/documentation/customers/dashboard_customer_info.png')}}"
                                     title="Customer Info">
                            </li>

                            The various sections provide you with some key data on the user:<br/>
                            <li>Customer information, first and last name, e-mail address, mobile no, sign-up date, date of
                                last visit.
                            </li>
                            <li>Information regarding the subscription to the store's newsletter and subscription to ads
                                from partnering companies, the age, date of last update, and whether or not the account
                                is active.
                            </li>
                            <li>shipping address </li>
                            <li>billing address </li>
                            <li>Orders done by a customer</li>

                            <li>
                                In order to process the orders you receive, you have to view the information they
                                contain.
                                Click on the View order button to the right of the order.
                                <img src="{{asset('uploads/documentation/customers/dashboard_customers_order_info.png')}}"
                                     title="Customer Order View">
                                <img src="{{asset('uploads/documentation/customers/dashboard_customers_view_order.png')}}"
                                     title="Customer View Order ">
                            </li>

                            <li>
                                Clicking on the <b>edit</b> button brings you to a form(<b>Edit orders</b> in orders)<br/>

                                <img src="{{asset('uploads/documentation/customers/dashboard_customers_edit_order.png')}}"
                                     title="Customer Edit Order ">
                            </li>
                        </ul>
                    </div>
                </div>


            </div>
        </div>
    </div>

</div>


@stop