@extends('layouts.dashboard_default')

@section('content')

<!-- page start-->
<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/documentation')}}">Documentation</a></li>
            <li><a href="{{URL::to('dashboard/documentation/tags')}}">Tags</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">

    @include('_partials.dashboard.vertical_tabs_documentation')

    <div class="col-lg-10">
        <!--        <h3>dff</h3>-->

        <div class="panel">
            <div class="panel-heading">Tags</div>
            <div class="panel-body">
                <div class="row justify">

                    <div class="span12 privacy_policy_div">
                        <ul class="information_use">
                            <li>
                                The Tags feature enables you to associate your products with keywords.this way you can
                                group the products
                            </li>
                            <li>For e.g You have created <b>Upcoming Products</b> tag and it is added in product <b>GL
                                    9011 </b> then
                                it will show in the upcoming product block on home page of your shop.
                                <img src="{{asset('uploads/documentation/tags/upcoming_products.png')}}"
                                     title="Product Tag">
                            </li>
                            <li>
                                You can associate several tags to your product directly in the <b>Tags</b> field from the
                                moment you create a product. See the <b>Products</b> tab of this documentation
                            </li>

                            <li>
                                The best way to manage them is to go to the <b>Tags</b> sub-tab under the Catalog tab.
                                You will arrive at the list of all the tags used in your store in all languages.

                                <img src="{{asset('uploads/documentation/tags/dashboard_tag_menu.png')}}" title="Tag Menu">
                                <img src="{{asset('uploads/documentation/tags/dashboard_tags.png')}}" title="Tag List">

                            </li>

                            <li>
                                To add new category click on <b>Add New</b>.By filling all the details in this form you can create tag
                                <img src="{{asset('uploads/documentation/tags/dashboard_tags_create.png')}}" title="Tag List">
                            </li>

                            <li>You can edit <i class="btn btn-xs btn-info fa fa-pencil"></i>
                                and delete <i class="btn btn-xs btn-danger fa fa-trash-o"></i> tags too.
                            </li>

                        </ul>


                    </div>
                </div>


            </div>
        </div>
    </div>

</div>


@stop