@extends('layouts.dashboard_default')

@section('content')

<!-- page start-->
<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/documentation')}}">Documentation</a></li>
            <li><a href="{{URL::to('dashboard/documentation/service-centers')}}">Service Centers</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">

    @include('_partials.dashboard.vertical_tabs_documentation')
    <style type="text/css">
        p {
            margin: 0 10px 0 21px
        }
    </style>
    <div class="col-lg-10">
        <!--        <h3>dff</h3>-->

        <div class="panel">
            <div class="panel-heading">Demos</div>
            <div class="panel-body">
                <div class="row justify">

                    <div class="span12 privacy_policy_div">
                        <ul class="information_use">
                            <li>
                                Customers who have requested for a demo will be displayed on demos page.
                            </li>

                            <li>
                                In order to view the demos information click on the <b>Demos</b> link in header
                                <img src="{{asset('uploads/documentation/demos/dashboard_demo.png')}}"
                                     title="View Demos">
                            </li>

                            <li>To filter a date wise data Select <b>To Date</b> and <b>From Date</b> and Clicking on
                                <b>Get data</b> button give you demo list which are requested within those time period.
                                <img src="{{asset('uploads/documentation/demos/dashboard_demo_filter_by_date.png')}}"
                                     title="Demos Get Data">

                            </li>
                            <li>
                                Storing these filtered data in file is easy for you.Clicking on <b>Download CSV </b>button
                                will generate CSV file containing demo data.
                                <img src="{{asset('uploads/documentation/demos/demos_csv.png')}}"
                                     title="Demos Generate CSV">
                            </li>

                            <li>You can delete <i class="btn btn-xs btn-danger fa fa-trash-o"></i> demos too. </li>

                    </div>
                </div>

            </div>
        </div>
    </div>

</div>


@stop