@extends('layouts.dashboard_default')

@section('content')

<!-- page start-->
<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/documentation')}}">Documentation</a></li>
            <li><a href="{{URL::to('dashboard/documentation/coupons')}}">Coupons</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">

    @include('_partials.dashboard.vertical_tabs_documentation')
    <style type="text/css">
        p {
            margin: 0 10px 0 21px;
        }
    </style>
    <div class="col-lg-10">
        <!--        <h3>dff</h3>-->

        <div class="panel">
            <div class="panel-heading">Coupons</div>
            <div class="panel-body">
                <div class="row justify">

                    <div class="span12 privacy_policy_div">
                        <p>
                            Coupons play an important role in your daily relationships with your customers. Typically,
                            customers like two things when shopping:
                        </p>
                        <ul class="information_use">
                            <li>Feeling special, unique.</li>
                            <li>Getting good prices.</li>
                        </ul>
                        <p>
                            Both can be achieved with personalized discounts, and this is precisely where coupon come
                            into play – or more precisely its Coupon price rule.
                        </p>
                        <ul class="information_use">
                            <li>
                                Coupon price rules enables you to assign price reductions by category. As its name
                                implies, this type of rules applies to a range of products; it cannot be used for a
                                single product.
                            </li>
                            <li>
                                For instance, you can set a rule that would say that for customer would get 10% off on
                                your <b>Induction Cooker</b> for the first week of July.
                            </li>

                            <li>In the case where you would like to have more information on a given coupons, you can
                                click on the <b>Coupon</b> button in header .This gives you a view of coupons, with some
                                details:
                                <img src="{{asset('uploads/documentation/coupons/dashboard_main_coupon_view.png')}}"
                                     title="Coupon List">
                            </li>

                            <li>
                                <b>Coupon Categories > View :</b> it shows category list on which coupon code shall be applied
                                <img
                                    src="{{asset('uploads/documentation/coupons/dashboard_main_coupon_categories.png')}}"
                                    title="View Coupon Categories">
                            </li>

                            <li>
                                <b>Actions > View Coupon Codes:</b> it shows coupon code list which shall be used by customers
                                <img
                                    src="{{asset('uploads/documentation/coupons/dashboard_main_coupon_codes.png')}}"
                                    title="View Coupon Codes">
                            </li>

                            <li>
                                The creation form page enabling you to precisely build new rules.
                                <img src="{{asset('uploads/documentation/coupons/dashboard_main_coupon_create.png')}}"
                                     title="Create Coupon">
                            </li>

                            <li><b>Name : </b> define coupon name.</li>
                            <li><b>Percentage : </b> The value of the discount. Depending on the value "10% off".
                            </li>
                            <li><b>Minimum Value : </b> Defines the cart minimum value.To apply coupon minimum cart
                                value is required
                            </li>
                            <li><b>Expiry : </b> The rule applies in this time frame.</li>
                            <li><b>Is Unique ? : </b></li>
                            <li><b>No of coupons to be generated? : </b> How many Coupon is to be generated</li>
                            <li><b>Categories: </b> to which category coupon should be applied</li>

                        </ul>
                    </div>
                </div>


            </div>
        </div>
    </div>

</div>


@stop