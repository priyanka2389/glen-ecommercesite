@extends('layouts.dashboard_default')

@section('content')
<!-- page start-->
<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/products')}}">Products</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div>

</div>

<div class="row">
    @include('_partials.dashboard.vertical_tabs_documentation')
</div>

@stop