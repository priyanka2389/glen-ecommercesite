@extends('layouts.dashboard_default')

@section('content')

<!-- page start-->
<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/documentation')}}">Documentation</a></li>
            <li><a href="{{URL::to('dashboard/documentation/dealers')}}">Dealers</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">

    @include('_partials.dashboard.vertical_tabs_documentation')
    <style type="text/css">
        p {
            margin: 0 10px 0 21px
        }
    </style>
    <div class="col-lg-10">
        <!--        <h3>dff</h3>-->

        <div class="panel">
            <div class="panel-heading">Dealers</div>
            <div class="panel-body">
                <div class="row justify">

                    <div class="span12 privacy_policy_div">
                        <ul class="information_use">
                            <li>
                                Dealer buys product from glen and sell to customers.Glen has given dealership in all
                                over India.
                                The <b>Dealers</b> tab enables you to see all of the information about dealers from your
                                store.

                            </li>

                            <li>
                                In order to view the dealer information click on the <b>Dealer link</b> in header
                                <img src="{{asset('uploads/documentation/dealers/dashboard_dealers.png')}}"
                                     title="View Dealers">
                            </li>

                            <li><b>Small Appliances :</b> indicates whether dealer provides small appliances or not.You can disable it by clicking on <i class="fa fa-check green"></i> check mark.</li>
                            <li><b>Large Appliances :</b> indicates whether dealer provides large appliances or not.You can disable it by clicking on <i class="fa fa-check green"></i> check mark.</li>
                            <li><b>Active :</b>indicates whether dealer displays on your shop or not.You can disable it by clicking on <i class="fa fa-check green"></i> check mark.</li>
                            <li>
                                Clicking on the <b>Add Dealer</b> button brings you to a form:
                                <img src="{{asset('uploads/documentation/dealers/dashboard_dealers_create.png')}}"
                                     title="Add Dealer">
                            </li>

                            <li><b>Shop Name: </b> it indicates dealer's shop name </li>
                            <li><b>Address :</b> it indicates dealer's shop address </li>
                            <li><b>City :</b> it indicates dealer's shop city </li>
                            <li><b>State :</b> it indicates dealer's shop state </li>
                            <li><b>Pincode :</b> it indicates dealer's shop pincode </li>
                            <li><b>Mobile :</b> it indicates dealer's mobile number</li>
                            <li><b>Phone :</b> it indicates dealer's phone number </li>
                            <li>
                                <b>Sequence :</b> Sequence defines the order of dealer.You can set its position from
                                following three categories
                                <ul>
                                    <li><b>At Top :</b> At top will move dealer at top of all the categories.</li>
                                    <li><b>At End :</b> After will move dealer after the category which you have
                                        selected.
                                    </li>
                                    <li><b>After :</b> At end will move dealer at end of all the categories.</li>
                                </ul>
                            </li>
                            <li><b>Is small Appliances? :</b> Select Yes if dealer provides small appliances. </li>
                            <li><b>Is large Appliances? :</b> Select Yes if dealer provides large appliances. </li>
                            <li><b>Is Active :</b> Select Yes if you have to show dealer on your shop otherwise select No. </li>

                        </ul>

                        <ul class="information_use">
                            <li>You can edit <i class="btn btn-xs btn-info fa fa-pencil"></i>
                                and delete <i class="btn btn-xs btn-danger fa fa-trash-o"></i> dealers too.
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>


@stop