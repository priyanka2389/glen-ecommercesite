@extends('layouts.dashboard_default')

@section('content')

    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">
    <script type="text/javascript"
            src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_overlay.png">
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite.png">
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite@2x.png">

    <script type="text/javascript">
        $(document).ready(function () {

            $('.view_categories').click(function (e) {
                e.preventDefault();
                $coupon_id = $(this).attr('data-coupon-id');

                $.fancybox.showLoading({
                    helpers: {
                        overlay: {closeClick: false}
                    }
                });
                $.fancybox.helpers.overlay.open({parent: $('body'), closeClick: false});

                $url = "{{URL::to('dashboard/main-coupon/main-coupon-categories')}}" + '/' + $coupon_id;
                console.log($url);

                $.fancybox({
                    href: $url,
                    type: 'ajax',
                    autoSize: true,
                    helpers: {
                        overlay: {closeClick: false}
                    }
                });

            });

        });
    </script>


    <div class="row">
        <div class="col-lg-12"></div>
        {{ Notification::showSuccess() }}
    </div>

    <!-- page start-->
    <div class="row">
        <div class="col-lg-10">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="{{URL::to('dashboard/main-coupon')}}"><i class="fa fa-home"></i> Home</a></li>
                <li>Main Coupons</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
        <div class="col-lg-2">
            <a href="{{URL::to('dashboard/main-coupon/create')}}" class="btn btn-sm btn-primary"><i
                        class="fa fa-plus-circle"></i> Add New</a>
        </div>


    </div>

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Coupons List
                </header>
                <div class="panel-body table-responsive">
                    <table class="table  table-hover general-table">
                        <thead>
                        <tr>

                            <th>Name</th>
                            <th>Discount Percent</th>
                            <th>Min Value for Discount</th>
                            <th>Expiry</th>
                            <th>Is Unique</th>
                            <th>Created At</th>
                            <th>Coupon Categories</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>


                        @if(!is_null($main_coupons))

                            @foreach($main_coupons as $coupon)


                                <tr class="">

                                    <td>{{$coupon->name}}</td>
                                    <td>{{$coupon->percentage}}%</td>
                                    <td>{{$coupon->min_value}}</td>
                                    <td>{{$coupon->expiry_date}}</td>
                                    <td>
                                        @if($coupon->is_unique==1)
                                            Yes
                                        @else
                                            No
                                        @endif
                                    </td>
                                    <td>
                                        <?php $created_at = $coupon->created_at;
                                        echo date("d M Y", strtotime($created_at));
                                        ?>
                                    </td>
                                    <td><a href="" class="view_categories" data-coupon-id="{{$coupon->id}}">View</a>
                                    </td>
                                    <td>
                                        <a href="{{URL::to('dashboard/main-coupon/codes/'.$coupon->id)}}"
                                           class="btn btn-info btn-xs" data-toggle="tooltip" title="View Coupon Codes">
                                            <i class="fa fa-search"></i>
                                        </a>
                                        <a href="{{URL::to('dashboard/main-coupon/download-csv/'.$coupon->id)}}"
                                           class="btn btn-success btn-xs" data-toggle="tooltip"
                                           title="Download Coupon Codes">
                                            <i class="fa fa-download"></i>
                                        </a>
                                        {{--<a href="{{URL::to('dashboard/main-coupon/codes/'.$coupon->id)}}">View Coupon Codes</a>--}}
                                    </td>

                                </tr>

                            @endforeach


                        @endif

                        </tbody>

                    </table>
                </div>
            </section>

        </div>
        <div class="text-center">
            <?php if (!empty($main_coupons)) {
                echo $main_coupons->links();
            } ?>
        </div>

    </div>


@stop