@extends('layouts.dashboard_default')

@section('content')


<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $("#form").validate();

        $(".expiry_date").datepicker({
            dateFormat: "yy-mm-dd",
            minDate: new Date,
            changeMonth: true,
            changeYear: true
        });
    });
</script>

<style type="text/css">
    .categories_div label.error {
        position: absolute;
        top: -20px;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        {{ Notification::showSuccess() }}
        {{ Notification::showError() }}
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/maon-coupons')}}">Coupons</a></li>
            <li>Tags</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="panel">
            <div class="panel-heading">
                Add Coupon
            </div>
            <div class="panel-body">

                <div class="form">
                    <form action="{{URL::to('dashboard/main-coupon/edit')}}"
                          class="cmxform form-horizontal" id="form" method="post" enctype="multipart/form-data">


                        <div class="form-group ">
                            <label for="name" class="control-label col-lg-3">Name *</label>

                            <div class="col-lg-6">
                                <input name="name" type="text" class="required form-control"
                                       id="name"
                                       value="{{$main_coupon->name}}"/>
                                <span class="error">{{$errors->first('name')}}</span>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="percentage" class="control-label col-lg-3">Percentage*</label>

                            <div class="col-lg-6">
                                <input name="percentage" type="text" class="required number form-control"
                                       id="percentage"
                                       value="{{$main_coupon->percentage}}"/>
                                <span class="error">{{$errors->first('percentage')}}</span>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="min_value" class="control-label col-lg-3">Minimum Value*</label>

                            <div class="col-lg-6">
                                <input name="min_value" type="text" class="required number form-control"
                                       id="min_value"
                                       value="{{$main_coupon->min_value}}"/>
                                <span class="error">{{$errors->first('min_value')}}</span>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="expiry_date" class="control-label col-lg-3">Expiry*</label>

                            <div class="col-lg-6">
                                <input name="expiry_date" type="text" class="required form-control expiry_date"
                                       id="expiry_date"
                                       value="{{$main_coupon->expiry_date}}" readonly/>
                                <span class="error">{{$errors->first('expiry_date')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="unique" class="control-label col-lg-3">Is Unique ?* </label>

                            <?php $is_unique = $main_coupon->is_unique; ?>
                            <div class="col-lg-3">
                                <div class="radio">
                                    <input name="unique" type="radio" value="1" id="unique" class="required"
                                    @if(isset($is_unique)&& $is_unique==1) checked @endif/>Yes
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="radio">
                                    <input name="unique" type="radio" value="0" id="unique" class="required"
                                    @if(isset($is_unique)&& $is_unique==0) checked @endif/>Yes
                                </div>
                            </div>
                            <span class="error">{{$errors->first('unique')}}</span>
                        </div>

                        <div class="form-group ">
                            <label for="coupon_qty" class="control-label col-lg-3">No of coupons to be
                                generated?*</label>

                            <div class="col-lg-6">
                                <input name="coupon_qty" type="text" class="required digits form-control"
                                       id="coupon_qty"
                                       value="{{Input::old('coupon_qty')}}"/>
                                <span class="error">{{$errors->first('coupon_qty')}}</span>
                            </div>
                        </div>

                        <div class="form-group">

                            <label for="category_id" class="control-label col-lg-3">Select the <b>Categories</b> given
                                below:</label>

                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-12 categories_div">
                            @if(!is_null($categories))
                            @foreach($categories as $category)
                            <div class="col-lg-3">
                                <div class="checkbox">
                                    <input name="category_id[]"
                                           type="checkbox" value="{{$category->id}}" class="required"/>{{$category->name}}
                                </div>
                            </div>
                            @endforeach
                            @endif

                            <span class="error">{{$errors->first('category_id')}}</span>
                        </div>


                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                                <button class="btn btn-success" type="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>


@stop