<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">


    <title></title>


    <!--Core CSS -->
    {{HTML::style('backoffice/css/bootstrap.min.css')}}
    {{HTML::style('backoffice/css/bootstrap-reset.css')}}
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"/>

    <!-- Custom styles for this template -->
    {{HTML::style('backoffice/css/style.css')}}
    {{HTML::style('backoffice/css/style-responsive.css')}}
    {{HTML::style('backoffice/css/custom.css')}}
    {{HTML::style('frontoffice/css/rupee.css')}}


<!--    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.0.min.js"></script>-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h4>Coupon applicable on below categories:</h4>
            <table class="table table-stripped table-hover">
                <thead>

                <tr>
                    <th>#</th>
                    <th>Category</th>
                </tr>

                </thead>

                <tbody>


                @foreach($categories as $i=>$category)

                <tr>
                    <td>{{$i+1}}</td>
                    <td>{{$category->name}}</td>
                </tr>

                @endforeach

                </tbody>

            </table>
        </div>
    </div>
</div>

</body>
</html>