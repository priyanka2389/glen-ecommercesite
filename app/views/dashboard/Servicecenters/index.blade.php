@extends('layouts.dashboard_default')

@section('content')

<script type="text/javascript">
    $(document).ready(function () {

        $('.delete').click(function (e) {

            e.preventDefault();
            var url = $(this).attr("href");
            var delete_dealer = confirm("Are you sure you want to delete this Service Center?");
            if (delete_dealer == true) {
                window.location.href = url;
            } else {
                return false;
            }
        });

    });
</script>

<div class="row">
    <div class="col-lg-12"></div>
    {{ Notification::showSuccess() }}
</div>

<!-- page start-->
<div class="row">
    <div class="col-lg-10">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li>Service Centers</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
    <div class="col-lg-2">
        <a href="{{URL::to('dashboard/service-centers/create')}}" class="btn btn-primary btn-sm">
            <i class="fa fa-plus-circle"></i> Add Service Center</a>
    </div>
</div>

<div class="row">

    <!--    user info starts here-->
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Service Centers
            </header>
            <div class="panel-body table-responsive">
                <table class="table  table-hover general-table">
                    <thead>
                    <tr>

                        <th>Shop Name</th>
                        <th>Address 1</th>
                        <!--                        <th>Address 2</th>-->
                        <th>City</th>
                        <th>State</th>
                        <th>Pincode</th>
                        <th>Mobile</th>
                        <th>Phone</th>
                        <th>Sequence</th>
                        <th>Small Appliance</th>
                        <th>Large Appliance</th>
                        <th>Active</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>

                    @if(!is_null($servicecenters))

                    @foreach($servicecenters as $i=>$servicecenter)

                    <?php $id = $servicecenter->id; ?>

                    <tr>

                        <td>{{$servicecenter->name}}</td>
                        <td>{{$servicecenter->address1 or '-'}}</td>
                        <td>{{$servicecenter->city}}</td>
                        <td>{{$servicecenter->state }}</td>
                        <td>{{$servicecenter->pincode}}</td>
                        <td>{{$servicecenter->mobile}}</td>
                        <td>@if(isset($servicecenter->phone)) {{$servicecenter->phone}} @else - @endif</td>
                        <td>{{$servicecenter->sequence}}</td>
                        <td>
                            @if($servicecenter->is_small_appliance)
                            <a href="{{URL::to('dashboard/service-centers/update-appliance-type/'.$id.'/small/0')}}">
                                <i class="fa fa-check green"></i>
                            </a>
                            @else
                            <a href="{{URL::to('dashboard/service-centers/update-appliance-type/'.$id.'/small/1')}}">
                                <i class="fa fa-times red"></i>
                            </a>
                            @endif
                        </td>
                        <td>
                            @if($servicecenter->is_large_appliance)
                            <a href="{{URL::to('dashboard/service-centers/update-appliance-type/'.$id.'/large/0')}}">
                                <i class="fa fa-check green"></i>
                            </a>
                            @else
                            <a href="{{URL::to('dashboard/service-centers/update-appliance-type/'.$id.'/large/1')}}">
                                <i class="fa fa-times red"></i>
                            </a>
                            @endif
                        </td>
                        <td>
                            @if($servicecenter->is_active)
                            <a href="{{URL::to('dashboard/service-centers/activate-or-deactivate/'.$id.'/0')}}">
                                <i class="fa fa-check green"></i>
                            </a>
                            @else
                            <a href="{{URL::to('dashboard/service-centers/activate-or-deactivate/'.$id.'/1')}}">
                                <i class="fa fa-times red"></i>
                            </a>
                            @endif
                        </td>
                        <td><?php echo AppUtil::getParsedDate($servicecenter->created_at); ?></td>
                        <td>
                            <a href="{{URL::to('dashboard/service-centers/edit/'.$id)}}"
                               class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
                            <a href="{{URL::to('dashboard/service-centers/delete/'.$id)}}"
                               class="btn btn-xs btn-danger delete"><i class="fa fa-trash-o"></i></a>
                        </td>
                        <td>

                        </td>
                    </tr>

                    @endforeach

                    @endif

                    </tbody>

                </table>
            </div>
        </section>

    </div>
    <!--user info ends here-->

    @if(isset($servicecenters))
    <div class="text-center">
        <?php echo $servicecenters->links(); ?>
    </div>
    @endif

</div>


@stop