@extends('layouts.dashboard_default')

@section('content')


    <div class="row">
        <div class="col-lg-12">
            {{Notification::showSuccess()}}
        </div>
    </div>


    <div class="row">
        <div class="col-lg-10">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
                <li>Page</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
        <div class="col-lg-2">
            <a href="{{URL::to('dashboard/pages/create')}}" class="btn btn-primary btn-sm"><i
                        class="fa fa-plus-circle"></i> Add New</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Pages
                </header>
                <div class="panel-body table-responsive">
                    <table class="table general-table">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Short code</th>
                            <th>Active</th>
                            <th>Created At</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        @if(!is_null($pages))

                            @foreach($pages as $page)
                                <tr>
                                    <td>{{$page->id}}</td>
                                    <td>{{$page->title}}</td>
                                    <td>{{$page->short_code}}</td>
                                    <td>
                                        @if($page->is_active==1)
                                            <a href="{{URL::to('dashboard/pages/activate-or-deactivate/'.$page->id.'/0')}}">
                                                <i class="fa fa-check green"></i>
                                            </a>
                                        @else
                                            <a href="{{URL::to('dashboard/pages/activate-or-deactivate/'.$page->id.'/1')}}">
                                                <i class="fa fa-times red"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td>{{$page->created_at}}</td>

                                    <td>
                                        <a href="{{URL::to('dashboard/pages/edit/'.$page->id)}}"
                                           class="btn btn-info btn-xs" title="Edit"><i
                                                    class="fa fa-pencil"></i></a>

                                        <a href="{{URL::to('dashboard/pages/delete/'.$page->id)}}"
                                           class="btn btn-xs btn-danger delete"><i class="fa fa-trash-o"></i></a>
                                    </td>

                                </tr>


                            @endforeach

                        @else
                            <tr><td colspan="6" align="center">No result found</td></tr>
                        @endif

                        </tbody>

                    </table>
                </div>
            </section>
        </div>

    </div>


@stop