@extends('layouts.dashboard_default')

@section('content')

    {{HTML::style('backoffice/css/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}
    {{HTML::script('backoffice/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}
    {{HTML::script('backoffice/js/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}

    <script type="text/javascript">
        $(document).ready(function () {

            $('#form').validate({ignore: ":hidden:not(select)"});

//            $('.source_code').wysihtml5({
//                "image": false,
//                "html":true
//            });

            $('#sequence,#category').chosen();

            //todo::remove this before commit
            $('#name').blur(function () {
                var name = $(this).val();
                $('#shortcode').val(name.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-'));
                $('#code').val(name.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-'));
                return false;
            });


        });

    </script>
    <div class="row">

        <div class="col-lg-12">

            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
                <li><a href="{{URL::to('dashboard/pages')}}">Page</a></li>
                <li>Edit</li>
            </ul>
            <!--breadcrumbs end -->

        </div>

    </div>

    <div class="row">

        <div class="col-lg-10">

            <h3>{{$page->title}}</h3>

            <div class="panel">

                <div class="panel-heading">
                    Edit
                </div>

                <div class="panel-body">


                    <div class="form">
                        <form action="{{URL::to('dashboard/pages/edit/'.$page->id)}}" class="cmxform form-horizontal"
                              id="form"
                              method="post">

                            <div class="form-group ">
                                <label for="name" class="control-label col-lg-3">Title *</label>

                                <div class="col-lg-7">
                                    <input name="title" type="text" class="required form-control" id="title"
                                           value="{{$page->title}}"/>
                                    <span class="error">{{$errors->first('title')}}</span>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="shortcode" class="control-label col-lg-3">Shortcode *</label>

                                <div class="col-lg-7">
                                    <input name="short_code" type="text" class="required form-control" id="short_code"
                                           value="{{$page->short_code}}"/>
                                    <span class="error">{{$errors->first('short_code')}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="source_code" class="control-label col-lg-3">Source Code* (Please enter your page source code(except header and footer) here)</label>

                                <div class="col-lg-7">
                                    <textarea class="required source_code form-control" name="source_code"
                                              id="source_code" rows="8">{{$page->source_code}}</textarea>
                                    <span class="error">{{$errors->first('source_code')}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php $active = $page->is_active; ?>
                                <label for="active" class="control-label col-lg-3">Active ? *</label>

                                <div class="col-lg-3">
                                    <div class="radio">
                                        <input type="radio" class="required" name="active" value="1"
                                        @if(isset($active)&& $active==1) checked @endif/>Yes
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="radio">
                                        <input type="radio" class="required" name="active" value="0"
                                        @if(isset($active)&& $active==0) checked @endif/>No
                                    </div>
                                </div>
                                <span class="error">{{$errors->first('active')}}</span>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-default" type="button">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>


                </div>

            </div>

        </div>

    </div>


    </div>


@stop