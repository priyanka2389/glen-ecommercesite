@extends('layouts.dashboard_default')

@section('content')

    {{HTML::style('backoffice/css/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}
    {{HTML::script('backoffice/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}
    {{HTML::script('backoffice/js/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}

    <script type="text/javascript">
        $(document).ready(function () {

//            $('.source_code').wysihtml5({
//                "image": false,
//                "html": true
//            });

            $('#form').validate({ignore: ":hidden:not(select)"});
            $('#category,#sequence').chosen();

            $('#title').blur(function () {
                var title = $(this).val();
                $('#short_code').val(title.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-'));
                return false;
            });

        });
    </script>


    <!-- page start-->
    <div class="row">
        <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="{{URL::to('dashboard/pages')}}">Pages</a></li>
                <li>Add New</li>
            </ul>
            <!--breadcrumbs end -->
        </div>

    </div>


    <div class="row">

        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
                    <div class="form">
                        <form action="{{URL::to('dashboard/pages/create')}}" class="cmxform form-horizontal"
                              id="form" method="post">
                            <div class="form-group ">
                                <label for="name" class="control-label col-lg-3">Title *</label>

                                <div class="col-lg-6">
                                    <input name="title" type="text" class="required form-control" id="title"
                                           value="{{Input::old('title')}}"/>
                                    <span class="error">{{$errors->first('title')}}</span>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="short_code" class="control-label col-lg-3">Shortcode *</label>

                                <div class="col-lg-6">
                                    <input name="short_code" type="text" class="form-control" id="short_code"
                                           value="{{Input::old('short_code')}}"/>
                                    <span class="error">{{$errors->first('short_code')}}</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="description_secondary" class="control-label col-lg-3">Source Code * (Please enter your page source code(except header and footer) here)</label>

                                <div class="col-lg-6">
        <textarea name="source_code" class="source_code form-control" id="source_code"
                  rows="8">{{Input::old('source_code')}}</textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="active" class="control-label col-lg-3">Active ? *</label>

                                <div class="col-lg-3">
                                    <div class="radio">
                                        <input name="active" type="radio" class="required" value="1"/>Yes
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="radio">
                                        <input name="active" type="radio" class="required" value="0"/>No
                                    </div>
                                </div>
                                <span class="error">{{$errors->first('active')}}</span>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-default" type="button">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>


@stop