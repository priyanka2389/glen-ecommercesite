@extends('layouts.dashboard_default')

@section('content')

    <script src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">

    {{HTML::style('backoffice/css/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}
    {{HTML::script('backoffice/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}
    {{HTML::script('backoffice/js/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}


    <script type="text/javascript">
        $(document).ready(function () {
            $('#description').wysihtml5({
                "image": false
            });

            $('#form').validate();
            $("#start_date,#end_date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

            $('#name').blur(function () {
                var name = $(this).val();
                $('#shortcode').val(name.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-'));
                $('#code').val(name.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-'));
                return false;
            });


        });

    </script>

    <div class="row">
        <div class="col-lg-12">
            {{Notification::showSuccess()}}
            {{Notification::showError()}}
        </div>
    </div>

    <div class="row">

        <div class="col-lg-12">

            <!--breadcrumbs start -->
            <ul class="breadcrumb edit">
                <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
                <li><a href="{{URL::to('dashboard/combo')}}">Combo</a></li>
                <li>Edit</a></li>
            </ul>
            <!--breadcrumbs end -->

        </div>

    </div>

    <div class="row">

        <!--side tab starts here-->
        <div class="col-lg-2">
            <ul class="nav nav-tabs tabs-left">
                <li class="active">
                    <a href="{{URL::to('dashboard/combo/edit/'.$combo->id)}}">Combo Info</a>
                </li>
                <li class="">
                    <a href="{{URL::to('dashboard/combo-products/'.$combo->id)}}">Combo Products</a>
                </li>
                <li class="">
                    <a href="{{URL::to('dashboard/combo-images/'.$combo->id)}}">Images</a>
                </li>
            </ul>
        </div>
        <!--side tabs ends here-->


        <div class="col-lg-10">

            <div class="panel">


                <div class="panel-body">

                    <div class="form">

                        <h4>Combo Info</h4>
                        <hr/>
                        <form class="cmxform form-horizontal " id="form" method="post"
                              action="{{URL::to('dashboard/combo/edit/'.$combo->id)}}">

                            <div class="form-group ">
                                <label for="name" class="control-label col-lg-3">Name *</label>

                                <div class="col-lg-6">
                                    <input name="name" type="text" class="required form-control" id="name"
                                           value="{{$combo->name or ''}}"/>
                                    <span class="error">{{$errors->first('name')}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="code" class="control-label col-lg-3">Code *</label>

                                <div class="col-lg-7">
                                    <input name="code" type="text" class="required form-control" id="code"
                                           value="{{$combo->code or ''}}"/>
                                    <span class="error">{{$errors->first('code')}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="shortcode" class="control-label col-lg-3">Shortcode *</label>

                                <div class="col-lg-7">
                                    <input name="shortcode" type="text" class="required form-control" id="shortcode"
                                           value="{{$combo->shortcode or ''}}"/>
                                    <span class="error">{{$errors->first('shortcode')}}</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="description" class="control-label col-lg-3">Description *</label>

                                <div class="col-lg-7">
                                <textarea class="required form-control" name="description" id="description" rows="6">{{$combo->description
                                    or ''}}</textarea>
                                    <span class="error">{{$errors->first('description')}}</span>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="name" class="control-label col-lg-3">Type</label>

                                <div class="col-lg-6">
                                    <input name="type" type="text" class="form-control" id="type"
                                           value="{{$combo->type or ''}}"/>
                                    <span class="error">{{$errors->first('type')}}</span>
                                </div>

                            </div>

                            <?php $is_cod = $combo->is_cod; ?>
                            <div class="form-group">
                                <label for="cod" class="control-label col-lg-3">Is Cod ? </label>

                                <div class="col-lg-3">
                                    <div class="radio">
                                        <input name="cod" type="radio" value="1"
                                        @if(isset($is_cod)&& $is_cod==1) checked @endif/>Yes
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="radio">
                                        <input name="cod" type="radio" value="0"
                                        @if(isset($is_cod)&& $is_cod==0) checked @endif/>No
                                    </div>
                                </div>
                                <span class="error">{{$errors->first('cod')}}</span>
                            </div>

                            <?php $is_active = $combo->is_active; ?>
                            <div class="form-group">
                                <label for="cod" class="control-label col-lg-3">Is Active ? </label>

                                <div class="col-lg-3">
                                    <div class="radio">
                                        <input name="is_active" type="radio" value="1"
                                        @if(isset($is_active)&& $is_active==1) checked @endif/>Yes
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="radio">
                                        <input name="is_active" type="radio" value="0"
                                        @if(isset($is_active)&& $is_active==0) checked @endif/>No
                                    </div>
                                </div>
                                <span class="error">{{$errors->first('is_active')}}</span>
                            </div>

                            <div class="form-group ">
                                <label for="combo_price" class="control-label col-lg-3">Combo Price *</label>

                                <div class="col-lg-6">
                                    <input name="combo_price" type="text" class="form-control required number" id="type"
                                           value="{{$combo->combo_price or ''}}"/>
                                    <span class="error">{{$errors->first('combo_price')}}</span>
                                </div>

                            </div>

                            <div class="form-group ">
                                <label for="start_date" class="control-label col-lg-3">Start Date</label>

                                <div class="col-lg-2">
                                    <input name="start_date" type="text" class="date form-control" id="start_date"
                                           value="{{date('Y-m-d',strtotime($combo->start_date)) }}"/>
                                </div>

                                <label for="end_date" class="control-label col-lg-2">End Date</label>

                                <div class="col-lg-2">
                                    <input name="end_date" type="text" class="date form-control " id="end_date"
                                           value="{{date('Y-m-d',strtotime($combo->end_date)) }}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-5 col-lg-6">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </form>


                    </div>


                </div>

            </div>

        </div>

    </div>


    </div>


@stop