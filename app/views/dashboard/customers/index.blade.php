@extends('layouts.dashboard_default')

@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var from_date;
            var to_date;

            var selected_from_date = $.url().param("from_date"); // parse the current page URL
            var selected_to_date = $.url().param("to_date"); // parse the current page URL

            //isUndefined is underscore js function
            if (!_.isUndefined(selected_from_date)) {
                from_date = selected_from_date;
            } else {
                from_date = -7;
            }

            if (!_.isUndefined(selected_to_date)) {
                to_date = selected_to_date;
            } else {
                to_date = 'today';
            }


            $(".datepicker").datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: new Date,
                changeMonth: true,
                changeYear: true,
                onSelect: function (selected, evnt) {
                    if ($('#to_date').val() < $('#from_date').val()) {
                        $('.alert-danger').html('<strong>To Date</strong> must be greater than <strong>From Date</strong>.').removeClass('hidden');
                    } else {
                        $('.alert-danger').addClass('hidden');
                    }
                }
            });

            var selectedFromDate = '<?php //echo $from_date; ?>';
            var selectedToDate = '<?php //echo $to_date; ?>';
            if (selectedFromDate && selectedToDate) {
                $('#from_date').datepicker('setDate', selectedFromDate);
                $('#to_date').datepicker('setDate', selectedToDate);
            } else {
                $('#to_date').datepicker('setDate', to_date);
                $('#from_date').datepicker('setDate', from_date);

            }

            //triggers when download csv button is clicked
            $('#download_csv').click(function (e) {

                e.preventDefault();
                from_date = $('#from_date').val();
                to_date = $('#to_date').val();
                if (from_date == "") {
                    from_date = "0000-00-00";
                }
                var newsletters = $("input[name=newsletters]:checked").val();
                var special_offers = $("input[name=special_offers]:checked").val();

                var download_csv_url = "{{URL::to('dashboard/customers/download-csv')}}" + "?from_date=" + from_date + "&to_date=" + to_date + '&newsletters=' + newsletters + '&special_offers=' + special_offers;
                window.location.href = download_csv_url;
            });

            //triggers when get data button is clicked

            $("[data-toggle='tooltip']").tooltip();
        });

    </script>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger hidden" role="alert"></div>
            {{ Notification::showSuccess() }}
            {{ Notification::showError() }}
        </div>
    </div>


    <!-- page start-->
    <div class="row">
        <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
                <li>Customers</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel filters">
                <header class="panel-heading">
                    <i class="fa fa-filter"></i> Filters
                </header>
                <div class="panel-body table-responsive">
                    <form name="form" class="form form-group " id="paymentForm" method="post"
                          action="{{URL::to('dashboard/customers')}}">
                        <div class="col-lg-12">
                            <div class="col-lg-4 form-horizontal">
                                <div class="form-group">
                                    <label for="from_date" class="col-sm-3 control-label">From Date:</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="datepicker form-control" id="from_date"
                                               name="from_date">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 form-horizontal">
                                <div class="form-group">
                                    <label for="to_date" class="col-sm-3 control-label">To Date:</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="datepicker form-control" id="to_date" name="to_date">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-12">
                            <div class="col-lg-3 form-horizontal" style="margin:0 89px 0 -53px;">
                                <div class="form-group">
                                    <label for="status" class="col-sm-6 control-label">Newsletters:</label>
                                    <?php $i = 0; ?>
                                    @foreach($newsletters as $key=>$newsletter)
                                        <div class="col-sm-3">
                                            <div class="radio">
                                                <?php
                                                (isset($selected_newsletter) && $newsletter == $selected_newsletter) ? $checked = 'checked' : $checked = '';
                                                ?>
                                                <input type="radio" name="newsletters" class="newsletters"
                                                       value="{{$newsletter}}" {{$checked}} >{{ucfirst($key)}}
                                            </div>
                                        </div>
                                        <?php $i++; ?>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-lg-4 form-horizontal">

                                <div class="form-group">
                                    <label for="status" class="col-sm-5 control-label">Special Offers:</label>

                                    <?php $i = 0; ?>
                                    @foreach($special_offers as $key=>$special_offer)
                                        @if($i % 2 == 0)
                                            @if($i!=0)
                                                <label for="status" class="col-sm-5 control-label"></label>
                                            @endif
                                            <div class="col-lg-7" style="margin-left:-18px;">
                                                @endif
                                                <div class="col-lg-6">
                                                    <div class="radio">
                                                        <?php
                                                        (isset($selected_special_offers) && $special_offer == $selected_special_offers) ? $checked = 'checked' : $checked = '';
                                                        ?>
                                                        <input type="radio" name="special_offers"
                                                               class="special_offers"
                                                               value="{{($special_offer)}}" {{$checked}} >{{ucfirst($key)}}
                                                    </div>
                                                </div>
                                                @if($i % 2 == 1)
                                            </div>
                                        @endif
                                        <?php $i++; ?>
                                    @endforeach
                                </div>

                            </div>

                        </div>

                        <div class="col-lg-12">
                            <div class="col-lg-3 col-md-6 col-sm-8 col-xs-10 button_div pull-right">
                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                                    {{--<a href="" class="btn btn-info" id="get_data">Get Data</a>--}}
                                    <input type="submit" name="get_data" class="btn btn-info" id="get_data"
                                           value="Get Data">
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    <a href="" class="btn btn-info" id="download_csv">Download Csv</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </section>
        </div>
    </div>

    <div class="row">

        <!--    user info starts here-->
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Customers
                </header>
                <div class="panel-body table-responsive">
                    <table class="table  table-hover general-table">
                        <thead>
                        <tr>

                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Landline</th>
                            <th>Newsletters</th>
                            <th>Special Offers</th>
                            <th>Created On</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        @if(!is_null($users))

                            @foreach($users as $i=>$user)

                                <tr>
                                    <td>{{$i+1}}</td>
                                    <td>{{$user->first_name or '-'}}</td>
                                    <td>{{$user->last_name or '-'}}</td>
                                    <td>{{$user->email or '-'}}</td>
                                    <td>{{$user->mobile or '-'}}</td>
                                    <td>{{$user->landline or '-'}}</td>
                                    <td>
                                        @if($user->newsletters) <i class="fa fa-check green"></i> @else <i
                                                class="fa fa-times red"></i> @endif
                                    </td>
                                    <td>
                                        @if($user->special_offers) <i class="fa fa-check green"></i> @else <i
                                                class="fa fa-times red"></i> @endif
                                    </td>
                                    <td>{{$user->created_at or '-'}}</td>
                                    <td>
                                        <a class="btn btn-info btn-xs"
                                           href="{{URL::to('dashboard/customers/info/'.$user->id)}}">
                                            <i class="fa fa-search"></i> View
                                        </a>
                                    </td>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td colspan="9" align="center">No data found</td>
                            </tr>
                        @endif

                        </tbody>

                    </table>
                </div>
            </section>

        </div>
        <!--user info ends here-->
        <div class="text-center">
            <?php if (!empty($users)) {
                echo $users->appends(array('from_date' => $from_date, 'to_date' => $to_date, 'newsletters' => $selected_newsletter, 'special_offers' => $selected_special_offers))->links();
            } ?>
        </div>

    </div>


@stop