@extends('layouts.dashboard_default')

@section('content')
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">
<script type="text/javascript"
        src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
<link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_overlay.png">
<link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite.png">
<link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite@2x.png">

<script type="text/javascript">
    $(document).ready(function () {

        $('.view_order').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            $order_id = $(this).attr('data-order-id');
            $url = "{{URL::to('dashboard/customers/order-items')}}" + '/' + $order_id;
            $.fancybox.showLoading({
                helpers: {
                    overlay: {closeClick: false}
                }
            });
            $.fancybox.helpers.overlay.open({parent: $('body'), closeClick: false});
            $.get($url, function (data) {
                $.fancybox(data);
            });
        });

        $('.edit_order_status').click(function (e) {

            e.preventDefault();
            var order_id = $(this).attr("data-order-id");
            var user_id = $(this).attr("data-user-id");
            var order_status = $(this).attr("data-order-status");
            var payment_status = $(this).attr("data-payment-status");

            $("#order_status option[value=" + order_status + "]").attr("selected", "selected");
            $("#payment_status option[value=" + payment_status + "]").attr("selected", "selected");

            $('#myModal').modal();
            $('.update_order_btn').click(function (f) {

                f.preventDefault();
                $('.update_order_btn').html("<i class=\"fa fa-spinner fa-spin\"></i>Saving Changes..");
                var form = $('#form');
                var url = "{{URL::to('dashboard/customers/ajax-update-order-status')}}" + '/' + order_id + '/' + user_id;
                $.post(url, form.serialize(), function (data) {
                    if (data.success == "true") {
                        window.location.href = "{{URL::to('dashboard/customers/info')}}" + '/' + +user_id;
                        $('.update_order_btn').html("Save changes");
                    } else {
                        alert("Some error occurred please try again later");
                        $('.update_order_btn').html("Save changes");
                    }
                });

            });

        });

    });
</script>


<div class="row">
    <div class="col-lg-12"></div>
    {{ Notification::showSuccess() }}
</div>

<!-- page start-->
<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::to('dashboard/customers')}}">Customers</a></li>
            <li>Customer Info</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">

<!--    customer info starts here-->
<div class="col-sm-12">
    <h3>{{$user->first_name.' '.$user->last_name}}</h3>
    <section class="panel panel-info">
        <header class="panel-heading">
            Customer Info
        </header>
        <div class="panel-body table-responsive">
            <table class="table  table-hover general-table">
                <thead>
                <tr>

                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Landline</th>
                    <th>Newsletters</th>
                    <th>Special Offers</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                </tr>
                </thead>

                <tbody>

                @if(!is_null($user))


                <tr>
                    <td>{{$user->first_name or '-'}}</td>
                    <td>{{$user->last_name or '-'}}</td>
                    <td>{{$user->email or '-'}}</td>
                    <td>{{$user->mobile or '-'}}</td>
                    <td>{{$user->landline or '-'}}</td>
                    <td>
                        @if($user->newsletters) <i class="fa fa-check green"></i> @else <i
                            class="fa fa-times red"></i> @endif
                    </td>
                    <td>
                        @if($user->special_offers) <i class="fa fa-check green"></i> @else <i
                            class="fa fa-times red"></i> @endif
                    </td>
                    <td>{{AppUtil::getParsedDate($user->created_at)}}</td>
                    <td>{{AppUtil::getParsedDate($user->updated_at)}}</td>
                    <td></td>
                </tr>


                @endif

                </tbody>

            </table>
        </div>
    </section>

</div>
<!--    customer info ends here-->

<!--    customer shipping addresses starts here-->
<div class="col-sm-12">
    <section class="panel panel-info">
        <header class="panel-heading">
            Shipping Addresses
        </header>
        <div class="panel-body table-responsive">
            <table class="table  table-hover general-table">
                <thead>
                <tr>

                    <th>Address Line 1</th>
                    <th>Address Line 2</th>
                    <th>State</th>
                    <th>Landmark</th>
                    <th>Pincode</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                </tr>
                </thead>

                <tbody>

                @if(!is_null($shipping_addresses))

                @foreach($shipping_addresses as $shipping_address)

                <tr>
                    <td>{{$shipping_address->line1 or '-'}}</td>
                    <td>{{$shipping_address->line2 or '-'}}</td>
                    <td>{{$shipping_address->state or '-'}}</td>
                    <td>{{$shipping_address->landmark or '-'}}</td>
                    <td>{{$shipping_address->pincode or '-'}}</td>
                    <td>{{AppUtil::getParsedDate($shipping_address->created_at)}}</td>
                    <td>{{AppUtil::getParsedDate($shipping_address->updated_at)}}</td>

                </tr>

                @endforeach

                @endif

                </tbody>

            </table>
        </div>
    </section>

</div>
<!--    customers shipping addresses ends here-->

<!--    customer billing addresses starts here-->
<div class="col-sm-12">
    <section class="panel panel-info">
        <header class="panel-heading">
            Billing Addresses
        </header>
        <div class="panel-body table-responsive">
            <table class="table  table-hover general-table">
                <thead>
                <tr>

                    <th>Address Line 1</th>
                    <th>Address Line 2</th>
                    <th>State</th>
                    <th>Landmark</th>
                    <th>Pincode</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                </tr>
                </thead>

                <tbody>

                @if(!is_null($billing_addresses))

                @foreach($billing_addresses as $billing_address)

                <tr>
                    <td>{{$billing_address->line1 or '-'}}</td>
                    <td>{{$billing_address->line2 or '-'}}</td>
                    <td>{{$billing_address->state or '-'}}</td>
                    <td>{{$billing_address->landmark or '-'}}</td>
                    <td>{{$billing_address->pincode or '-'}}</td>
                    <td>{{AppUtil::getParsedDate($billing_address->created_at)}}</td>
                    <td>{{AppUtil::getParsedDate($billing_address->updated_at)}}</td>

                </tr>

                @endforeach

                @endif

                </tbody>

            </table>
        </div>
    </section>

</div>
<!--    customer billing addresses ends here-->

<!--    customer orders start here-->
<div class="col-sm-12">
    <section class="panel panel-info">
        <header class="panel-heading">
            Orders
        </header>
        <div class="panel-body table-responsive">
            <table class="table  table-hover general-table">
                <thead>
                <tr>

                    <th>#</th>
                    <th>Reference</th>
                    <th>Net Value</th>
                    <th>Final Value</th>
                    <th>Status</th>
                    <th>Payment Status</th>
                    <th>Payment Mode</th>
                    <th>Notes</th>
                    <th>Created At</th>
                    <th>View Order</th>
                    {{--<th>Actions</th>--}}
                </tr>
                </thead>

                <tbody>

                @if(!is_null($orders))

                @foreach($orders as $i=>$row)

                <tr>
                    <td>{{$i+1}}</td>
                    <td>{{$row->reference}}</td>
                    <td>{{$row->net_value}}</td>
                    <td>{{$row->final_value}}</td>
                    <td>
                        @if(!empty($row->status)) {{$row->status}} @else - @endif
                    </td>
                    <td>
                        @if(!empty($row->payment_status)) {{$row->payment_status}} @else - @endif
                    </td>
                    <td>{{$row->payment_mode}}</td>
                    <td>
                        @if(!empty($row->notes)) {{$row->notes}} @else - @endif
                    </td>
                    <td>{{AppUtil::getParsedDate($row->created_at)}}</td>
                    <td><a href="javascript:void(0);"
                           id="view_order_{{$row->id}}" class="view_order fancybox.ajax btn btn-info btn-xs" data-order-id="{{$row->id}}"> <i class="fa fa-search"></i></a>
                    </td>
                    {{--<td>--}}
                        {{--<a href="#" class="edit_order_status" data-order-id="{{$row->id}}"--}}
                           {{--data-user-id="{{$user->id or ''}}" data-order-status="{{$row->status}}"--}}
                           {{--data-payment-status="{{$row->payment_status}}">Edit</a>--}}
                    {{--</td>--}}
                </tr>

                @endforeach

                @endif

                </tbody>

            </table>
        </div>
    </section>

</div>
<!--    customer orders ends here-->


</div>

<!--edit order status modal-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Order Status</h4>
            </div>

            <form action="#" class="form-horizontal" role="form"
                  method="post" id="form">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="order_status" class="col-sm-3 control-label">Order Status</label>

                        <div class="col-sm-9">
                            <select name="order_status" class="form-control" id="order_status">
                                <option value="new">New</option>
                                <option value="open">Open</option>
                                <option value="closed">Closed</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="payment_status" class="col-sm-3 control-label">Payment Status</label>

                        <div class="col-sm-9">
                            <select name="payment_status" class="form-control" id="payment_status">
                                <option value="unpaid">Unpaid</option>
                                <option value="paid">Paid</option>
                                <option value="pending">Pending</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="notes" class="col-sm-3 control-label">Notes</label>

                        <div class="col-sm-9">
                            <textarea name="notes" class="form-control" id="notes" rows="3"></textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary update_order_btn">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

@stop