@extends('layouts.dashboard_default')

@section('content')

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>


<script type="text/javascript">
    $(document).ready(function () {

        var from_date;
        var to_date;

        var selected_from_date = $.url().param("from_date"); // parse the current page URL
        var selected_to_date = $.url().param("to_date"); // parse the current page URL

        //isUndefined is underscore js function
        if (!_.isUndefined(selected_from_date)) {
            from_date = selected_from_date;
        } else {
            from_date = -7;
        }

        if (!_.isUndefined(selected_to_date)) {
            to_date = selected_to_date;
        } else {
            to_date = 'today';
        }


        $(".datepicker").datepicker({
            dateFormat: "yy-mm-dd",
            maxDate: new Date,
            changeMonth: true,
            changeYear: true
        });

        $('#to_date').datepicker('setDate', to_date);
        $('#from_date').datepicker('setDate', selected_from_date);

        var selectedFromDate = '<?php echo $from_date; ?>';
        var selectedToDate = '<?php echo $to_date; ?>';
        if (selectedFromDate && selectedToDate) {
            $('#from_date').datepicker('setDate', selectedFromDate);
            $('#to_date').datepicker('setDate', selectedToDate);
        } else {
            $('#to_date').datepicker('setDate', to_date);
            $('#from_date').datepicker('setDate', from_date);

        }


        //triggers when download csv button is clicked
        $('#download_csv').click(function (e) {

            e.preventDefault();
            from_date = $('#from_date').val();
            to_date = $('#to_date').val();
            if (from_date == "") {
                from_date = "0000-00-00";
            }
            var download_csv_url = "{{URL::to('dashboard/enquiries/download-csv')}}" + "?from_date=" + from_date + "&to_date=" + to_date;
            window.location.href = download_csv_url;
        });

        //triggers when get data button is clicked

        $('#get_data').click(function (e) {

            e.preventDefault();
            from_date = $('#from_date').val();
            to_date = $('#to_date').val();
            if (from_date == "") {
                from_date = "0000-00-00";
            }
            var get_data_url = "{{URL::to('dashboard/enquiries')}}" + "?from_date=" + from_date + "&to_date=" + to_date;
            window.location.href = get_data_url;
        });

        $('.delete').click(function (e) {

            e.preventDefault();
            var delete_url = $(this).attr("href");
            if (confirm("Are you sure you want to delete this entry?")) {
                window.location.href = delete_url;
            } else {
                return;
            }

        });

    });
</script>

<div class="row">
    <div class="col-lg-12">
        {{Notification::showSuccess()}}
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li>Enquiries</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">
    <div class="col-lg-4 form-horizontal">
        <div class="form-group">
            <label for="from_date" class="col-sm-3 control-label">From Date</label>

            <div class="col-sm-6">
                <input type="text" class="datepicker form-control" id="from_date">
            </div>
        </div>
    </div>

    <div class="col-lg-4 form-horizontal">
        <div class="form-group">
            <label for="to_date" class="col-sm-3 control-label">To Date</label>

            <div class="col-sm-6">
                <input type="text" class="datepicker form-control" id="to_date">
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-8 col-xs-10 button_div">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
            <a href="" class="btn btn-info" id="get_data">Get Data</a>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <a href="" class="btn btn-info" id="download_csv">Download Csv</a>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Enquiries
            </header>
            <div class="panel-body table-responsive">
                <table class="table general-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>City</th>
                        <th>Message</th>
                        <th>Source</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>

                    @if(!is_null($enquiries))

                    @foreach($enquiries as $enquiry)

                    <tr>
                        <td>{{$enquiry->name or ''}}</td>
                        <td>{{$enquiry->email or ''}}</td>
                        <td>{{$enquiry->mobile or ''}}</td>
                        <td>{{$enquiry->city or ''}}</td>
                        <td>{{$enquiry->message or '-'}}</td>
                        <td>{{$enquiry->source or '-'}}</td>
                        <td>
                            <?php echo AppUtil::getParsedDate($enquiry->created_at) ?>
                        </td>
                        <td>
                            <a href="{{URL::to('dashboard/enquiries/delete/'.$enquiry->id)}}"
                               class="btn btn-xs btn-danger delete">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>

                    @endforeach

                    @endif

                    </tbody>

                </table>
            </div>
        </section>
    </div>
    <div class="text-center">
        <?php if (!empty($enquiries)) {
            echo $enquiries->links();
        } ?>
    </div>
</div>


@stop