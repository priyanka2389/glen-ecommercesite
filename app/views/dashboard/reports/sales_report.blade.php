@extends('layouts.dashboard_default')

@section('content')
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">
    <script type="text/javascript"
            src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_overlay.png">
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite.png">
    <link src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/fancybox_sprite@2x.png">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $("[data-toggle='tooltip']").tooltip();

            var from_date;
            var to_date;

            var selected_from_date = $.url().param("from_date"); // parse the current page URL
            var selected_to_date = $.url().param("to_date"); // parse the current page URL

            //isUndefined is underscore js function
            if (!_.isUndefined(selected_from_date)) {
                from_date = selected_from_date;
            } else {
                var date = new Date();
                from_date = new Date(date.getFullYear(), date.getMonth(), 1);
            }

            if (!_.isUndefined(selected_to_date)) {
                to_date = selected_to_date;
            } else {
                to_date = 'today';
            }


            $(".datepicker").datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: new Date,
                changeMonth: true,
                changeYear: true,
                onSelect: function (selected, evnt) {
                    if ($('#to_date').val() < $('#from_date').val()) {
                        $('.alert-danger').html('<strong>To Date</strong> must be greater than <strong>From Date</strong>.').removeClass('hidden');
                    }else{
                        $('.alert-danger').addClass('hidden');
                    }
                }
            });

            $('#to_date').datepicker('setDate', to_date);
            $('#from_date').datepicker('setDate', from_date);


            //triggers when get data button is clicked

            $('#get_data').click(function (e) {

                e.preventDefault();
                from_date = $('#from_date').val();
                to_date = $('#to_date').val();
                if (from_date == "") {
                    from_date = "0000-00-00";
                }
                var get_data_url = "{{URL::to('dashboard/reports/sales-report')}}" + "?from_date=" + from_date + "&to_date=" + to_date;
                window.location.href = get_data_url;
            });

            //triggers when download csv button is clicked
            $('#download_csv').click(function (e) {

                e.preventDefault();
                from_date = $('#from_date').val();
                to_date = $('#to_date').val();
                if (from_date == "") {
                    from_date = "0000-00-00";
                }
                var download_csv_url = "{{URL::to('dashboard/reports/download-order-sales-csv')}}" + "?from_date=" + from_date + "&to_date=" + to_date;
                window.location.href = download_csv_url;
            });

            $('.view_order').click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                e.preventDefault();
                from_date = $('#from_date').val();
                to_date = $('#to_date').val();
                if (from_date == "") {
                    from_date = "0000-00-00";
                }
                $category_id = $(this).attr('data-category-id');
                $item_type = $(this).attr('data-item-type');
                $url = "{{URL::to('dashboard/reports/category-wise-orders')}}" + '/' + $category_id + "?from_date=" + from_date + "&to_date=" + to_date + "&item_type=" + $item_type;
                $.fancybox.showLoading({
                    helpers: {
                        overlay: {closeClick: false}
                    }
                });
                $.fancybox.helpers.overlay.open({parent: $('body'), closeClick: false});
                $.get($url, function (data) {
                    $.fancybox(data);
                });
            });
            $(".view_order").hover(
                    function () {
                        $(this).find('.fa-search').removeClass('hidden');
                    }, function () {
                        $(this).find('.fa-search').addClass('hidden');
                    }
            );
        });
    </script>


    <style type="text/css">
        #header_tab li.active a, #header_tab li a:hover {
            border-radius: 0 !important;
            -webkit-border-radius: 0 !important;
        }

        .table-responsive {
            overflow-x: hidden;
        }

        .tooltip {
            margin-left: 32px !important;
        }

        /*.table-responsive td{text-align: center;}*/
    </style>


    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger hidden" role="alert"></div>
            {{Notification::showSuccess()}}
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
                <li>Sales Report</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 form-horizontal">
            <div class="form-group">
                <label for="from_date" class="col-sm-3 control-label">From Date</label>

                <div class="col-sm-6">
                    <input type="text" class="datepicker form-control" id="from_date">
                </div>
            </div>
        </div>

        <div class="col-lg-4 form-horizontal">
            <div class="form-group">
                <label for="to_date" class="col-sm-3 control-label">To Date</label>

                <div class="col-sm-6">
                    <input type="text" class="datepicker form-control" id="to_date">
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-8 col-xs-10 button_div">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                <a href="{{URL::to('dashboard/reports/cart-csv')}}" class="btn btn-info" id="get_data">Get Data</a>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <a href="" class="btn btn-info" id="download_csv">Download Csv</a>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Sales Report
                </header>
                <div class="panel-body table-responsive">

                    <section class="panel">
                        <div class="panel-body table-responsive">
                            <div style="margin-bottom: 30px;">
                                <strong>This report is generated based on following cases:</strong><br>
                                <strong>COD -</strong> Order Status, Payment Status : 1. New, Pending 2. In-transit,
                                Pending 3. Delivered, Paid<br>
                                <strong>Online -</strong> Order Status, Payment Status : 1. New, Paid 2. In-transit,
                                Paid 3. Delivered, Paid
                            </div>

                            <table class="table general-table">
                                <thead class="panel-heading">
                                <tr>
                                    <th class="col-lg-4">Chimneys</th>
                                    <th class="col-lg-2">Sold products</th>
                                    <th class="col-lg-2">Online Sale (rs.)</th>
                                    <th class="col-lg-2">COD Sale (rs.)</th>
                                    <th class="col-lg-2">Total Sale (rs.)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $chimney_total_online_sale = 0;$chimney_total_cod_sale = 0; $chimney_total_quantity = 0;?>
                                @if(count($chimneys['categories']) > 0)
                                    @foreach($chimneys['categories'] as $chimney)
                                        <?php $chimney_total_online_sale = $chimney_total_online_sale + $chimney['online_total_price']; ?>
                                        <?php $chimney_total_cod_sale = $chimney_total_cod_sale + $chimney['cod_total_price']; ?>
                                        <?php $chimney_total_quantity = $chimney_total_quantity + $chimney['quantity']; ?>
                                        <tr>
                                            <td class="col-lg-4">
                                                <a class="view_order" data-toggle="tooltip"
                                                   title="View Category Wise Orders" data-item-type="product"
                                                   data-category-id="{{$chimney['category_id']}}"
                                                   href="javascript:void(0);">
                                                    <i class="fa fa-search hidden"></i>
                                                    {{$chimney['category_name'] or ''}}
                                                </a>
                                            </td>
                                            <td class="col-lg-2">{{$chimney['quantity'] or '-'}}</td>
                                            <td class="col-lg-2">{{($chimney['online_total_price'] > 0) ? $chimney['online_total_price'] : '-'}}</td>
                                            <td class="col-lg-2">{{($chimney['cod_total_price'] > 0) ? $chimney['cod_total_price'] : '-'}}</td>
                                            <td class="col-lg-2">{{($chimney['cod_total_price'] > 0 || $chimney['online_total_price'] > 0) ? $chimney['cod_total_price']+$chimney['online_total_price'] : '-'}}</td>
                                        </tr>
                                    @endforeach
                                    <tr class="panel-footer">
                                        <td>Total Sale</td>
                                        <td>{{$chimney_total_quantity}}</td>
                                        <td>{{($chimney_total_online_sale > 0) ? $chimney_total_online_sale : '-'}}</td>
                                        <td>{{($chimney_total_cod_sale > 0) ? $chimney_total_cod_sale : '-'}}</td>
                                        <td>{{($chimney_total_online_sale > 0 || $chimney_total_cod_sale > 0) ? $chimney_total_online_sale + $chimney_total_cod_sale : '-'}}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="1" align="center">No sales record found for this time duration.
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </section>

                    <section class="panel">
                        <div class="panel-body table-responsive">
                            <table class="table general-table">
                                <thead class="panel-heading">
                                <tr>
                                    <th class="col-lg-4">Cooktops</th>
                                    <th class="col-lg-2">Sold products</th>
                                    <th class="col-lg-2">Online Sale (rs.)</th>
                                    <th class="col-lg-2">COD Sale (rs.)</th>
                                    <th class="col-lg-2">Total Sale (rs.)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $cooktop_total_online_sale = 0;$cooktop_total_cod_sale = 0; $cooktop_total_quantity = 0;?>
                                @if(count($cooktops['categories']) > 0)
                                    @foreach($cooktops['categories'] as $cooktop)
                                        <?php $cooktop_total_online_sale = $cooktop_total_online_sale + $cooktop['online_total_price']; ?>
                                        <?php $cooktop_total_cod_sale = $cooktop_total_cod_sale + $cooktop['cod_total_price']; ?>
                                        <?php $cooktop_total_quantity = $cooktop_total_quantity + $cooktop['quantity']; ?>
                                        <tr>
                                            <td class="col-lg-4">
                                                <a class="view_order" data-toggle="tooltip"
                                                   title="View Category Wise Orders" data-item-type="product"
                                                   data-category-id="{{$cooktop['category_id']}}"
                                                   href="javascript:void(0);">
                                                    <i class="fa fa-search hidden"></i>
                                                    {{$cooktop['category_name'] or ''}}
                                                </a>
                                            </td>
                                            <td class="col-lg-2">{{$cooktop['quantity'] or '-'}}</td>
                                            <td class="col-lg-2">{{($cooktop['online_total_price'] > 0) ? $cooktop['online_total_price'] : '-'}}</td>
                                            <td class="col-lg-2">{{($cooktop['cod_total_price'] > 0) ? $cooktop['cod_total_price'] : '-'}}</td>
                                            <td class="col-lg-2">{{($cooktop['cod_total_price'] > 0 || $cooktop['online_total_price'] > 0) ? $cooktop['cod_total_price']+$cooktop['online_total_price'] : '-'}}</td>
                                        </tr>
                                    @endforeach
                                    <tr class="panel-footer">
                                        <td>Total Sale</td>
                                        <td>{{$cooktop_total_quantity}}</td>
                                        <td>{{($cooktop_total_online_sale > 0) ? $cooktop_total_online_sale : '-'}}</td>
                                        <td>{{($cooktop_total_cod_sale > 0) ? $cooktop_total_cod_sale : '-'}}</td>
                                        <td>{{($cooktop_total_cod_sale > 0 || $cooktop_total_online_sale > 0) ? $cooktop_total_cod_sale + $cooktop_total_online_sale : '-'}}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="1" align="center">No sales record found for this time duration.
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </section>

                    <section class="panel">
                        <div class="panel-body table-responsive">
                            <table class="table general-table">
                                <thead class="panel-heading">
                                <tr>
                                    <th class="col-lg-4">Built-In-Series</th>
                                    <th class="col-lg-2">Sold products</th>
                                    <th class="col-lg-2">Online Sale (rs.)</th>
                                    <th class="col-lg-2">COD Sale (rs.)</th>
                                    <th class="col-lg-2">Total Sale (rs.)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $built_in_hob_total_online_sale = 0;$built_in_hob_total_cod_sale = 0; $built_in_hob_total_quantity = 0;?>
                                @if(count($built_in_hobs['categories']) > 0)
                                    @foreach($built_in_hobs['categories'] as $built_in_hob)
                                        <?php $built_in_hob_total_online_sale = $built_in_hob_total_online_sale + $built_in_hob['online_total_price']; ?>
                                        <?php $built_in_hob_total_cod_sale = $built_in_hob_total_cod_sale + $built_in_hob['cod_total_price']; ?>
                                        <?php $built_in_hob_total_quantity = $built_in_hob_total_quantity + $built_in_hob['quantity']; ?>
                                        <tr>
                                            <td class="col-lg-4">
                                                <a class="view_order" data-toggle="tooltip"
                                                   title="View Category Wise Orders" data-item-type="product"
                                                   data-category-id="{{$built_in_hob['category_id']}}"
                                                   href="javascript:void(0);">
                                                    <i class="fa fa-search hidden"></i>
                                                    {{$built_in_hob['category_name'] or ''}}
                                                </a>
                                            </td>
                                            <td class="col-lg-2">{{$built_in_hob['quantity'] or '-'}}</td>
                                            <td class="col-lg-2">{{($built_in_hob['online_total_price'] > 0) ? $built_in_hob['online_total_price'] : '-'}}</td>
                                            <td class="col-lg-2">{{($built_in_hob['cod_total_price'] > 0) ? $built_in_hob['cod_total_price'] : '-'}}</td>
                                            <td class="col-lg-2">{{($built_in_hob['cod_total_price'] > 0 || $built_in_hob['online_total_price'] > 0) ? $built_in_hob['cod_total_price']+$built_in_hob['online_total_price'] : '-'}}</td>
                                        </tr>
                                    @endforeach
                                    <tr class="panel-footer">
                                        <td>Total Sale</td>
                                        <td>{{$built_in_hob_total_quantity}}</td>
                                        <td>{{($built_in_hob_total_online_sale > 0) ? $built_in_hob_total_online_sale : '-'}}</td>
                                        <td>{{($built_in_hob_total_cod_sale > 0) ? $built_in_hob_total_cod_sale : '-'}}</td>
                                        <td>{{($built_in_hob_total_cod_sale > 0 || $built_in_hob_total_online_sale > 0) ? $built_in_hob_total_cod_sale + $built_in_hob_total_online_sale : '-'}}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="1" align="center">No sales record found for this time duration.
                                        </td>
                                    </tr>
                                @endif

                                </tbody>

                            </table>
                        </div>
                    </section>

                    <section class="panel">
                        <div class="panel-body table-responsive">
                            <table class="table general-table">
                                <thead class="panel-heading">
                                <tr>
                                    <th class="col-lg-4">Small Appliances</th>
                                    <th class="col-lg-2">Sold products</th>
                                    <th class="col-lg-2">Online Sale (rs.)</th>
                                    <th class="col-lg-2">COD Sale (rs.)</th>
                                    <th class="col-lg-2">Total Sale (rs.)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $small_appliance_total_online_sale = 0;$small_appliance_total_cod_sale = 0; $small_appliance_total_quantity = 0;?>
                                @if(count($small_appliances['categories']) > 0)
                                    @foreach($small_appliances['categories'] as $small_appliance)
                                        <?php $small_appliance_total_online_sale = $small_appliance_total_online_sale + $small_appliance['online_total_price']; ?>
                                        <?php $small_appliance_total_cod_sale = $small_appliance_total_cod_sale + $small_appliance['cod_total_price']; ?>
                                        <?php $small_appliance_total_quantity = $small_appliance_total_quantity + $small_appliance['quantity']; ?>
                                        <tr>
                                            <td class="col-lg-4">
                                                <a class="view_order" data-toggle="tooltip"
                                                   title="View Category Wise Orders" data-item-type="product"
                                                   data-category-id="{{$small_appliance['category_id']}}"
                                                   href="javascript:void(0);">
                                                    <i class="fa fa-search hidden"></i>
                                                    {{$small_appliance['category_name'] or ''}}
                                                </a>
                                            </td>
                                            <td class="col-lg-2">{{$small_appliance['quantity'] or '-'}}</td>
                                            <td class="col-lg-2">{{($small_appliance['online_total_price'] > 0) ? $small_appliance['online_total_price'] : '-'}}</td>
                                            <td class="col-lg-2">{{($small_appliance['cod_total_price'] > 0) ? $small_appliance['cod_total_price'] : '-'}}</td>
                                            <td class="col-lg-2">{{($small_appliance['cod_total_price'] > 0 || $small_appliance['online_total_price'] > 0) ? $small_appliance['cod_total_price']+$small_appliance['online_total_price'] : '-'}}</td>
                                        </tr>
                                    @endforeach
                                    <tr class="panel-footer">
                                        <td>Total Sale</td>
                                        <td>{{$small_appliance_total_quantity}}</td>
                                        <td>{{($small_appliance_total_online_sale > 0) ? $small_appliance_total_online_sale : '-'}}</td>
                                        <td>{{($small_appliance_total_cod_sale > 0) ? $small_appliance_total_cod_sale : '-'}}</td>
                                        <td>{{($small_appliance_total_cod_sale > 0 || $small_appliance_total_online_sale > 0) ? $small_appliance_total_cod_sale + $small_appliance_total_online_sale : '-'}}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="1" align="center">No sales record found for this time duration.
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </section>

                    <section class="panel">
                        <div class="panel-body table-responsive">
                            <table class="table general-table">
                                <thead class="panel-heading">
                                <tr>
                                    <th class="col-lg-4">Combos</th>
                                    <th class="col-lg-2">Sold combos</th>
                                    <th class="col-lg-2">Online Sale (rs.)</th>
                                    <th class="col-lg-2">COD Sale (rs.)</th>
                                    <th class="col-lg-2">Total Sale (rs.)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $combo_total_online_sale = 0;$combo_total_cod_sale = 0; $combo_total_quantity = 0;?>
                                @if(isset($combos) && count($combos) > 0)
                                    @foreach($combos as $combo)
                                        <?php $combo_total_online_sale = $combo_total_online_sale + ($combo['online_sale']); ?>
                                        <?php $combo_total_cod_sale = $combo_total_cod_sale + ($combo['cod_sale']); ?>
                                        <?php $combo_total_quantity = $combo_total_quantity + $combo['qty']; ?>
                                        <tr>
                                            <td class="col-lg-4">
                                                <a class="view_order" data-toggle="tooltip"
                                                   title="View Category Wise Orders"
                                                   data-item-type="combo"
                                                   data-category-id="{{$combo['combo_id']}}"
                                                   href="javascript:void(0);">
                                                    <i class="fa fa-search hidden"></i>
                                                    {{$combo['combo_name'] or ''}}
                                                </a>
                                            </td>
                                            <td class="col-lg-2">{{$combo['qty'] or ''}}</td>
                                            <td class="col-lg-2">{{($combo['online_sale'] > 0) ? $combo['online_sale'] : '-'}}</td>
                                            <td class="col-lg-2">{{($combo['cod_sale'] > 0) ? $combo['cod_sale'] : '-'}}</td>
                                            <td class="col-lg-2">{{($combo['cod_sale'] > 0 || $combo['online_sale'] > 0) ? $combo['cod_sale']+$combo['online_sale'] : '-'}}</td>
                                        </tr>
                                    @endforeach
                                    <tr class="panel-footer">
                                        <td>Total Sale</td>
                                        <td>{{$combo_total_quantity}}</td>
                                        <td>{{($combo_total_online_sale > 0) ? $combo_total_online_sale : '-'}}</td>
                                        <td>{{($combo_total_cod_sale > 0) ? $combo_total_cod_sale : '-'}}</td>
                                        <td>{{($combo_total_cod_sale > 0 || $combo_total_online_sale > 0) ? $combo_total_cod_sale + $combo_total_online_sale : '-'}}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="3" align="center">No sales record found for this time duration.
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </section>

                    <section class="panel">
                        <div class="panel-body table-responsive">
                            <table class="table general-table">
                                <thead class="panel-heading">
                                <tr>
                                    <th class="col-lg-4">Total Sale</th>
                                    <th class="col-lg-2">Total Sold products</th>
                                    <th class="col-lg-2">Total Online Sale (rs.)</th>
                                    <th class="col-lg-2">Total COD Sale (rs.)</th>
                                    <th class="col-lg-2">Total Sale (rs.)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="col-lg-4"></td>
                                    <td class="col-lg-2">{{$chimney_total_quantity + $cooktop_total_quantity + $built_in_hob_total_quantity + $small_appliance_total_quantity + $combo_total_quantity}}</td>
                                    <td class="col-lg-2">{{$chimney_total_online_sale + $cooktop_total_online_sale + $built_in_hob_total_online_sale + $small_appliance_total_online_sale + $combo_total_online_sale}}</td>
                                    <td class="col-lg-2">{{$chimney_total_cod_sale + $cooktop_total_cod_sale + $built_in_hob_total_cod_sale + $small_appliance_total_cod_sale + $combo_total_cod_sale}}</td>
                                    <td class="col-lg-2">{{($chimney_total_online_sale + $chimney_total_cod_sale)+ ($cooktop_total_online_sale + $cooktop_total_cod_sale) + ($built_in_hob_total_online_sale+$built_in_hob_total_cod_sale) + ($small_appliance_total_online_sale+$small_appliance_total_cod_sale) + ($combo_total_online_sale + $combo_total_cod_sale)}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </section>

                </div>
            </section>

        </div>
    </div>


@stop