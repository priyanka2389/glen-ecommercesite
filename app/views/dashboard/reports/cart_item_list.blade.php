<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <title></title>

    <!--Core CSS -->
    {{HTML::style('backoffice/css/bootstrap.min.css')}}
    {{HTML::style('backoffice/css/bootstrap-reset.css')}}
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"/>

    <!-- Custom styles for this template -->
    {{HTML::style('backoffice/css/style.css')}}
    {{HTML::style('backoffice/css/style-responsive.css')}}
    {{HTML::style('backoffice/css/custom.css')}}
    {{HTML::style('frontoffice/css/rupee.css')}}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .orders.table thead > tr > th, .orders.table tbody > tr > th, .orders.table tfoot > tr > th, .orders.table thead > tr > td, .orders.table tbody > tr > td, .orders.table tfoot > tr > td {
            padding: 5px;
        }
    </style>
</head>
<body>

<div class="container orders">
    <div class="row">
        <div class="col-lg-12">
            <h3>List of Cart Items</h3>
            <table class="table table-stripped table-hover">
                <thead>
                <tr>
                    <th>Cart Id</th>
                    <th>Item Name</th>
                    <th>User Name</th>
                    <th>Mobile</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Category total (Rs.)</th>
                    <th>Cart total (Rs.)</th>
                    <th>Creation Date</th>
                </tr>
                </thead>

                <tbody>
                @foreach($cart as $cart_item)
                    <tr>
                        <td>{{$cart_item['cart_id']}}</td>
                        <td>{{$cart_item['item_name']}}</td>
                        <td>{{ucfirst(strtolower($cart_item['user_name']))}}</td>
                        <td>{{$cart_item['mobile'] or '-'}}</td>
                        <td>{{$cart_item['category']}}</td>
                        <td>{{$cart_item['sub_category']}}</td>
                        <td>{{$cart_item['price']}} x {{$cart_item['quantity']}} = {{$cart_item['category_total']}}</td>
                        <td>{{$cart_item['cart_total']}}</td>
                        <td>{{date('Y-m-d', strtotime($cart_item['added_date']))}}</td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div>

</body>
</html>