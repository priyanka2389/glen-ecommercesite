<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <title></title>

    <!--Core CSS -->
    {{HTML::style('backoffice/css/bootstrap.min.css')}}
    {{HTML::style('backoffice/css/bootstrap-reset.css')}}
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"/>

    <!-- Custom styles for this template -->
    {{HTML::style('backoffice/css/style.css')}}
    {{HTML::style('backoffice/css/style-responsive.css')}}
    {{HTML::style('backoffice/css/custom.css')}}
    {{HTML::style('frontoffice/css/rupee.css')}}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .orders.table thead > tr > th, .orders.table tbody > tr > th, .orders.table tfoot > tr > th, .orders.table thead > tr > td, .orders.table tbody > tr > td, .orders.table tfoot > tr > td {
            padding: 5px;
        }
    </style>
</head>
<body>

<div class="container orders">
    <div class="row">
        <div class="col-lg-12">
            <h3>List of Orders</h3>
            <table class="table table-stripped table-hover">
                <thead>

                <tr>
                    <th>Order No</th>
                    <th>User Name</th>
                    <th>State</th>
                    <th>City</th>
                    <th>Payment Method</th>
                    <th>Order Status</th>
                    <th>Payment Status</th>
                    <th>Order Total</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Category Total</th>
                    <th>Order Date</th>
                </tr>

                </thead>

                <tbody>

                @foreach($orders as $order)

                    <tr>
                        <td>{{$order['order_id']}}</td>
                        <td>{{ucfirst(strtolower($order['user_name']))}}</td>
                        <td>{{$order['shipping_address_state']}}</td>
                        <td>{{$order['shipping_address_city']}}</td>
                        <td>{{$order['payment_method']}}</td>
                        <td>{{$order['order_status']}}</td>
                        <td>{{$order['payment_status']}}</td>
                        <td>{{$order['order_total']}}</td>
                        <td>{{$order['category']}}</td>
                        <td>{{$order['sub_category']}}</td>
                        <td>{{$order['category_total']}}</td>
                        <td>{{date('Y-m-d', strtotime($order['order_date']))}}</td>
                    </tr>

                @endforeach

                </tbody>

            </table>
        </div>
    </div>
</div>

</body>
</html>