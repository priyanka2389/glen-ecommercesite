@extends('layouts.dashboard_default')

@section('content')

<style type="text/css">
    .green{
        color: green;
    }
    .red{
        color:red
    }
</style>

<div class="row">
    <div class="col-lg-12"></div>
    {{ Notification::showSuccess() }}
</div>

<!-- page start-->
<div class="row">
    <div class="col-lg-10">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{URL::to('dashboard/main-coupon')}}"><i class="fa fa-home"></i> Main Coupon</a></li>
            <li>Codes</li>
        </ul>
        <!--breadcrumbs end -->
    </div>

</div>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Codes List
            </header>
            <div class="panel-body">
                <table class="table  table-hover general-table">
                    <thead>
                    <tr>
                        <th>Code</th>
                        <th>Is Used</th>
                        <th>Created At</th>
                    </tr>
                    </thead>

                    <tbody>


                    @if(!is_null($codes))

                    @foreach($codes as $code)


                    <tr class="">

                        <td>{{$code->code}}</td>
                        <td>
                            @if(!is_null($code->order_id))
                            <b class="red">Yes</b>
                            @else
                            <b class="green">No</b>
                            @endif
                        </td>
                        <td>
                            <?php $created_at = $code->created_at;
                            echo date("d M Y", strtotime($created_at));
                            ?>
                        </td>
                    </tr>

                    @endforeach


                    @endif

                    </tbody>

                </table>
            </div>
        </section>

    </div>


</div>


@stop