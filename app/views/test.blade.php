<ul class="nav nav-tabs margin-bottom0">
    <li class="active"><a href="#price" data-toggle="tab">Price</a></li>
</ul>

<div id="myTabContent" class="tab-content tab-bg">
    <div class="tab-pane  active" id="price">
        <div class="row-fluid">
            <div class="span11" padding-top-5px="">
                <p>
                    <label class="checkbox-inline margin-right-15 label-font font-14px" for="amount">Range:</label>
                    <span id="amount" style="border:0; color:#f6931f; font-weight:bold;"><span
                            class="WebRupee">Rs.</span>0 - <span class="WebRupee">Rs.</span>55990</span>
                </p>

                <div id="price_slider"
                     class="filter_price ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"
                     data-min-selected="0" data-max-selected="55990" data-min="0" data-max="55990"
                     aria-disabled="false">
                    <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div>
                    <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a><a
                        class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 100%;"></a></div>
            </div>
        </div>
    </div>
</div>

http://glensite.localhost.com/order/return?vpc_Amount=242950&vpc_BatchNo=0&vpc_Command=pay&vpc_Locale=en&vpc_MerchTxnRef=1779&vpc_Merchant=00100336&vpc_Message=E5000%3A+Cannot+form+a+matching+secure+hash+based+on+the+merchant%27s+request+using+either+of+the+two+merchant%27s+secrets&vpc_OrderInfo=1779&vpc_TransactionNo=0&vpc_TxnResponseCode=7&vpc_Version=1