<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/migrate', function () {
    Artisan::call('migrate', [
        '--path' => "app/database/migrations"
    ]);
    echo '<br>done with app tables migrations';
});

Route::get('/load-libraries', function () {
    $output = Artisan::call('dump-autoload');
    echo $output;
    echo 'dump-autoload complete';
});

Route::get("/shop", function () {
    return Redirect::to(URL::to('/'));
});
Route::get("/coupon", function () {
    return Redirect::to('http://coupon.glenindia.com/coupon/');
});
Route::controller("dashboard/url", "UrlDashboardController");
Route::get("/shop/{segment}/{segmentnext?}", 'UrlController@getRedirectUrl');

Route::get('/', "HomeController@getIndex");

//Route::get("{category}/{prouct_code}",function($category,$product_code){
//   //get by product code
//
//});
//Route::get("{category}-{product_code}-{product_id}", function () {
//    $request = Request::create('/', 'GET');
//    return Route::dispatch($request)->getContent();
//});


Route::get('category/test', "CategoryController@getTest");
Route::get('category/designer_chimney/test', "CategoryController@getTest");

//Route::when('dashboard*', 'auth_admin');

Route::controller('test', "TestController");

Route::controller('dashboard/documentation', 'DashboardDocumentationController');

Route::controller('dashboard/category', "DashboardCategoryController");
Route::controller("dashboard/products", "DashboardProductsController");
Route::controller("dashboard/tags", "DashboardTagsController");

Route::controller('dashboard/customers', "DashboardCustomersController");

Route::get("dashboard/category-documents/{category_id}", 'DashboardCategoryDocumentsController@getIndex');
Route::controller('dashboard/category-documents', "DashboardCategoryDocumentsController");

Route::get("dashboard/category-videos/{category_id}", 'DashboardCategoryVideosController@getIndex');
Route::controller('dashboard/category-videos', "DashboardCategoryVideosController");

Route::get("dashboard/category-attributes/{category_id}", 'DashboardCategoryAttributesController@getIndex');
Route::controller('dashboard/category-attributes', "DashboardCategoryAttributesController");

Route::get('dashboard/product-documents/{product_id}', "DashboardProductDocumentsController@getIndex");
Route::controller("dashboard/product-documents", "DashboardProductDocumentsController");

Route::get('dashboard/product-videos/{product_id}', "DashboardProductVideosController@getIndex");
Route::controller("dashboard/product-videos", "DashboardProductVideosController");

Route::get('dashboard/product-images/{product_id}', "DashboardProductImagesController@getIndex");
Route::controller("dashboard/product-images", "DashboardProductImagesController");

Route::get('dashboard/product-attributes/{product_id}', "DashboardProductAttributesController@getIndex");
Route::controller("dashboard/product-attributes", "DashboardProductAttributesController");

Route::get('dashboard/product-tags/{product_id}', "DashboardProductTagsController@getIndex");
Route::controller("dashboard/product-tags", "DashboardProductTagsController");

Route::get('dashboard/combo-products/{combo_id}', "DashboardComboProductsController@getIndex");
Route::controller("dashboard/combo-products", "DashboardComboProductsController");

Route::get('dashboard/variant/{product_id}', 'DashboardVariantController@getIndex');
Route::controller('dashboard/variant', 'DashboardVariantController');

Route::controller('dashboard/main-coupon', 'DashboardMainCouponController');

Route::get('dashboard/product-specific-attributes/{product_id}', 'DashboardProductSpecificAttributes@getIndex');
Route::controller('dashboard/product-specific-attributes', 'DashboardProductSpecificAttributes');


Route::controller('dashboard/dealers', 'DashboardDealersController');
Route::controller('dashboard/distributors', 'DashboardDistributorsController');
Route::controller('dashboard/retail-stores', 'DashboardRetailStoresController');

Route::controller('dashboard/combo', 'DashboardComboController');

Route::get('dashboard/combo-images/{combo_id}', "DashboardComboImagesController@getIndex");
Route::controller("dashboard/combo-images", "DashboardComboImagesController");

Route::controller('dashboard/order', 'DashboardOrderController');
Route::controller('dashboard/demo', 'DashboardDemoController');
Route::controller('dashboard/message', 'DashboardMessageController');
Route::controller('dashboard/pages', 'DashboardPageController');
Route::controller('dashboard/call', 'DashboardCallController');
Route::controller('dashboard/service-centers', 'DashboardServicesCentersController');
Route::controller('dashboard/settings', 'DashboardSettingsController');
Route::controller('dashboard/mails', 'DashboardMailController');
Route::controller('dashboard/reports', 'DashboardReportController');
Route::controller('dashboard/feedback', 'DashboardFeedbackController');
Route::controller('dashboard/enquiries', 'DashboardOfferController');


Route::controller("admin", "AdminController");
Route::controller('dashboard', 'DashboardController');
Route::controller('dealers', 'DealersController');
Route::controller('retail-stores', 'RetailStoreController');
Route::controller('distributors', 'DistributorController');
Route::controller('demo', 'DemoController');
Route::controller('message', 'MessageController');
Route::controller('call', 'CallController');
Route::controller('service-centers', 'ServicesCentersController');
Route::controller('password', 'RemindersController');

Route::get('retail-outlets', 'DealersController@getIndex');

Route::get('terms-conditions', function () {
    $data['searchProducts'] = AppUtil::getSearchProducts();
    return View::make('frontoffice.terms_and_conditions',$data);
});
Route::get('return-policy', function () {
    $data['searchProducts'] = AppUtil::getSearchProducts();
    return View::make('frontoffice.return_policy',$data);
});
Route::get('privacy-policy', function () {
    $data['searchProducts'] = AppUtil::getSearchProducts();
    return View::make('frontoffice.privacy_policy',$data);
});
//Route::get('distributors', function () {
//    return View::make('frontoffice.distributors');
//});
//Route::get('service-centers', function () {
//    return View::make('frontoffice.service_centers');
//});
//Route::get('retail-stores', function () {
//    return View::make('frontoffice.retail_stores');
//});
Route::get('product-manuals', function () {
    $data['searchProducts'] = AppUtil::getSearchProducts();
    return View::make('frontoffice.product_manuals',$data);
});
Route::get('corporate', function () {
    $data['searchProducts'] = AppUtil::getSearchProducts();
    return View::make('frontoffice.corporate',$data);
});
Route::get('catalogues', function () {
    $data['searchProducts'] = AppUtil::getSearchProducts();
    return View::make('frontoffice.catalogues',$data);
});
Route::get('downloads', function () {
    return View::make('frontoffice.downloads');
});
Route::get('customer-service', function () {
    return View::make('frontoffice.customer_service');
});

//page
Route::get('page/{page}', function () {
    $short_code = Request::segment(2);
    $view_name = "page";

    return AppUtil::route('PageController', "getPage", array($short_code, $view_name));
});

//media
Route::get('media-press-&-magazine-ads', function () {

//    $data['searchProducts'] = $this->productService->getSearchProducts();
    $data['searchProducts'] = AppUtil::getSearchProducts();
    return View::make('frontoffice.press_releases',$data);
});
Route::get('media-tv-ads', function () {
    $short_code = "media-tv-ads";
    $view_name = "tv_ads";

    return AppUtil::route('PageController', "getPage", array($short_code, $view_name));
});
Route::get('media-product-demos', function () {
    $data['searchProducts'] = AppUtil::getSearchProducts();
    return View::make('frontoffice.product_demos',$data);
});
Route::get('recipe-book', function () {
    $data['searchProducts'] = AppUtil::getSearchProducts();
    return View::make('frontoffice.recipe_book',$data);
});

//offer zone
Route::get('offer-zone', function () {

    $short_code = "offer-zone";
    $view_name = "offerzone.offer_zone";

    return AppUtil::route('PageController', "getPage", array($short_code, $view_name));
});
Route::get('independence-day-offers', function () {
    return View::make('frontoffice.offerzone.independence_day_offer');
});
Route::get('offer', function () {
    return View::make('frontoffice.offerzone.offer_zone');
});


Route::controller('home', 'HomeController');
Route::controller('category', 'CategoryController');
Route::controller("cart", "CartController");
Route::controller("order", "OrderController");
Route::controller("contact-us", "ContactController");
Route::controller("feedback", "FeedbackController");
Route::controller("page", "PageController");
Route::controller("mail", "MailRecordsController");

//Route::get('/product/compare', 'ProductsController@getCompare');
//Route::get('/product/cart-items', 'ProductsController@getCartItems');
//Route::get('/product/remove-compare-id', 'ProductsController@getRemoveCompareId');

//Route::pattern('product_id', '[0-9]+');
//Route::get('/product/{product_id}', 'ProductsController@getIndex');


Route::controller('user', 'UserController');
Route::controller('product', 'ProductsController');
Route::get('shipping-and-delivery-faqs', 'ProductsController@getFreeShippingContent');
Route::get('faqs', function () {
    $data['searchProducts'] = AppUtil::getSearchProducts();
    return View::make('frontoffice.FAQs',$data);
});
//Route::controller('combo', 'ComboController');


//route for inserting data
//Route::controller("insert", 'InsertController');


//static category pages routes starts here

//chimneys
Route::get('island-chimneys', function () {
    $id = 29;
    $view_name = "chimneys.island_chimneys";
//    $view_name = "category_sample";
//    return View::make('frontoffice.temp');
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('split-chimneys', function () {
    $id = 28;
    $view_name = "chimneys.split_chimneys";
    //$view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('designer-hood-chimneys', function () {
    $id = 27;
    $view_name = "chimneys.designer_chimneys";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('straight-line-chimneys', function () {
    $id = 30;
    $view_name = "chimneys.straight_line_chimneys";
    //$view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});

//Cooking Appliances
//-&-induction-cooktops
Route::get('glass', function () {
   return Redirect::to('/glass-and-induction-cooktop', 301); 
});
Route::get('glass-and-induction-cooktop', function () {
    $id = 32;
    $view_name = "cooking_appliances.glass_and_induction_cooktops";
    //$view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('platinum-cooktops', function () {
    $id = 34;
    $view_name = "cooking_appliances.platinum_cooktops";
    // $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('stainless-steel-cooktops', function () {
    $id = 35;
    $view_name = "cooking_appliances.stainless_steel_cooktops";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('indian-cooking-ranges', function () {
    $id = 3;
    $view_name = "cooking_appliances.cooking_range";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('international-cooking-ranges', function () {
    $id = 2;
    $view_name = "cooking_appliances.internationals_cooking_range";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});

//Built-in-series appliances
Route::get('built-in-ovens', function () {
    $id = 40;
    $view_name = "built_in_series.built_in_ovens";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('microwave-oven', function () {
    $id = 41;
    $view_name = "built_in_series.microwave_oven";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('glass-hobs', function () {
    $id = 38;
    $view_name = "built_in_series.glass_hobs";
    //$view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('induction-hobs', function () {
    $id = 37;
    $view_name = "built_in_series.induction_hobs";
    //$view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('stainless-steel-hob', function () {
    $id = 39;
//    $view_name = "category_sample";
    $view_name = "built_in_series.stainless_steel_hobs";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});

//Small Appliances

Route::get('food-processors', function () {
    $id = 12;
    $view_name = "small_appliances.food_processors";
    //$view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('juicer-mixer-grinders', function () {
    $id = 17;
    //$view_name = "small_appliances.juicer_mixer_grinders";
    $view_name = "kitchen_appliances.food_preparations.juicer_mixer_grinder";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('mixer-grinders', function () {
    $id = 19;
    $view_name = "kitchen_appliances.food_preparations.mixer_grinders";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('choppers', function () {
    $id = 10;
    $view_name = "kitchen_appliances.food_preparations.choppers";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('blender-grinders', function () {
    $id = 11;
    $view_name = "kitchen_appliances.food_preparations..blender_grinders";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('slow-juicer', function () {
    $id = 44;
    $view_name = "kitchen_appliances.food_preparations..slow_juicer";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('hand-mixers', function () {
    $id = 14;
    $view_name = "kitchen_appliances.food_preparations.hand_mixer";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('egg-master', function () {
    $id = 42;
    $view_name = "small_appliances.egg_master";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('kettles-&-tea-makers', function () {
    $id = 18;
    $view_name = "small_appliances.kettles_and_tea_makers";
    //$view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('toasters', function () {
    $id = 24;
    $view_name = "small_appliances.toasters";
    //$view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('sandwich-makers', function () {
    $id = 22;
    $view_name = "kitchen_appliances.cooking_appliances.sandwich_makers";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('bread-makers', function () {
    $id = 9;
    $view_name = "small_appliances.bread_makers";
    //$view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('air-fryer', function () {
    $id = 8;
    $view_name = "small_appliances.air_fryer";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('steam-cookers', function () {
    $id = 23;
    $view_name = "kitchen_appliances.cooking_appliances.steam_cooker";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('rice-cookers', function () {
    $id = 21;
    $view_name = "small_appliances.rice_cookers";
    //$view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('glass-grill', function () {
    $id = 13;
    $view_name = "kitchen_appliances.cooking_appliances.glass_grill";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('oven-toaster-grillers', function () {
    $id = 20;
    $view_name = "cooking_appliances.oven_toaster_grillers";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('tandoors', function () {
    $id = 25;
    $view_name = "small_appliances.tandoors";
    //$view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('induction-cookers', function () {
    $id = 15;
    $view_name = "kitchen_appliances.cooking_appliances.induction_cookers";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('OFR-heaters', function () {
    $id = 5;
    $view_name = "small_appliances.OFR_heaters";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('PTC-heaters', function () {
    $id = 6;
    $view_name = "small_appliances.PTC_heaters";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('irons', function () {
    $id = 16;
    $view_name = "small_appliances.iron";
//    $view_name = "category_sample";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});
Route::get('coffee-maker', function () {
    $id = 43;
    $view_name = "small_appliances.fausta_coffee_maker";
    return AppUtil::route('CategoryController', "getData", array($id, $view_name));
});

//route to get combo products
Route::get("combo/{product_code}", 'ComboController@getCombo');

//route for building the link along with category name, product name and product id
Route::get("{category}/{product_code}", 'ProductsController@getIndex');


//route to get campaigns
Route::get("breakfast-made-easy", "OffersController@getBreakfastMadeEasy");
Route::get("combo-offers", "OffersController@getComboOffers");
Route::get("chimney-offers", "OffersController@getChimneyOffers");
Route::get("cooktop-offers", "OffersController@getCooktopOffers");
Route::get("built-in-hob-offers", "OffersController@getBuiltInHobOffers");
Route::get("mothers-day-special-offers", "OffersController@getMothersDaySpecialOffers");
Route::get("request-a-call", "OffersController@getRequestCall");
Route::post("request-a-call", "OffersController@postRequestCall");


//bindings starts here
App::bind("iAttributeRepository", "EloquentAttributeRepository");
App::bind("iCategoryRepository", "EloquentCategoryRepository");
App::bind("iComboRepository", "EloquentComboRepository");
App::bind("iDealersRepository", "EloquentDealersRepository");
App::bind("iDistributorsRepository", "EloquentDistributorsRepository");
App::bind("iRetailStoresRepository", "EloquentRetailStoresRepository");
App::bind("iDocumentRepository", "EloquentDocumentRepository");
App::bind("iImageRepository", "EloquentImageRepository");
App::bind("iPageDataRepository", "EloquentPageDataRepository");
App::bind("iProductRepository", "EloquentProductRepository");
App::bind("iUserRepository", "EloquentUserRepository");
App::bind("iVideoRepository", "EloquentVideoRepository");
App::bind("iTagRepository", "EloquentTagRepository");
App::bind("iCartRepository", "EloquentCartRepository");
App::bind("iPgResponseRepository", "EloquentPgResponseRepository");
App::bind("iEnquiryRepository", "EloquentEnquiryRepository");
App::bind("iCareerRepository", "EloquentCareerRepository");
App::bind("iServicesEnquiryRepository", "EloquentServicesEnquiryRepository");
App::bind("iDemoRepository", "EloquentDemoRepository");
App::bind("iFeedbackRepository", "EloquentFeedbackRepository");
App::bind("iCouponMainRepository", "EloquentCouponMainRepository");
App::bind("iServicesCentersRepository", "EloquentServicesCentersRepository");
App::bind("iTrackingInfoRepository", "EloquentTrackingInfoRepository");
App::bind("iMessageRepository", "EloquentMessageRepository");
App::bind("iCallRepository", "EloquentCallRepository");
App::bind("iSettingsRepository", "EloquentSettingsRepository");
App::bind("iPageRepository", "EloquentPageRepository");
App::bind("iMailRecordsRepository", "EloquentMailRecordsRepository");
App::bind("iOfferRepository", "EloquentOfferRepository");

